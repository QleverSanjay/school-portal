-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.43-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema isg
--

CREATE DATABASE IF NOT EXISTS isg;
USE isg;

--
-- Definition of table `branch`
--

DROP TABLE IF EXISTS `branch`;
CREATE TABLE `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address_line` varchar(255) DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pincode` varchar(6) CHARACTER SET latin1 NOT NULL,
  `contactNumber` varchar(14) CHARACTER SET latin1 NOT NULL,
  `contactEmail` varchar(64) DEFAULT NULL,
  `websiteUrl` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `institute_id` int(11) NOT NULL,
  `taluka_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `fk_branches_institute1_idx` (`institute_id`),
  KEY `fk_branches_taluka1_idx` (`taluka_id`),
  KEY `fk_branches_district1_idx` (`district_id`),
  KEY `fk_branch_state1_idx` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;


--
-- Definition of table `caste`
--

DROP TABLE IF EXISTS `caste`;
CREATE TABLE `caste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) DEFAULT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `caste`
--

/*!40000 ALTER TABLE `caste` DISABLE KEYS */;
INSERT INTO `caste` (`id`,`code`,`name`,`active`) VALUES 
 (1,'NT','NT','Y'),
 (2,'Open','OPEN','Y'),
 (3,'SC','SC','Y'),
 (4,'OBC','OBC','Y');
/*!40000 ALTER TABLE `caste` ENABLE KEYS */;


--
-- Definition of table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` varchar(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `taluka_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_taluka1_idx` (`taluka_id`),
  CONSTRAINT `fk_city_taluka1` FOREIGN KEY (`taluka_id`) REFERENCES `taluka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;


--
-- Definition of table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_courses_branches1_idx` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course`
--

/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`,`name`,`active`,`branch_id`) VALUES 
 (3,'English','Y',NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;


--
-- Definition of table `district`
--

DROP TABLE IF EXISTS `district`;
CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_STATE_ID_idx` (`state_id`),
  CONSTRAINT `FK_STATE_ID` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

/*!40000 ALTER TABLE `district` DISABLE KEYS */;
INSERT INTO `district` (`id`,`code`,`name`,`active`,`state_id`) VALUES 
 (1,'Thane','Thane','Y',1),
 (2,'Mumbai','Mumbai','Y',1);
/*!40000 ALTER TABLE `district` ENABLE KEYS */;


--
-- Definition of table `division`
--

DROP TABLE IF EXISTS `division`;
CREATE TABLE `division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `displayName` varchar(45) NOT NULL,
  `code` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `FK_division_branch_1` (`branch_id`),
  CONSTRAINT `FK_division_branch_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

/*!40000 ALTER TABLE `division` DISABLE KEYS */;
/*!40000 ALTER TABLE `division` ENABLE KEYS */;


--
-- Definition of table `email_config`
--

DROP TABLE IF EXISTS `email_config`;
CREATE TABLE `email_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverName` varchar(255) NOT NULL,
  `port` varchar(45) NOT NULL,
  `connectionType` varchar(45) NOT NULL,
  `security` varchar(45) NOT NULL,
  `authenticationMethod` varchar(45) NOT NULL,
  `username` varchar(65) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_config`
--

/*!40000 ALTER TABLE `email_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_config` ENABLE KEYS */;


--
-- Definition of table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET latin1 NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `venue` varchar(100) CHARACTER SET latin1 NOT NULL,
  `startdate` date NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `alldayevent` char(1) DEFAULT 'N',
  `reminderSet` char(1) NOT NULL DEFAULT 'N',
  `image_url` varchar(255) DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `reccuring` char(1) DEFAULT 'N',
  `reminderbefore` int(10) DEFAULT NULL COMMENT 'reminder before will be stored in minutes basis',
  `board` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `year` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `semester` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `standard` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `division` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `summary` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `forparent` varchar(1) DEFAULT 'N',
  `forstudent` varchar(1) DEFAULT 'N',
  `forteacher` varchar(1) DEFAULT 'N',
  `course_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `frequency` varchar(15) DEFAULT NULL,
  `by_day` varchar(45) DEFAULT NULL,
  `by_date` varchar(45) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `is_schedule_updated` char(1) DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to N for new, M for Modified and D for deleted',
  PRIMARY KEY (`id`),
  KEY `fk_event_branch1_idx` (`branch_id`),
  KEY `fk_event_course_idx` (`course_id`),
  CONSTRAINT `fk_event_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;


--
-- Definition of table `event_group`
--

DROP TABLE IF EXISTS `event_group`;
CREATE TABLE `event_group` (
  `group_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_event_group_group1_idx` (`group_id`),
  KEY `fk_event_group_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_group_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_group`
--

/*!40000 ALTER TABLE `event_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_group` ENABLE KEYS */;


--
-- Definition of table `fees`
--

DROP TABLE IF EXISTS `fees`;
CREATE TABLE `fees` (
  `id` int(10) NOT NULL,
  `head_id` int(10) NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `amount` varchar(45) CHARACTER SET latin1 NOT NULL,
  `duedate` date NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `standard_id` int(11) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `isemail` char(1) DEFAULT NULL,
  `issms` char(1) DEFAULT NULL,
  `isnotification` char(1) DEFAULT NULL,
  `frequency` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fees_branch_idx` (`branch_id`),
  KEY `fk_fees_caste_idx` (`caste_id`),
  KEY `FK_fees_standard` (`standard_id`),
  CONSTRAINT `FK_fees_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fees_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fees_caste` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fees`
--

/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;


--
-- Definition of table `goal_reward_type`
--

DROP TABLE IF EXISTS `goal_reward_type`;
CREATE TABLE `goal_reward_type` (
  `code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `description` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal_reward_type`
--

/*!40000 ALTER TABLE `goal_reward_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `goal_reward_type` ENABLE KEYS */;


--
-- Definition of table `goal_status`
--

DROP TABLE IF EXISTS `goal_status`;
CREATE TABLE `goal_status` (
  `code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `description` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal_status`
--

/*!40000 ALTER TABLE `goal_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `goal_status` ENABLE KEYS */;


--
-- Definition of table `goals`
--

DROP TABLE IF EXISTS `goals`;
CREATE TABLE `goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET latin1 NOT NULL,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `reward_type` varchar(15) CHARACTER SET latin1 NOT NULL,
  `reward_amount` decimal(10,2) DEFAULT NULL,
  `reward_description` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `feedback` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `student_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status_code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parent_id_idx` (`parent_id`),
  KEY `fk_status_code_idx` (`status_code`),
  KEY `fk_goal_type_idx` (`reward_type`),
  CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `parent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reward_type` FOREIGN KEY (`reward_type`) REFERENCES `goal_reward_type` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_code` FOREIGN KEY (`status_code`) REFERENCES `goal_status` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goals`
--

/*!40000 ALTER TABLE `goals` DISABLE KEYS */;
/*!40000 ALTER TABLE `goals` ENABLE KEYS */;


--
-- Definition of table `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `chat_dialogue_id` varchar(45) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_group_branch1_idx` (`branch_id`),
  CONSTRAINT `fk_group_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group`
--

/*!40000 ALTER TABLE `group` DISABLE KEYS */;
/*!40000 ALTER TABLE `group` ENABLE KEYS */;


--
-- Definition of table `group_forum`
--

DROP TABLE IF EXISTS `group_forum`;
CREATE TABLE `group_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `student_comments_allowed` char(1) NOT NULL DEFAULT 'Y',
  `is_active` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_group_id_idx` (`group_id`),
  KEY `branch_branch_id_idx` (`branch_id`),
  CONSTRAINT `branch_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `group_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_forum`
--

/*!40000 ALTER TABLE `group_forum` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_forum` ENABLE KEYS */;


--
-- Definition of table `group_individual`
--

DROP TABLE IF EXISTS `group_individual`;
CREATE TABLE `group_individual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_groupindividual_groupid` (`group_id`),
  KEY `FK_groupindividual_userid` (`user_id`),
  KEY `FK_groupindividual_roleid` (`role_id`),
  CONSTRAINT `FK_groupindividual_groupid` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `FK_groupindividual_roleid` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_groupindividual_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_individual`
--

/*!40000 ALTER TABLE `group_individual` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_individual` ENABLE KEYS */;


--
-- Definition of table `group_standard`
--

DROP TABLE IF EXISTS `group_standard`;
CREATE TABLE `group_standard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_group_standard_group_1` (`group_id`),
  KEY `FK_group_standard_standard` (`standard_id`),
  KEY `FK_group_standard_division` (`division_id`),
  CONSTRAINT `FK_group_standard_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_group_standard_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_group_standard_group_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_standard`
--

/*!40000 ALTER TABLE `group_standard` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_standard` ENABLE KEYS */;


--
-- Definition of table `group_tags`
--

DROP TABLE IF EXISTS `group_tags`;
CREATE TABLE `group_tags` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `tag` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_grouptags_goup_id_idx` (`group_id`),
  CONSTRAINT `FK_grouptags_goup_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_tags`
--

/*!40000 ALTER TABLE `group_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_tags` ENABLE KEYS */;


--
-- Definition of table `group_user`
--

DROP TABLE IF EXISTS `group_user`;
CREATE TABLE `group_user` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `FK_groupuser_user` (`user_id`),
  CONSTRAINT `FK_groupuser_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `FK_groupuser_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_user`
--

/*!40000 ALTER TABLE `group_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_user` ENABLE KEYS */;


--
-- Definition of table `guardian_type`
--

DROP TABLE IF EXISTS `guardian_type`;
CREATE TABLE `guardian_type` (
  `guardianTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `guardianTypeName` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`guardianTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guardian_type`
--

/*!40000 ALTER TABLE `guardian_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `guardian_type` ENABLE KEYS */;


--
-- Definition of table `head`
--

DROP TABLE IF EXISTS `head`;
CREATE TABLE `head` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `head`
--

/*!40000 ALTER TABLE `head` DISABLE KEYS */;
INSERT INTO `head` (`id`,`name`,`active`) VALUES 
 (1,'Transportation fees','Y'),
 (2,'Primary','Y'),
 (3,'Term Fees','Y'),
 (4,'Annual Event Fees','Y'),
 (5,'School Development','Y');
/*!40000 ALTER TABLE `head` ENABLE KEYS */;


--
-- Definition of table `institute`
--

DROP TABLE IF EXISTS `institute`;
CREATE TABLE `institute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `logo_url` varchar(255) DEFAULT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institute`
--

/*!40000 ALTER TABLE `institute` DISABLE KEYS */;
/*!40000 ALTER TABLE `institute` ENABLE KEYS */;


--
-- Definition of table `institute_branch`
--

DROP TABLE IF EXISTS `institute_branch`;
CREATE TABLE `institute_branch` (
  `instituteBranchId` int(11) NOT NULL AUTO_INCREMENT,
  `branchId` int(11) NOT NULL,
  `instituteId` int(11) NOT NULL,
  `active` char(1) DEFAULT NULL,
  PRIMARY KEY (`instituteBranchId`),
  KEY `fk_instituebranch_branch_idx` (`branchId`),
  KEY `fk_instituebranch_institute_idx` (`instituteId`),
  CONSTRAINT `fk_instituebranch_branch` FOREIGN KEY (`branchId`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instituebranch_institute` FOREIGN KEY (`instituteId`) REFERENCES `institute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institute_branch`
--

/*!40000 ALTER TABLE `institute_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `institute_branch` ENABLE KEYS */;


--
-- Definition of table `institute_vendor`
--

DROP TABLE IF EXISTS `institute_vendor`;
CREATE TABLE `institute_vendor` (
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`branch_id`) USING BTREE,
  KEY `FK_institute_vendor_2` (`branch_id`),
  CONSTRAINT `FK_institute_vendor_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_institute_vendor_2` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institute_vendor`
--

/*!40000 ALTER TABLE `institute_vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `institute_vendor` ENABLE KEYS */;


--
-- Definition of table `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `priority` varchar(45) NOT NULL COMMENT 'High and Low',
  `date` datetime NOT NULL,
  `image_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `standard` int(11) DEFAULT NULL,
  `detail_description` text,
  PRIMARY KEY (`id`),
  KEY `fk_notification_branch1_idx` (`branch_id`),
  CONSTRAINT `fk_notice_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;


--
-- Definition of table `notice_group`
--

DROP TABLE IF EXISTS `notice_group`;
CREATE TABLE `notice_group` (
  `notice_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`notice_id`,`group_id`),
  KEY `fk_noticegroup_notice_idx` (`notice_id`),
  KEY `fk_noticegroup_group_idx` (`group_id`),
  CONSTRAINT `fk_noticegroup_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_noticegroup_notice` FOREIGN KEY (`notice_id`) REFERENCES `notice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notice_group`
--

/*!40000 ALTER TABLE `notice_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice_group` ENABLE KEYS */;


--
-- Definition of table `notice_individual`
--

DROP TABLE IF EXISTS `notice_individual`;
CREATE TABLE `notice_individual` (
  `notice_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`notice_id`,`user_id`),
  KEY `fk_noticeindividual_noticeId_idx` (`notice_id`),
  KEY `fk_noticeindividual_userid_idx` (`user_id`),
  CONSTRAINT `fk_noticeindividual_noticeId` FOREIGN KEY (`notice_id`) REFERENCES `notice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_noticeindividual_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notice_individual`
--

/*!40000 ALTER TABLE `notice_individual` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice_individual` ENABLE KEYS */;


--
-- Definition of table `otp`
--

DROP TABLE IF EXISTS `otp`;
CREATE TABLE `otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otp` varchar(6) NOT NULL,
  `generatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `primaryContact` varchar(25) NOT NULL,
  `active` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp`
--

/*!40000 ALTER TABLE `otp` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp` ENABLE KEYS */;


--
-- Definition of table `parent`
--

DROP TABLE IF EXISTS `parent`;
CREATE TABLE `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `middlename` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `primaryContact` varchar(10) NOT NULL,
  `secondaryContact` varchar(10) DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `pincode` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `taluka_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_parent_user1_idx` (`user_id`),
  KEY `fk_parent_taluka1_idx` (`taluka_id`),
  KEY `fk_parent_district1_idx` (`district_id`),
  KEY `fk_parent_state1_idx` (`state_id`),
  KEY `fk_parent_caste1_idx` (`caste_id`),
  KEY `fk_parent_religion1_idx` (`religion_id`),
  CONSTRAINT `fk_parent_caste1` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_religion1` FOREIGN KEY (`religion_id`) REFERENCES `religion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_state1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_taluka1` FOREIGN KEY (`taluka_id`) REFERENCES `taluka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parent`
--

/*!40000 ALTER TABLE `parent` DISABLE KEYS */;
/*!40000 ALTER TABLE `parent` ENABLE KEYS */;


--
-- Definition of table `passbook`
--

DROP TABLE IF EXISTS `passbook`;
CREATE TABLE `passbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `date` date NOT NULL,
  `is_debit` char(1) CHARACTER SET latin1 NOT NULL,
  `is_credit` char(45) CHARACTER SET latin1 NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_transaction_id` int(11) DEFAULT NULL,
  `for_entity_name` varchar(100) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_created_by_user_id_idx` (`user_id`),
  KEY `fk_parent_transaction_id_idx` (`parent_transaction_id`),
  CONSTRAINT `fk_created_by_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_transaction_id` FOREIGN KEY (`parent_transaction_id`) REFERENCES `passbook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passbook`
--

/*!40000 ALTER TABLE `passbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `passbook` ENABLE KEYS */;


--
-- Definition of table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `parent_id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `paid_date` datetime NOT NULL,
  PRIMARY KEY (`parent_id`,`fees_id`,`student_id`),
  KEY `parent_id_idx` (`parent_id`),
  KEY `fees_id_fk_idx` (`student_id`),
  KEY `fk_idx` (`fees_id`),
  CONSTRAINT `fees_id_fk` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parent_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `parent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_id_fk` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;


--
-- Definition of table `personal_task`
--

DROP TABLE IF EXISTS `personal_task`;
CREATE TABLE `personal_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `venue` varchar(100) DEFAULT NULL,
  `startdate` date NOT NULL,
  `enddate` date DEFAULT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `alldayevent` char(1) DEFAULT 'N',
  `reminder` char(1) DEFAULT 'N',
  `reminderbefore` int(10) DEFAULT NULL COMMENT 'reminder before will be stored in minutes basis',
  `forstudent` varchar(1) DEFAULT 'N',
  `student_id` int(11) DEFAULT NULL,
  `recurring` char(1) DEFAULT 'N',
  `frequency` varchar(15) DEFAULT NULL,
  `by_day` varchar(45) DEFAULT NULL,
  `by_date` varchar(45) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `is_schedule_updated` char(1) DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_student_id_idx` (`student_id`),
  KEY `fk_role_id_idx` (`role_id`),
  CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personal_task`
--

/*!40000 ALTER TABLE `personal_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_task` ENABLE KEYS */;


--
-- Definition of table `religion`
--

DROP TABLE IF EXISTS `religion`;
CREATE TABLE `religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `religion`
--

/*!40000 ALTER TABLE `religion` DISABLE KEYS */;
INSERT INTO `religion` (`id`,`code`,`name`,`active`) VALUES 
 (1,'HN','Hindu','Y');
/*!40000 ALTER TABLE `religion` ENABLE KEYS */;


--
-- Definition of table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`,`name`) VALUES 
 (1,'ADMIN'),
 (2,'STUDENT'),
 (3,'PARENT'),
 (4,'TEACHER'),
 (5,'VENDOR'),
 (6,'SCHOOL ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


--
-- Definition of table `school_admin`
--

DROP TABLE IF EXISTS `school_admin`;
CREATE TABLE `school_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) NOT NULL,
  `dateOfBirth` varchar(45) DEFAULT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `primaryContact` varchar(45) NOT NULL,
  `secondaryContact` varchar(45) DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `pinCode` varchar(45) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` varchar(45) NOT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_schooladminuser_1_idx` (`user_id`),
  CONSTRAINT `FK_schooladminuser_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school_admin`
--

/*!40000 ALTER TABLE `school_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_admin` ENABLE KEYS */;


--
-- Definition of table `sms_config`
--

DROP TABLE IF EXISTS `sms_config`;
CREATE TABLE `sms_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendorUrl` varchar(255) NOT NULL,
  `username` varchar(65) NOT NULL,
  `password` varchar(45) NOT NULL,
  `sid` varchar(45) NOT NULL,
  `flash_message` varchar(1) NOT NULL DEFAULT 'N',
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_config`
--

/*!40000 ALTER TABLE `sms_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_config` ENABLE KEYS */;


--
-- Definition of table `standard`
--

DROP TABLE IF EXISTS `standard`;
CREATE TABLE `standard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `displayName` varchar(45) NOT NULL,
  `code` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `FK_standard_branch_1` (`branch_id`),
  CONSTRAINT `FK_standard_branch_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standard`
--

/*!40000 ALTER TABLE `standard` DISABLE KEYS */;
/*!40000 ALTER TABLE `standard` ENABLE KEYS */;


--
-- Definition of table `standard_division`
--

DROP TABLE IF EXISTS `standard_division`;
CREATE TABLE `standard_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_standard_division_branch` (`branch_id`),
  KEY `FK_standard_division_standard` (`standard_id`),
  KEY `FK_standard_division_division` (`division_id`),
  CONSTRAINT `FK_standard_division_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_standard_division_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_standard_division_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standard_division`
--

/*!40000 ALTER TABLE `standard_division` DISABLE KEYS */;
/*!40000 ALTER TABLE `standard_division` ENABLE KEYS */;


--
-- Definition of table `state`
--

DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` (`id`,`code`,`name`,`active`) VALUES 
 (1,'NH','Maharashtra','Y');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;


--
-- Definition of table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `father_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mother_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `dob` date NOT NULL,
  `relation_with_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email_address` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mobile` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `registration_code` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `line1` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `pincode` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `religion` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `standard_id` int(11) DEFAULT NULL,
  `standard_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `roll_number` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_child` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `user_id` int(11) NOT NULL,
  `student_upload_id` int(11) DEFAULT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_student_branch_idx` (`branch_id`),
  KEY `fk_student_casteId_idx` (`caste_id`),
  KEY `FK_student_uploadstudent` (`student_upload_id`),
  KEY `FK_student_standard` (`standard_id`),
  KEY `FK_student_division` (`division_id`),
  CONSTRAINT `FK_student_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_student_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_casteId` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_student_uploadstudent` FOREIGN KEY (`student_upload_id`) REFERENCES `student_upload` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

/*!40000 ALTER TABLE `student` DISABLE KEYS */;
/*!40000 ALTER TABLE `student` ENABLE KEYS */;


--
-- Definition of table `student_event`
--

DROP TABLE IF EXISTS `student_event`;
CREATE TABLE `student_event` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `fk_event_student_event1_idx` (`event_id`),
  KEY `fk_event_user_event1_idx` (`user_id`),
  CONSTRAINT `fk_event_student_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_user_event1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_event`
--

/*!40000 ALTER TABLE `student_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_event` ENABLE KEYS */;


--
-- Definition of table `student_group`
--

DROP TABLE IF EXISTS `student_group`;
CREATE TABLE `student_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `joined` char(1) DEFAULT 'N' COMMENT 'if value set to ''N'' means group is suggested group.\nelse if value set to ''Y'' means group is joined by student.',
  `group_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_student_group_group1_idx` (`group_id`),
  KEY `fk_student_group_student1_idx` (`student_id`),
  CONSTRAINT `FK_student_group_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `fk_student_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_group`
--

/*!40000 ALTER TABLE `student_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_group` ENABLE KEYS */;


--
-- Definition of table `student_upload`
--

DROP TABLE IF EXISTS `student_upload`;
CREATE TABLE `student_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `father_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mother_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dob` date NOT NULL,
  `relation_with_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `mobile` varchar(10) CHARACTER SET latin1 NOT NULL,
  `registration_code` varchar(10) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(10) NOT NULL,
  `line1` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `pincode` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `religion` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `standard_id` int(11) NOT NULL,
  `standard_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `roll_number` varchar(45) CHARACTER SET latin1 NOT NULL,
  `goal_child` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `branch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_studentupload_branchId_idx` (`branch_id`),
  KEY `fk_studentupload_userId_idx` (`user_id`),
  KEY `fk_studentupload_casteId_idx` (`caste_id`),
  KEY `fk_student_upload_standard` (`standard_id`),
  KEY `fk_student_upload_division` (`division_id`),
  CONSTRAINT `fk_student_upload_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_upload_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_casteId` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_userId` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_upload`
--

/*!40000 ALTER TABLE `student_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_upload` ENABLE KEYS */;


--
-- Definition of table `taluka`
--

DROP TABLE IF EXISTS `taluka`;
CREATE TABLE `taluka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `district_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DISTRICT_idx` (`district_id`),
  CONSTRAINT `FK_DISTRICT` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taluka`
--

/*!40000 ALTER TABLE `taluka` DISABLE KEYS */;
INSERT INTO `taluka` (`id`,`code`,`name`,`active`,`district_id`) VALUES 
 (1,'Kalyan','Kalyan','Y',1),
 (2,'Mumbai','Mumbai','Y',2);
/*!40000 ALTER TABLE `taluka` ENABLE KEYS */;


--
-- Definition of table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `emailAddress` varchar(45) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(1) CHARACTER SET latin1 NOT NULL,
  `primaryContact` varchar(45) CHARACTER SET latin1 NOT NULL,
  `secondaryContact` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pinCode` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_teacher_1` (`user_id`),
  CONSTRAINT `FK_teacher_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher`
--

/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;


--
-- Definition of table `token`
--

DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `token` varchar(30) CHARACTER SET latin1 NOT NULL,
  `created_date` datetime NOT NULL,
  `valid_for` int(11) NOT NULL DEFAULT '60' COMMENT 'Valid for in minutes after creation',
  `for_entity_id` int(11) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `token`
--

/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `role_id` int(10) unsigned NOT NULL,
  `institute_id` int(11) DEFAULT NULL,
  `gcm_id` varchar(255) DEFAULT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `FK_user_roll1` (`role_id`) USING BTREE,
  KEY `FK_user_institute` (`institute_id`),
  CONSTRAINT `FK_user_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_user_institute` FOREIGN KEY (`institute_id`) REFERENCES `institute` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`,`username`,`password`,`active`,`role_id`,`institute_id`,`gcm_id`,`isDelete`) VALUES 
 (1,'admin','pass','Y',1,NULL,NULL,'N');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


--
-- Definition of table `user_chat_profile`
--

DROP TABLE IF EXISTS `user_chat_profile`;
CREATE TABLE `user_chat_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `chat_user_profile_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_user_profile_id_UNIQUE` (`chat_user_profile_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_chat_profile`
--

/*!40000 ALTER TABLE `user_chat_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_chat_profile` ENABLE KEYS */;


--
-- Definition of table `user_event`
--

DROP TABLE IF EXISTS `user_event`;
CREATE TABLE `user_event` (
  `user_id` int(11) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `entity_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `event_group_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to N for new, M for Modified and D for deleted',
  PRIMARY KEY (`id`),
  KEY `FK_userevent_user` (`user_id`),
  KEY `FK_userevent_role` (`role_id`),
  KEY `FK_userevent_event` (`event_id`),
  KEY `FK_userevent_eventgroup` (`event_group_id`),
  CONSTRAINT `FK_userevent_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `FK_userevent_eventgroup` FOREIGN KEY (`event_group_id`) REFERENCES `event_group` (`id`),
  CONSTRAINT `FK_userevent_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_userevent_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_event`
--

/*!40000 ALTER TABLE `user_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_event` ENABLE KEYS */;


--
-- Definition of table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `emailAddress` varchar(45) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(1) CHARACTER SET latin1 NOT NULL,
  `primaryContact` varchar(45) CHARACTER SET latin1 NOT NULL,
  `secondaryContact` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pinCode` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_teacher_1` (`user_id`),
  CONSTRAINT `FK_vendor_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor`
--

/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

--- Added 19-01-2016 ---
ALTER TABLE `isg`.`event_group` ADD COLUMN `status` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to  N for new, M for Modified and D for deleted'AFTER `id`;

ALTER TABLE `isg`.`student_event` ADD COLUMN `status` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'Set it  to N for new, M for Modified and D for deleted' AFTER `event_id`;

ALTER TABLE `isg`.`notice` MODIFY COLUMN `id` INTEGER NOT NULL,
 ADD COLUMN `sent_by` INTEGER NOT NULL COMMENT 'user id ' AFTER `detail_description`,
 ADD CONSTRAINT `FK_notice_user` FOREIGN KEY `FK_notice_user` (`sent_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`notice` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;

ALTER TABLE `isg`.`fees` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;

ALTER TABLE `isg`.`group_tags` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;
--- End 19-01-2016 ---

--- Added 20-01-2016 -----
ALTER TABLE `isg`.`notice` DROP COLUMN `standard` ;
ALTER TABLE `isg`.`student` DROP COLUMN `standard_name` ;
ALTER TABLE `isg`.`event` DROP COLUMN `division` , DROP COLUMN `standard` , DROP COLUMN `semester` , DROP COLUMN `year` , DROP COLUMN `board` ;
ALTER TABLE `isg`.`student_upload` DROP COLUMN `standard_name` ;
--- End 20-01-2016 -----

--- Added 21-01-2016 ---
ALTER TABLE `isg`.`user` MODIFY COLUMN `username` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
--- End 21-01-2016---

--- Aded 23-01-2016 --- 
ALTER TABLE `isg`.`student` ADD COLUMN `hobbies` VARCHAR(100) NULL  AFTER `isDelete` ;
ALTER TABLE `isg`.`student` CHANGE COLUMN `hobbies` `hobbies` VARCHAR(100) NULL DEFAULT NULL  AFTER `goal_parent` ;
--- End 23-01-2016 ---


--- Added 28-01-2016 ---
CREATE TABLE `isg`.`subject` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `branch_id` INTEGER NOT NULL,
  `standard_id` INTEGER NOT NULL,
  `semester` VARCHAR(45) NOT NULL,
  `name` VARCHAR(65) NOT NULL,
  `total_marks` VARCHAR(45) NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `is_delete` CHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_subject_branch` FOREIGN KEY `FK_subject_branch` (`branch_id`)
    REFERENCES `branch` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_subject_standard` FOREIGN KEY `FK_subject_standard` (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


CREATE TABLE `isg`.`student_result` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `student_id` INTEGER NOT NULL,
  `result` VARCHAR(45),
  `final_grade` VARCHAR(45),
  `position` VARCHAR(45),
  `general_remark` VARCHAR(100),
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_student_result_student` FOREIGN KEY `FK_student_result_student` (`student_id`)
    REFERENCES `student` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


CREATE TABLE `isg`.`student_marks` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `student_id` INTEGER NOT NULL,
  `theory` VARCHAR(45),
  `practical` VARCHAR(45),
  `obtained` VARCHAR(45),
  `subject_id` INTEGER NOT NULL,
  `student_result_id` INTEGER NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_student_marks_student` FOREIGN KEY `FK_student_marks_student` (`student_id`)
    REFERENCES `student` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_student_marks_subject` FOREIGN KEY `FK_student_marks_subject` (`subject_id`)
    REFERENCES `subject` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_student_marks_student_result` FOREIGN KEY `FK_student_marks_student_result` (`student_result_id`)
    REFERENCES `student_result` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;
--- End 28-01-2016 ---

--- Added 30-01-2016 ---

ALTER TABLE `isg`.`student_result` ADD COLUMN `gross_total` VARCHAR(45) AFTER `general_remark`;
ALTER TABLE `isg`.`student_result` ADD COLUMN `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `gross_total`;
--- End 30-01-2016 ---

--- Added 31-01-2016 ---
CREATE TABLE `isg`.`user_mall_profile` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `is_active` VARCHAR(1) NOT NULL DEFAULT 'Y',
  `is_delete` VARCHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_user_mall_profile_user` FOREIGN KEY `FK_user_mall_profile_user` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `branch_forum_detail`;

CREATE  TABLE `branch_forum_detail` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `forum_category_id` INT(11) NOT NULL ,
  `read_write_group_id` INT(11) NOT NULL ,
  `read_only_group_id` INT(11) NOT NULL ,
  `branch_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_branch_id_idx` (`branch_id` ASC) ,
  CONSTRAINT `fk_branch_id`
    FOREIGN KEY (`branch_id` )
    REFERENCES `branch` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

DROP TABLE IF EXISTS `group_forum`;
CREATE TABLE `group_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `read_only` char(1) NOT NULL DEFAULT 'Y',
  `is_active` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `group_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_group_id_idx` (`group_id`),
  KEY `branch_branch_id_idx` (`branch_id`),
  CONSTRAINT `branch_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `group_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_forum_profile`;

CREATE TABLE `user_forum_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `forum_profile_id` int(11) NOT NULL,
  `is_active` char(1) DEFAULT 'Y',
  `is_delete` char(1) DEFAULT 'N',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `forum_profile_id_UNIQUE` (`forum_profile_id`),
  KEY `fk_user_id_forum_profile` (`user_id`),
  CONSTRAINT `fk_user_id_forum_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--- End 31-01-2016 ---

--- Added 02-02-2016 ---
ALTER TABLE `isg`.`group_forum` ADD COLUMN `forum_group_id` INTEGER NOT NULL AFTER `branch_id`;
--- End 02-02-2016 ---

--- Added 03-02-2016 ---
DROP TABLE IF EXISTS `isg`.`menu_items`;

CREATE TABLE  `isg`.`menu_items` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_description` varchar(100) DEFAULT NULL,
  `state_url` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `isg`.`menu_permission`;

CREATE TABLE  `isg`.`menu_permission` (
  `menu_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `permission_name` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `rest_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`menu_permission_id`),
  KEY `fk_menu_id_permission` (`menu_id`),
  CONSTRAINT `fk_menu_id_permission` FOREIGN KEY (`menu_id`) REFERENCES `menu_items` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `isg`.`role_menu`;

CREATE TABLE  `isg`.`role_menu` (
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`,`role_id`),
  KEY `fk_role_menu_role_id` (`role_id`),
  CONSTRAINT `fk_role_menu_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu_items` (`menu_id`),
  CONSTRAINT `fk_role_menu_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `isg`.`role_menu_permission`;

CREATE TABLE  `isg`.`role_menu_permission` (
  `menu_permission_id` int(11) NOT NULL,
  `role_id` varchar(50) NOT NULL,
  PRIMARY KEY (`menu_permission_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `isg`.`user_role`;

CREATE TABLE  `isg`.`user_role` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_role_role_id` (`role_id`),
  CONSTRAINT `fk_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--- End 03-02-2016 ---


--- Add 04-02-2016 ---

DROP TABLE IF EXISTS `isg`.`vendor`;
CREATE TABLE  `isg`.`vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_legal_name` varchar(100) NOT NULL,
  `merchant_marketing_name` varchar(100) NOT NULL,
  `bank_account_number` varchar(20) NOT NULL,
  `me_code` varchar(6) NOT NULL,
  `t_id` varchar(8) NOT NULL,
  `lg_code` varchar(6) NOT NULL,
  `lc_code` varchar(6) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `address_line1` varchar(100) NOT NULL,
  `area` varchar(56) NOT NULL,
  `city` varchar(56) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `primary_phone` varchar(12) NOT NULL,
  `secondary_phone` varchar(12) DEFAULT NULL,
  `fax_no` varchar(12) DEFAULT NULL,
  `email` varchar(56) NOT NULL,
  `shop_ownership` char(1) DEFAULT NULL,
  `other_group` varchar(100) DEFAULT NULL,
  `other_group_address` varchar(256) DEFAULT NULL,
  `proprietor_name` varchar(256) NOT NULL,
  `proprietor_address_line1` varchar(100) NOT NULL,
  `proprietor_address_area` varchar(56) NOT NULL,
  `proprietor_address_city` varchar(56) NOT NULL,
  `proprietor_address_pincode` varchar(6) NOT NULL,
  `proprietor_primary_phone` varchar(12) NOT NULL,
  `proprietor_secondary_phone` varchar(12) DEFAULT NULL,
  `proprietor_fax_no` varchar(12) DEFAULT NULL,
  `ownership` varchar(56) NOT NULL,
  `ownership_other` varchar(100) DEFAULT NULL,
  `business_start_date` date NOT NULL,
  `merchant_pan` varchar(12) NOT NULL,
  `credit_card_accepted` char(1) DEFAULT NULL,
  `credit_card_accepted_other` varchar(56) DEFAULT NULL,
  `annual_turn_over` varchar(15) DEFAULT NULL,
  `annual_total_on_credit_card` varchar(15) DEFAULT NULL,
  `average_per_transaction` varchar(15) DEFAULT NULL,
  `credit_card_bank` varchar(56) DEFAULT NULL,
  `credit_card_since_year` varchar(4) DEFAULT NULL,
  `eb_saving_account` char(1) DEFAULT NULL,
  `eb_current_account` char(1) DEFAULT NULL,
  `eb_term_deposit` char(1) DEFAULT NULL,
  `eb_demat` char(1) DEFAULT NULL,
  `eb_auto_loan` char(1) DEFAULT NULL,
  `eb_personal_loan` char(1) DEFAULT NULL,
  `eb_two_wheeler` char(1) DEFAULT NULL,
  `eb_credit_card` char(1) DEFAULT NULL,
  `eb_loan_against_shares` char(1) DEFAULT NULL,
  `eb_business_banking` char(1) DEFAULT NULL,
  `eb_insurance` char(1) DEFAULT NULL,
  `eb_mutual_fund` char(1) DEFAULT NULL,
  `card_accept_type_visa` char(1) DEFAULT NULL,
  `card_accept_type_master` char(1) DEFAULT NULL,
  `card_accept_type_dinner` char(1) DEFAULT NULL,
  `physical_mpr` char(1) DEFAULT NULL,
  `email_mpr` char(1) DEFAULT NULL,
  `email_mpr_text` varchar(100) DEFAULT NULL,
  `mpr_frequency` varchar(100) DEFAULT NULL,
  `selling_category` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_vendor_userId` (`user_id`),
  CONSTRAINT `FK_vendor_userId` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;


-- Add 4-02-2016 --

ALTER TABLE `isg`.`head` ADD COLUMN `branch_id` INTEGER AFTER `active`,
 ADD CONSTRAINT `FK_head_branchId` FOREIGN KEY `FK_head_branchId` (`branch_id`)
    REFERENCES `branch` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`head` MODIFY COLUMN `branch_id` INTEGER,
 ADD COLUMN `is_delete` CHAR(1) NOT NULL AFTER `branch_id`;

ALTER TABLE `isg`.`head` MODIFY COLUMN `is_delete` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N';

ALTER TABLE `isg`.`head` DROP INDEX `name_UNIQUE`,
 ADD UNIQUE INDEX `name_UNIQUE` USING BTREE(`name`, `branch_id`);


ALTER TABLE `isg`.`fees` MODIFY COLUMN `head_id` INTEGER NOT NULL,
 ADD CONSTRAINT `FK_fees_head` FOREIGN KEY `FK_fees_head` (`head_id`)
    REFERENCES `head` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- End 4-02-2016 --

    
-- Add 13-02-2016 --

DROP TABLE IF EXISTS `isg`.`academic_year`;
CREATE TABLE  `isg`.`academic_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `is_current_active_year` char(1) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_academic_year_branchId` (`branch_id`),
  CONSTRAINT `FK_academic_year_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `isg`.`holidays`;
CREATE TABLE  `isg`.`holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_academic_year` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_holidays_currentYear` (`current_academic_year`),
  KEY `FK_holidays_branchId` (`branch_id`),
  CONSTRAINT `FK_holidays_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_holidays_currentYear` FOREIGN KEY (`current_academic_year`) REFERENCES `academic_year` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `isg`.`working_days`;
CREATE TABLE  `isg`.`working_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_academic_year` int(11) NOT NULL,
  `monday` char(1) DEFAULT NULL,
  `tuesday` char(1) DEFAULT NULL,
  `wednesday` char(1) DEFAULT NULL,
  `thursday` char(1) DEFAULT NULL,
  `friday` char(1) DEFAULT NULL,
  `saturday` char(1) DEFAULT NULL,
  `sunday` char(1) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_working_days_currentYear` (`current_academic_year`),
  KEY `FK_working_days_branchId` (`branch_id`),
  CONSTRAINT `FK_working_days_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_working_days_currentYear` FOREIGN KEY (`current_academic_year`) REFERENCES `academic_year` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
    
-- End 13-02-2016 --

-- ADD 20-02-2016 --
ALTER TABLE `isg`.`student_marks` ADD COLUMN `remarks` VARCHAR(100) AFTER `student_result_id`;
-- END 20-02-2016 -- 

-- ADD 21-02-2016 --
ALTER TABLE `isg`.`student_result` ADD COLUMN `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `gross_total`;

ALTER TABLE `isg`.`user` ADD COLUMN `authentication_type` VARCHAR(45) NOT NULL DEFAULT 'APPLICATION' AFTER `isDelete`;

DROP TABLE IF EXISTS `isg`.`authentication_type`;
CREATE  TABLE `isg`.`authentication_type` (
  `code` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`code`) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) )
DEFAULT CHARACTER SET = utf8;


ALTER TABLE `isg_sit`.`user` 
  ADD CONSTRAINT `fk_authentication_type`
  FOREIGN KEY (`authentication_type` )
  REFERENCES `isg_sit`.`authentication_type` (`code` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_authentication_type_idx` (`authentication_type` ASC) ;

-- END 21-02-2016 --
 
-- ADD 23-02-2016 --

ALTER TABLE `isg`.`group` ADD COLUMN `created_by` INTEGER NOT NULL AFTER `isDelete`,
 ADD CONSTRAINT `FK_group_user` FOREIGN KEY `FK_group_user` (`created_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- END 23-02-2016 --

-- ADD 25-02-2016 --

    ALTER TABLE `isg`.`email_config` ADD COLUMN `password` VARCHAR(45) NOT NULL AFTER `active`;
    
-- END 25-02-2016 --   

-- ADD 28-02-2016 --

ALTER TABLE `isg`.`group` ADD COLUMN `is_public_group` CHAR(1) DEFAULT 'N' AFTER `created_by`;

-- END 28-02-2016 -- 
    
-- ADD 10-03-2016 --

DELIMITER $$



DROP TRIGGER IF EXISTS isg.parent_insert_trig$$

USE `isg`$$





CREATE

DEFINER=`root`@`localhost`

TRIGGER `isg`.`parent_insert_trig`

AFTER INSERT ON `isg`.`parent`

FOR EACH ROW

-- Edit trigger body code below this line. Do not edit lines above this one

BEGIN



IF NOT EXISTS (SELECT id FROM contact WHERE user_id = NEW.user_id) THEN

insert into contact(email,phone,user_id,firstname, lastname, city, photo) 

values(NEW.email, NEW.primaryContact, NEW.user_id, NEW.firstname, NEW.lastname, NEW.city, NEW.photo);

END IF;



END$$

DELIMITER ;



ALTER TABLE `isg`.`token` ADD COLUMN `role_id` INT(10) UNSIGNED NOT NULL  AFTER `for_entity_id` ,
  ADD CONSTRAINT `fk_token_role_id`
  FOREIGN KEY (`role_id` )
  REFERENCES `isg`.`role` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_token_role_id_idx` (`role_id` ASC) ;


DROP TABLE IF EXISTS `isg`.`contact`;
CREATE TABLE  `isg`.`contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `photo` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `firstname` varchar(45) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_profile_search_user_id_idx` (`user_id`),
  CONSTRAINT `fk_profile_search_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `isg`.`user_contact`;
CREATE TABLE  `isg`.`user_contact` (
  `user_id` int(11) NOT NULL,
  `contact_user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`contact_user_id`),
  KEY `fk_user_contact_user_id_idx` (`user_id`),
  KEY `fk_user_contact_contact_user_id_idx` (`contact_user_id`),
  CONSTRAINT `fk_user_contact_contact_user_id` FOREIGN KEY (`contact_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_contact_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `isg`.`forgot_password_otp`;
CREATE TABLE  `isg`.`forgot_password_otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otp` varchar(10) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `expired_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `fk_forgot_password_otp_user_id_idx` (`user_id`),
  CONSTRAINT `fk_forgot_password_otp_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- END 10-03-2016 --


-- ADD 11-03-2016 --

ALTER TABLE `isg`.`fees` ADD COLUMN `academic_year_id` INTEGER NOT NULL,
 ADD CONSTRAINT `FK_fees_academicYear` FOREIGN KEY `FK_fees_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`standard` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `active`,
 ADD CONSTRAINT `FK_standard_academicYear` FOREIGN KEY `FK_standard_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`division` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `active`,
 ADD CONSTRAINT `FK_division_academicYear` FOREIGN KEY `FK_division_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`head` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `is_delete`,
 ADD CONSTRAINT `FK_head_academicYear` FOREIGN KEY `FK_head_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`notice` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `sent_by`,
 ADD CONSTRAINT `FK_notice_academicYear` FOREIGN KEY `FK_notice_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
    
ALTER TABLE `isg`.`group` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `is_public_group`,
 ADD CONSTRAINT `FK_group_academicYear` FOREIGN KEY `FK_group_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;   
    
ALTER TABLE `isg`.`event` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `forvendor`,
 ADD CONSTRAINT `FK_event_academicYear` FOREIGN KEY `FK_event_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;    

-- END 11-03-2016 -- 

-- Add 16-03-2016  Add Academic Year-- 

ALTER TABLE `isg`.`group_forum` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `created_by`,
 ADD CONSTRAINT `FK_group_forum_academicYear` FOREIGN KEY `FK_group_forum_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`subject` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `is_delete`,
 ADD CONSTRAINT `FK_subject_academicYear` FOREIGN KEY `FK_subject_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;



ALTER TABLE `isg`.`student_result` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `created_date`,
 ADD CONSTRAINT `FK_student_result_academicYear` FOREIGN KEY `FK_student_result_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

-- End 16-03-2016 --        

-- START 29-04-2016 --

CREATE TABLE `isg`.`payment_setting` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `gateway_id` INTEGER NOT NULL,
  `ic_amount` VARCHAR(10),
  `ic_percentage` VARCHAR(10),
  `institute_id` INTEGER ,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_payment_setting_institute` FOREIGN KEY `FK_payment_setting_institute` (`institute_id`)
    REFERENCES `institute` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)

-- END 29-04-2016 --