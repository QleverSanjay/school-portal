/* 09-08-2016*/

ALTER TABLE `qfix`.`standard` MODIFY COLUMN `code` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;
update `menu_items` set menu_name = "Groups And Communities" where menu_id = 3;

/* 09-08-2016 */

/* 11-08-2016 */

DROP TABLE IF EXISTS `qfix`.`subject_category`;
CREATE TABLE  `qfix`.`subject_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `qfix`.`subject`;
CREATE TABLE  `qfix`.`subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `category_id` int(11) NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `code` varchar(45) DEFAULT NULL,
  `theory` double NOT NULL,
  `practical` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_subject_branch` (`branch_id`),
  KEY `FK_subject_subject_category` (`category_id`),
  CONSTRAINT `FK_subject_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_subject_subject_category` FOREIGN KEY (`category_id`) REFERENCES `subject_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO subject_category values (1, 'DEFAULT', 'DEFAULT', 'N');

CREATE TABLE `qfix`.`standard_subject` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `standard_id` INTEGER NOT NULL,
  `subject_id` INTEGER NOT NULL,
  `is_delete` CHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_standard_subject_standard` FOREIGN KEY `FK_standard_subject_standard` (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_standard_subject_subject` FOREIGN KEY `FK_standard_subject_subject` (`subject_id`)
    REFERENCES `subject` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


CREATE TABLE `qfix`.`teacher_standard` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `teacher_id` INTEGER UNSIGNED NOT NULL,
  `standard_id` INTEGER NOT NULL,
  `division_id` INTEGER,
  `subject_id` INTEGER,
  `is_delete` CHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_teacher_standard_techer` FOREIGN KEY `FK_teacher_standard_techer` (`teacher_id`)
    REFERENCES `teacher` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_teacher_standard_standard` FOREIGN KEY `FK_teacher_standard_standard` (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_teacher_standard_division` FOREIGN KEY `FK_teacher_standard_division` (`division_id`)
    REFERENCES `division` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_teacher_standard_subject` FOREIGN KEY `FK_teacher_standard_subject` (`subject_id`)
    REFERENCES `subject` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


/* 11-08-2016 */

/* 16-08-2016 */

drop table `payments`;

CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `paid_date` datetime NOT NULL,
  `payment_audit_id` int(11) DEFAULT NULL,
  `fees_id` int(11) DEFAULT NULL,
  `passbook_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fees_id_fk_idx` (`student_id`),
  KEY `fk_idx` (`fees_id`),
  KEY `FK_payments_payment_audit_id` (`payment_audit_id`),
  KEY `fk_payment_passbook_id_idx` (`passbook_id`),
  KEY `fk_payment_created_by_user_idx` (`created_by`),
  CONSTRAINT `fk_payment_passbook_id` FOREIGN KEY (`passbook_id`) REFERENCES `passbook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fees_id_fk` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_payments_payment_audit_id` FOREIGN KEY (`payment_audit_id`) REFERENCES `payment_audit` (`id`),
  CONSTRAINT `fk_payment_created_by_user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_id_fk` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

/* 16-08-2016 */


/* 22-08-2016 */

ALTER TABLE `qfix`.`student_marks` DROP COLUMN `student_id`,
 DROP FOREIGN KEY `FK_student_marks_student`;
/* 22-08-2016 */

 
 
 /* 31-08-2016 */

insert into menu_permission (menu_permission_id, menu_id, permission_name, url, rest_url) values(130, 8, 'View', 'student.view', null);
insert into `role_menu_permission` (menu_permission_id, role_id) values (130, 4) ;
 /* 31-08-2016 */




/* 01-09-2016 */

truncate  `taluka`;
truncate  `district`;
truncate  `state`;

/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` (`id`,`code`,`name`,`active`) VALUES
 (1,'1','JAMMU & KASHMIR','Y'),
 (2,'2','HIMACHAL PRADESH','Y'),
 (3,'3','PUNJAB','Y'),
 (4,'4','CHANDIGARH','Y'),
 (5,'5','UTTARANCHAL','Y'),
 (6,'6','HARYANA','Y'),
 (7,'7','DELHI','Y'),
 (8,'8','RAJASTHAN','Y'),
 (9,'9','UTTAR PRADESH','Y'),
 (10,'10','BIHAR','Y'),
 (11,'11','SIKKIM','Y'),
 (12,'12','ARUNACHAL PRADESH','Y'),
 (13,'13','NAGALAND','Y'),
 (14,'14','MANIPUR','Y'),
 (15,'15','MIZORAM','Y'),
 (16,'16','TRIPURA','Y'),
 (17,'17','MEGHALAYA','Y'),
 (18,'18','ASSAM','Y'),
 (19,'19','WEST BENGAL','Y'),
 (20,'20','JHARKHAND','Y'),
 (21,'21','ORISSA','Y'),
 (22,'22','CHHATTISGARH','Y'),
 (23,'23','MADHYA PRADESH','Y'),
 (24,'24','GUJARAT','Y'),
 (25,'25','DAMAN & DIU','Y'),
 (26,'26','DADRA & NAGAR HAVELI','Y'),
 (27,'27','MAHARASHTRA','Y'),
 (28,'28','ANDHRA PRADESH','Y'),
 (29,'29','KARNATAKA','Y'),
 (30,'30','GOA','Y'),
 (31,'31','LAKSHADWEEP','Y'),
 (32,'32','KERALA','Y'),
 (33,'33','TAMIL NADU','Y'),
 (34,'34','PONDICHERRY','Y'),
 (35,'35','ANDAMAN & NICOBAR ISLANDS','Y');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;




/*!40000 ALTER TABLE `district` DISABLE KEYS */;
INSERT INTO `district` (`id`,`code`,`name`,`active`,`state_id`) VALUES 
 (1,'1',' JAMMU & KASHMIR','Y',1),
 (2,'2',' Kupwara','Y',1),
 (3,'3',' Baramula','Y',1),
 (4,'4',' Srinagar','Y',1),
 (5,'5',' Badgam','Y',1),
 (6,'6',' Pulwama','Y',1),
 (7,'7',' Anantnag','Y',1),
 (8,'8',' Leh (Ladakh)','Y',1),
 (9,'9',' Kargil','Y',1),
 (10,'10',' Doda','Y',1),
 (11,'11',' Udhampur','Y',1),
 (12,'12',' Punch','Y',1),
 (13,'13',' Rajauri','Y',1),
 (14,'14',' Jammu','Y',1),
 (15,'15',' Kathua','Y',1),
 (16,'16',' HIMACHAL PRADESH','Y',2),
 (17,'17',' Chamba','Y',2),
 (18,'18',' Kangra','Y',2),
 (19,'19',' Lahul & Spiti','Y',2),
 (20,'20',' Kullu','Y',2),
 (21,'21',' Mandi','Y',2),
 (22,'22',' Hamirpur','Y',2),
 (23,'23',' Una','Y',2),
 (24,'24',' Bilaspur','Y',2),
 (25,'25',' Solan','Y',2),
 (26,'26',' Sirmaur','Y',2),
 (27,'27',' Shimla','Y',2),
 (28,'28',' Kinnaur','Y',2),
 (29,'29',' PUNJAB','Y',3),
 (30,'30',' Gurdaspur','Y',3),
 (31,'31',' Amritsar','Y',3),
 (32,'32',' Kapurthala','Y',3),
 (33,'33',' Jalandhar','Y',3),
 (34,'34',' Hoshiarpur','Y',3),
 (35,'35',' Nawanshahr *','Y',3),
 (36,'36',' Rupnagar','Y',3),
 (37,'37',' Fatehgarh Sahib *','Y',3),
 (38,'38',' Ludhiana','Y',3),
 (39,'39',' Moga *','Y',3),
 (40,'40',' Firozpur','Y',3),
 (41,'41',' Muktsar *','Y',3),
 (42,'42',' Faridkot','Y',3),
 (43,'43',' Bathinda','Y',3),
 (44,'44',' Mansa *','Y',3),
 (45,'45',' Sangrur','Y',3),
 (46,'46',' Patiala','Y',3),
 (47,'47',' CHANDIGARH','Y',4),
 (48,'48',' Chandigarh','Y',4),
 (49,'49',' UTTARANCHAL','Y',5),
 (50,'50',' Uttarkashi','Y',5),
 (51,'51',' Chamoli','Y',5),
 (52,'52',' Rudraprayag *','Y',5),
 (53,'53',' Tehri Garhwal','Y',5),
 (54,'54',' Dehradun','Y',5),
 (55,'55',' Garhwal','Y',5),
 (56,'56',' Pithoragarh','Y',5),
 (57,'57',' Bageshwar','Y',5),
 (58,'58',' Almora','Y',5),
 (59,'59',' Champawat','Y',5),
 (60,'60',' Nainital','Y',5),
 (61,'61',' Udham Singh Nagar *','Y',5),
 (62,'62',' Hardwar','Y',5),
 (63,'63',' HARYANA','Y',6),
 (64,'64',' Panchkula *','Y',6),
 (65,'65',' Ambala','Y',6),
 (66,'66',' Yamunanagar','Y',6),
 (67,'67',' Kurukshetra','Y',6),
 (68,'68',' Kaithal','Y',6),
 (69,'69',' Karnal','Y',6),
 (70,'70',' Panipat','Y',6),
 (71,'71',' Sonipat','Y',6),
 (72,'72',' Jind','Y',6),
 (73,'73',' Fatehabad *','Y',6),
 (74,'74',' Sirsa','Y',6),
 (75,'75',' Hisar','Y',6),
 (76,'76',' Bhiwani','Y',6),
 (77,'77',' Rohtak','Y',6),
 (78,'78',' Jhajjar *','Y',6),
 (79,'79',' Mahendragarh','Y',6),
 (80,'80',' Rewari','Y',6),
 (81,'81',' Gurgaon','Y',6),
 (82,'82',' Faridabad','Y',6),
 (83,'83',' DELHI','Y',7),
 (84,'84',' North West   *','Y',7),
 (85,'85',' North   *','Y',7),
 (86,'86',' North East   *','Y',7),
 (87,'87',' East   *','Y',7),
 (88,'88',' New Delhi','Y',7),
 (89,'89',' Central  *','Y',7),
 (90,'90',' West   *','Y',7),
 (91,'91',' South West   *','Y',7),
 (92,'92',' South  *','Y',7),
 (93,'93',' RAJASTHAN','Y',8),
 (94,'94',' Ganganagar','Y',8),
 (95,'95',' Hanumangarh *','Y',8),
 (96,'96',' Bikaner','Y',8),
 (97,'97',' Churu','Y',8),
 (98,'98',' Jhunjhunun','Y',8),
 (99,'99',' Alwar','Y',8),
 (100,'100',' Bharatpur','Y',8),
 (101,'101',' Dhaulpur','Y',8),
 (102,'102',' Karauli *','Y',8),
 (103,'103',' Sawai Madhopur','Y',8),
 (104,'104',' Dausa *','Y',8),
 (105,'105',' Jaipur','Y',8),
 (106,'106',' Sikar','Y',8),
 (107,'107',' Nagaur','Y',8),
 (108,'108',' Jodhpur','Y',8),
 (109,'109',' Jaisalmer','Y',8),
 (110,'110',' Barmer','Y',8),
 (111,'111',' Jalor','Y',8),
 (112,'112',' Sirohi','Y',8),
 (113,'113',' Pali','Y',8),
 (114,'114',' Ajmer','Y',8),
 (115,'115',' Tonk','Y',8),
 (116,'116',' Bundi','Y',8),
 (117,'117',' Bhilwara','Y',8),
 (118,'118',' Rajsamand *','Y',8),
 (119,'119',' Udaipur','Y',8),
 (120,'120',' Dungarpur','Y',8),
 (121,'121',' Banswara','Y',8),
 (122,'122',' Chittaurgarh','Y',8),
 (123,'123',' Kota','Y',8),
 (124,'124',' Baran *','Y',8),
 (125,'125',' Jhalawar','Y',8),
 (126,'126',' UTTAR PRADESH','Y',9),
 (127,'127',' Saharanpur','Y',9),
 (128,'128',' Muzaffarnagar','Y',9),
 (129,'129',' Bijnor','Y',9),
 (130,'130',' Moradabad','Y',9),
 (131,'131',' Rampur','Y',9),
 (132,'132',' Jyotiba Phule Nagar *','Y',9),
 (133,'133',' Meerut','Y',9),
 (134,'134',' Baghpat *','Y',9),
 (135,'135',' Ghaziabad','Y',9),
 (136,'136',' Gautam Buddha Nagar *','Y',9),
 (137,'137',' Bulandshahr','Y',9),
 (138,'138',' Aligarh','Y',9),
 (139,'139',' Hathras *','Y',9),
 (140,'140',' Mathura','Y',9),
 (141,'141',' Agra','Y',9),
 (142,'142',' Firozabad','Y',9),
 (143,'143',' Etah','Y',9),
 (144,'144',' Mainpuri','Y',9),
 (145,'145',' Budaun','Y',9),
 (146,'146',' Bareilly','Y',9),
 (147,'147',' Pilibhit','Y',9),
 (148,'148',' Shahjahanpur','Y',9),
 (149,'149',' Kheri','Y',9),
 (150,'150',' Sitapur','Y',9),
 (151,'151',' Hardoi','Y',9),
 (152,'152',' Unnao','Y',9),
 (153,'153',' Lucknow','Y',9),
 (154,'154',' Rae Bareli','Y',9),
 (155,'155',' Farrukhabad','Y',9),
 (156,'156',' Kannauj *','Y',9),
 (157,'157',' Etawah','Y',9),
 (158,'158',' Auraiya *','Y',9),
 (159,'159',' Kanpur Dehat','Y',9),
 (160,'160',' Kanpur Nagar','Y',9),
 (161,'161',' Jalaun','Y',9),
 (162,'162',' Jhansi','Y',9),
 (163,'163',' Lalitpur','Y',9),
 (164,'164',' Hamirpur','Y',9),
 (165,'165',' Mahoba *','Y',9),
 (166,'166',' Banda','Y',9),
 (167,'167',' Chitrakoot *','Y',9),
 (168,'168',' Fatehpur','Y',9),
 (169,'169',' Pratapgarh','Y',9),
 (170,'170',' Kaushambi *','Y',9),
 (171,'171',' Allahabad','Y',9),
 (172,'172',' Barabanki','Y',9),
 (173,'173',' Faizabad','Y',9),
 (174,'174',' Ambedkar Nagar *','Y',9),
 (175,'175',' Sultanpur','Y',9),
 (176,'176',' Bahraich','Y',9),
 (177,'177',' Shrawasti *','Y',9),
 (178,'178',' Balrampur *','Y',9),
 (179,'179',' Gonda','Y',9),
 (180,'180',' Siddharthnagar','Y',9),
 (181,'181',' Basti','Y',9),
 (182,'182',' Sant Kabir Nagar *','Y',9),
 (183,'183',' Maharajganj','Y',9),
 (184,'184',' Gorakhpur','Y',9),
 (185,'185',' Kushinagar *','Y',9),
 (186,'186',' Deoria','Y',9),
 (187,'187',' Azamgarh','Y',9),
 (188,'188',' Mau','Y',9),
 (189,'189',' Ballia','Y',9),
 (190,'190',' Jaunpur','Y',9),
 (191,'191',' Ghazipur','Y',9),
 (192,'192',' Chandauli *','Y',9),
 (193,'193',' Varanasi','Y',9),
 (194,'194',' Sant Ravidas Nagar *','Y',9),
 (195,'195',' Mirzapur','Y',9),
 (196,'196',' Sonbhadra','Y',9),
 (197,'197',' BIHAR','Y',10),
 (198,'198',' Pashchim Champaran','Y',10),
 (199,'199',' Purba Champaran','Y',10),
 (200,'200',' Sheohar *','Y',10),
 (201,'201',' Sitamarhi','Y',10),
 (202,'202',' Madhubani','Y',10),
 (203,'203',' Supaul *','Y',10),
 (204,'204',' Araria','Y',10),
 (205,'205',' Kishanganj','Y',10),
 (206,'206',' Purnia','Y',10),
 (207,'207',' Katihar','Y',10),
 (208,'208',' Madhepura','Y',10),
 (209,'209',' Saharsa','Y',10),
 (210,'210',' Darbhanga','Y',10),
 (211,'211',' Muzaffarpur','Y',10),
 (212,'212',' Gopalganj','Y',10),
 (213,'213',' Siwan','Y',10),
 (214,'214',' Saran','Y',10),
 (215,'215',' Vaishali','Y',10),
 (216,'216',' Samastipur','Y',10),
 (217,'217',' Begusarai','Y',10),
 (218,'218',' Khagaria','Y',10),
 (219,'219',' Bhagalpur','Y',10),
 (220,'220',' Banka *','Y',10),
 (221,'221',' Munger','Y',10),
 (222,'222',' Lakhisarai *','Y',10),
 (223,'223',' Sheikhpura *','Y',10),
 (224,'224',' Nalanda','Y',10),
 (225,'225',' Patna','Y',10),
 (226,'226',' Bhojpur','Y',10),
 (227,'227',' Buxar *','Y',10),
 (228,'228',' Kaimur (Bhabua) *','Y',10),
 (229,'229',' Rohtas','Y',10),
 (230,'230',' Jehanabad','Y',10),
 (231,'231',' Aurangabad','Y',10),
 (232,'232',' Gaya','Y',10),
 (233,'233',' Nawada','Y',10),
 (234,'234',' Jamui *','Y',10),
 (235,'235',' SIKKIM','Y',11),
 (236,'236',' North','Y',11),
 (237,'237',' West','Y',11),
 (238,'238',' South','Y',11),
 (239,'239',' East','Y',11),
 (240,'240',' ARUNACHAL PRADESH','Y',12),
 (241,'241',' Tawang','Y',12),
 (242,'242',' West Kameng','Y',12),
 (243,'243',' East Kameng','Y',12),
 (244,'244',' Papum Pare *','Y',12),
 (245,'245',' Lower Subansiri','Y',12),
 (246,'246',' Upper Subansiri','Y',12),
 (247,'247',' West Siang','Y',12),
 (248,'248',' East Siang','Y',12),
 (249,'249',' Upper Siang *','Y',12),
 (250,'250',' Dibang Valley','Y',12),
 (251,'251',' Lohit','Y',12),
 (252,'252',' Changlang','Y',12),
 (253,'253',' Tirap','Y',12),
 (254,'254',' NAGALAND','Y',13),
 (255,'255',' Mon','Y',13),
 (256,'256',' Tuensang','Y',13),
 (257,'257',' Mokokchung','Y',13),
 (258,'258',' Zunheboto','Y',13),
 (259,'259',' Wokha','Y',13),
 (260,'260',' Dimapur *','Y',13),
 (261,'261',' Kohima','Y',13),
 (262,'262',' Phek','Y',13),
 (263,'263',' MANIPUR','Y',14),
 (264,'264',' Senapati','Y',14),
 (265,'265',' Tamenglong','Y',14),
 (266,'266',' Churachandpur','Y',14),
 (267,'267',' Bishnupur','Y',14),
 (268,'268',' Thoubal','Y',14),
 (269,'269',' Imphal West','Y',14),
 (270,'270',' Imphal East *','Y',14),
 (271,'271',' Ukhrul','Y',14),
 (272,'272',' Chandel','Y',14),
 (273,'273',' MIZORAM','Y',15),
 (274,'274',' Mamit *','Y',15),
 (275,'275',' Kolasib *','Y',15),
 (276,'276',' Aizawl','Y',15),
 (277,'277',' Champhai *','Y',15),
 (278,'278',' Serchhip *','Y',15),
 (279,'279',' Lunglei','Y',15),
 (280,'280',' Lawngtlai','Y',15),
 (281,'281',' Saiha *','Y',15),
 (282,'282',' TRIPURA','Y',16),
 (283,'283',' West Tripura','Y',16),
 (284,'284',' South Tripura','Y',16),
 (285,'285',' Dhalai  *','Y',16),
 (286,'286',' North Tripura','Y',16),
 (287,'287',' MEGHALAYA','Y',17),
 (288,'288',' West Garo Hills','Y',17),
 (289,'289',' East Garo Hills','Y',17),
 (290,'290',' South Garo Hills *','Y',17),
 (291,'291',' West Khasi Hills','Y',17),
 (292,'292',' Ri Bhoi  *','Y',17),
 (293,'293',' East Khasi Hills','Y',17),
 (294,'294',' Jaintia Hills','Y',17),
 (295,'295',' ASSAM','Y',18),
 (296,'296',' Kokrajhar','Y',18),
 (297,'297',' Dhubri','Y',18),
 (298,'298',' Goalpara','Y',18),
 (299,'299',' Bongaigaon','Y',18),
 (300,'300',' Barpeta','Y',18),
 (301,'301',' Kamrup','Y',18),
 (302,'302',' Nalbari','Y',18),
 (303,'303',' Darrang','Y',18),
 (304,'304',' Marigaon','Y',18),
 (305,'305',' Nagaon','Y',18),
 (306,'306',' Sonitpur','Y',18),
 (307,'307',' Lakhimpur','Y',18),
 (308,'308',' Dhemaji','Y',18),
 (309,'309',' Tinsukia','Y',18),
 (310,'310',' Dibrugarh','Y',18),
 (311,'311',' Sibsagar','Y',18),
 (312,'312',' Jorhat','Y',18),
 (313,'313',' Golaghat','Y',18),
 (314,'314',' Karbi Anglong','Y',18),
 (315,'315',' North Cachar Hills','Y',18),
 (316,'316',' Cachar','Y',18),
 (317,'317',' Karimganj','Y',18),
 (318,'318',' Hailakandi','Y',18),
 (319,'319',' WEST BENGAL','Y',19),
 (320,'320',' Darjiling','Y',19),
 (321,'321',' Jalpaiguri','Y',19),
 (322,'322',' Koch Bihar','Y',19),
 (323,'323',' Uttar Dinajpur','Y',19),
 (324,'324',' Dakshin Dinajpur *','Y',19),
 (325,'325',' Maldah','Y',19),
 (326,'326',' Murshidabad','Y',19),
 (327,'327',' Birbhum','Y',19),
 (328,'328',' Barddhaman','Y',19),
 (329,'329',' Nadia','Y',19),
 (330,'330',' North Twenty Four Parganas','Y',19),
 (331,'331',' Hugli','Y',19),
 (332,'332',' Bankura','Y',19),
 (333,'333',' Puruliya','Y',19),
 (334,'334',' Medinipur','Y',19),
 (335,'335',' Haora','Y',19),
 (336,'336',' Kolkata','Y',19),
 (337,'337',' South  Twenty Four Parganas','Y',19),
 (338,'338',' JHARKHAND','Y',20),
 (339,'339',' Garhwa *','Y',20),
 (340,'340',' Palamu','Y',20),
 (341,'341',' Chatra *','Y',20),
 (342,'342',' Hazaribag','Y',20),
 (343,'343',' Kodarma *','Y',20),
 (344,'344',' Giridih','Y',20),
 (345,'345',' Deoghar','Y',20),
 (346,'346',' Godda','Y',20),
 (347,'347',' Sahibganj','Y',20),
 (348,'348',' Pakaur *','Y',20),
 (349,'349',' Dumka','Y',20),
 (350,'350',' Dhanbad','Y',20),
 (351,'351',' Bokaro *','Y',20),
 (352,'352',' Ranchi','Y',20),
 (353,'353',' Lohardaga','Y',20),
 (354,'354',' Gumla','Y',20),
 (355,'355',' Pashchimi Singhbhum','Y',20),
 (356,'356',' Purbi Singhbhum','Y',20),
 (357,'357',' ORISSA','Y',21),
 (358,'358',' Bargarh  *','Y',21),
 (359,'359',' Jharsuguda  *','Y',21),
 (360,'360',' Sambalpur','Y',21),
 (361,'361',' Debagarh  *','Y',21),
 (362,'362',' Sundargarh','Y',21),
 (363,'363',' Kendujhar','Y',21),
 (364,'364',' Mayurbhanj','Y',21),
 (365,'365',' Baleshwar','Y',21),
 (366,'366',' Bhadrak  *','Y',21),
 (367,'367',' Kendrapara *','Y',21),
 (368,'368',' Jagatsinghapur  *','Y',21),
 (369,'369',' Cuttack','Y',21),
 (370,'370',' Jajapur  *','Y',21),
 (371,'371',' Dhenkanal','Y',21),
 (372,'372',' Anugul  *','Y',21),
 (373,'373',' Nayagarh  *','Y',21),
 (374,'374',' Khordha  *','Y',21),
 (375,'375',' Puri','Y',21),
 (376,'376',' Ganjam','Y',21),
 (377,'377',' Gajapati  *','Y',21),
 (378,'378',' Kandhamal','Y',21),
 (379,'379',' Baudh  *','Y',21),
 (380,'380',' Sonapur  *','Y',21),
 (381,'381',' Balangir','Y',21),
 (382,'382',' Nuapada  *','Y',21),
 (383,'383',' Kalahandi','Y',21),
 (384,'384',' Rayagada  *','Y',21),
 (385,'385',' Nabarangapur  *','Y',21),
 (386,'386',' Koraput','Y',21),
 (387,'387',' Malkangiri  *','Y',21),
 (388,'388',' CHHATTISGARH','Y',22),
 (389,'389',' Koriya *','Y',22),
 (390,'390',' Surguja','Y',22),
 (391,'391',' Jashpur *','Y',22),
 (392,'392',' Raigarh','Y',22),
 (393,'393',' Korba *','Y',22),
 (394,'394',' Janjgir - Champa*','Y',22),
 (395,'395',' Bilaspur','Y',22),
 (396,'396',' Kawardha *','Y',22),
 (397,'397',' Rajnandgaon','Y',22),
 (398,'398',' Durg','Y',22),
 (399,'399',' Raipur','Y',22),
 (400,'400',' Mahasamund *','Y',22),
 (401,'401',' Dhamtari *','Y',22),
 (402,'402',' Kanker *','Y',22),
 (403,'403',' Baster','Y',22),
 (404,'404',' Dantewada*','Y',22),
 (405,'405',' MADHYA PRADESH','Y',23),
 (406,'406',' Sheopur *','Y',23),
 (407,'407',' Morena','Y',23),
 (408,'408',' Bhind','Y',23),
 (409,'409',' Gwalior','Y',23),
 (410,'410',' Datia','Y',23),
 (411,'411',' Shivpuri','Y',23),
 (412,'412',' Guna','Y',23),
 (413,'413',' Tikamgarh','Y',23),
 (414,'414',' Chhatarpur','Y',23),
 (415,'415',' Panna','Y',23),
 (416,'416',' Sagar','Y',23),
 (417,'417',' Damoh','Y',23),
 (418,'418',' Satna','Y',23),
 (419,'419',' Rewa','Y',23),
 (420,'420',' Umaria *','Y',23),
 (421,'421',' Shahdol','Y',23),
 (422,'422',' Sidhi','Y',23),
 (423,'423',' Neemuch *','Y',23),
 (424,'424',' Mandsaur','Y',23),
 (425,'425',' Ratlam','Y',23),
 (426,'426',' Ujjain','Y',23),
 (427,'427',' Shajapur','Y',23),
 (428,'428',' Dewas','Y',23),
 (429,'429',' Jhabua','Y',23),
 (430,'430',' Dhar','Y',23),
 (431,'431',' Indore','Y',23),
 (432,'432',' West Nimar','Y',23),
 (433,'433',' Barwani *','Y',23),
 (434,'434',' East Nimar','Y',23),
 (435,'435',' Rajgarh','Y',23),
 (436,'436',' Vidisha','Y',23),
 (437,'437',' Bhopal','Y',23),
 (438,'438',' Sehore','Y',23),
 (439,'439',' Raisen','Y',23),
 (440,'440',' Betul','Y',23),
 (441,'441',' Harda *','Y',23),
 (442,'442',' Hoshangabad','Y',23),
 (443,'443',' Katni *','Y',23),
 (444,'444',' Jabalpur','Y',23),
 (445,'445',' Narsimhapur','Y',23),
 (446,'446',' Dindori *','Y',23),
 (447,'447',' Mandla','Y',23),
 (448,'448',' Chhindwara','Y',23),
 (449,'449',' Seoni','Y',23),
 (450,'450',' Balaghat','Y',23),
 (451,'451',' GUJARAT','Y',24),
 (452,'452',' Kachchh','Y',24),
 (453,'453',' Banas Kantha','Y',24),
 (454,'454',' Patan  *','Y',24),
 (455,'455',' Mahesana','Y',24),
 (456,'456',' Sabar Kantha','Y',24),
 (457,'457',' Gandhinagar','Y',24),
 (458,'458',' Ahmadabad','Y',24),
 (459,'459',' Surendranagar','Y',24),
 (460,'460',' Rajkot','Y',24),
 (461,'461',' Jamnagar','Y',24),
 (462,'462',' Porbandar  *','Y',24),
 (463,'463',' Junagadh','Y',24),
 (464,'464',' Amreli','Y',24),
 (465,'465',' Bhavnagar','Y',24),
 (466,'466',' Anand  *','Y',24),
 (467,'467',' Kheda','Y',24),
 (468,'468',' Panch Mahals','Y',24),
 (469,'469',' Dohad  *','Y',24),
 (470,'470',' Vadodara','Y',24),
 (471,'471',' Narmada  *','Y',24),
 (472,'472',' Bharuch','Y',24),
 (473,'473',' Surat','Y',24),
 (474,'474',' The Dangs','Y',24),
 (475,'475',' Navsari  *','Y',24),
 (476,'476',' Valsad','Y',24),
 (477,'477',' DAMAN & DIU','Y',25),
 (478,'478',' Diu','Y',25),
 (479,'479',' Daman','Y',25),
 (480,'480',' DADRA & NAGAR HAVELI','Y',26),
 (481,'481',' Dadra & Nagar Haveli','Y',26),
 (483,'483',' Nandurbar *','Y',27),
 (484,'484',' Dhule','Y',27),
 (485,'485',' Jalgaon','Y',27),
 (486,'486',' Buldana','Y',27),
 (487,'487',' Akola','Y',27),
 (488,'488',' Washim *','Y',27),
 (489,'489',' Amravati','Y',27),
 (490,'490',' Wardha','Y',27),
 (491,'491',' Nagpur','Y',27),
 (492,'492',' Bhandara','Y',27),
 (493,'493',' Gondiya *','Y',27),
 (494,'494',' Gadchiroli','Y',27),
 (495,'495',' Chandrapur','Y',27),
 (496,'496',' Yavatmal','Y',27),
 (497,'497',' Nanded','Y',27),
 (498,'498',' Hingoli *','Y',27),
 (499,'499',' Parbhani','Y',27),
 (500,'500',' Jalna','Y',27),
 (501,'501',' Aurangabad','Y',27),
 (502,'502',' Nashik','Y',27),
 (503,'503',' Thane','Y',27),
 (504,'504',' Mumbai (Suburban) *','Y',27),
 (505,'505',' Mumbai','Y',27),
 (506,'506',' Raigarh','Y',27),
 (507,'507',' Pune','Y',27),
 (508,'508',' Ahmadnagar','Y',27),
 (509,'509',' Bid','Y',27),
 (510,'510',' Latur','Y',27),
 (511,'511',' Osmanabad','Y',27),
 (512,'512',' Solapur','Y',27),
 (513,'513',' Satara','Y',27),
 (514,'514',' Ratnagiri','Y',27),
 (515,'515',' Sindhudurg','Y',27),
 (516,'516',' Kolhapur','Y',27),
 (517,'517',' Sangli','Y',27),
 (518,'518',' ANDHRA PRADESH','Y',28),
 (519,'519',' Adilabad','Y',28),
 (520,'520',' Nizamabad','Y',28),
 (521,'521',' Karimnagar','Y',28),
 (522,'522',' Medak','Y',28),
 (523,'523',' Hyderabad','Y',28),
 (524,'524',' Rangareddi','Y',28),
 (525,'525',' Mahbubnagar','Y',28),
 (526,'526',' Nalgonda','Y',28),
 (527,'527',' Warangal','Y',28),
 (528,'528',' Khammam','Y',28),
 (529,'529',' Srikakulam','Y',28),
 (530,'530',' Vizianagaram','Y',28),
 (531,'531',' Visakhapatnam','Y',28),
 (532,'532',' East Godavari','Y',28),
 (533,'533',' West Godavari','Y',28),
 (534,'534',' Krishna','Y',28),
 (535,'535',' Guntur','Y',28),
 (536,'536',' Prakasam','Y',28),
 (537,'537',' Nellore','Y',28),
 (538,'538',' Cuddapah','Y',28),
 (539,'539',' Kurnool','Y',28),
 (540,'540',' Anantapur','Y',28),
 (541,'541',' Chittoor','Y',28),
 (542,'542',' KARNATAKA','Y',29),
 (543,'543',' Belgaum','Y',29),
 (544,'544',' Bagalkot *','Y',29),
 (545,'545',' Bijapur','Y',29),
 (546,'546',' Gulbarga','Y',29),
 (547,'547',' Bidar','Y',29),
 (548,'548',' Raichur','Y',29),
 (549,'549',' Koppal *','Y',29),
 (550,'550',' Gadag *','Y',29),
 (551,'551',' Dharwad','Y',29),
 (552,'552',' Uttara Kannada','Y',29),
 (553,'553',' Haveri *','Y',29),
 (554,'554',' Bellary','Y',29),
 (555,'555',' Chitradurga','Y',29),
 (556,'556',' Davangere*','Y',29),
 (557,'557',' Shimoga','Y',29),
 (558,'558',' Udupi *','Y',29),
 (559,'559',' Chikmagalur','Y',29),
 (560,'560',' Tumkur','Y',29),
 (561,'561',' Kolar','Y',29),
 (562,'562',' Bangalore','Y',29),
 (563,'563',' Bangalore Rural','Y',29),
 (564,'564',' Mandya','Y',29),
 (565,'565',' Hassan','Y',29),
 (566,'566',' Dakshina Kannada','Y',29),
 (567,'567',' Kodagu','Y',29),
 (568,'568',' Mysore','Y',29),
 (569,'569',' Chamrajnagar*','Y',29),
 (570,'570',' GOA','Y',30),
 (571,'571',' North Goa','Y',30),
 (572,'572',' South Goa','Y',30),
 (573,'573',' LAKSHADWEEP','Y',31),
 (574,'574',' Lakshadweep','Y',31),
 (575,'575',' KERALA','Y',32),
 (576,'576',' Kasaragod','Y',32),
 (577,'577',' Kannur','Y',32),
 (578,'578',' Wayanad','Y',32),
 (579,'579',' Kozhikode','Y',32),
 (580,'580',' Malappuram','Y',32),
 (581,'581',' Palakkad','Y',32),
 (582,'582',' Thrissur','Y',32),
 (583,'583',' Ernakulam','Y',32),
 (584,'584',' Idukki','Y',32),
 (585,'585',' Kottayam','Y',32),
 (586,'586',' Alappuzha','Y',32),
 (587,'587',' Pathanamthitta','Y',32),
 (588,'588',' Kollam','Y',32),
 (589,'589',' Thiruvananthapuram','Y',32),
 (590,'590',' TAMIL NADU','Y',33),
 (591,'591',' Thiruvallur','Y',33),
 (592,'592',' Chennai','Y',33),
 (593,'593',' Kancheepuram','Y',33),
 (594,'594',' Vellore','Y',33),
 (595,'595',' Dharmapuri','Y',33),
 (596,'596',' Tiruvannamalai','Y',33),
 (597,'597',' Viluppuram','Y',33),
 (598,'598',' Salem','Y',33),
 (599,'599',' Namakkal   *','Y',33),
 (600,'600',' Erode','Y',33),
 (601,'601',' The Nilgiris','Y',33),
 (602,'602',' Coimbatore','Y',33),
 (603,'603',' Dindigul','Y',33),
 (604,'604',' Karur  *','Y',33),
 (605,'605',' Tiruchirappalli','Y',33),
 (606,'606',' Perambalur  *','Y',33),
 (607,'607',' Ariyalur  *','Y',33),
 (608,'608',' Cuddalore','Y',33),
 (609,'609',' Nagapattinam  *','Y',33),
 (610,'610',' Thiruvarur','Y',33),
 (611,'611',' Thanjavur','Y',33),
 (612,'612',' Pudukkottai','Y',33),
 (613,'613',' Sivaganga','Y',33),
 (614,'614',' Madurai','Y',33),
 (615,'615',' Theni  *','Y',33),
 (616,'616',' Virudhunagar','Y',33),
 (617,'617',' Ramanathapuram','Y',33),
 (618,'618',' Thoothukkudi','Y',33),
 (619,'619',' Tirunelveli','Y',33),
 (620,'620',' Kanniyakumari','Y',33),
 (621,'621',' PONDICHERRY','Y',34),
 (622,'622',' Yanam','Y',34),
 (623,'623',' Pondicherry','Y',34),
 (624,'624',' Mahe','Y',34),
 (625,'625',' Karaikal','Y',34),
 (626,'626',' ANDAMAN & NICOBAR ISLANDS','Y',35),
 (627,'627',' Andamans','Y',35),
 (628,'628',' Nicobars','Y',35);
/*!40000 ALTER TABLE `district` ENABLE KEYS */;






/*!40000 ALTER TABLE `taluka` DISABLE KEYS */;
INSERT INTO `taluka` (`id`,`code`,`name`,`active`,`district_id`) VALUES 
 (1,'1',' JAMMU & KASHMIR','Y',1),
 (2,'2',' Kupwara','Y',2),
 (3,'3',' Kupwara','Y',2),
 (4,'4',' Handwara','Y',2),
 (5,'5',' Karnah','Y',2),
 (6,'6',' Baramula','Y',3),
 (7,'7',' Gurez','Y',3),
 (8,'8',' Bandipore','Y',3),
 (9,'9',' Sonawari','Y',3),
 (10,'10',' Sopore','Y',3),
 (11,'11',' Pattan','Y',3),
 (12,'12',' Baramula','Y',3),
 (13,'13',' Uri','Y',3),
 (14,'14',' Gulmarg','Y',3),
 (15,'15',' Srinagar','Y',4),
 (16,'16',' Kangan','Y',4),
 (17,'17',' Ganderbal','Y',4),
 (18,'18',' Srinagar','Y',4),
 (19,'19',' Badgam','Y',5),
 (20,'20',' Beerwah','Y',5),
 (21,'21',' Badgam','Y',5),
 (22,'22',' Chadura','Y',5),
 (23,'23',' Pulwama','Y',6),
 (24,'24',' Pampore','Y',6),
 (25,'25',' Tral','Y',6),
 (26,'26',' Pulwama','Y',6),
 (27,'27',' Shupiyan','Y',6),
 (28,'28',' Anantnag','Y',7),
 (29,'29',' Pahalgam','Y',7),
 (30,'30',' Bijbehara','Y',7),
 (31,'31',' Anantnag','Y',7),
 (32,'32',' Kulgam','Y',7),
 (33,'33',' Duru','Y',7),
 (34,'34',' Leh (Ladakh)','Y',8),
 (35,'35',' Leh','Y',8),
 (36,'36',' Kargil','Y',9),
 (37,'37',' Kargil','Y',9),
 (38,'38',' Zanskar','Y',9),
 (39,'39',' Doda','Y',10),
 (40,'40',' Banihal','Y',10),
 (41,'41',' Ramban','Y',10),
 (42,'42',' Doda','Y',10),
 (43,'43',' Kishtwar','Y',10),
 (44,'44',' Thathri','Y',10),
 (45,'45',' Bhalessa (Gandoh)','Y',10),
 (46,'46',' Bhaderwah','Y',10),
 (47,'47',' Udhampur','Y',11),
 (48,'48',' Gool Gulab Garh','Y',11),
 (49,'49',' Reasi','Y',11),
 (50,'50',' Udhampur','Y',11),
 (51,'51',' Chenani','Y',11),
 (52,'52',' Ramnagar','Y',11),
 (53,'53',' Punch','Y',12),
 (54,'54',' Haveli','Y',12),
 (55,'55',' Mendhar','Y',12),
 (56,'56',' Surankote','Y',12),
 (57,'57',' Rajauri','Y',13),
 (58,'58',' Thanamandi','Y',13),
 (59,'59',' Rajauri','Y',13),
 (60,'60',' Budhal','Y',13),
 (61,'61',' Kalakote','Y',13),
 (62,'62',' Nowshehra ','Y',13),
 (63,'63',' Sunderbani  ','Y',13),
 (64,'64',' Jammu','Y',14),
 (65,'65',' Akhnoor','Y',14),
 (66,'66',' Jammu','Y',14),
 (67,'67',' Ranbirsinghpora','Y',14),
 (68,'68',' Bishna','Y',14),
 (69,'69',' Samba','Y',14),
 (70,'70',' Kathua','Y',15),
 (71,'71',' Billawar','Y',15),
 (72,'72',' Bashohli','Y',15),
 (73,'73',' Kathua','Y',15),
 (74,'74',' Hiranagar','Y',15),
 (75,'75',' HIMACHAL PRADESH','Y',16),
 (76,'76',' Chamba','Y',17),
 (77,'77',' Pangi(T)','Y',17),
 (78,'78',' Chaurah(T)','Y',17),
 (79,'79',' Saluni(T)','Y',17),
 (80,'80',' Bhalai(S.T)','Y',17),
 (81,'81',' Dalhousie(T)','Y',17),
 (82,'82',' Bhattiyat(T)','Y',17),
 (83,'83',' Sihunta(S.T)','Y',17),
 (84,'84',' Chamba(T)','Y',17),
 (85,'85',' Holi(S.T)','Y',17),
 (86,'86',' Brahmaur(T)','Y',17),
 (87,'87',' Kangra','Y',18),
 (88,'88',' Nurpur(T)','Y',18),
 (89,'89',' Indora(T)','Y',18),
 (90,'90',' Fatehpur(T)','Y',18),
 (91,'91',' Jawali(T)','Y',18),
 (92,'92',' Harchakian(S.T)','Y',18),
 (93,'93',' Shahpur(T)','Y',18),
 (94,'94',' Dharmsala(T)','Y',18),
 (95,'95',' Kangra(T)','Y',18),
 (96,'96',' Baroh(T)','Y',18),
 (97,'97',' Dera Gopipur(T)','Y',18),
 (98,'98',' Jaswan(T)','Y',18),
 (99,'99',' Rakkar(S.T)','Y',18),
 (100,'100',' Khundian(T)','Y',18),
 (101,'101',' Thural(S.T)','Y',18),
 (102,'102',' Dhira(S.T)','Y',18),
 (103,'103',' Jai Singhpur(T)','Y',18),
 (104,'104',' Palampur(T)','Y',18),
 (105,'105',' Baijnath(T)','Y',18),
 (106,'106',' Multhan(S.T)','Y',18),
 (107,'107',' Lahul & Spiti','Y',19),
 (108,'108',' Udaipur (S.T)','Y',19),
 (109,'109',' Lahul     (T)','Y',19),
 (110,'110',' Spiti       (T)','Y',19),
 (111,'111',' Kullu','Y',20),
 (112,'112',' Manali(T)','Y',20),
 (113,'113',' Kullu(T)','Y',20),
 (114,'114',' Sainj(S.T)','Y',20),
 (115,'115',' Banjar(T)','Y',20),
 (116,'116',' Ani(S.T)','Y',20),
 (117,'117',' Nermand(T)','Y',20),
 (118,'118',' Mandi','Y',21),
 (119,'119',' Padhar(T)','Y',21),
 (120,'120',' Jogindarnagar(T)','Y',21),
 (121,'121',' Lad Bharol(T)','Y',21),
 (122,'122',' Sandhol(S.T)','Y',21),
 (123,'123',' Dharmpur(S.T)','Y',21),
 (124,'124',' Kotli(S.T)','Y',21),
 (125,'125',' Sarkaghat(T)','Y',21),
 (126,'126',' Baldwara(S.T)','Y',21),
 (127,'127',' Sundarnagar(T)','Y',21),
 (128,'128',' Mandi(T)','Y',21),
 (129,'129',' Aut(S.T)','Y',21),
 (130,'130',' Bali Chowki(S.T)','Y',21),
 (131,'131',' Thunag(T)','Y',21),
 (132,'132',' Chachyot(T)','Y',21),
 (133,'133',' Nihri(S.T)','Y',21),
 (134,'134',' Karsog(T)','Y',21),
 (135,'135',' Hamirpur','Y',22),
 (136,'136',' Tira Sujanpur(T)','Y',22),
 (137,'137',' Nadaun(T)','Y',22),
 (138,'138',' Hamirpur(T)','Y',22),
 (139,'139',' Barsar(T)','Y',22),
 (140,'140',' Dhatwal(ST)','Y',22),
 (141,'141',' Bhoranj(T)','Y',22),
 (142,'142',' Una','Y',23),
 (143,'143',' Bharwain(S.T)','Y',23),
 (144,'144',' Amb(T)','Y',23),
 (145,'145',' Bangana(T)','Y',23),
 (146,'146',' Una(T)','Y',23),
 (147,'147',' Haroli(S.T)','Y',23),
 (148,'148',' Bilaspur','Y',24),
 (149,'149',' Ghumarwin(T)','Y',24),
 (150,'150',' Jhanduta(T)','Y',24),
 (151,'151',' Naina Devi(S.T)','Y',24),
 (152,'152',' Bilaspur Sadar(T)','Y',24),
 (153,'153',' Solan','Y',25),
 (154,'154',' Arki(T)','Y',25),
 (155,'155',' Ramshahr(S.T)','Y',25),
 (156,'156',' Nalagarh(T)','Y',25),
 (157,'157',' Krishangarh(S.T)','Y',25),
 (158,'158',' Kasauli(T)','Y',25),
 (159,'159',' Solan(T)','Y',25),
 (160,'160',' Kandaghat(T)','Y',25),
 (161,'161',' Sirmaur','Y',26),
 (162,'162',' Rajgarh(T)','Y',26),
 (163,'163',' Nohra(S.T)','Y',26),
 (164,'164',' Pachhad(T)','Y',26),
 (165,'165',' Renuka(T)','Y',26),
 (166,'166',' Dadahu(S.T)','Y',26),
 (167,'167',' Nahan(T)','Y',26),
 (168,'168',' Paonta Sahib(T)','Y',26),
 (169,'169',' Kamrau(S.T)','Y',26),
 (170,'170',' Shalai(T)','Y',26),
 (171,'171',' Ronhat(S.T)','Y',26),
 (172,'172',' Shimla','Y',27),
 (173,'173',' Rampur(T)','Y',27),
 (174,'174',' Nankhari(S.T)','Y',27),
 (175,'175',' Kumharsain(T)','Y',27),
 (176,'176',' Seoni(T)','Y',27),
 (177,'177',' Shimla Rural(T)','Y',27),
 (178,'178',' Shimla Urban(T)','Y',27),
 (179,'179',' Junga(S.T)','Y',27),
 (180,'180',' Theog(T)','Y',27),
 (181,'181',' Chaupal(T)','Y',27),
 (182,'182',' Cheta(S.T)','Y',27),
 (183,'183',' Nerua(S.T)','Y',27),
 (184,'184',' Jubbal(T)','Y',27),
 (185,'185',' Kotkhai(T)','Y',27),
 (186,'186',' Tikar(S.T)','Y',27),
 (187,'187',' Rohru(T)','Y',27),
 (188,'188',' Chirgaon(T)','Y',27),
 (189,'189',' Dodra Kwar(T)','Y',27),
 (190,'190',' Kinnaur','Y',28),
 (191,'191',' Hangrang(S.T)','Y',28),
 (192,'192',' Poo(T)','Y',28),
 (193,'193',' Morang(T)','Y',28),
 (194,'194',' Kalpa(T)','Y',28),
 (195,'195',' Nichar(T)','Y',28),
 (196,'196',' Sangla(T)','Y',28),
 (197,'197',' PUNJAB','Y',29),
 (198,'198',' Gurdaspur','Y',30),
 (199,'199',' Dhar Kalan','Y',30),
 (200,'200',' Pathankot','Y',30),
 (201,'201',' Gurdaspur','Y',30),
 (202,'202',' Batala','Y',30),
 (203,'203',' Dera Baba Nanak','Y',30),
 (204,'204',' Amritsar','Y',31),
 (205,'205',' Ajnala','Y',31),
 (206,'206',' Amritsar -I','Y',31),
 (207,'207',' Amritsar- II','Y',31),
 (208,'208',' Tarn-Taran','Y',31),
 (209,'209',' Patti','Y',31),
 (210,'210',' Khadur Sahib','Y',31),
 (211,'211',' Baba Bakala','Y',31),
 (212,'212',' Kapurthala','Y',32),
 (213,'213',' Bhulath','Y',32),
 (214,'214',' Kapurthala','Y',32),
 (215,'215',' Sultanpur Lodhi','Y',32),
 (216,'216',' Phagwara','Y',32),
 (217,'217',' Jalandhar','Y',33),
 (218,'218',' Shahkot','Y',33),
 (219,'219',' Nakodar','Y',33),
 (220,'220',' Phillaur','Y',33),
 (221,'221',' Jalandhar - I','Y',33),
 (222,'222',' Jalandhar -II','Y',33),
 (223,'223',' Hoshiarpur','Y',34),
 (224,'224',' Dasua','Y',34),
 (225,'225',' Mukerian','Y',34),
 (226,'226',' Hoshiarpur','Y',34),
 (227,'227',' Garhshankar','Y',34),
 (228,'228',' Nawanshahr *','Y',35),
 (229,'229',' Nawanshahr','Y',35),
 (230,'230',' Balachaur','Y',35),
 (231,'231',' Rupnagar','Y',36),
 (232,'232',' Anandpur Sahib','Y',36),
 (233,'233',' Rupnagar','Y',36),
 (234,'234',' Kharar','Y',36),
 (235,'235',' S.A.S.Nagar (Mohali)','Y',36),
 (236,'236',' Fatehgarh Sahib *','Y',37),
 (237,'237',' Bassi Pathana','Y',37),
 (238,'238',' Fatehgarh Sahib','Y',37),
 (239,'239',' Amloh','Y',37),
 (240,'240',' Khamanon','Y',37),
 (241,'241',' Ludhiana','Y',38),
 (242,'242',' Samrala','Y',38),
 (243,'243',' Khanna','Y',38),
 (244,'244',' Payal','Y',38),
 (245,'245',' Ludhiana (East)','Y',38),
 (246,'246',' Ludhiana (West)','Y',38),
 (247,'247',' Raikot','Y',38),
 (248,'248',' Jagraon','Y',38),
 (249,'249',' Moga *','Y',39),
 (250,'250',' Nihal Singhwala','Y',39),
 (251,'251',' Bagha Purana','Y',39),
 (252,'252',' Moga','Y',39),
 (253,'253',' Firozpur','Y',40),
 (254,'254',' Zira','Y',40),
 (255,'255',' Firozepur','Y',40),
 (256,'256',' Jalalabad','Y',40),
 (257,'257',' Fazilka','Y',40),
 (258,'258',' Abohar','Y',40),
 (259,'259',' Muktsar *','Y',41),
 (260,'260',' Malout','Y',41),
 (261,'261',' Giddarbaha','Y',41),
 (262,'262',' Muktsar','Y',41),
 (263,'263',' Faridkot','Y',42),
 (264,'264',' Faridkot','Y',42),
 (265,'265',' Jaitu','Y',42),
 (266,'266',' Bathinda','Y',43),
 (267,'267',' Rampura Phul','Y',43),
 (268,'268',' Bathinda','Y',43),
 (269,'269',' Talwandi Sabo','Y',43),
 (270,'270',' Mansa *','Y',44),
 (271,'271',' Sardulgarh','Y',44),
 (272,'272',' Budhlada','Y',44),
 (273,'273',' Mansa','Y',44),
 (274,'274',' Sangrur','Y',45),
 (275,'275',' Barnala','Y',45),
 (276,'276',' Malerkotla','Y',45),
 (277,'277',' Dhuri','Y',45),
 (278,'278',' Sangrur','Y',45),
 (279,'279',' Sunam','Y',45),
 (280,'280',' Moonak','Y',45),
 (281,'281',' Patiala','Y',46),
 (282,'282',' Samana','Y',46),
 (283,'283',' Nabha','Y',46),
 (284,'284',' Patiala','Y',46),
 (285,'285',' Rajpura','Y',46),
 (286,'286',' Dera Bassi','Y',46),
 (287,'287',' CHANDIGARH','Y',47),
 (288,'288',' Chandigarh','Y',48),
 (289,'289',' Chandigarh','Y',48),
 (290,'290',' UTTARANCHAL','Y',49),
 (291,'291',' Uttarkashi','Y',50),
 (292,'292',' Puraula','Y',50),
 (293,'293',' Rajgarhi','Y',50),
 (294,'294',' Dunda','Y',50),
 (295,'295',' Bhatwari','Y',50),
 (296,'296',' Chamoli','Y',51),
 (297,'297',' Joshimath ','Y',51),
 (298,'298',' Chamoli','Y',51),
 (299,'299',' Pokhari **','Y',51),
 (300,'300',' Karnaprayag ','Y',51),
 (301,'301',' Tharali','Y',51),
 (302,'302',' Gair Sain **','Y',51),
 (303,'303',' Rudraprayag *','Y',52),
 (304,'304',' Ukhimath','Y',52),
 (305,'305',' Rudraprayag ','Y',52),
 (306,'306',' Tehri Garhwal','Y',53),
 (307,'307',' Ghansali **','Y',53),
 (308,'308',' Devprayag','Y',53),
 (309,'309',' Pratapnagar','Y',53),
 (310,'310',' Tehri','Y',53),
 (311,'311',' Narendranagar','Y',53),
 (312,'312',' Dehradun','Y',54),
 (313,'313',' Chakrata','Y',54),
 (314,'314',' Vikasnagar **','Y',54),
 (315,'315',' Dehradun','Y',54),
 (316,'316',' Rishikesh **','Y',54),
 (317,'317',' Garhwal','Y',55),
 (318,'318',' Srinagar **','Y',55),
 (319,'319',' Pauri','Y',55),
 (320,'320',' Thali Sain','Y',55),
 (321,'321',' Dhoomakot','Y',55),
 (322,'322',' Lansdowne ','Y',55),
 (323,'323',' Kotdwara','Y',55),
 (324,'324',' Pithoragarh','Y',56),
 (325,'325',' Munsiari','Y',56),
 (326,'326',' Dharchula','Y',56),
 (327,'327',' Didihat','Y',56),
 (328,'328',' Gangolihat','Y',56),
 (329,'329',' Pithoragarh','Y',56),
 (330,'330',' Bageshwar','Y',57),
 (331,'331',' Kapkot **','Y',57),
 (332,'332',' Bageshwar ','Y',57),
 (333,'333',' Almora','Y',58),
 (334,'334',' Bhikia Sain','Y',58),
 (335,'335',' Ranikhet','Y',58),
 (336,'336',' Almora','Y',58),
 (337,'337',' Champawat','Y',59),
 (338,'338',' Champawat','Y',59),
 (339,'339',' Nainital','Y',60),
 (340,'340',' Kosya Kutauli','Y',60),
 (341,'341',' Nainital','Y',60),
 (342,'342',' Dhari','Y',60),
 (343,'343',' Haldwani','Y',60),
 (344,'344',' Udham Singh Nagar *','Y',61),
 (345,'345',' Kashipur','Y',61),
 (346,'346',' Kichha','Y',61),
 (347,'347',' Sitarganj','Y',61),
 (348,'348',' Khatima','Y',61),
 (349,'349',' Hardwar','Y',62),
 (350,'350',' Roorkee','Y',62),
 (351,'351',' Hardwar','Y',62),
 (352,'352',' Laksar','Y',62),
 (353,'353',' HARYANA','Y',63),
 (354,'354',' Panchkula *','Y',64),
 (355,'355',' Kalka','Y',64),
 (356,'356',' Panchkula','Y',64),
 (357,'357',' Ambala','Y',65),
 (358,'358',' Naraingarh','Y',65),
 (359,'359',' Ambala','Y',65),
 (360,'360',' Barara','Y',65),
 (361,'361',' Yamunanagar','Y',66),
 (362,'362',' Jagadhri','Y',66),
 (363,'363',' Chhachhrauli','Y',66),
 (364,'364',' Kurukshetra','Y',67),
 (365,'365',' Shahbad','Y',67),
 (366,'366',' Pehowa','Y',67),
 (367,'367',' Thanesar','Y',67),
 (368,'368',' Kaithal','Y',68),
 (369,'369',' Guhla','Y',68),
 (370,'370',' Kaithal','Y',68),
 (371,'371',' Karnal','Y',69),
 (372,'372',' Nilokheri','Y',69),
 (373,'373',' Indri','Y',69),
 (374,'374',' Karnal','Y',69),
 (375,'375',' Assandh','Y',69),
 (376,'376',' Gharaunda','Y',69),
 (377,'377',' Panipat','Y',70),
 (378,'378',' Panipat','Y',70),
 (379,'379',' Israna','Y',70),
 (380,'380',' Samalkha','Y',70),
 (381,'381',' Sonipat','Y',71),
 (382,'382',' Gohana','Y',71),
 (383,'383',' Ganaur','Y',71),
 (384,'384',' Sonipat','Y',71),
 (385,'385',' Kharkhoda','Y',71),
 (386,'386',' Jind','Y',72),
 (387,'387',' Narwana','Y',72),
 (388,'388',' Jind','Y',72),
 (389,'389',' Julana','Y',72),
 (390,'390',' Safidon','Y',72),
 (391,'391',' Fatehabad *','Y',73),
 (392,'392',' Ratia','Y',73),
 (393,'393',' Tohana','Y',73),
 (394,'394',' Fatehabad','Y',73),
 (395,'395',' Sirsa','Y',74),
 (396,'396',' Dabwali','Y',74),
 (397,'397',' Sirsa','Y',74),
 (398,'398',' Rania','Y',74),
 (399,'399',' Ellenabad','Y',74),
 (400,'400',' Hisar','Y',75),
 (401,'401',' Adampur','Y',75),
 (402,'402',' Hisar','Y',75),
 (403,'403',' Narnaund','Y',75),
 (404,'404',' Hansi','Y',75),
 (405,'405',' Bhiwani','Y',76),
 (406,'406',' Bawani Khera','Y',76),
 (407,'407',' Bhiwani','Y',76),
 (408,'408',' Tosham','Y',76),
 (409,'409',' Siwani','Y',76),
 (410,'410',' Loharu ','Y',76),
 (411,'411',' Dadri','Y',76),
 (412,'412',' Rohtak','Y',77),
 (413,'413',' Maham','Y',77),
 (414,'414',' Rohtak','Y',77),
 (415,'415',' Jhajjar *','Y',78),
 (416,'416',' Beri','Y',78),
 (417,'417',' Bahadurgarh','Y',78),
 (418,'418',' Jhajjar','Y',78),
 (419,'419',' Mahendragarh','Y',79),
 (420,'420',' Mahendragarh','Y',79),
 (421,'421',' Narnaul','Y',79),
 (422,'422',' Rewari','Y',80),
 (423,'423',' Kosli','Y',80),
 (424,'424',' Rewari','Y',80),
 (425,'425',' Bawal','Y',80),
 (426,'426',' Gurgaon','Y',81),
 (427,'427',' Pataudi','Y',81),
 (428,'428',' Gurgaon','Y',81),
 (429,'429',' Sohna','Y',81),
 (430,'430',' Taoru','Y',81),
 (431,'431',' Nuh','Y',81),
 (432,'432',' Ferozepur Jhirka','Y',81),
 (433,'433',' Punahana','Y',81),
 (434,'434',' Faridabad','Y',82),
 (435,'435',' Faridabad','Y',82),
 (436,'436',' Ballabgarh','Y',82),
 (437,'437',' Palwal','Y',82),
 (438,'438',' Hathin','Y',82),
 (439,'439',' Hodal','Y',82),
 (440,'440',' DELHI','Y',83),
 (441,'441',' North West   *','Y',84),
 (442,'442',' Narela','Y',84),
 (443,'443',' Saraswati Vihar','Y',84),
 (444,'444',' Model Town','Y',84),
 (445,'445',' North   *','Y',85),
 (446,'446',' Civil Lines','Y',85),
 (447,'447',' Sadar Bazar','Y',85),
 (448,'448',' Kotwali','Y',85),
 (449,'449',' North East   *','Y',86),
 (450,'450',' Seelam Pur','Y',86),
 (451,'451',' Shahdara','Y',86),
 (452,'452',' Seema Puri','Y',86),
 (453,'453',' East   *','Y',87),
 (454,'454',' Gandhi Nagar','Y',87),
 (455,'455',' Vivek Vihar','Y',87),
 (456,'456',' Preet Vihar','Y',87),
 (457,'457',' New Delhi','Y',88),
 (458,'458',' Parliament Street','Y',88),
 (459,'459',' Connaught Place','Y',88),
 (460,'460',' Chanakya Puri','Y',88),
 (461,'461',' Central  *','Y',89),
 (462,'462',' Karol Bagh','Y',89),
 (463,'463',' Pahar Ganj','Y',89),
 (464,'464',' Darya Ganj','Y',89),
 (465,'465',' West   *','Y',90),
 (466,'466',' Punjabi Bagh','Y',90),
 (467,'467',' Patel Nagar','Y',90),
 (468,'468',' Rajouri Garden','Y',90),
 (469,'469',' South West   *','Y',91),
 (470,'470',' Najafgarh','Y',91),
 (471,'471',' Delhi Cantonment.','Y',91),
 (472,'472',' Vasant Vihar','Y',91),
 (473,'473',' South  *','Y',92),
 (474,'474',' Defence Colony','Y',92),
 (475,'475',' Hauz Khas','Y',92),
 (476,'476',' Kalkaji','Y',92),
 (477,'477',' RAJASTHAN','Y',93),
 (478,'478',' Ganganagar','Y',94),
 (479,'479',' Karanpur','Y',94),
 (480,'480',' Ganganagar','Y',94),
 (481,'481',' Sadulshahar','Y',94),
 (482,'482',' Padampur','Y',94),
 (483,'483',' Raisinghnagar','Y',94),
 (484,'484',' Anupgarh','Y',94),
 (485,'485',' Gharsana','Y',94),
 (486,'486',' Vijainagar','Y',94),
 (487,'487',' Suratgarh','Y',94),
 (488,'488',' Hanumangarh *','Y',95),
 (489,'489',' Sangaria','Y',95),
 (490,'490',' Tibi','Y',95),
 (491,'491',' Hanumangarh','Y',95),
 (492,'492',' Pilibanga','Y',95),
 (493,'493',' Rawatsar','Y',95),
 (494,'494',' Nohar','Y',95),
 (495,'495',' Bhadra','Y',95),
 (496,'496',' Bikaner','Y',96),
 (497,'497',' Bikaner','Y',96),
 (498,'498',' Poogal','Y',96),
 (499,'499',' Lunkaransar','Y',96),
 (500,'500',' Kolayat','Y',96),
 (501,'501',' Nokha','Y',96),
 (502,'502',' Khajuwala','Y',96),
 (503,'503',' Chhatargarh','Y',96),
 (504,'504',' Churu','Y',97),
 (505,'505',' Taranagar','Y',97),
 (506,'506',' Rajgarh','Y',97),
 (507,'507',' Sardarshahar','Y',97),
 (508,'508',' Churu','Y',97),
 (509,'509',' Dungargarh','Y',97),
 (510,'510',' Ratangarh','Y',97),
 (511,'511',' Sujangarh','Y',97),
 (512,'512',' Jhunjhunun','Y',98),
 (513,'513',' Jhunjhunun','Y',98),
 (514,'514',' Chirawa','Y',98),
 (515,'515',' Buhana','Y',98),
 (516,'516',' Khetri','Y',98),
 (517,'517',' Nawalgarh','Y',98),
 (518,'518',' Udaipurwati','Y',98),
 (519,'519',' Alwar','Y',99),
 (520,'520',' Behror','Y',99),
 (521,'521',' Mandawar','Y',99),
 (522,'522',' Kotkasim','Y',99),
 (523,'523',' Tijara','Y',99),
 (524,'524',' Kishangarh Bas','Y',99),
 (525,'525',' Ramgarh','Y',99),
 (526,'526',' Alwar','Y',99),
 (527,'527',' Bansur','Y',99),
 (528,'528',' Thanagazi','Y',99),
 (529,'529',' Rajgarh','Y',99),
 (530,'530',' Lachhmangarh','Y',99),
 (531,'531',' Kathumar','Y',99),
 (532,'532',' Bharatpur','Y',100),
 (533,'533',' Pahari','Y',100),
 (534,'534',' Kaman','Y',100),
 (535,'535',' Nagar','Y',100),
 (536,'536',' Deeg','Y',100),
 (537,'537',' Nadbai','Y',100),
 (538,'538',' Kumher','Y',100),
 (539,'539',' Bharatpur','Y',100),
 (540,'540',' Weir','Y',100),
 (541,'541',' Bayana','Y',100),
 (542,'542',' Rupbas','Y',100),
 (543,'543',' Dhaulpur','Y',101),
 (544,'544',' Baseri','Y',101),
 (545,'545',' Bari','Y',101),
 (546,'546',' Sepau','Y',101),
 (547,'547',' Dhaulpur','Y',101),
 (548,'548',' Rajakhera','Y',101),
 (549,'549',' Karauli *','Y',102),
 (550,'550',' Todabhim','Y',102),
 (551,'551',' Nadoti','Y',102),
 (552,'552',' Hindaun','Y',102),
 (553,'553',' Karauli','Y',102),
 (554,'554',' Mandrail','Y',102),
 (555,'555',' Sapotra','Y',102),
 (556,'556',' Sawai Madhopur','Y',103),
 (557,'557',' Gangapur','Y',103),
 (558,'558',' Bamanwas','Y',103),
 (559,'559',' Malarna Doongar','Y',103),
 (560,'560',' Bonli','Y',103),
 (561,'561',' Chauth Ka Barwara','Y',103),
 (562,'562',' Sawai Madhopur','Y',103),
 (563,'563',' Khandar','Y',103),
 (564,'564',' Dausa *','Y',104),
 (565,'565',' Baswa','Y',104),
 (566,'566',' Mahwa','Y',104),
 (567,'567',' Sikrai','Y',104),
 (568,'568',' Dausa','Y',104),
 (569,'569',' Lalsot','Y',104),
 (570,'570',' Jaipur','Y',105),
 (571,'571',' Kotputli','Y',105),
 (572,'572',' Viratnagar','Y',105),
 (573,'573',' Shahpura','Y',105),
 (574,'574',' Chomu','Y',105),
 (575,'575',' Phulera (HQ.Sambhar)','Y',105),
 (576,'576',' Dudu (HQ. Mauzamabad)','Y',105),
 (577,'577',' Phagi','Y',105),
 (578,'578',' Sanganer','Y',105),
 (579,'579',' Jaipur','Y',105),
 (580,'580',' Amber','Y',105),
 (581,'581',' Jamwa Ramgarh','Y',105),
 (582,'582',' Bassi','Y',105),
 (583,'583',' Chaksu','Y',105),
 (584,'584',' Sikar','Y',106),
 (585,'585',' Fatehpur','Y',106),
 (586,'586',' Lachhmangarh','Y',106),
 (587,'587',' Sikar','Y',106),
 (588,'588',' Danta Ramgarh','Y',106),
 (589,'589',' Sri Madhopur','Y',106),
 (590,'590',' Neem Ka Thana','Y',106),
 (591,'591',' Nagaur','Y',107),
 (592,'592',' Ladnu','Y',107),
 (593,'593',' Didwana','Y',107),
 (594,'594',' Jayal','Y',107),
 (595,'595',' Nagaur','Y',107),
 (596,'596',' Kheenvsar','Y',107),
 (597,'597',' Merta','Y',107),
 (598,'598',' Degana','Y',107),
 (599,'599',' Parbatsar','Y',107),
 (600,'600',' Makrana','Y',107),
 (601,'601',' Nawa','Y',107),
 (602,'602',' Jodhpur','Y',108),
 (603,'603',' Phalodi','Y',108),
 (604,'604',' Osian','Y',108),
 (605,'605',' Bhopalgarh','Y',108),
 (606,'606',' Jodhpur','Y',108),
 (607,'607',' Shergarh','Y',108),
 (608,'608',' Luni','Y',108),
 (609,'609',' Bilara','Y',108),
 (610,'610',' Jaisalmer','Y',109),
 (611,'611',' Jaisalmer','Y',109),
 (612,'612',' Pokaran','Y',109),
 (613,'613',' Fatehgarh','Y',109),
 (614,'614',' Barmer','Y',110),
 (615,'615',' Sheo','Y',110),
 (616,'616',' Baytoo','Y',110),
 (617,'617',' Pachpadra','Y',110),
 (618,'618',' Siwana','Y',110),
 (619,'619',' Gudha Malani','Y',110),
 (620,'620',' Barmer','Y',110),
 (621,'621',' Ramsar','Y',110),
 (622,'622',' Chohtan','Y',110),
 (623,'623',' Jalor','Y',111),
 (624,'624',' Sayla','Y',111),
 (625,'625',' Ahore','Y',111),
 (626,'626',' Jalor','Y',111),
 (627,'627',' Bhinmal','Y',111),
 (628,'628',' Bagora','Y',111),
 (629,'629',' Sanchore','Y',111),
 (630,'630',' Raniwara','Y',111),
 (631,'631',' Sirohi','Y',112),
 (632,'632',' Sheoganj','Y',112),
 (633,'633',' Sirohi','Y',112),
 (634,'634',' Pindwara','Y',112),
 (635,'635',' Abu Road','Y',112),
 (636,'636',' Reodar','Y',112),
 (637,'637',' Pali','Y',113),
 (638,'638',' Jaitaran','Y',113),
 (639,'639',' Raipur','Y',113),
 (640,'640',' Sojat','Y',113),
 (641,'641',' Rohat','Y',113),
 (642,'642',' Pali','Y',113),
 (643,'643',' Marwar Junction','Y',113),
 (644,'644',' Desuri','Y',113),
 (645,'645',' Sumerpur','Y',113),
 (646,'646',' Bali','Y',113),
 (647,'647',' Ajmer','Y',114),
 (648,'648',' Kishangarh','Y',114),
 (649,'649',' Ajmer','Y',114),
 (650,'650',' Peesangan','Y',114),
 (651,'651',' Beawar','Y',114),
 (652,'652',' Masuda','Y',114),
 (653,'653',' Nasirabad','Y',114),
 (654,'654',' Bhinay','Y',114),
 (655,'655',' Sarwar','Y',114),
 (656,'656',' Kekri','Y',114),
 (657,'657',' Tonk','Y',115),
 (658,'658',' Malpura','Y',115),
 (659,'659',' Peeplu','Y',115),
 (660,'660',' Niwai','Y',115),
 (661,'661',' Tonk','Y',115),
 (662,'662',' Todaraisingh','Y',115),
 (663,'663',' Deoli','Y',115),
 (664,'664',' Uniara','Y',115),
 (665,'665',' Bundi','Y',116),
 (666,'666',' Hindoli','Y',116),
 (667,'667',' Nainwa','Y',116),
 (668,'668',' Indragarh','Y',116),
 (669,'669',' Keshoraipatan','Y',116),
 (670,'670',' Bundi','Y',116),
 (671,'671',' Bhilwara','Y',117),
 (672,'672',' Asind','Y',117),
 (673,'673',' Hurda','Y',117),
 (674,'674',' Shahpura','Y',117),
 (675,'675',' Banera','Y',117),
 (676,'676',' Mandal','Y',117),
 (677,'677',' Raipur','Y',117),
 (678,'678',' Sahara','Y',117),
 (679,'679',' Bhilwara','Y',117),
 (680,'680',' Kotri','Y',117),
 (681,'681',' Jahazpur','Y',117),
 (682,'682',' Mandalgarh','Y',117),
 (683,'683',' Beejoliya','Y',117),
 (684,'684',' Rajsamand *','Y',118),
 (685,'685',' Bhim','Y',118),
 (686,'686',' Deogarh','Y',118),
 (687,'687',' Amet','Y',118),
 (688,'688',' Kumbhalgarh','Y',118),
 (689,'689',' Rajsamand','Y',118),
 (690,'690',' Railmagra','Y',118),
 (691,'691',' Nathdwara','Y',118),
 (692,'692',' Udaipur','Y',119),
 (693,'693',' Mavli','Y',119),
 (694,'694',' Gogunda','Y',119),
 (695,'695',' Kotra','Y',119),
 (696,'696',' Jhadol','Y',119),
 (697,'697',' Girwa','Y',119),
 (698,'698',' Vallabhnagar','Y',119),
 (699,'699',' Dhariawad','Y',119),
 (700,'700',' Salumbar','Y',119),
 (701,'701',' Sarada','Y',119),
 (702,'702',' Kherwara','Y',119),
 (703,'703',' Dungarpur','Y',120),
 (704,'704',' Dungarpur','Y',120),
 (705,'705',' Aspur','Y',120),
 (706,'706',' Sagwara','Y',120),
 (707,'707',' Simalwara','Y',120),
 (708,'708',' Banswara','Y',121),
 (709,'709',' Ghatol','Y',121),
 (710,'710',' Garhi','Y',121),
 (711,'711',' Banswara','Y',121),
 (712,'712',' Bagidora','Y',121),
 (713,'713',' Kushalgarh','Y',121),
 (714,'714',' Chittaurgarh','Y',122),
 (715,'715',' Rashmi','Y',122),
 (716,'716',' Gangrar','Y',122),
 (717,'717',' Begun','Y',122),
 (718,'718',' Rawatbhata','Y',122),
 (719,'719',' Chittaurgarh','Y',122),
 (720,'720',' Kapasan','Y',122),
 (721,'721',' Dungla','Y',122),
 (722,'722',' Bhadesar','Y',122),
 (723,'723',' Nimbahera','Y',122),
 (724,'724',' Chhoti Sadri','Y',122),
 (725,'725',' Bari Sadri','Y',122),
 (726,'726',' Pratapgarh','Y',122),
 (727,'727',' Arnod','Y',122),
 (728,'728',' Kota','Y',123),
 (729,'729',' Pipalda','Y',123),
 (730,'730',' Digod','Y',123),
 (731,'731',' Ladpura','Y',123),
 (732,'732',' Ramganj Mandi','Y',123),
 (733,'733',' Sangod','Y',123),
 (734,'734',' Baran *','Y',124),
 (735,'735',' Mangrol','Y',124),
 (736,'736',' Antah','Y',124),
 (737,'737',' Baran','Y',124),
 (738,'738',' Atru','Y',124),
 (739,'739',' Kishanganj','Y',124),
 (740,'740',' Shahbad','Y',124),
 (741,'741',' Chhabra','Y',124),
 (742,'742',' Chhipabarod','Y',124),
 (743,'743',' Jhalawar','Y',125),
 (744,'744',' Khanpur','Y',125),
 (745,'745',' Jhalrapatan','Y',125),
 (746,'746',' Aklera','Y',125),
 (747,'747',' Manohar Thana','Y',125),
 (748,'748',' Pachpahar','Y',125),
 (749,'749',' Pirawa','Y',125),
 (750,'750',' Gangdhar','Y',125),
 (751,'751',' UTTAR PRADESH','Y',126),
 (752,'752',' Saharanpur','Y',127),
 (753,'753',' Behat','Y',127),
 (754,'754',' Saharanpur','Y',127),
 (755,'755',' Nakur','Y',127),
 (756,'756',' Deoband','Y',127),
 (757,'757',' Muzaffarnagar','Y',128),
 (758,'758',' Kairana','Y',128),
 (759,'759',' Shamli **','Y',128),
 (760,'760',' Muzaffarnagar','Y',128),
 (761,'761',' Budhana','Y',128),
 (762,'762',' Jansath','Y',128),
 (763,'763',' Bijnor','Y',129),
 (764,'764',' Najibabad','Y',129),
 (765,'765',' Bijnor','Y',129),
 (766,'766',' Nagina','Y',129),
 (767,'767',' Dhampur','Y',129),
 (768,'768',' Chandpur','Y',129),
 (769,'769',' Moradabad','Y',130),
 (770,'770',' Thakurdwara','Y',130),
 (771,'771',' Moradabad','Y',130),
 (772,'772',' Bilari','Y',130),
 (773,'773',' Sambhal','Y',130),
 (774,'774',' Chandausi','Y',130),
 (775,'775',' Rampur','Y',131),
 (776,'776',' Suar','Y',131),
 (777,'777',' Bilaspur','Y',131),
 (778,'778',' Rampur','Y',131),
 (779,'779',' Shahabad ','Y',131),
 (780,'780',' Milak ','Y',131),
 (781,'781',' Jyotiba Phule Nagar *','Y',132),
 (782,'782',' Dhanaura','Y',132),
 (783,'783',' Amroha','Y',132),
 (784,'784',' Hasanpur','Y',132),
 (785,'785',' Meerut','Y',133),
 (786,'786',' Sardhana','Y',133),
 (787,'787',' Mawana','Y',133),
 (788,'788',' Meerut','Y',133),
 (789,'789',' Baghpat *','Y',134),
 (790,'790',' Baraut **','Y',134),
 (791,'791',' Baghpat','Y',134),
 (792,'792',' Khekada **','Y',134),
 (793,'793',' Ghaziabad','Y',135),
 (794,'794',' Modinagar','Y',135),
 (795,'795',' Ghaziabad','Y',135),
 (796,'796',' Hapur','Y',135),
 (797,'797',' Garhmukteshwar','Y',135),
 (798,'798',' Gautam Buddha Nagar *','Y',136),
 (799,'799',' Dadri','Y',136),
 (800,'800',' Gautam Buddha Nagar **','Y',136),
 (801,'801',' Jewar **','Y',136),
 (802,'802',' Bulandshahr','Y',137),
 (803,'803',' Sikandrabad','Y',137),
 (804,'804',' Bulandshahr','Y',137),
 (805,'805',' Siana','Y',137),
 (806,'806',' Anupshahr','Y',137),
 (807,'807',' Debai **','Y',137),
 (808,'808',' Shikarpur **','Y',137),
 (809,'809',' Khurja ','Y',137),
 (810,'810',' Aligarh','Y',138),
 (811,'811',' Khair','Y',138),
 (812,'812',' Gabhana **','Y',138),
 (813,'813',' Atrauli','Y',138),
 (814,'814',' Koil ','Y',138),
 (815,'815',' Iglas','Y',138),
 (816,'816',' Hathras *','Y',139),
 (817,'817',' Sasni **','Y',139),
 (818,'818',' Sikandra Rao ','Y',139),
 (819,'819',' Hathras','Y',139),
 (820,'820',' Sadabad','Y',139),
 (821,'821',' Mathura','Y',140),
 (822,'822',' Chhata','Y',140),
 (823,'823',' Mat','Y',140),
 (824,'824',' Mathura','Y',140),
 (825,'825',' Agra','Y',141),
 (826,'826',' Etmadpur','Y',141),
 (827,'827',' Agra','Y',141),
 (828,'828',' Kiraoli','Y',141),
 (829,'829',' Kheragarh','Y',141),
 (830,'830',' Fatehabad','Y',141),
 (831,'831',' Bah','Y',141),
 (832,'832',' Firozabad','Y',142),
 (833,'833',' Tundla **','Y',142),
 (834,'834',' Firozabad','Y',142),
 (835,'835',' Jasrana ','Y',142),
 (836,'836',' Shikohabad ','Y',142),
 (837,'837',' Etah','Y',143),
 (838,'838',' Kasganj','Y',143),
 (839,'839',' Patiyali','Y',143),
 (840,'840',' Aliganj ','Y',143),
 (841,'841',' Etah','Y',143),
 (842,'842',' Jalesar ','Y',143),
 (843,'843',' Mainpuri','Y',144),
 (844,'844',' Mainpuri','Y',144),
 (845,'845',' Karhal','Y',144),
 (846,'846',' Bhogaon ','Y',144),
 (847,'847',' Budaun','Y',145),
 (848,'848',' Gunnaur ','Y',145),
 (849,'849',' Bisauli ','Y',145),
 (850,'850',' Bilsi **','Y',145),
 (851,'851',' Sahaswan','Y',145),
 (852,'852',' Budaun','Y',145),
 (853,'853',' Dataganj','Y',145),
 (854,'854',' Bareilly','Y',146),
 (855,'855',' Baheri','Y',146),
 (856,'856',' Meerganj ','Y',146),
 (857,'857',' Aonla ','Y',146),
 (858,'858',' Bareilly ','Y',146),
 (859,'859',' Nawabganj ','Y',146),
 (860,'860',' Faridpur ','Y',146),
 (861,'861',' Pilibhit','Y',147),
 (862,'862',' Pilibhit ','Y',147),
 (863,'863',' Bisalpur','Y',147),
 (864,'864',' Puranpur','Y',147),
 (865,'865',' Shahjahanpur','Y',148),
 (866,'866',' Powayan','Y',148),
 (867,'867',' Tilhar','Y',148),
 (868,'868',' Shahjahanpur','Y',148),
 (869,'869',' Jalalabad','Y',148),
 (870,'870',' Kheri','Y',149),
 (871,'871',' Nighasan','Y',149),
 (872,'872',' Gola Gokaran Nath','Y',149),
 (873,'873',' Mohammdi','Y',149),
 (874,'874',' Lakhimpur','Y',149),
 (875,'875',' Dhaurahara','Y',149),
 (876,'876',' Sitapur','Y',150),
 (877,'877',' Misrikh','Y',150),
 (878,'878',' Sitapur','Y',150),
 (879,'879',' Laharpur ','Y',150),
 (880,'880',' Biswan','Y',150),
 (881,'881',' Mahmudabad','Y',150),
 (882,'882',' Sidhauli','Y',150),
 (883,'883',' Hardoi','Y',151),
 (884,'884',' Shahabad','Y',151),
 (885,'885',' Sawayajpur **','Y',151),
 (886,'886',' Hardoi ','Y',151),
 (887,'887',' Bilgram','Y',151),
 (888,'888',' Sandila ','Y',151),
 (889,'889',' Unnao','Y',152),
 (890,'890',' Safipur','Y',152),
 (891,'891',' Hasanganj ','Y',152),
 (892,'892',' Unnao','Y',152),
 (893,'893',' Purwa ','Y',152),
 (894,'894',' Bighapur **','Y',152),
 (895,'895',' Lucknow','Y',153),
 (896,'896',' Malihabad','Y',153),
 (897,'897',' Bakshi Ka Talab**','Y',153),
 (898,'898',' Lucknow','Y',153),
 (899,'899',' Mohanlalganj ','Y',153),
 (900,'900',' Rae Bareli','Y',154),
 (901,'901',' Maharajganj','Y',154),
 (902,'902',' Tiloi','Y',154),
 (903,'903',' Rae Bareli ','Y',154),
 (904,'904',' Lalganj ','Y',154),
 (905,'905',' Dalmau ','Y',154),
 (906,'906',' Unchahar **','Y',154),
 (907,'907',' Salon','Y',154),
 (908,'908',' Farrukhabad','Y',155),
 (909,'909',' Kaimganj','Y',155),
 (910,'910',' Amritpur **','Y',155),
 (911,'911',' Farrukhabad ','Y',155),
 (912,'912',' Kannauj *','Y',156),
 (913,'913',' Chhibramau','Y',156),
 (914,'914',' Kannauj','Y',156),
 (915,'915',' Tirwa **','Y',156),
 (916,'916',' Etawah','Y',157),
 (917,'917',' Jaswantnagar **','Y',157),
 (918,'918',' Saifai **','Y',157),
 (919,'919',' Etawah','Y',157),
 (920,'920',' Bharthana','Y',157),
 (921,'921',' Chakarnagar **','Y',157),
 (922,'922',' Auraiya *','Y',158),
 (923,'923',' Bidhuna','Y',158),
 (924,'924',' Auraiya ','Y',158),
 (925,'925',' Kanpur Dehat','Y',159),
 (926,'926',' Rasulabad ','Y',159),
 (927,'927',' Derapur','Y',159),
 (928,'928',' Akbarpur ','Y',159),
 (929,'929',' Bhognipur','Y',159),
 (930,'930',' Sikandra **','Y',159),
 (931,'931',' Kanpur Nagar','Y',160),
 (932,'932',' Bilhaur ','Y',160),
 (933,'933',' Kanpur ','Y',160),
 (934,'934',' Ghatampur ','Y',160),
 (935,'935',' Jalaun ','Y',161),
 (936,'936',' Madhogarh ** ','Y',161),
 (937,'937',' Jalaun','Y',161),
 (938,'938',' Kalpi ','Y',161),
 (939,'939',' Orai','Y',161),
 (940,'940',' Konch','Y',161),
 (941,'941',' Jhansi','Y',162),
 (942,'942',' Moth','Y',162),
 (943,'943',' Garautha','Y',162),
 (944,'944',' Tahrauli **','Y',162),
 (945,'945',' Mauranipur','Y',162),
 (946,'946',' Jhansi','Y',162),
 (947,'947',' Lalitpur','Y',163),
 (948,'948',' Talbehat','Y',163),
 (949,'949',' Lalitpur ','Y',163),
 (950,'950',' Mahroni','Y',163),
 (951,'951',' Hamirpur','Y',164),
 (952,'952',' Hamirpur','Y',164),
 (953,'953',' Rath','Y',164),
 (954,'954',' Maudaha','Y',164),
 (955,'955',' Mahoba *','Y',165),
 (956,'956',' Kulpahar','Y',165),
 (957,'957',' Charkhari','Y',165),
 (958,'958',' Mahoba ','Y',165),
 (959,'959',' Banda','Y',166),
 (960,'960',' Banda','Y',166),
 (961,'961',' Baberu','Y',166),
 (962,'962',' Atarra ','Y',166),
 (963,'963',' Naraini','Y',166),
 (964,'964',' Chitrakoot *','Y',167),
 (965,'965',' Karwi','Y',167),
 (966,'966',' Mau','Y',167),
 (967,'967',' Fatehpur','Y',168),
 (968,'968',' Bindki','Y',168),
 (969,'969',' Fatehpur','Y',168),
 (970,'970',' Khaga ','Y',168),
 (971,'971',' Pratapgarh','Y',169),
 (972,'972',' Lalganj Ajhara ','Y',169),
 (973,'973',' Kunda','Y',169),
 (974,'974',' Pratapgarh','Y',169),
 (975,'975',' Patti','Y',169),
 (976,'976',' Kaushambi *','Y',170),
 (977,'977',' Sirathu','Y',170),
 (978,'978',' Manjhanpur ','Y',170),
 (979,'979',' Chail','Y',170),
 (980,'980',' Allahabad ','Y',171),
 (981,'981',' Soraon','Y',171),
 (982,'982',' Phulpur ','Y',171),
 (983,'983',' Allahabad **','Y',171),
 (984,'984',' Bara  ','Y',171),
 (985,'985',' Karchhana ','Y',171),
 (986,'986',' Handia','Y',171),
 (987,'987',' Meja','Y',171),
 (988,'988',' Koraon **','Y',171),
 (989,'989',' Barabanki','Y',172),
 (990,'990',' Fatehpur','Y',172),
 (991,'991',' Ramnagar ','Y',172),
 (992,'992',' Nawabganj ','Y',172),
 (993,'993',' Sirauli Gauspur**','Y',172),
 (994,'994',' Ramsanehighat','Y',172),
 (995,'995',' Haidergarh','Y',172),
 (996,'996',' Faizabad','Y',173),
 (997,'997',' Rudauli','Y',173),
 (998,'998',' Milkipur **','Y',173),
 (999,'999',' Sohawal **','Y',173),
 (1000,'1000',' Faizabad','Y',173),
 (1001,'1001',' Bikapur','Y',173),
 (1002,'1002',' Ambedkar Nagar *','Y',174),
 (1003,'1003',' Tanda','Y',174),
 (1004,'1004',' Allapur **','Y',174),
 (1005,'1005',' Jalalpur ','Y',174),
 (1006,'1006',' Akbarpur','Y',174),
 (1007,'1007',' Sultanpur','Y',175),
 (1008,'1008',' Musafirkhana ','Y',175),
 (1009,'1009',' Gauriganj','Y',175),
 (1010,'1010',' Amethi ','Y',175),
 (1011,'1011',' Sultanpur','Y',175),
 (1012,'1012',' Lambhuwa **','Y',175),
 (1013,'1013',' Kadipur ','Y',175),
 (1014,'1014',' Bahraich','Y',176),
 (1015,'1015',' Nanpara ','Y',176),
 (1016,'1016',' Mahasi **','Y',176),
 (1017,'1017',' Bahraich','Y',176),
 (1018,'1018',' Kaiserganj','Y',176),
 (1019,'1019',' Shrawasti *','Y',177),
 (1020,'1020',' Bhinga','Y',177),
 (1021,'1021',' Ikauna **','Y',177),
 (1022,'1022',' Payagpur **','Y',177),
 (1023,'1023',' Balrampur *','Y',178),
 (1024,'1024',' Balrampur','Y',178),
 (1025,'1025',' Tulsipur','Y',178),
 (1026,'1026',' Utraula','Y',178),
 (1027,'1027',' Gonda','Y',179),
 (1028,'1028',' Gonda ','Y',179),
 (1029,'1029',' Colonelganj ','Y',179),
 (1030,'1030',' Tarabganj','Y',179),
 (1031,'1031',' Mankapur ','Y',179),
 (1032,'1032',' Siddharthnagar','Y',180),
 (1033,'1033',' Shohratgarh **','Y',180),
 (1034,'1034',' Naugarh','Y',180),
 (1035,'1035',' Bansi','Y',180),
 (1036,'1036',' Itwa','Y',180),
 (1037,'1037',' Domariyaganj','Y',180),
 (1038,'1038',' Basti','Y',181),
 (1039,'1039',' Bhanpur','Y',181),
 (1040,'1040',' Harraiya','Y',181),
 (1041,'1041',' Basti','Y',181),
 (1042,'1042',' Sant Kabir Nagar *','Y',182),
 (1043,'1043',' Mehdawal **','Y',182),
 (1044,'1044',' Khalilabad','Y',182),
 (1045,'1045',' Ghanghata **','Y',182),
 (1046,'1046',' Maharajganj','Y',183),
 (1047,'1047',' Nautanwa','Y',183),
 (1048,'1048',' Nichlaul','Y',183),
 (1049,'1049',' Pharenda','Y',183),
 (1050,'1050',' Maharajganj','Y',183),
 (1051,'1051',' Gorakhpur','Y',184),
 (1052,'1052',' Campierganj **','Y',184),
 (1053,'1053',' Sahjanwa','Y',184),
 (1054,'1054',' Gorakhpur','Y',184),
 (1055,'1055',' Chauri Chaura','Y',184),
 (1056,'1056',' Bansgaon','Y',184),
 (1057,'1057',' Khajani','Y',184),
 (1058,'1058',' Gola ','Y',184),
 (1059,'1059',' Kushinagar *','Y',185),
 (1060,'1060',' Padrauna','Y',185),
 (1061,'1061',' Hata','Y',185),
 (1062,'1062',' Kasya **','Y',185),
 (1063,'1063',' Tamkuhi Raj','Y',185),
 (1064,'1064',' Deoria','Y',186),
 (1065,'1065',' Deoria','Y',186),
 (1066,'1066',' Rudrapur','Y',186),
 (1067,'1067',' Barhaj **','Y',186),
 (1068,'1068',' Salempur','Y',186),
 (1069,'1069',' Bhatpar Rani **','Y',186),
 (1070,'1070',' Azamgarh','Y',187),
 (1071,'1071',' Burhanpur','Y',187),
 (1072,'1072',' Sagri','Y',187),
 (1073,'1073',' Azamgarh ','Y',187),
 (1074,'1074',' Nizamabad **','Y',187),
 (1075,'1075',' Phulpur ','Y',187),
 (1076,'1076',' Lalganj','Y',187),
 (1077,'1077',' Mehnagar **','Y',187),
 (1078,'1078',' Mau','Y',188),
 (1079,'1079',' Ghosi','Y',188),
 (1080,'1080',' Madhuban **','Y',188),
 (1081,'1081',' Maunath Bhanjan','Y',188),
 (1082,'1082',' Muhammadabad Gohna','Y',188),
 (1083,'1083',' Ballia','Y',189),
 (1084,'1084',' Belthara Road **','Y',189),
 (1085,'1085',' Sikanderpur **','Y',189),
 (1086,'1086',' Rasra','Y',189),
 (1087,'1087',' Ballia','Y',189),
 (1088,'1088',' Bansdih','Y',189),
 (1089,'1089',' Bairia','Y',189),
 (1090,'1090',' Jaunpur','Y',190),
 (1091,'1091',' Shahganj','Y',190),
 (1092,'1092',' Badlapur','Y',190),
 (1093,'1093',' Machhlishahr','Y',190),
 (1094,'1094',' Jaunpur ','Y',190),
 (1095,'1095',' Mariahu ','Y',190),
 (1096,'1096',' Kerakat','Y',190),
 (1097,'1097',' Ghazipur','Y',191),
 (1098,'1098',' Jakhanian **','Y',191),
 (1099,'1099',' Saidpur','Y',191),
 (1100,'1100',' Ghazipur','Y',191),
 (1101,'1101',' Mohammadabad ','Y',191),
 (1102,'1102',' Zamania','Y',191),
 (1103,'1103',' Chandauli *','Y',192),
 (1104,'1104',' Sakaldiha','Y',192),
 (1105,'1105',' Chandauli','Y',192),
 (1106,'1106',' Chakia','Y',192),
 (1107,'1107',' Varanasi','Y',193),
 (1108,'1108',' Pindra **','Y',193),
 (1109,'1109',' Varanasi','Y',193),
 (1110,'1110',' Sant Ravidas Nagar *','Y',194),
 (1111,'1111',' Bhadohi','Y',194),
 (1112,'1112',' Gyanpur','Y',194),
 (1113,'1113',' Mirzapur','Y',195),
 (1114,'1114',' Mirzapur','Y',195),
 (1115,'1115',' Lalganj','Y',195),
 (1116,'1116',' Marihan','Y',195),
 (1117,'1117',' Chunar','Y',195),
 (1118,'1118',' Sonbhadra','Y',196),
 (1119,'1119',' Ghorawal **','Y',196),
 (1120,'1120',' Robertsganj','Y',196),
 (1121,'1121',' Dudhi','Y',196),
 (1122,'1122',' BIHAR','Y',197),
 (1123,'1123',' Pashchim Champaran','Y',198),
 (1124,'1124',' Sidhaw','Y',198),
 (1125,'1125',' Ramnagar','Y',198),
 (1126,'1126',' Gaunaha','Y',198),
 (1127,'1127',' Mainatanr','Y',198),
 (1128,'1128',' Narkatiaganj','Y',198),
 (1129,'1129',' Lauria','Y',198),
 (1130,'1130',' Bagaha','Y',198),
 (1131,'1131',' Piprasi','Y',198),
 (1132,'1132',' Madhubani','Y',198),
 (1133,'1133',' Bhitaha','Y',198),
 (1134,'1134',' Thakrahan','Y',198),
 (1135,'1135',' Jogapatti','Y',198),
 (1136,'1136',' Chanpatia','Y',198),
 (1137,'1137',' Sikta','Y',198),
 (1138,'1138',' Majhaulia','Y',198),
 (1139,'1139',' Bettiah','Y',198),
 (1140,'1140',' Bairia','Y',198),
 (1141,'1141',' Nautan','Y',198),
 (1142,'1142',' Purba Champaran','Y',199),
 (1143,'1143',' Raxaul','Y',199),
 (1144,'1144',' Adapur','Y',199),
 (1145,'1145',' Ramgarhwa','Y',199),
 (1146,'1146',' Sugauli','Y',199),
 (1147,'1147',' Banjaria','Y',199),
 (1148,'1148',' Narkatia','Y',199),
 (1149,'1149',' Bankatwa','Y',199),
 (1150,'1150',' Ghorasahan','Y',199),
 (1151,'1151',' Dhaka','Y',199),
 (1152,'1152',' Chiraia','Y',199),
 (1153,'1153',' Motihari','Y',199),
 (1154,'1154',' Turkaulia','Y',199),
 (1155,'1155',' Harsidhi','Y',199),
 (1156,'1156',' Paharpur','Y',199),
 (1157,'1157',' Areraj','Y',199),
 (1158,'1158',' Sangrampur','Y',199),
 (1159,'1159',' Kesaria','Y',199),
 (1160,'1160',' Kalyanpur','Y',199),
 (1161,'1161',' Kotwa','Y',199),
 (1162,'1162',' Piprakothi','Y',199),
 (1163,'1163',' Chakia(Pipra)','Y',199),
 (1164,'1164',' Pakri Dayal','Y',199),
 (1165,'1165',' Patahi','Y',199),
 (1166,'1166',' Phenhara','Y',199),
 (1167,'1167',' Madhuban','Y',199),
 (1168,'1168',' Tetaria','Y',199),
 (1169,'1169',' Mehsi','Y',199),
 (1170,'1170',' Sheohar *','Y',200),
 (1171,'1171',' Purnahiya','Y',200),
 (1172,'1172',' Piprarhi','Y',200),
 (1173,'1173',' Sheohar','Y',200),
 (1174,'1174',' Dumri Katsari','Y',200),
 (1175,'1175',' Tariani Chowk','Y',200),
 (1176,'1176',' Sitamarhi','Y',201),
 (1177,'1177',' Bairgania','Y',201),
 (1178,'1178',' Suppi','Y',201),
 (1179,'1179',' Majorganj','Y',201),
 (1180,'1180',' Sonbarsa','Y',201),
 (1181,'1181',' Parihar','Y',201),
 (1182,'1182',' Sursand','Y',201),
 (1183,'1183',' Bathnaha','Y',201),
 (1184,'1184',' Riga','Y',201),
 (1185,'1185',' Parsauni','Y',201),
 (1186,'1186',' Belsand','Y',201),
 (1187,'1187',' Runisaidpur','Y',201),
 (1188,'1188',' Dumra','Y',201),
 (1189,'1189',' Bajpatti','Y',201),
 (1190,'1190',' Charaut','Y',201),
 (1191,'1191',' Pupri','Y',201),
 (1192,'1192',' Nanpur','Y',201),
 (1193,'1193',' Bokhara','Y',201),
 (1194,'1194',' Madhubani','Y',202),
 (1195,'1195',' Madhwapur','Y',202),
 (1196,'1196',' Harlakhi','Y',202),
 (1197,'1197',' Basopatti','Y',202),
 (1198,'1198',' Jainagar','Y',202),
 (1199,'1199',' Ladania','Y',202),
 (1200,'1200',' Laukaha','Y',202),
 (1201,'1201',' Laukahi','Y',202),
 (1202,'1202',' Phulparas','Y',202),
 (1203,'1203',' Babubarhi','Y',202),
 (1204,'1204',' Khajauli','Y',202),
 (1205,'1205',' Kaluahi','Y',202),
 (1206,'1206',' Benipatti','Y',202),
 (1207,'1207',' Bisfi','Y',202),
 (1208,'1208',' Madhubani','Y',202),
 (1209,'1209',' Pandaul','Y',202),
 (1210,'1210',' Rajnagar','Y',202),
 (1211,'1211',' Andhratharhi','Y',202),
 (1212,'1212',' Jhanjharpur','Y',202),
 (1213,'1213',' Ghoghardiha','Y',202),
 (1214,'1214',' Lakhnaur','Y',202),
 (1215,'1215',' Madhepur','Y',202),
 (1216,'1216',' Supaul *','Y',203),
 (1217,'1217',' Nirmali','Y',203),
 (1218,'1218',' Basantpur','Y',203),
 (1219,'1219',' Chhatapur','Y',203),
 (1220,'1220',' Pratapganj','Y',203),
 (1221,'1221',' Raghopur','Y',203),
 (1222,'1222',' Saraigarh Bhaptiyahi','Y',203),
 (1223,'1223',' Kishanpur','Y',203),
 (1224,'1224',' Marauna','Y',203),
 (1225,'1225',' Supaul','Y',203),
 (1226,'1226',' Pipra','Y',203),
 (1227,'1227',' Tribeniganj','Y',203),
 (1228,'1228',' Araria','Y',204),
 (1229,'1229',' Narpatganj','Y',204),
 (1230,'1230',' Forbesganj','Y',204),
 (1231,'1231',' Bhargama','Y',204),
 (1232,'1232',' Raniganj','Y',204),
 (1233,'1233',' Araria','Y',204),
 (1234,'1234',' Kursakatta','Y',204),
 (1235,'1235',' Sikti','Y',204),
 (1236,'1236',' Palasi','Y',204),
 (1237,'1237',' Jokihat','Y',204),
 (1238,'1238',' Kishanganj','Y',205),
 (1239,'1239',' Terhagachh','Y',205),
 (1240,'1240',' Dighalbank','Y',205),
 (1241,'1241',' Thakurganj','Y',205),
 (1242,'1242',' Pothia','Y',205),
 (1243,'1243',' Bahadurganj','Y',205),
 (1244,'1244',' Kochadhamin','Y',205),
 (1245,'1245',' Kishanganj','Y',205),
 (1246,'1246',' Purnia','Y',206),
 (1247,'1247',' Banmankhi','Y',206),
 (1248,'1248',' Barhara','Y',206),
 (1249,'1249',' Bhawanipur','Y',206),
 (1250,'1250',' Rupauli','Y',206),
 (1251,'1251',' Dhamdaha','Y',206),
 (1252,'1252',' Krityanand Nagar','Y',206),
 (1253,'1253',' Purnia East','Y',206),
 (1254,'1254',' Kasba','Y',206),
 (1255,'1255',' Srinagar','Y',206),
 (1256,'1256',' Jalalgarh','Y',206),
 (1257,'1257',' Amour','Y',206),
 (1258,'1258',' Baisa','Y',206),
 (1259,'1259',' Baisi','Y',206),
 (1260,'1260',' Dagarua','Y',206),
 (1261,'1261',' Katihar','Y',207),
 (1262,'1262',' Falka','Y',207),
 (1263,'1263',' Korha','Y',207),
 (1264,'1264',' Hasanganj','Y',207),
 (1265,'1265',' Kadwa','Y',207),
 (1266,'1266',' Balrampur','Y',207),
 (1267,'1267',' Barsoi','Y',207),
 (1268,'1268',' Azamnagar','Y',207),
 (1269,'1269',' Pranpur','Y',207),
 (1270,'1270',' Dandkhora','Y',207),
 (1271,'1271',' katihar','Y',207),
 (1272,'1272',' Mansahi','Y',207),
 (1273,'1273',' Barari','Y',207),
 (1274,'1274',' Sameli','Y',207),
 (1275,'1275',' Kursela','Y',207),
 (1276,'1276',' Manihari','Y',207),
 (1277,'1277',' Amdabad','Y',207),
 (1278,'1278',' Madhepura','Y',208),
 (1279,'1279',' Gamharia','Y',208),
 (1280,'1280',' Singheshwar','Y',208),
 (1281,'1281',' Ghailarh','Y',208),
 (1282,'1282',' Madhepura','Y',208),
 (1283,'1283',' Shankarpur','Y',208),
 (1284,'1284',' Kumarkhand','Y',208),
 (1285,'1285',' Murliganj','Y',208),
 (1286,'1286',' Gwalpara','Y',208),
 (1287,'1287',' Bihariganj','Y',208),
 (1288,'1288',' Kishanganj','Y',208),
 (1289,'1289',' Puraini','Y',208),
 (1290,'1290',' Alamnagar','Y',208),
 (1291,'1291',' Chausa','Y',208),
 (1292,'1292',' Saharsa','Y',209),
 (1293,'1293',' Nauhatta','Y',209),
 (1294,'1294',' Satar Kataiya','Y',209),
 (1295,'1295',' Mahishi','Y',209),
 (1296,'1296',' Kahara','Y',209),
 (1297,'1297',' Saur Bazar','Y',209),
 (1298,'1298',' Patarghat','Y',209),
 (1299,'1299',' Sonbarsa','Y',209),
 (1300,'1300',' Simri Bakhtiarpur','Y',209),
 (1301,'1301',' Salkhua','Y',209),
 (1302,'1302',' Banma Itarhi','Y',209),
 (1303,'1303',' Darbhanga','Y',210),
 (1304,'1304',' Jale','Y',210),
 (1305,'1305',' Singhwara','Y',210),
 (1306,'1306',' Keotiranway','Y',210),
 (1307,'1307',' Darbhanga','Y',210),
 (1308,'1308',' Manigachhi','Y',210),
 (1309,'1309',' Tardih','Y',210),
 (1310,'1310',' Alinagar','Y',210),
 (1311,'1311',' Benipur','Y',210),
 (1312,'1312',' Bahadurpur','Y',210),
 (1313,'1313',' Hanumannagar','Y',210),
 (1314,'1314',' Hayaghat','Y',210),
 (1315,'1315',' Baheri','Y',210),
 (1316,'1316',' Biraul','Y',210),
 (1317,'1317',' Ghanshyampur','Y',210),
 (1318,'1318',' Kiratpur','Y',210),
 (1319,'1319',' Gora Bauram','Y',210),
 (1320,'1320',' Kusheshwar Asthan','Y',210),
 (1321,'1321',' Kusheshwar Asthan Purbi','Y',210),
 (1322,'1322',' Muzaffarpur','Y',211),
 (1323,'1323',' Sahebganj','Y',211),
 (1324,'1324',' Baruraj (Motipur)','Y',211),
 (1325,'1325',' Paroo','Y',211),
 (1326,'1326',' Saraiya','Y',211),
 (1327,'1327',' Marwan','Y',211),
 (1328,'1328',' Kanti','Y',211),
 (1329,'1329',' Minapur','Y',211),
 (1330,'1330',' Bochaha','Y',211),
 (1331,'1331',' Aurai','Y',211),
 (1332,'1332',' Katra','Y',211),
 (1333,'1333',' Gaighat','Y',211),
 (1334,'1334',' Bandra','Y',211),
 (1335,'1335',' Dholi (Moraul)','Y',211),
 (1336,'1336',' Musahari','Y',211),
 (1337,'1337',' Kurhani','Y',211),
 (1338,'1338',' Sakra','Y',211),
 (1339,'1339',' Gopalganj','Y',212),
 (1340,'1340',' Kataiya','Y',212),
 (1341,'1341',' Bijaipur','Y',212),
 (1342,'1342',' Bhorey','Y',212),
 (1343,'1343',' Pachdeori','Y',212),
 (1344,'1344',' Kuchaikote','Y',212),
 (1345,'1345',' phulwaria','Y',212),
 (1346,'1346',' Hathua','Y',212),
 (1347,'1347',' Uchkagaon','Y',212),
 (1348,'1348',' Thawe','Y',212),
 (1349,'1349',' Gopalganj','Y',212),
 (1350,'1350',' Manjha','Y',212),
 (1351,'1351',' Barauli','Y',212),
 (1352,'1352',' Sidhwalia','Y',212),
 (1353,'1353',' Baikunthpur','Y',212),
 (1354,'1354',' Siwan','Y',213),
 (1355,'1355',' Nautan','Y',213),
 (1356,'1356',' Siwan','Y',213),
 (1357,'1357',' Barharia','Y',213),
 (1358,'1358',' Goriakothi','Y',213),
 (1359,'1359',' Lakri Nabiganj','Y',213),
 (1360,'1360',' Basantpur','Y',213),
 (1361,'1361',' Bhagwanpur Hat','Y',213),
 (1362,'1362',' Maharajganj','Y',213),
 (1363,'1363',' Pachrukhi','Y',213),
 (1364,'1364',' Hussainganj','Y',213),
 (1365,'1365',' Ziradei','Y',213),
 (1366,'1366',' Mairwa','Y',213),
 (1367,'1367',' Guthani','Y',213),
 (1368,'1368',' Darauli','Y',213),
 (1369,'1369',' Andar','Y',213),
 (1370,'1370',' Raghunathpur','Y',213),
 (1371,'1371',' Hasanpura','Y',213),
 (1372,'1372',' Daraundha','Y',213),
 (1373,'1373',' Siswan','Y',213),
 (1374,'1374',' Saran','Y',214),
 (1375,'1375',' Mashrakh','Y',214),
 (1376,'1376',' Panapur','Y',214),
 (1377,'1377',' Taraiya','Y',214),
 (1378,'1378',' Ishupur','Y',214),
 (1379,'1379',' Baniapur','Y',214),
 (1380,'1380',' Lahladpur','Y',214),
 (1381,'1381',' Ekma','Y',214),
 (1382,'1382',' Manjhi','Y',214),
 (1383,'1383',' Jalalpur','Y',214),
 (1384,'1384',' Revelganj','Y',214),
 (1385,'1385',' Chapra','Y',214),
 (1386,'1386',' Nagra','Y',214),
 (1387,'1387',' Marhaura','Y',214),
 (1388,'1388',' Amnour','Y',214),
 (1389,'1389',' Maker','Y',214),
 (1390,'1390',' Parsa','Y',214),
 (1391,'1391',' Dariapur','Y',214),
 (1392,'1392',' Garkha','Y',214),
 (1393,'1393',' Dighwara','Y',214),
 (1394,'1394',' Sonepur','Y',214),
 (1395,'1395',' Vaishali','Y',215),
 (1396,'1396',' Vaishali','Y',215),
 (1397,'1397',' Paterhi Belsar','Y',215),
 (1398,'1398',' Lalganj','Y',215),
 (1399,'1399',' Bhagwanpur','Y',215),
 (1400,'1400',' Goraul','Y',215),
 (1401,'1401',' Chehra Kalan','Y',215),
 (1402,'1402',' Patepur','Y',215),
 (1403,'1403',' Mahua','Y',215),
 (1404,'1404',' Jandaha','Y',215),
 (1405,'1405',' Raja Pakar','Y',215),
 (1406,'1406',' Hajipur','Y',215),
 (1407,'1407',' Raghopur','Y',215),
 (1408,'1408',' Bidupur','Y',215),
 (1409,'1409',' Desri','Y',215),
 (1410,'1410',' Sahdai Buzurg','Y',215),
 (1411,'1411',' Mahnar','Y',215),
 (1412,'1412',' Samastipur','Y',216),
 (1413,'1413',' Kalyanpur','Y',216),
 (1414,'1414',' Warisnagar','Y',216),
 (1415,'1415',' Shivaji Nagar','Y',216),
 (1416,'1416',' Khanpur','Y',216),
 (1417,'1417',' Samastipur','Y',216),
 (1418,'1418',' Pusa','Y',216),
 (1419,'1419',' Tajpur','Y',216),
 (1420,'1420',' Morwa','Y',216),
 (1421,'1421',' Patori','Y',216),
 (1422,'1422',' Mohanpur','Y',216),
 (1423,'1423',' Mohiuddinagar','Y',216),
 (1424,'1424',' Sarairanjan','Y',216),
 (1425,'1425',' Vidyapati Nagar','Y',216),
 (1426,'1426',' Dalsinghsarai','Y',216),
 (1427,'1427',' Ujiarpur','Y',216),
 (1428,'1428',' Bibhutpur','Y',216),
 (1429,'1429',' Rosera','Y',216),
 (1430,'1430',' Singhia','Y',216),
 (1431,'1431',' Hasanpur','Y',216),
 (1432,'1432',' Bithan','Y',216),
 (1433,'1433',' Begusarai','Y',217),
 (1434,'1434',' Khudabandpur','Y',217),
 (1435,'1435',' Chorahi','Y',217),
 (1436,'1436',' Garhpura','Y',217),
 (1437,'1437',' Cheria Bariarpur','Y',217),
 (1438,'1438',' Bhagwanpur','Y',217),
 (1439,'1439',' Mansurchak','Y',217),
 (1440,'1440',' Bachhwara','Y',217),
 (1441,'1441',' Teghra','Y',217),
 (1442,'1442',' Barauni','Y',217),
 (1443,'1443',' Birpur','Y',217),
 (1444,'1444',' Begusarai','Y',217),
 (1445,'1445',' Naokothi','Y',217),
 (1446,'1446',' Bakhri','Y',217),
 (1447,'1447',' Dandari','Y',217),
 (1448,'1448',' Sahebpur Kamal','Y',217),
 (1449,'1449',' Balia','Y',217),
 (1450,'1450',' Matihani','Y',217),
 (1451,'1451',' Shamho Akha Kurha','Y',217),
 (1452,'1452',' Khagaria','Y',218),
 (1453,'1453',' Alauli','Y',218),
 (1454,'1454',' Khagaria','Y',218),
 (1455,'1455',' Mansi','Y',218),
 (1456,'1456',' Chautham','Y',218),
 (1457,'1457',' Beldaur','Y',218),
 (1458,'1458',' Gogari','Y',218),
 (1459,'1459',' Parbatta','Y',218),
 (1460,'1460',' Bhagalpur','Y',219),
 (1461,'1461',' Narayanpur','Y',219),
 (1462,'1462',' Bihpur','Y',219),
 (1463,'1463',' Kharik','Y',219),
 (1464,'1464',' Naugachhia','Y',219),
 (1465,'1465',' Rangra Chowk','Y',219),
 (1466,'1466',' Gopalpur','Y',219),
 (1467,'1467',' Pirpainti','Y',219),
 (1468,'1468',' Colgong','Y',219),
 (1469,'1469',' Ismailpur','Y',219),
 (1470,'1470',' Sabour','Y',219),
 (1471,'1471',' Nathnagar','Y',219),
 (1472,'1472',' Sultanganj','Y',219),
 (1473,'1473',' Shahkund','Y',219),
 (1474,'1474',' Goradih','Y',219),
 (1475,'1475',' Jagdishpur','Y',219),
 (1476,'1476',' Sonhaula','Y',219),
 (1477,'1477',' Banka *','Y',220),
 (1478,'1478',' Shambhuganj','Y',220),
 (1479,'1479',' Amarpur','Y',220),
 (1480,'1480',' Rajaun','Y',220),
 (1481,'1481',' Dhuraiya','Y',220),
 (1482,'1482',' Barahat','Y',220),
 (1483,'1483',' Banka','Y',220),
 (1484,'1484',' Phulidumar','Y',220),
 (1485,'1485',' Belhar','Y',220),
 (1486,'1486',' Chanan','Y',220),
 (1487,'1487',' Katoria','Y',220),
 (1488,'1488',' Bausi','Y',220),
 (1489,'1489',' Munger','Y',221),
 (1490,'1490',' Munger','Y',221),
 (1491,'1491',' Bariyarpur','Y',221),
 (1492,'1492',' Jamalpur','Y',221),
 (1493,'1493',' Dharhara','Y',221),
 (1494,'1494',' Kharagpur','Y',221),
 (1495,'1495',' Asarganj','Y',221),
 (1496,'1496',' Tarapur','Y',221),
 (1497,'1497',' Tetiha Bambor','Y',221),
 (1498,'1498',' Sangrampur','Y',221),
 (1499,'1499',' Lakhisarai *','Y',222),
 (1500,'1500',' Barahiya','Y',222),
 (1501,'1501',' Pipariya','Y',222),
 (1502,'1502',' Surajgarha','Y',222),
 (1503,'1503',' Lakhisarai','Y',222),
 (1504,'1504',' Ramgarh Chowk','Y',222),
 (1505,'1505',' Halsi','Y',222),
 (1506,'1506',' Sheikhpura *','Y',223),
 (1507,'1507',' Barbigha','Y',223),
 (1508,'1508',' Shekhopur Sarai','Y',223),
 (1509,'1509',' Sheikhpura','Y',223),
 (1510,'1510',' Ghat Kusmha','Y',223),
 (1511,'1511',' Chewara','Y',223),
 (1512,'1512',' Ariari','Y',223),
 (1513,'1513',' Nalanda','Y',224),
 (1514,'1514',' Karai Parsurai','Y',224),
 (1515,'1515',' Nagar Nausa','Y',224),
 (1516,'1516',' Harnaut','Y',224),
 (1517,'1517',' Chandi','Y',224),
 (1518,'1518',' Rahui','Y',224),
 (1519,'1519',' Bind','Y',224),
 (1520,'1520',' Sarmera','Y',224),
 (1521,'1521',' Asthawan ','Y',224),
 (1522,'1522',' Bihar','Y',224),
 (1523,'1523',' Noorsarai','Y',224),
 (1524,'1524',' Tharthari ','Y',224),
 (1525,'1525',' Parbalpur','Y',224),
 (1526,'1526',' Hilsa','Y',224),
 (1527,'1527',' Ekangarsarai','Y',224),
 (1528,'1528',' Islampur','Y',224),
 (1529,'1529',' Ben','Y',224),
 (1530,'1530',' Rajgir','Y',224),
 (1531,'1531',' Silao','Y',224),
 (1532,'1532',' Giriak','Y',224),
 (1533,'1533',' Katrisarai','Y',224),
 (1534,'1534',' Patna','Y',225),
 (1535,'1535',' Maner','Y',225),
 (1536,'1536',' Dinapur-Cum-Khagaul','Y',225),
 (1537,'1537',' Patna Rural (a) Patna Rural (b)','Y',225),
 (1538,'1538',' Sampatchak','Y',225),
 (1539,'1539',' Phulwari','Y',225),
 (1540,'1540',' Bihta','Y',225),
 (1541,'1541',' Naubatpur','Y',225),
 (1542,'1542',' Bikram','Y',225),
 (1543,'1543',' Dulhin Bazar','Y',225),
 (1544,'1544',' Paliganj','Y',225),
 (1545,'1545',' Masaurhi ','Y',225),
 (1546,'1546',' Dhanarua','Y',225),
 (1547,'1547',' Punpun','Y',225),
 (1548,'1548',' Fatwah','Y',225),
 (1549,'1549',' Daniawan ','Y',225),
 (1550,'1550',' Khusrupur','Y',225),
 (1551,'1551',' Bakhtiarpur','Y',225),
 (1552,'1552',' Athmalgola','Y',225),
 (1553,'1553',' Belchhi','Y',225),
 (1554,'1554',' Barh','Y',225),
 (1555,'1555',' Pandarak','Y',225),
 (1556,'1556',' Ghoswari','Y',225),
 (1557,'1557',' Mokameh','Y',225),
 (1558,'1558',' Bhojpur','Y',226),
 (1559,'1559',' Shahpur','Y',226),
 (1560,'1560',' Arrah','Y',226),
 (1561,'1561',' Barhara','Y',226),
 (1562,'1562',' Koilwar','Y',226),
 (1563,'1563',' Sandesh','Y',226),
 (1564,'1564',' Udwant Nagar','Y',226),
 (1565,'1565',' Behea','Y',226),
 (1566,'1566',' Jagdishpur','Y',226),
 (1567,'1567',' Piro','Y',226),
 (1568,'1568',' Charpokhri','Y',226),
 (1569,'1569',' Garhani','Y',226),
 (1570,'1570',' Agiaon','Y',226),
 (1571,'1571',' Tarari','Y',226),
 (1572,'1572',' Sahar','Y',226),
 (1573,'1573',' Buxar *','Y',227),
 (1574,'1574',' Simri','Y',227),
 (1575,'1575',' Chakki','Y',227),
 (1576,'1576',' Barhampur','Y',227),
 (1577,'1577',' Chaugain','Y',227),
 (1578,'1578',' Kesath ','Y',227),
 (1579,'1579',' Dumraon ','Y',227),
 (1580,'1580',' Buxar','Y',227),
 (1581,'1581',' Chausa','Y',227),
 (1582,'1582',' Rajpur','Y',227),
 (1583,'1583',' Itarhi','Y',227),
 (1584,'1584',' Nawanagar','Y',227),
 (1585,'1585',' Kaimur (Bhabua) *','Y',228),
 (1586,'1586',' Ramgarh','Y',228),
 (1587,'1587',' Noawan','Y',228),
 (1588,'1588',' Kudra','Y',228),
 (1589,'1589',' Mohania','Y',228),
 (1590,'1590',' Durgawati','Y',228),
 (1591,'1591',' Chand','Y',228),
 (1592,'1592',' Chainpur','Y',228),
 (1593,'1593',' Bhabua','Y',228),
 (1594,'1594',' Rampur','Y',228),
 (1595,'1595',' Bhagwanpur','Y',228),
 (1596,'1596',' Adhaura','Y',228),
 (1597,'1597',' Rohtas','Y',229),
 (1598,'1598',' Kochas','Y',229),
 (1599,'1599',' Dinara','Y',229),
 (1600,'1600',' Dawath','Y',229),
 (1601,'1601',' Suryapura','Y',229),
 (1602,'1602',' Bikramganj','Y',229),
 (1603,'1603',' Karakat','Y',229),
 (1604,'1604',' Nasriganj','Y',229),
 (1605,'1605',' Rajpur','Y',229),
 (1606,'1606',' Sanjhauli','Y',229),
 (1607,'1607',' Nokha','Y',229),
 (1608,'1608',' Kargahar','Y',229),
 (1609,'1609',' Chenari','Y',229),
 (1610,'1610',' Nauhatta','Y',229),
 (1611,'1611',' Sheosagar','Y',229),
 (1612,'1612',' Sasaram','Y',229),
 (1613,'1613',' Akorhi Gola','Y',229),
 (1614,'1614',' Dehri','Y',229),
 (1615,'1615',' Tilouthu','Y',229),
 (1616,'1616',' Rohtas','Y',229),
 (1617,'1617',' Jehanabad ','Y',230),
 (1618,'1618',' Arwal','Y',230),
 (1619,'1619',' Kaler','Y',230),
 (1620,'1620',' Karpi','Y',230),
 (1621,'1621',' Sonbhadra Banshi','Y',230),
 (1622,'1622',' Suryapur Kurtha','Y',230),
 (1623,'1623',' Ratni Faridpur','Y',230),
 (1624,'1624',' Jehanabad','Y',230),
 (1625,'1625',' Kako','Y',230),
 (1626,'1626',' Modanganj','Y',230),
 (1627,'1627',' Ghoshi','Y',230),
 (1628,'1628',' Makhdumpur','Y',230),
 (1629,'1629',' Hulasganj','Y',230),
 (1630,'1630',' Aurangabad','Y',231),
 (1631,'1631',' Daudnagar','Y',231),
 (1632,'1632',' Haspura','Y',231),
 (1633,'1633',' Goh','Y',231),
 (1634,'1634',' Rafiganj','Y',231),
 (1635,'1635',' Obra','Y',231),
 (1636,'1636',' Aurangabad','Y',231),
 (1637,'1637',' Barun','Y',231),
 (1638,'1638',' Nabinagar','Y',231),
 (1639,'1639',' Kutumba','Y',231),
 (1640,'1640',' Deo','Y',231),
 (1641,'1641',' Madanpur','Y',231),
 (1642,'1642',' Gaya','Y',232),
 (1643,'1643',' Konch','Y',232),
 (1644,'1644',' Tikari ','Y',232),
 (1645,'1645',' Belaganj','Y',232),
 (1646,'1646',' Khizirsarai','Y',232),
 (1647,'1647',' Neem Chak Bathani','Y',232),
 (1648,'1648',' Muhra','Y',232),
 (1649,'1649',' Atri','Y',232),
 (1650,'1650',' Manpur','Y',232),
 (1651,'1651',' Gaya','Y',232),
 (1652,'1652',' Paraiya','Y',232),
 (1653,'1653',' Guraru','Y',232),
 (1654,'1654',' Gurua','Y',232),
 (1655,'1655',' Amas','Y',232),
 (1656,'1656',' Banke Bazar','Y',232),
 (1657,'1657',' Imamganj','Y',232),
 (1658,'1658',' Dumaria','Y',232),
 (1659,'1659',' Sherghati','Y',232),
 (1660,'1660',' Dobhi','Y',232),
 (1661,'1661',' Bodh Gaya','Y',232),
 (1662,'1662',' Tan Kuppa','Y',232),
 (1663,'1663',' Wazirganj','Y',232),
 (1664,'1664',' Fatehpur','Y',232),
 (1665,'1665',' Mohanpur','Y',232),
 (1666,'1666',' Barachatti','Y',232),
 (1667,'1667',' Nawada','Y',233),
 (1668,'1668',' Nardiganj','Y',233),
 (1669,'1669',' Nawada','Y',233),
 (1670,'1670',' Warisaliganj','Y',233),
 (1671,'1671',' Kashi Chak','Y',233),
 (1672,'1672',' Pakribarawan','Y',233),
 (1673,'1673',' Kawakol','Y',233),
 (1674,'1674',' Roh','Y',233),
 (1675,'1675',' Govindpur','Y',233),
 (1676,'1676',' Akbarpur','Y',233),
 (1677,'1677',' Hisua','Y',233),
 (1678,'1678',' Narhat','Y',233),
 (1679,'1679',' Meskaur','Y',233),
 (1680,'1680',' Sirdala','Y',233),
 (1681,'1681',' Rajauli','Y',233),
 (1682,'1682',' Jamui *','Y',234),
 (1683,'1683',' Islamnagar Aliganj','Y',234),
 (1684,'1684',' Sikandra','Y',234),
 (1685,'1685',' Jamui','Y',234),
 (1686,'1686',' Barhat','Y',234),
 (1687,'1687',' Lakshmipur','Y',234),
 (1688,'1688',' Jhajha','Y',234),
 (1689,'1689',' Gidhaur','Y',234),
 (1690,'1690',' Khaira','Y',234),
 (1691,'1691',' Sono','Y',234),
 (1692,'1692',' Chakai','Y',234),
 (1693,'1693',' SIKKIM','Y',235),
 (1694,'1694',' North ','Y',236),
 (1695,'1695',' Chungthang','Y',236),
 (1696,'1696',' Mangan','Y',236),
 (1697,'1697',' West','Y',237),
 (1698,'1698',' Gyalshing','Y',237),
 (1699,'1699',' Soreng','Y',237),
 (1700,'1700',' South','Y',238),
 (1701,'1701',' Namchi','Y',238),
 (1702,'1702',' Ravong','Y',238),
 (1703,'1703',' East','Y',239),
 (1704,'1704',' Gangtok','Y',239),
 (1705,'1705',' Pakyong','Y',239),
 (1706,'1706',' Rongli','Y',239),
 (1707,'1707',' ARUNACHAL PRADESH','Y',240),
 (1708,'1708',' Tawang','Y',241),
 (1709,'1709',' Zemithang Circle','Y',241),
 (1710,'1710',' Lumla Circle','Y',241),
 (1711,'1711',' Dudunghar Circle','Y',241),
 (1712,'1712',' Tawang Circle','Y',241),
 (1713,'1713',' Jang Circle','Y',241),
 (1714,'1714',' Mukto Circle','Y',241),
 (1715,'1715',' Thingbu Circle','Y',241),
 (1716,'1716',' West Kameng','Y',242),
 (1717,'1717',' Dirang Circle','Y',242),
 (1718,'1718',' Nafra Circle','Y',242),
 (1719,'1719',' Bomdila Circle','Y',242),
 (1720,'1720',' Kalaktang Circle','Y',242),
 (1721,'1721',' Rupa Circle','Y',242),
 (1722,'1722',' Singchung Circle','Y',242),
 (1723,'1723',' Jamiri Circle','Y',242),
 (1724,'1724',' Thrizino Circle','Y',242),
 (1725,'1725',' Bhalukpong Circle','Y',242),
 (1726,'1726',' Balemu Circle','Y',242),
 (1727,'1727',' East Kameng','Y',243),
 (1728,'1728',' Seijosa Circle','Y',243),
 (1729,'1729',' Pakke Kessang Circle','Y',243),
 (1730,'1730',' Richukrong Circle','Y',243),
 (1731,'1731',' Seppa Circle','Y',243),
 (1732,'1732',' Lada Circle','Y',243),
 (1733,'1733',' Bameng Circle','Y',243),
 (1734,'1734',' Pipu Circle','Y',243),
 (1735,'1735',' Khenewa Circle','Y',243),
 (1736,'1736',' Chayangtajo Circle','Y',243),
 (1737,'1737',' Sawa Circle','Y',243),
 (1738,'1738',' Papum Pare *','Y',244),
 (1739,'1739',' Balijan Circle','Y',244),
 (1740,'1740',' Itanagar Circle','Y',244),
 (1741,'1741',' Naharlagun Circle','Y',244),
 (1742,'1742',' Doimukh Circle','Y',244),
 (1743,'1743',' Toru Circle','Y',244),
 (1744,'1744',' Sagalee Circle','Y',244),
 (1745,'1745',' Leporiang Circle','Y',244),
 (1746,'1746',' Mengio Circle','Y',244),
 (1747,'1747',' Kimin Circle','Y',244),
 (1748,'1748',' Lower Subansiri','Y',245),
 (1749,'1749',' Ziro Circle','Y',245),
 (1750,'1750',' Yachuli Circle','Y',245),
 (1751,'1751',' Pistana Circle','Y',245),
 (1752,'1752',' Palin Circle','Y',245),
 (1753,'1753',' Yangte Circle','Y',245),
 (1754,'1754',' Sangram Circle','Y',245),
 (1755,'1755',' Nyapin Circle','Y',245),
 (1756,'1756',' Koloriang Circle','Y',245),
 (1757,'1757',' Chambang Circle','Y',245),
 (1758,'1758',' Sarli Circle','Y',245),
 (1759,'1759',' Parsi-Parlo Circle','Y',245),
 (1760,'1760',' Damin Circle','Y',245),
 (1761,'1761',' Longding Koling Circle','Y',245),
 (1762,'1762',' Tali Circle','Y',245),
 (1763,'1763',' Kamporijo Circle','Y',245),
 (1764,'1764',' Dollungmukh Circle','Y',245),
 (1765,'1765',' Raga Circle','Y',245),
 (1766,'1766',' Upper Subansiri','Y',246),
 (1767,'1767',' Taksing Circle','Y',246),
 (1768,'1768',' Limeking Circle','Y',246),
 (1769,'1769',' Nacho Circle','Y',246),
 (1770,'1770',' Siyum Circle','Y',246),
 (1771,'1771',' Taliha Circle','Y',246),
 (1772,'1772',' Payeng Circle','Y',246),
 (1773,'1773',' Giba Circle','Y',246),
 (1774,'1774',' Daporijo Circle','Y',246),
 (1775,'1775',' Puchi Geko Circle','Y',246),
 (1776,'1776',' Dumporijo Circle','Y',246),
 (1777,'1777',' Baririjo Circle','Y',246),
 (1778,'1778',' West Siang','Y',247),
 (1779,'1779',' Mechuka Circle','Y',247),
 (1780,'1780',' Monigong Circle','Y',247),
 (1781,'1781',' Pidi Circle','Y',247),
 (1782,'1782',' Payum Circle','Y',247),
 (1783,'1783',' Tato Circle','Y',247),
 (1784,'1784',' Kaying Circle','Y',247),
 (1785,'1785',' Darak Circle','Y',247),
 (1786,'1786',' Kamba Circle','Y',247),
 (1787,'1787',' Rumgong Circle','Y',247),
 (1788,'1788',' Jomlo Mobuk Circle','Y',247),
 (1789,'1789',' Liromoba Circle','Y',247),
 (1790,'1790',' Yomcha Circle','Y',247),
 (1791,'1791',' Along Circle','Y',247),
 (1792,'1792',' Tirbin Circle','Y',247),
 (1793,'1793',' Basar Circle','Y',247),
 (1794,'1794',' Daring Circle','Y',247),
 (1795,'1795',' Gensi Circle','Y',247),
 (1796,'1796',' Likabali Circle','Y',247),
 (1797,'1797',' Kangku Circle','Y',247),
 (1798,'1798',' Bagra Circle','Y',247),
 (1799,'1799',' East Siang','Y',248),
 (1800,'1800',' Boleng Circle','Y',248),
 (1801,'1801',' Riga Circle','Y',248),
 (1802,'1802',' Pangin Circle','Y',248),
 (1803,'1803',' Rebo-Perging Circle','Y',248),
 (1804,'1804',' Koyu Circle','Y',248),
 (1805,'1805',' Nari Circle','Y',248),
 (1806,'1806',' New Seren Circle','Y',248),
 (1807,'1807',' Bilat Circle','Y',248),
 (1808,'1808',' Ruksin Circle','Y',248),
 (1809,'1809',' Sille-Oyan Circle','Y',248),
 (1810,'1810',' Pasighat Circle','Y',248),
 (1811,'1811',' Mebo Circle','Y',248),
 (1812,'1812',' Upper Siang *','Y',249),
 (1813,'1813',' Tuting Circle','Y',249),
 (1814,'1814',' Migging Circle','Y',249),
 (1815,'1815',' Palling Circle','Y',249),
 (1816,'1816',' Gelling Circle','Y',249),
 (1817,'1817',' Singa Circle','Y',249),
 (1818,'1818',' Yingwong Circle','Y',249),
 (1819,'1819',' Jengging Circle','Y',249),
 (1820,'1820',' Geku Circle','Y',249),
 (1821,'1821',' Mariyang Circle','Y',249),
 (1822,'1822',' Katan Circle ','Y',249);
INSERT INTO `taluka` (`id`,`code`,`name`,`active`,`district_id`) VALUES 
 (1823,'1823',' Dibang Valley','Y',250),
 (1824,'1824',' Mipi Circle','Y',250),
 (1825,'1825',' Anini Circle ','Y',250),
 (1826,'1826',' Etalin Circle','Y',250),
 (1827,'1827',' Anelih Circle','Y',250),
 (1828,'1828',' Koronli Circle','Y',250),
 (1829,'1829',' Hunli Circle','Y',250),
 (1830,'1830',' Desali Circle','Y',250),
 (1831,'1831',' Roing Circle','Y',250),
 (1832,'1832',' Dambuk Circle','Y',250),
 (1833,'1833',' Koronu Circle','Y',250),
 (1834,'1834',' Lohit','Y',251),
 (1835,'1835',' Sunpura Circle','Y',251),
 (1836,'1836',' Tezu Circle','Y',251),
 (1837,'1837',' Hayuliang Circle','Y',251),
 (1838,'1838',' Manchal Circle','Y',251),
 (1839,'1839',' Goiliang Circle','Y',251),
 (1840,'1840',' Chaglagam Circle','Y',251),
 (1841,'1841',' Kibithoo Circle','Y',251),
 (1842,'1842',' Walong Circle','Y',251),
 (1843,'1843',' Hawai Circle','Y',251),
 (1844,'1844',' Wakro Circle','Y',251),
 (1845,'1845',' Chowkham Circle','Y',251),
 (1846,'1846',' Namsai Circle','Y',251),
 (1847,'1847',' Piyong Circle','Y',251),
 (1848,'1848',' Mahadevpur Circle','Y',251),
 (1849,'1849',' Changlang','Y',252),
 (1850,'1850',' Khimiyong Circle','Y',252),
 (1851,'1851',' Changlang Circle','Y',252),
 (1852,'1852',' Namtok Circle','Y',252),
 (1853,'1853',' Manmao Circle','Y',252),
 (1854,'1854',' Nampong Circle','Y',252),
 (1855,'1855',' Jairampur Circle','Y',252),
 (1856,'1856',' Vijoynagar Circle','Y',252),
 (1857,'1857',' Miao Circle','Y',252),
 (1858,'1858',' Kharsang Circle','Y',252),
 (1859,'1859',' Diyun Circle','Y',252),
 (1860,'1860',' Bordumsa Circle','Y',252),
 (1861,'1861',' Tirap','Y',253),
 (1862,'1862',' Namsang Circle','Y',253),
 (1863,'1863',' Khonsa Circle','Y',253),
 (1864,'1864',' Kanubari Circle','Y',253),
 (1865,'1865',' Longding Circle','Y',253),
 (1866,'1866',' Pumao Circle','Y',253),
 (1867,'1867',' Pangchao Circle','Y',253),
 (1868,'1868',' Wakka Circle','Y',253),
 (1869,'1869',' Laju Circle','Y',253),
 (1870,'1870',' NAGALAND','Y',254),
 (1871,'1871',' Mon','Y',255),
 (1872,'1872',' Naginimora','Y',255),
 (1873,'1873',' Tizit','Y',255),
 (1874,'1874',' Hunta','Y',255),
 (1875,'1875',' Shangyu','Y',255),
 (1876,'1876',' Mon Sadar','Y',255),
 (1877,'1877',' Wakching','Y',255),
 (1878,'1878',' Aboi','Y',255),
 (1879,'1879',' Longshen','Y',255),
 (1880,'1880',' Phomching','Y',255),
 (1881,'1881',' Chen','Y',255),
 (1882,'1882',' Longching','Y',255),
 (1883,'1883',' Mopong','Y',255),
 (1884,'1884',' Tobu','Y',255),
 (1885,'1885',' Monyakshu','Y',255),
 (1886,'1886',' Tuensang','Y',256),
 (1887,'1887',' Tamlu','Y',256),
 (1888,'1888',' Yongya','Y',256),
 (1889,'1889',' Longleng','Y',256),
 (1890,'1890',' Noksen','Y',256),
 (1891,'1891',' Chare','Y',256),
 (1892,'1892',' Longkhim','Y',256),
 (1893,'1893',' Tuensang Sadar','Y',256),
 (1894,'1894',' Noklak','Y',256),
 (1895,'1895',' Panso','Y',256),
 (1896,'1896',' Shamator','Y',256),
 (1897,'1897',' Tsurungtho','Y',256),
 (1898,'1898',' Chessore','Y',256),
 (1899,'1899',' Seyochung','Y',256),
 (1900,'1900',' Amahator','Y',256),
 (1901,'1901',' Kiphire Sadar','Y',256),
 (1902,'1902',' Thonoknyu','Y',256),
 (1903,'1903',' Kiusam','Y',256),
 (1904,'1904',' Sitimi','Y',256),
 (1905,'1905',' Longmatra','Y',256),
 (1906,'1906',' Pungro','Y',256),
 (1907,'1907',' Mokokchung','Y',257),
 (1908,'1908',' Longchem','Y',257),
 (1909,'1909',' Alongkima','Y',257),
 (1910,'1910',' Tuli','Y',257),
 (1911,'1911',' Changtongya','Y',257),
 (1912,'1912',' Chuchuyimlang','Y',257),
 (1913,'1913',' Kubolong','Y',257),
 (1914,'1914',' Mangkolemba','Y',257),
 (1915,'1915',' Ongpangkong','Y',257),
 (1916,'1916',' Zunheboto','Y',258),
 (1917,'1917',' V.K.','Y',258),
 (1918,'1918',' Akuluto','Y',258),
 (1919,'1919',' Suruhoto','Y',258),
 (1920,'1920',' Asuto','Y',258),
 (1921,'1921',' Aghunato','Y',258),
 (1922,'1922',' Zunheboto Sadar','Y',258),
 (1923,'1923',' Atoizu','Y',258),
 (1924,'1924',' Pughoboto','Y',258),
 (1925,'1925',' Ghatashi','Y',258),
 (1926,'1926',' Satakha','Y',258),
 (1927,'1927',' Satoi','Y',258),
 (1928,'1928',' Wokha','Y',259),
 (1929,'1929',' Changpang','Y',259),
 (1930,'1930',' Aitepyong','Y',259),
 (1931,'1931',' Bhandari','Y',259),
 (1932,'1932',' Baghty','Y',259),
 (1933,'1933',' Sungro','Y',259),
 (1934,'1934',' Sanis','Y',259),
 (1935,'1935',' Lotsu','Y',259),
 (1936,'1936',' Ralan','Y',259),
 (1937,'1937',' Wozhuro','Y',259),
 (1938,'1938',' Wokha Sadar','Y',259),
 (1939,'1939',' Chukitong','Y',259),
 (1940,'1940',' Dimapur *','Y',260),
 (1941,'1941',' Niuland','Y',260),
 (1942,'1942',' Kuhoboto','Y',260),
 (1943,'1943',' Nihokhu','Y',260),
 (1944,'1944',' Dimapur Sadar','Y',260),
 (1945,'1945',' Chumukedima','Y',260),
 (1946,'1946',' Dhansiripar','Y',260),
 (1947,'1947',' Medziphema','Y',260),
 (1948,'1948',' Kohima','Y',261),
 (1949,'1949',' Tseminyu','Y',261),
 (1950,'1950',' Chiephobozou','Y',261),
 (1951,'1951',' Kezocha','Y',261),
 (1952,'1952',' Jakhama','Y',261),
 (1953,'1953',' Kohima Sadar','Y',261),
 (1954,'1954',' Sechu','Y',261),
 (1955,'1955',' Ngwalwa','Y',261),
 (1956,'1956',' Jalukie','Y',261),
 (1957,'1957',' Athibung','Y',261),
 (1958,'1958',' Nsong','Y',261),
 (1959,'1959',' Tening','Y',261),
 (1960,'1960',' Peren','Y',261),
 (1961,'1961',' Phek','Y',262),
 (1962,'1962',' Sekruzu','Y',262),
 (1963,'1963',' Phek Sadar','Y',262),
 (1964,'1964',' Meluri','Y',262),
 (1965,'1965',' Phokhungri','Y',262),
 (1966,'1966',' Chazouba','Y',262),
 (1967,'1967',' Chetheba','Y',262),
 (1968,'1968',' Sakraba','Y',262),
 (1969,'1969',' Pfutsero','Y',262),
 (1970,'1970',' Khezhakeno','Y',262),
 (1971,'1971',' Chizami','Y',262),
 (1972,'1972',' MANIPUR','Y',263),
 (1973,'1973',' Senapati','Y',264),
 (1974,'1974',' Mao-Maram Sub-Division','Y',264),
 (1975,'1975',' Paomata Sub-Division','Y',264),
 (1976,'1976',' Purul Sub-Division','Y',264),
 (1977,'1977',' Sadar Hills West Sub-Division','Y',264),
 (1978,'1978',' Saitu Gamphazol Sub-Division ','Y',264),
 (1979,'1979',' Sadar Hills East Sub-Division','Y',264),
 (1980,'1980',' Tamenglong','Y',265),
 (1981,'1981',' Tamenglong West Sub-Division','Y',265),
 (1982,'1982',' Tamenglong North Sub-Division','Y',265),
 (1983,'1983',' Tamenglong Sub-Division ','Y',265),
 (1984,'1984',' Nungba Sub-Division','Y',265),
 (1985,'1985',' Churachandpur','Y',266),
 (1986,'1986',' Tipaimukh Sub-Division','Y',266),
 (1987,'1987',' Thanlon Sub-Division','Y',266),
 (1988,'1988',' Churachandpur North Sub-Div.','Y',266),
 (1989,'1989',' Churachandpur  Sub-Division','Y',266),
 (1990,'1990',' Singngat Sub-Division ','Y',266),
 (1991,'1991',' Bishnupur','Y',267),
 (1992,'1992',' Nambol Sub-Division','Y',267),
 (1993,'1993',' Bishnupur Sub-Division','Y',267),
 (1994,'1994',' Moirang Sub-Division ','Y',267),
 (1995,'1995',' Thoubal','Y',268),
 (1996,'1996',' Lilong Sub-Division','Y',268),
 (1997,'1997',' Thoubal Sub-Division','Y',268),
 (1998,'1998',' Kakching Sub-Div.','Y',268),
 (1999,'1999',' Imphal West','Y',269),
 (2000,'2000',' Lamshang Sub-Division ','Y',269),
 (2001,'2001',' Patsoi Sub-Division','Y',269),
 (2002,'2002',' Lamphelpat Sub-Division','Y',269),
 (2003,'2003',' Wangoi Sub-Division','Y',269),
 (2004,'2004',' Imphal East *','Y',270),
 (2005,'2005',' Jiribam  Sub-Division','Y',270),
 (2006,'2006',' Sawombung Sub-Division','Y',270),
 (2007,'2007',' Porompat Sub-Division','Y',270),
 (2008,'2008',' Keirao Bitra Sub-Division','Y',270),
 (2009,'2009',' Ukhrul','Y',271),
 (2010,'2010',' Ukhrul North  Sub-Division','Y',271),
 (2011,'2011',' Ukhrul Central Sub-Division','Y',271),
 (2012,'2012',' Kamjong Chassad Sub-Div.','Y',271),
 (2013,'2013',' Phungyar Phaisat Sub-Division','Y',271),
 (2014,'2014',' Ukhrul South Sub-Division','Y',271),
 (2015,'2015',' Chandel','Y',272),
 (2016,'2016',' Machi  Sub-Division','Y',272),
 (2017,'2017',' Tengnoupal Sub-Division','Y',272),
 (2018,'2018',' Chandel Sub-Div.','Y',272),
 (2019,'2019',' Chakpikarong Sub-Division','Y',272),
 (2020,'2020',' MIZORAM','Y',273),
 (2021,'2021',' Mamit *','Y',274),
 (2022,'2022',' Zawlnuam','Y',274),
 (2023,'2023',' West Phaileng','Y',274),
 (2024,'2024',' Reiek','Y',274),
 (2025,'2025',' Kolasib *','Y',275),
 (2026,'2026',' North Thingdawl','Y',275),
 (2027,'2027',' Aizawl','Y',276),
 (2028,'2028',' Darlawn','Y',276),
 (2029,'2029',' Phullen','Y',276),
 (2030,'2030',' Thingsulthliah','Y',276),
 (2031,'2031',' Tlangnuam','Y',276),
 (2032,'2032',' Aibawk','Y',276),
 (2033,'2033',' Champhai *','Y',277),
 (2034,'2034',' Ngopa','Y',277),
 (2035,'2035',' Khawzawl','Y',277),
 (2036,'2036',' Khawbung','Y',277),
 (2037,'2037',' Serchhip *','Y',278),
 (2038,'2038',' Serchhip','Y',278),
 (2039,'2039',' East Lungdar','Y',278),
 (2040,'2040',' Lunglei','Y',279),
 (2041,'2041',' West Bunghmun','Y',279),
 (2042,'2042',' Lungsen','Y',279),
 (2043,'2043',' Lunglei','Y',279),
 (2044,'2044',' Hnahthial','Y',279),
 (2045,'2045',' Lawngtlai','Y',280),
 (2046,'2046',' Chawngte','Y',280),
 (2047,'2047',' Lawngtlai','Y',280),
 (2048,'2048',' Saiha *','Y',281),
 (2049,'2049',' Sangau','Y',281),
 (2050,'2050',' Tuipang','Y',281),
 (2051,'2051',' TRIPURA','Y',282),
 (2052,'2052',' West Tripura ','Y',283),
 (2053,'2053',' Mohanpur','Y',283),
 (2054,'2054',' Hezamara','Y',283),
 (2055,'2055',' Pabmabil','Y',283),
 (2056,'2056',' Khowai','Y',283),
 (2057,'2057',' Tulashikhar','Y',283),
 (2058,'2058',' Kalyanpur','Y',283),
 (2059,'2059',' Teliamura','Y',283),
 (2060,'2060',' Mandai','Y',283),
 (2061,'2061',' Jirania','Y',283),
 (2062,'2062',' Dukli','Y',283),
 (2063,'2063',' Jampuijala','Y',283),
 (2064,'2064',' Bishalgarh','Y',283),
 (2065,'2065',' Boxanagar','Y',283),
 (2066,'2066',' Melaghar','Y',283),
 (2067,'2067',' Kathalia','Y',283),
 (2068,'2068',' South Tripura ','Y',284),
 (2069,'2069',' Killa','Y',284),
 (2070,'2070',' Amarpur','Y',284),
 (2071,'2071',' Matarbari','Y',284),
 (2072,'2072',' Kakraban','Y',284),
 (2073,'2073',' Rajnagar','Y',284),
 (2074,'2074',' Hrishyamukh','Y',284),
 (2075,'2075',' Bagafa','Y',284),
 (2076,'2076',' Karbuk','Y',284),
 (2077,'2077',' Rupaichhari','Y',284),
 (2078,'2078',' Satchand','Y',284),
 (2079,'2079',' Dhalai  *','Y',285),
 (2080,'2080',' Salema','Y',285),
 (2081,'2081',' Manu','Y',285),
 (2082,'2082',' Ambassa','Y',285),
 (2083,'2083',' Chhamanu','Y',285),
 (2084,'2084',' Dumburnagar','Y',285),
 (2085,'2085',' North Tripura ','Y',286),
 (2086,'2086',' Gournagar','Y',286),
 (2087,'2087',' Kadamtala','Y',286),
 (2088,'2088',' Panisagar','Y',286),
 (2089,'2089',' Damchhara','Y',286),
 (2090,'2090',' Pencharthal','Y',286),
 (2091,'2091',' Kumarghat','Y',286),
 (2092,'2092',' Dasda','Y',286),
 (2093,'2093',' Jampuii hills','Y',286),
 (2094,'2094',' MEGHALAYA','Y',287),
 (2095,'2095',' West Garo Hills','Y',288),
 (2096,'2096',' Selsella','Y',288),
 (2097,'2097',' Dadenggiri','Y',288),
 (2098,'2098',' Tikrikilla','Y',288),
 (2099,'2099',' Rongram','Y',288),
 (2100,'2100',' Betasing','Y',288),
 (2101,'2101',' Zikzak','Y',288),
 (2102,'2102',' Dalu','Y',288),
 (2103,'2103',' East Garo Hills','Y',289),
 (2104,'2104',' Resubelpara','Y',289),
 (2105,'2105',' Dambo Rongjeng','Y',289),
 (2106,'2106',' Songsak','Y',289),
 (2107,'2107',' Samanda','Y',289),
 (2108,'2108',' South Garo Hills *','Y',290),
 (2109,'2109',' Chokpot','Y',290),
 (2110,'2110',' Baghmara','Y',290),
 (2111,'2111',' Rongara','Y',290),
 (2112,'2112',' West Khasi Hills','Y',291),
 (2113,'2113',' Mawshynrut','Y',291),
 (2114,'2114',' Nongstoin','Y',291),
 (2115,'2115',' Mairang','Y',291),
 (2116,'2116',' Ranikor','Y',291),
 (2117,'2117',' Mawkyrwat','Y',291),
 (2118,'2118',' Ri Bhoi  *','Y',292),
 (2119,'2119',' Umling','Y',292),
 (2120,'2120',' Umsning','Y',292),
 (2121,'2121',' East Khasi Hills','Y',293),
 (2122,'2122',' Mawphlang','Y',293),
 (2123,'2123',' Mylliem','Y',293),
 (2124,'2124',' Mawryngkneng','Y',293),
 (2125,'2125',' Mawkynrew','Y',293),
 (2126,'2126',' Mawsynram','Y',293),
 (2127,'2127',' Shella Bholaganj','Y',293),
 (2128,'2128',' Pynursla','Y',293),
 (2129,'2129',' Jaintia Hills','Y',294),
 (2130,'2130',' Thadlaskein','Y',294),
 (2131,'2131',' Laskein','Y',294),
 (2132,'2132',' Amlarem','Y',294),
 (2133,'2133',' Khliehriat','Y',294),
 (2134,'2134',' ASSAM','Y',295),
 (2135,'2135',' Kokrajhar','Y',296),
 (2136,'2136',' Gossaigaon','Y',296),
 (2137,'2137',' Bhowraguri','Y',296),
 (2138,'2138',' Dotoma','Y',296),
 (2139,'2139',' Kokrajhar','Y',296),
 (2140,'2140',' Sidli (PT-I)','Y',296),
 (2141,'2141',' Dhubri','Y',297),
 (2142,'2142',' Agamoni','Y',297),
 (2143,'2143',' Golokganj','Y',297),
 (2144,'2144',' Dhubri','Y',297),
 (2145,'2145',' BagribariI','Y',297),
 (2146,'2146',' Bilasipara','Y',297),
 (2147,'2147',' Chapar','Y',297),
 (2148,'2148',' South Salmara','Y',297),
 (2149,'2149',' Mankachar','Y',297),
 (2150,'2150',' Goalpara','Y',298),
 (2151,'2151',' Lakhipur','Y',298),
 (2152,'2152',' Balijana','Y',298),
 (2153,'2153',' Matia','Y',298),
 (2154,'2154',' Dudhnai','Y',298),
 (2155,'2155',' Rangjuli','Y',298),
 (2156,'2156',' Bongaigaon','Y',299),
 (2157,'2157',' Sidli (PT-II)','Y',299),
 (2158,'2158',' Bongaigaon','Y',299),
 (2159,'2159',' Boitamari','Y',299),
 (2160,'2160',' Srijangram','Y',299),
 (2161,'2161',' Bijni','Y',299),
 (2162,'2162',' Barpeta','Y',300),
 (2163,'2163',' Barnagar','Y',300),
 (2164,'2164',' Kalgachia','Y',300),
 (2165,'2165',' Baghbor','Y',300),
 (2166,'2166',' Barpeta','Y',300),
 (2167,'2167',' Sarthebari','Y',300),
 (2168,'2168',' Bajali','Y',300),
 (2169,'2169',' Sarupeta','Y',300),
 (2170,'2170',' Jalah','Y',300),
 (2171,'2171',' Kamrup','Y',301),
 (2172,'2172',' Goreswar','Y',301),
 (2173,'2173',' Rangia','Y',301),
 (2174,'2174',' Kamalpur ','Y',301),
 (2175,'2175',' Kamalpur ','Y',301),
 (2176,'2176',' Hajo','Y',301),
 (2177,'2177',' Chhaygaon','Y',301),
 (2178,'2178',' Chamaria','Y',301),
 (2179,'2179',' Nagarbera','Y',301),
 (2180,'2180',' Boko','Y',301),
 (2181,'2181',' Palasbari','Y',301),
 (2182,'2182',' Guwahati','Y',301),
 (2183,'2183',' North Guwahati','Y',301),
 (2184,'2184',' Dispur','Y',301),
 (2185,'2185',' Sonapur','Y',301),
 (2186,'2186',' Chandrapur','Y',301),
 (2187,'2187',' Nalbari','Y',302),
 (2188,'2188',' Baska','Y',302),
 (2189,'2189',' Barama','Y',302),
 (2190,'2190',' Tihu','Y',302),
 (2191,'2191',' Pachim Nalbari','Y',302),
 (2192,'2192',' Barkhetri','Y',302),
 (2193,'2193',' Barbhag','Y',302),
 (2194,'2194',' Nalbari','Y',302),
 (2195,'2195',' Ghograpar','Y',302),
 (2196,'2196',' Tamulpur','Y',302),
 (2197,'2197',' Darrang','Y',303),
 (2198,'2198',' Harisinga','Y',303),
 (2199,'2199',' Khoirabari','Y',303),
 (2200,'2200',' Pathorighat','Y',303),
 (2201,'2201',' Sipajhar','Y',303),
 (2202,'2202',' Mangaldoi','Y',303),
 (2203,'2203',' Kalajgaon','Y',303),
 (2204,'2204',' Dalgaon','Y',303),
 (2205,'2205',' Dalgaon ','Y',303),
 (2206,'2206',' Udalguri','Y',303),
 (2207,'2207',' Majbat','Y',303),
 (2208,'2208',' Marigaon','Y',304),
 (2209,'2209',' Mayong','Y',304),
 (2210,'2210',' Bhuragaon','Y',304),
 (2211,'2211',' Laharighat','Y',304),
 (2212,'2212',' Marigaon','Y',304),
 (2213,'2213',' Mikirbheta','Y',304),
 (2214,'2214',' Nagaon','Y',305),
 (2215,'2215',' Koliabor','Y',305),
 (2216,'2216',' Samaguri ','Y',305),
 (2217,'2217',' Samaguri','Y',305),
 (2218,'2218',' Rupahi ','Y',305),
 (2219,'2219',' Rupahi ','Y',305),
 (2220,'2220',' Dhing','Y',305),
 (2221,'2221',' Nagaon','Y',305),
 (2222,'2222',' Raha','Y',305),
 (2223,'2223',' Kampur','Y',305),
 (2224,'2224',' Hojai','Y',305),
 (2225,'2225',' Lanka','Y',305),
 (2226,'2226',' Sonitpur','Y',306),
 (2227,'2227',' Dhekiajuli','Y',306),
 (2228,'2228',' Chariduar','Y',306),
 (2229,'2229',' Tezpur','Y',306),
 (2230,'2230',' Na-Duar','Y',306),
 (2231,'2231',' Biswanath','Y',306),
 (2232,'2232',' Helem','Y',306),
 (2233,'2233',' Gohpur','Y',306),
 (2234,'2234',' Lakhimpur','Y',307),
 (2235,'2235',' Narayanpur','Y',307),
 (2236,'2236',' Bihpuraia','Y',307),
 (2237,'2237',' Naobaicha','Y',307),
 (2238,'2238',' Kadam','Y',307),
 (2239,'2239',' North Lakhimpur','Y',307),
 (2240,'2240',' Dhakuakhana (PT-I)','Y',307),
 (2241,'2241',' Subansiri (PT-I)','Y',307),
 (2242,'2242',' Dhemaji','Y',308),
 (2243,'2243',' Subansiri (PT-II)','Y',308),
 (2244,'2244',' Dhemaji','Y',308),
 (2245,'2245',' Dhakuakhana (PT-II)','Y',308),
 (2246,'2246',' Sissibargaon','Y',308),
 (2247,'2247','  Jonai ','Y',308),
 (2248,'2248',' Tinsukia','Y',309),
 (2249,'2249',' Sadiya','Y',309),
 (2250,'2250',' Doom Dooma','Y',309),
 (2251,'2251',' Tinsukia','Y',309),
 (2252,'2252',' Margherita','Y',309),
 (2253,'2253',' Dibrugarh','Y',310),
 (2254,'2254',' Dibrugarh West','Y',310),
 (2255,'2255',' Dibrugarh East','Y',310),
 (2256,'2256',' Chabua','Y',310),
 (2257,'2257',' Tengakhat','Y',310),
 (2258,'2258',' Moran','Y',310),
 (2259,'2259',' Tingkhong','Y',310),
 (2260,'2260',' Naharkathiya','Y',310),
 (2261,'2261',' Sibsagar','Y',311),
 (2262,'2262',' Dimow','Y',311),
 (2263,'2263',' Sibsagar','Y',311),
 (2264,'2264',' Amguri ','Y',311),
 (2265,'2265',' Amguri ','Y',311),
 (2266,'2266',' Nazira ','Y',311),
 (2267,'2267',' Nazira ','Y',311),
 (2268,'2268',' Sonari ','Y',311),
 (2269,'2269',' Sonari ','Y',311),
 (2270,'2270',' Mahmora','Y',311),
 (2271,'2271',' Jorhat','Y',312),
 (2272,'2272',' Majuli','Y',312),
 (2273,'2273',' Jorhat West','Y',312),
 (2274,'2274',' Jorhat East','Y',312),
 (2275,'2275',' Teok','Y',312),
 (2276,'2276',' Titabar','Y',312),
 (2277,'2277',' Golaghat','Y',313),
 (2278,'2278',' Bokakhat','Y',313),
 (2279,'2279',' Khumtai','Y',313),
 (2280,'2280',' Dergaon','Y',313),
 (2281,'2281',' Golaghat','Y',313),
 (2282,'2282',' Sarupathar','Y',313),
 (2283,'2283',' Karbi Anglong','Y',314),
 (2284,'2284',' Donka','Y',314),
 (2285,'2285',' Diphu ','Y',314),
 (2286,'2286',' Diphu ','Y',314),
 (2287,'2287',' Phuloni ','Y',314),
 (2288,'2288',' Phuloni ','Y',314),
 (2289,'2289',' Silonijan','Y',314),
 (2290,'2290',' North Cachar Hills','Y',315),
 (2291,'2291',' Umrangso','Y',315),
 (2292,'2292',' Haflong','Y',315),
 (2293,'2293',' Mahur','Y',315),
 (2294,'2294',' Maibong','Y',315),
 (2295,'2295',' Cachar','Y',316),
 (2296,'2296',' Katigora','Y',316),
 (2297,'2297',' Silchar','Y',316),
 (2298,'2298',' Udarbond','Y',316),
 (2299,'2299',' Sonai ','Y',316),
 (2300,'2300',' Sonai ','Y',316),
 (2301,'2301',' Lakhipur','Y',316),
 (2302,'2302',' Karimganj','Y',317),
 (2303,'2303',' Karimganj','Y',317),
 (2304,'2304',' Badarpur','Y',317),
 (2305,'2305',' Nilambazar','Y',317),
 (2306,'2306',' Patharkandi','Y',317),
 (2307,'2307',' Ramkrishna Nagar','Y',317),
 (2308,'2308',' Hailakandi','Y',318),
 (2309,'2309',' Algapur','Y',318),
 (2310,'2310',' Hailakandi','Y',318),
 (2311,'2311',' Lala','Y',318),
 (2312,'2312',' Katlichara','Y',318),
 (2313,'2313',' WEST BENGAL','Y',319),
 (2314,'2314',' Darjiling ','Y',320),
 (2315,'2315',' Darjeeling Pulbazar','Y',320),
 (2316,'2316',' Rangli Rangliot','Y',320),
 (2317,'2317',' Kalimpong -I','Y',320),
 (2318,'2318',' Kalimpong - II','Y',320),
 (2319,'2319',' Gorubathan','Y',320),
 (2320,'2320',' Jorebunglow Sukiapokhri','Y',320),
 (2321,'2321',' Mirik','Y',320),
 (2322,'2322',' Kurseong','Y',320),
 (2323,'2323',' Matigara ','Y',320),
 (2324,'2324',' Naxalbari','Y',320),
 (2325,'2325',' Phansidewa','Y',320),
 (2326,'2326',' Kharibari','Y',320),
 (2327,'2327',' Jalpaiguri ','Y',321),
 (2328,'2328',' Rajganj','Y',321),
 (2329,'2329',' Mal','Y',321),
 (2330,'2330',' Matiali','Y',321),
 (2331,'2331',' Nagrakata','Y',321),
 (2332,'2332',' Madarihat','Y',321),
 (2333,'2333',' Kalchini','Y',321),
 (2334,'2334',' Kumargram','Y',321),
 (2335,'2335',' Alipurduar - I','Y',321),
 (2336,'2336',' Alipurduar - II','Y',321),
 (2337,'2337',' Falakata','Y',321),
 (2338,'2338',' Dhupguri','Y',321),
 (2339,'2339',' Maynaguri','Y',321),
 (2340,'2340',' Jalpaiguri','Y',321),
 (2341,'2341',' Koch Bihar ','Y',322),
 (2342,'2342',' Haldibari','Y',322),
 (2343,'2343',' Mekliganj','Y',322),
 (2344,'2344',' Mathabhanga - I','Y',322),
 (2345,'2345',' Mathabhanga - II','Y',322),
 (2346,'2346',' Cooch Behar - I','Y',322),
 (2347,'2347',' Cooch Behar - II','Y',322),
 (2348,'2348',' Tufanganj - I','Y',322),
 (2349,'2349',' Tufanganj - II','Y',322),
 (2350,'2350',' Dinhata - I','Y',322),
 (2351,'2351',' Dinhata - II','Y',322),
 (2352,'2352',' Sitai','Y',322),
 (2353,'2353',' Sitalkuchi','Y',322),
 (2354,'2354',' Uttar Dinajpur','Y',323),
 (2355,'2355',' Chopra','Y',323),
 (2356,'2356',' Islampur','Y',323),
 (2357,'2357',' Goalpokhar - I','Y',323),
 (2358,'2358',' Goalpokhar - II','Y',323),
 (2359,'2359',' Karandighi','Y',323),
 (2360,'2360',' Raiganj','Y',323),
 (2361,'2361',' Hemtabad','Y',323),
 (2362,'2362',' Kaliaganj','Y',323),
 (2363,'2363',' Itahar','Y',323),
 (2364,'2364',' Dakshin Dinajpur *','Y',324),
 (2365,'2365',' Kushmundi','Y',324),
 (2366,'2366',' Gangarampur','Y',324),
 (2367,'2367',' Kumarganj','Y',324),
 (2368,'2368',' Hilli','Y',324),
 (2369,'2369',' Balurghat','Y',324),
 (2370,'2370',' Tapan','Y',324),
 (2371,'2371',' Bansihari','Y',324),
 (2372,'2372',' Harirampur','Y',324),
 (2373,'2373',' Maldah ','Y',325),
 (2374,'2374',' Harishchandrapur - I','Y',325),
 (2375,'2375',' Harishchandrapur - II','Y',325),
 (2376,'2376',' Chanchal - I','Y',325),
 (2377,'2377',' Chanchal - II','Y',325),
 (2378,'2378',' Ratua - I','Y',325),
 (2379,'2379',' Ratua - II','Y',325),
 (2380,'2380',' Gazole','Y',325),
 (2381,'2381',' Bamangola','Y',325),
 (2382,'2382',' Habibpur','Y',325),
 (2383,'2383',' Maldah (old)','Y',325),
 (2384,'2384',' English Bazar','Y',325),
 (2385,'2385',' Manikchak','Y',325),
 (2386,'2386',' Kaliachak - I','Y',325),
 (2387,'2387',' Kaliachak - II','Y',325),
 (2388,'2388',' Kaliachak - III','Y',325),
 (2389,'2389',' Murshidabad ','Y',326),
 (2390,'2390',' Farakka','Y',326),
 (2391,'2391',' Samserganj','Y',326),
 (2392,'2392',' Suti - I','Y',326),
 (2393,'2393',' Suti - II','Y',326),
 (2394,'2394',' Raghunathganj - I','Y',326),
 (2395,'2395',' Raghunathganj - II','Y',326),
 (2396,'2396',' Lalgola','Y',326),
 (2397,'2397',' Sagardighi','Y',326),
 (2398,'2398',' Bhagawangola - I','Y',326),
 (2399,'2399',' Bhagawangola - II','Y',326),
 (2400,'2400',' Raninagar - II','Y',326),
 (2401,'2401',' Jalangi','Y',326),
 (2402,'2402',' Domkal','Y',326),
 (2403,'2403',' Raninagar - I','Y',326),
 (2404,'2404',' Murshidabad Jiaganj','Y',326),
 (2405,'2405',' Nabagram','Y',326),
 (2406,'2406',' Khargram','Y',326),
 (2407,'2407',' Kandi','Y',326),
 (2408,'2408',' Berhampore','Y',326),
 (2409,'2409',' Hariharpara','Y',326),
 (2410,'2410',' Nawda','Y',326),
 (2411,'2411',' Beldanga - I','Y',326),
 (2412,'2412',' Beldanga - II','Y',326),
 (2413,'2413',' Bharatpur - II','Y',326),
 (2414,'2414',' Bharatpur - I','Y',326),
 (2415,'2415',' Burwan','Y',326),
 (2416,'2416',' Birbhum','Y',327),
 (2417,'2417',' Murarai - I','Y',327),
 (2418,'2418',' Murarai - II','Y',327),
 (2419,'2419',' Nalhati - I','Y',327),
 (2420,'2420',' Nalhati - II','Y',327),
 (2421,'2421',' Rampurhat - I','Y',327),
 (2422,'2422',' Rampurhat - II','Y',327),
 (2423,'2423',' Mayureswar - I','Y',327),
 (2424,'2424',' Mayureswar - II','Y',327),
 (2425,'2425',' Mohammad Bazar','Y',327),
 (2426,'2426',' Rajnagar','Y',327),
 (2427,'2427',' Suri - I','Y',327),
 (2428,'2428',' Suri - II','Y',327),
 (2429,'2429',' Sainthia','Y',327),
 (2430,'2430',' Labpur','Y',327),
 (2431,'2431',' Nanoor','Y',327),
 (2432,'2432',' Bolpur Sriniketan','Y',327),
 (2433,'2433',' Illambazar','Y',327),
 (2434,'2434',' Dubrajpur','Y',327),
 (2435,'2435',' Khoyrasol','Y',327),
 (2436,'2436',' Barddhaman ','Y',328),
 (2437,'2437',' Salanpur','Y',328),
 (2438,'2438',' Barabani','Y',328),
 (2439,'2439',' Jamuria','Y',328),
 (2440,'2440',' Raniganj','Y',328),
 (2441,'2441',' Ondal','Y',328),
 (2442,'2442',' Pandabeswar','Y',328),
 (2443,'2443',' Faridpur Durgapur','Y',328),
 (2444,'2444',' Kanksa','Y',328),
 (2445,'2445',' Ausgram - II','Y',328),
 (2446,'2446',' Ausgram - I','Y',328),
 (2447,'2447',' Mangolkote','Y',328),
 (2448,'2448',' Ketugram - I','Y',328),
 (2449,'2449',' Ketugram - II','Y',328),
 (2450,'2450',' Katwa - I','Y',328),
 (2451,'2451',' Katwa - II','Y',328),
 (2452,'2452',' Purbasthali - I','Y',328),
 (2453,'2453',' Purbasthali - II','Y',328),
 (2454,'2454',' Manteswar','Y',328),
 (2455,'2455',' Bhatar','Y',328),
 (2456,'2456',' Galsi - I','Y',328),
 (2457,'2457',' Galsi - II','Y',328),
 (2458,'2458',' Burdwan - I','Y',328),
 (2459,'2459',' Burdwan - II','Y',328),
 (2460,'2460',' Memari - I','Y',328),
 (2461,'2461',' Memari - II','Y',328),
 (2462,'2462',' Kalna - I','Y',328),
 (2463,'2463',' Kalna - II','Y',328),
 (2464,'2464',' Jamalpur','Y',328),
 (2465,'2465',' Raina - I','Y',328),
 (2466,'2466',' Khandaghosh','Y',328),
 (2467,'2467',' Raina - II','Y',328),
 (2468,'2468',' Nadia ','Y',329),
 (2469,'2469',' Karimpur - I','Y',329),
 (2470,'2470',' Karimpur - II','Y',329),
 (2471,'2471',' Tehatta - I','Y',329),
 (2472,'2472',' Tehatta - II','Y',329),
 (2473,'2473',' Kaliganj','Y',329),
 (2474,'2474',' Nakashipara','Y',329),
 (2475,'2475',' Chapra','Y',329),
 (2476,'2476',' Krishnagar - II','Y',329),
 (2477,'2477','  Nabadwip','Y',329),
 (2478,'2478',' Krishnagar - I','Y',329),
 (2479,'2479',' Krishnaganj','Y',329),
 (2480,'2480',' Hanskhali','Y',329),
 (2481,'2481',' Santipur','Y',329),
 (2482,'2482',' Ranaghat - I','Y',329),
 (2483,'2483',' Ranaghat - II','Y',329),
 (2484,'2484',' Chakdah','Y',329),
 (2485,'2485',' Haringhata','Y',329),
 (2486,'2486',' North Twenty Four Parganas','Y',330),
 (2487,'2487',' Bagda','Y',330),
 (2488,'2488',' Bongaon','Y',330),
 (2489,'2489',' Gaighata','Y',330),
 (2490,'2490',' Swarupnagar','Y',330),
 (2491,'2491',' Habra - I','Y',330),
 (2492,'2492',' Habra - II','Y',330),
 (2493,'2493',' Amdanga','Y',330),
 (2494,'2494',' Barrackpur - I','Y',330),
 (2495,'2495',' Barrackpur - II','Y',330),
 (2496,'2496',' Barasat - I','Y',330),
 (2497,'2497',' Barasat - II','Y',330),
 (2498,'2498',' Deganga','Y',330),
 (2499,'2499',' Baduria','Y',330),
 (2500,'2500',' Basirhat - I','Y',330),
 (2501,'2501',' Basirhat - II','Y',330),
 (2502,'2502',' Haroa','Y',330),
 (2503,'2503',' Rajarhat','Y',330),
 (2504,'2504',' Minakhan','Y',330),
 (2505,'2505',' Sandeskhali - I','Y',330),
 (2506,'2506',' Sandeskhali - II','Y',330),
 (2507,'2507',' Hasnabad','Y',330),
 (2508,'2508',' Hingalganj','Y',330),
 (2509,'2509',' Hugli ','Y',331),
 (2510,'2510',' Goghat - I','Y',331),
 (2511,'2511',' Goghat - II','Y',331),
 (2512,'2512',' Arambag','Y',331),
 (2513,'2513',' Pursura','Y',331),
 (2514,'2514',' Tarakeswar','Y',331),
 (2515,'2515',' Dhaniakhali','Y',331),
 (2516,'2516',' Pandua','Y',331),
 (2517,'2517',' Balagarh','Y',331),
 (2518,'2518',' Chinsurah - Magra','Y',331),
 (2519,'2519',' Polba - Dadpur','Y',331),
 (2520,'2520',' Haripal','Y',331),
 (2521,'2521',' Singur','Y',331),
 (2522,'2522',' Serampur Uttarpara','Y',331),
 (2523,'2523',' Chanditala - I','Y',331),
 (2524,'2524',' Chanditala - II','Y',331),
 (2525,'2525',' Jangipara','Y',331),
 (2526,'2526',' Khanakul - I','Y',331),
 (2527,'2527',' Khanakul - II','Y',331),
 (2528,'2528',' Bankura ','Y',332),
 (2529,'2529',' Saltora','Y',332),
 (2530,'2530',' Mejhia','Y',332),
 (2531,'2531',' Gangajalghati','Y',332),
 (2532,'2532',' Chhatna','Y',332),
 (2533,'2533',' Indpur','Y',332),
 (2534,'2534',' Bankura - I','Y',332),
 (2535,'2535',' Bankura - II','Y',332),
 (2536,'2536',' Barjora','Y',332),
 (2537,'2537',' Sonamukhi','Y',332),
 (2538,'2538',' Patrasayer','Y',332),
 (2539,'2539',' Indus','Y',332),
 (2540,'2540',' Kotulpur','Y',332),
 (2541,'2541',' Jaypur','Y',332),
 (2542,'2542',' Vishnupur','Y',332),
 (2543,'2543',' Onda','Y',332),
 (2544,'2544',' Taldangra','Y',332),
 (2545,'2545',' Simlapal','Y',332),
 (2546,'2546',' Khatra','Y',332),
 (2547,'2547',' Hirbandh','Y',332),
 (2548,'2548',' Ranibundh','Y',332),
 (2549,'2549',' Raipur','Y',332),
 (2550,'2550',' Sarenga','Y',332),
 (2551,'2551',' Puruliya','Y',333),
 (2552,'2552',' Jaipur','Y',333),
 (2553,'2553',' Purulia - II','Y',333),
 (2554,'2554',' Para','Y',333),
 (2555,'2555',' Raghunathpur - II','Y',333),
 (2556,'2556',' Raghunathpur - I','Y',333),
 (2557,'2557',' Neturia','Y',333),
 (2558,'2558',' Santuri','Y',333),
 (2559,'2559',' Kashipur','Y',333),
 (2560,'2560',' Hura','Y',333),
 (2561,'2561',' Purulia - I','Y',333),
 (2562,'2562',' Puncha','Y',333),
 (2563,'2563',' Arsha','Y',333),
 (2564,'2564',' Jhalda - I','Y',333),
 (2565,'2565',' Jhalda - II','Y',333),
 (2566,'2566',' Bagmundi','Y',333),
 (2567,'2567',' Balarampur','Y',333),
 (2568,'2568',' Barabazar','Y',333),
 (2569,'2569',' Manbazar - I','Y',333),
 (2570,'2570',' Manbazar - II','Y',333),
 (2571,'2571',' Bundwan','Y',333),
 (2572,'2572',' Medinipur ','Y',334),
 (2573,'2573',' Binpur - II','Y',334),
 (2574,'2574',' Binpur - I','Y',334),
 (2575,'2575',' Garbeta - II','Y',334),
 (2576,'2576',' Garbeta - I','Y',334),
 (2577,'2577',' Garbeta - III','Y',334),
 (2578,'2578',' Chandrakona - I','Y',334),
 (2579,'2579',' Chandrakona - II','Y',334),
 (2580,'2580',' Ghatal','Y',334),
 (2581,'2581',' Daspur - I','Y',334),
 (2582,'2582',' Daspur - II','Y',334),
 (2583,'2583',' Keshpur','Y',334),
 (2584,'2584',' Salbani','Y',334),
 (2585,'2585',' Midnapore','Y',334),
 (2586,'2586',' Jhargram','Y',334),
 (2587,'2587',' Jamboni','Y',334),
 (2588,'2588',' Gopiballavpur - II','Y',334),
 (2589,'2589',' Gopiballavpur - I','Y',334),
 (2590,'2590',' Nayagram','Y',334),
 (2591,'2591',' Sankrail','Y',334),
 (2592,'2592',' Kharagpur - I','Y',334),
 (2593,'2593',' Kharagpur - II','Y',334),
 (2594,'2594',' Debra','Y',334),
 (2595,'2595',' Panskura - I','Y',334),
 (2596,'2596',' Panskura - II','Y',334),
 (2597,'2597',' Tamluk','Y',334),
 (2598,'2598',' Sahid Matangini','Y',334),
 (2599,'2599',' Nanda Kumar','Y',334),
 (2600,'2600',' Mahisadal','Y',334),
 (2601,'2601',' Moyna','Y',334),
 (2602,'2602',' Pingla','Y',334),
 (2603,'2603',' Sabang','Y',334),
 (2604,'2604',' Narayangarh','Y',334),
 (2605,'2605',' Keshiary','Y',334),
 (2606,'2606',' Dantan - I','Y',334),
 (2607,'2607',' Dantan - II','Y',334),
 (2608,'2608',' Potashpur - I','Y',334),
 (2609,'2609',' Potashpur - II','Y',334),
 (2610,'2610',' Bhagawanpur - II','Y',334),
 (2611,'2611',' Bhagawanpur - I','Y',334),
 (2612,'2612',' Nandigram - III','Y',334),
 (2613,'2613',' Sutahata - I','Y',334),
 (2614,'2614',' Sutahata - II','Y',334),
 (2615,'2615',' Nandigram - I','Y',334),
 (2616,'2616',' Nandigram - II','Y',334),
 (2617,'2617',' Khejuri - I','Y',334),
 (2618,'2618',' Khejuri - II','Y',334),
 (2619,'2619',' Contai - I','Y',334),
 (2620,'2620',' Contai - II','Y',334),
 (2621,'2621',' Contai - III','Y',334),
 (2622,'2622',' Egra - I','Y',334),
 (2623,'2623',' Egra - II','Y',334),
 (2624,'2624',' Mohanpur','Y',334),
 (2625,'2625',' Ramnagar - I','Y',334),
 (2626,'2626',' Ramnagar - II','Y',334),
 (2627,'2627',' Haora ','Y',335),
 (2628,'2628',' Udaynarayanpur','Y',335),
 (2629,'2629',' Amta - II','Y',335),
 (2630,'2630',' Amta - I','Y',335),
 (2631,'2631',' Jagatballavpur','Y',335),
 (2632,'2632',' Domjur','Y',335),
 (2633,'2633',' Bally Jagachha','Y',335),
 (2634,'2634',' Sankrail','Y',335),
 (2635,'2635',' Panchla','Y',335),
 (2636,'2636',' Uluberia - II','Y',335),
 (2637,'2637',' Uluberia - I','Y',335),
 (2638,'2638',' Bagnan - I','Y',335),
 (2639,'2639',' Bagnan - II','Y',335),
 (2640,'2640',' Shyampur - I','Y',335),
 (2641,'2641',' Shyampur - II','Y',335),
 (2642,'2642',' Kolkata','Y',336),
 (2643,'2643',' South  Twenty Four Parganas','Y',337),
 (2644,'2644',' Thakurpukur Mahestola','Y',337),
 (2645,'2645',' Budge Budge - I','Y',337),
 (2646,'2646',' Budge Budge - II','Y',337),
 (2647,'2647',' Bishnupur - I','Y',337),
 (2648,'2648',' Bishnupur - II','Y',337),
 (2649,'2649',' Sonarpur','Y',337),
 (2650,'2650',' Bhangar - I','Y',337),
 (2651,'2651',' Bhangar - II','Y',337),
 (2652,'2652',' Canning - I','Y',337),
 (2653,'2653',' Canning - II','Y',337),
 (2654,'2654',' Baruipur','Y',337),
 (2655,'2655',' Magrahat - II','Y',337),
 (2656,'2656',' Magrahat - I','Y',337),
 (2657,'2657',' Falta','Y',337),
 (2658,'2658',' Diamond Harbour - I','Y',337),
 (2659,'2659',' Diamond Harbour - II','Y',337),
 (2660,'2660',' Kulpi','Y',337),
 (2661,'2661',' Mandirbazar','Y',337),
 (2662,'2662',' Mathurapur - I','Y',337),
 (2663,'2663',' Jaynagar - I','Y',337),
 (2664,'2664',' Jaynagar - II','Y',337),
 (2665,'2665',' Kultali','Y',337),
 (2666,'2666',' Basanti','Y',337),
 (2667,'2667',' Gosaba','Y',337),
 (2668,'2668',' Mathurapur - II','Y',337),
 (2669,'2669',' Kakdwip','Y',337),
 (2670,'2670',' Sagar','Y',337),
 (2671,'2671',' Namkhana','Y',337),
 (2672,'2672',' Patharpratima','Y',337),
 (2673,'2673',' JHARKHAND','Y',338),
 (2674,'2674',' Garhwa *','Y',339),
 (2675,'2675',' Kharaundhi','Y',339),
 (2676,'2676',' Bhawnathpur','Y',339),
 (2677,'2677',' Kandi','Y',339),
 (2678,'2678',' Majhiaon','Y',339),
 (2679,'2679',' Ramna','Y',339),
 (2680,'2680',' Nagaruntari','Y',339),
 (2681,'2681',' Dhurki','Y',339),
 (2682,'2682',' Dandai','Y',339),
 (2683,'2683',' Chinia','Y',339),
 (2684,'2684',' Meral (Pipra Kalan)','Y',339),
 (2685,'2685',' Garhwa','Y',339),
 (2686,'2686',' Ranka','Y',339),
 (2687,'2687',' Ramkanda','Y',339),
 (2688,'2688',' Bhandaria','Y',339),
 (2689,'2689',' Palamu','Y',340),
 (2690,'2690',' Hussainabad','Y',340),
 (2691,'2691',' Hariharganj','Y',340),
 (2692,'2692',' Chhatarpur','Y',340),
 (2693,'2693',' Pandu','Y',340),
 (2694,'2694',' Bishrampur','Y',340),
 (2695,'2695',' Patan','Y',340),
 (2696,'2696',' Manatu','Y',340),
 (2697,'2697',' Panki','Y',340),
 (2698,'2698',' Manika','Y',340),
 (2699,'2699',' Satbarwa','Y',340),
 (2700,'2700',' Leslieganj','Y',340),
 (2701,'2701',' Daltonganj','Y',340),
 (2702,'2702',' Chainpur','Y',340),
 (2703,'2703',' Barwadih','Y',340),
 (2704,'2704',' Mahuadanr','Y',340),
 (2705,'2705',' Garu','Y',340),
 (2706,'2706',' Latehar','Y',340),
 (2707,'2707',' Balumath','Y',340),
 (2708,'2708',' Chandwa','Y',340),
 (2709,'2709',' Chatra *','Y',341),
 (2710,'2710',' Hunterganj','Y',341),
 (2711,'2711',' Pratappur','Y',341),
 (2712,'2712',' Kunda','Y',341),
 (2713,'2713',' Lawalaung','Y',341),
 (2714,'2714',' Chatra','Y',341),
 (2715,'2715',' Itkhori','Y',341),
 (2716,'2716',' Gidhaur','Y',341),
 (2717,'2717',' Pathalgora','Y',341),
 (2718,'2718',' Simaria','Y',341),
 (2719,'2719',' Tandwa','Y',341),
 (2720,'2720',' Hazaribag','Y',342),
 (2721,'2721',' Chauparan','Y',342),
 (2722,'2722',' Barhi','Y',342),
 (2723,'2723',' Padma','Y',342),
 (2724,'2724',' Ichak','Y',342),
 (2725,'2725',' Barkatha','Y',342),
 (2726,'2726',' Bishungarh','Y',342),
 (2727,'2727',' Hazaribag','Y',342),
 (2728,'2728',' Katkamsandi','Y',342),
 (2729,'2729',' Keredari','Y',342),
 (2730,'2730',' Barkagaon','Y',342),
 (2731,'2731',' Patratu','Y',342),
 (2732,'2732',' Churchu','Y',342),
 (2733,'2733',' Mandu','Y',342),
 (2734,'2734',' Ramgarh','Y',342),
 (2735,'2735',' Gola','Y',342),
 (2736,'2736',' Kodarma *','Y',343),
 (2737,'2737',' Satgawan','Y',343),
 (2738,'2738',' Kodarma','Y',343),
 (2739,'2739',' Jainagar','Y',343),
 (2740,'2740',' Markacho','Y',343),
 (2741,'2741',' Giridih','Y',344),
 (2742,'2742',' Gawan','Y',344),
 (2743,'2743',' Tisri','Y',344),
 (2744,'2744',' Deori','Y',344),
 (2745,'2745',' Dhanwar','Y',344),
 (2746,'2746',' Jamua','Y',344),
 (2747,'2747',' Bengabad','Y',344),
 (2748,'2748',' Gande','Y',344),
 (2749,'2749',' Giridih','Y',344),
 (2750,'2750',' Birni','Y',344),
 (2751,'2751',' Bagodar','Y',344),
 (2752,'2752',' Dumri','Y',344),
 (2753,'2753',' Pirtanr','Y',344),
 (2754,'2754',' Deoghar','Y',345),
 (2755,'2755',' Deoghar','Y',345),
 (2756,'2756',' Mohanpur','Y',345),
 (2757,'2757',' Sarwan','Y',345),
 (2758,'2758',' Devipur','Y',345),
 (2759,'2759',' Madhupur','Y',345),
 (2760,'2760',' Karon','Y',345),
 (2761,'2761',' Sarath','Y',345),
 (2762,'2762',' Palojori','Y',345),
 (2763,'2763',' Godda','Y',346),
 (2764,'2764',' Meherma','Y',346),
 (2765,'2765',' Thakur Gangti','Y',346),
 (2766,'2766',' Boarijor','Y',346),
 (2767,'2767',' Mahagama','Y',346),
 (2768,'2768',' Pathargama','Y',346),
 (2769,'2769',' Godda','Y',346),
 (2770,'2770',' Poreyahat','Y',346),
 (2771,'2771',' Sundar Pahari','Y',346),
 (2772,'2772',' Sahibganj','Y',347),
 (2773,'2773',' Sahibganj','Y',347),
 (2774,'2774',' Mandro','Y',347),
 (2775,'2775',' Borio','Y',347),
 (2776,'2776',' Barhait','Y',347),
 (2777,'2777',' Taljhari','Y',347),
 (2778,'2778',' Rajmahal','Y',347),
 (2779,'2779',' Udhwa','Y',347),
 (2780,'2780',' Pathna','Y',347),
 (2781,'2781',' Barharwa','Y',347),
 (2782,'2782',' Pakaur *','Y',348),
 (2783,'2783',' Litipara','Y',348),
 (2784,'2784',' Amrapara','Y',348),
 (2785,'2785',' Hiranpur','Y',348),
 (2786,'2786',' Pakaur','Y',348),
 (2787,'2787',' Maheshpur','Y',348),
 (2788,'2788',' Pakuria','Y',348),
 (2789,'2789',' Dumka','Y',349),
 (2790,'2790',' Saraiyahat','Y',349),
 (2791,'2791',' Jarmundi','Y',349),
 (2792,'2792',' Ramgarh','Y',349),
 (2793,'2793',' Gopikandar','Y',349),
 (2794,'2794',' Kathikund','Y',349),
 (2795,'2795',' Shikaripara','Y',349),
 (2796,'2796',' Ranishwar','Y',349),
 (2797,'2797',' Dumka','Y',349),
 (2798,'2798',' Jama','Y',349),
 (2799,'2799',' Masalia','Y',349),
 (2800,'2800',' Narayanpur','Y',349),
 (2801,'2801',' Jamtara','Y',349),
 (2802,'2802',' Nala','Y',349),
 (2803,'2803',' Kundhit','Y',349),
 (2804,'2804',' Dhanbad','Y',350),
 (2805,'2805',' Tundi','Y',350),
 (2806,'2806',' Topchanchi','Y',350),
 (2807,'2807',' Baghmara-Cum-Katras','Y',350),
 (2808,'2808',' Gobindpur','Y',350),
 (2809,'2809',' Dhanbad-Cum-Ken-duadih-Cum-Jagta','Y',350),
 (2810,'2810',' Jharia-Cum-Jorap-okhar-Cum-Sindri','Y',350),
 (2811,'2811',' Baliapur','Y',350),
 (2812,'2812',' Nirsa-Cum-Chirkunda','Y',350),
 (2813,'2813',' Bokaro *','Y',351),
 (2814,'2814',' Nawadih','Y',351),
 (2815,'2815',' Bermo','Y',351),
 (2816,'2816',' Gumia','Y',351),
 (2817,'2817',' Peterwar','Y',351),
 (2818,'2818',' Kasmar','Y',351),
 (2819,'2819',' Jaridih','Y',351),
 (2820,'2820',' Chas','Y',351),
 (2821,'2821',' Chandankiyari','Y',351),
 (2822,'2822',' Ranchi','Y',352),
 (2823,'2823',' Burmu','Y',352),
 (2824,'2824',' Kanke','Y',352),
 (2825,'2825',' Ormanjhi','Y',352),
 (2826,'2826',' Angara','Y',352),
 (2827,'2827',' Silli','Y',352),
 (2828,'2828',' Sonahatu','Y',352),
 (2829,'2829',' Namkum','Y',352),
 (2830,'2830',' Ratu','Y',352),
 (2831,'2831',' Mandar','Y',352),
 (2832,'2832',' Chanho','Y',352),
 (2833,'2833',' Bero','Y',352),
 (2834,'2834',' Lapung','Y',352),
 (2835,'2835',' Karra','Y',352),
 (2836,'2836',' Torpa','Y',352),
 (2837,'2837',' Rania','Y',352),
 (2838,'2838',' Murhu','Y',352),
 (2839,'2839',' Khunti','Y',352),
 (2840,'2840',' Bundu','Y',352),
 (2841,'2841',' Erki (Tamar II)','Y',352),
 (2842,'2842',' Tamar I','Y',352),
 (2843,'2843',' Lohardaga','Y',353),
 (2844,'2844',' Kisko','Y',353),
 (2845,'2845',' Kuru','Y',353),
 (2846,'2846',' Lohardaga','Y',353),
 (2847,'2847',' Senha','Y',353),
 (2848,'2848',' Bhandra','Y',353),
 (2849,'2849',' Gumla','Y',354),
 (2850,'2850',' Bishunpur','Y',354),
 (2851,'2851',' Ghaghra','Y',354),
 (2852,'2852',' Sisai','Y',354),
 (2853,'2853',' Verno','Y',354),
 (2854,'2854',' Kamdara','Y',354),
 (2855,'2855',' Basia','Y',354),
 (2856,'2856',' Gumla','Y',354),
 (2857,'2857',' Chainpur','Y',354),
 (2858,'2858',' Dumri','Y',354),
 (2859,'2859',' Raidih','Y',354),
 (2860,'2860',' Palkot','Y',354),
 (2861,'2861',' simdega','Y',354),
 (2862,'2862',' Kurdeg','Y',354),
 (2863,'2863',' Bolba','Y',354),
 (2864,'2864',' Thethaitangar','Y',354),
 (2865,'2865',' Kolebira','Y',354),
 (2866,'2866',' Jaldega','Y',354),
 (2867,'2867',' Bano','Y',354),
 (2868,'2868',' Pashchimi Singhbhum','Y',355),
 (2869,'2869',' Sonua','Y',355),
 (2870,'2870',' Bandgaon','Y',355),
 (2871,'2871',' Chakradharpur','Y',355),
 (2872,'2872',' Kuchai','Y',355),
 (2873,'2873',' Kharsawan','Y',355),
 (2874,'2874',' Chandil','Y',355),
 (2875,'2875',' Ichagarh','Y',355),
 (2876,'2876',' Nimdih','Y',355),
 (2877,'2877',' Adityapur','Y',355),
 (2878,'2878',' Seraikela','Y',355),
 (2879,'2879',' Gobindpur','Y',355),
 (2880,'2880',' Khuntpani','Y',355),
 (2881,'2881',' Goilkera','Y',355),
 (2882,'2882',' Manoharpur','Y',355),
 (2883,'2883',' Noamundi','Y',355),
 (2884,'2884',' Tonto','Y',355),
 (2885,'2885',' Chaibasa','Y',355),
 (2886,'2886',' Tantnagar','Y',355),
 (2887,'2887',' Manjhari','Y',355),
 (2888,'2888',' Jhinkpani','Y',355),
 (2889,'2889',' Jagannathpur','Y',355),
 (2890,'2890',' Kumardungi','Y',355),
 (2891,'2891',' Majhgaon','Y',355),
 (2892,'2892',' Purbi Singhbhum','Y',356),
 (2893,'2893',' Patamda','Y',356),
 (2894,'2894',' Golmuri-Cum-Jugsalai','Y',356),
 (2895,'2895',' Ghatshila','Y',356),
 (2896,'2896',' Potka','Y',356),
 (2897,'2897',' Musabani','Y',356),
 (2898,'2898',' Dumaria','Y',356),
 (2899,'2899',' Dhalbhumgarh','Y',356),
 (2900,'2900',' Chakulia','Y',356),
 (2901,'2901',' Baharagora','Y',356),
 (2902,'2902',' ORISSA','Y',357),
 (2903,'2903',' Bargarh  *','Y',358),
 (2904,'2904',' Paikamal','Y',358),
 (2905,'2905',' Jharabandha','Y',358),
 (2906,'2906',' Jharabandha','Y',358),
 (2907,'2907',' Padmapur','Y',358),
 (2908,'2908',' Burden','Y',358),
 (2909,'2909',' Gaisilet','Y',358),
 (2910,'2910',' Melchhamunda','Y',358),
 (2911,'2911',' Sohela','Y',358),
 (2912,'2912',' Bijepur','Y',358),
 (2913,'2913',' Barapali','Y',358),
 (2914,'2914',' Bheden','Y',358),
 (2915,'2915',' Bargarh','Y',358),
 (2916,'2916',' Bhatli','Y',358),
 (2917,'2917',' Ambabhona','Y',358),
 (2918,'2918',' Attabira','Y',358),
 (2919,'2919',' Jharsuguda  *','Y',359),
 (2920,'2920',' Rengali','Y',359),
 (2921,'2921',' Lakhanpur','Y',359),
 (2922,'2922',' Belpahar','Y',359),
 (2923,'2923',' Banaharapali','Y',359),
 (2924,'2924',' Orient','Y',359),
 (2925,'2925',' Brajarajnagar','Y',359),
 (2926,'2926',' Jharsuguda','Y',359),
 (2927,'2927',' Laikera','Y',359),
 (2928,'2928',' Kolabira','Y',359),
 (2929,'2929',' Sambalpur','Y',360),
 (2930,'2930',' Govindpur','Y',360),
 (2931,'2931',' Mahulpalli','Y',360),
 (2932,'2932',' Kochinda','Y',360),
 (2933,'2933',' Jamankira','Y',360),
 (2934,'2934',' Kisinda','Y',360),
 (2935,'2935',' Naktideul','Y',360),
 (2936,'2936',' Rairakhol','Y',360),
 (2937,'2937',' Charamal','Y',360),
 (2938,'2938',' Jujomura','Y',360),
 (2939,'2939',' Dhama','Y',360),
 (2940,'2940',' Burla','Y',360),
 (2941,'2941',' Hirakud','Y',360),
 (2942,'2942',' Ainthapali','Y',360),
 (2943,'2943',' Dhanupali','Y',360),
 (2944,'2944',' Sadar','Y',360),
 (2945,'2945',' Sasan','Y',360),
 (2946,'2946',' Katarbaga','Y',360),
 (2947,'2947',' Debagarh  *','Y',361),
 (2948,'2948',' Debagarh','Y',361),
 (2949,'2949',' Barkot','Y',361),
 (2950,'2950',' Kundheigola','Y',361),
 (2951,'2951',' Reamal','Y',361),
 (2952,'2952',' Sundargarh','Y',362),
 (2953,'2953',' Hemgir','Y',362),
 (2954,'2954',' Lephripara','Y',362),
 (2955,'2955',' Bhasma','Y',362),
 (2956,'2956',' Bhasma','Y',362),
 (2957,'2957',' Sundargarh Town','Y',362),
 (2958,'2958',' Sundargarh    ','Y',362),
 (2959,'2959',' Kinjirkela','Y',362),
 (2960,'2960',' Kinjirkela ','Y',362),
 (2961,'2961',' Kinjirkela ','Y',362),
 (2962,'2962',' Talasara','Y',362),
 (2963,'2963',' Talasara','Y',362),
 (2964,'2964',' Baragaon','Y',362),
 (2965,'2965',' Kutra','Y',362),
 (2966,'2966',' Rajagangapur','Y',362),
 (2967,'2967',' Rajagangapur','Y',362),
 (2968,'2968',' Raiboga','Y',362),
 (2969,'2969',' Biramitrapur','Y',362),
 (2970,'2970',' Biramitrapur','Y',362),
 (2971,'2971',' Hatibari','Y',362),
 (2972,'2972',' Bisra','Y',362),
 (2973,'2973',' Bisra','Y',362),
 (2974,'2974',' Bondamunda','Y',362),
 (2975,'2975',' Bondamunda','Y',362),
 (2976,'2976',' Brahmani Tarang','Y',362),
 (2977,'2977',' Raghunathapali','Y',362),
 (2978,'2978',' Tangarapali','Y',362),
 (2979,'2979',' Lathikata','Y',362),
 (2980,'2980',' Banki','Y',362),
 (2981,'2981',' Kamarposh Balang','Y',362),
 (2982,'2982',' Koida','Y',362),
 (2983,'2983',' Lahunipara','Y',362),
 (2984,'2984',' Gurundia','Y',362),
 (2985,'2985',' Tikaetpali','Y',362),
 (2986,'2986',' Banei','Y',362),
 (2987,'2987',' Mahulpada','Y',362),
 (2988,'2988',' Kendujhar','Y',363),
 (2989,'2989',' Telkoi','Y',363),
 (2990,'2990',' Kanjipani','Y',363),
 (2991,'2991',' Nayakote','Y',363),
 (2992,'2992',' Barbil','Y',363),
 (2993,'2993',' Joda','Y',363),
 (2994,'2994',' Champua','Y',363),
 (2995,'2995',' Champua','Y',363),
 (2996,'2996',' Baria','Y',363),
 (2997,'2997',' Turumunga','Y',363),
 (2998,'2998',' Turumunga','Y',363),
 (2999,'2999',' Patana','Y',363),
 (3000,'3000',' Patana','Y',363),
 (3001,'3001',' Ghatgaon','Y',363),
 (3002,'3002',' Ghatgaon','Y',363),
 (3003,'3003',' Kendujhar Sadar','Y',363),
 (3004,'3004',' Kendujhar Town','Y',363),
 (3005,'3005',' Pandapara','Y',363),
 (3006,'3006',' Harichandanpur','Y',363),
 (3007,'3007',' Daitari','Y',363),
 (3008,'3008',' Ghasipura','Y',363),
 (3009,'3009',' Sainkul','Y',363),
 (3010,'3010',' Nandipada','Y',363),
 (3011,'3011',' Anandapur','Y',363),
 (3012,'3012',' Anandapur','Y',363),
 (3013,'3013',' Soso','Y',363),
 (3014,'3014',' Mayurbhanj','Y',364),
 (3015,'3015',' Tiringi','Y',364),
 (3016,'3016',' Bahalda','Y',364),
 (3017,'3017',' Gorumahisani','Y',364),
 (3018,'3018',' Rairangpur','Y',364),
 (3019,'3019',' Rairangpur Town','Y',364),
 (3020,'3020',' Badampahar','Y',364),
 (3021,'3021',' Bisoi','Y',364),
 (3022,'3022',' Bangiriposi','Y',364),
 (3023,'3023',' Jharpokharia','Y',364),
 (3024,'3024',' Chandua','Y',364),
 (3025,'3025',' Koliana','Y',364),
 (3026,'3026',' Baripada Sadar','Y',364),
 (3027,'3027',' Baripada Town','Y',364),
 (3028,'3028',' Suliapada','Y',364),
 (3029,'3029',' Muruda ','Y',364),
 (3030,'3030',' Muruda ','Y',364),
 (3031,'3031',' Muruda ','Y',364),
 (3032,'3032',' Betanati','Y',364),
 (3033,'3033',' Betanati','Y',364),
 (3034,'3034',' Rasagobindapur','Y',364),
 (3035,'3035',' Baisinga','Y',364),
 (3036,'3036',' Barsahi','Y',364),
 (3037,'3037',' Khunta','Y',364),
 (3038,'3038',' Udala','Y',364),
 (3039,'3039',' Kaptipada','Y',364),
 (3040,'3040',' Sharata','Y',364),
 (3041,'3041',' Mahuldiha','Y',364),
 (3042,'3042',' Thakurmunda','Y',364),
 (3043,'3043',' Karanjia','Y',364),
 (3044,'3044',' Jashipur','Y',364),
 (3045,'3045',' Jashipur','Y',364),
 (3046,'3046',' Raruan','Y',364),
 (3047,'3047',' Raruan','Y',364),
 (3048,'3048',' Baleshwar','Y',365),
 (3049,'3049',' Raibania','Y',365),
 (3050,'3050',' Jaleswar','Y',365),
 (3051,'3051',' Bhograi','Y',365),
 (3052,'3052',' Baliapal','Y',365),
 (3053,'3053',' Singla','Y',365),
 (3054,'3054',' Singla','Y',365),
 (3055,'3055',' Basta','Y',365),
 (3056,'3056',' Rupsa','Y',365),
 (3057,'3057',' Baleshwar Sadar','Y',365),
 (3058,'3058',' Chandipur','Y',365),
 (3059,'3059',' Remuna','Y',365),
 (3060,'3060',' Bampada','Y',365),
 (3061,'3061',' Nilagiri','Y',365),
 (3062,'3062',' Berhampur','Y',365),
 (3063,'3063',' Oupada','Y',365),
 (3064,'3064',' Soro','Y',365),
 (3065,'3065',' Khaira','Y',365),
 (3066,'3066',' Similia','Y',365),
 (3067,'3067',' Bhadrak  *','Y',366),
 (3068,'3068',' Agarpada','Y',366),
 (3069,'3069',' Bant','Y',366),
 (3070,'3070',' Bant','Y',366),
 (3071,'3071',' Bhadrak Rural','Y',366),
 (3072,'3072',' Bhadrak Rural','Y',366),
 (3073,'3073',' Bhandari Pokhari','Y',366),
 (3074,'3074',' Dhamanagar','Y',366),
 (3075,'3075',' Dhusuri','Y',366),
 (3076,'3076',' Dhusuri','Y',366),
 (3077,'3077',' Tihidi','Y',366),
 (3078,'3078',' Tihidi','Y',366),
 (3079,'3079',' Chandabali','Y',366),
 (3080,'3080',' Bansada','Y',366),
 (3081,'3081',' Naikanidihi','Y',366),
 (3082,'3082',' Basudebpur','Y',366),
 (3083,'3083',' Kendrapara *','Y',367),
 (3084,'3084',' Rajkanika','Y',367),
 (3085,'3085',' Aali','Y',367),
 (3086,'3086',' Pattamundai','Y',367),
 (3087,'3087',' Kendrapara','Y',367),
 (3088,'3088',' Patkura','Y',367),
 (3089,'3089',' Patkura','Y',367),
 (3090,'3090',' Mahakalapada','Y',367),
 (3091,'3091',' Rajnagar','Y',367),
 (3092,'3092',' Jagatsinghapur  *','Y',368),
 (3093,'3093',' Paradip','Y',368),
 (3094,'3094',' Kujang','Y',368),
 (3095,'3095',' Ersama','Y',368),
 (3096,'3096',' Tirtol','Y',368),
 (3097,'3097',' Balikuda','Y',368),
 (3098,'3098',' Naugaon','Y',368),
 (3099,'3099',' Jagatsinghapur','Y',368),
 (3100,'3100',' Cuttack','Y',369),
 (3101,'3101',' Mahanga','Y',369),
 (3102,'3102',' Salepur','Y',369),
 (3103,'3103',' Jagatpur','Y',369),
 (3104,'3104',' Kishannagar','Y',369),
 (3105,'3105',' Niali','Y',369),
 (3106,'3106',' Gobindpur','Y',369),
 (3107,'3107',' Cuttack Sadar','Y',369),
 (3108,'3108',' Tangi','Y',369),
 (3109,'3109',' Choudwar','Y',369),
 (3110,'3110',' Choudwar','Y',369),
 (3111,'3111',' Gurudijhatia','Y',369),
 (3112,'3112',' Barang','Y',369),
 (3113,'3113',' Athagad','Y',369),
 (3114,'3114',' Tigiria','Y',369),
 (3115,'3115',' Banki','Y',369),
 (3116,'3116',' Baidyeswar','Y',369),
 (3117,'3117',' Badamba','Y',369),
 (3118,'3118',' Kanpur','Y',369),
 (3119,'3119',' Narasinghpur','Y',369),
 (3120,'3120',' Jajapur  *','Y',370),
 (3121,'3121',' Sukinda','Y',370),
 (3122,'3122',' Duburi','Y',370),
 (3123,'3123',' Jajapur Road','Y',370),
 (3124,'3124',' Korai','Y',370),
 (3125,'3125',' Jajapur   ','Y',370),
 (3126,'3126',' Mangalpur','Y',370),
 (3127,'3127',' Binjharpur','Y',370),
 (3128,'3128',' Binjharpur','Y',370),
 (3129,'3129',' Balichandrapur','Y',370),
 (3130,'3130',' Balichandrapur','Y',370),
 (3131,'3131',' Badachana','Y',370),
 (3132,'3132',' Badachana','Y',370),
 (3133,'3133',' Dharmasala','Y',370),
 (3134,'3134',' Dhenkanal','Y',371),
 (3135,'3135',' Bhuban','Y',371),
 (3136,'3136',' Kamakshyanagar','Y',371),
 (3137,'3137',' Kamakshyanagar','Y',371),
 (3138,'3138',' Parajang','Y',371),
 (3139,'3139',' Parajang','Y',371),
 (3140,'3140',' Tumusingha','Y',371),
 (3141,'3141',' Motunga','Y',371),
 (3142,'3142',' Balimi','Y',371),
 (3143,'3143',' Hindol','Y',371),
 (3144,'3144',' Rasol','Y',371),
 (3145,'3145',' Dhenkanal Sadar','Y',371),
 (3146,'3146',' Gandia','Y',371),
 (3147,'3147',' Gandia','Y',371),
 (3148,'3148',' Gandia','Y',371),
 (3149,'3149',' Anugul  *','Y',372),
 (3150,'3150',' Palalahada','Y',372),
 (3151,'3151',' Khamar','Y',372),
 (3152,'3152',' Rengali Damsite','Y',372),
 (3153,'3153',' Kaniha','Y',372),
 (3154,'3154',' NTPC','Y',372),
 (3155,'3155',' Samal Barrage','Y',372),
 (3156,'3156',' Talcher Sadar','Y',372),
 (3157,'3157',' Colliery','Y',372),
 (3158,'3158',' Bikrampur','Y',372),
 (3159,'3159',' NALCO','Y',372),
 (3160,'3160',' Banarpal','Y',372),
 (3161,'3161',' Anugul','Y',372),
 (3162,'3162',' Bantala','Y',372),
 (3163,'3163',' Purunakot','Y',372),
 (3164,'3164',' Jarapada','Y',372),
 (3165,'3165',' Jarapada','Y',372),
 (3166,'3166',' Chhendipada','Y',372),
 (3167,'3167',' Handapa','Y',372),
 (3168,'3168',' Kishorenagar','Y',372),
 (3169,'3169',' Thakurgarh','Y',372),
 (3170,'3170',' Athmallik','Y',372),
 (3171,'3171',' Nayagarh  *','Y',373),
 (3172,'3172',' Dasapalla','Y',373),
 (3173,'3173',' Gania','Y',373),
 (3174,'3174',' Khandapada','Y',373),
 (3175,'3175',' Fategarh','Y',373),
 (3176,'3176',' Nayagarh','Y',373),
 (3177,'3177',' Nuagaon','Y',373),
 (3178,'3178',' Odagaon','Y',373),
 (3179,'3179',' Sarankul','Y',373),
 (3180,'3180',' Ranapur','Y',373),
 (3181,'3181',' Khordha  *','Y',374),
 (3182,'3182',' Bolagad','Y',374),
 (3183,'3183',' Begunia','Y',374),
 (3184,'3184',' Begunia','Y',374),
 (3185,'3185',' Khordha','Y',374),
 (3186,'3186',' Khordha','Y',374),
 (3187,'3187',' Chandaka','Y',374),
 (3188,'3188',' Chandaka','Y',374),
 (3189,'3189',' Khandagiri','Y',374),
 (3190,'3190',' Saheednagar','Y',374),
 (3191,'3191',' Balianta','Y',374),
 (3192,'3192',' Balipatana','Y',374),
 (3193,'3193',' Lingaraj','Y',374),
 (3194,'3194',' Lingaraj','Y',374),
 (3195,'3195',' Jatani','Y',374),
 (3196,'3196',' Jankia','Y',374),
 (3197,'3197',' Jankia','Y',374),
 (3198,'3198',' Jankia','Y',374),
 (3199,'3199',' Tangi','Y',374),
 (3200,'3200',' Tangi','Y',374),
 (3201,'3201',' Balugaon','Y',374),
 (3202,'3202',' Banapur','Y',374),
 (3203,'3203',' Puri','Y',375),
 (3204,'3204',' Delanga','Y',375),
 (3205,'3205',' Pipili','Y',375),
 (3206,'3206',' Nimapada','Y',375),
 (3207,'3207',' Nimapada','Y',375),
 (3208,'3208',' Gop','Y',375),
 (3209,'3209',' Gop','Y',375),
 (3210,'3210',' Kakatpur','Y',375),
 (3211,'3211',' Konark','Y',375),
 (3212,'3212',' Konark','Y',375),
 (3213,'3213',' Satyabadi','Y',375),
 (3214,'3214',' Satyabadi','Y',375),
 (3215,'3215',' Chandanpur','Y',375),
 (3216,'3216',' Sadar','Y',375),
 (3217,'3217',' Sadar','Y',375),
 (3218,'3218',' Brahmagiri','Y',375),
 (3219,'3219',' Brahmagiri','Y',375),
 (3220,'3220',' Krushna Prasad','Y',375),
 (3221,'3221',' Ganjam','Y',376),
 (3222,'3222',' Tarasingi','Y',376),
 (3223,'3223',' Buguda','Y',376),
 (3224,'3224',' Bhanjanagar','Y',376),
 (3225,'3225',' Bhanjanagar','Y',376),
 (3226,'3226',' Gangapur','Y',376),
 (3227,'3227',' Gangapur','Y',376),
 (3228,'3228',' Gangapur','Y',376),
 (3229,'3229',' Gangapur','Y',376),
 (3230,'3230',' Surada','Y',376),
 (3231,'3231',' Badagada','Y',376),
 (3232,'3232',' Badagada','Y',376),
 (3233,'3233',' Asika','Y',376),
 (3234,'3234',' Purusottampur','Y',376),
 (3235,'3235',' Purusottampur','Y',376),
 (3236,'3236',' Purusottampur','Y',376),
 (3237,'3237',' Kabisuryanagar','Y',376),
 (3238,'3238',' Kabisuryanagar','Y',376),
 (3239,'3239',' Kodala','Y',376),
 (3240,'3240',' Kodala','Y',376),
 (3241,'3241',' Khalikote','Y',376),
 (3242,'3242',' Khalikote','Y',376),
 (3243,'3243',' Rambha','Y',376),
 (3244,'3244',' Rambha','Y',376),
 (3245,'3245',' Rambha','Y',376),
 (3246,'3246',' Chhatrapur','Y',376),
 (3247,'3247',' Chhatrapur','Y',376),
 (3248,'3248',' Gopalpur','Y',376),
 (3249,'3249',' Gopalpur','Y',376),
 (3250,'3250',' Brahmapur Sadar','Y',376),
 (3251,'3251',' Brahmapur Sadar','Y',376),
 (3252,'3252',' Brahmapur Sadar','Y',376),
 (3253,'3253',' Golanthara','Y',376),
 (3254,'3254',' Golanthara','Y',376),
 (3255,'3255',' Nuagaon','Y',376),
 (3256,'3256',' Nuagaon','Y',376),
 (3257,'3257',' Nuagaon','Y',376),
 (3258,'3258',' Digapahandi','Y',376),
 (3259,'3259',' Digapahandi','Y',376),
 (3260,'3260',' Digapahandi','Y',376),
 (3261,'3261',' Jarada','Y',376),
 (3262,'3262',' Patapur','Y',376),
 (3263,'3263',' Patapur','Y',376),
 (3264,'3264',' Patapur','Y',376),
 (3265,'3265',' Hinjili','Y',376),
 (3266,'3266',' Hinjili','Y',376),
 (3267,'3267',' Hinjili','Y',376),
 (3268,'3268',' Hinjili','Y',376),
 (3269,'3269',' Hinjili','Y',376),
 (3270,'3270',' Ramagiri','Y',376),
 (3271,'3271',' Gajapati  *','Y',377),
 (3272,'3272',' Ramagiri','Y',377),
 (3273,'3273',' Ramagiri','Y',377),
 (3274,'3274',' Adva','Y',377),
 (3275,'3275',' Mohana','Y',377),
 (3276,'3276',' R.Udaygiri','Y',377),
 (3277,'3277',' Garabandha','Y',377),
 (3278,'3278',' Parlakhemundi','Y',377),
 (3279,'3279',' Kashinagara','Y',377),
 (3280,'3280',' Serango','Y',377),
 (3281,'3281',' Serango','Y',377),
 (3282,'3282',' Rayagada','Y',377),
 (3283,'3283',' Kandhamal','Y',378),
 (3284,'3284',' Gochhapada','Y',378),
 (3285,'3285',' Phulabani','Y',378),
 (3286,'3286',' Phulabani Town ','Y',378),
 (3287,'3287',' Khajuripada','Y',378),
 (3288,'3288',' G.Udayagiri','Y',378),
 (3289,'3289',' Tikabali','Y',378),
 (3290,'3290',' Sarangagarh','Y',378),
 (3291,'3291',' Sarangagarh','Y',378),
 (3292,'3292',' Phiringia','Y',378),
 (3293,'3293',' Baliguda','Y',378),
 (3294,'3294',' Tumudibandha','Y',378),
 (3295,'3295',' Belaghar','Y',378),
 (3296,'3296',' Kotagarh','Y',378),
 (3297,'3297',' Brahmanigaon','Y',378),
 (3298,'3298',' Daringbadi','Y',378),
 (3299,'3299',' Raikia','Y',378),
 (3300,'3300',' Baudh  *','Y',379),
 (3301,'3301',' Kantamal','Y',379),
 (3302,'3302',' Manamunda','Y',379),
 (3303,'3303',' Manamunda','Y',379),
 (3304,'3304',' Baunsuni','Y',379),
 (3305,'3305',' Baudh Sadar','Y',379),
 (3306,'3306',' Puruna Katak','Y',379),
 (3307,'3307',' Harbhanga','Y',379),
 (3308,'3308',' Sonapur  *','Y',380),
 (3309,'3309',' Dunguripali','Y',380),
 (3310,'3310',' Dunguripali','Y',380),
 (3311,'3311',' Dunguripali','Y',380),
 (3312,'3312',' Tarbha','Y',380),
 (3313,'3313',' Sonapur','Y',380),
 (3314,'3314',' Biramaharajpur','Y',380),
 (3315,'3315',' Ulunda','Y',380),
 (3316,'3316',' Binika','Y',380),
 (3317,'3317',' Binika','Y',380),
 (3318,'3318',' Rampur','Y',380),
 (3319,'3319',' Rampur','Y',380),
 (3320,'3320',' Balangir','Y',381),
 (3321,'3321',' Khaprakhol','Y',381),
 (3322,'3322',' Turekela','Y',381),
 (3323,'3323',' Belpara','Y',381),
 (3324,'3324',' Kantabanji','Y',381),
 (3325,'3325',' Bangomunda','Y',381),
 (3326,'3326',' Sindhekela','Y',381),
 (3327,'3327',' Sindhekela','Y',381),
 (3328,'3328',' Titlagarh','Y',381),
 (3329,'3329',' Saintala','Y',381),
 (3330,'3330',' Tushura','Y',381),
 (3331,'3331',' Patnagarh','Y',381),
 (3332,'3332',' Balangir','Y',381),
 (3333,'3333',' Balangir','Y',381),
 (3334,'3334',' Loisinga','Y',381),
 (3335,'3335',' Loisinga','Y',381),
 (3336,'3336',' Nuapada  *','Y',382),
 (3337,'3337',' Jonk','Y',382),
 (3338,'3338',' Nuapada','Y',382),
 (3339,'3339',' Komana','Y',382),
 (3340,'3340',' Khariar','Y',382),
 (3341,'3341',' Boden','Y',382),
 (3342,'3342',' Sinapali','Y',382),
 (3343,'3343',' Kalahandi','Y',383),
 (3344,'3344',' Kokasara','Y',383),
 (3345,'3345',' Kokasara','Y',383),
 (3346,'3346',' Dharamgarh','Y',383),
 (3347,'3347',' Kegaon','Y',383),
 (3348,'3348',' Kegaon','Y',383),
 (3349,'3349',' Sadar','Y',383),
 (3350,'3350',' Sadar','Y',383),
 (3351,'3351',' Kesinga','Y',383),
 (3352,'3352',' Kesinga','Y',383),
 (3353,'3353',' Narala','Y',383),
 (3354,'3354',' Narala','Y',383),
 (3355,'3355',' Madanpur Rampur','Y',383),
 (3356,'3356',' Lanjigarh','Y',383),
 (3357,'3357',' Lanjigarh','Y',383),
 (3358,'3358',' Thuamul Rampur','Y',383),
 (3359,'3359',' Junagarh','Y',383),
 (3360,'3360',' Junagarh','Y',383),
 (3361,'3361',' Jayapatna','Y',383),
 (3362,'3362',' Rayagada  *','Y',384),
 (3363,'3363',' Ambadala','Y',384),
 (3364,'3364',' Muniguda','Y',384),
 (3365,'3365',' Muniguda','Y',384),
 (3366,'3366',' Bishamakatak','Y',384),
 (3367,'3367',' Gudari','Y',384),
 (3368,'3368',' Padmapur','Y',384),
 (3369,'3369',' Puttasing','Y',384),
 (3370,'3370',' Gunupur','Y',384),
 (3371,'3371',' Rayagada','Y',384),
 (3372,'3372',' Kalyanasingpur','Y',384),
 (3373,'3373',' Kashipur','Y',384),
 (3374,'3374',' Tikiri','Y',384),
 (3375,'3375',' Nabarangapur  *','Y',385),
 (3376,'3376',' Raighar','Y',385),
 (3377,'3377',' Umarkote','Y',385),
 (3378,'3378',' Chandahandi','Y',385),
 (3379,'3379',' Jharigan','Y',385),
 (3380,'3380',' Jharigan','Y',385),
 (3381,'3381',' Dabugan','Y',385),
 (3382,'3382',' Dabugan','Y',385),
 (3383,'3383',' Paparahandi','Y',385),
 (3384,'3384',' Tentulikhunti','Y',385),
 (3385,'3385',' Khatiguda','Y',385),
 (3386,'3386',' Nabarangapur','Y',385),
 (3387,'3387',' Kodinga','Y',385),
 (3388,'3388',' Koraput','Y',386),
 (3389,'3389',' Kotpad','Y',386),
 (3390,'3390',' Boriguma','Y',386),
 (3391,'3391',' Bhairabsingipur','Y',386),
 (3392,'3392',' Dasamantapur','Y',386),
 (3393,'3393',' Lakshmipur','Y',386),
 (3394,'3394',' Narayanpatana','Y',386),
 (3395,'3395',' Kakiriguma','Y',386),
 (3396,'3396',' Koraput','Y',386),
 (3397,'3397',' Koraput Town','Y',386),
 (3398,'3398',' Nandapur','Y',386),
 (3399,'3399',' Nandapur','Y',386),
 (3400,'3400',' Similiguda','Y',386),
 (3401,'3401',' Damanjodi','Y',386),
 (3402,'3402',' Pottangi','Y',386),
 (3403,'3403',' Padua','Y',386),
 (3404,'3404',' Sunabeda','Y',386),
 (3405,'3405',' Machh kund','Y',386),
 (3406,'3406',' Boipariguda','Y',386),
 (3407,'3407',' Jeypur','Y',386),
 (3408,'3408',' Kundura','Y',386),
 (3409,'3409',' Malkangiri  *','Y',387),
 (3410,'3410',' Malkangiri','Y',387),
 (3411,'3411',' Mathili','Y',387),
 (3412,'3412',' Mudulipada','Y',387),
 (3413,'3413',' Chitrakonda','Y',387),
 (3414,'3414',' Orkel','Y',387),
 (3415,'3415',' Kalimela','Y',387),
 (3416,'3416',' M.V. 79','Y',387),
 (3417,'3417',' Motu','Y',387),
 (3418,'3418',' CHHATTISGARH','Y',388),
 (3419,'3419',' Koriya *','Y',389),
 (3420,'3420',' Bharatpur','Y',389),
 (3421,'3421',' Baikunthpur','Y',389),
 (3422,'3422',' Sonhat','Y',389),
 (3423,'3423',' Manendragarh','Y',389),
 (3424,'3424',' Surguja','Y',390),
 (3425,'3425',' Pal','Y',390),
 (3426,'3426',' Wadrafnagar','Y',390),
 (3427,'3427',' Pratappur','Y',390),
 (3428,'3428',' Samari','Y',390),
 (3429,'3429',' Surajpur','Y',390),
 (3430,'3430',' Ambikapur','Y',390),
 (3431,'3431',' Rajpur','Y',390),
 (3432,'3432',' Lundra','Y',390),
 (3433,'3433',' Sitapur','Y',390),
 (3434,'3434',' Jashpur *','Y',391),
 (3435,'3435',' Bagicha','Y',391),
 (3436,'3436',' Jashpur','Y',391),
 (3437,'3437',' Kunkuri','Y',391),
 (3438,'3438',' Pathalgaon','Y',391),
 (3439,'3439',' Raigarh','Y',392),
 (3440,'3440',' Udaipur (Dharamjaigarh)','Y',392),
 (3441,'3441',' Lailunga','Y',392),
 (3442,'3442',' Gharghoda','Y',392),
 (3443,'3443',' Raigarh','Y',392),
 (3444,'3444',' Kharsia','Y',392),
 (3445,'3445',' Sarangarh','Y',392),
 (3446,'3446',' Korba *','Y',393),
 (3447,'3447',' Katghora','Y',393),
 (3448,'3448',' Pali','Y',393),
 (3449,'3449',' Korba','Y',393),
 (3450,'3450',' Kartala','Y',393),
 (3451,'3451',' Janjgir - Champa*','Y',394),
 (3452,'3452',' Janjgir','Y',394),
 (3453,'3453',' Nawagarh','Y',394),
 (3454,'3454',' Champa','Y',394),
 (3455,'3455',' Sakti','Y',394),
 (3456,'3456',' Pamgarh','Y',394),
 (3457,'3457',' Dabhara','Y',394),
 (3458,'3458',' Malkharoda','Y',394),
 (3459,'3459',' Jaijaipur','Y',394),
 (3460,'3460',' Bilaspur','Y',395),
 (3461,'3461',' Pendraroad','Y',395),
 (3462,'3462',' Lormi','Y',395),
 (3463,'3463',' Kota','Y',395),
 (3464,'3464',' Mungeli','Y',395),
 (3465,'3465',' Takhatpur','Y',395),
 (3466,'3466',' Bilaspur','Y',395),
 (3467,'3467',' Masturi','Y',395),
 (3468,'3468',' Bilha','Y',395),
 (3469,'3469',' Kawardha *','Y',396),
 (3470,'3470',' Kawardha','Y',396),
 (3471,'3471',' Pandariya','Y',396),
 (3472,'3472',' Rajnandgaon','Y',397),
 (3473,'3473',' Chhuikhadan','Y',397),
 (3474,'3474',' Khairagarh','Y',397),
 (3475,'3475',' Dongargarh','Y',397),
 (3476,'3476',' Rajnandgaon','Y',397),
 (3477,'3477',' Dongargaon','Y',397),
 (3478,'3478',' Mohla','Y',397),
 (3479,'3479',' Manpur','Y',397),
 (3480,'3480',' Ambagarh','Y',397),
 (3481,'3481',' Durg','Y',398),
 (3482,'3482',' Nawagarh','Y',398),
 (3483,'3483',' Bemetra','Y',398),
 (3484,'3484',' Saja','Y',398),
 (3485,'3485',' Berla','Y',398),
 (3486,'3486',' Dhamdha','Y',398),
 (3487,'3487',' Durg','Y',398),
 (3488,'3488',' Patan','Y',398),
 (3489,'3489',' Gunderdehi','Y',398),
 (3490,'3490',' Dondiluhara','Y',398),
 (3491,'3491',' Sanjari Balod','Y',398),
 (3492,'3492',' Gurur','Y',398),
 (3493,'3493',' Raipur','Y',399),
 (3494,'3494',' Simga','Y',399),
 (3495,'3495',' Bhatapara','Y',399),
 (3496,'3496',' Baloda Bazar','Y',399),
 (3497,'3497',' Palari','Y',399),
 (3498,'3498',' Kasdol','Y',399),
 (3499,'3499',' Bilaigarh','Y',399),
 (3500,'3500',' Arang','Y',399),
 (3501,'3501',' Abhanpur','Y',399),
 (3502,'3502',' Raipur','Y',399),
 (3503,'3503',' Rajim','Y',399),
 (3504,'3504',' Tilda ','Y',399),
 (3505,'3505',' Bindranawagarh','Y',399),
 (3506,'3506',' Deobhog','Y',399),
 (3507,'3507',' Mahasamund *','Y',400),
 (3508,'3508',' Basna','Y',400),
 (3509,'3509',' Saraipali','Y',400),
 (3510,'3510',' Mahasamund','Y',400),
 (3511,'3511',' Dhamtari *','Y',401),
 (3512,'3512',' Kurud','Y',401),
 (3513,'3513',' Dhamtari','Y',401),
 (3514,'3514',' Nagri','Y',401),
 (3515,'3515',' Kanker *','Y',402),
 (3516,'3516',' Charama','Y',402),
 (3517,'3517',' Bhanupratappur','Y',402),
 (3518,'3518',' Kanker','Y',402),
 (3519,'3519',' Narharpur','Y',402),
 (3520,'3520',' Antagarh','Y',402),
 (3521,'3521',' Pakhanjur','Y',402),
 (3522,'3522',' Bastar','Y',403),
 (3523,'3523',' Keshkal','Y',403),
 (3524,'3524',' Narayanpur','Y',403),
 (3525,'3525',' Kondagaon','Y',403),
 (3526,'3526',' Jagdalpur','Y',403),
 (3527,'3527',' Dantewada*','Y',404),
 (3528,'3528',' Bhopalpattanam (Matdand)','Y',404),
 (3529,'3529',' Bijapur','Y',404),
 (3530,'3530',' Dantewada','Y',404),
 (3531,'3531',' Konta','Y',404),
 (3532,'3532',' MADHYA PRADESH','Y',405),
 (3533,'3533',' Sheopur *','Y',406),
 (3534,'3534',' Vijaypur','Y',406),
 (3535,'3535',' Sheopur','Y',406),
 (3536,'3536',' Karahal','Y',406),
 (3537,'3537',' Morena','Y',407),
 (3538,'3538',' Ambah','Y',407),
 (3539,'3539',' Porsa','Y',407),
 (3540,'3540',' Morena','Y',407),
 (3541,'3541',' Joura','Y',407);
INSERT INTO `taluka` (`id`,`code`,`name`,`active`,`district_id`) VALUES 
 (3542,'3542',' Kailaras','Y',407),
 (3543,'3543',' Sabalgarh','Y',407),
 (3544,'3544',' Bhind','Y',408),
 (3545,'3545',' Ater','Y',408),
 (3546,'3546',' Bhind','Y',408),
 (3547,'3547',' Mehgaon','Y',408),
 (3548,'3548',' Gohad','Y',408),
 (3549,'3549',' Ron','Y',408),
 (3550,'3550',' Mihona','Y',408),
 (3551,'3551',' Lahar','Y',408),
 (3552,'3552',' Gwalior','Y',409),
 (3553,'3553',' Gird','Y',409),
 (3554,'3554',' Pichhore','Y',409),
 (3555,'3555',' Bhitarwar','Y',409),
 (3556,'3556',' Datia','Y',410),
 (3557,'3557',' Seondha','Y',410),
 (3558,'3558',' Datia','Y',410),
 (3559,'3559',' Bhander','Y',410),
 (3560,'3560',' Shivpuri','Y',411),
 (3561,'3561',' Pohari','Y',411),
 (3562,'3562',' Shivpuri','Y',411),
 (3563,'3563',' Narwar','Y',411),
 (3564,'3564',' Karera','Y',411),
 (3565,'3565',' Kolaras','Y',411),
 (3566,'3566',' Pichhore','Y',411),
 (3567,'3567',' Khaniyadhana','Y',411),
 (3568,'3568',' Guna','Y',412),
 (3569,'3569',' Isagarh','Y',412),
 (3570,'3570',' Chanderi','Y',412),
 (3571,'3571',' Guna','Y',412),
 (3572,'3572',' Ashoknagar','Y',412),
 (3573,'3573',' Raghogarh','Y',412),
 (3574,'3574',' Mungaoli','Y',412),
 (3575,'3575',' Kumbhraj','Y',412),
 (3576,'3576',' Aron','Y',412),
 (3577,'3577',' Chachaura','Y',412),
 (3578,'3578',' Tikamgarh','Y',413),
 (3579,'3579',' Niwari','Y',413),
 (3580,'3580',' Prithvipur','Y',413),
 (3581,'3581',' Jatara','Y',413),
 (3582,'3582',' Palera','Y',413),
 (3583,'3583',' Baldeogarh','Y',413),
 (3584,'3584',' Tikamgarh','Y',413),
 (3585,'3585',' Chhatarpur','Y',414),
 (3586,'3586',' Gaurihar','Y',414),
 (3587,'3587',' Laundi','Y',414),
 (3588,'3588',' Nowgaon','Y',414),
 (3589,'3589',' Chhatarpur','Y',414),
 (3590,'3590',' Rajnagar','Y',414),
 (3591,'3591',' Bada-Malhera','Y',414),
 (3592,'3592',' Bijawar','Y',414),
 (3593,'3593',' Panna','Y',415),
 (3594,'3594',' Ajaigarh','Y',415),
 (3595,'3595',' Panna','Y',415),
 (3596,'3596',' Gunnor','Y',415),
 (3597,'3597',' Pawai','Y',415),
 (3598,'3598',' Shahnagar','Y',415),
 (3599,'3599',' Sagar','Y',416),
 (3600,'3600',' Bina','Y',416),
 (3601,'3601',' Khurai','Y',416),
 (3602,'3602',' Banda','Y',416),
 (3603,'3603',' Rahatgarh','Y',416),
 (3604,'3604',' Sagar','Y',416),
 (3605,'3605',' Garhakota','Y',416),
 (3606,'3606',' Rehli','Y',416),
 (3607,'3607',' Kesli','Y',416),
 (3608,'3608',' Deori','Y',416),
 (3609,'3609',' Damoh','Y',417),
 (3610,'3610',' Hatta','Y',417),
 (3611,'3611',' Patera','Y',417),
 (3612,'3612',' Batiyagarh','Y',417),
 (3613,'3613',' Patharia','Y',417),
 (3614,'3614',' Damoh','Y',417),
 (3615,'3615',' Jabera','Y',417),
 (3616,'3616',' Tendukheda','Y',417),
 (3617,'3617',' Satna','Y',418),
 (3618,'3618',' Raghurajnagar','Y',418),
 (3619,'3619',' Nagod','Y',418),
 (3620,'3620',' Unchehara','Y',418),
 (3621,'3621',' Rampur-Baghelan','Y',418),
 (3622,'3622',' Amarpatan','Y',418),
 (3623,'3623',' Ramnagar','Y',418),
 (3624,'3624',' Maihar','Y',418),
 (3625,'3625',' Rewa','Y',419),
 (3626,'3626',' Teonthar','Y',419),
 (3627,'3627',' Sirmour','Y',419),
 (3628,'3628',' Hanumana','Y',419),
 (3629,'3629',' Mauganj','Y',419),
 (3630,'3630',' Huzur','Y',419),
 (3631,'3631',' Raipur - Karchuliyan','Y',419),
 (3632,'3632',' Gurh','Y',419),
 (3633,'3633',' Umaria *','Y',420),
 (3634,'3634',' Bandhogarh','Y',420),
 (3635,'3635',' Shahdol','Y',421),
 (3636,'3636',' Beohari','Y',421),
 (3637,'3637',' Jaisinghnagar','Y',421),
 (3638,'3638',' Sohagpur','Y',421),
 (3639,'3639',' Jaitpur','Y',421),
 (3640,'3640',' Kotma','Y',421),
 (3641,'3641',' Anuppur','Y',421),
 (3642,'3642',' Jaithari','Y',421),
 (3643,'3643',' Pushprajgarh','Y',421),
 (3644,'3644',' Sidhi','Y',422),
 (3645,'3645',' Rampur Naikin','Y',422),
 (3646,'3646',' Churhat','Y',422),
 (3647,'3647',' Gopadbanas','Y',422),
 (3648,'3648',' Sihawal','Y',422),
 (3649,'3649',' Chitrangi','Y',422),
 (3650,'3650',' Deosar','Y',422),
 (3651,'3651',' Majhauli','Y',422),
 (3652,'3652',' Kusmi','Y',422),
 (3653,'3653',' Singrauli','Y',422),
 (3654,'3654',' Neemuch *','Y',423),
 (3655,'3655',' Jawad','Y',423),
 (3656,'3656',' Neemuch','Y',423),
 (3657,'3657',' Manasa','Y',423),
 (3658,'3658',' Mandsaur','Y',424),
 (3659,'3659',' Bhanpura','Y',424),
 (3660,'3660',' Malhargarh','Y',424),
 (3661,'3661',' Garoth','Y',424),
 (3662,'3662',' Mandsaur','Y',424),
 (3663,'3663',' Sitamau','Y',424),
 (3664,'3664',' Ratlam','Y',425),
 (3665,'3665',' Piploda','Y',425),
 (3666,'3666',' Jaora','Y',425),
 (3667,'3667',' Alot','Y',425),
 (3668,'3668',' Sailana','Y',425),
 (3669,'3669',' Bajna','Y',425),
 (3670,'3670',' Ratlam','Y',425),
 (3671,'3671',' Ujjain','Y',426),
 (3672,'3672',' Khacharod','Y',426),
 (3673,'3673',' Nagda','Y',426),
 (3674,'3674',' Mahidpur','Y',426),
 (3675,'3675',' Ghatiya','Y',426),
 (3676,'3676',' Tarana','Y',426),
 (3677,'3677',' Ujjain','Y',426),
 (3678,'3678',' Badnagar','Y',426),
 (3679,'3679',' Shajapur','Y',427),
 (3680,'3680',' Susner','Y',427),
 (3681,'3681',' Nalkheda','Y',427),
 (3682,'3682',' Badod','Y',427),
 (3683,'3683',' Agar','Y',427),
 (3684,'3684',' Shajapur','Y',427),
 (3685,'3685',' Moman Badodiya','Y',427),
 (3686,'3686',' Shujalpur','Y',427),
 (3687,'3687',' Kalapipal','Y',427),
 (3688,'3688',' Dewas','Y',428),
 (3689,'3689',' Tonk Khurd','Y',428),
 (3690,'3690',' Sonkatch','Y',428),
 (3691,'3691',' Dewas','Y',428),
 (3692,'3692',' Kannod','Y',428),
 (3693,'3693',' Bagli','Y',428),
 (3694,'3694',' Khategaon','Y',428),
 (3695,'3695',' Jhabua','Y',429),
 (3696,'3696',' Thandla','Y',429),
 (3697,'3697',' Petlawad','Y',429),
 (3698,'3698',' Meghnagar','Y',429),
 (3699,'3699',' Jhabua','Y',429),
 (3700,'3700',' Bhavra','Y',429),
 (3701,'3701',' Jobat','Y',429),
 (3702,'3702',' Alirajpur','Y',429),
 (3703,'3703',' Ranapur','Y',429),
 (3704,'3704',' Dhar','Y',430),
 (3705,'3705',' Badnawar','Y',430),
 (3706,'3706',' Sardarpur','Y',430),
 (3707,'3707',' Dhar','Y',430),
 (3708,'3708',' Gandhwani','Y',430),
 (3709,'3709',' Kukshi','Y',430),
 (3710,'3710',' Manawar','Y',430),
 (3711,'3711',' Dharampuri','Y',430),
 (3712,'3712',' Indore','Y',431),
 (3713,'3713',' Depalpur','Y',431),
 (3714,'3714',' Sawer','Y',431),
 (3715,'3715',' Indore','Y',431),
 (3716,'3716',' Mhow ','Y',431),
 (3717,'3717',' West Nimar','Y',432),
 (3718,'3718',' Barwaha','Y',432),
 (3719,'3719',' Maheshwar','Y',432),
 (3720,'3720',' Kasrawad','Y',432),
 (3721,'3721',' Segaon','Y',432),
 (3722,'3722',' Bhikangaon','Y',432),
 (3723,'3723',' Khargone','Y',432),
 (3724,'3724',' Bhagwanpura','Y',432),
 (3725,'3725',' Jhiranya','Y',432),
 (3726,'3726',' Barwani *','Y',433),
 (3727,'3727',' Barwani','Y',433),
 (3728,'3728',' Thikri','Y',433),
 (3729,'3729',' Rajpur','Y',433),
 (3730,'3730',' Pansemal','Y',433),
 (3731,'3731',' Niwali','Y',433),
 (3732,'3732',' Sendhwa','Y',433),
 (3733,'3733',' East Nimar','Y',434),
 (3734,'3734',' Harsud','Y',434),
 (3735,'3735',' Khandwa','Y',434),
 (3736,'3736',' Pandhana','Y',434),
 (3737,'3737',' Burhanpur','Y',434),
 (3738,'3738',' Nepanagar','Y',434),
 (3739,'3739',' Rajgarh','Y',435),
 (3740,'3740',' Jirapur','Y',435),
 (3741,'3741',' Khilchipur','Y',435),
 (3742,'3742',' Rajgarh','Y',435),
 (3743,'3743',' Biaora','Y',435),
 (3744,'3744',' Sarangpur','Y',435),
 (3745,'3745',' Narsinghgarh','Y',435),
 (3746,'3746',' Vidisha','Y',436),
 (3747,'3747',' Lateri','Y',436),
 (3748,'3748',' Sironj','Y',436),
 (3749,'3749',' Kurwai','Y',436),
 (3750,'3750',' Basoda','Y',436),
 (3751,'3751',' Nateran','Y',436),
 (3752,'3752',' Gyaraspur','Y',436),
 (3753,'3753',' Vidisha','Y',436),
 (3754,'3754',' Bhopal','Y',437),
 (3755,'3755',' Berasia','Y',437),
 (3756,'3756',' Huzur','Y',437),
 (3757,'3757',' Sehore','Y',438),
 (3758,'3758',' Sehore','Y',438),
 (3759,'3759',' Ashta','Y',438),
 (3760,'3760',' Ichhawar','Y',438),
 (3761,'3761',' Nasrullaganj','Y',438),
 (3762,'3762',' Budni','Y',438),
 (3763,'3763',' Raisen','Y',439),
 (3764,'3764',' Raisen','Y',439),
 (3765,'3765',' Gairatganj','Y',439),
 (3766,'3766',' Begamganj','Y',439),
 (3767,'3767',' Goharganj','Y',439),
 (3768,'3768',' Baraily','Y',439),
 (3769,'3769',' Silwani','Y',439),
 (3770,'3770',' Udaipura','Y',439),
 (3771,'3771',' Betul','Y',440),
 (3772,'3772',' Bhainsdehi','Y',440),
 (3773,'3773',' Betul ','Y',440),
 (3774,'3774',' Shahpur','Y',440),
 (3775,'3775',' Multai','Y',440),
 (3776,'3776',' Amla','Y',440),
 (3777,'3777',' Harda *','Y',441),
 (3778,'3778',' Khirkiya','Y',441),
 (3779,'3779',' Harda','Y',441),
 (3780,'3780',' Timarni','Y',441),
 (3781,'3781',' Hoshangabad','Y',442),
 (3782,'3782',' Seoni-Malwa','Y',442),
 (3783,'3783',' Itarsi','Y',442),
 (3784,'3784',' Hoshangabad','Y',442),
 (3785,'3785',' Babai','Y',442),
 (3786,'3786',' Sohagpur','Y',442),
 (3787,'3787',' Pipariya','Y',442),
 (3788,'3788',' Bankhedi','Y',442),
 (3789,'3789',' Katni *','Y',443),
 (3790,'3790',' Murwara','Y',443),
 (3791,'3791',' Vijayraghavgarh','Y',443),
 (3792,'3792',' Bahoriband','Y',443),
 (3793,'3793',' Dhimar Kheda','Y',443),
 (3794,'3794',' Jabalpur','Y',444),
 (3795,'3795',' Sihora','Y',444),
 (3796,'3796',' Patan','Y',444),
 (3797,'3797',' Jabalpur','Y',444),
 (3798,'3798',' Kundam','Y',444),
 (3799,'3799',' Narsimhapur','Y',445),
 (3800,'3800',' Gotegaon','Y',445),
 (3801,'3801',' Gadarwara','Y',445),
 (3802,'3802',' Narsimhapur','Y',445),
 (3803,'3803',' Kareli','Y',445),
 (3804,'3804',' Tendukheda','Y',445),
 (3805,'3805',' Dindori *','Y',446),
 (3806,'3806',' Shahpura','Y',446),
 (3807,'3807',' Dindori ','Y',446),
 (3808,'3808',' Mandla','Y',447),
 (3809,'3809',' Niwas','Y',447),
 (3810,'3810',' Mandla','Y',447),
 (3811,'3811',' Bichhiya','Y',447),
 (3812,'3812',' Nainpur','Y',447),
 (3813,'3813',' Chhindwara','Y',448),
 (3814,'3814',' Tamia','Y',448),
 (3815,'3815',' Amarwara','Y',448),
 (3816,'3816',' Chaurai','Y',448),
 (3817,'3817',' Jamai','Y',448),
 (3818,'3818',' Parasia','Y',448),
 (3819,'3819',' Chhindwara','Y',448),
 (3820,'3820',' Sausar','Y',448),
 (3821,'3821',' Bichhua','Y',448),
 (3822,'3822',' Pandhurna','Y',448),
 (3823,'3823',' Seoni','Y',449),
 (3824,'3824',' Lakhnadon','Y',449),
 (3825,'3825',' Ghansor','Y',449),
 (3826,'3826',' Keolari','Y',449),
 (3827,'3827',' Seoni','Y',449),
 (3828,'3828',' Barghat','Y',449),
 (3829,'3829',' Kurai','Y',449),
 (3830,'3830',' Balaghat','Y',450),
 (3831,'3831',' Katangi','Y',450),
 (3832,'3832',' Waraseoni','Y',450),
 (3833,'3833',' Balaghat','Y',450),
 (3834,'3834',' Kirnapur','Y',450),
 (3835,'3835',' Baihar','Y',450),
 (3836,'3836',' Lanji','Y',450),
 (3837,'3837',' GUJARAT','Y',451),
 (3838,'3838',' Kachchh','Y',452),
 (3839,'3839',' Lakhpat','Y',452),
 (3840,'3840',' Rapar','Y',452),
 (3841,'3841',' Bhachau','Y',452),
 (3842,'3842',' Anjar','Y',452),
 (3843,'3843',' Bhuj','Y',452),
 (3844,'3844',' Nakhatrana','Y',452),
 (3845,'3845',' Abdasa','Y',452),
 (3846,'3846',' Mandvi','Y',452),
 (3847,'3847',' Mundra','Y',452),
 (3848,'3848',' Gandhidham','Y',452),
 (3849,'3849',' Banas Kantha','Y',453),
 (3850,'3850',' Vav','Y',453),
 (3851,'3851',' Tharad','Y',453),
 (3852,'3852',' Dhanera','Y',453),
 (3853,'3853',' Dantiwada','Y',453),
 (3854,'3854',' Amirgadh','Y',453),
 (3855,'3855',' Danta','Y',453),
 (3856,'3856',' Vadgam','Y',453),
 (3857,'3857',' Palanpur','Y',453),
 (3858,'3858',' Deesa','Y',453),
 (3859,'3859',' Deodar','Y',453),
 (3860,'3860',' Bhabhar','Y',453),
 (3861,'3861',' Kankrej','Y',453),
 (3862,'3862',' Patan  *','Y',454),
 (3863,'3863',' Santalpur','Y',454),
 (3864,'3864',' Radhanpur','Y',454),
 (3865,'3865',' Vagdod','Y',454),
 (3866,'3866',' Sidhpur','Y',454),
 (3867,'3867',' Patan','Y',454),
 (3868,'3868',' Harij','Y',454),
 (3869,'3869',' Sami','Y',454),
 (3870,'3870',' Chanasma','Y',454),
 (3871,'3871',' Mahesana','Y',455),
 (3872,'3872',' Satlasana','Y',455),
 (3873,'3873',' Kheralu','Y',455),
 (3874,'3874',' Unjha','Y',455),
 (3875,'3875',' Visnagar','Y',455),
 (3876,'3876',' Vadnagar','Y',455),
 (3877,'3877',' Vijapur','Y',455),
 (3878,'3878',' Mahesana','Y',455),
 (3879,'3879',' Becharaji','Y',455),
 (3880,'3880',' Kadi','Y',455),
 (3881,'3881',' Sabar Kantha','Y',456),
 (3882,'3882',' Khedbrahma','Y',456),
 (3883,'3883',' Vijaynagar','Y',456),
 (3884,'3884',' Vadali','Y',456),
 (3885,'3885',' Idar','Y',456),
 (3886,'3886',' Bhiloda','Y',456),
 (3887,'3887',' Meghraj','Y',456),
 (3888,'3888',' Himatnagar','Y',456),
 (3889,'3889',' Prantij','Y',456),
 (3890,'3890',' Talod','Y',456),
 (3891,'3891',' Modasa','Y',456),
 (3892,'3892',' Dhansura','Y',456),
 (3893,'3893',' Malpur','Y',456),
 (3894,'3894',' Bayad','Y',456),
 (3895,'3895',' Gandhinagar','Y',457),
 (3896,'3896',' Kalol','Y',457),
 (3897,'3897',' Mansa','Y',457),
 (3898,'3898',' Gandhinagar','Y',457),
 (3899,'3899',' Dehgam','Y',457),
 (3900,'3900',' Ahmadabad','Y',458),
 (3901,'3901',' Mandal','Y',458),
 (3902,'3902',' Detroj-Rampura','Y',458),
 (3903,'3903',' Viramgam','Y',458),
 (3904,'3904',' Sanand','Y',458),
 (3905,'3905',' Ahmadabad City','Y',458),
 (3906,'3906',' Daskroi','Y',458),
 (3907,'3907',' Dholka','Y',458),
 (3908,'3908',' Bavla','Y',458),
 (3909,'3909',' Ranpur','Y',458),
 (3910,'3910',' Barwala','Y',458),
 (3911,'3911',' Dhandhuka','Y',458),
 (3912,'3912',' Surendranagar','Y',459),
 (3913,'3913',' Halvad','Y',459),
 (3914,'3914',' Dhrangadhra','Y',459),
 (3915,'3915',' Dasada','Y',459),
 (3916,'3916',' Lakhtar','Y',459),
 (3917,'3917',' Wadhwan','Y',459),
 (3918,'3918',' Muli','Y',459),
 (3919,'3919',' Chotila','Y',459),
 (3920,'3920',' Sayla','Y',459),
 (3921,'3921',' Chuda','Y',459),
 (3922,'3922',' Limbdi','Y',459),
 (3923,'3923',' Rajkot','Y',460),
 (3924,'3924',' Maliya','Y',460),
 (3925,'3925',' Morvi','Y',460),
 (3926,'3926',' Tankara','Y',460),
 (3927,'3927',' Wankaner','Y',460),
 (3928,'3928',' Paddhari','Y',460),
 (3929,'3929',' Rajkot','Y',460),
 (3930,'3930',' Lodhika','Y',460),
 (3931,'3931',' Kotda Sangani','Y',460),
 (3932,'3932',' Jasdan','Y',460),
 (3933,'3933',' Gondal','Y',460),
 (3934,'3934',' Jamkandorna','Y',460),
 (3935,'3935',' Upleta','Y',460),
 (3936,'3936',' Dhoraji','Y',460),
 (3937,'3937',' Jetpur','Y',460),
 (3938,'3938',' Jamnagar','Y',461),
 (3939,'3939',' Okhamandal','Y',461),
 (3940,'3940',' Khambhalia','Y',461),
 (3941,'3941',' Jamnagar','Y',461),
 (3942,'3942',' Jodiya','Y',461),
 (3943,'3943',' Dhrol','Y',461),
 (3944,'3944',' Kalavad','Y',461),
 (3945,'3945',' Lalpur','Y',461),
 (3946,'3946',' Kalyanpur','Y',461),
 (3947,'3947',' Bhanvad','Y',461),
 (3948,'3948',' Jamjodhpur','Y',461),
 (3949,'3949',' Porbandar  *','Y',462),
 (3950,'3950',' Porbandar','Y',462),
 (3951,'3951',' Ranavav','Y',462),
 (3952,'3952',' Kutiyana','Y',462),
 (3953,'3953',' Junagadh','Y',463),
 (3954,'3954',' Manavadar','Y',463),
 (3955,'3955',' Vanthali','Y',463),
 (3956,'3956',' Junagadh','Y',463),
 (3957,'3957',' Bhesan','Y',463),
 (3958,'3958',' Visavadar','Y',463),
 (3959,'3959',' Mendarda','Y',463),
 (3960,'3960',' Keshod','Y',463),
 (3961,'3961',' Mangrol','Y',463),
 (3962,'3962',' Malia','Y',463),
 (3963,'3963',' Talala','Y',463),
 (3964,'3964',' Patan-Veraval','Y',463),
 (3965,'3965',' Sutrapada','Y',463),
 (3966,'3966',' Kodinar','Y',463),
 (3967,'3967',' Una','Y',463),
 (3968,'3968',' Amreli','Y',464),
 (3969,'3969',' Kunkavav Vadia','Y',464),
 (3970,'3970',' Babra','Y',464),
 (3971,'3971',' Lathi','Y',464),
 (3972,'3972',' Lilia','Y',464),
 (3973,'3973',' Amreli','Y',464),
 (3974,'3974',' Bagasara','Y',464),
 (3975,'3975',' Dhari','Y',464),
 (3976,'3976',' Savar Kundla','Y',464),
 (3977,'3977',' Khambha','Y',464),
 (3978,'3978',' Jafrabad','Y',464),
 (3979,'3979',' Rajula','Y',464),
 (3980,'3980',' Bhavnagar','Y',465),
 (3981,'3981',' Botad','Y',465),
 (3982,'3982',' Vallabhipur','Y',465),
 (3983,'3983',' Gadhada','Y',465),
 (3984,'3984',' Umrala','Y',465),
 (3985,'3985',' Bhavnagar','Y',465),
 (3986,'3986',' Ghogha','Y',465),
 (3987,'3987',' Sihor','Y',465),
 (3988,'3988',' Gariadhar','Y',465),
 (3989,'3989',' Palitana','Y',465),
 (3990,'3990',' Talaja','Y',465),
 (3991,'3991',' Mahuva','Y',465),
 (3992,'3992',' Anand  *','Y',466),
 (3993,'3993',' Tarapur','Y',466),
 (3994,'3994',' Sojitra','Y',466),
 (3995,'3995',' Umreth','Y',466),
 (3996,'3996',' Anand','Y',466),
 (3997,'3997',' Petlad','Y',466),
 (3998,'3998',' Khambhat','Y',466),
 (3999,'3999',' Borsad','Y',466),
 (4000,'4000',' Anklav','Y',466),
 (4001,'4001',' Kheda','Y',467),
 (4002,'4002',' Kapadvanj','Y',467),
 (4003,'4003',' Virpur','Y',467),
 (4004,'4004',' Balasinor','Y',467),
 (4005,'4005',' Kathlal','Y',467),
 (4006,'4006',' Mehmedabad','Y',467),
 (4007,'4007',' Kheda','Y',467),
 (4008,'4008',' Matar','Y',467),
 (4009,'4009',' Nadiad','Y',467),
 (4010,'4010',' Mahudha','Y',467),
 (4011,'4011',' Thasra','Y',467),
 (4012,'4012',' Panch Mahals','Y',468),
 (4013,'4013',' Khanpur','Y',468),
 (4014,'4014',' Kadana','Y',468),
 (4015,'4015',' Santrampur','Y',468),
 (4016,'4016',' Lunawada','Y',468),
 (4017,'4017',' Shehera','Y',468),
 (4018,'4018',' Morwa (Hadaf)','Y',468),
 (4019,'4019',' Godhra','Y',468),
 (4020,'4020',' Kalol','Y',468),
 (4021,'4021',' Ghoghamba','Y',468),
 (4022,'4022',' Halol','Y',468),
 (4023,'4023',' Jambughoda','Y',468),
 (4024,'4024',' Dohad  *','Y',469),
 (4025,'4025',' Fatepura','Y',469),
 (4026,'4026',' Jhalod','Y',469),
 (4027,'4027',' Limkheda','Y',469),
 (4028,'4028',' Dohad','Y',469),
 (4029,'4029',' Garbada','Y',469),
 (4030,'4030',' Devgadbaria','Y',469),
 (4031,'4031',' Dhanpur','Y',469),
 (4032,'4032',' Vadodara','Y',470),
 (4033,'4033',' Savli','Y',470),
 (4034,'4034',' Vadodara','Y',470),
 (4035,'4035',' Vaghodia','Y',470),
 (4036,'4036',' Jetpur Pavi','Y',470),
 (4037,'4037',' Chhota Udaipur','Y',470),
 (4038,'4038',' Kavant','Y',470),
 (4039,'4039',' Nasvadi','Y',470),
 (4040,'4040',' Sankheda','Y',470),
 (4041,'4041',' Dabhoi','Y',470),
 (4042,'4042',' Padra','Y',470),
 (4043,'4043',' Karjan','Y',470),
 (4044,'4044',' Sinor','Y',470),
 (4045,'4045',' Narmada  *','Y',471),
 (4046,'4046',' Tilakwada','Y',471),
 (4047,'4047',' Nandod','Y',471),
 (4048,'4048',' Dediapada','Y',471),
 (4049,'4049',' Sagbara','Y',471),
 (4050,'4050',' Bharuch','Y',472),
 (4051,'4051',' Jambusar','Y',472),
 (4052,'4052',' Amod','Y',472),
 (4053,'4053',' Vagra','Y',472),
 (4054,'4054',' Bharuch','Y',472),
 (4055,'4055',' Jhagadia','Y',472),
 (4056,'4056',' Anklesvar','Y',472),
 (4057,'4057',' Hansot','Y',472),
 (4058,'4058',' Valia','Y',472),
 (4059,'4059',' Surat','Y',473),
 (4060,'4060',' Olpad','Y',473),
 (4061,'4061',' Mangrol','Y',473),
 (4062,'4062',' Umarpada','Y',473),
 (4063,'4063',' Nizar','Y',473),
 (4064,'4064',' Uchchhal','Y',473),
 (4065,'4065',' Songadh','Y',473),
 (4066,'4066',' Mandvi','Y',473),
 (4067,'4067',' Kamrej','Y',473),
 (4068,'4068',' Surat City','Y',473),
 (4069,'4069',' Chorasi','Y',473),
 (4070,'4070',' Palsana','Y',473),
 (4071,'4071',' Bardoli','Y',473),
 (4072,'4072',' Vyara','Y',473),
 (4073,'4073',' Valod','Y',473),
 (4074,'4074',' Mahuva','Y',473),
 (4075,'4075',' The Dangs','Y',474),
 (4076,'4076',' The Dangs','Y',474),
 (4077,'4077',' Navsari  *','Y',475),
 (4078,'4078',' Navsari','Y',475),
 (4079,'4079',' Jalalpore','Y',475),
 (4080,'4080',' Gandevi','Y',475),
 (4081,'4081',' Chikhli','Y',475),
 (4082,'4082',' Bansda','Y',475),
 (4083,'4083',' Valsad','Y',476),
 (4084,'4084',' Valsad','Y',476),
 (4085,'4085',' Dharampur','Y',476),
 (4086,'4086',' Pardi','Y',476),
 (4087,'4087',' Kaprada','Y',476),
 (4088,'4088',' Umbergaon','Y',476),
 (4089,'4089',' DAMAN & DIU','Y',477),
 (4090,'4090',' Diu','Y',478),
 (4091,'4091',' Diu','Y',478),
 (4092,'4092',' Daman','Y',479),
 (4093,'4093',' Daman','Y',479),
 (4094,'4094',' DADRA & NAGAR HAVELI','Y',480),
 (4095,'4095',' Dadra & Nagar Haveli','Y',481),
 (4096,'4096',' Dadra & Nagar Haveli','Y',481),
 (4098,'4098',' Nandurbar *','Y',483),
 (4099,'4099',' Akkalkuwa','Y',483),
 (4100,'4100',' Akrani','Y',483),
 (4101,'4101',' Talode','Y',483),
 (4102,'4102',' Shahade','Y',483),
 (4103,'4103',' Nandurbar','Y',483),
 (4104,'4104',' Nawapur','Y',483),
 (4105,'4105',' Dhule','Y',484),
 (4106,'4106',' Shirpur','Y',484),
 (4107,'4107',' Sindkhede','Y',484),
 (4108,'4108',' Sakri','Y',484),
 (4109,'4109',' Dhule','Y',484),
 (4110,'4110',' Jalgaon','Y',485),
 (4111,'4111',' Chopda','Y',485),
 (4112,'4112',' Yawal','Y',485),
 (4113,'4113',' Raver','Y',485),
 (4114,'4114',' Edlabad (Muktainagar)','Y',485),
 (4115,'4115',' Bodvad','Y',485),
 (4116,'4116',' Bhusawal','Y',485),
 (4117,'4117',' Jalgaon','Y',485),
 (4118,'4118',' Erandol','Y',485),
 (4119,'4119',' Dharangaon','Y',485),
 (4120,'4120',' Amalner','Y',485),
 (4121,'4121',' Parola','Y',485),
 (4122,'4122',' Bhadgaon','Y',485),
 (4123,'4123',' Chalqfixaon','Y',485),
 (4124,'4124',' Pachora','Y',485),
 (4125,'4125',' Jamner','Y',485),
 (4126,'4126',' Buldana','Y',486),
 (4127,'4127',' Jalgaon (Jamod)','Y',486),
 (4128,'4128',' Sangrampur','Y',486),
 (4129,'4129',' Shegaon','Y',486),
 (4130,'4130',' Nandura','Y',486),
 (4131,'4131',' Malkapur','Y',486),
 (4132,'4132',' Motala','Y',486),
 (4133,'4133',' Khamgaon','Y',486),
 (4134,'4134',' Mehkar','Y',486),
 (4135,'4135',' Chikhli','Y',486),
 (4136,'4136',' Buldana','Y',486),
 (4137,'4137',' Deolgaon Raja','Y',486),
 (4138,'4138',' Sindkhed Raja','Y',486),
 (4139,'4139',' Lonar','Y',486),
 (4140,'4140',' Akola','Y',487),
 (4141,'4141',' Telhara','Y',487),
 (4142,'4142',' Akot','Y',487),
 (4143,'4143',' Balapur','Y',487),
 (4144,'4144',' Akola','Y',487),
 (4145,'4145',' Murtijapur','Y',487),
 (4146,'4146',' Patur','Y',487),
 (4147,'4147',' Barshitakli','Y',487),
 (4148,'4148',' Washim *','Y',488),
 (4149,'4149',' Malegaon','Y',488),
 (4150,'4150',' Mangrulpir','Y',488),
 (4151,'4151',' Karanja','Y',488),
 (4152,'4152',' Manora','Y',488),
 (4153,'4153',' Washim','Y',488),
 (4154,'4154',' Risod','Y',488),
 (4155,'4155',' Amravati','Y',489),
 (4156,'4156',' Dharni','Y',489),
 (4157,'4157',' Chikhaldara','Y',489),
 (4158,'4158',' Anjangaon Surji','Y',489),
 (4159,'4159',' Achalpur','Y',489),
 (4160,'4160',' Chandurbazar','Y',489),
 (4161,'4161',' Morshi','Y',489),
 (4162,'4162',' Warud','Y',489),
 (4163,'4163',' Teosa','Y',489),
 (4164,'4164',' Amravati','Y',489),
 (4165,'4165',' Bhatkuli','Y',489),
 (4166,'4166',' Daryapur','Y',489),
 (4167,'4167',' Nandgaon-Khandeshwar','Y',489),
 (4168,'4168',' Chandur Railway','Y',489),
 (4169,'4169',' Dhamangaon Railway','Y',489),
 (4170,'4170',' Wardha','Y',490),
 (4171,'4171',' Ashti','Y',490),
 (4172,'4172',' Karanja','Y',490),
 (4173,'4173',' Arvi','Y',490),
 (4174,'4174',' Seloo','Y',490),
 (4175,'4175',' Wardha','Y',490),
 (4176,'4176',' Deoli','Y',490),
 (4177,'4177',' Hinganghat','Y',490),
 (4178,'4178',' Samudrapur','Y',490),
 (4179,'4179',' Nagpur','Y',491),
 (4180,'4180',' Narkhed','Y',491),
 (4181,'4181',' Katol','Y',491),
 (4182,'4182',' Kalameshwar','Y',491),
 (4183,'4183',' Savner','Y',491),
 (4184,'4184',' Parseoni','Y',491),
 (4185,'4185',' Ramtek','Y',491),
 (4186,'4186',' Mauda','Y',491),
 (4187,'4187',' Kamptee','Y',491),
 (4188,'4188',' Nagpur (Rural)','Y',491),
 (4189,'4189',' Nagpur (Urban)','Y',491),
 (4190,'4190',' Hingna','Y',491),
 (4191,'4191',' Umred','Y',491),
 (4192,'4192',' Kuhi','Y',491),
 (4193,'4193',' Bhiwapur','Y',491),
 (4194,'4194',' Bhandara','Y',492),
 (4195,'4195',' Tumsar','Y',492),
 (4196,'4196',' Mohadi','Y',492),
 (4197,'4197',' Bhandara','Y',492),
 (4198,'4198',' Sakoli','Y',492),
 (4199,'4199',' Lakhani','Y',492),
 (4200,'4200',' Pauni','Y',492),
 (4201,'4201',' Lakhandur','Y',492),
 (4202,'4202',' Gondiya *','Y',493),
 (4203,'4203',' Tirora','Y',493),
 (4204,'4204',' Goregaon','Y',493),
 (4205,'4205',' Gondiya','Y',493),
 (4206,'4206',' Amgaon','Y',493),
 (4207,'4207',' Salekasa','Y',493),
 (4208,'4208',' Sadak-Arjuni','Y',493),
 (4209,'4209',' Arjuni Morgaon','Y',493),
 (4210,'4210',' Deori','Y',493),
 (4211,'4211',' Gadchiroli','Y',494),
 (4212,'4212',' Desaiganj (Vadasa)','Y',494),
 (4213,'4213',' Armori','Y',494),
 (4214,'4214',' Kurkheda','Y',494),
 (4215,'4215',' Korchi','Y',494),
 (4216,'4216',' Dhanora','Y',494),
 (4217,'4217',' Gadchiroli','Y',494),
 (4218,'4218',' Chamorshi','Y',494),
 (4219,'4219',' Mulchera','Y',494),
 (4220,'4220',' Etapalli','Y',494),
 (4221,'4221',' Bhamragad','Y',494),
 (4222,'4222',' Aheri','Y',494),
 (4223,'4223',' Sironcha','Y',494),
 (4224,'4224',' Chandrapur','Y',495),
 (4225,'4225',' Warora','Y',495),
 (4226,'4226',' Chimur','Y',495),
 (4227,'4227',' Nagbhir','Y',495),
 (4228,'4228',' Brahmapuri','Y',495),
 (4229,'4229',' Sawali','Y',495),
 (4230,'4230',' Sindewahi','Y',495),
 (4231,'4231',' Bhadravati','Y',495),
 (4232,'4232',' Chandrapur','Y',495),
 (4233,'4233',' Mul','Y',495),
 (4234,'4234',' Pombhurna','Y',495),
 (4235,'4235',' Ballarpur','Y',495),
 (4236,'4236',' Korpana','Y',495),
 (4237,'4237',' Rajura','Y',495),
 (4238,'4238',' Gondpipri','Y',495),
 (4239,'4239',' Yavatmal','Y',496),
 (4240,'4240',' Ner','Y',496),
 (4241,'4241',' Babulgaon','Y',496),
 (4242,'4242',' Kalamb','Y',496),
 (4243,'4243',' Yavatmal','Y',496),
 (4244,'4244',' Darwha','Y',496),
 (4245,'4245',' Digras','Y',496),
 (4246,'4246',' Pusad','Y',496),
 (4247,'4247',' Umarkhed','Y',496),
 (4248,'4248',' Mahagaon','Y',496),
 (4249,'4249',' Arni','Y',496),
 (4250,'4250',' Ghatanji','Y',496),
 (4251,'4251',' Kelapur','Y',496),
 (4252,'4252',' Ralegaon','Y',496),
 (4253,'4253',' Maregaon','Y',496),
 (4254,'4254',' Zari-Jamani','Y',496),
 (4255,'4255',' Wani','Y',496),
 (4256,'4256',' Nanded','Y',497),
 (4257,'4257',' Mahoor','Y',497),
 (4258,'4258',' Kinwat','Y',497),
 (4259,'4259',' Himayatnagar','Y',497),
 (4260,'4260',' Hadgaon','Y',497),
 (4261,'4261',' Ardhapur','Y',497),
 (4262,'4262',' Nanded','Y',497),
 (4263,'4263',' Mudkhed','Y',497),
 (4264,'4264',' Bhokar','Y',497),
 (4265,'4265',' Umri','Y',497),
 (4266,'4266',' Dharmabad','Y',497),
 (4267,'4267',' Biloli','Y',497),
 (4268,'4268',' Naigaon (Khairgaon)','Y',497),
 (4269,'4269',' Loha','Y',497),
 (4270,'4270',' Kandhar','Y',497),
 (4271,'4271',' Mukhed','Y',497),
 (4272,'4272',' Deglur','Y',497),
 (4273,'4273',' Hingoli *','Y',498),
 (4274,'4274',' Sengaon','Y',498),
 (4275,'4275',' Hingoli','Y',498),
 (4276,'4276',' Aundha (Nagnath)','Y',498),
 (4277,'4277',' Kalamnuri','Y',498),
 (4278,'4278',' Basmath','Y',498),
 (4279,'4279',' Parbhani','Y',499),
 (4280,'4280',' Sailu','Y',499),
 (4281,'4281',' Jintur','Y',499),
 (4282,'4282',' Parbhani','Y',499),
 (4283,'4283',' Manwath','Y',499),
 (4284,'4284',' Pathri','Y',499),
 (4285,'4285',' Sonpeth','Y',499),
 (4286,'4286',' Gangakhed','Y',499),
 (4287,'4287',' Palam','Y',499),
 (4288,'4288',' Purna','Y',499),
 (4289,'4289',' Jalna','Y',500),
 (4290,'4290',' Bhokardan','Y',500),
 (4291,'4291',' Jafferabad','Y',500),
 (4292,'4292',' Jalna','Y',500),
 (4293,'4293',' Badnapur','Y',500),
 (4294,'4294',' Ambad','Y',500),
 (4295,'4295',' Ghansawangi','Y',500),
 (4296,'4296',' Partur','Y',500),
 (4297,'4297',' Mantha','Y',500),
 (4298,'4298',' Aurangabad','Y',501),
 (4299,'4299',' Kannad','Y',501),
 (4300,'4300',' Soegaon','Y',501),
 (4301,'4301',' Sillod','Y',501),
 (4302,'4302',' Phulambri','Y',501),
 (4303,'4303',' Aurangabad','Y',501),
 (4304,'4304',' Khuldabad','Y',501),
 (4305,'4305',' Vaijapur','Y',501),
 (4306,'4306',' Gangapur','Y',501),
 (4307,'4307',' Paithan','Y',501),
 (4308,'4308',' Nashik','Y',502),
 (4309,'4309',' Surgana','Y',502),
 (4310,'4310',' Kalwan','Y',502),
 (4311,'4311',' Deola','Y',502),
 (4312,'4312',' Baglan','Y',502),
 (4313,'4313',' Malegaon','Y',502),
 (4314,'4314',' Nandgaon','Y',502),
 (4315,'4315',' Chandvad','Y',502),
 (4316,'4316',' Dindori','Y',502),
 (4317,'4317',' Peint','Y',502),
 (4318,'4318',' Trimbakeshwar','Y',502),
 (4319,'4319',' Nashik','Y',502),
 (4320,'4320',' Igatpuri','Y',502),
 (4321,'4321',' Sinnar','Y',502),
 (4322,'4322',' Niphad','Y',502),
 (4323,'4323',' Yevla','Y',502),
 (4324,'4324',' Thane','Y',503),
 (4325,'4325',' Talasari','Y',503),
 (4326,'4326',' Dahanu','Y',503),
 (4327,'4327',' Vikramgad','Y',503),
 (4328,'4328',' Jawhar','Y',503),
 (4329,'4329',' Mokhada','Y',503),
 (4330,'4330',' Vada','Y',503),
 (4331,'4331',' Palghar','Y',503),
 (4332,'4332',' Vasai','Y',503),
 (4333,'4333',' Thane','Y',503),
 (4334,'4334',' Bhiwandi','Y',503),
 (4335,'4335',' Shahapur','Y',503),
 (4336,'4336',' Kalyan','Y',503),
 (4337,'4337',' Ulhasnagar','Y',503),
 (4338,'4338',' Ambarnath','Y',503),
 (4339,'4339',' Murbad','Y',503),
 (4340,'4340',' Mumbai (Suburban) *','Y',504),
 (4341,'4341',' Mumbai','Y',505),
 (4342,'4342',' Raigarh','Y',506),
 (4343,'4343',' Uran','Y',506),
 (4344,'4344',' Panvel','Y',506),
 (4345,'4345',' Karjat','Y',506),
 (4346,'4346',' Khalapur','Y',506),
 (4347,'4347',' Pen','Y',506),
 (4348,'4348',' Alibag','Y',506),
 (4349,'4349',' Murud','Y',506),
 (4350,'4350',' Roha','Y',506),
 (4351,'4351',' Sudhagad','Y',506),
 (4352,'4352',' Mangaon','Y',506),
 (4353,'4353',' Tala','Y',506),
 (4354,'4354',' Shrivardhan','Y',506),
 (4355,'4355',' Mhasla','Y',506),
 (4356,'4356',' Mahad','Y',506),
 (4357,'4357',' Poladpur','Y',506),
 (4358,'4358',' Pune','Y',507),
 (4359,'4359',' Junnar','Y',507),
 (4360,'4360',' Ambegaon','Y',507),
 (4361,'4361',' Shirur','Y',507),
 (4362,'4362',' Khed','Y',507),
 (4363,'4363',' Mawal','Y',507),
 (4364,'4364',' Mulshi','Y',507),
 (4365,'4365',' Haveli','Y',507),
 (4366,'4366',' Pune City','Y',507),
 (4367,'4367',' Daund','Y',507),
 (4368,'4368',' Purandhar','Y',507),
 (4369,'4369',' Velhe','Y',507),
 (4370,'4370',' Bhor','Y',507),
 (4371,'4371',' Baramati','Y',507),
 (4372,'4372',' Indapur','Y',507),
 (4373,'4373',' Ahmadnagar','Y',508),
 (4374,'4374',' Akola','Y',508),
 (4375,'4375',' Sangamner','Y',508),
 (4376,'4376',' Kopargaon','Y',508),
 (4377,'4377',' Rahta','Y',508),
 (4378,'4378',' Shrirampur','Y',508),
 (4379,'4379',' Nevasa','Y',508),
 (4380,'4380',' Shevgaon','Y',508),
 (4381,'4381',' Pathardi','Y',508),
 (4382,'4382',' Nagar','Y',508),
 (4383,'4383',' Rahuri','Y',508),
 (4384,'4384',' Parner','Y',508),
 (4385,'4385',' Shrigonda','Y',508),
 (4386,'4386',' Karjat','Y',508),
 (4387,'4387',' Jamkhed','Y',508),
 (4388,'4388',' Bid','Y',509),
 (4389,'4389',' Ashti','Y',509),
 (4390,'4390',' Patoda','Y',509),
 (4391,'4391',' Shirur (Kasar)','Y',509),
 (4392,'4392',' Georai','Y',509),
 (4393,'4393',' Manjalegaon','Y',509),
 (4394,'4394',' Wadwani','Y',509),
 (4395,'4395',' Bid','Y',509),
 (4396,'4396',' Kaij','Y',509),
 (4397,'4397',' Dharur','Y',509),
 (4398,'4398',' Parli','Y',509),
 (4399,'4399',' Ambejogai','Y',509),
 (4400,'4400',' Latur','Y',510),
 (4401,'4401',' Latur','Y',510),
 (4402,'4402',' Renapur','Y',510),
 (4403,'4403',' Ahmadpur','Y',510),
 (4404,'4404',' Jalkot','Y',510),
 (4405,'4405',' Chakur','Y',510),
 (4406,'4406',' Shirur-Anantpal','Y',510),
 (4407,'4407',' Ausa','Y',510),
 (4408,'4408',' Nilanga','Y',510),
 (4409,'4409',' Deoni','Y',510),
 (4410,'4410',' Udgir','Y',510),
 (4411,'4411',' Osmanabad','Y',511),
 (4412,'4412',' Paranda','Y',511),
 (4413,'4413',' Bhum','Y',511),
 (4414,'4414',' Washi','Y',511),
 (4415,'4415',' Kalamb','Y',511),
 (4416,'4416',' Osmanabad','Y',511),
 (4417,'4417',' Tuljapur','Y',511),
 (4418,'4418',' Lohara','Y',511),
 (4419,'4419',' Umarga','Y',511),
 (4420,'4420',' Solapur','Y',512),
 (4421,'4421',' Karmala','Y',512),
 (4422,'4422',' Madha','Y',512),
 (4423,'4423',' Barshi','Y',512),
 (4424,'4424',' Solapur North','Y',512),
 (4425,'4425',' Mohol','Y',512),
 (4426,'4426',' Pandharpur','Y',512),
 (4427,'4427',' Malshiras','Y',512),
 (4428,'4428',' Sangole','Y',512),
 (4429,'4429',' Mangalvedhe','Y',512),
 (4430,'4430',' Solapur South','Y',512),
 (4431,'4431',' Akkalkot','Y',512),
 (4432,'4432',' Satara','Y',513),
 (4433,'4433',' Mahabaleshwar','Y',513),
 (4434,'4434',' Wai','Y',513),
 (4435,'4435',' Khandala','Y',513),
 (4436,'4436',' Phaltan','Y',513),
 (4437,'4437',' Man','Y',513),
 (4438,'4438',' Khatav','Y',513),
 (4439,'4439',' Koregaon','Y',513),
 (4440,'4440',' Satara','Y',513),
 (4441,'4441',' Jaoli','Y',513),
 (4442,'4442',' Patan','Y',513),
 (4443,'4443',' Karad','Y',513),
 (4444,'4444',' Ratnagiri','Y',514),
 (4445,'4445',' Mandangad','Y',514),
 (4446,'4446',' Dapoli','Y',514),
 (4447,'4447',' Khed','Y',514),
 (4448,'4448',' Chiplun','Y',514),
 (4449,'4449',' Guhagar','Y',514),
 (4450,'4450',' Ratnagiri','Y',514),
 (4451,'4451',' Sangameshwar','Y',514),
 (4452,'4452',' Lanja','Y',514),
 (4453,'4453',' Rajapur','Y',514),
 (4454,'4454',' Sindhudurg','Y',515),
 (4455,'4455',' Devgad','Y',515),
 (4456,'4456',' Vaibhavvadi','Y',515),
 (4457,'4457',' Kankavli','Y',515),
 (4458,'4458',' Malwan','Y',515),
 (4459,'4459',' Vengurla','Y',515),
 (4460,'4460',' Kudal','Y',515),
 (4461,'4461',' Sawantwadi','Y',515),
 (4462,'4462',' Dodamarg','Y',515),
 (4463,'4463',' Kolhapur','Y',516),
 (4464,'4464',' Shahuwadi','Y',516),
 (4465,'4465',' Panhala','Y',516),
 (4466,'4466',' Hatkanangle','Y',516),
 (4467,'4467',' Shirol','Y',516),
 (4468,'4468',' Karvir','Y',516),
 (4469,'4469',' Bavda','Y',516),
 (4470,'4470',' Radhanagari','Y',516),
 (4471,'4471',' Kagal','Y',516),
 (4472,'4472',' Bhudargad','Y',516),
 (4473,'4473',' Ajra','Y',516),
 (4474,'4474',' Gadhinglaj','Y',516),
 (4475,'4475',' Chandgad','Y',516),
 (4476,'4476',' Sangli','Y',517),
 (4477,'4477',' Shirala','Y',517),
 (4478,'4478',' Walwa','Y',517),
 (4479,'4479',' Palus','Y',517),
 (4480,'4480',' Khanapur','Y',517),
 (4481,'4481',' Atpadi','Y',517),
 (4482,'4482',' Tasgaon','Y',517),
 (4483,'4483',' Miraj','Y',517),
 (4484,'4484',' Kavathe-Mahankal','Y',517),
 (4485,'4485',' Jat','Y',517),
 (4486,'4486',' ANDHRA PRADESH','Y',518),
 (4487,'4487',' Adilabad','Y',519),
 (4488,'4488',' Tamsi','Y',519),
 (4489,'4489',' Adilabad','Y',519),
 (4490,'4490',' Jainad','Y',519),
 (4491,'4491',' Bela','Y',519),
 (4492,'4492',' Talamadugu','Y',519),
 (4493,'4493',' Gudihatnoor','Y',519),
 (4494,'4494',' Indervelly','Y',519),
 (4495,'4495',' Narnoor','Y',519),
 (4496,'4496',' Kerameri ','Y',519),
 (4497,'4497',' Wankidi','Y',519),
 (4498,'4498',' Sirpur (T)','Y',519),
 (4499,'4499',' Koutala','Y',519),
 (4500,'4500',' Bejjur','Y',519),
 (4501,'4501',' Kaghaznagar ','Y',519),
 (4502,'4502',' Asifabad ','Y',519),
 (4503,'4503',' Jainoor','Y',519),
 (4504,'4504',' Utnoor','Y',519),
 (4505,'4505',' Ichoda','Y',519),
 (4506,'4506',' Bazarhatnoor','Y',519),
 (4507,'4507',' Boath','Y',519),
 (4508,'4508',' Neeradigonda','Y',519),
 (4509,'4509',' Sirpur','Y',519),
 (4510,'4510',' Rebbena','Y',519),
 (4511,'4511',' Bhimini','Y',519),
 (4512,'4512',' Dahegaon','Y',519),
 (4513,'4513',' Vemanpalle','Y',519),
 (4514,'4514',' Nennel','Y',519),
 (4515,'4515',' Tandur','Y',519),
 (4516,'4516',' Tiryani','Y',519),
 (4517,'4517',' Jannaram','Y',519),
 (4518,'4518',' Kaddam (Peddur)','Y',519),
 (4519,'4519',' Sarangapur','Y',519),
 (4520,'4520',' Kuntala','Y',519),
 (4521,'4521',' Kubeer','Y',519),
 (4522,'4522',' Bhainsa','Y',519),
 (4523,'4523',' Thanoor','Y',519),
 (4524,'4524',' Mudhole','Y',519),
 (4525,'4525',' Lokeshwaram','Y',519),
 (4526,'4526',' Dilawarpur','Y',519),
 (4527,'4527',' Nirmal','Y',519),
 (4528,'4528',' Laxmanchanda','Y',519),
 (4529,'4529',' Mamda','Y',519),
 (4530,'4530',' Khanapur','Y',519),
 (4531,'4531',' Dandepalle','Y',519),
 (4532,'4532',' Kasipet','Y',519),
 (4533,'4533',' Bellampalle','Y',519),
 (4534,'4534',' Kotapalle','Y',519),
 (4535,'4535',' Mandamarri','Y',519),
 (4536,'4536',' Luxettipet','Y',519),
 (4537,'4537',' Mancherial','Y',519),
 (4538,'4538',' Jaipur','Y',519),
 (4539,'4539',' Chennur','Y',519),
 (4540,'4540',' Nizamabad','Y',520),
 (4541,'4541',' Ranjal','Y',520),
 (4542,'4542',' Navipet','Y',520),
 (4543,'4543',' Nandipet','Y',520),
 (4544,'4544',' Armur','Y',520),
 (4545,'4545',' Balkonda','Y',520),
 (4546,'4546',' Mortad','Y',520),
 (4547,'4547',' Kammar palle','Y',520),
 (4548,'4548',' Bheemgal','Y',520),
 (4549,'4549',' Velpur','Y',520),
 (4550,'4550',' Jakranpalle','Y',520),
 (4551,'4551',' Makloor','Y',520),
 (4552,'4552',' Nizamabad','Y',520),
 (4553,'4553',' Yedpalle','Y',520),
 (4554,'4554',' Bodhan','Y',520),
 (4555,'4555',' Kotgiri','Y',520),
 (4556,'4556',' Madnoor','Y',520),
 (4557,'4557',' Jukkal','Y',520),
 (4558,'4558',' Bichkunda','Y',520),
 (4559,'4559',' Birkoor','Y',520),
 (4560,'4560',' Varni','Y',520),
 (4561,'4561',' Dichpalle','Y',520),
 (4562,'4562',' Dharpalle','Y',520),
 (4563,'4563',' Sirkonda','Y',520),
 (4564,'4564',' Machareddy','Y',520),
 (4565,'4565',' Sadasivanagar','Y',520),
 (4566,'4566',' Gandhari','Y',520),
 (4567,'4567',' Banswada','Y',520),
 (4568,'4568',' Pitlam','Y',520),
 (4569,'4569',' Nizamsagar','Y',520),
 (4570,'4570',' Yellareddy','Y',520),
 (4571,'4571',' Nagareddypet','Y',520),
 (4572,'4572',' Lingampet','Y',520),
 (4573,'4573',' Tadwai','Y',520),
 (4574,'4574',' Kamareddy','Y',520),
 (4575,'4575',' Bhiknur','Y',520),
 (4576,'4576',' Domakonda','Y',520),
 (4577,'4577',' Karimnagar','Y',521),
 (4578,'4578',' Ibrahimpatnam','Y',521),
 (4579,'4579',' Mallapur','Y',521),
 (4580,'4580',' Raikal','Y',521),
 (4581,'4581',' Sarangapur','Y',521),
 (4582,'4582',' Dharmapuri','Y',521),
 (4583,'4583',' Velgatoor','Y',521),
 (4584,'4584',' Ramagundam','Y',521),
 (4585,'4585',' Kamanpur','Y',521),
 (4586,'4586',' Manthani','Y',521),
 (4587,'4587',' Kataram','Y',521),
 (4588,'4588',' Mahadevpur','Y',521),
 (4589,'4589',' Mutharam (N)','Y',521),
 (4590,'4590',' Malharrao mandal','Y',521),
 (4591,'4591',' Mutharam (A)','Y',521),
 (4592,'4592',' Srirampur','Y',521),
 (4593,'4593',' Peddapalle','Y',521),
 (4594,'4594',' Julapalle','Y',521),
 (4595,'4595',' Bommareddypalle (H/o.Dharmaram)','Y',521),
 (4596,'4596',' Gollapalle','Y',521),
 (4597,'4597',' Mallial','Y',521),
 (4598,'4598',' Jagtial','Y',521),
 (4599,'4599',' Medipalle','Y',521),
 (4600,'4600',' Koratla','Y',521),
 (4601,'4601',' Metpalle','Y',521),
 (4602,'4602',' Kathlapur','Y',521),
 (4603,'4603',' Chandurthi','Y',521),
 (4604,'4604',' Kodimial','Y',521),
 (4605,'4605',' Pegadapalle','Y',521),
 (4606,'4606',' Gangadhara','Y',521),
 (4607,'4607',' Ramadugu','Y',521),
 (4608,'4608',' Choppadandi','Y',521),
 (4609,'4609',' Sulthanabad','Y',521),
 (4610,'4610',' Odela','Y',521),
 (4611,'4611',' Manakondur','Y',521),
 (4612,'4612',' Karimnagar','Y',521),
 (4613,'4613',' Boinpalle','Y',521),
 (4614,'4614',' Vemulawada','Y',521),
 (4615,'4615',' Konaraopeta','Y',521),
 (4616,'4616',' Yellareddipet','Y',521),
 (4617,'4617',' Gambhiraopet','Y',521),
 (4618,'4618',' Mustabad','Y',521),
 (4619,'4619',' Sirsilla','Y',521),
 (4620,'4620',' Ellanthakunta','Y',521),
 (4621,'4621',' Bejjanki','Y',521),
 (4622,'4622',' Thimmapur (LMD)','Y',521),
 (4623,'4623',' Veenavanka','Y',521),
 (4624,'4624',' Jammikunta','Y',521),
 (4625,'4625',' Shankarapeta kesavapatnam','Y',521),
 (4626,'4626',' Chigurumamidi','Y',521),
 (4627,'4627',' Koheda','Y',521),
 (4628,'4628',' Husnabad','Y',521),
 (4629,'4629',' Saidapur','Y',521),
 (4630,'4630',' Huzurabad','Y',521),
 (4631,'4631',' Kamalapur','Y',521),
 (4632,'4632',' Bheemadevarpalle','Y',521),
 (4633,'4633',' Elkathurthy','Y',521),
 (4634,'4634',' Medak','Y',522),
 (4635,'4635',' Kangti','Y',522),
 (4636,'4636',' Manoor','Y',522),
 (4637,'4637',' Narayankhed','Y',522),
 (4638,'4638',' Kalher','Y',522),
 (4639,'4639',' Shankarampet (A)','Y',522),
 (4640,'4640',' Papannapet','Y',522),
 (4641,'4641',' Medak','Y',522),
 (4642,'4642',' Ramayampet','Y',522),
 (4643,'4643',' Dubbak','Y',522),
 (4644,'4644',' Siddipet','Y',522),
 (4645,'4645',' Chinnakodur','Y',522),
 (4646,'4646',' Nangnoor','Y',522),
 (4647,'4647',' Kondapak','Y',522),
 (4648,'4648',' Mirdoddi','Y',522),
 (4649,'4649',' Doulthabad','Y',522),
 (4650,'4650',' Chegunta','Y',522),
 (4651,'4651',' Shankarampet (R)','Y',522),
 (4652,'4652',' Kulcharam','Y',522),
 (4653,'4653',' Tekmal','Y',522),
 (4654,'4654',' Alladurg','Y',522),
 (4655,'4655',' Regode','Y',522),
 (4656,'4656',' Raikode','Y',522),
 (4657,'4657',' Nyalkal','Y',522),
 (4658,'4658',' Zahirabad','Y',522),
 (4659,'4659',' Kohir','Y',522),
 (4660,'4660',' Jharasangam','Y',522),
 (4661,'4661',' Munipalle','Y',522),
 (4662,'4662',' Pulkal','Y',522),
 (4663,'4663',' Andole','Y',522),
 (4664,'4664',' Kowdipalle','Y',522),
 (4665,'4665',' Veldurthy','Y',522),
 (4666,'4666',' Tupran','Y',522),
 (4667,'4667',' Gajwel','Y',522),
 (4668,'4668',' Jagdevpur','Y',522),
 (4669,'4669',' Wargal','Y',522),
 (4670,'4670',' Mulugu','Y',522),
 (4671,'4671',' Shivampet','Y',522),
 (4672,'4672',' Narsapur','Y',522),
 (4673,'4673',' Hathnoora','Y',522),
 (4674,'4674',' Sadasivpet','Y',522),
 (4675,'4675',' Kondapoor','Y',522),
 (4676,'4676',' Sangareddy','Y',522),
 (4677,'4677',' Jinnaram','Y',522),
 (4678,'4678',' Patancheru','Y',522),
 (4679,'4679',' Ramchandrapuram','Y',522),
 (4680,'4680',' Hyderabad','Y',523),
 (4681,'4681',' Shaikpet','Y',523),
 (4682,'4682',' Ameerpet','Y',523),
 (4683,'4683',' Secunderabad','Y',523),
 (4684,'4684',' Tirumalagiri','Y',523),
 (4685,'4685',' Maredpalle','Y',523),
 (4686,'4686',' Musheerabad','Y',523),
 (4687,'4687',' Amberpet','Y',523),
 (4688,'4688',' Himayathnagar','Y',523),
 (4689,'4689',' Nampally','Y',523),
 (4690,'4690',' Khairatabad','Y',523),
 (4691,'4691',' Asifnagar','Y',523),
 (4692,'4692',' Golconda','Y',523),
 (4693,'4693',' Bahadurpura','Y',523),
 (4694,'4694',' Bandlaguda','Y',523),
 (4695,'4695',' Charminar','Y',523),
 (4696,'4696',' Saidabad','Y',523),
 (4697,'4697',' Rangareddi','Y',524),
 (4698,'4698',' Marpalle','Y',524),
 (4699,'4699',' Mominpet','Y',524),
 (4700,'4700',' Nawabpet','Y',524),
 (4701,'4701',' Shankarpalle','Y',524),
 (4702,'4702',' Serilingampalle','Y',524),
 (4703,'4703',' Balanagar','Y',524),
 (4704,'4704',' Quthbullapur','Y',524),
 (4705,'4705',' Medchal','Y',524),
 (4706,'4706',' Shamirpet','Y',524),
 (4707,'4707',' Malkajgiri','Y',524),
 (4708,'4708',' Keesara','Y',524),
 (4709,'4709',' Ghatkesar','Y',524),
 (4710,'4710',' Uppal kalan ','Y',524),
 (4711,'4711',' Hayathnagar','Y',524),
 (4712,'4712',' Saroornagar','Y',524),
 (4713,'4713',' Rajendranagar','Y',524),
 (4714,'4714',' Moinabad','Y',524),
 (4715,'4715',' Chevella','Y',524),
 (4716,'4716',' Vicarabad','Y',524),
 (4717,'4717',' Dharur','Y',524),
 (4718,'4718',' Bantwaram','Y',524),
 (4719,'4719',' Peddemul','Y',524),
 (4720,'4720',' Tandur','Y',524),
 (4721,'4721',' Basheerabad','Y',524),
 (4722,'4722',' Yalal','Y',524),
 (4723,'4723',' Doma','Y',524),
 (4724,'4724',' Gandeed','Y',524),
 (4725,'4725',' Kulkacharla','Y',524),
 (4726,'4726',' Pargi','Y',524),
 (4727,'4727',' Pudur','Y',524),
 (4728,'4728',' Shabad','Y',524),
 (4729,'4729',' Shamshabad','Y',524),
 (4730,'4730',' Maheswaram','Y',524),
 (4731,'4731',' Kandukur','Y',524),
 (4732,'4732',' Ibrahimpatnam','Y',524),
 (4733,'4733',' Manchal','Y',524),
 (4734,'4734',' Yacharam','Y',524),
 (4735,'4735',' Mahbubnagar','Y',525),
 (4736,'4736',' Kodangal','Y',525),
 (4737,'4737',' Bomraspet','Y',525),
 (4738,'4738',' Kosgi','Y',525),
 (4739,'4739',' Doulathabad','Y',525),
 (4740,'4740',' Damaragidda','Y',525),
 (4741,'4741',' Maddur','Y',525),
 (4742,'4742',' Hanwada','Y',525),
 (4743,'4743',' Nawabpet','Y',525),
 (4744,'4744',' Balanagar','Y',525),
 (4745,'4745',' Kondurg','Y',525),
 (4746,'4746',' Farooqnagar','Y',525),
 (4747,'4747',' Kothur','Y',525),
 (4748,'4748',' Keshampet','Y',525),
 (4749,'4749',' Talakondapalle','Y',525),
 (4750,'4750',' Amangal','Y',525),
 (4751,'4751',' Madgul','Y',525),
 (4752,'4752',' Veldanda','Y',525),
 (4753,'4753',' Midjil','Y',525),
 (4754,'4754',' Jadcharla','Y',525),
 (4755,'4755',' Mahabubnagar','Y',525),
 (4756,'4756',' Koilkonda','Y',525),
 (4757,'4757',' Narayanpet','Y',525),
 (4758,'4758',' Utkoor','Y',525),
 (4759,'4759',' Dhanwada','Y',525),
 (4760,'4760',' Devarkadra','Y',525),
 (4761,'4761',' Bhoothpur','Y',525),
 (4762,'4762',' Thimmajipet','Y',525),
 (4763,'4763',' Kalwakurthy','Y',525),
 (4764,'4764',' Vangoor','Y',525),
 (4765,'4765',' Amrabad','Y',525),
 (4766,'4766',' Achampet','Y',525),
 (4767,'4767',' Uppununthala','Y',525),
 (4768,'4768',' Telkapalle','Y',525),
 (4769,'4769',' Tadoor','Y',525),
 (4770,'4770',' Nagarkurnool','Y',525),
 (4771,'4771',' Bijinapalle','Y',525),
 (4772,'4772',' Ghanpur','Y',525),
 (4773,'4773',' Addakal','Y',525),
 (4774,'4774',' Chinnachintakunta','Y',525),
 (4775,'4775',' Narwa','Y',525),
 (4776,'4776',' Makthal','Y',525),
 (4777,'4777',' Maganoor','Y',525),
 (4778,'4778',' Dharur','Y',525),
 (4779,'4779',' Atmakur','Y',525),
 (4780,'4780',' Kothakota','Y',525),
 (4781,'4781',' Peddamandadi','Y',525),
 (4782,'4782',' Wanaparthy','Y',525),
 (4783,'4783',' Gopalpeta','Y',525),
 (4784,'4784',' Balmoor','Y',525),
 (4785,'4785',' Lingal','Y',525),
 (4786,'4786',' Peddakothapalle','Y',525),
 (4787,'4787',' Kodair','Y',525),
 (4788,'4788',' Pangal','Y',525),
 (4789,'4789',' Pebbair','Y',525),
 (4790,'4790',' Gadwal','Y',525),
 (4791,'4791',' Maldakal','Y',525),
 (4792,'4792',' Ghattu','Y',525),
 (4793,'4793',' Aiza','Y',525),
 (4794,'4794',' Itikyal','Y',525),
 (4795,'4795',' Weepangandla','Y',525),
 (4796,'4796',' Kollapur','Y',525),
 (4797,'4797',' Waddepalle','Y',525),
 (4798,'4798',' Manopad','Y',525),
 (4799,'4799',' Alampur','Y',525),
 (4800,'4800',' Nalgonda','Y',526),
 (4801,'4801',' Bommalaramaram','Y',526),
 (4802,'4802',' M.Turkapalle','Y',526),
 (4803,'4803',' Rajapet','Y',526),
 (4804,'4804',' Yadagirigutta','Y',526),
 (4805,'4805',' Alair','Y',526),
 (4806,'4806',' Gundala','Y',526),
 (4807,'4807',' Thirumalgiri','Y',526),
 (4808,'4808',' Thungathurthi','Y',526),
 (4809,'4809',' Nuthankal','Y',526),
 (4810,'4810',' Atmakur (S)','Y',526),
 (4811,'4811',' Jaji reddi gudem','Y',526),
 (4812,'4812',' Sali gouraram','Y',526),
 (4813,'4813',' Mothkur','Y',526),
 (4814,'4814',' Atmakur (M)','Y',526),
 (4815,'4815',' Valigonda','Y',526),
 (4816,'4816',' Bhuvanagiri','Y',526),
 (4817,'4817',' Bibinagar','Y',526),
 (4818,'4818',' Pochampalle','Y',526),
 (4819,'4819',' Choutuppal','Y',526),
 (4820,'4820',' Ramannapeta','Y',526),
 (4821,'4821',' Chityala','Y',526),
 (4822,'4822',' Narketpalle','Y',526),
 (4823,'4823',' Kattangoor','Y',526),
 (4824,'4824',' Nakrekal','Y',526),
 (4825,'4825',' Kethe palle','Y',526),
 (4826,'4826',' Suryapet','Y',526),
 (4827,'4827',' Chivvemla','Y',526),
 (4828,'4828',' Mothey','Y',526),
 (4829,'4829',' Nadigudem','Y',526),
 (4830,'4830',' Munagala','Y',526),
 (4831,'4831',' Penpahad','Y',526),
 (4832,'4832',' Vemulapalle','Y',526),
 (4833,'4833',' Thipparthi','Y',526),
 (4834,'4834',' Nalgonda','Y',526),
 (4835,'4835',' Munugode','Y',526),
 (4836,'4836',' Narayanpur','Y',526),
 (4837,'4837',' Marriguda','Y',526),
 (4838,'4838',' Chintha palle','Y',526),
 (4839,'4839',' Gundla palle','Y',526),
 (4840,'4840',' Chandam pet','Y',526),
 (4841,'4841',' Devarkonda','Y',526),
 (4842,'4842',' Nampalle','Y',526),
 (4843,'4843',' Chandur','Y',526),
 (4844,'4844',' Kangal','Y',526),
 (4845,'4845',' Gurrampode','Y',526),
 (4846,'4846',' Pedda adiserla palle','Y',526),
 (4847,'4847',' Peddavoora','Y',526),
 (4848,'4848',' Anumula','Y',526),
 (4849,'4849',' Nidamanur','Y',526),
 (4850,'4850',' Thripuraram','Y',526),
 (4851,'4851',' Damaracherla','Y',526),
 (4852,'4852',' Miryalaguda','Y',526),
 (4853,'4853',' Nereducherla','Y',526),
 (4854,'4854',' Garide palle','Y',526),
 (4855,'4855',' Chilkur','Y',526),
 (4856,'4856',' Kodad','Y',526),
 (4857,'4857',' Huzurnagar','Y',526),
 (4858,'4858',' Mattam palle','Y',526),
 (4859,'4859',' Mella cheruvu','Y',526),
 (4860,'4860',' Warangal','Y',527),
 (4861,'4861',' Cherial','Y',527),
 (4862,'4862',' Maddur','Y',527),
 (4863,'4863',' Bachannapet','Y',527),
 (4864,'4864',' Narmetta','Y',527),
 (4865,'4865',' Ghanpur (Station)','Y',527),
 (4866,'4866',' Dharamsagar','Y',527),
 (4867,'4867',' Hasanparthy','Y',527),
 (4868,'4868',' Parakal','Y',527),
 (4869,'4869',' Mogullapalle','Y',527),
 (4870,'4870',' Chityal','Y',527),
 (4871,'4871',' Bhupalpalle','Y',527),
 (4872,'4872',' Ghanpur (Mulug)','Y',527),
 (4873,'4873',' Venkatapur','Y',527),
 (4874,'4874',' Eturnagaram','Y',527),
 (4875,'4875',' Mangapet','Y',527),
 (4876,'4876',' Tadvai','Y',527),
 (4877,'4877',' Govindaraopet','Y',527),
 (4878,'4878',' Mulug','Y',527),
 (4879,'4879',' Regonda','Y',527),
 (4880,'4880',' Shyampet','Y',527),
 (4881,'4881',' Nallabelly','Y',527),
 (4882,'4882',' Duggondi','Y',527),
 (4883,'4883',' Atmakur','Y',527),
 (4884,'4884',' Hanamkonda','Y',527),
 (4885,'4885',' Zaffergadh','Y',527),
 (4886,'4886',' Palakurthi','Y',527),
 (4887,'4887',' Raghunathpalle','Y',527),
 (4888,'4888',' Jangaon','Y',527),
 (4889,'4889',' Lingalaghanpur','Y',527),
 (4890,'4890',' Devaruppula','Y',527),
 (4891,'4891',' Kodakandla','Y',527),
 (4892,'4892',' Raiparthy','Y',527),
 (4893,'4893',' Wardhanna pet','Y',527),
 (4894,'4894',' Sangam','Y',527),
 (4895,'4895',' Warangal','Y',527),
 (4896,'4896',' Geesugonda','Y',527),
 (4897,'4897',' Narsampet','Y',527),
 (4898,'4898',' Khanapur','Y',527),
 (4899,'4899',' Kothagudem','Y',527),
 (4900,'4900',' Gudur','Y',527),
 (4901,'4901',' Chennaraopet','Y',527),
 (4902,'4902',' Nekkonda','Y',527),
 (4903,'4903',' Parvathagiri','Y',527),
 (4904,'4904',' Thorrur','Y',527),
 (4905,'4905',' Nellikudur','Y',527),
 (4906,'4906',' Kesamudram','Y',527),
 (4907,'4907',' Mahabubabad','Y',527),
 (4908,'4908',' Narsimhulapet','Y',527),
 (4909,'4909',' Maripeda','Y',527),
 (4910,'4910',' Kuravi','Y',527),
 (4911,'4911',' Dornakal','Y',527),
 (4912,'4912',' Khammam','Y',528),
 (4913,'4913',' Wazeed','Y',528),
 (4914,'4914',' Venkatapuram','Y',528),
 (4915,'4915',' Pinapaka','Y',528),
 (4916,'4916',' Cherla','Y',528),
 (4917,'4917',' Manugur','Y',528),
 (4918,'4918',' Aswapuram','Y',528),
 (4919,'4919',' Dummugudem','Y',528),
 (4920,'4920',' Bhadrachalam','Y',528),
 (4921,'4921',' Kunavaram','Y',528),
 (4922,'4922',' Chintur','Y',528),
 (4923,'4923',' Vararamachandrapuram','Y',528),
 (4924,'4924',' Velairpad','Y',528),
 (4925,'4925',' Kukunoor','Y',528),
 (4926,'4926',' Burgumpahad','Y',528),
 (4927,'4927',' Palwancha','Y',528),
 (4928,'4928',' Kothagudem','Y',528),
 (4929,'4929',' Tekulapalle','Y',528),
 (4930,'4930',' Yellandu','Y',528),
 (4931,'4931',' Gundala','Y',528),
 (4932,'4932',' Bayyaram','Y',528),
 (4933,'4933',' Garla','Y',528),
 (4934,'4934',' Singareni','Y',528),
 (4935,'4935',' Kamepalle','Y',528),
 (4936,'4936',' Julurpadu','Y',528),
 (4937,'4937',' Chandrugonda','Y',528),
 (4938,'4938',' Mulkalapalle','Y',528),
 (4939,'4939',' Aswaraopet','Y',528),
 (4940,'4940',' Dammapet','Y',528),
 (4941,'4941',' Sathupalle','Y',528),
 (4942,'4942',' Penuballe','Y',528),
 (4943,'4943',' Enkuru','Y',528),
 (4944,'4944',' Tirumalayapalem','Y',528),
 (4945,'4945',' Kusumanchi','Y',528),
 (4946,'4946',' Khammam (Rural)','Y',528),
 (4947,'4947',' Khammam (Urban)','Y',528),
 (4948,'4948',' Mudigonda','Y',528),
 (4949,'4949',' Nelakondapalle','Y',528),
 (4950,'4950',' Chinthakani','Y',528),
 (4951,'4951',' Konijerla','Y',528),
 (4952,'4952',' Tallada','Y',528),
 (4953,'4953',' Kallur','Y',528),
 (4954,'4954',' Wyra','Y',528),
 (4955,'4955',' Bonakal','Y',528),
 (4956,'4956',' Madhira','Y',528),
 (4957,'4957',' Yerrupalem','Y',528),
 (4958,'4958',' Vemsoor','Y',528),
 (4959,'4959',' Srikakulam','Y',529),
 (4960,'4960',' Veeraghattam','Y',529),
 (4961,'4961',' Seethampeta','Y',529),
 (4962,'4962',' Bhamini','Y',529),
 (4963,'4963',' Kothuru','Y',529),
 (4964,'4964',' Pathapatnam','Y',529),
 (4965,'4965',' Meliaputtu','Y',529),
 (4966,'4966',' Palasa','Y',529),
 (4967,'4967',' Mandasa','Y',529),
 (4968,'4968',' Kanchili','Y',529),
 (4969,'4969',' Ichchapuram','Y',529),
 (4970,'4970',' Kaviti','Y',529),
 (4971,'4971',' Sompeta','Y',529),
 (4972,'4972',' Vajrapukothuru','Y',529),
 (4973,'4973',' Nandigam','Y',529),
 (4974,'4974',' Hiramandalam','Y',529),
 (4975,'4975',' Palakonda','Y',529),
 (4976,'4976',' Vangara','Y',529),
 (4977,'4977',' Regidi Amadalavalasa','Y',529),
 (4978,'4978',' Laxminarasupeta','Y',529),
 (4979,'4979',' Saravakota','Y',529),
 (4980,'4980',' Tekkali','Y',529),
 (4981,'4981',' Santhabommali','Y',529),
 (4982,'4982',' Kotabommili','Y',529),
 (4983,'4983',' Jalumuru','Y',529),
 (4984,'4984',' Sarubujjili','Y',529),
 (4985,'4985',' Burja','Y',529),
 (4986,'4986',' Santhakaviti','Y',529),
 (4987,'4987',' Rajam','Y',529),
 (4988,'4988',' Ganguvarisigadam','Y',529),
 (4989,'4989',' Amadalavalasa','Y',529),
 (4990,'4990',' Narasannapeta','Y',529),
 (4991,'4991',' Polaki','Y',529),
 (4992,'4992',' Gara','Y',529),
 (4993,'4993',' Srikakulam','Y',529),
 (4994,'4994',' Ponduru','Y',529),
 (4995,'4995',' Laveru','Y',529),
 (4996,'4996',' Ranasthalam','Y',529),
 (4997,'4997',' Etcherla','Y',529),
 (4998,'4998',' Vizianagaram','Y',530),
 (4999,'4999',' Komarada','Y',530),
 (5000,'5000',' Gummalakshmipuram','Y',530),
 (5001,'5001',' Kurupam','Y',530),
 (5002,'5002',' Jiyyammavalasa','Y',530),
 (5003,'5003',' Garugubilli','Y',530),
 (5004,'5004',' Parvathipuram','Y',530),
 (5005,'5005',' Makkuva','Y',530),
 (5006,'5006',' Seethanagaram','Y',530),
 (5007,'5007',' Balijipeta','Y',530),
 (5008,'5008',' Bobbili','Y',530),
 (5009,'5009',' Salur','Y',530),
 (5010,'5010',' Pachipenta','Y',530),
 (5011,'5011',' Ramabhadrapuram','Y',530),
 (5012,'5012',' Badangi','Y',530),
 (5013,'5013',' Therlam','Y',530),
 (5014,'5014',' Merakamudidam','Y',530),
 (5015,'5015',' Dathirajeru','Y',530),
 (5016,'5016',' Mentada','Y',530),
 (5017,'5017',' Gajapathinagaram','Y',530),
 (5018,'5018',' Garividi','Y',530),
 (5019,'5019',' Cheepurupalle','Y',530),
 (5020,'5020',' Gurla','Y',530),
 (5021,'5021',' Bondapalle','Y',530),
 (5022,'5022',' Gantyada','Y',530),
 (5023,'5023',' Srungavarapukota','Y',530),
 (5024,'5024',' Vepada','Y',530),
 (5025,'5025',' Lakkavarapukota','Y',530),
 (5026,'5026',' Kothavalasa','Y',530),
 (5027,'5027',' Jami','Y',530),
 (5028,'5028',' Vizianagaram','Y',530),
 (5029,'5029',' Nellimarla','Y',530),
 (5030,'5030',' Pusapatirega','Y',530),
 (5031,'5031',' Denkada','Y',530),
 (5032,'5032',' Bhogapuram','Y',530),
 (5033,'5033',' Visakhapatnam','Y',531),
 (5034,'5034',' Munchingi puttu','Y',531),
 (5035,'5035',' Peda bayalu','Y',531),
 (5036,'5036',' Dumbriguda','Y',531),
 (5037,'5037',' Araku valley','Y',531),
 (5038,'5038',' Ananthagiri','Y',531),
 (5039,'5039',' Hukumpeta','Y',531),
 (5040,'5040',' Paderu','Y',531),
 (5041,'5041',' G.madugula','Y',531),
 (5042,'5042',' Chintapalle','Y',531),
 (5043,'5043',' Gudem kotha veedhi','Y',531),
 (5044,'5044',' Koyyuru','Y',531),
 (5045,'5045',' Nathavaram','Y',531),
 (5046,'5046',' Golugonda','Y',531),
 (5047,'5047',' Narsipatnam','Y',531),
 (5048,'5048',' Rolugunta','Y',531),
 (5049,'5049',' Ravikamatham','Y',531),
 (5050,'5050',' Madugula','Y',531),
 (5051,'5051',' Cheedikada','Y',531),
 (5052,'5052',' Devarapalle','Y',531),
 (5053,'5053',' K.Kotapadu','Y',531),
 (5054,'5054',' Sabbavaram','Y',531),
 (5055,'5055',' Pendurthi','Y',531),
 (5056,'5056',' Anandapuram','Y',531),
 (5057,'5057',' Padmanabham','Y',531),
 (5058,'5058',' Bheemunipatnam','Y',531),
 (5059,'5059',' Visakhapatnam (Rural)','Y',531),
 (5060,'5060',' Visakhapatnam (Urban)','Y',531),
 (5061,'5061',' Pedagantyada','Y',531),
 (5062,'5062',' Gajuwaka','Y',531),
 (5063,'5063',' Paravada','Y',531),
 (5064,'5064',' Anakapalle','Y',531),
 (5065,'5065',' Chodavaram','Y',531),
 (5066,'5066',' Butchayyapeta','Y',531),
 (5067,'5067',' Kotauratla','Y',531),
 (5068,'5068',' Makavarapalem','Y',531),
 (5069,'5069',' Kasimkota','Y',531),
 (5070,'5070',' Munagapaka','Y',531),
 (5071,'5071',' Atchutapuram','Y',531),
 (5072,'5072',' Yelamanchili','Y',531),
 (5073,'5073',' Nakkapalle','Y',531),
 (5074,'5074',' Payakaraopeta','Y',531),
 (5075,'5075',' S.Rayavaram','Y',531),
 (5076,'5076',' Rambilli','Y',531),
 (5077,'5077',' East Godavari','Y',532),
 (5078,'5078',' Maredumilli','Y',532),
 (5079,'5079',' Devipatnam','Y',532),
 (5080,'5080',' Y. Ramavaram','Y',532),
 (5081,'5081',' Addateegala','Y',532),
 (5082,'5082',' Rajavommangi','Y',532),
 (5083,'5083',' Kotananduru','Y',532),
 (5084,'5084',' Tuni','Y',532),
 (5085,'5085',' Sankhavaram','Y',532),
 (5086,'5086',' Yeleswaram','Y',532),
 (5087,'5087',' Gangavaram','Y',532),
 (5088,'5088',' Rampachodavaram','Y',532),
 (5089,'5089',' Seethanagaram','Y',532),
 (5090,'5090',' Gokavaram','Y',532),
 (5091,'5091',' Jaggampeta','Y',532),
 (5092,'5092',' Kirlampudi','Y',532),
 (5093,'5093',' Prathipadu','Y',532),
 (5094,'5094',' Thondangi','Y',532),
 (5095,'5095',' Gollaprolu','Y',532),
 (5096,'5096',' Peddapuram','Y',532),
 (5097,'5097',' Gandepalle','Y',532),
 (5098,'5098',' Korukonda','Y',532),
 (5099,'5099',' Rajahmundry (U)','Y',532),
 (5100,'5100',' Rajahmundry Rural','Y',532),
 (5101,'5101',' Rajanagaram','Y',532),
 (5102,'5102',' Rangampeta','Y',532),
 (5103,'5103',' Samalkota','Y',532),
 (5104,'5104',' Pithapuram','Y',532),
 (5105,'5105',' Kothapalle','Y',532),
 (5106,'5106',' Kakinada Rural','Y',532),
 (5107,'5107',' Kakinada (U)','Y',532),
 (5108,'5108',' Pedapudi','Y',532),
 (5109,'5109',' Biccavolu','Y',532),
 (5110,'5110',' Anaparthy','Y',532),
 (5111,'5111',' Kadiam','Y',532),
 (5112,'5112',' Atreyapuram','Y',532),
 (5113,'5113',' Mandapeta','Y',532),
 (5114,'5114',' Rayavaram','Y',532),
 (5115,'5115',' Karapa','Y',532),
 (5116,'5116',' Kajuluru','Y',532),
 (5117,'5117',' Ramachandrapuram','Y',532),
 (5118,'5118',' Alamuru','Y',532),
 (5119,'5119',' Ravulapalem','Y',532),
 (5120,'5120',' Kothapeta','Y',532),
 (5121,'5121',' Kapileswarapuram','Y',532),
 (5122,'5122',' Pamarru','Y',532),
 (5123,'5123',' Thallarevu','Y',532),
 (5124,'5124',' I. Polavaram','Y',532),
 (5125,'5125',' Mummidivaram','Y',532),
 (5126,'5126',' Ainavilli','Y',532),
 (5127,'5127',' P.Gannavaram','Y',532),
 (5128,'5128',' Ambajipeta','Y',532),
 (5129,'5129',' Mamidikuduru','Y',532),
 (5130,'5130',' Razole','Y',532),
 (5131,'5131',' Malikipuram','Y',532),
 (5132,'5132',' Sakhinetipalle','Y',532),
 (5133,'5133',' Allavaram','Y',532),
 (5134,'5134',' Amalapuram','Y',532),
 (5135,'5135',' Uppalaguptam','Y',532),
 (5136,'5136',' Katrenikona','Y',532),
 (5137,'5137',' West Godavari','Y',533),
 (5138,'5138',' Chintalapudi','Y',533),
 (5139,'5139',' Lingapalem','Y',533),
 (5140,'5140',' T.Narasapuram','Y',533),
 (5141,'5141',' Jeelugu milli','Y',533),
 (5142,'5142',' Buttayagudem','Y',533),
 (5143,'5143',' Polavaram','Y',533),
 (5144,'5144',' Tallapudi','Y',533),
 (5145,'5145',' Gopalapuram','Y',533),
 (5146,'5146',' Koyyalagudem','Y',533),
 (5147,'5147',' Jangareddigudem','Y',533),
 (5148,'5148',' Kamavarapukota','Y',533),
 (5149,'5149',' Dwarakatirumala','Y',533),
 (5150,'5150',' Nallajerla','Y',533),
 (5151,'5151',' Devarapalle','Y',533),
 (5152,'5152',' Kovvur','Y',533),
 (5153,'5153',' Chagallu','Y',533),
 (5154,'5154',' Nidadavole','Y',533),
 (5155,'5155',' Tadepalligudem','Y',533),
 (5156,'5156',' Unguturu','Y',533),
 (5157,'5157',' Bhimadole','Y',533),
 (5158,'5158',' Pedavegi','Y',533),
 (5159,'5159',' Pedapadu','Y',533),
 (5160,'5160',' Eluru','Y',533),
 (5161,'5161',' Denduluru','Y',533),
 (5162,'5162',' Nidamarru','Y',533),
 (5163,'5163',' Pentapadu','Y',533),
 (5164,'5164',' Undrajavaram','Y',533),
 (5165,'5165',' Peravali','Y',533),
 (5166,'5166',' Tanuku','Y',533),
 (5167,'5167',' Attili','Y',533),
 (5168,'5168',' Ganapavaram','Y',533),
 (5169,'5169',' Akividu','Y',533),
 (5170,'5170',' Undi','Y',533),
 (5171,'5171',' Palacoderu','Y',533),
 (5172,'5172',' Penumantra','Y',533),
 (5173,'5173',' Iragavaram','Y',533),
 (5174,'5174',' Penugonda','Y',533),
 (5175,'5175',' Achanta','Y',533),
 (5176,'5176',' Poduru','Y',533),
 (5177,'5177',' Veeravasaram','Y',533),
 (5178,'5178',' Bhimavaram','Y',533),
 (5179,'5179',' Kalla','Y',533),
 (5180,'5180',' Mogaltur','Y',533),
 (5181,'5181',' Narsapur','Y',533),
 (5182,'5182',' Palacole','Y',533),
 (5183,'5183',' Elamanchili','Y',533),
 (5184,'5184',' Krishna','Y',534),
 (5185,'5185',' Vatsavai','Y',534),
 (5186,'5186',' Jaggayyapeta','Y',534),
 (5187,'5187',' Penuganchiprolu','Y',534),
 (5188,'5188',' Nandigama','Y',534),
 (5189,'5189',' Veerullapadu','Y',534),
 (5190,'5190',' Mylavaram','Y',534),
 (5191,'5191',' Gampalagudem','Y',534),
 (5192,'5192',' Tiruvuru','Y',534),
 (5193,'5193',' A.Konduru','Y',534),
 (5194,'5194',' Reddigudem','Y',534),
 (5195,'5195',' Vissannapeta','Y',534),
 (5196,'5196',' Chatrai','Y',534),
 (5197,'5197',' Musunuru','Y',534),
 (5198,'5198',' Nuzvid','Y',534),
 (5199,'5199',' Bapulapadu','Y',534),
 (5200,'5200',' Agiripalle','Y',534),
 (5201,'5201',' G.Konduru','Y',534),
 (5202,'5202',' Kanchikacherla','Y',534),
 (5203,'5203',' Chandarlapadu','Y',534),
 (5204,'5204',' Ibrahimpatnam','Y',534),
 (5205,'5205',' Vijayawada (Urban)','Y',534),
 (5206,'5206',' Vijayawada (Rural)','Y',534),
 (5207,'5207',' Gannavaram','Y',534),
 (5208,'5208',' Unguturu','Y',534),
 (5209,'5209',' Nandivada','Y',534),
 (5210,'5210',' Mandavalli','Y',534),
 (5211,'5211',' Kaikalur','Y',534),
 (5212,'5212',' Kalidindi','Y',534),
 (5213,'5213',' Kruthivennu','Y',534),
 (5214,'5214',' Bantumilli','Y',534),
 (5215,'5215',' Mudinepalle','Y',534),
 (5216,'5216',' Gudivada','Y',534),
 (5217,'5217',' Pedaparupudi','Y',534),
 (5218,'5218',' Kankipadu','Y',534),
 (5219,'5219',' Penamaluru','Y',534),
 (5220,'5220',' Thotlavalluru','Y',534),
 (5221,'5221',' Pamidimukkala','Y',534),
 (5222,'5222',' Vuyyuru','Y',534),
 (5223,'5223',' Pamarru','Y',534),
 (5224,'5224',' Gudlavalleru','Y',534),
 (5225,'5225',' Pedana','Y',534),
 (5226,'5226',' Guduru','Y',534),
 (5227,'5227',' Movva','Y',534),
 (5228,'5228',' Ghantasala','Y',534),
 (5229,'5229',' Machilipatnam','Y',534),
 (5230,'5230',' Challapalle','Y',534),
 (5231,'5231',' Mopidevi','Y',534),
 (5232,'5232',' Avanigadda','Y',534),
 (5233,'5233',' Nagayalanka','Y',534),
 (5234,'5234',' Koduru','Y',534),
 (5235,'5235',' Guntur','Y',535),
 (5236,'5236',' Macherla','Y',535),
 (5237,'5237',' Veldurthy','Y',535),
 (5238,'5238',' Durgi','Y',535),
 (5239,'5239',' Rentachintala','Y',535),
 (5240,'5240',' Gurazala','Y',535),
 (5241,'5241',' Dachepalle','Y',535),
 (5242,'5242',' Karempudi','Y',535),
 (5243,'5243',' Piduguralla','Y',535),
 (5244,'5244',' Machavaram','Y',535),
 (5245,'5245',' Bellamkonda','Y',535),
 (5246,'5246',' Atchampet','Y',535),
 (5247,'5247',' Krosuru','Y',535),
 (5248,'5248',' Amaravathi','Y',535),
 (5249,'5249',' Thullur','Y',535),
 (5250,'5250',' Tadepalle','Y',535),
 (5251,'5251',' Mangalagiri','Y',535),
 (5252,'5252',' Tadikonda','Y',535),
 (5253,'5253',' Pedakurapadu','Y',535),
 (5254,'5254',' Sattenapalle','Y',535),
 (5255,'5255',' Rajupalem','Y',535),
 (5256,'5256',' Nekarikallu','Y',535),
 (5257,'5257',' Bollapalle','Y',535),
 (5258,'5258',' Vinukonda','Y',535),
 (5259,'5259',' Nuzendla','Y',535),
 (5260,'5260',' Savalyapuram Kanumarlapudi','Y',535),
 (5261,'5261',' Ipur','Y',535),
 (5262,'5262',' Rompicherla','Y',535),
 (5263,'5263',' Narasaraopeta','Y',535),
 (5264,'5264',' Muppalla','Y',535),
 (5265,'5265',' Nadendla','Y',535),
 (5266,'5266',' Chilakaluripet H/o.Purushotha Patnam','Y',535),
 (5267,'5267',' Edlapadu','Y',535),
 (5268,'5268',' Phirangipuram','Y',535),
 (5269,'5269',' Medikonduru','Y',535),
 (5270,'5270',' Guntur','Y',535),
 (5271,'5271',' Pedakakani','Y',535),
 (5272,'5272',' Duggirala','Y',535),
 (5273,'5273',' Kollipara','Y',535),
 (5274,'5274',' Tenali','Y',535),
 (5275,'5275',' Chebrolu','Y',535),
 (5276,'5276',' Vatticherukuru','Y',535),
 (5277,'5277',' Prathipadu','Y',535),
 (5278,'5278',' Pedanandipadu','Y',535),
 (5279,'5279',' Kakumanu','Y',535),
 (5280,'5280',' Ponnur','Y',535),
 (5281,'5281',' Tsundur','Y',535),
 (5282,'5282',' Amruthalur','Y',535),
 (5283,'5283',' Vemuru','Y',535),
 (5284,'5284',' Kollur','Y',535),
 (5285,'5285',' Bhattiprolu','Y',535),
 (5286,'5286',' Cherukupalle H/o Arumbaka','Y',535),
 (5287,'5287',' Pittalavanipalem','Y',535),
 (5288,'5288',' Karlapalem','Y',535),
 (5289,'5289',' Bapatla','Y',535),
 (5290,'5290',' Nizampatnam','Y',535),
 (5291,'5291',' Nagaram','Y',535),
 (5292,'5292',' Repalle','Y',535),
 (5293,'5293',' Prakasam','Y',536),
 (5294,'5294',' Yerragondapalem','Y',536),
 (5295,'5295',' Pullalacheruvu','Y',536),
 (5296,'5296',' Tripuranthakam','Y',536),
 (5297,'5297',' Dornala','Y',536),
 (5298,'5298',' Pedda Raveedu','Y',536),
 (5299,'5299',' Donakonda','Y',536),
 (5300,'5300',' Kurichedu','Y',536);
INSERT INTO `taluka` (`id`,`code`,`name`,`active`,`district_id`) VALUES 
 (5301,'5301',' Santhamaguluru','Y',536),
 (5302,'5302',' Ballikurava','Y',536),
 (5303,'5303',' Martur','Y',536),
 (5304,'5304',' Yeddana pudi','Y',536),
 (5305,'5305',' Parchur','Y',536),
 (5306,'5306',' Karamchedu','Y',536),
 (5307,'5307',' Inkollu','Y',536),
 (5308,'5308',' Janakavarampanguluru','Y',536),
 (5309,'5309',' Addanki','Y',536),
 (5310,'5310',' Mundlamuru','Y',536),
 (5311,'5311',' Darsi','Y',536),
 (5312,'5312',' Markapur','Y',536),
 (5313,'5313',' Ardhaveedu','Y',536),
 (5314,'5314',' Cumbum','Y',536),
 (5315,'5315',' Tarlupadu','Y',536),
 (5316,'5316',' Konakanamitla','Y',536),
 (5317,'5317',' Podili','Y',536),
 (5318,'5318',' Thallur','Y',536),
 (5319,'5319',' Korisapadu','Y',536),
 (5320,'5320',' Chirala','Y',536),
 (5321,'5321',' Vetapalem','Y',536),
 (5322,'5322',' Chinaganjam','Y',536),
 (5323,'5323',' Naguluppala padu','Y',536),
 (5324,'5324',' Maddipadu','Y',536),
 (5325,'5325',' Chimakurthy','Y',536),
 (5326,'5326',' Marripudi','Y',536),
 (5327,'5327',' Hanumanthuni padu','Y',536),
 (5328,'5328',' Bestavaripeta','Y',536),
 (5329,'5329',' Racherla','Y',536),
 (5330,'5330',' Giddaluru','Y',536),
 (5331,'5331',' Komarolu','Y',536),
 (5332,'5332',' Veligandla','Y',536),
 (5333,'5333',' Kanigiri','Y',536),
 (5334,'5334',' Kondapi','Y',536),
 (5335,'5335',' Santhanuthala padu','Y',536),
 (5336,'5336',' Ongole','Y',536),
 (5337,'5337',' Kotha patnam','Y',536),
 (5338,'5338',' Tangutur','Y',536),
 (5339,'5339',' Zarugumilli','Y',536),
 (5340,'5340',' Ponnaluru','Y',536),
 (5341,'5341',' Pedacherlo palle','Y',536),
 (5342,'5342',' Chandra sekhara puram','Y',536),
 (5343,'5343',' Pamur','Y',536),
 (5344,'5344',' Voletivaripalem','Y',536),
 (5345,'5345',' Kandukur','Y',536),
 (5346,'5346',' Singarayakonda','Y',536),
 (5347,'5347',' Lingasamudram','Y',536),
 (5348,'5348',' Gudlur','Y',536),
 (5349,'5349',' Ulavapadu','Y',536),
 (5350,'5350',' Nellore','Y',537),
 (5351,'5351',' Seetharamapuram','Y',537),
 (5352,'5352',' Udayagiri','Y',537),
 (5353,'5353',' Varikuntapadu','Y',537),
 (5354,'5354',' Kondapuram','Y',537),
 (5355,'5355',' Jaladanki','Y',537),
 (5356,'5356',' Kavali','Y',537),
 (5357,'5357',' Bogole','Y',537),
 (5358,'5358',' Kaligiri','Y',537),
 (5359,'5359',' Vinjamur','Y',537),
 (5360,'5360',' Duttalur','Y',537),
 (5361,'5361',' Marripadu','Y',537),
 (5362,'5362',' Atmakur','Y',537),
 (5363,'5363',' Anumasamudrampeta','Y',537),
 (5364,'5364',' Dagadarthi','Y',537),
 (5365,'5365',' Allur','Y',537),
 (5366,'5366',' Vidavalur','Y',537),
 (5367,'5367',' Kodavalur','Y',537),
 (5368,'5368',' Buchireddipalem ','Y',537),
 (5369,'5369',' Sangam','Y',537),
 (5370,'5370',' Chejerla','Y',537),
 (5371,'5371',' Ananthasagaram','Y',537),
 (5372,'5372',' Kaluvoya','Y',537),
 (5373,'5373',' Rapur','Y',537),
 (5374,'5374',' Podalakur','Y',537),
 (5375,'5375',' Nellore','Y',537),
 (5376,'5376',' Kovur','Y',537),
 (5377,'5377',' Indukurpet','Y',537),
 (5378,'5378',' Thotapalligudur','Y',537),
 (5379,'5379',' Muthukur','Y',537),
 (5380,'5380',' Venkatachalam','Y',537),
 (5381,'5381',' Manubolu','Y',537),
 (5382,'5382',' Gudur','Y',537),
 (5383,'5383',' Sydapuram','Y',537),
 (5384,'5384',' Dakkili','Y',537),
 (5385,'5385',' Venkatagiri','Y',537),
 (5386,'5386',' Balayapalle','Y',537),
 (5387,'5387',' Ozili','Y',537),
 (5388,'5388',' Chillakur','Y',537),
 (5389,'5389',' Kota','Y',537),
 (5390,'5390',' Vakadu','Y',537),
 (5391,'5391',' Chittamur','Y',537),
 (5392,'5392',' Naidupet','Y',537),
 (5393,'5393',' Pellakur','Y',537),
 (5394,'5394',' Doravarisatram','Y',537),
 (5395,'5395',' Sullurpeta','Y',537),
 (5396,'5396',' Tada','Y',537),
 (5397,'5397',' Cuddapah','Y',538),
 (5398,'5398',' Kondapuram','Y',538),
 (5399,'5399',' Mylavaram','Y',538),
 (5400,'5400',' Peddamudium','Y',538),
 (5401,'5401',' Rajupalem','Y',538),
 (5402,'5402',' Duvvur','Y',538),
 (5403,'5403',' S.Mydukur','Y',538),
 (5404,'5404',' Brahmamgarimattam','Y',538),
 (5405,'5405',' Sri Avadhuth kasinayana','Y',538),
 (5406,'5406',' Kalasapadu','Y',538),
 (5407,'5407',' Porumamilla','Y',538),
 (5408,'5408',' B.Kodur','Y',538),
 (5409,'5409',' Badvel','Y',538),
 (5410,'5410',' Gopavaram','Y',538),
 (5411,'5411',' Khajipet','Y',538),
 (5412,'5412',' Chapadu','Y',538),
 (5413,'5413',' Proddatur','Y',538),
 (5414,'5414',' Jammalamadugu','Y',538),
 (5415,'5415',' Muddanur','Y',538),
 (5416,'5416',' Simhadripuram','Y',538),
 (5417,'5417',' Lingala','Y',538),
 (5418,'5418',' Pulivendla','Y',538),
 (5419,'5419',' Vemula','Y',538),
 (5420,'5420',' Thondur','Y',538),
 (5421,'5421',' Veerapunayunipalle','Y',538),
 (5422,'5422',' Yerraguntla','Y',538),
 (5423,'5423',' Kamalapuram','Y',538),
 (5424,'5424',' Vallur','Y',538),
 (5425,'5425',' Chennur','Y',538),
 (5426,'5426',' Atlur','Y',538),
 (5427,'5427',' Vontimitta','Y',538),
 (5428,'5428',' Sidhout','Y',538),
 (5429,'5429',' Cuddapah','Y',538),
 (5430,'5430',' Chinthakommadinne','Y',538),
 (5431,'5431',' Pendlimarry','Y',538),
 (5432,'5432',' Vempalle','Y',538),
 (5433,'5433',' Chakarayapet','Y',538),
 (5434,'5434',' Galiveedu','Y',538),
 (5435,'5435',' Chinnamudiam','Y',538),
 (5436,'5436',' Sambepalle','Y',538),
 (5437,'5437',' T.Sundupalle','Y',538),
 (5438,'5438',' Rayachoti','Y',538),
 (5439,'5439',' Lakkireddipalle','Y',538),
 (5440,'5440',' Ramapuram','Y',538),
 (5441,'5441',' Veeraballe','Y',538),
 (5442,'5442',' Nandalur','Y',538),
 (5443,'5443',' Penagalur','Y',538),
 (5444,'5444',' Chitvel','Y',538),
 (5445,'5445',' Rajampet','Y',538),
 (5446,'5446',' Pullampet','Y',538),
 (5447,'5447',' Obulavaripalle','Y',538),
 (5448,'5448',' Rly.kodur','Y',538),
 (5449,'5449',' Kurnool','Y',539),
 (5450,'5450',' Mantralayam','Y',539),
 (5451,'5451',' Kosigi','Y',539),
 (5452,'5452',' Kowthalam','Y',539),
 (5453,'5453',' Pedda kadabur','Y',539),
 (5454,'5454',' Yemmiganur','Y',539),
 (5455,'5455',' Nandavaram','Y',539),
 (5456,'5456',' C.Belagal','Y',539),
 (5457,'5457',' Gudur','Y',539),
 (5458,'5458',' Kallur','Y',539),
 (5459,'5459',' Kurnool','Y',539),
 (5460,'5460',' Nandikotkur','Y',539),
 (5461,'5461',' Pagidyala','Y',539),
 (5462,'5462',' Jupadu bungalow','Y',539),
 (5463,'5463',' Kothapalle','Y',539),
 (5464,'5464',' Srisailam','Y',539),
 (5465,'5465',' Atmakur','Y',539),
 (5466,'5466',' Pamulapadu','Y',539),
 (5467,'5467',' Midthur','Y',539),
 (5468,'5468',' Orvakal','Y',539),
 (5469,'5469',' Kodumur','Y',539),
 (5470,'5470',' Gonegandla','Y',539),
 (5471,'5471',' Adoni','Y',539),
 (5472,'5472',' Holagunda','Y',539),
 (5473,'5473',' Halaharvi','Y',539),
 (5474,'5474',' Alur','Y',539),
 (5475,'5475',' Aspari','Y',539),
 (5476,'5476',' Devanakonda','Y',539),
 (5477,'5477',' Krishnagiri','Y',539),
 (5478,'5478',' Veldurthi','Y',539),
 (5479,'5479',' Bethamcherla','Y',539),
 (5480,'5480',' Panyam','Y',539),
 (5481,'5481',' Gadivemula','Y',539),
 (5482,'5482',' Velgode','Y',539),
 (5483,'5483',' Bandi Atmakur','Y',539),
 (5484,'5484',' Nandyal','Y',539),
 (5485,'5485',' Mahanandi','Y',539),
 (5486,'5486',' Sirvel','Y',539),
 (5487,'5487',' Gospadu','Y',539),
 (5488,'5488',' Banaganapalle','Y',539),
 (5489,'5489',' Dhone','Y',539),
 (5490,'5490',' Pathikonda','Y',539),
 (5491,'5491',' Chippagiri','Y',539),
 (5492,'5492',' Maddikera (East)','Y',539),
 (5493,'5493',' Tuggali','Y',539),
 (5494,'5494',' Peapally','Y',539),
 (5495,'5495',' Owk','Y',539),
 (5496,'5496',' Koilkuntla','Y',539),
 (5497,'5497',' Rudravaram','Y',539),
 (5498,'5498',' Allagadda','Y',539),
 (5499,'5499',' Dornipadu','Y',539),
 (5500,'5500',' Sanjamala','Y',539),
 (5501,'5501',' Kolimigundla','Y',539),
 (5502,'5502',' Uyyalawada','Y',539),
 (5503,'5503',' Chagalamarri','Y',539),
 (5504,'5504',' Anantapur','Y',540),
 (5505,'5505',' D.Hirehal','Y',540),
 (5506,'5506',' Rayadurg','Y',540),
 (5507,'5507',' Kanekal','Y',540),
 (5508,'5508',' Bommanahal','Y',540),
 (5509,'5509',' Vidapanakal','Y',540),
 (5510,'5510',' Guntakal','Y',540),
 (5511,'5511',' Gooty','Y',540),
 (5512,'5512',' Peddavadugur','Y',540),
 (5513,'5513',' Yadiki','Y',540),
 (5514,'5514',' Tadpatri','Y',540),
 (5515,'5515',' Peddapappur','Y',540),
 (5516,'5516',' Pamidi','Y',540),
 (5517,'5517',' Vijrakarur','Y',540),
 (5518,'5518',' Uravakonda','Y',540),
 (5519,'5519',' Beluguppa','Y',540),
 (5520,'5520',' Gummagatta','Y',540),
 (5521,'5521',' Brahmasamudram','Y',540),
 (5522,'5522',' Kalyandurg','Y',540),
 (5523,'5523',' Atmakur','Y',540),
 (5524,'5524',' Kudair','Y',540),
 (5525,'5525',' Garladinne','Y',540),
 (5526,'5526',' Singanamala','Y',540),
 (5527,'5527',' Putlur','Y',540),
 (5528,'5528',' Yellanur','Y',540),
 (5529,'5529',' Narpala','Y',540),
 (5530,'5530',' Bukkarayasamudram','Y',540),
 (5531,'5531',' Anantapur','Y',540),
 (5532,'5532',' Raptadu','Y',540),
 (5533,'5533',' Settur','Y',540),
 (5534,'5534',' Kundurpi','Y',540),
 (5535,'5535',' Kambadur','Y',540),
 (5536,'5536',' Kanaganapalle','Y',540),
 (5537,'5537',' Dharmavaram','Y',540),
 (5538,'5538',' Bathalapalle','Y',540),
 (5539,'5539',' Tadimarri','Y',540),
 (5540,'5540',' Mudigubba','Y',540),
 (5541,'5541',' Talupula','Y',540),
 (5542,'5542',' Nambulapulakunta','Y',540),
 (5543,'5543',' Gandlapenta','Y',540),
 (5544,'5544',' Kadiri','Y',540),
 (5545,'5545',' Nallamada','Y',540),
 (5546,'5546',' Bukkapatnam','Y',540),
 (5547,'5547',' Kothacheruvu','Y',540),
 (5548,'5548',' Chennekothapalle','Y',540),
 (5549,'5549',' Ramagiri','Y',540),
 (5550,'5550',' Roddam','Y',540),
 (5551,'5551',' Madakasira','Y',540),
 (5552,'5552',' Amarapuram','Y',540),
 (5553,'5553',' Gudibanda','Y',540),
 (5554,'5554',' Rolla','Y',540),
 (5555,'5555',' Agali','Y',540),
 (5556,'5556',' Parigi','Y',540),
 (5557,'5557',' Penukonda','Y',540),
 (5558,'5558',' Puttaparthi','Y',540),
 (5559,'5559',' Obuladevarecheruvu','Y',540),
 (5560,'5560',' Nallacheruvu','Y',540),
 (5561,'5561',' Tanakallu','Y',540),
 (5562,'5562',' Amadagur','Y',540),
 (5563,'5563',' Gorantla','Y',540),
 (5564,'5564',' Somandepalle','Y',540),
 (5565,'5565',' Hindupur','Y',540),
 (5566,'5566',' Lepakshi','Y',540),
 (5567,'5567',' Chilamathur','Y',540),
 (5568,'5568',' Chittoor','Y',541),
 (5569,'5569',' Mulakalacheruvu','Y',541),
 (5570,'5570',' Thamballapalle','Y',541),
 (5571,'5571',' Peddamandyam','Y',541),
 (5572,'5572',' Gurramkonda','Y',541),
 (5573,'5573',' Kalakada','Y',541),
 (5574,'5574',' Kambhamvaripalle','Y',541),
 (5575,'5575',' Rompicherla','Y',541),
 (5576,'5576',' Yerravaripalem','Y',541),
 (5577,'5577',' Tirupathi (Rural)','Y',541),
 (5578,'5578',' Renigunta','Y',541),
 (5579,'5579',' Yerpedu','Y',541),
 (5580,'5580',' Srikalahasti','Y',541),
 (5581,'5581',' Thottambedu','Y',541),
 (5582,'5582',' Buchinaidu Khandriga','Y',541),
 (5583,'5583',' Varadaiahpalem','Y',541),
 (5584,'5584',' K.V.B.Puram','Y',541),
 (5585,'5585',' Tirupati (Urban)','Y',541),
 (5586,'5586',' Chandragiri','Y',541),
 (5587,'5587',' Chinnagottigallu','Y',541),
 (5588,'5588',' Piler','Y',541),
 (5589,'5589',' Kalikiri','Y',541),
 (5590,'5590',' Vayalpad','Y',541),
 (5591,'5591',' Kurabalakota','Y',541),
 (5592,'5592',' Peddathippa samudram','Y',541),
 (5593,'5593',' B.Kothakota','Y',541),
 (5594,'5594',' Madanapalle','Y',541),
 (5595,'5595',' Nimmanapalle','Y',541),
 (5596,'5596',' Sodum','Y',541),
 (5597,'5597',' Pulicherla','Y',541),
 (5598,'5598',' Pakala','Y',541),
 (5599,'5599',' Vedurukuppam','Y',541),
 (5600,'5600',' Ramachandra puram','Y',541),
 (5601,'5601',' Vadamalapeta','Y',541),
 (5602,'5602',' Narayanavanam','Y',541),
 (5603,'5603',' Pitchatur','Y',541),
 (5604,'5604',' Satyavedu','Y',541),
 (5605,'5605',' Nagalapuram','Y',541),
 (5606,'5606',' Nindra','Y',541),
 (5607,'5607',' Vijayapuram','Y',541),
 (5608,'5608',' Nagari','Y',541),
 (5609,'5609',' Puttur','Y',541),
 (5610,'5610',' Karvetinagar','Y',541),
 (5611,'5611',' Penumur','Y',541),
 (5612,'5612',' Puthalapattu','Y',541),
 (5613,'5613',' Irala','Y',541),
 (5614,'5614',' Somala','Y',541),
 (5615,'5615',' Chowdepalle','Y',541),
 (5616,'5616',' Ramasamudram','Y',541),
 (5617,'5617',' Punganur','Y',541),
 (5618,'5618',' Peddapanjani','Y',541),
 (5619,'5619',' Gangavaram','Y',541),
 (5620,'5620',' Thavanampalle','Y',541),
 (5621,'5621',' Srirangarajapuram','Y',541),
 (5622,'5622',' Gangadhara Nellore','Y',541),
 (5623,'5623',' Chittoor','Y',541),
 (5624,'5624',' Palamaner','Y',541),
 (5625,'5625',' Baireddipalle','Y',541),
 (5626,'5626',' Venkatagirikota','Y',541),
 (5627,'5627',' Santhipuram ','Y',541),
 (5628,'5628',' Gudupalle','Y',541),
 (5629,'5629',' Kuppam','Y',541),
 (5630,'5630',' Ramakuppam','Y',541),
 (5631,'5631',' Bangarupalyam','Y',541),
 (5632,'5632',' Yadamari','Y',541),
 (5633,'5633',' Gudipala','Y',541),
 (5634,'5634',' Palasamudram','Y',541),
 (5635,'5635',' KARNATAKA','Y',542),
 (5636,'5636',' Belgaum','Y',543),
 (5637,'5637',' Chikodi','Y',543),
 (5638,'5638',' Athni','Y',543),
 (5639,'5639',' Raybag','Y',543),
 (5640,'5640',' Gokak','Y',543),
 (5641,'5641',' Hukeri','Y',543),
 (5642,'5642',' Belgaum','Y',543),
 (5643,'5643',' Khanapur','Y',543),
 (5644,'5644',' Sampgaon','Y',543),
 (5645,'5645',' Parasgad','Y',543),
 (5646,'5646',' Ramdurg','Y',543),
 (5647,'5647',' Bagalkot *','Y',544),
 (5648,'5648',' Jamkhandi','Y',544),
 (5649,'5649',' Bilgi','Y',544),
 (5650,'5650',' Mudhol','Y',544),
 (5651,'5651',' Badami','Y',544),
 (5652,'5652',' Bagalkot','Y',544),
 (5653,'5653',' Hungund','Y',544),
 (5654,'5654',' Bijapur','Y',545),
 (5655,'5655',' Bijapur','Y',545),
 (5656,'5656',' Indi','Y',545),
 (5657,'5657',' Sindgi','Y',545),
 (5658,'5658',' Basavana Bagevadi','Y',545),
 (5659,'5659',' Muddebihal','Y',545),
 (5660,'5660',' Gulbarga','Y',546),
 (5661,'5661',' Aland','Y',546),
 (5662,'5662',' Afzalpur','Y',546),
 (5663,'5663',' Gulbarga','Y',546),
 (5664,'5664',' Chincholi','Y',546),
 (5665,'5665',' Sedam','Y',546),
 (5666,'5666',' Chitapur','Y',546),
 (5667,'5667',' Jevargi','Y',546),
 (5668,'5668',' Shahpur','Y',546),
 (5669,'5669',' Shorapur','Y',546),
 (5670,'5670',' Yadgir','Y',546),
 (5671,'5671',' Bidar','Y',547),
 (5672,'5672',' Basavakalyan','Y',547),
 (5673,'5673',' Aurad','Y',547),
 (5674,'5674',' Bhalki','Y',547),
 (5675,'5675',' Bidar','Y',547),
 (5676,'5676',' Homnabad','Y',547),
 (5677,'5677',' Raichur','Y',548),
 (5678,'5678',' Lingsugur','Y',548),
 (5679,'5679',' Devadurga','Y',548),
 (5680,'5680',' Raichur','Y',548),
 (5681,'5681',' Manvi','Y',548),
 (5682,'5682',' Sindhnur','Y',548),
 (5683,'5683',' Koppal *','Y',549),
 (5684,'5684',' Yelbarga','Y',549),
 (5685,'5685',' Kushtagi','Y',549),
 (5686,'5686',' Gangawati','Y',549),
 (5687,'5687',' Koppal','Y',549),
 (5688,'5688',' Gadag *','Y',550),
 (5689,'5689',' Nargund','Y',550),
 (5690,'5690',' Ron','Y',550),
 (5691,'5691',' Gadag','Y',550),
 (5692,'5692',' Shirhatti','Y',550),
 (5693,'5693',' Mundargi','Y',550),
 (5694,'5694',' Dharwad','Y',551),
 (5695,'5695',' Dharwad','Y',551),
 (5696,'5696',' Navalgund','Y',551),
 (5697,'5697',' Hubli','Y',551),
 (5698,'5698',' Kalghatgi','Y',551),
 (5699,'5699',' Kundgol','Y',551),
 (5700,'5700',' Uttara Kannada','Y',552),
 (5701,'5701',' Karwar','Y',552),
 (5702,'5702',' Supa','Y',552),
 (5703,'5703',' Haliyal','Y',552),
 (5704,'5704',' Yellapur','Y',552),
 (5705,'5705',' Mundgod','Y',552),
 (5706,'5706',' Sirsi','Y',552),
 (5707,'5707',' Ankola','Y',552),
 (5708,'5708',' Kumta','Y',552),
 (5709,'5709',' Siddapur','Y',552),
 (5710,'5710',' Honavar','Y',552),
 (5711,'5711',' Bhatkal','Y',552),
 (5712,'5712',' Haveri *','Y',553),
 (5713,'5713',' Shiggaon','Y',553),
 (5714,'5714',' Savanur','Y',553),
 (5715,'5715',' Hangal','Y',553),
 (5716,'5716',' Haveri','Y',553),
 (5717,'5717',' Byadgi','Y',553),
 (5718,'5718',' Hirekerur','Y',553),
 (5719,'5719',' Ranibennur','Y',553),
 (5720,'5720',' Bellary','Y',554),
 (5721,'5721',' Hadagalli','Y',554),
 (5722,'5722',' Hagaribommanahalli','Y',554),
 (5723,'5723',' Hospet','Y',554),
 (5724,'5724',' Siruguppa','Y',554),
 (5725,'5725',' Bellary','Y',554),
 (5726,'5726',' Sandur','Y',554),
 (5727,'5727',' Kudligi','Y',554),
 (5728,'5728',' Chitradurga','Y',555),
 (5729,'5729',' Molakalmuru','Y',555),
 (5730,'5730',' Challakere','Y',555),
 (5731,'5731',' Chitradurga','Y',555),
 (5732,'5732',' Holalkere','Y',555),
 (5733,'5733',' Hosdurga','Y',555),
 (5734,'5734',' Hiriyur','Y',555),
 (5735,'5735',' Davanagere*','Y',556),
 (5736,'5736',' Harihar','Y',556),
 (5737,'5737',' Harapanahalli','Y',556),
 (5738,'5738',' Jagalur','Y',556),
 (5739,'5739',' Davanagere','Y',556),
 (5740,'5740',' Honnali','Y',556),
 (5741,'5741',' Channagiri','Y',556),
 (5742,'5742',' Shimoga','Y',557),
 (5743,'5743',' Sagar','Y',557),
 (5744,'5744',' Sorab','Y',557),
 (5745,'5745',' Shikarpur','Y',557),
 (5746,'5746',' Hosanagara','Y',557),
 (5747,'5747',' Tirthahalli','Y',557),
 (5748,'5748',' Shimoga','Y',557),
 (5749,'5749',' Bhadravati','Y',557),
 (5750,'5750',' Udupi *','Y',558),
 (5751,'5751',' Kundapura','Y',558),
 (5752,'5752',' Udupi','Y',558),
 (5753,'5753',' Karkal','Y',558),
 (5754,'5754',' Chikmagalur','Y',559),
 (5755,'5755',' Sringeri','Y',559),
 (5756,'5756',' Koppa','Y',559),
 (5757,'5757',' Narasimharajapura','Y',559),
 (5758,'5758',' Tarikere','Y',559),
 (5759,'5759',' Kadur','Y',559),
 (5760,'5760',' Chikmagalur','Y',559),
 (5761,'5761',' Mudigere','Y',559),
 (5762,'5762',' Tumkur','Y',560),
 (5763,'5763',' Chiknayakanhalli','Y',560),
 (5764,'5764',' Sira','Y',560),
 (5765,'5765',' Pavagada','Y',560),
 (5766,'5766',' Madhugiri','Y',560),
 (5767,'5767',' Koratagere','Y',560),
 (5768,'5768',' Tumkur','Y',560),
 (5769,'5769',' Gubbi','Y',560),
 (5770,'5770',' Tiptur','Y',560),
 (5771,'5771',' Turuvekere','Y',560),
 (5772,'5772',' Kunigal','Y',560),
 (5773,'5773',' Kolar','Y',561),
 (5774,'5774',' Gauribidanur','Y',561),
 (5775,'5775',' Chik Ballapur','Y',561),
 (5776,'5776',' Gudibanda','Y',561),
 (5777,'5777',' Bagepalli','Y',561),
 (5778,'5778',' Sidlaghatta','Y',561),
 (5779,'5779',' Chintamani','Y',561),
 (5780,'5780',' Srinivaspur','Y',561),
 (5781,'5781',' Kolar','Y',561),
 (5782,'5782',' Malur','Y',561),
 (5783,'5783',' Bangarapet','Y',561),
 (5784,'5784',' Mulbagal','Y',561),
 (5785,'5785',' Bangalore','Y',562),
 (5786,'5786',' Bangalore North','Y',562),
 (5787,'5787',' Bangalore South','Y',562),
 (5788,'5788',' Anekal','Y',562),
 (5789,'5789',' Bangalore Rural','Y',563),
 (5790,'5790',' Nelamangala','Y',563),
 (5791,'5791',' Dod Ballapur','Y',563),
 (5792,'5792',' Devanhalli','Y',563),
 (5793,'5793',' Hoskote','Y',563),
 (5794,'5794',' Magadi','Y',563),
 (5795,'5795',' Ramanagaram','Y',563),
 (5796,'5796',' Channapatna','Y',563),
 (5797,'5797',' Kanakapura','Y',563),
 (5798,'5798',' Mandya','Y',564),
 (5799,'5799',' Krishnarajpet','Y',564),
 (5800,'5800',' Nagamangala','Y',564),
 (5801,'5801',' Pandavapura','Y',564),
 (5802,'5802',' Shrirangapattana','Y',564),
 (5803,'5803',' Mandya','Y',564),
 (5804,'5804',' Maddur','Y',564),
 (5805,'5805',' Malavalli','Y',564),
 (5806,'5806',' Hassan','Y',565),
 (5807,'5807',' Sakleshpur','Y',565),
 (5808,'5808',' Belur','Y',565),
 (5809,'5809',' Arsikere','Y',565),
 (5810,'5810',' Hassan','Y',565),
 (5811,'5811',' Alur','Y',565),
 (5812,'5812',' Arkalgud','Y',565),
 (5813,'5813',' Hole Narsipur','Y',565),
 (5814,'5814',' Channarayapatna','Y',565),
 (5815,'5815',' Dakshina Kannada','Y',566),
 (5816,'5816',' Mangalore','Y',566),
 (5817,'5817',' Bantval','Y',566),
 (5818,'5818',' Beltangadi','Y',566),
 (5819,'5819',' Puttur','Y',566),
 (5820,'5820',' Sulya','Y',566),
 (5821,'5821',' Kodagu','Y',567),
 (5822,'5822',' Madikeri','Y',567),
 (5823,'5823',' Somvarpet','Y',567),
 (5824,'5824',' Virajpet','Y',567),
 (5825,'5825',' Mysore','Y',568),
 (5826,'5826',' Piriyapatna','Y',568),
 (5827,'5827',' Hunsur','Y',568),
 (5828,'5828',' Krishnarajanagara','Y',568),
 (5829,'5829',' Mysore','Y',568),
 (5830,'5830',' Heggadadevankote','Y',568),
 (5831,'5831',' Nanjangud','Y',568),
 (5832,'5832',' Tirumakudal Narsipur','Y',568),
 (5833,'5833',' Chamarajanagar*','Y',569),
 (5834,'5834',' Gundlupet','Y',569),
 (5835,'5835',' Chamarajanagar','Y',569),
 (5836,'5836',' Yelandur','Y',569),
 (5837,'5837',' Kollegal','Y',569),
 (5838,'5838',' GOA','Y',570),
 (5839,'5839',' North Goa ','Y',571),
 (5840,'5840',' Pernem','Y',571),
 (5841,'5841',' Bardez','Y',571),
 (5842,'5842',' Tiswadi','Y',571),
 (5843,'5843',' Bicholim ','Y',571),
 (5844,'5844',' Satari','Y',571),
 (5845,'5845',' Ponda','Y',571),
 (5846,'5846',' South Goa','Y',572),
 (5847,'5847',' Mormugao ','Y',572),
 (5848,'5848',' Salcete','Y',572),
 (5849,'5849',' Quepem ','Y',572),
 (5850,'5850',' Sanguem ','Y',572),
 (5851,'5851',' Canacona','Y',572),
 (5852,'5852',' LAKSHADWEEP','Y',573),
 (5853,'5853',' Lakshadweep','Y',574),
 (5854,'5854',' Amini','Y',574),
 (5855,'5855',' Kavaratti','Y',574),
 (5856,'5856',' Andrott','Y',574),
 (5857,'5857',' Minicoy','Y',574),
 (5858,'5858',' KERALA','Y',575),
 (5859,'5859',' Kasaragod','Y',576),
 (5860,'5860',' Kasaragod','Y',576),
 (5861,'5861',' Hosdurg','Y',576),
 (5862,'5862',' Kannur','Y',577),
 (5863,'5863',' Taliparamba','Y',577),
 (5864,'5864',' Kannur','Y',577),
 (5865,'5865',' Thalassery','Y',577),
 (5866,'5866',' Wayanad','Y',578),
 (5867,'5867',' Mananthavady','Y',578),
 (5868,'5868',' Sulthanbathery','Y',578),
 (5869,'5869',' Vythiri','Y',578),
 (5870,'5870',' Kozhikode','Y',579),
 (5871,'5871',' Vadakara','Y',579),
 (5872,'5872',' Quilandy','Y',579),
 (5873,'5873',' Kozhikode','Y',579),
 (5874,'5874',' Malappuram','Y',580),
 (5875,'5875',' Ernad','Y',580),
 (5876,'5876',' Nilambur','Y',580),
 (5877,'5877',' Perinthalmanna','Y',580),
 (5878,'5878',' Tirur','Y',580),
 (5879,'5879',' Tirurangadi','Y',580),
 (5880,'5880',' Ponnani','Y',580),
 (5881,'5881',' Palakkad','Y',581),
 (5882,'5882',' Ottappalam','Y',581),
 (5883,'5883',' Mannarkad','Y',581),
 (5884,'5884',' Palakkad','Y',581),
 (5885,'5885',' Chittur','Y',581),
 (5886,'5886',' Alathur','Y',581),
 (5887,'5887',' Thrissur','Y',582),
 (5888,'5888',' Talappilly','Y',582),
 (5889,'5889',' Chavakkad','Y',582),
 (5890,'5890',' Thrissur','Y',582),
 (5891,'5891',' Kodungallur','Y',582),
 (5892,'5892',' Mukundapuram','Y',582),
 (5893,'5893',' Ernakulam','Y',583),
 (5894,'5894',' Kunnathunad','Y',583),
 (5895,'5895',' Aluva','Y',583),
 (5896,'5896',' Paravur','Y',583),
 (5897,'5897',' Kochi','Y',583),
 (5898,'5898',' Kanayannur','Y',583),
 (5899,'5899',' Muvattupuzha','Y',583),
 (5900,'5900',' Kothamangalam','Y',583),
 (5901,'5901',' Idukki','Y',584),
 (5902,'5902',' Devikulam','Y',584),
 (5903,'5903',' Udumbanchola','Y',584),
 (5904,'5904',' Thodupuzha','Y',584),
 (5905,'5905',' Peerumade','Y',584),
 (5906,'5906',' Kottayam','Y',585),
 (5907,'5907',' Meenachil','Y',585),
 (5908,'5908',' Vaikom','Y',585),
 (5909,'5909',' Kottayam','Y',585),
 (5910,'5910',' Changanassery','Y',585),
 (5911,'5911',' Kanjirappally','Y',585),
 (5912,'5912',' Alappuzha','Y',586),
 (5913,'5913',' Cherthala','Y',586),
 (5914,'5914',' Ambalappuzha','Y',586),
 (5915,'5915',' Kuttanad','Y',586),
 (5916,'5916',' Karthikappally','Y',586),
 (5917,'5917',' Chengannur','Y',586),
 (5918,'5918',' Mavelikkara','Y',586),
 (5919,'5919',' Pathanamthitta','Y',587),
 (5920,'5920',' Thiruvalla','Y',587),
 (5921,'5921',' Mallappally','Y',587),
 (5922,'5922',' Ranni','Y',587),
 (5923,'5923',' Kozhenchery','Y',587),
 (5924,'5924',' Adoor','Y',587),
 (5925,'5925',' Kollam','Y',588),
 (5926,'5926',' Karunagappally','Y',588),
 (5927,'5927',' Kunnathur','Y',588),
 (5928,'5928',' Pathanapuram','Y',588),
 (5929,'5929',' Kottarakkara','Y',588),
 (5930,'5930',' Kollam','Y',588),
 (5931,'5931',' Thiruvananthapuram','Y',589),
 (5932,'5932',' Chirayinkeezhu','Y',589),
 (5933,'5933',' Nedumangad','Y',589),
 (5934,'5934',' Thiruvananthapuram','Y',589),
 (5935,'5935',' Neyyattinkara','Y',589),
 (5936,'5936',' TAMIL NADU','Y',590),
 (5937,'5937',' Thiruvallur','Y',591),
 (5938,'5938',' Gummidipoondi','Y',591),
 (5939,'5939',' Ponneri','Y',591),
 (5940,'5940',' Uthukkottai','Y',591),
 (5941,'5941',' Tiruttani','Y',591),
 (5942,'5942',' Pallipattu','Y',591),
 (5943,'5943',' Thiruvallur','Y',591),
 (5944,'5944',' Poonamallee','Y',591),
 (5945,'5945',' Ambattur','Y',591),
 (5946,'5946',' Chennai','Y',592),
 (5947,'5947',' Kancheepuram','Y',593),
 (5948,'5948',' Sriperumbudur','Y',593),
 (5949,'5949',' Tambaram','Y',593),
 (5950,'5950',' Chengalpattu','Y',593),
 (5951,'5951',' Kancheepuram','Y',593),
 (5952,'5952',' Uthiramerur','Y',593),
 (5953,'5953',' Tirukalukundram','Y',593),
 (5954,'5954',' Maduranthakam','Y',593),
 (5955,'5955',' Cheyyur','Y',593),
 (5956,'5956',' Vellore','Y',594),
 (5957,'5957',' Gudiyatham','Y',594),
 (5958,'5958',' Katpadi','Y',594),
 (5959,'5959',' Wallajah','Y',594),
 (5960,'5960',' Arakonam','Y',594),
 (5961,'5961',' Arcot','Y',594),
 (5962,'5962',' Vellore','Y',594),
 (5963,'5963',' Vaniyambadi','Y',594),
 (5964,'5964',' Tirupathur','Y',594),
 (5965,'5965',' Dharmapuri','Y',595),
 (5966,'5966',' Hosur','Y',595),
 (5967,'5967',' Krishnagiri','Y',595),
 (5968,'5968',' Denkanikottai','Y',595),
 (5969,'5969',' Palakkodu','Y',595),
 (5970,'5970',' Pochampalli','Y',595),
 (5971,'5971',' Uthangarai','Y',595),
 (5972,'5972',' Harur','Y',595),
 (5973,'5973',' Pappireddipatti','Y',595),
 (5974,'5974',' Dharmapuri','Y',595),
 (5975,'5975',' Pennagaram','Y',595),
 (5976,'5976',' Tiruvannamalai','Y',596),
 (5977,'5977',' Arani','Y',596),
 (5978,'5978',' Cheyyar','Y',596),
 (5979,'5979',' Vandavasi','Y',596),
 (5980,'5980',' Polur','Y',596),
 (5981,'5981',' Chengam','Y',596),
 (5982,'5982',' Tiruvannamalai','Y',596),
 (5983,'5983',' Viluppuram','Y',597),
 (5984,'5984',' Gingee','Y',597),
 (5985,'5985',' Tindivanam','Y',597),
 (5986,'5986',' Vanur','Y',597),
 (5987,'5987',' Viluppuram','Y',597),
 (5988,'5988',' Tirukkoyilur','Y',597),
 (5989,'5989',' Sankarapuram','Y',597),
 (5990,'5990',' Kallakkurichi','Y',597),
 (5991,'5991',' Ulundurpettai','Y',597),
 (5992,'5992',' Salem','Y',598),
 (5993,'5993',' Mettur','Y',598),
 (5994,'5994',' Omalur','Y',598),
 (5995,'5995',' Edappadi','Y',598),
 (5996,'5996',' Sankari','Y',598),
 (5997,'5997',' Salem','Y',598),
 (5998,'5998',' Yercaud','Y',598),
 (5999,'5999',' Vazhapadi','Y',598),
 (6000,'6000',' Attur','Y',598),
 (6001,'6001',' Gangavalli','Y',598),
 (6002,'6002',' Namakkal   *','Y',599),
 (6003,'6003',' Tiruchengode','Y',599),
 (6004,'6004',' Rasipuram','Y',599),
 (6005,'6005',' Namakkal','Y',599),
 (6006,'6006',' Paramathi-Velur','Y',599),
 (6007,'6007',' Erode','Y',600),
 (6008,'6008',' Sathyamangalam','Y',600),
 (6009,'6009',' Bhavani','Y',600),
 (6010,'6010',' Gobichetti - Palayam','Y',600),
 (6011,'6011',' Perundurai','Y',600),
 (6012,'6012',' Erode','Y',600),
 (6013,'6013',' Kangeyam','Y',600),
 (6014,'6014',' Dharapuram','Y',600),
 (6015,'6015',' The Nilgiris','Y',601),
 (6016,'6016',' Panthalur','Y',601),
 (6017,'6017',' Gudalur','Y',601),
 (6018,'6018',' Udhagamandalam','Y',601),
 (6019,'6019',' Kotagiri','Y',601),
 (6020,'6020',' Coonoor','Y',601),
 (6021,'6021',' Kundah','Y',601),
 (6022,'6022',' Coimbatore','Y',602),
 (6023,'6023',' Mettupalayam','Y',602),
 (6024,'6024',' Avanashi','Y',602),
 (6025,'6025',' Tiruppur','Y',602),
 (6026,'6026',' Palladam','Y',602),
 (6027,'6027',' Coimbatore North','Y',602),
 (6028,'6028',' Coimbatore South','Y',602),
 (6029,'6029',' Pollachi','Y',602),
 (6030,'6030',' Udumalaipettai','Y',602),
 (6031,'6031',' Valparai','Y',602),
 (6032,'6032',' Dindigul','Y',603),
 (6033,'6033',' Palani','Y',603),
 (6034,'6034',' Oddanchatram','Y',603),
 (6035,'6035',' Vedasandur','Y',603),
 (6036,'6036',' Natham','Y',603),
 (6037,'6037',' Dindigul','Y',603),
 (6038,'6038',' Kodaikanal','Y',603),
 (6039,'6039',' Nilakkottai','Y',603),
 (6040,'6040',' Karur  *','Y',604),
 (6041,'6041',' Aravakurichi','Y',604),
 (6042,'6042',' Karur','Y',604),
 (6043,'6043',' Krishnarayapuram','Y',604),
 (6044,'6044',' Kulithalai','Y',604),
 (6045,'6045',' Tiruchirappalli','Y',605),
 (6046,'6046',' Thottiyam','Y',605),
 (6047,'6047',' Musiri','Y',605),
 (6048,'6048',' Thuraiyur','Y',605),
 (6049,'6049',' Manachanallur','Y',605),
 (6050,'6050',' Lalgudi','Y',605),
 (6051,'6051',' Srirangam','Y',605),
 (6052,'6052',' Tiruchirappalli','Y',605),
 (6053,'6053',' Manapparai','Y',605),
 (6054,'6054',' Perambalur  *','Y',606),
 (6055,'6055',' Veppanthattai','Y',606),
 (6056,'6056',' Perambalur','Y',606),
 (6057,'6057',' Kunnam','Y',606),
 (6058,'6058',' Ariyalur  *','Y',607),
 (6059,'6059',' Sendurai','Y',607),
 (6060,'6060',' Udayarpalayam','Y',607),
 (6061,'6061',' Ariyalur','Y',607),
 (6062,'6062',' Cuddalore','Y',608),
 (6063,'6063',' Panruti','Y',608),
 (6064,'6064',' Cuddalore','Y',608),
 (6065,'6065',' Chidambaram','Y',608),
 (6066,'6066',' Kattumannarkoil','Y',608),
 (6067,'6067',' Virudhachalam','Y',608),
 (6068,'6068',' Tittakudi','Y',608),
 (6069,'6069',' Nagapattinam  *','Y',609),
 (6070,'6070',' Sirkali','Y',609),
 (6071,'6071',' Mayiladuthurai','Y',609),
 (6072,'6072',' Tharangambadi','Y',609),
 (6073,'6073',' Nagapattinam','Y',609),
 (6074,'6074',' Kilvelur','Y',609),
 (6075,'6075',' Thirukkuvalai','Y',609),
 (6076,'6076',' Vedaranyam','Y',609),
 (6077,'6077',' Thiruvarur','Y',610),
 (6078,'6078',' Valangaiman','Y',610),
 (6079,'6079',' Kodavasal','Y',610),
 (6080,'6080',' Nannilam','Y',610),
 (6081,'6081',' Thiruvarur','Y',610),
 (6082,'6082',' Needamangalam','Y',610),
 (6083,'6083',' Mannargudi','Y',610),
 (6084,'6084',' Thiruthuraipoondi','Y',610),
 (6085,'6085',' Thanjavur','Y',611),
 (6086,'6086',' Thiruvidaimarudur','Y',611),
 (6087,'6087',' Kumbakonam','Y',611),
 (6088,'6088',' Papanasam','Y',611),
 (6089,'6089',' Thiruvaiyaru','Y',611),
 (6090,'6090',' Thanjavur','Y',611),
 (6091,'6091',' Orathanadu','Y',611),
 (6092,'6092',' Pattukkottai','Y',611),
 (6093,'6093',' Peravurani','Y',611),
 (6094,'6094',' Pudukkottai','Y',612),
 (6095,'6095',' Iluppur','Y',612),
 (6096,'6096',' Kulathur','Y',612),
 (6097,'6097',' Gandarvakkottai','Y',612),
 (6098,'6098',' Pudukkottai','Y',612),
 (6099,'6099',' Thirumayam','Y',612),
 (6100,'6100',' Alangudi','Y',612),
 (6101,'6101',' Aranthangi','Y',612),
 (6102,'6102',' Manamelkudi','Y',612),
 (6103,'6103',' Avudayarkoil','Y',612),
 (6104,'6104',' Sivaganga','Y',613),
 (6105,'6105',' Tirupathur','Y',613),
 (6106,'6106',' Karaikkudi','Y',613),
 (6107,'6107',' Devakottai','Y',613),
 (6108,'6108',' Sivaganga','Y',613),
 (6109,'6109',' Manamadurai','Y',613),
 (6110,'6110',' Ilayangudi','Y',613),
 (6111,'6111',' Madurai','Y',614),
 (6112,'6112',' Melur','Y',614),
 (6113,'6113',' Madurai North','Y',614),
 (6114,'6114',' Vadipatti','Y',614),
 (6115,'6115',' Usilampatti','Y',614),
 (6116,'6116',' Peraiyur','Y',614),
 (6117,'6117',' Thirumangalam','Y',614),
 (6118,'6118',' Madurai South','Y',614),
 (6119,'6119',' Theni  *','Y',615),
 (6120,'6120',' Bodinayakanur','Y',615),
 (6121,'6121',' Periyakulam','Y',615),
 (6122,'6122',' Theni ','Y',615),
 (6123,'6123',' Uthamapalayam','Y',615),
 (6124,'6124',' Andipatti','Y',615),
 (6125,'6125',' Virudhunagar','Y',616),
 (6126,'6126',' Rajapalayam','Y',616),
 (6127,'6127',' Srivilliputhur','Y',616),
 (6128,'6128',' Sivakasi','Y',616),
 (6129,'6129',' Virudhunagar','Y',616),
 (6130,'6130',' Kariapatti','Y',616),
 (6131,'6131',' Tiruchuli','Y',616),
 (6132,'6132',' Aruppukkottai','Y',616),
 (6133,'6133',' Sattur','Y',616),
 (6134,'6134',' Ramanathapuram','Y',617),
 (6135,'6135',' Tiruvadanai','Y',617),
 (6136,'6136',' Paramakudi','Y',617),
 (6137,'6137',' Mudukulathur','Y',617),
 (6138,'6138',' Kamuthi','Y',617),
 (6139,'6139',' Kadaladi','Y',617),
 (6140,'6140',' Ramanathapuram','Y',617),
 (6141,'6141',' Rameswaram','Y',617),
 (6142,'6142',' Thoothukkudi','Y',618),
 (6143,'6143',' Kovilpatti','Y',618),
 (6144,'6144',' Ettayapuram','Y',618),
 (6145,'6145',' Vilathikulam','Y',618),
 (6146,'6146',' Ottapidaram','Y',618),
 (6147,'6147',' Thoothukkudi','Y',618),
 (6148,'6148',' Srivaikuntam','Y',618),
 (6149,'6149',' Tiruchendur','Y',618),
 (6150,'6150',' Sathankulam','Y',618),
 (6151,'6151',' Tirunelveli ','Y',619),
 (6152,'6152',' Sivagiri','Y',619),
 (6153,'6153',' Sankarankoil','Y',619),
 (6154,'6154',' Veerakeralamputhur','Y',619),
 (6155,'6155',' Tenkasi','Y',619),
 (6156,'6156',' Shenkottai','Y',619),
 (6157,'6157',' Alangulam','Y',619),
 (6158,'6158',' Tirunelveli','Y',619),
 (6159,'6159',' Palayamkottai','Y',619),
 (6160,'6160',' Ambasamudram','Y',619),
 (6161,'6161',' Nanguneri','Y',619),
 (6162,'6162',' Radhapuram','Y',619),
 (6163,'6163',' Kanniyakumari','Y',620),
 (6164,'6164',' Vilavancode','Y',620),
 (6165,'6165',' Kalkulam','Y',620),
 (6166,'6166',' Thovala','Y',620),
 (6167,'6167',' Agastheeswaram','Y',620),
 (6168,'6168',' PONDICHERRY','Y',621),
 (6169,'6169',' Yanam','Y',622),
 (6170,'6170',' Pondicherry','Y',623),
 (6171,'6171',' Mannadipet Commune Panchayat','Y',623),
 (6172,'6172',' Villianur Commune Panchayat','Y',623),
 (6173,'6173',' Ariankuppam Commune Panchayat','Y',623),
 (6174,'6174',' Nettapakkam Commune Panchayat','Y',623),
 (6175,'6175',' Bahour Commune Panchayat','Y',623),
 (6176,'6176',' Mahe','Y',624),
 (6177,'6177',' Karaikal','Y',625),
 (6178,'6178',' Nedungadu Commune Panchayat','Y',625),
 (6179,'6179',' Kottucherry Commune Panchayat','Y',625),
 (6180,'6180',' Thirunallar Commune Panchayat','Y',625),
 (6181,'6181',' Neravy Commune Panchayat','Y',625),
 (6182,'6182',' Thirumalairayan Pattinam Commune Panchayat','Y',625),
 (6183,'6183',' ANDAMAN & NICOBAR ISLANDS','Y',626),
 (6184,'6184',' Andamans','Y',627),
 (6185,'6185',' Diglipur','Y',627),
 (6186,'6186',' Mayabunder','Y',627),
 (6187,'6187',' Rangat','Y',627),
 (6188,'6188',' Ferrargunj','Y',627),
 (6189,'6189',' Port Blair','Y',627),
 (6190,'6190',' Nicobars','Y',628),
 (6191,'6191',' Car Nicobar','Y',628),
 (6192,'6192',' Nancowry','Y',628),
 (6193,'6193','Pimpalgaon','Y',502);
/*!40000 ALTER TABLE `taluka` ENABLE KEYS */;



ALTER TABLE `qfix`.`student` ADD COLUMN `state_id` INT NULL  AFTER `isDelete` , ADD COLUMN `district_id` INT NULL  AFTER `state_id` , ADD COLUMN `taluka_id` INT NULL  AFTER `district_id` , 

  ADD CONSTRAINT `fk_student_state_idx`

  FOREIGN KEY (`state_id` )

  REFERENCES `qfix`.`state` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `fk_student_district_idx`

  FOREIGN KEY (`district_id` )

  REFERENCES `qfix`.`district` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `fk_student_taluka_idx`

  FOREIGN KEY (`taluka_id` )

  REFERENCES `qfix`.`taluka` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_student_state1_idx_idx` (`state_id` ASC) 

, ADD INDEX `fk_student_district_idx_idx` (`district_id` ASC) 

, ADD INDEX `fk_student_taluka_idx_idx` (`taluka_id` ASC) ;



ALTER TABLE `qfix`.`teacher` ADD COLUMN `state_id` INT NULL  AFTER `isDelete` , ADD COLUMN `district_id` INT NULL  AFTER `state_id` , ADD COLUMN `taluka_id` INT NULL  AFTER `district_id` , 

  ADD CONSTRAINT `fk_teacher_state_idx`

  FOREIGN KEY (`state_id` )

  REFERENCES `qfix`.`state` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `fk_teacher_district_idx`

  FOREIGN KEY (`district_id` )

  REFERENCES `qfix`.`district` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `fk_teacher_taluka_idx`

  FOREIGN KEY (`taluka_id` )

  REFERENCES `qfix`.`taluka` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_teacher_state1_idx_idx` (`state_id` ASC) 

, ADD INDEX `fk_teacher_district_idx_idx` (`district_id` ASC) 

, ADD INDEX `fk_teacher_taluka_idx_idx` (`taluka_id` ASC) ;


/* 01-09-2016 */


/* 03-09-2016 */
ALTER TABLE `qfix`.`event` ADD COLUMN `sent_by` INTEGER COMMENT 'user_id' AFTER `academic_year_id`,
 ADD CONSTRAINT `FK_event_sentBy` FOREIGN KEY `FK_event_sentBy` (`sent_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;




	insert into menu_permission(menu_id,permission_name,url) values(11,'View Event', 'event.view');

	insert into role_menu values(4,11);

  	insert into role_menu_permission values(61,4);
	insert into role_menu_permission values(62,4);
	insert into role_menu_permission values(63,4);
  	insert into role_menu_permission values(64,4);
	insert into role_menu_permission values(66,4);
	insert into role_menu_permission values(130,4);

/* 03-09-2016 */


/* 08-09-2016 */

	/* GROUP Creation Queiry*/
	insert into role_menu values(4,3);

  	insert into role_menu_permission values(33,4);
	insert into role_menu_permission values(34,4);
	insert into role_menu_permission values(35,4);
 	insert into role_menu_permission values(36,4);
	insert into role_menu_permission values(37,4);
	

	/* Added Late payment charges and divisioId in  Fees */
	ALTER TABLE `qfix`.`fees` ADD COLUMN `late_payment_charges` INT(11) NULL  AFTER `academic_year_id` ;

	ALTER TABLE `qfix`.`fees` CHANGE COLUMN `late_payment_charges` `late_payment_charges` INT(11) NULL  , ADD COLUMN `division_id` INT(11) NULL  AFTER `late_payment_charges` , 

  	ADD CONSTRAINT `fk_fees_division`

  	FOREIGN KEY (`division_id` )

  	REFERENCES `qfix`.`division` (`id` )

 	ON DELETE NO ACTION

  	ON UPDATE NO ACTION , ADD INDEX `fk_fees_division_idx` (`division_id` ASC) ;

/* Added table student_late_fee_payment*/
DROP TABLE IF EXISTS `qfix`.`student_late_fee_payment`;
CREATE TABLE  `qfix`.`student_late_fee_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `late_payment_charges` int(11) NOT NULL,
  `is_student_specific` char(1) NOT NULL,
  `is_paid` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_student_late_fee_payment_student_idx_idx` (`student_id`),
  KEY `student_late_fee_fees_idx_idx` (`fees_id`),
  CONSTRAINT `fk_student_late_fee_payment_student_idx` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_late_fee_fees_idx` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1278 DEFAULT CHARSET=latin1;

/* 08-09-2016 */


/* 12-09-2016 */
	/* Added skip timetbale on holiday flag*/
  ALTER TABLE `qfix`.`working_days` CHANGE COLUMN `monday` `monday` CHAR(1) NOT NULL DEFAULT 'N'  , CHANGE COLUMN `tuesday` `tuesday` CHAR(1) NOT NULL DEFAULT 'N'  , CHANGE COLUMN `wednesday` `wednesday` CHAR(1) NOT NULL DEFAULT 'N'  , CHANGE COLUMN `thursday` `thursday` CHAR(1) NOT NULL DEFAULT 'N'  , CHANGE COLUMN `friday` `friday` CHAR(1) NOT NULL DEFAULT 'N'  , CHANGE COLUMN `saturday` `saturday` CHAR(1) NOT NULL DEFAULT 'N'  , CHANGE COLUMN `sunday` `sunday` CHAR(1) NOT NULL  , ADD COLUMN `skip_timetable_on_holiday` CHAR(1) NOT NULL  AFTER `branch_id` ;
  
/* 12-09-2016 */
  
  
  /* 13-09-2016   Added timetable*/

   DROP TABLE IF EXISTS `qfix`.`timetable`;
CREATE TABLE  `qfix`.`timetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standard_id` int(11) NOT NULL,
  `json` text CHARACTER SET latin1 NOT NULL,
  `is_delete` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_timetable_standard_id_idx` (`standard_id`),
  CONSTRAINT `fk_timetable_standard_id` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `qfix`.`timetable_detail`;
CREATE TABLE  `qfix`.`timetable_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `date` date NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `status` char(1) CHARACTER SET latin1 NOT NULL COMMENT 'Valid status N, D',
  `timetable_id` int(11) NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `_id` bigint(20) NOT NULL DEFAULT '1234',
  PRIMARY KEY (`id`),
  KEY `fk_timetable_id_idx` (`timetable_id`),
  CONSTRAINT `fk_timetable_id` FOREIGN KEY (`timetable_id`) REFERENCES `timetable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `qfix`.`timetable_division`;
CREATE TABLE  `qfix`.`timetable_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timetable_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_timetable_division_timetable_idx` (`timetable_id`),
  KEY `timetable_division_division_idx` (`division_id`),
  CONSTRAINT `fk_timetable_division_timetable_idx` FOREIGN KEY (`timetable_id`) REFERENCES `timetable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `timetable_division_division_idx` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


/* Timetable menu query */ 

INSERT INTO `menu_items` (`menu_id`,`menu_name`,`menu_description`,`state_url`,`menu_position`) VALUES
(38,'Timetable',NULL,'timetable.list',7);


INSERT INTO `menu_permission` (`menu_permission_id`,`menu_id`,`permission_name`,`url`,`rest_url`) VALUES 
 (132,38,'Add','timetable.add',NULL),
 (133,38,'Edit','timetable.edit',NULL),
 (134,38,'Save','',NULL),
 (135,38,'List','timetable.list',NULL),
 (136,38,'View','timetable.view',NULL);


 INSERT INTO `role_menu` (`role_id`,`menu_id`) VALUES 
  (1,38),
  (6,38),
  (4,38);

INSERT INTO `role_menu_permission` (`menu_permission_id`,`role_id`) VALUES 
 (132,'1'),
 (133,'1'),
 (134,'1'),
 (135,'1'),
 
 (132,'6'),
 (133,'6'),
 (134,'6'),
 (135,'6'),

 (135,'4'),
 (136,'4');
  /* 13-09-2016*/


 /* 20-09-2016 */

 /* FOR USER ID ADDED*/
 
	/* BRANCH STUDENT PARENT*/ 
 insert into branch_student_parent (branch_id, student_id, parent_id)
select branch_id,id,parent_id from student where branch_id IS NOT NULL and parent_id IS NOT NULL;


 /* NOTICE */
 
ALTER TABLE `qfix`.`notice_individual` ADD COLUMN `for_user_id` INTEGER AFTER `user_id`,
 ADD CONSTRAINT `FK_notice_individual_for_user_id` FOREIGN KEY `FK_notice_individual_for_user_id` (`for_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

/* ALTER TABLE `notice_individual` DROP COLUMN `read_status` ; */ 
update  `notice_individual` set for_user_id = user_id;

insert IGNORE into notice_individual (notice_id, user_id,  for_user_id)
select ni.notice_id,p.user_id, ni.user_id from
notice_individual ni
inner join student s on ni.user_id= s.user_id
inner join parent p on p.id= s.parent_id;


ALTER TABLE `qfix`.`notice_individual` MODIFY COLUMN `for_user_id` INTEGER NOT NULL,
 DROP PRIMARY KEY,
 ADD PRIMARY KEY  USING BTREE(`notice_id`, `user_id`, `for_user_id`);
/* NOTICE INDIVIDUAL END */ 

CREATE TABLE `notice_individual_read_status` (
  `notice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `for_user_id` int(11) NOT NULL,
  `read_status` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`notice_id`,`user_id`,`for_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




ALTER TABLE `notice_individual_read_status` CHARACTER SET = utf8 , 

  ADD CONSTRAINT `fk_notice_individual_read_status_notice_id`

  FOREIGN KEY (`notice_id` )

  REFERENCES `notice` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `fk_notice_individual_read_status_user_id`

  FOREIGN KEY (`user_id` )

  REFERENCES `user` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION,

  ADD CONSTRAINT `fk_notice_individual_read_status_for_user_id`

  FOREIGN KEY (`for_user_id` )

  REFERENCES `user` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_notice_individual_read_status_notice_id_idx` (`notice_id` ASC)

, ADD INDEX `fk_notice_individual_read_status_user_id_idx` (`user_id` ASC) 

, ADD INDEX `fk_notice_individual_read_status_for_user_id_idx` (`for_user_id` ASC) ;



/*  EDIARY by Vishal sir.*/
ALTER TABLE `user_ediary` ADD COLUMN `for_user_id` INT(11) NULL  AFTER `user_id` , 

  ADD CONSTRAINT `fk_ediaryindividual_foruserid`

  FOREIGN KEY (`for_user_id` )

  REFERENCES `user` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_ediaryindividual_foruserid_idx` (`for_user_id` ASC) ;



update  `user_ediary` set for_user_id = user_id;
 
insert IGNORE into user_ediary (ediary_id, user_id,  for_user_id)
select ue.ediary_id ,p.user_id, ue.user_id from
user_ediary as ue
inner join student s on ue.user_id= s.user_id
inner join parent p on p.id= s.parent_id;


ALTER TABLE `qfix`.`user_ediary` DROP PRIMARY KEY,
 ADD PRIMARY KEY  USING BTREE(`ediary_id`, `user_id`, `for_user_id`);
 
 
CREATE  TABLE `user_ediary_read_status` (

  `ediary_id` INT(11) NOT NULL ,

  `user_id` INT(11) NOT NULL ,

  `for_user_id` INT(11) NOT NULL,

  `read_status` CHAR(1) NOT NULL DEFAULT 'N' ,

  PRIMARY KEY (`ediary_id`, `user_id`, `for_user_id`) ,

  INDEX `fk_user_ediary_read_status_ediary_id_idx` (`ediary_id` ASC) ,

  INDEX `fk_user_ediary_read_status_user_id_idx` (`user_id` ASC) ,

  INDEX `fk_user_ediary_read_status_for_user_id_idx` (`for_user_id` ASC) ,

  CONSTRAINT `fk_user_ediary_read_status_ediary_id`

    FOREIGN KEY (`ediary_id` )

    REFERENCES `ediary` (`id` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION,

  CONSTRAINT `fk_user_ediary_read_status_user_id`

    FOREIGN KEY (`user_id` )

    REFERENCES `user` (`id` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION,

  CONSTRAINT `fk_user_ediary_read_status_for_user_id`

    FOREIGN KEY (`for_user_id` )

    REFERENCES `user` (`id` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION);
    
    

 /* Student Marks By Sushant Sir*/

ALTER TABLE `qfix`.`student_marks`

  ADD CONSTRAINT `FK_student_marks_subject`

  FOREIGN KEY (`subject_id` )

  REFERENCES `qfix`.`subject` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION;

CREATE TABLE `student_result_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `qfix`.`student_result` ADD COLUMN `result_type_id` INT(11) NULL  AFTER `academic_year_id` ,

  ADD CONSTRAINT `FK_student_result_type`

  FOREIGN KEY (`result_type_id` )

  REFERENCES `qfix`.`student_result_type` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `FK_student_result_type_idx` (`result_type_id` ASC) ;

ALTER TABLE `qfix`.`student_result` ADD COLUMN `title` VARCHAR(45) NULL  AFTER `result_type_id` ;

INSERT INTO `qfix`.`student_result_type` (`id`, `name`) VALUES ('1', 'MARKS');

INSERT INTO `qfix`.`student_result_type` (`id`, `name`) VALUES ('2', 'GRADES');


/*  EVENT for_user_id added*/
ALTER TABLE `user_event` ADD COLUMN `for_user_id` INT(11) NULL  AFTER `status` ,

  ADD CONSTRAINT `FK_user_event_for_user_id`

  FOREIGN KEY (`for_user_id` )

  REFERENCES `user` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `FK_user_event_for_user_id_idx` (`for_user_id` ASC) ;

update `user_event` set for_user_id = user_id;

insert IGNORE into user_event (user_id, role_id, entity_id, event_id, event_group_id, status, for_user_id)
select p.user_id, 3,p.id, ue.event_id, ue.event_group_id, ue.status, ue.user_id from
user_event as ue
inner join student s on ue.user_id= s.user_id
inner join parent p on p.id= s.parent_id;

ALTER TABLE `qfix`.`user_event` MODIFY COLUMN `for_user_id` INTEGER NOT NULL;


CREATE TABLE `user_event_read_status` (
  `user_id` int(11) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `entity_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `for_user_id` int(11) DEFAULT NULL,
  `read_status` char(1) NOT NULL DEFAULT 'N',
  KEY `FK_user_event_read_status_user` (`user_id`),
  KEY `FK_user_event_read_status_role` (`role_id`),
  KEY `FK_user_event_read_status_event` (`event_id`),
  KEY `FK_user_event_read_status_for_user_id_idx` (`for_user_id`),
  CONSTRAINT `FK_user_event_read_status_for_user_id` FOREIGN KEY (`for_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_event_read_status_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_event_read_status_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_event_read_status_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `user_event_read_status` CHANGE COLUMN `for_user_id` `for_user_id` INT(11) NOT NULL  ;

ALTER TABLE `user_event_read_status`

ADD PRIMARY KEY (`user_id`, `event_id`, `for_user_id`) ;

/*  EVENT for_user_id END*/

/* 20-09-2016*/


/* 24-09-2016*/

ALTER TABLE `qfix`.`student_event` ADD PRIMARY KEY (`user_id`, `event_id`);

/* 24-09-2016*/ 

/* 28-09-2016 */

DROP TABLE IF EXISTS `profession`;
CREATE TABLE `profession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `profession` (`id`,`name`) VALUES 
 (1,'Doctor'),
 (2,'Engineer'),
 (3,'Charter Accountant'),
 (4,'Service'),
 (5,'Business'),
 (6,'House wife');


DROP TABLE IF EXISTS `income_range`;
CREATE TABLE `income_range` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `income_range` (`id`,`name`) VALUES 
 (1,'0-1,00,000'),
 (2,'1,00,00-5,00,000'),
 (3,'5,00,000-10,00,000'),
 (4,'10,00,000 and above');

 ALTER TABLE `qfix`.`parent` ADD COLUMN `profession_id` INTEGER AFTER `photo`,
 ADD COLUMN `income_range_id` INTEGER AFTER `profession_id`,
 ADD CONSTRAINT `FK_parent_profession` FOREIGN KEY `FK_parent_profession` (`profession_id`)
    REFERENCES `profession` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
 ADD CONSTRAINT `FK_parent_income_range` FOREIGN KEY `FK_parent_income_range` (`income_range_id`)
    REFERENCES `income_range` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

 ALTER TABLE `qfix`.`parent_upload` ADD COLUMN `profession_id` INTEGER AFTER `student_id`,
 ADD COLUMN `income_range_id` INTEGER AFTER `profession_id`,
 ADD CONSTRAINT `FK_parent_upload_profession` FOREIGN KEY `FK_parent_upload_profession` (`profession_id`)
    REFERENCES `profession` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
 ADD CONSTRAINT `FK_parent_upload_income_range` FOREIGN KEY `FK_parent_upload_income_range` (`income_range_id`)
    REFERENCES `income_range` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

/* 28-06-2016 */


/* 07-10-2016 */
    ALTER TABLE `payment`.`
    ` ADD COLUMN `customer_phone` VARCHAR(15) NULL  AFTER `payment_for_description` , ADD COLUMN `customer_address` VARCHAR(200) NULL  AFTER `customer_phone` , ADD COLUMN `customer_city` VARCHAR(50) NULL  AFTER `customer_address` , ADD COLUMN `customer_state` VARCHAR(50) NULL  AFTER `customer_city` , ADD COLUMN `customer_country` VARCHAR(50) NULL  AFTER `customer_state` , ADD COLUMN `customer_pincode` VARCHAR(15) NULL  AFTER `customer_country` ;
/* 07-10-2016 */
    
/* 20-10-2016 */
    
    ALTER TABLE `student` ADD COLUMN `is_registration_email_sent` CHAR(1) NOT NULL DEFAULT 'N'  AFTER `taluka_id` ;
	ALTER TABLE `parent` ADD COLUMN `is_registration_email_sent` CHAR(1) NOT NULL DEFAULT 'N'  AFTER `income_range_id`
/* 20-10-2016 */

/* 21-06-2016 */
	ALTER TABLE `qfix`.`fees` ADD COLUMN `student_id` INT(11) NULL  AFTER `division_id` ,

  ADD CONSTRAINT `FK_fees_student`

  FOREIGN KEY (`student_id` )

  REFERENCES `qfix`.`student` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `FK_fees_student_idx` (`student_id` ASC) ;


DROP TABLE IF EXISTS `qfix`.`service_request`;
CREATE TABLE  `qfix`.`service_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_type` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `email_response` varchar(45) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `FK_service_request_branch_idx` (`branch_id`),
  CONSTRAINT `FK_service_request_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `qfix`.`service_request` (`id`, `request_type`, `email`, `email_response`, `branch_id`) VALUES ('1', 'Lunch Coupon1', 'lucnh1@test.com', 'lunch1 response', '1');

INSERT INTO `qfix`.`service_request` (`id`, `request_type`, `email`, `email_response`, `branch_id`) VALUES ('2', 'Club Coupon1', 'club1@test.com', 'club1 response', '1');

INSERT INTO `qfix`.`service_request` (`id`, `request_type`, `email`, `email_response`, `branch_id`) VALUES ('3', 'Lunch Coupon2', 'lucnh2@test.com', 'lunch2 response', '2');

INSERT INTO `qfix`.`service_request` (`id`, `request_type`, `email`, `email_response`, `branch_id`) VALUES ('4', 'Club Coupon2', 'club2@test.com', 'club2 response', '2');

  /* 21-06-2016 */
  
/* 22-06-2016 */

CREATE TABLE `branch_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `is_payment_enabled_for_student` char(1) NOT NULL DEFAULT 'N',
  `create_parent_profile` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_branch_configuration_branch_id_idx` (`branch_id`),
  CONSTRAINT `fk_branch_configuration_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

/* 22-06-2016 */




/* 28-10-2016*/

insert into taluka values(6194, '6194', 'SUKDEVPUR', 'Y', 337);
ALTER TABLE `qfix`.`student` MODIFY COLUMN `registration_code` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;
ALTER TABLE `qfix`.`student_upload` MODIFY COLUMN `registration_code` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

/* 28-10-2016 */ 

/* 02-11-2016 */

/* PAYMENT GATEWAY CONFIGURATION SQL FOR NEW SCHOOLS */
insert into `payment`.`payment_gateway_configuration` values(4, 'TPSL', 'Teckprocess', 'PAILAN COLLEGE OF MANAGEMENT AND TECH', '6636259131GPLFAX', '6014291051IBXWQV', 'test', 'T45223', 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew', 1);
insert into `payment`.`payment_gateway_configuration` values(5, 'TPSL', 'Teckprocess', 'PAILAN EDUCATIONAL TRUST', '6636259131GPLFAX', '6014291051IBXWQV', 'test', 'T45222', 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew', 1);
insert into `payment`.`payment_gateway_configuration` values(6, 'TPSL', 'Teckprocess', 'PAILAN TECHNICAL CAMPUS', '6636259131GPLFAX', '6014291051IBXWQV', 'test', 'T45224', 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew', 1);
insert into `payment`.`payment_gateway_configuration` values(7, 'TPSL', 'Teckprocess', 'PAILAN WORLD SCHOOL', '6636259131GPLFAX', '6014291051IBXWQV', 'test', 'T45225', 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew', 1);

insert into `payment`.`payment_gateway_configuration` values(8, 'TPSL', 'Teckprocess', 'PRAHLADRAI DALMIA  LIONS COLLEGE JR', '2018558970PUFMKO', '2983857609XWTNFV', 'TEST', 'T46070', 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew', 1);

insert into `qfix`.`payment_setting` values(12, 4, '0', '0', 34);
insert into `qfix`.`payment_setting` values(13, 5, '0', '0', 36);
insert into `qfix`.`payment_setting` values(14, 6, '0', '0', 35);
insert into `qfix`.`payment_setting` values(15, 7, '0', '0', 33);

insert into branch_configuration values(5, 109, 'Y', 'Y');
insert into branch_configuration values(6, 110, 'Y', 'Y');
insert into branch_configuration values(7, 111, 'Y', 'Y');

ALTER TABLE `qfix`.`student` MODIFY COLUMN `religion` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;
ALTER TABLE `qfix`.`student_upload` MODIFY COLUMN `religion` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;

/* 02-11-2016*/




/* 04-11-2016 */

/* ISG (payment_setting)*/

ALTER TABLE `qfix`.`payment_setting` ADD COLUMN `branch_id` INT(11) NULL  AFTER `institute_id` , ADD COLUMN `fees_id` INT(11) NULL  AFTER `branch_id` , 

  ADD CONSTRAINT `FK_payment_setting_branch`

  FOREIGN KEY (`branch_id` )

  REFERENCES `qfix`.`branch` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `FK_payment_setting_fees`

  FOREIGN KEY (`fees_id` )

  REFERENCES `qfix`.`fees` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `FK_payment_setting_branch_idx` (`branch_id` ASC) 

, ADD INDEX `FK_payment_setting_fees_idx` (`fees_id` ASC) ;


/* PAYMENT */

ALTER TABLE `payment`.`payment_gateway_configuration` ADD COLUMN `branch_id` INT(11) NULL  AFTER `payment_gateway_type` ;

DROP TABLE IF EXISTS `payment`.`payment_gateway_configuration_scheme`;
CREATE TABLE  `payment`.`payment_gateway_configuration_scheme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheme_code` varchar(45) NOT NULL,
  `payment_gateway_configuration_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `is_default` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_scheme_gateway_configuration_id_idx` (`payment_gateway_configuration_id`),
  CONSTRAINT `fk_scheme_gateway_configuration_id` FOREIGN KEY (`payment_gateway_configuration_id`) REFERENCES `payment_gateway_configuration` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `qfix`.`fees` ADD COLUMN `is_split_payment` CHAR(1) NOT NULL DEFAULT 'N' AFTER `student_id`;
/* 04-11-2016 */



/* 07-11-2016 */

ALTER TABLE `qfix`.`payment_setting` DROP FOREIGN KEY `FK_payment_setting_fees` ;


ALTER TABLE `qfix`.`payment_setting` DROP COLUMN `fees_id`

, DROP INDEX `FK_payment_setting_fees_idx` ;


DROP TABLE IF EXISTS `qfix`.`fees_split_configuration`;
CREATE TABLE  `qfix`.`fees_split_configuration` (
  `fees_id` int(11) NOT NULL,
  `scheme_code_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `is_seed_account` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`fees_id`,`scheme_code_id`),
  KEY `FK_fees_split_configuration_fees_id_idx` (`fees_id`),
  CONSTRAINT `FK_fees_split_configuration_fees_id` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


    
/* 07-11-2016 */ 

/* 09-11-2016*/

ALTER TABLE `qfix`.`fees` ADD COLUMN `is_partial_payment_allowed` CHAR(1) NOT NULL DEFAULT 'N' AFTER `is_split_payment`;

/* 09-11-2016*/

/* 11-11-2016 */

CREATE TABLE `qfix`.`fees_code_configuration` (
  `code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_branch_id_UNIQUE` (`code`,`branch_id`),
  KEY `fk_fees_code_configuration_branch_id_idx` (`branch_id`),
  CONSTRAINT `fk_fees_code_configuration_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

ALTER TABLE `qfix`.`fees` ADD COLUMN `fees_code_configuration_id` INT(11) NULL  AFTER `is_split_payment` , 

  ADD CONSTRAINT `FK_fees_fees_code_configuration_id`

  FOREIGN KEY (`fees_code_configuration_id` )

  REFERENCES `qfix`.`fees_code_configuration` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `FK_fees_fees_code_configuration_id_idx` (`fees_code_configuration_id` ASC) ;


ALTER TABLE `qfix`.`student` ADD COLUMN `fees_code_configuration_id` INT(11) NULL  AFTER `is_registration_email_sent` ,

  ADD CONSTRAINT `FK_student_fees_code_configuration_id`

  FOREIGN KEY (`fees_code_configuration_id` )

  REFERENCES `qfix`.`fees_code_configuration` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `FK_student_fees_code_configuration_id_idx` (`fees_code_configuration_id` ASC) ;


ALTER TABLE `qfix`.`student_upload` ADD COLUMN `fees_code_configuration_id` INT(11) NULL  AFTER `isDelete` ,

  ADD CONSTRAINT `FK_student_upload_fees_code_configuration_id`

  FOREIGN KEY (`fees_code_configuration_id` )

  REFERENCES `qfix`.`fees_code_configuration` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `FK_student_fees_code_configuration_id_idx` (`fees_code_configuration_id` ASC) ;



ALTER TABLE `qfix`.`student_late_fee_payment` ADD COLUMN `discount_amount` DECIMAL(10,2) NULL DEFAULT 0  AFTER `is_paid` ;

ALTER TABLE `qfix`.`payment_setting` DROP COLUMN `institute_id`,
 DROP FOREIGN KEY `FK_payment_setting_institute`;

 ALTER TABLE `qfix`.`fees` MODIFY COLUMN `caste_id` INTEGER DEFAULT NULL;

/* 11-11-2016 */

/* 14-11-2016 */

 ALTER TABLE `qfix`.`fees` ADD COLUMN `is_delete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `is_partial_payment_allowed`;
 ALTER TABLE `qfix`.`head` DROP INDEX `name_UNIQUE`;

/* 14-11-2016 */
 
 

/* LIVE CHANGES DONE ON (15-11-2016)*/
 ALTER TABLE `payment`.`payment_gateway_configuration` DROP COLUMN `branch_id`;

insert into `payment`.`payment_gateway_configuration_scheme` values(1, 'QFIX', 1, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(2, 'QFIX', 2, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(3, 'test', 3, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(4, 'test', 4, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(5, 'test', 5, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(6, 'test', 6, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(7, 'test', 7, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(8, 'TEST', 8, 'Default Account', 'Y');
insert into `payment`.`payment_gateway_configuration_scheme` values(9, 'SDJR', 8, 'Seed Account', 'N');

insert into payment_setting values(16, 8, '0', '0', 30, 101);



 ALTER table `payment`.`payment_detail`  ADD COLUMN `split_payment_detail` VARCHAR(255) ;

/* 15-11-2016 */

/* 18-11-2016 */
 ALTER TABLE `qfix`.`student` MODIFY COLUMN `dob` DATE DEFAULT NULL;
 ALTER TABLE `qfix`.`parent` MODIFY COLUMN `email` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
 ALTER TABLE `qfix`.`parent_upload` MODIFY COLUMN `email` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;

 ALTER TABLE `qfix`.`student_upload` 
 MODIFY COLUMN `dob` DATE DEFAULT NULL,
 MODIFY COLUMN `email_address` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `mobile` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `registration_code` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `standard_id` INTEGER DEFAULT NULL,
 MODIFY COLUMN `division_id` INTEGER DEFAULT NULL,
 MODIFY COLUMN `roll_number` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `isDelete` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

/* 18-11-2016 */

/* 21-11-2016 */

 ALTER TABLE `qfix`.`student` ADD COLUMN `create_login` CHAR(1) NOT NULL DEFAULT 'N' AFTER `fees_code_configuration_id`;

 ALTER TABLE `qfix`.`parent_upload` ADD COLUMN `create_login` CHAR(1) NOT NULL DEFAULT 'N' AFTER `income_range_id`;

/* 21-11-2016 */

/* 28-11-2016*/
 	ALTER TABLE `qfix`.`student_upload` ADD COLUMN `hash_code` INTEGER NOT NULL AFTER `fees_code_configuration_id`;

 	ALTER TABLE `qfix`.`branch_student_parent` ADD COLUMN `parent_upload_id` INTEGER AFTER `useParentDetailsForComm`,
 	ADD CONSTRAINT `FK_branch_student_parent_parent_upload` FOREIGN KEY `FK_branch_student_parent_parent_upload` (`parent_upload_id`)
    REFERENCES `parent_upload` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

/* 28-11-2016 */

/* 01-12-2016 */
    ALTER TABLE `qfix`.`fees` MODIFY COLUMN `standard_id` INTEGER DEFAULT NULL;
    ALTER TABLE `qfix`.`fees` MODIFY COLUMN `description` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;
/* 01-12-2016 */

/* 02-12-2016 */
ALTER TABLE `qfix`.`academic_year` ADD COLUMN `is_delete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `branch_id`;
/* 02-12-2016 */


/* 05-12-2016 */
UPDATE `qfix`.`taluka` SET `name`='Nabadwip' WHERE `id`='2477';

INSERT INTO `qfix`.`district` (`code`, `name`, `active`, `state_id`) VALUES ('629', 'Alipurduar', 'Y', '19');
INSERT INTO `qfix`.`district` (`code`, `name`, `active`, `state_id`) VALUES ('630', 'Medinipur', 'Y', '19');

INSERT INTO `qfix`.`taluka` (`code`, `name`, `active`, `district_id`) VALUES ('1231', 'Alipurduar', 'Y', '629');
INSERT INTO `qfix`.`taluka` (`code`, `name`, `active`, `district_id`) VALUES ('6196', 'Khargram', 'Y', '630');


/* 05-12-2016 */

/* 06-12-2016 */
ALTER TABLE `payment`.`payment_detail` 
CHANGE COLUMN `customer_email` `customer_email` VARCHAR(100) NULL ;

ALTER TABLE `qfix`.`student` 
CHANGE COLUMN `line1` `line1` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL ;

ALTER TABLE `qfix`.`parent` 
CHANGE COLUMN `addressLine` `addressLine` VARCHAR(100) NULL DEFAULT NULL ;

ALTER TABLE `qfix`.`parent_upload` 
CHANGE COLUMN `addressLine` `addressLine` VARCHAR(100) NULL DEFAULT NULL ;

ALTER TABLE `qfix`.`student_upload` MODIFY COLUMN `line1` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;
/* 06-12-2016 */

/* 22-12-2016 */
ALTER TABLE `qfix`.`branch_configuration` MODIFY COLUMN `is_payment_enabled_for_student` CHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Y';
/* 22-12-2016 */

/*24-12-2016*/
ALTER TABLE `qfix`.`head` MODIFY COLUMN `academic_year_id` INTEGER DEFAULT NULL;
insert into `role_menu_permission` (menu_permission_id, role_id) values (137, 4);
/*24-12-2016*/


/*09-01-2017*/
DROP TABLE IF EXISTS `qfix`.`profile_status`;
CREATE TABLE  `qfix`.`profile_status` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_profile_created` char(1) NOT NULL DEFAULT 'N',
  `forum_profile_created` char(1) NOT NULL DEFAULT 'N',
  `shopping_profile_created` char(1) NOT NULL DEFAULT 'N',
  `groups_assigned` char(1) NOT NULL DEFAULT 'N',
  `chat_groups_assigned` char(1) NOT NULL DEFAULT 'N',
  `forum_groups_assigned` char(1) NOT NULL DEFAULT 'N',
  `events_assigned` char(1) NOT NULL DEFAULT 'N',
  `notices_assigned` char(1) NOT NULL DEFAULT 'N',
  `ediary_assigned` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `FK_profile_status_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*09-01-2017*/

/* 12-01-2017 */

DROP TRIGGER /*!50030 IF EXISTS */ `teacher_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `teacher_insert_trig` AFTER INSERT ON `teacher` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one


BEGIN
IF NOT EXISTS (SELECT id FROM contact WHERE user_id = NEW.user_id) THEN
	insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
	values(NEW.emailAddress, NEW.primaryContact, NEW.user_id, NEW.firstName, NEW.lastName, NEW.city, NEW.photo);
END IF;
END $$

DELIMITER ;

/* 12-01-2017 */


/* 13-01-2017 */

DROP TABLE IF EXISTS `qfix`.`branch_admin`;
CREATE TABLE  `qfix`.`branch_admin` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`branch_id`,`user_id`),
  KEY `FK_branch_admin_user` (`user_id`),
  CONSTRAINT `FK_branch_admin_1_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_branch_admin_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into branch_admin(user_id, branch_id) select u.id as user_id, b.id as branch_id from user as u
INNER JOIN branch as b on b.institute_id = u.institute_id ;

/* 13-01-2017*/


/*25-01-2017*/
ALTER TABLE `qfix`.`group_standard` 
DROP FOREIGN KEY `FK_group_standard_division`;
ALTER TABLE `qfix`.`group_standard` 
CHANGE COLUMN `division_id` `division_id` INT(11) NULL ;
ALTER TABLE `qfix`.`group_standard` 
ADD CONSTRAINT `FK_group_standard_division`
  FOREIGN KEY (`division_id`)
  REFERENCES `qfix`.`division` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  /*25-01-2017*/


  /* 08-02-2017 */

DROP TABLE IF EXISTS `qfix`.`branch_display_layout`;
CREATE TABLE  `qfix`.`branch_display_layout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fees_description` varchar(45) NOT NULL,
  `fees_amount` double NOT NULL,
  `other_description` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_branch_display_layout_branch` (`branch_id`),
  CONSTRAINT `FK_branch_display_layout_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qfix`.`fees_display_layout`;
CREATE TABLE  `qfix`.`fees_display_layout` (
  `fees_id` int(11) NOT NULL,
  `branch_display_layout_id` int(11) NOT NULL,
  PRIMARY KEY (`fees_id`,`branch_display_layout_id`),
  KEY `FK_fees_display_layout_branch_display_layout` (`branch_display_layout_id`),
  CONSTRAINT `FK_fees_display_layout_fees` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`),
  CONSTRAINT `FK_fees_display_layout_branch_display_layout` FOREIGN KEY (`branch_display_layout_id`) REFERENCES `branch_display_layout` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/* 08-02-2017 */ 


/* 10-02-2017 */

DROP TABLE IF EXISTS `qfix`.`late_fees_payment_detail`;
CREATE TABLE  `qfix`.`late_fees_payment_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `frequency` varchar(45) DEFAULT NULL,
  `by_days` varchar(45) DEFAULT NULL,
  `amount` double NOT NULL,
  `maximum_amount` double DEFAULT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_late_fees_payment_detail_branch` (`branch_id`),
  CONSTRAINT `FK_late_fees_payment_detail_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;


ALTER TABLE `qfix`.`fees` DROP COLUMN `late_payment_charges`,
 ADD COLUMN `recurring` CHAR(1) NOT NULL DEFAULT 'N' AFTER `is_delete`,
 ADD COLUMN `recurring_frequency` VARCHAR(45) AFTER `recurring`,
 ADD COLUMN `by_days` VARCHAR(45) AFTER `recurring_frequency`,
 ADD COLUMN `late_fee_detail_id` INTEGER AFTER `by_days`,
 ADD COLUMN `reminder_start_date` DATE AFTER `late_fee_detail_id`,
 ADD COLUMN `fee_type` VARCHAR(45) NOT NULL AFTER `reminder_start_date`,
 ADD CONSTRAINT `FK_fees_late_fees_payment_detail` FOREIGN KEY `FK_fees_late_fees_payment_detail` (`late_fee_detail_id`)
    REFERENCES `late_fees_payment_detail` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `qfix`.`fees` ADD COLUMN `by_date` INTEGER AFTER `fee_type`;

ALTER TABLE `qfix`.`late_fees_payment_detail` ADD COLUMN `by_date` INTEGER AFTER `is_delete`;

/* 10-02-2017 */

/* 23-02-2017 */

ALTER TABLE `qfix`.`fees` ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `by_date`,
 ADD COLUMN `created_by` INTEGER AFTER `created_at`,
 ADD COLUMN `updated_at` TIMESTAMP AFTER `created_by`,
 ADD COLUMN `updated_by` INTEGER AFTER `updated_at`,
 ADD CONSTRAINT `FK_fees_user` FOREIGN KEY `FK_fees_user` (`created_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
 ADD CONSTRAINT `FK_fees_user_2` FOREIGN KEY `FK_fees_user_2` (`updated_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
/* 23-02-2017 */
   
/* 09-03-2017 */    

 ALTER TABLE `qfix`.`fees_schedule` 
 DROP COLUMN `carry_forward_id`,
 DROP COLUMN `carry_forward_amount`,
 ADD COLUMN `fee_start_date` DATE NOT NULL AFTER `reminder_start_date`,
 DROP FOREIGN KEY `FK_fees_schedule_fees_schedule_2`;

ALTER TABLE `qfix`.`fees_schedule` MODIFY COLUMN `can_pay` CHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Y',
 MODIFY COLUMN `is_delete` CHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'N';

 ALTER TABLE `qfix`.`fees_schedule` MODIFY COLUMN `status` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;


  /* MASTER FEES SCHEDULE */

 DROP TABLE IF EXISTS `qfix`.`fees_schedule`;
CREATE TABLE  `qfix`.`fees_schedule` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `fees_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `base_amount` double NOT NULL DEFAULT '0',
  `late_payment_charges` double NOT NULL DEFAULT '0',
  `total_amount` double NOT NULL,
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `status` varchar(20) NOT NULL,
  `partial_paid_amount` double DEFAULT NULL,
  `pyment_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_fees_schedule_id` bigint(25) DEFAULT NULL,
  `can_pay` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `fee_for_duration` varchar(45) DEFAULT NULL,
  `late_payment_for_fees_schedule` bigint(20) DEFAULT NULL,
  `reminder_start_date` date DEFAULT NULL,
  `fee_start_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fees_schedule_fees` (`fees_id`),
  KEY `FK_fees_schedule_user` (`user_id`),
  KEY `FK_fees_schedule_payments` (`pyment_id`),
  KEY `FK_fees_schedule_fees_schedule` (`parent_fees_schedule_id`),
  KEY `FK_fees_schedule_late_payment_for_fees_schedule` (`late_payment_for_fees_schedule`),
  CONSTRAINT `FK_fees_schedule_fees` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`),
  CONSTRAINT `FK_fees_schedule_fees_schedule` FOREIGN KEY (`parent_fees_schedule_id`) REFERENCES `fees_schedule` (`id`),
  CONSTRAINT `FK_fees_schedule_late_payment_for_fees_schedule` FOREIGN KEY (`late_payment_for_fees_schedule`) REFERENCES `fees_schedule` (`id`),
  CONSTRAINT `FK_fees_schedule_payments` FOREIGN KEY (`pyment_id`) REFERENCES `payments` (`id`),
  CONSTRAINT `FK_fees_schedule_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 PACK_KEYS=1;

  /* MASTER FEES SCHEDULE */


ALTER TABLE `qfix`.`payment_audit` MODIFY COLUMN `option_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'Payment Option',
 MODIFY COLUMN `mode_of_payment` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'Mode';

 
 
 
DROP TABLE IF EXISTS `qfix`.`app_version`;
CREATE TABLE  `qfix`.`app_version` (
  `name` varchar(10) NOT NULL,
  `version` varchar(10) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB ;



ALTER TABLE `qfix`.`student_late_fee_payment` MODIFY COLUMN `id` BIGINT(25) UNSIGNED NOT NULL AUTO_INCREMENT,
 DROP COLUMN `student_id`,
 DROP COLUMN `fees_id`,
 ADD COLUMN `fees_schedule_id` BIGINT(25) AFTER `discount_amount`,
 DROP FOREIGN KEY `fk_student_late_fee_payment_student_idx`,
 DROP FOREIGN KEY `student_late_fee_fees_idx`,
 ADD CONSTRAINT `FK_student_late_fee_payment_fees_schedule` FOREIGN KEY `FK_student_late_fee_payment_fees_schedule` (`fees_schedule_id`)
    REFERENCES `fees_schedule` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;



CREATE TABLE `payment`.`payment_verification_job` (
  `started_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finished_at` TIMESTAMP,
  PRIMARY KEY (`started_at`)
)
ENGINE = InnoDB;


DROP TABLE IF EXISTS `payment`.`payment_verification_status`;
CREATE TABLE  `payment`.`payment_verification_status` (
  `qfix_reference_number` varchar(20) NOT NULL,
  `payment_detail_id` int(11) NOT NULL,
  `original_payment_status` varchar(20) NOT NULL,
  `changed_payment_status` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_service_updated` char(1) DEFAULT NULL,
  `payment_service_updated` char(1) DEFAULT NULL,
  `retry_count` int(11) NOT NULL,
  PRIMARY KEY (`qfix_reference_number`),
  KEY `FK_payment_verification_status_payment_detail` (`payment_detail_id`),
  CONSTRAINT `FK_payment_verification_status_payment_detail` FOREIGN KEY (`payment_detail_id`) REFERENCES `payment_detail` (`id`),
  CONSTRAINT `FK_payment_verification_status_payment_detail_qfix_ref_number` FOREIGN KEY (`qfix_reference_number`) REFERENCES `payment_detail` (`qfix_reference_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




ALTER TABLE `qfix`.`payments` ADD COLUMN `fees_schedule_id` BIGINT(25) AFTER `paymentreference_details`,
 ADD CONSTRAINT `FK_payments_fees_schedule` FOREIGN KEY `FK_payments_fees_schedule` (`fees_schedule_id`)
    REFERENCES `fees_schedule` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

    
    DROP TABLE IF EXISTS `qfix`.`payment_transaction_status`;
DROP TABLE IF EXISTS `qfix`.`payment_transaction_acknowledgement_failures`;
CREATE TABLE  `qfix`.`payment_transaction_acknowledgement_failures` (
  `qfix_reference_number` varchar(20) NOT NULL,
  `payload` text,
  `entity_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_audit_id` int(11) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `fee_schedule_id` bigint(25) unsigned DEFAULT NULL,
  `fee_dues_obj` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`qfix_reference_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* 06-04-17 */
ALTER TABLE `payment`.`transaction_audit` ADD COLUMN `updated_by` VARCHAR(45) NOT NULL DEFAULT 'ONLINE' AFTER `updated_at`;
ALTER TABLE `payment`.`payment_detail` ADD COLUMN `updated_by` VARCHAR(45) NOT NULL DEFAULT 'ONLINE' AFTER `split_payment_detail`;
ALTER TABLE `payment`.`payment_detail` ADD COLUMN `updated_at` TIMESTAMP AFTER `updated_by`;
ALTER TABLE `qfix`.`payment_transaction_acknowledgement_failures` ADD COLUMN `error_stack` TEXT AFTER `created_date`;

ALTER TABLE `payment`.`payment_verification_status` MODIFY COLUMN `user_service_updated` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'N',
 MODIFY COLUMN `payment_service_updated` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'N';

ALTER TABLE `payment`.`payment_verification_job` ADD COLUMN `id` BIGINT(25) NOT NULL AFTER `finished_at`,
 DROP PRIMARY KEY,
 ADD PRIMARY KEY (`id`);
 
ALTER TABLE `payment`.`payment_verification_job` MODIFY COLUMN `id` BIGINT(25) NOT NULL AUTO_INCREMENT;

INSERT INTO `payment`.`payment_status` (`code`,`description`) VALUES
 ('ABORTED','Payment Aborted'),
 ('AWAITED','Payment Awaited'),
 ('FAILED','Payment Failed'),
 ('NOTFOUND','Payment Not found'),
 ('USER ABORTED','Payment USER ABORTED'),
 ('USER ABORTE','Payment USER ABORTE'),
 ('REFUND','Payment REFUND'),
 ('PENDING','Payment Pending');
 
 DROP TABLE IF EXISTS `payment`.`tpsl_transaction_acknowledgement`;
CREATE TABLE `payment`.`tpsl_transaction_acknowledgement` (
  `qfix_reference_number` varchar(20) NOT NULL,
  `tspl_reference_number` varchar(20) DEFAULT NULL,
  `response_payload` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_service_acknowledge_status` char(1) DEFAULT NULL,
  `payment_response_status` char(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `qfix`.`payment_transaction_acknowledgement_failures` DROP PRIMARY KEY;

ALTER TABLE `qfix`.`online_admission` ADD COLUMN `form_number` VARCHAR(20) AFTER `online_admission_course_id`;



/* 10-04-2017 */


DROP TABLE IF EXISTS `qfix`.`fees_late_payment_schedule`;
CREATE TABLE  `qfix`.`fees_late_payment_schedule` (
  `parent_fees_schedule_id` bigint(25) NOT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `FK_fees_late_payment_schedule_fees_schedule` (`parent_fees_schedule_id`),
  CONSTRAINT `FK_fees_late_payment_schedule_fees_schedule` FOREIGN KEY (`parent_fees_schedule_id`) REFERENCES `fees_schedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `qfix`.`parent` DROP INDEX `email_UNIQUE`;

ALTER TABLE `qfix`.`late_fees_payment_detail` ADD COLUMN `base_amount` DOUBLE AFTER `by_date`;



DROP TABLE IF EXISTS `qfix`.`fees_repeat_schedule`;
CREATE TABLE  `qfix`.`fees_repeat_schedule` (
  `fees_id` int(11) NOT NULL,
  `repeat_date` date NOT NULL,
  `is_scheduled` char(1) NOT NULL DEFAULT 'N',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `due_date` date NOT NULL,
  `reminder_start_date` date NOT NULL,
  `fee_for_duration` varchar(45) NOT NULL,
  KEY `FK_fees_repeat_schedule_fees` (`fees_id`),
  CONSTRAINT `FK_fees_repeat_schedule_fees` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


 /* 16-04-2017 */
CREATE  TABLE `qfix`.`user_invalid_login_audit` (

  `username` VARCHAR(100) NOT NULL ,

  `password` VARCHAR(45) NULL ,

  `device` VARCHAR(10) NULL );


  /* 19-04-2017 */
ALTER TABLE `qfix`.`branch_configuration` ADD COLUMN `offline_payments_enabled` CHAR(1) DEFAULT 'N' AFTER `create_parent_profile`;
ALTER TABLE `payment`.`payment_detail` ADD COLUMN `offline_payments_enabled` CHAR(1) NOT NULL DEFAULT 'N' AFTER `updated_at`;

CREATE TABLE `payment`.`payment_mode` (
  `code` VARCHAR(3) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`code`)
)
ENGINE = InnoDB;

insert into `payment`.`payment_mode` values
('O', 'Online'),
('C', 'Cash'),
('D', 'DD'),
('H', 'Cheque'),
('N', 'Neft/Rtgs');

ALTER TABLE `payment`.`payment_detail` ADD COLUMN `payment_mode` VARCHAR(3) NOT NULL DEFAULT 'O' AFTER `updated_at`;

  /* 25-05-2017 */
 ALTER TABLE `qfix`.`fees_repeat_schedule` 
CHANGE COLUMN `reminder_start_date` `reminder_start_date` DATE NULL ;

ALTER TABLE `qfix`.`fees` 
CHANGE COLUMN `frequency` `frequency` VARCHAR(45) NULL ;


/* 05-06-2017 */
ALTER TABLE `qfix`.`online_admission` ADD COLUMN `unique_identifier` VARCHAR(100) AFTER `form_number`;
/* 05-06-2017 */

/* 07-06-2017 */
ALTER TABLE `qfix`.`branch_configuration` ADD COLUMN `admission_form_search_enabled` CHAR(1) NOT NULL DEFAULT 'N' AFTER `offline_payments_enabled`,
 ADD COLUMN `admission_form_edit_enabled_for_student` CHAR(1) NOT NULL DEFAULT 'N' AFTER `admission_form_search_enabled`,
 ADD COLUMN `admission_form_payment_enabled` CHAR(1) NOT NULL DEFAULT 'Y' AFTER `admission_form_edit_enabled_for_student`;
 
ALTER TABLE `qfix`.`branch_configuration` ADD UNIQUE INDEX `Index_branch_id`(`branch_id`);
 /* 07-06-2017 */

ALTER TABLE `qfix`.`branch` MODIFY COLUMN `name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

DROP TABLE IF EXISTS `payment`.`initial_transaction_reponse`;
CREATE TABLE  `payment`.`initial_transaction_reponse` (
  `payment_detail_id` int(11) NOT NULL,
  `transaction_status_code` varchar(20) NOT NULL,
  `payment_gateway_response` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_detail_id`),
  CONSTRAINT `FK_initial_transaction_reponse_payment_detail` FOREIGN KEY (`payment_detail_id`) REFERENCES `payment_detail` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `payment`.`transaction_audit` ADD COLUMN `request_parameters` TEXT AFTER `updated_by`;


DROP TABLE IF EXISTS `qfix`.`online_admission_configuration`;
CREATE TABLE  `qfix`.`online_admission_configuration` (
  `branch_id` int(11) NOT NULL,
  `allow_to_specify_custom_admission_fees_amount` char(1) NOT NULL DEFAULT 'N',
  `admission_form_redirect_to_payment` char(1) NOT NULL DEFAULT 'N',
  `admission_form_search_enabled` char(1) NOT NULL DEFAULT 'N',
  `admission_form_edit_enabled_for_student` char(1) NOT NULL DEFAULT 'N',
  `admission_form_payment_enabled` char(1) NOT NULL,
  PRIMARY KEY (`branch_id`),
  CONSTRAINT `FK_online_admission_configuration_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `qfix`.`online_admission_course` ADD COLUMN `min_amount` DOUBLE AFTER `export_template_factory_name`,
 ADD COLUMN `max_amount` DOUBLE AFTER `min_amount`;

 
/* 20-06-2017 */
 DROP TABLE IF EXISTS `payment`.`transaction_error_code`;
CREATE TABLE  `payment`.`transaction_error_code` (
  `code` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 

insert into payment.transaction_error_code values('ERROR000','exception from jar');
insert into payment.transaction_error_code values('ERROR002','Wrong Request Type parameter');
insert into payment.transaction_error_code values('ERROR003','Invalid or Empty amount Parameter');
insert into payment.transaction_error_code values('ERROR005','Invalid or Empty Currency Parameter');
insert into payment.transaction_error_code values('ERROR006','Invalid or Empty ITC Parameter');
insert into payment.transaction_error_code values('ERROR008','Empty Request Type Parameter');
insert into payment.transaction_error_code values('ERROR009','Invalid or Empty Return Url Parameter');
insert into payment.transaction_error_code values('ERROR0101','card_details_not_found');
insert into payment.transaction_error_code values('ERROR022','Wrong or Empty Return S2S Url Parameter');
insert into payment.transaction_error_code values('ERROR023','Wrong or Empty Shopping cart Parameter');
insert into payment.transaction_error_code values('ERROR027','Wrong or Empty Shopping cart Parameter');
insert into payment.transaction_error_code values('ERROR033','Wrong or Empty TPSL Txn Id Parameter');
insert into payment.transaction_error_code values('ERROR035','Wrong or Empty TPSL Txn Date Parameter');
insert into payment.transaction_error_code values('ERROR036','Encryption Error In Response');
insert into payment.transaction_error_code values('ERROR037','DECRYPTION ERROR In Request');
insert into payment.transaction_error_code values('ERROR039','WRONG or Empty BANK CODE');
insert into payment.transaction_error_code values('ERROR061','Checksum Mismatch In Request');
insert into payment.transaction_error_code values('ERROR063','tilda(~) not found in end of the "msg" parameter');
insert into payment.transaction_error_code values('ERROR064','Checksum Mismatch In Response');
insert into payment.transaction_error_code values('ERROR065','Invalid Or Empty web-service locator');
insert into payment.transaction_error_code values('ERROR066','Invalid Or Empty TranId/MerchantTxnRef No Required');
insert into payment.transaction_error_code values('ERROR067','Blank Or Invalid Key');
insert into payment.transaction_error_code values('ERROR068','Blank Or Invalid IV');
insert into payment.transaction_error_code values('ERROR069','Blank Response From PG');
insert into payment.transaction_error_code values('ERROR070','Invalid or empty request kit version');
insert into payment.transaction_error_code values('ERROR071','Payload Insertion Fail');
insert into payment.transaction_error_code values('ERROR072','Invalid or empty Merchant Txn ref No');
insert into payment.transaction_error_code values('ERROR073','ERROR: Incorrect token response format');
insert into payment.transaction_error_code values('ERROR074','Invalid or empty card id');
insert into payment.transaction_error_code values('ERROR075','Invalid or empty cust id');
insert into payment.transaction_error_code values('ERROR077','ERROR: Empty Property Path Parameter');
insert into payment.transaction_error_code values('ERROR078','ERROR: PG Response HASH does not match with Response');
insert into payment.transaction_error_code values('ERROR079','ERROR: PG Response Does not have HASH Value');
insert into payment.transaction_error_code values('ERROR080','ERROR: PG Response Decryption Fail');
insert into payment.transaction_error_code values('ERROR081','empty card name');
insert into payment.transaction_error_code values('ERROR082','empty card no');
insert into payment.transaction_error_code values('ERROR083','Invalid or empty card exp month');
insert into payment.transaction_error_code values('ERROR084','Invalid or empty card exp year');
insert into payment.transaction_error_code values('ERROR085','Error while communicating with bank');
insert into payment.transaction_error_code values('ERROR086','Error while updating payload');
insert into payment.transaction_error_code values('ERROR087','Selected Bank is not moto type');
insert into payment.transaction_error_code values('ERROR088','Invalid Bank Selection for Merchant');
insert into payment.transaction_error_code values('ERROR089','This_Card_Is_Not_Allowed_For_This_Transaction.');
insert into payment.transaction_error_code values('ERROR090','Invalid card CVV');
insert into payment.transaction_error_code values('ERROR091','web_service_request_time_out');
insert into payment.transaction_error_code values('ERROR300','Refund successfully submitted');
insert into payment.transaction_error_code values('ERROR301','Merchant Code Mismatch In Refund');
insert into payment.transaction_error_code values('ERROR302','SRC Amount Mismatch In Refund');
insert into payment.transaction_error_code values('ERROR303','Refund Not Allowed For This Transaction');
insert into payment.transaction_error_code values('ERROR304','REFUND NOT ENABLED FOR MERCHANT');
insert into payment.transaction_error_code values('ERROR305','TRANSACTION DATE EXCEED From 90 DAYS In Refund');
insert into payment.transaction_error_code values('ERROR306','REFUND AMOUNT CANNOT BE GREATER THAN BALANCE AMOUNT');
insert into payment.transaction_error_code values('ERROR307','Requsted Merchant Amt Is Null In Refund');
insert into payment.transaction_error_code values('ERROR308','Requested Refund Amt Is Null In Refund');
insert into payment.transaction_error_code values('ERROR309','Invalid Transaction ID');
insert into payment.transaction_error_code values('TPME001','Error while trying to generate checksum from request parameter ');
insert into payment.transaction_error_code values('TPME002','One of the six mandatory parameters is blank. ');
insert into payment.transaction_error_code values('TPME003','Improper property file data ');
insert into payment.transaction_error_code values('TPME004','Error while trying to generate checksum from response parameter ');
insert into payment.transaction_error_code values('TPME005','Invalid Mandatory parameters for generating checksum');
insert into payment.transaction_error_code values('TPME006','Invalid number of tokens in msg parameter ');
insert into payment.transaction_error_code values('TPME007','Error while generating checksum');
insert into payment.transaction_error_code values('TPME008','Invalid Checksum key ');
insert into payment.transaction_error_code values('TPME009','CheckSum mismatch during response from techprocess ');
insert into payment.transaction_error_code values('TPPGE001','Invalid Payment Gateway Host Name ');
insert into payment.transaction_error_code values('TPPGE002','Merchant Code is mandatory');
insert into payment.transaction_error_code values('TPPGE003','Invalid Referrer URL');
insert into payment.transaction_error_code values('TPPGE004','Invalid Referrer URL provided by merchant ');
insert into payment.transaction_error_code values('TPPGE005','Merchant Transaction Amount is mandatory');
insert into payment.transaction_error_code values('TPPGE006','Bank ID is mandatory ');
insert into payment.transaction_error_code values('TPPGE007','Currency ismandatory');
insert into payment.transaction_error_code values('TPPGE008','Merchant`s currency should be INR ');
insert into payment.transaction_error_code values('TPPGE009','Invalid Item Code ');
insert into payment.transaction_error_code values('TPPGE010','Merchant Payment Reference Number is mandatory ');
insert into payment.transaction_error_code values('TPPGE011','Invalid Merchant Payment Reference Number ');
insert into payment.transaction_error_code values('TPPGE012','Merchant CST Login is mandatory ');
insert into payment.transaction_error_code values('TPPGE013','Invalid Customer Name');
insert into payment.transaction_error_code values('TPPGE014','Invalid E-Mail ID ');
insert into payment.transaction_error_code values('TPPGE015','Invalid Phone Number ');
insert into payment.transaction_error_code values('TPPGE016','Invalid Address');
insert into payment.transaction_error_code values('TPPGE017','Invalid City');
insert into payment.transaction_error_code values('TPPGE018','Invalid Pin Code');
insert into payment.transaction_error_code values('TPPGE019','Invalid Country ');
insert into payment.transaction_error_code values('TPPGE020','Customer Name is mandatory ');
insert into payment.transaction_error_code values('TPPGE021','Customer Name should be alphanumeric');
insert into payment.transaction_error_code values('TPPGE022','E-Mail ID is mandatory ');
insert into payment.transaction_error_code values('TPPGE023','Invalid E-Mail ID Format ');
insert into payment.transaction_error_code values('TPPGE024','Contact Number is mandatory ');
insert into payment.transaction_error_code values('TPPGE025','Contact Number`s Format is invalid');
insert into payment.transaction_error_code values('TPPGE026','Address is mandatory');
insert into payment.transaction_error_code values('TPPGE027','Address should be alphanumeric');
insert into payment.transaction_error_code values('TPPGE028','City is mandatory');
insert into payment.transaction_error_code values('TPPGE029','City should be alphanumeric');
insert into payment.transaction_error_code values('TPPGE030','Pincodeis mandatory');
insert into payment.transaction_error_code values('TPPGE031','Pincode should be numeric ');
insert into payment.transaction_error_code values('TPPGE032','Stateis mandatory ');
insert into payment.transaction_error_code values('TPPGE033','State should be alphanumeric ');
insert into payment.transaction_error_code values('TPPGE034','Countryis mandatory ');
insert into payment.transaction_error_code values('TPPGE035','Country should be alphanumeric ');
insert into payment.transaction_error_code values('TPPGE036','Product Code 1 is mandatory');
insert into payment.transaction_error_code values('TPPGE037','Product Code 2 is mandatory');
insert into payment.transaction_error_code values('TPPGE038','Product Code 3 is mandatory');
insert into payment.transaction_error_code values('TPPGE039','Product Code 4 is mandatory');
insert into payment.transaction_error_code values('TPPGE040','Product Code 5 is mandatory');
insert into payment.transaction_error_code values('TPPGE041','Product Code 6 is mandatory');
insert into payment.transaction_error_code values('TPPGE042','Product Code 7 is mandatory');
insert into payment.transaction_error_code values('TPPGE043','Product Code 8 is mandatory');
insert into payment.transaction_error_code values('TPPGE044','Product Code 9 is mandatory');
insert into payment.transaction_error_code values('TPPGE045','Product Code 10 is mandatory ');
insert into payment.transaction_error_code values('TPPGE046','Address is mandatory');
insert into payment.transaction_error_code values('TPPGE047','Mobile is mandatory ');
insert into payment.transaction_error_code values('TPPGE048','UDF1 is mandatory ');
insert into payment.transaction_error_code values('TPPGE049','UDF2 is mandatory ');
insert into payment.transaction_error_code values('TPPGE050','UDF3 is mandatory ');
insert into payment.transaction_error_code values('TPPGE051','Checksum is mandatory ');
insert into payment.transaction_error_code values('TPPGE052','Product Code 1 format is invalid');
insert into payment.transaction_error_code values('TPPGE053','Product Code 2 format is invalid');
insert into payment.transaction_error_code values('TPPGE054','Product Code 3 format is invalid');
insert into payment.transaction_error_code values('TPPGE055','Product Code 4 format is invalid');
insert into payment.transaction_error_code values('TPPGE056','Product Code 5 format is invalid');
insert into payment.transaction_error_code values('TPPGE057','Product Code 6 format is invalid');
insert into payment.transaction_error_code values('TPPGE058','Product Code 7 format is invalid');
insert into payment.transaction_error_code values('TPPGE059','Product Code 8 format is invalid');
insert into payment.transaction_error_code values('TPPGE060','Product Code 9 format is invalid');
insert into payment.transaction_error_code values('TPPGE061','Product Code 10 format is invalid');
insert into payment.transaction_error_code values('TPPGE062','UDF 1 format is invalid');
insert into payment.transaction_error_code values('TPPGE063','UDF 2 format is invalid');
insert into payment.transaction_error_code values('TPPGE064','UDF 3 format is invalid');
insert into payment.transaction_error_code values('TPPGE065','Checksum format is invalid');
insert into payment.transaction_error_code values('TPPGE066','Sum of products amount and commision amount does not match with total amount');
insert into payment.transaction_error_code values('TPPGE067','Invalid State');
insert into payment.transaction_error_code values('TPPGE068','Invalid Amount provided by Submerchant , Amount should be Greater than the Minimum transaction Amount');
insert into payment.transaction_error_code values('TPPGE069','Invalid Amount provided by Submerchant , Amount should not be Greater than the Maximum transaction Amount');
insert into payment.transaction_error_code values('TPPGE070','Test or Live Flag not configured');
insert into payment.transaction_error_code values('TPPGE071','Bank selection for the merchant is not configured properly');
insert into payment.transaction_error_code values('TPPGE072','Product Codes are invalid');
insert into payment.transaction_error_code values('TPPGE073','Invalid position of special chatacter in ITC parameter');
insert into payment.transaction_error_code values('TPPGE074','Invalid Scheme Code passes in ITC parameter');
insert into payment.transaction_error_code values('TPPGE075','Tariff not defined for merchant for payment code');
insert into payment.transaction_error_code values('TPPGE076','Tariff not defined for bank for payment code');
insert into payment.transaction_error_code values('TPPGE077','Tariff not defined for merchant');
insert into payment.transaction_error_code values('TPPGE078','Tariff not defined for bank');
insert into payment.transaction_error_code values('TPPGE079','Invalid merchant received due to improper merchant configuration');
insert into payment.transaction_error_code values('TPPGE080','Invalid bank details received due to improper bank configuration');
insert into payment.transaction_error_code values('TPPGE081','Error due to merchant code validation in payment gateway common');
insert into payment.transaction_error_code values('TPPGE082','Error while encrypting Transaction ID');
insert into payment.transaction_error_code values('TPPGE083','Unable to gather Transaction Details');
insert into payment.transaction_error_code values('TPPGE084','Unable to create Unique ID for merchant');
insert into payment.transaction_error_code values('TPPGE085','Unable to get Cheksum Key');
insert into payment.transaction_error_code values('TPPGE086','Biller ID is empty');
insert into payment.transaction_error_code values('TPPGE087','Tata Sky Subscriber ID is mandatory');
insert into payment.transaction_error_code values('TPPGE088','Return URL ismandatory');
insert into payment.transaction_error_code values('TPPGE089','Invalid Additional Info 1');
insert into payment.transaction_error_code values('TPPGE090','Invalid Additional Info 2');
insert into payment.transaction_error_code values('TPPGE091','Invalid Additional Info 3');
insert into payment.transaction_error_code values('TPPGE092','Checksum Value is mandatory');
insert into payment.transaction_error_code values('TPPGE093','Invalid Merchant ID');
insert into payment.transaction_error_code values('TPPGE094','Error while generating checksum');
insert into payment.transaction_error_code values('TPPGE095','Merchant Checksum mismatch ');
insert into payment.transaction_error_code values('TPPGE096','Error while generating Citibank checksum');
insert into payment.transaction_error_code values('TPPGE097','Error while encoding Icicibankdata ');
insert into payment.transaction_error_code values('TPPGE098','Error while encrypting parameters for HDFC Bank');
insert into payment.transaction_error_code values('TPPGE099','Error while reading configuration files for HDFC CC');
insert into payment.transaction_error_code values('TPPGE100','Error while reading configuration files for IOB Debit Card ');
insert into payment.transaction_error_code values('TPPGE101','Error while reading IOB Debit Card API');
insert into payment.transaction_error_code values('TPPGE102','Merchant Configuration issue for Allahabad Bank');
insert into payment.transaction_error_code values('TPPGE103','Error while forwarding request to Bank');
insert into payment.transaction_error_code values('TPPGE104','Error while reading verification status ');
insert into payment.transaction_error_code values('TPPGE105','Error while validating bank response');
insert into payment.transaction_error_code values('TPPGE106','Error while reading verification timeout');
insert into payment.transaction_error_code values('TPPGE107','Bank verification URL not configured ');
insert into payment.transaction_error_code values('TPPGE108','Error while verifying the transactions with bank ');
insert into payment.transaction_error_code values('TPPGE109','Error while inserting multiple response from bank');
insert into payment.transaction_error_code values('TPPGE110','Error while updating duplicate bank transaction id ');
insert into payment.transaction_error_code values('TPPGE111','Error while updating transaction response details from bank');
insert into payment.transaction_error_code values('TPPGE112','Error while updating account details received from bank ');
insert into payment.transaction_error_code values('TPPGE113','Transaction failed due to improper parameters from bank');
insert into payment.transaction_error_code values('TPPGE114','Error while processing MID for HDFC Bank');
insert into payment.transaction_error_code values('TPPGE115','Error while retriving transaction date. ');
insert into payment.transaction_error_code values('TPPGE116','Error while processing MID for HDFC Netbanking ');
insert into payment.transaction_error_code values('TPPGE117','Normal Failed Transaction due to Invalid Bank Tran ID');
insert into payment.transaction_error_code values('TPPGE118','Failed Transaction due to Duplicate Bank Transaction ID ');
insert into payment.transaction_error_code values('TPPGE119','Failed Transaction due to Amount Mismatch');
insert into payment.transaction_error_code values('TPPGE120','Transaction declined by the bank (Normal)');
insert into payment.transaction_error_code values('TPPGE121','Error while updating transaction response while forwarding to merchant (merchant) ');
insert into payment.transaction_error_code values('TPPGE122','Transaction failed due to invalid bank transaction id (suspicious tran) ');
insert into payment.transaction_error_code values('TPPGE123',' Connection timed out ');
insert into payment.transaction_error_code values('TPPGE124','Error while processing account details ');
insert into payment.transaction_error_code values('TPPGE125','Error occured during transaction verivication with bank ');
insert into payment.transaction_error_code values('TPPGE126','Improper bank verification response');
insert into payment.transaction_error_code values('TPPGE127','Error while updating bank information during verification ');
insert into payment.transaction_error_code values('TPPGE128','Error while insertingststus query response during verification ');
insert into payment.transaction_error_code values('TPPGE129','Error due to improper response from bank');
insert into payment.transaction_error_code values('TPPGE130','Error while inserting citibank details ');
insert into payment.transaction_error_code values('TPPGE131','Error while capturing parameters for forwarding to bank ');
insert into payment.transaction_error_code values('TPPGE132','Error while requesting or receiving verification process ');
insert into payment.transaction_error_code values('TPPGE133','Failed due to verification retry has reached maximum limit ');
insert into payment.transaction_error_code values('TPPGE134','Error due to invalid scheme code during shopping cart transaction ');
insert into payment.transaction_error_code values('TPPGE135','Error due to improper tariff amount during shopping cart transaction ');
insert into payment.transaction_error_code values('TPPGE136','Error due to sum of product`s amount is different than total transaction amount');
insert into payment.transaction_error_code values('TPPGE137','Error due to unspecified tariff calculation for the bank');
insert into payment.transaction_error_code values('TPPGE138','Error due to tariff not defined for the bank for the given range ');
insert into payment.transaction_error_code values('TPPGE139','Configuration issue of CitiBank ');
insert into payment.transaction_error_code values('TPPGE140','Configuration issue of CitiBank CC');
insert into payment.transaction_error_code values('TPPGE141','Error while reading HDFC CC API ');
insert into payment.transaction_error_code values('TPPGE142','Error while processing account details for IDBI Bank ');
insert into payment.transaction_error_code values('TPPGE143','Configuration issue for Indian Bank');
insert into payment.transaction_error_code values('TPPGE144','Error while decoding Icicibank data');
insert into payment.transaction_error_code values('TPPGE145','Error while reading configuration files for Standard Chartered Bank ');
insert into payment.transaction_error_code values('TPPGE146','Error while loading merchant`s customized page');
insert into payment.transaction_error_code values('TPPGE147','Error while retriving bank details for the merchant');
insert into payment.transaction_error_code values('TPPGE148','Error while cancelling this transaction ');
insert into payment.transaction_error_code values('TPPGE149','Error while generating Transaction ID due to configuration issues ');
insert into payment.transaction_error_code values('TPPGE150','Error while capturing details of the transaction');
insert into payment.transaction_error_code values('TPPGE151','Error while inserting customer details ');
insert into payment.transaction_error_code values('TPPGE152','Error due to transaction amount is less than tariff amount');
insert into payment.transaction_error_code values('TPPGE153','Error due to unspecified tariff calculation for the merchant');
insert into payment.transaction_error_code values('TPPGE154','Error due to tariff not defined for the merchant for the given range ');
insert into payment.transaction_error_code values('TPPGE155','Error due to merchant tariff amount is less than bank tariff amount');
insert into payment.transaction_error_code values('TPPGE156','Error due to merchant tariff amount is less than the bank tariff amount (percentage - percentage)');
insert into payment.transaction_error_code values('TPPGE157','Error due to the merchant tariff amount is less than bank tariff amount (fixed - shared) ');
insert into payment.transaction_error_code values('TPPGE158','Error due to the merchant tariff amount is less than the bank tariff amount (percentage - shared)');
insert into payment.transaction_error_code values('TPPGE159','Error occured due to the merchant tariff amount is less than bank tariff amount(fixed - percentage fixed - percentage) ');
insert into payment.transaction_error_code values('TPPGE160','Error while reading verification retry count ');
insert into payment.transaction_error_code values('TPPGE161','Error due to Product Code 1 is not configured');
insert into payment.transaction_error_code values('TPPGE162','Error due to Product Code 2 is not configured');
insert into payment.transaction_error_code values('TPPGE163','Error due to Product Code 3 is not configured');
insert into payment.transaction_error_code values('TPPGE164','Error due to Product Code 4 is not configured');
insert into payment.transaction_error_code values('TPPGE165','Error due to Product Code 5 is not configured');
insert into payment.transaction_error_code values('TPPGE166','Error due to Product Code 6 is not configured');
insert into payment.transaction_error_code values('TPPGE167','Error due to Product Code 7 is not configured');
insert into payment.transaction_error_code values('TPPGE168','Error due to Product Code 8 is not configured');
insert into payment.transaction_error_code values('TPPGE169','Error due to Product Code 9 is not configured');
insert into payment.transaction_error_code values('TPPGE170','Error due to Product Code 10 is not configured');
insert into payment.transaction_error_code values('TPPGE171','Error while updating verification retry count ');
insert into payment.transaction_error_code values('TPPGE172','Error while creating XML to be sent to the merchant`s server');
insert into payment.transaction_error_code values('TPPGE173','Error while generating Hask Key for IDBI CC');
insert into payment.transaction_error_code values('TPPGE174','Error while generating Hask Key for IDBI CC Response');
insert into payment.transaction_error_code values('TPPGE175','Error due to invalid Hask Key for IDBI CC');
insert into payment.transaction_error_code values('TPPGE176','Error due to Payment Cycle not configured for the merchant');
insert into payment.transaction_error_code values('TPPGE177','Error while encrypting data for SBI');
insert into payment.transaction_error_code values('TPPGE178','Error while decrypting data for SBI');
insert into payment.transaction_error_code values('TPPGE179','Error while generating checksum for SBI');
insert into payment.transaction_error_code values('TPPGE180','Error while generating checksum for indian bank');
insert into payment.transaction_error_code values('TPPGE181','Transaction failed due to checksum mismatch for indian bank');
insert into payment.transaction_error_code values('TPPGE182','Error due to invalid verification response from Indian Bank');
insert into payment.transaction_error_code values('TPPGE183','Normal Failed IDBI CC Transaction due to Invalid Hashkey');
insert into payment.transaction_error_code values('TPPGE184','Error while updating Account Number for ICICI Broking');
insert into payment.transaction_error_code values('TPPGE185','Error while reading Key from property file for DCB');
insert into payment.transaction_error_code values('TPPGE186','Error while encrypting data for DCB');
insert into payment.transaction_error_code values('TPPGE187','Error while updating IOB Bank information INTO the system');
insert into payment.transaction_error_code values('TPPGE188','Error due to Invalid Bank Transaction Id for Oriental Bank');
insert into payment.transaction_error_code values('TPPGE189','Error Due to checksum mismatch ');
insert into payment.transaction_error_code values('TPPGE190','Invalid Account Number passed by merchant');
insert into payment.transaction_error_code values('TPPGE191','Error while reading Property Path during Transaction.');
insert into payment.transaction_error_code values('TPPGE192','Error while generatin Checksum during Transaction.');
insert into payment.transaction_error_code values('TPPGE193','Error While spliting Verification output of BOI');
insert into payment.transaction_error_code values('TPPGE194','Error while trying to generate IVR Txn ID');
insert into payment.transaction_error_code values('TPPGE195','Error while trying to generate Message Hash');
insert into payment.transaction_error_code values('TPPGE196','Error while parsing the response parameter sent from bank');
insert into payment.transaction_error_code values('TPPGE197','Invalid parameters received');
insert into payment.transaction_error_code values('TPPGE198','Error While Updating OTP Response');
insert into payment.transaction_error_code values('TPPGE199','Error due to Invalid Session');
insert into payment.transaction_error_code values('TPPGE200','Error While Updating OTP Authentication Response');
insert into payment.transaction_error_code values('TPPGE201','OTP Authentication Failed');
insert into payment.transaction_error_code values('TPPGE202','Message Hash mismatch');
insert into payment.transaction_error_code values('TPPGE203','Data in incorrect format');
insert into payment.transaction_error_code values('TPPGE204','Required in put are not provided.');
insert into payment.transaction_error_code values('TPPGE205','Invalid merchant ID.');
insert into payment.transaction_error_code values('TPPGE206','System can`t authorize the transaction.');
insert into payment.transaction_error_code values('TPPGE207','Payment gateway not available.');
insert into payment.transaction_error_code values('TPPGE208','Invalid OPT entered');
insert into payment.transaction_error_code values('TPPGE209','Transaction decline.');
insert into payment.transaction_error_code values('TPPGE210','Invalid Number of Verification Parameters.');
insert into payment.transaction_error_code values('TPPGE211','Error due to SSL Handshake Exception');
insert into payment.transaction_error_code values('TPPGE212','Transaction Cancel on Transaction Charges Page.');
insert into payment.transaction_error_code values('TPPGE213','Exception while reteriving Transaction Charges from Database.');
insert into payment.transaction_error_code values('TPPGE214','Transaction cancelled by user');
insert into payment.transaction_error_code values('TPPGE215','Error while reading images from properties file');
insert into payment.transaction_error_code values('TPPGE216','Transaction could not be completed due to bank configuration.');
insert into payment.transaction_error_code values('TPPGE217','Invalid Type Of Integration for Merchant.');
insert into payment.transaction_error_code values('TPPGE218','Error while posting Data to ICICI Moto');
insert into payment.transaction_error_code values('TPPGE219','Error Due to Invalid Request Parameter, while Card Registraion');
insert into payment.transaction_error_code values('TPPGE220','Error Due to Invalid Card Id from Registered Moto Merchant');
insert into payment.transaction_error_code values('TPPGE221','This Card is not allowed for transactions.');
insert into payment.transaction_error_code values('TPPGE222','Transaction Failed Due To Amount Mismatch In Capture Leg.');
insert into payment.transaction_error_code values('TPPGE223','Transaction Failed In Capture Leg Due To Execption.');
insert into payment.transaction_error_code values('TPPGE224','Transaction Failed In MODE 3B Leg - 3DS.');
insert into payment.transaction_error_code values('TPPGE225','Transaction Failed Due To Security Hash Not Match.');
insert into payment.transaction_error_code values('TPPGE226','Merchant Disabled');
insert into payment.transaction_error_code values('TPPGE227','Merchant Disabled');
insert into payment.transaction_error_code values('TPPGE500','Authentication failed.Transaction cannot be authorized');
insert into payment.transaction_error_code values('TPPGE501','Card Bin Stop Listed');
insert into payment.transaction_error_code values('TPPGE502','Card Number Invalid');
insert into payment.transaction_error_code values('TPPGE503','Invalid TRANSACTION');
insert into payment.transaction_error_code values('TPPGE504','Not sufficient fund');
insert into payment.transaction_error_code values('TPPGE505','Rejected By Acquirer');
insert into payment.transaction_error_code values('TPPGE506','Unable to authorize');
insert into payment.transaction_error_code values('TPPGE507','Unauthorized usage');
insert into payment.transaction_error_code values('TPPGE508','Card Expired');
insert into payment.transaction_error_code values('TPPGE509','Card Type Invalid');
insert into payment.transaction_error_code values('TPPGE510','Expired card');
insert into payment.transaction_error_code values('TPPGE511','format error');
insert into payment.transaction_error_code values('TPPGE512','Internal Processing Error');
insert into payment.transaction_error_code values('TPPGE513','invalid card number');
insert into payment.transaction_error_code values('TPPGE514','tran not permitted');
insert into payment.transaction_error_code values('TPPGE515','CAF status 0 or 9');
insert into payment.transaction_error_code values('TPPGE601','Invalid Request');
insert into payment.transaction_error_code values('TPPGE602','Invalid Parameters');
insert into payment.transaction_error_code values('TPPGE603','Invalid Merchant Code');
insert into payment.transaction_error_code values('TPPGE604','Invalid Investor Id');
insert into payment.transaction_error_code values('TPPGE605','Invalid Alias Name');
insert into payment.transaction_error_code values('TPPGE606','Invalid Card No');
insert into payment.transaction_error_code values('TPPGE607','Invalid Card Holder Name');
insert into payment.transaction_error_code values('TPPGE608','Invalid Bank Name');
insert into payment.transaction_error_code values('TPPGE609','Invalid Card Type');
insert into payment.transaction_error_code values('TPPGE610','Invalid Expiry Month');
insert into payment.transaction_error_code values('TPPGE611','Invalid Expiry Year');
insert into payment.transaction_error_code values('TPPGE612','Invalid Street One');
insert into payment.transaction_error_code values('TPPGE613','Invalid Street Two');
insert into payment.transaction_error_code values('TPPGE614','Invalid City');
insert into payment.transaction_error_code values('TPPGE615','Invalid State');
insert into payment.transaction_error_code values('TPPGE616','Invalid Pin Code');
insert into payment.transaction_error_code values('TPPGE617','Invalid Combination');
insert into payment.transaction_error_code values('TPPGE618','Invalid Dedup Status');
insert into payment.transaction_error_code values('TPPGE619','Bin value not Exist: Card is not valid');
insert into payment.transaction_error_code values('TPPGE620','Error While Checking Expiry Month & Expiry Year');
insert into payment.transaction_error_code values('TPPGE621','Technical Error');
insert into payment.transaction_error_code values('TPPGE622','Transaction failed due to data temperd');
insert into payment.transaction_error_code values('TPPGE900','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE901','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE902','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE903','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE904','Technical Error');
insert into payment.transaction_error_code values('TPPGE905','Invalid credit Card Details');
insert into payment.transaction_error_code values('TPPGE906','Wrong 3D secure credential');
insert into payment.transaction_error_code values('TPPGE907','Insufficient Fund');
insert into payment.transaction_error_code values('TPPGE908','Declined by issuer');
insert into payment.transaction_error_code values('TPPGE909','Stolen/Expired/Lost/Invalid Card');
insert into payment.transaction_error_code values('TPPGE910','Unsupported card type');
insert into payment.transaction_error_code values('TPPGE911','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE912','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE913','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE914','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE915','Cancel by user in OTP/3DSecure capture Page');
insert into payment.transaction_error_code values('TPPGE916','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE917','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE918','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE919','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE920','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE921','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE922','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE923','Technical Error');
insert into payment.transaction_error_code values('TPPGE924','Technical Error');
insert into payment.transaction_error_code values('TPPGE925','Invalid credit Card Details');
insert into payment.transaction_error_code values('TPPGE926','Invalid credit Card Details');
insert into payment.transaction_error_code values('TPPGE927','Invalid credit Card Details');
insert into payment.transaction_error_code values('TPPGE928','Invalid credit Card Details');
insert into payment.transaction_error_code values('TPPGE929','Wrong 3D secure credential');
insert into payment.transaction_error_code values('TPPGE930','Insufficient Fund');
insert into payment.transaction_error_code values('TPPGE931','Insufficient Fund');
insert into payment.transaction_error_code values('TPPGE932','Declined by issuer');
insert into payment.transaction_error_code values('TPPGE933','Declined by issuer');
insert into payment.transaction_error_code values('TPPGE934','Declined by issuer');
insert into payment.transaction_error_code values('TPPGE935','Declined by issuer');
insert into payment.transaction_error_code values('TPPGE936','Declined by issuer');
insert into payment.transaction_error_code values('TPPGE937','Stolen/Expired/Lost/Invalid Card');
insert into payment.transaction_error_code values('TPPGE938','Stolen/Expired/Lost/Invalid Card');
insert into payment.transaction_error_code values('TPPGE939','Stolen/Expired/Lost/Invalid Card');
insert into payment.transaction_error_code values('TPPGE940','Stolen/Expired/Lost/Invalid Card');
insert into payment.transaction_error_code values('TPPGE941','Unsupported card type');
insert into payment.transaction_error_code values('TPPGE942','Technical Error');
insert into payment.transaction_error_code values('TPPGE943','Technical Error');
insert into payment.transaction_error_code values('TPPGE944','Technical Error');
insert into payment.transaction_error_code values('TPPGE945','Technical Error');
insert into payment.transaction_error_code values('TPPGE946','Card Gateway Failure');
insert into payment.transaction_error_code values('TPPGE947','Session expired.Transaction cannot be processed');
insert into payment.transaction_error_code values('TPPGE948','Technical Error');
insert into payment.transaction_error_code values('TPPGE949','Technical Error');
insert into payment.transaction_error_code values('TPPGE950','Failed Transaction due to Duplicate Bank Transaction ID');
insert into payment.transaction_error_code values('TPPGE951','Failed Transaction due to Amount Mismatch');
insert into payment.transaction_error_code values('TPPGE952','Technical Error');
insert into payment.transaction_error_code values('TPPGE953','Technical Error');
insert into payment.transaction_error_code values('TPPGE954','Transaction already processed');
insert into payment.transaction_error_code values('TPPGE955','Back/Refresh Clicked');
insert into payment.transaction_error_code values('TPPGE956','GV00004-PARes status not successful');
insert into payment.transaction_error_code values('TPPGE957','Declined by issuer');
insert into payment.transaction_error_code values('ERROR092','Web service execution error from SingleURL kit');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_001','CONNECTION TIME OUT');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_002','SERVER DOWN');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_003','DEFAULT ERROR');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_004','INVALID URL');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_005','SSL ERROR');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_006','UNKNOWN ERROR');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_007','FILE NOT FOUND');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_008','INVALID JSON FORMAT');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_009','WEBVIEW ERROR');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_010','SOCKET TIME OUT');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_011','CHECKOUT VALIDATION CODE');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_012','REQUEST PAYMENT MODE CODE');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_013','SI NOT ENABLED CODE');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_014','BANK CODE NOT SUPPORTED CODE');
insert into payment.transaction_error_code values('ERROR_PAYNIMO_100','NETWORK ERROR CODE');

insert into payment.transaction_error_code values('0392', 'Cancelled BY User');
insert into payment.transaction_error_code values('0396', 'Transaction response not received from Bank, Status Check on same Day');
insert into payment.transaction_error_code values('0397', 'Transaction Response not received from Bank. Status Check on next Day');
insert into payment.transaction_error_code values('0398', 'Corporate Transaction Initiated');
insert into payment.transaction_error_code values('0399', 'Failed Resposne received from bank');
insert into payment.transaction_error_code values('0400', 'Refund Initiated Successfully');
insert into payment.transaction_error_code values('0401', 'Refund in Progress (Currently not in used)');
insert into payment.transaction_error_code values('0402', 'Instant Refund Initiated Successfully(Currently not in used)');
insert into payment.transaction_error_code values('0499', 'Refund initiation failed');
insert into payment.transaction_error_code values('9999', 'Transaction not found in PG');

 ALTER TABLE `payment`.`transaction_audit` ADD COLUMN `transaction_error_code` VARCHAR(100) AFTER `request_parameters`,
 ADD CONSTRAINT `FK_transaction_audit_transaction_error_code` FOREIGN KEY `FK_transaction_audit_transaction_error_code` (`transaction_error_code`)
    REFERENCES `transaction_error_code` (`code`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


insert into `qfix`.`menu_items` values (45, 'Acknowledge Offline Payments',  null, 'offline.list', 30);
insert into `qfix`.`menu_permission` values (149, 45, 'list', 'offline.list', null);
insert into `role_menu` values (1, 45), (6, 45);
insert into `role_menu_permission` values (1, 149), (6, 149);

ALTER TABLE `qfix`.`payment_audit` ADD COLUMN `payment_mode` CHAR(1) NOT NULL DEFAULT 'O' AFTER `mode_of_payment`;
ALTER TABLE `qfix`.`payment_audit` ADD COLUMN `updated_by` INTEGER AFTER `payment_mode`,
 ADD COLUMN `updated_at` TIMESTAMP AFTER `updated_by`,
 ADD CONSTRAINT `FK_payment_audit_user` FOREIGN KEY `FK_payment_audit_user` (`updated_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
ALTER TABLE `qfix`.`payment_audit` ADD COLUMN `updated_by_process` VARCHAR(45) NOT NULL DEFAULT 'ONLINE' AFTER `updated_at`;

insert into user (username, active, authentication_type, temp_password, password, password_salt, device, gcm_id, institute_id, isDelete) 
values ('QF!X-Schedu1er@dm!n','Y','APPLICATION','QF!X-Schedu!er@dm!n@9001',0x649A192FA2E6CD7C665038F9125D1E2D08230074,0x98435ED265EAEB2C,NULL,NULL,NULL,'N'),
('QF!X-Payment#1spatcher@dm!n','Y','APPLICATION','QF!X-Payment#!spatcher@dm!n@8090',0x748D6FB5C60A33A5217730BE55C2218A88E7832F,0x332C333E25423F4B,NULL,NULL,NULL,'N');

insert into user_role values( 169000 , 1), values( 168999, 1);

ALTER TABLE `qfix`.`user` ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `isDelete`;



/* 30-06-2017*/
-- New  changes starts from here --
/* ALTER TABLE `qfix`.`branch_display_layout` RENAME TO `qfix`.`display_head`,
 CHANGE COLUMN `fees_description` `name` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
 CHANGE COLUMN `other_description` `description` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 ADD COLUMN `active` CHAR(1) NOT NULL DEFAULT 'Y' AFTER `is_delete`;
 
ALTER TABLE `qfix`.`display_head` DROP COLUMN `fees_amount`;
*/


DROP TABLE IF EXISTS `qfix`.`display_head`;
CREATE TABLE  `qfix`.`display_head` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `FK_branch_display_layout_branch` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `qfix`.`display_template`;
CREATE TABLE  `qfix`.`display_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_display_template_branch` (`branch_id`),
  CONSTRAINT `FK_display_template_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `qfix`.`display_template_head`;
CREATE TABLE  `qfix`.`display_template_head` (
  `display_head_id` int(11) NOT NULL AUTO_INCREMENT,
  `display_template_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`display_head_id`,`display_template_id`),
  KEY `FK_display_template_head_display_template` (`display_template_id`),
  CONSTRAINT `FK_display_template_head_display_head` FOREIGN KEY (`display_head_id`) REFERENCES `display_head` (`id`),
  CONSTRAINT `FK_display_template_head_display_template` FOREIGN KEY (`display_template_id`) REFERENCES `display_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2074 DEFAULT CHARSET=latin1;

    ALTER TABLE `qfix`.`fees` ADD COLUMN `display_template_id` INTEGER AFTER `updated_by`,
 ADD CONSTRAINT `FK_fees_display_template` FOREIGN KEY `FK_fees_display_template` (`display_template_id`)
    REFERENCES `display_template` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
    
insert into display_head (id, name, description, branch_id, is_delete, active)
select bdl.id, fees_description, other_description, bdl.branch_id, bdl.is_delete, 'Y' from branch_display_layout as bdl;

 insert into district values(634, 634, 'Paschim Bardhaman', 'Y', 19);
insert into taluka values (6199, 6199, 'Asansol', 'Y' , '634');

update `menu_items` set menu_name = 'Display Head' , state_url = 'displayHead.list' where menu_id = 41;
update `menu_permission` set url = 'displayHead.list' where menu_permission_id = 141;
update `menu_permission` set url = 'displayHead.add' where menu_permission_id = 142;
update `menu_permission` set url = 'displayHead.edit' where menu_permission_id = 143;


/* Other Charges Chnages....*/
ALTER TABLE `qfix`.`payment_audit` ADD COLUMN `total_paid_amount` DOUBLE AFTER `updated_by_process`;
ALTER TABLE `qfix`.`payments` ADD COLUMN `other_charges` DOUBLE AFTER `fees_schedule_id`;
ALTER TABLE `payment`.`payment_detail` ADD COLUMN `total_paid_amount` DOUBLE AFTER `payment_mode`;

DROP TABLE IF EXISTS `payment`.`transaction_charges`;
CREATE TABLE  `payment`.`transaction_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_gateway_configuration_id` int(11) NOT NULL,
  `charges` double DEFAULT NULL,
  `charges_type` varchar(45) DEFAULT 'AMOUNT' COMMENT 'Amount Or Percentage',
  `charges_for` varchar(45) DEFAULT NULL COMMENT 'Debit, Credit OR NET BANKING',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_transaction_charges_payment_gateway_configuration` (`payment_gateway_configuration_id`),
  CONSTRAINT `FK_transaction_charges_payment_gateway_configuration` FOREIGN KEY (`payment_gateway_configuration_id`) REFERENCES `payment_gateway_configuration` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


/* OFFLINE CHANGES .....****/


ALTER TABLE `payment`.`payment_gateway_configuration` ADD COLUMN `transaction_type` VARCHAR(10) NOT NULL DEFAULT 'ONLINE' AFTER `payment_gateway_type`,
 ADD COLUMN `parent_gateway_id` INTEGER AFTER `transaction_type`,
 ADD CONSTRAINT `FK_payment_gateway_configuration_gateway_id` FOREIGN KEY `FK_payment_gateway_configuration_gateway_id` (`parent_gateway_id`)
    REFERENCES `payment_gateway_configuration` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `payment`.`payment_gateway_configuration` 
ADD COLUMN `alias_code` VARCHAR(45) NULL AFTER `parent_gateway_id`;


/* Added on 22-08-2017 */

ALTER TABLE `qfix`.`payments` ADD COLUMN `bank_name` VARCHAR(65) AFTER `other_charges`,
 ADD COLUMN `bank_branch_name` VARCHAR(65) AFTER `bank_name`,
 ADD COLUMN `ifsc_code` VARCHAR(25) AFTER `bank_branch_name`,
 ADD COLUMN `cheque_or_dd_number` VARCHAR(25) AFTER `ifsc_code`;
 
 /*Added on 01-09-2017*/
 
 ALTER TABLE `qfix`.`teacher` 
CHANGE COLUMN `primaryContact` `primaryContact` VARCHAR(45) CHARACTER SET 'latin1' NOT NULL ;


/*Pitabas 16-9-2017*/ 
CREATE TABLE IF NOT EXISTS `feescode_student` (
  `student_id` int(11) NOT NULL,
  `fees_code_configuration_id` int(11) NOT NULL,
  `is_active` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`student_id`,`fees_code_configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*Pitabas 16-9-2017*/ 
 
/*for adding branch logo -- Ameya S* 16-9-2017*/
alter table branch add column logo_url varchar(255);
/*for adding branch logo -- Ameya S* 16-9-2017*/

/*for adding branch logo -- Ameya S*/



ALTER TABLE `qfix`.`school_admin` 
CHANGE COLUMN `city` `city` VARCHAR(45) NULL ,
CHANGE COLUMN `pinCode` `pinCode` VARCHAR(45) NULL ;

/*Divya 16-9-2017*/
UPDATE `qfix`.`menu_items` SET `menu_name`='Standards/course' WHERE  `menu_id`=4;
UPDATE `qfix`.`menu_items` SET `menu_name`='Standard/course Subject' WHERE  `menu_id`=37;

ALTER TABLE `qfix`.`student` MODIFY COLUMN `line1` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci;
ALTER TABLE `qfix`.`student_upload` MODIFY COLUMN `line1` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci;

INSERT INTO `qfix`.`menu_items` (`menu_id`, `menu_name`, `state_url`, `menu_position`) VALUES ('48', 'Assignments', 'assignment.list', '34');
INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `url`) VALUES ('153', '48', 'assignments.list');
INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `permission_name`, `url`, `rest_url`) VALUES ('154', '48', 'Send Assignments', 'assignments.send', 'school/assignments/save');
INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `permission_name`, `url`) VALUES ('155', '48', 'All Notice', 'notice.view');
UPDATE `qfix`.`menu_permission` SET `permission_name`='All Assignments', `url`='assignments.view' WHERE `menu_permission_id`='155';

INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('1', '48');
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('4', '48');
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('6', '48');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('153', '1');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('153', '4');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('153', '6');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('154', '1');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('154', '4');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('154', '6');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('155', '1');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('155', '4');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('155', '6');
/*Divya 16-9-2017*/


/*Praveen 16-9-2017*/

ALTER table online_admission ADD column username varchar(256);
/*Praveen 16-9-2017*/


ALTER TABLE `qfix`.`teacher` 
 MODIFY COLUMN `city` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 MODIFY COLUMN `pinCode` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci;
 
 
 DROP TABLE IF EXISTS `qfix`.`online_admission_user_document`;
CREATE TABLE  `qfix`.`online_admission_user_document` (
  `online_admission_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_path` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`online_admission_id`,`document_id`),
  KEY `FK_online_admission_user_codument_document` (`document_id`),
  CONSTRAINT `FK_online_admission_user_codument_document` FOREIGN KEY (`document_id`) REFERENCES `online_admission_document` (`id`),
  CONSTRAINT `FK_online_admission_user_codument_online_admission` FOREIGN KEY (`online_admission_id`) REFERENCES `online_admission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `qfix`.`menu_items` values ('49', 'Express Onboard', NULL, 'expressOnboard.page', 0);

INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `permission_name`, `url`) VALUES ('156', '49', 'Save', 'expressOnboard.page');

INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('1', '49');

INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('156', '1');


DROP TABLE IF EXISTS `qfix`.`board`;
CREATE TABLE  `qfix`.`board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `qfix`.`principal`;
CREATE TABLE  `qfix`.`principal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `principal_name` varchar(45) DEFAULT NULL,
  `mobile_no` int(11) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `principal_user_id_idx` (`user_id`),
  CONSTRAINT `principal_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `qfix`.`school_admin` MODIFY COLUMN `city` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `qfix`.`school_admin` MODIFY COLUMN `pinCode` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;



ALTER TABLE `qfix`.`branch` ADD COLUMN `board_id` INTEGER AFTER `logo_url`,
 ADD COLUMN `institute_type` VARCHAR(45) AFTER `board_id`,
 ADD CONSTRAINT `FK_branch_board` FOREIGN KEY `FK_branch_board` (`board_id`)
    REFERENCES `board` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `qfix`.`fees_code_configuration` MODIFY COLUMN `code` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 ADD COLUMN `description` VARCHAR(255) AFTER `id`;

 ALTER TABLE `qfix`.`fees_code_configuration` ADD COLUMN `is_delete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `description`;


INSERT INTO `qfix`.`menu_items` values ('50', 'Fees Code', NULL, 'fees-code.list', 10);

INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `permission_name`, `url`) VALUES ('158', '50', 'List', 'fees-code.list');
INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `permission_name`, `url`) VALUES ('159', '50', 'Save', 'fees-code.add');
INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `permission_name`, `url`) VALUES ('160', '50', 'Edit', 'fees-code.edit');
INSERT INTO `qfix`.`menu_permission` (`menu_permission_id`, `menu_id`, `permission_name`, `url`) VALUES ('161', '50', 'Delete', 'fees-code.delete');

INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('1', '50');
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('6', '50');

INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('158', '1');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('159', '1');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('160', '1');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('161', '1');

INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('158', '6');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('159', '6');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('160', '6');
INSERT INTO `qfix`.`role_menu_permission` (`menu_permission_id`, `role_id`) VALUES ('161', '6');


DROP TRIGGER IF EXISTS parent_insert_trig;
DROP TRIGGER IF EXISTS parent_update_trig;

DROP TRIGGER IF EXISTS student_insert_trig;
DROP TRIGGER IF EXISTS student_update_trig;

DROP TRIGGER IF EXISTS teacher_insert_trig;
DROP TRIGGER IF EXISTS teacher_update_trig;

DROP TRIGGER IF EXISTS vendor_insert_trig;
DROP TRIGGER IF EXISTS vendor_update_trig;



 ALTER TABLE `qfix`.`branch_configuration` ADD COLUMN `student_data_available` CHAR(1) NOT NULL DEFAULT 'Y' AFTER `offline_payments_enabled`,
 ADD COLUMN `unique_identifier_lable` VARCHAR(45) AFTER `student_data_available`,
 ADD COLUMN `unique_identifier_name` VARCHAR(45) AFTER `unique_identifier_lable`;


 ALTER TABLE `qfix`.`division` MODIFY COLUMN `displayName` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
 MODIFY COLUMN `academic_year_id` INTEGER,
 ADD COLUMN `qfix_defined_name` VARCHAR(45) AFTER `academic_year_id`;

ALTER TABLE `qfix`.`division` MODIFY COLUMN `code` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci;


 ALTER TABLE `qfix`.`standard` MODIFY COLUMN `academic_year_id` INTEGER,
 ADD COLUMN `qfix_defined_name` VARCHAR(45) AFTER `academic_year_id`;

 
 ALTER TABLE `qfix`.`subject` MODIFY COLUMN `branch_id` INTEGER NOT NULL DEFAULT 0,
 MODIFY COLUMN `category_id` INTEGER,
 MODIFY COLUMN `theory` DOUBLE,
 MODIFY COLUMN `practical` DOUBLE;

 insert into board values 
(2, 'ICSE', 'ICSE'),
(3, 'CBSE', 'CBSE'),
(4, 'SSC', 'SSC'),
(5, 'HSC', 'HSC');

ALTER TABLE `qfix`.`branch` MODIFY COLUMN `contactNumber` VARCHAR(14) CHARACTER SET latin1 COLLATE latin1_swedish_ci;

ALTER TABLE `qfix`.`principal` MODIFY COLUMN `mobile_no` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci;

/* END Express Onboard */

DROP TABLE IF EXISTS `qfix`.`express_onboard`;
CREATE TABLE  `qfix`.`express_onboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT 0,
  `craeted_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT '',
  `progress` varchar(1000) NOT NULL DEFAULT '',
  `institute_id` int(11) NOT NULL DEFAULT 0,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_express_onboard_institute` (`institute_id`),
  KEY `FK_express_onboard_branch` (`branch_id`),
  KEY `FK_express_onboard_created_by` (`craeted_by`),
  KEY `FK_express_onboard_updated_by` (`updated_by`),
  CONSTRAINT `FK_express_onboard_institute` FOREIGN KEY (`institute_id`) REFERENCES `institute` (`id`),
  CONSTRAINT `FK_express_onboard_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_express_onboard_created_by` FOREIGN KEY (`craeted_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_express_onboard_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/* Strting 17-10-2017 */

ALTER TABLE `qfix`.`late_fees_payment_detail` ADD COLUMN `combination_rule` VARCHAR(1000) AFTER `base_amount`;
ALTER TABLE `qfix`.`late_fees_payment_detail` MODIFY COLUMN `amount` DOUBLE DEFAULT NULL;
/** 30 Oct, 2017 **/
---- Express onboard changes -----

ALTER TABLE `qfix`.`branch_configuration`
ADD COLUMN `is_paydirect_enabled` CHAR(1) DEFAULT 'N' AFTER `unique_identifier_name`;

ALTER TABLE `qfix`.`working_days`
	ADD COLUMN `round_robin_timetable_days` INT NULL DEFAULT 0 AFTER `skip_timetable_on_holiday`;

ALTER TABLE `payment`.`payment_gateway_configuration` ADD COLUMN `single_kit_for_online_offline` VARCHAR(45) NOT NULL DEFAULT 'N' AFTER `alias_code`;


/* MULTIPLE TID changes starts here */

ALTER TABLE `qfix`.`fees` ADD COLUMN `currency` VARCHAR(25) NOT NULL DEFAULT 'INR' AFTER `display_template_id`;

DROP TABLE IF EXISTS `qfix`.`currency`;
CREATE TABLE  `qfix`.`currency` (
  `code` varchar(25) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `icon_css_class` varchar(45) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into currency values('INR', 'INR', 'rupee');
insert into currency values('USD', 'USD', 'usd');

ALTER TABLE `qfix`.`fees` ADD COLUMN `allow_user_entered_amount` CHAR(1) NOT NULL DEFAULT 'N' AFTER `currency`,
 ADD COLUMN `allow_repeat_entries` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_user_entered_amount`;

ALTER TABLE `payment_audit`
CHANGE COLUMN `pg_transaction_id` `pg_transaction_id` VARCHAR(265) NULL DEFAULT NULL AFTER `total_paid_amount`;


ALTER TABLE `payment`.`payment_detail` ADD COLUMN `currency_icon_class` VARCHAR(45) NOT NULL DEFAULT 'rupee' AFTER `payment_for_name`;
ALTER TABLE `payment`.`payment_detail` ADD COLUMN `currency` VARCHAR(45) NOT NULL DEFAULT 'INR' AFTER `currency_icon_class`;

ALTER TABLE `payment`.`payment_gateway_configuration` ADD COLUMN `currency` VARCHAR(45) NOT NULL DEFAULT 'INR' AFTER `single_kit_for_online_offline`;


/*Audit log changes*/

CREATE TABLE  `qfix`.`audit_log`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`entity_type` varchar(30) NOT NULL,
`user_id` int(11) NOT NULL,
`operation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`operation_type` varchar(30) NOT NULL,
`ip_address` varchar(30) NOT NULL,
`old_data` mediumtext,
`new_data` mediumtext ,
PRIMARY KEY (`id`) 
);

INSERT INTO `qfix`.`menu_items`(`menu_name`, `state_url`, `menu_position`) values ('Audit Log','auditLog.page', 8);
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('1', '51');


/** Branch configuration for parent portal logo **/
ALTER TABLE `branch_configuration`
	ADD COLUMN `show_logo_on_parent_portal` VARCHAR(1) NULL DEFAULT 'N' AFTER `is_paydirect_enabled`;

/*Audit log changes*/

/* BANK_PARTNER Role Changes */

/*run this onle once*/
INSERT INTO `qfix`.`role` (`id`, `name`) VALUES (8, 'BANK_PARTNER');

/* whatever id generated in the previous query will be placed in the below query */
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES ('8', '51');

/*run this onle once*/

/*run this for adding new user as BANK_PARTNER and change the username and temp_password, password as required*/
INSERT INTO `qfix`.`user` (`username`, `active`, `authentication_type`, `temp_password`, `password`, `password_salt`, `isDelete`) VALUES ('auditor', 'Y', 'APPLICATION', 'test', 'test', 'test', 'N');

insert into user_role (user_id, role_id) select id, 8 from user where username = 'auditor'
/*run this for adding new user as BANK_PARTNER and change the username and temp_password, password as required*/

/* BANK_PARTNER Role Changes */


-- Online Admission

CREATE TABLE `qfix`.`online_admission_scheme_configuration` (
  `branch_id` INTEGER NOT NULL,
  `scheme_code_id` INTEGER NOT NULL,
  `category` VARCHAR(45) NOT NULL DEFAULT 'INR',
  PRIMARY KEY (`branch_id`, `scheme_code_id`)
)
ENGINE = InnoDB;

ALTER TABLE `qfix`.`online_admission` ADD COLUMN `currency` VARCHAR(45) NOT NULL DEFAULT 'INR' AFTER `username`;

ALTER TABLE `qfix`.`online_admission` CHANGE COLUMN `currency` `category` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'INR'
, ROW_FORMAT = DYNAMIC;

 ALTER TABLE `qfix`.`online_admission_course_fees` DROP COLUMN `amountUsd`;
 
ALTER TABLE `qfix`.`online_admission_course_fees` ADD COLUMN `currency` VARCHAR(45) NOT NULL DEFAULT 'INR' AFTER `subcaste_id`;

/*Payment report changes*/

DROP TABLE IF EXISTS `qfix`.`payment_summary_report`;
CREATE TABLE  `qfix`.`payment_summary_report`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
    report_date datetime NOT NULL,        
	number_of_payments int(11) NOT NULL,
	grand_total double NOT NULL,        
	submerchant_charges double NOT NULL,
	refund_adjusted double NOT NULL,
	chargeback_adjusted double NOT NULL,
	chargeback_reversal double NOT NULL,
	total_net_amount double NOT NULL,    
    PRIMARY KEY (`id`)
);

ALTER TABLE `qfix`.`payment_audit` 
ADD COLUMN `settlement_date` DATETIME NULL DEFAULT NULL ;

ALTER TABLE `payment`.`payment_detail` 
ADD COLUMN `settlement_date` DATETIME NULL DEFAULT NULL ;

ALTER TABLE `qfix`.`payment_audit` 
ADD COLUMN `summary_report_id` INT(11) DEFAULT 0 AFTER `settlement_date`;

/** Menu grouping feature.*/
ALTER TABLE `menu_items`
	ADD COLUMN `group_tag` VARCHAR(50) NULL AFTER `menu_position`;
	
ALTER TABLE `menu_items`
ADD COLUMN `group_item_position` INT(11) NULL DEFAULT NULL AFTER `group_tag`;
	
	
ALTER TABLE `menu_items`
ADD COLUMN `group_name` VARCHAR(200) NULL DEFAULT NULL AFTER `group_item_position`;	

update menu_items set group_tag = 'engage', menu_position = 10, group_name = 'Q-Engage' 
where menu_name in ('Events', 'Chat', 'Forum', 'View Forum', 'Performance Report');

update menu_items set group_tag = 'connect', menu_position = 9, group_name = 'Q-Connect' where menu_name in ('Notice', 'Timetable','Holidays','E-Diary', 'Assignments','Groups And Communities');

update menu_items set group_tag = 'digital', menu_position = 11, group_name = 'Q-Admissions' where menu_name in ('Admission Report');

update menu_items set group_tag = 'institute', menu_position = 2, group_name = 'Institute' where menu_name in ('Institutes');

update menu_items set group_tag = 'qfix_pay', menu_position = 8, group_name = 'Q-Pay' where menu_name in ('Fees Report', 'Audit Log', 'Assign Permission','Receipt', 'Payment Settings','Acknowledge Offline Payments');

update menu_items set group_tag = 'branch', menu_position = 3, group_name = 'Branch' where menu_name in ('Branch');

update menu_items set group_tag = 'std_div' , menu_position = 5, group_name = 'Standard/Division' where menu_name in ('Standards/course', 'Division','Standard/Course Subject','Subject','Teachers');
		
update menu_items set group_tag = 'student', menu_position = 7, group_name = 'Student' where menu_name in ('Students','Student Report','Delete Student','Export Data');

update menu_items set group_tag = 'academic_year', menu_position = 4, group_name = 'Academic Year' where menu_name in ('Academic Year', 'Working Days');
	
update menu_items set group_tag = 'fees', menu_position = 6, group_name = 'Fees' where menu_name in ('Fees Head', 'Display Head', 'Fees Code', 'Display Template','DisplayTemplate', 'LateFees', 'Fees');
	
update menu_items set group_tag = 'general', menu_position = 12, group_name = 'General' where menu_name in ('About Us','Settings', 'Contact Us');
	
update menu_items set group_tag = 'express', menu_position = 1, group_name = 'Express' where menu_name in ('Express Onboard');

INSERT INTO `qfix`.`menu_items` (`menu_name`, `state_url`, `menu_position`, `group_tag`, `group_item_position`, `group_name`) VALUES ('Dashboard', 'dashboard.page', '0', 'dashboard', '0', 'Dashboard');
INSERT INTO role_menu (menu_id, role_id) values (54, 1);
update menu_items set group_item_position =	1 where state_url = 'workingday.list';
update menu_items set group_item_position =	5 where state_url = 'timetable.list';
update menu_items set group_item_position =	4 where state_url = 'teacher.list';
update menu_items set group_item_position =	2 where state_url = 'subject.list';
update menu_items set group_item_position =	2 where state_url = 'student.report';
update menu_items set group_item_position =	0 where state_url = 'student.list';
update menu_items set group_item_position =	1 where state_url = 'student.del';
update menu_items set group_item_position =	3 where state_url = 'standardSubject.list';
update menu_items set group_item_position =	1 where state_url = 'standard.list';
update menu_items set group_item_position =	0 where state_url = 'settings.email';
update menu_items set group_item_position =	4 where state_url = 'role-access';
update menu_items set group_item_position =	5 where state_url = 'payment-settings.list';
update menu_items set group_item_position =	3 where state_url = 'offline.list';
update menu_items set group_item_position =	1 where state_url = 'notice.list';
update menu_items set group_item_position =	5 where state_url = 'lateFeesDetail.list';
update menu_items set group_item_position =	0 where state_url = 'institute.list';
update menu_items set group_item_position =	4 where state_url = 'holiday.list';
update menu_items set group_item_position =	1 where state_url = 'head.list';
update menu_items set group_item_position =	0 where state_url = 'group.list';
update menu_items set group_item_position =	2 where state_url = 'forum.view';
update menu_items set group_item_position =	1 where state_url = 'forum.list';
update menu_items set group_item_position =	0 where state_url = 'feesreport.list';
update menu_items set group_item_position =	6 where state_url = 'feesreport.list';
update menu_items set group_item_position =	0 where state_url = 'fees.manage';
update menu_items set group_item_position =	3 where state_url = 'fees-code.list';
update menu_items set group_item_position =	0 where state_url = 'expressOnboard.page';
update menu_items set group_item_position =	3 where state_url = 'event.list';
update menu_items set group_item_position =	2 where state_url = 'ediary.list';
update menu_items set group_item_position =	0 where state_url = 'division.list';
update menu_items set group_item_position =	4 where state_url = 'displayTemplate.list';
update menu_items set group_item_position = 2 where state_url = 'displayHead.list';
update menu_items set group_item_position =	2 where state_url = 'contactus.page';
update menu_items set group_item_position =	0 where state_url = 'chat.list';
update menu_items set group_item_position =	0 where state_url = 'branch.list';
update menu_items set group_item_position =	2 where state_url = 'auditLog.page';
update menu_items set group_item_position =	3 where state_url = 'assignment.list';
update menu_items set group_item_position =	4 where state_url = 'admissionreport.list';
update menu_items set group_item_position =	5 where state_url = 'admissionreport.list';
update menu_items set group_item_position =	0 where state_url = 'academicyear.list';
update menu_items set group_item_position =	1 where state_url = 'aboutus.page';
update menu_items set group_item_position =	1 where state_url = 'receipt.add';
update menu_items set group_item_position =	4 where state_url = 'report.list';
update menu_items set group_item_position =	3 where state_url = 'exportData.list';
/* Adding additional links to the bank partner  role */

INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES (8, 1);
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES (8, 28);
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES (8, 8);
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES (8, 2);
INSERT INTO `qfix`.`role_menu` (`role_id`, `menu_id`) VALUES (8, 9);


/* Adding additional links to the bank partner  role */

/*to add holiday */
  
CREATE TABLE `public_holiday` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`title` LONGTEXT NOT NULL,
	`description` VARCHAR(45) NULL DEFAULT NULL,
	`from_date` DATE NOT NULL,
	`to_date` DATE NOT NULL,
	`is_public_holiday` CHAR(50) NOT NULL DEFAULT 'Y',
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB AUTO_INCREMENT=30;


/** OTP Based configuration for  branch. This controls which branch will have the OTP based login feature enabled on parent protal **/
ALTER TABLE `branch_configuration`
	ADD COLUMN `is_otp_enabled` VARCHAR(1) NULL DEFAULT 'N' AFTER `show_logo_on_parent_portal`;

ALTER TABLE `qfix`.`payment_summary_report` 
ADD COLUMN `uploaded_by` VARCHAR(100) NULL AFTER `total_net_amount`,
ADD COLUMN `uploaded_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `uploaded_by`;

ALTER TABLE `qfix`.`payment_audit` 
ADD COLUMN `settlement_amount` DOUBLE NULL AFTER `summary_report_id`;

ALTER TABLE `payment`.`payment_detail` ADD COLUMN `settlement_amount` DOUBLE NULL AFTER `settlement_date`

/**  4 Dec 2017 ***/
/** Branch Address field changes **/
ALTER TABLE `branch`
	CHANGE COLUMN `city` `city` VARCHAR(45) NULL COLLATE 'latin1_swedish_ci' AFTER `area`;

/** 08 Nov, 2017 **/	
-- Plug and play changes -----	
ALTER TABLE `payment_audit`
CHANGE COLUMN `pg_transaction_id` `pg_transaction_id` VARCHAR(265) NULL DEFAULT NULL AFTER `total_paid_amount`;


-- Refund Payment changes starts here ---

ALTER TABLE `qfix`.`branch_configuration` ADD COLUMN `allow_credit_card_refund` CHAR(1) NOT NULL DEFAULT 'N',
 ADD COLUMN `allow_credit_card_partial_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_credit_card_refund`,
 ADD COLUMN `allow_debit_card_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_credit_card_partial_refund`,
 ADD COLUMN `allow_debit_card_partial_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_debit_card_refund`,
 ADD COLUMN `allow_net_banking_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_debit_card_partial_refund`,
 ADD COLUMN `allow_net_banking_partial_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_net_banking_refund`,
 ADD COLUMN `allow_neft_rtgs_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_net_banking_partial_refund`,
 ADD COLUMN `allow_neft_rtgs_partial_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_neft_rtgs_refund`,
 ADD COLUMN `allow_cheque_dd_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_neft_rtgs_partial_refund`,
 ADD COLUMN `allow_cheque_dd_partial_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_cheque_dd_refund`,
 ADD COLUMN `allow_cash_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_cheque_dd_partial_refund`,
 ADD COLUMN `allow_cash_partial_refund` CHAR(1) NOT NULL DEFAULT 'N' AFTER `allow_cash_refund`;

insert into menu_items values(55, 'Refund Configuration', null, 'branch.refund', 8,'qfix_pay',7, 'Q-Pay' );

insert into menu_permission values (170, 55, 'Save', 'branch.refund', null);

insert into role_menu values (6, 55);
insert into role_menu values (1, 55);

insert into role_menu_permission values (170, 1);
insert into role_menu_permission values (170, 6);


DROP TABLE IF EXISTS `qfix`.`payment_refund_detail`;
CREATE TABLE  `qfix`.`payment_refund_detail` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  `payment_gateway_refund_id` varchar(20) NOT NULL,
  `payment_gateway_transaction_id` varchar(20) NOT NULL,
  `passbook_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_payment_refund_details_payments` (`payment_id`),
  KEY `FK_payment_refund_details_user` (`created_by`),
  KEY `FK_payment_refund_detail_passbook` (`passbook_id`),
  CONSTRAINT `FK_payment_refund_detail_passbook` FOREIGN KEY (`passbook_id`) REFERENCES `passbook` (`id`),
  CONSTRAINT `FK_payment_refund_details_payments` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`),
  CONSTRAINT `FK_payment_refund_details_user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `payment`.`payment_refund_detail`;
CREATE TABLE  `payment`.`payment_refund_detail` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `qfix_reference_number` varchar(45) NOT NULL,
  `amount` double NOT NULL,
  `request_payload` blob NOT NULL,
  `response_payload` blob NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_using_token` varchar(65) NOT NULL,
  `payment_gateway_transaction_id` varchar(20) NOT NULL,
  `payment_gateway_refund_id` varchar(20) NOT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_payment_refund_detail_payment_detail` (`qfix_reference_number`),
  CONSTRAINT `FK_payment_refund_detail_payment_detail` FOREIGN KEY (`qfix_reference_number`) REFERENCES `payment_detail` (`qfix_reference_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Express onboard session 11-DEC-2017*/

create table express_session(
id int(11) NOT NULL auto_increment,
stage varchar(255) NOT NULL,
user_id int(11)  NOT NULL,
saved_id int(11)  NOT NULL,
parent_id int(11),
PRIMARY KEY (`id`)
);

ALTER TABLE `holidays`
	ADD COLUMN `is_public_holiday` CHAR(50) NOT NULL DEFAULT 'N' AFTER `branch_id`;

Date: 08 Dec, 2017
/** Express Onboard, standard subject enhancements **/

CREATE TABLE `college_subject` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` TEXT NULL,
	`tag` TEXT NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

CREATE TABLE `school_subject` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` TEXT NULL,
	`tag` TEXT NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

ALTER TABLE `subject`
	ADD COLUMN `tag` VARCHAR(50) NULL DEFAULT NULL AFTER `practical`;
	
	
ALTER TABLE `public_holiday`
ADD COLUMN `is_public_holiday` CHAR(50) NOT NULL DEFAULT 'N';

CREATE TABLE `school_standards` (
	`id` INT(11) NOT NULL,
	`name` VARCHAR(50) NULL DEFAULT NULL,
	`tag` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

CREATE TABLE `college_years` (
	`id` INT(11) NOT NULL,
	`name` VARCHAR(50) NULL DEFAULT NULL,
	`tag` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

CREATE TABLE `college_courses` (
	`id` INT(11) NOT NULL,
	`name` LONGTEXT NULL,
	INDEX `Index 1` (`id`)
)
COLLATE='latin1_swedish_ci'ENGINE=InnoDB;

CREATE TABLE `school_board_affiliation` (
	`id` INT(11) NOT NULL,
	`board` LONGTEXT NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'ENGINE=InnoDB;

CREATE TABLE `college_universities` (
	`name` LONGTEXT NULL,
	`id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

ALTER TABLE `branch`
ADD COLUMN `board_affiliation` INT(11) NULL DEFAULT NULL AFTER `board_id`,
ADD CONSTRAINT `FK_board_affiliation` FOREIGN KEY (`board_affiliation`) 
REFERENCES `school_board_affiliation` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `branch`
ADD COLUMN `university` INT(11) NULL DEFAULT NULL AFTER `board_affiliation`,
ADD CONSTRAINT `FK_university` FOREIGN KEY (`university`) 
REFERENCES `college_universities` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;


/* Do not run this
 * 
 * ALTER TABLE `qfix`.`standard_subject` 
DROP FOREIGN KEY `FK_standard_subject_subject`;
ALTER TABLE `qfix`.`standard_subject` 
ADD CONSTRAINT `FK_standard_subject_subject`
  FOREIGN KEY (`subject_id`)
  REFERENCES `qfix`.`subject` (`id`)
  ON DELETE CASCADE;*/


/** Changing cascading affect on standard and standard division **/
ALTER TABLE `standard_division`
	DROP FOREIGN KEY `FK_standard_division_division`,
	DROP FOREIGN KEY `FK_standard_division_standard`;
ALTER TABLE `standard_division`
	ADD CONSTRAINT `FK_standard_division_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
	ADD CONSTRAINT `FK_standard_division_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;


CREATE TABLE `qfix`.`year_course_subject` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `standard_id` INT(11) NULL,
  `division_id` INT(11) NULL,
  `branch_id` INT(11) NULL,
  `is_delete` VARCHAR(1) NULL,
  PRIMARY KEY (`id`));
  
  ALTER TABLE `qfix`.`year_course_subject` 
ADD COLUMN `subject_id` INT(11) NULL AFTER `is_delete`;

  ALTER TABLE `qfix`.`year_course_subject` 
ADD COLUMN `academic_year_id` INT(11) NULL AFTER `subject_id`;

ALTER TABLE `qfix`.`year_course_subject` 
CHANGE COLUMN `is_delete` `is_delete` VARCHAR(1) NULL DEFAULT 'N' ;
	
/*missing transaction id change 12-12-17 - Ameya*/
create table incorrect_transaction_id(
`id` INT NOT NULL AUTO_INCREMENT,
	`transaction_id` varchar(20) NOT NULL,
	`date` datetime,
    `amount` double ,
	PRIMARY KEY (`id`) 
);

ALTER TABLE `qfix`.`incorrect_transaction_id` 
ADD COLUMN `type` VARCHAR(45) NOT NULL AFTER `amount`,
ADD COLUMN `merchant_id` VARCHAR(255) NULL AFTER `type`,
ADD COLUMN `bank_name` VARCHAR(255) NULL AFTER `merchant_id`,
ADD COLUMN `qfix_reference_number` VARCHAR(255) NULL AFTER `bank_name`,
ADD COLUMN `bank_transaction_id` VARCHAR(255) NULL AFTER `qfix_reference_number`,
ADD COLUMN `drawer_name` VARCHAR(255) NULL AFTER `bank_transaction_id`,
ADD COLUMN `pickup_location` VARCHAR(255) NULL AFTER `drawer_name`,
ADD COLUMN `pickup_point` VARCHAR(255) NULL AFTER `pickup_location`,
ADD COLUMN `cl_location` VARCHAR(255) NULL AFTER `pickup_point`,
ADD COLUMN `prod_code` VARCHAR(255) NULL AFTER `cl_location`;

/*missing transaction id change 12-12-17 - Ameya*/
