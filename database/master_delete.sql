truncate `group_user`;
truncate `group_individual`;
truncate `group_standard`;
truncate `group_standard`;
truncate `group_tags`;
truncate `group_forum`;
truncate `branch_vendor`;
truncate `vendor`;
truncate `user_event`;
truncate `user_chat_profile`;
truncate `token`;
truncate `teacher`;
truncate `student_event`;
truncate `student_group`;
truncate `student_marks`;
delete from `student_result`;
delete from `subject`;
truncate `standard_division`;
truncate `school_admin`;
truncate `otp`;
truncate `notice_individual`;
truncate `notice_group`;
truncate `payments`;
truncate `personal_task`;
truncate `branch_forum_detail`;

delete from `user_contact`;
delete from `contact`;
delete from `forgot_password_otp`;
delete FROM `email_config`;
delete FROM `sms_config`;
delete FROM `event_group`;
delete FROM `goals`;
truncate `passbook`;
delete FROM `event`;
DELETE FROM `fees`;
truncate `group_standard`;
truncate `ediary_group`;
truncate `working_days`;
truncate `holidays`;


truncate `user_ediary`;
truncate `group_tags`;
DELETE FROM `ediary`;
DELETE FROM `group`;
DELETE FROM `notice`;
DELETE FROM `student`;
DELETE FROM `student_upload`;
DELETE FROM `parent`;
DELETE FROM `division`;
DELETE FROM `standard`;
DELETE FROM `fees`;
DELETE FROM  `head`;
delete from  `academic_year`;
DELETE FROM `branch`;
DELETE FROM `user_forum_profile`;
DELETE FROM `user_mall_profile`;
truncate `user_payment_account`;
truncate role_menu_permission;
truncate role_menu;
truncate menu_permission;
DELETE FROM menu_items;
truncate `user_role`;
truncate `payment_audit`;
DELETE FROM `user`;
delete from  payment_setting where institute_id is not null;
DELETE FROM `institute`;


ALTER TABLE `institute` AUTO_INCREMENT = 1;
ALTER TABLE `branch` AUTO_INCREMENT = 1;
ALTER TABLE `academic_year` AUTO_INCREMENT = 1;
ALTER TABLE `division` AUTO_INCREMENT = 1;
ALTER TABLE `ediary` AUTO_INCREMENT = 1;
ALTER TABLE `event` AUTO_INCREMENT = 1;
ALTER TABLE `fees` AUTO_INCREMENT = 1;
ALTER TABLE `goals` AUTO_INCREMENT = 1;
ALTER TABLE `group` AUTO_INCREMENT = 1;
ALTER TABLE `head` AUTO_INCREMENT = 1;
ALTER TABLE `menu_items` AUTO_INCREMENT = 1;
ALTER TABLE `notice` AUTO_INCREMENT = 1;
ALTER TABLE `parent` AUTO_INCREMENT = 1;
ALTER TABLE `standard` AUTO_INCREMENT = 1;
ALTER TABLE `student` AUTO_INCREMENT = 1;
ALTER TABLE `student_result` AUTO_INCREMENT = 1;
ALTER TABLE `student_upload` AUTO_INCREMENT = 1;
ALTER TABLE `subject` AUTO_INCREMENT = 1;
ALTER TABLE `user` AUTO_INCREMENT = 1;
ALTER TABLE `teacher` AUTO_INCREMENT = 1;
ALTER TABLE `vendor` AUTO_INCREMENT = 1;
ALTER TABLE `user_forum_profile` AUTO_INCREMENT = 1;
ALTER TABLE `user_mall_profile` AUTO_INCREMENT = 1;
ALTER TABLE `contact` AUTO_INCREMENT = 1;

insert into `email_config` values(1, 'smtp.mailgun.org', '587', 'SMTP', 'SSL', 'SSLLLT', 'postmaster@www.eduqfix.com', 'Y','d78de44006f84544f9c6a14f3066d34e');

insert into `sms_config` values(1, 'http://smsc.smsconnexion.com/api/gateway.aspx', 'Qfixinfo', 'YH2LWqmrdJ', 'Qfixinfo', 'Y');

INSERT INTO `user` (`username`, `password`, `active`, `isDelete`) VALUES ('admin', 'admin@556', 'Y', 'N');

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES (1,1);

insert into `user_forum_profile` values (1, 'admin', 'admin@556', 0, 'Y', 'N', 1);

insert into `user_mall_profile` values (1, 1, 'v2stech', 'v2stech', 0, 'Y', 'N');

/* LOCAL test envirnment.
INSERT INTO `user_chat_profile` (`login`, `password`, `email`, `chat_user_profile_id`, `user_id`, `active`, `isDelete`)
VALUES ('v2stech-sushant', 'Mumbai@556', 'info@v2stech.com', '8831470', '1', 'Y', 'N'); 	
 */


/* UAT test envirnment.
INSERT INTO `user_chat_profile` (`login`, `password`, `email`, `chat_user_profile_id`, `user_id`, `active`, `isDelete`)
VALUES ('v2stech-uat', 'Mumbai@556', 'dayanand.rahate@v2stech.com', '14020913', '1', 'Y', 'N');  
 */

/* LIVE test envirnment.
INSERT INTO `user_chat_profile` (`login`, `password`, `email`, `chat_user_profile_id`, `user_id`, `active`, `isDelete`)
VALUES ('vikaskale', 'Mumbai@556', 'vikas.kale@v2stech.com', '7414349', '1', 'Y', 'N');  
 */

