--
-- Definition of trigger `parent_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `parent_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `parent_insert_trig` AFTER INSERT ON `parent` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

IF NOT EXISTS (SELECT id FROM contact WHERE user_id = NEW.user_id) THEN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.email, NEW.primaryContact, NEW.user_id, NEW.firstname, NEW.lastname, NEW.city, NEW.photo);
END IF;

END $$

DELIMITER ;

--
-- Definition of trigger `parent_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `parent_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `parent_update_trig` AFTER UPDATE ON `parent` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email, phone =NEW.primaryContact, photo = NEW.photo, 
firstname= NEW.firstname, lastname=NEW.lastname, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;






--
-- Definition of trigger `student_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `student_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `student_insert_trig` AFTER INSERT ON `student` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.email_address, NEW.mobile, NEW.user_id, NEW.first_name, NEW.last_name, NEW.city, NEW.photo);


END $$

DELIMITER ;

--
-- Definition of trigger `student_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `student_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `student_update_trig` AFTER UPDATE ON `student` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email_address, phone =NEW.mobile, photo = NEW.photo, 
firstname= NEW.first_name, lastname=NEW.last_name, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;

--
-- Definition of table `student_event`
--






--
-- Definition of trigger `teacher_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `teacher_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `teacher_insert_trig` AFTER INSERT ON `teacher` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one


BEGIN
IF NOT EXISTS (SELECT id FROM contact WHERE user_id = NEW.user_id) THEN
	insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
	values(NEW.emailAddress, NEW.primaryContact, NEW.user_id, NEW.firstName, NEW.lastName, NEW.city, NEW.photo);
END IF;
END $$

DELIMITER ;

--
-- Definition of trigger `teacher_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `teacher_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `teacher_update_trig` AFTER UPDATE ON `teacher` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.emailAddress, phone =NEW.primaryContact, photo = NEW.photo, 
firstname= NEW.firstName, lastname=NEW.lastName, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;

--
-- Definition of table `token`
--








--
-- Definition of trigger `vendor_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `vendor_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `vendor_insert_trig` AFTER INSERT ON `vendor` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, city) 
values(NEW.email, NEW.mobile, NEW.user_id, NEW.merchant_legal_name, NEW.city);


END $$

DELIMITER ;

--
-- Definition of trigger `vendor_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `vendor_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `vendor_update_trig` AFTER UPDATE ON `vendor` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email, phone =NEW.mobile,  
firstname= NEW.merchant_legal_name, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;







-- UPDATED ON 31-01-2017 ---

DROP TRIGGER /*!50030 IF EXISTS */ `vendor_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `vendor_insert_trig` AFTER INSERT ON `vendor` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, city) 
values(NEW.email, NEW.mobile, NEW.user_id, NEW.contact_first_name, NEW.city);


END $$

DELIMITER ;

--
-- Definition of trigger `vendor_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `vendor_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `vendor_update_trig` AFTER UPDATE ON `vendor` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email, phone =NEW.mobile,  
firstname= NEW.contact_first_name, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;