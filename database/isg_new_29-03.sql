-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.6.24


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema isg_sit
--

CREATE DATABASE IF NOT EXISTS isg_sit;
USE isg_sit;

--
-- Definition of table `academic_year`
--

DROP TABLE IF EXISTS `academic_year`;
CREATE TABLE `academic_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `is_current_active_year` char(1) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_academic_year_branchId` (`branch_id`),
  CONSTRAINT `FK_academic_year_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `academic_year`
--

/*!40000 ALTER TABLE `academic_year` DISABLE KEYS */;
/*!40000 ALTER TABLE `academic_year` ENABLE KEYS */;


--
-- Definition of table `authentication_type`
--

DROP TABLE IF EXISTS `authentication_type`;
CREATE TABLE `authentication_type` (
  `code` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authentication_type`
--

/*!40000 ALTER TABLE `authentication_type` DISABLE KEYS */;
INSERT INTO `authentication_type` (`code`,`description`) VALUES 
 ('APPLICATION','Application default login'),
 ('SOCIAL','Social profile based authentication');
/*!40000 ALTER TABLE `authentication_type` ENABLE KEYS */;


--
-- Definition of table `branch`
--

DROP TABLE IF EXISTS `branch`;
CREATE TABLE `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address_line` varchar(255) DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pincode` varchar(6) CHARACTER SET latin1 NOT NULL,
  `contactNumber` varchar(14) CHARACTER SET latin1 NOT NULL,
  `contactEmail` varchar(64) DEFAULT NULL,
  `websiteUrl` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `institute_id` int(11) NOT NULL,
  `taluka_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `fk_branches_institute1_idx` (`institute_id`),
  KEY `fk_branches_taluka1_idx` (`taluka_id`),
  KEY `fk_branches_district1_idx` (`district_id`),
  KEY `fk_branch_state1_idx` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;


--
-- Definition of table `branch_forum_detail`
--

DROP TABLE IF EXISTS `branch_forum_detail`;
CREATE TABLE `branch_forum_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_category_id` int(11) NOT NULL,
  `read_write_group_id` int(11) NOT NULL,
  `read_only_group_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_branch_id_idx` (`branch_id`),
  CONSTRAINT `fk_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_forum_detail`
--

/*!40000 ALTER TABLE `branch_forum_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch_forum_detail` ENABLE KEYS */;


--
-- Definition of table `branch_vendor`
--

DROP TABLE IF EXISTS `branch_vendor`;
CREATE TABLE `branch_vendor` (
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`branch_id`) USING BTREE,
  KEY `FK_institute_vendor_2` (`branch_id`),
  CONSTRAINT `FK_institute_vendor_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_institute_vendor_2` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_vendor`
--

/*!40000 ALTER TABLE `branch_vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch_vendor` ENABLE KEYS */;


--
-- Definition of table `caste`
--

DROP TABLE IF EXISTS `caste`;
CREATE TABLE `caste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) DEFAULT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `caste`
--

/*!40000 ALTER TABLE `caste` DISABLE KEYS */;
INSERT INTO `caste` (`id`,`code`,`name`,`active`) VALUES 
 (1,'OPEN','OPEN','Y'),
 (2,'OBC','OBC','Y'),
 (3,'SC','SC','Y'),
 (4,'NT','NT','Y');
/*!40000 ALTER TABLE `caste` ENABLE KEYS */;


--
-- Definition of table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` varchar(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `taluka_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_taluka1_idx` (`taluka_id`),
  CONSTRAINT `fk_city_taluka1` FOREIGN KEY (`taluka_id`) REFERENCES `taluka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;


--
-- Definition of table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `photo` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `firstname` varchar(45) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_profile_search_user_id_idx` (`user_id`),
  CONSTRAINT `fk_profile_search_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;


--
-- Definition of table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_courses_branches1_idx` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course`
--

/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`,`name`,`active`,`branch_id`) VALUES 
 (3,'English','Y',NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;


--
-- Definition of table `district`
--

DROP TABLE IF EXISTS `district`;
CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_STATE_ID_idx` (`state_id`),
  CONSTRAINT `FK_STATE_ID` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

/*!40000 ALTER TABLE `district` DISABLE KEYS */;
INSERT INTO `district` (`id`,`code`,`name`,`active`,`state_id`) VALUES 
 (1,'Thane','Thane','Y',1),
 (2,'Mumbai','Mumbai','Y',1);
/*!40000 ALTER TABLE `district` ENABLE KEYS */;


--
-- Definition of table `division`
--

DROP TABLE IF EXISTS `division`;
CREATE TABLE `division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `displayName` varchar(45) NOT NULL,
  `code` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_division_branch_1` (`branch_id`),
  KEY `FK_division_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_division_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_division_branch_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

/*!40000 ALTER TABLE `division` DISABLE KEYS */;
/*!40000 ALTER TABLE `division` ENABLE KEYS */;


--
-- Definition of table `ediary`
--

DROP TABLE IF EXISTS `ediary`;
CREATE TABLE `ediary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `status` varchar(45) DEFAULT 'N',
  `branch_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `group_entry` char(1) DEFAULT 'N',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ediary_branch_id_idx` (`branch_id`),
  KEY `fk_ediary_created_by_idx` (`created_by`),
  CONSTRAINT `fk_ediary_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediary_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ediary`
--

/*!40000 ALTER TABLE `ediary` DISABLE KEYS */;
/*!40000 ALTER TABLE `ediary` ENABLE KEYS */;


--
-- Definition of table `ediary_group`
--

DROP TABLE IF EXISTS `ediary_group`;
CREATE TABLE `ediary_group` (
  `ediary_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`ediary_id`,`group_id`),
  KEY `fk_ediarygroup_ediary_idx` (`ediary_id`),
  KEY `fk_ediarygroup_group_idx` (`group_id`),
  CONSTRAINT `fk_ediarygroup_ediary` FOREIGN KEY (`ediary_id`) REFERENCES `ediary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediarygroup_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ediary_group`
--

/*!40000 ALTER TABLE `ediary_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ediary_group` ENABLE KEYS */;


--
-- Definition of table `email_config`
--

DROP TABLE IF EXISTS `email_config`;
CREATE TABLE `email_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverName` varchar(255) NOT NULL,
  `port` varchar(45) NOT NULL,
  `connectionType` varchar(45) NOT NULL,
  `security` varchar(45) NOT NULL,
  `authenticationMethod` varchar(45) NOT NULL,
  `username` varchar(65) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_config`
--

/*!40000 ALTER TABLE `email_config` DISABLE KEYS */;
INSERT INTO `email_config` (`id`,`serverName`,`port`,`connectionType`,`security`,`authenticationMethod`,`username`,`active`,`password`) VALUES 
 (1,'smtp.gmail.com','587','SMTP','SSL','SSLLLT','qfixschool@gmail.com','Y','qfix@123');
/*!40000 ALTER TABLE `email_config` ENABLE KEYS */;


--
-- Definition of table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET latin1 NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `venue` varchar(100) CHARACTER SET latin1 NOT NULL,
  `startdate` date NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `alldayevent` char(1) DEFAULT 'N',
  `reminderSet` char(1) NOT NULL DEFAULT 'N',
  `image_url` varchar(255) DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `reccuring` char(1) DEFAULT 'N',
  `reminderbefore` int(10) DEFAULT NULL COMMENT 'reminder before will be stored in minutes basis',
  `summary` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `forparent` varchar(1) DEFAULT 'N',
  `forstudent` varchar(1) DEFAULT 'N',
  `forteacher` varchar(1) DEFAULT 'N',
  `course_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `frequency` varchar(15) DEFAULT NULL,
  `by_day` varchar(45) DEFAULT NULL,
  `by_date` varchar(45) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `is_schedule_updated` char(1) DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to N for new, M for Modified and D for deleted',
  `forvendor` varchar(1) DEFAULT NULL,
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_branch1_idx` (`branch_id`),
  KEY `fk_event_course_idx` (`course_id`),
  KEY `FK_event_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_event_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `fk_event_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;


--
-- Definition of table `event_group`
--

DROP TABLE IF EXISTS `event_group`;
CREATE TABLE `event_group` (
  `group_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to  N for new, M for Modified and D for deleted',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_event_group_group1_idx` (`group_id`),
  KEY `fk_event_group_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_group_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_group`
--

/*!40000 ALTER TABLE `event_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_group` ENABLE KEYS */;


--
-- Definition of table `fees`
--

DROP TABLE IF EXISTS `fees`;
CREATE TABLE `fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `head_id` int(11) NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `amount` varchar(45) CHARACTER SET latin1 NOT NULL,
  `duedate` date NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `standard_id` int(11) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `isemail` char(1) DEFAULT NULL,
  `issms` char(1) DEFAULT NULL,
  `isnotification` char(1) DEFAULT NULL,
  `frequency` varchar(45) NOT NULL,
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fees_branch_idx` (`branch_id`),
  KEY `fk_fees_caste_idx` (`caste_id`),
  KEY `FK_fees_standard` (`standard_id`),
  KEY `FK_fees_head` (`head_id`),
  KEY `FK_fees_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_fees_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_fees_head` FOREIGN KEY (`head_id`) REFERENCES `head` (`id`),
  CONSTRAINT `FK_fees_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fees_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fees_caste` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fees`
--

/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;


--
-- Definition of table `forgot_password_otp`
--

DROP TABLE IF EXISTS `forgot_password_otp`;
CREATE TABLE `forgot_password_otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otp` varchar(10) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `expired_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `fk_forgot_password_otp_user_id_idx` (`user_id`),
  CONSTRAINT `fk_forgot_password_otp_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forgot_password_otp`
--

/*!40000 ALTER TABLE `forgot_password_otp` DISABLE KEYS */;
/*!40000 ALTER TABLE `forgot_password_otp` ENABLE KEYS */;


--
-- Definition of table `goal_reward_type`
--

DROP TABLE IF EXISTS `goal_reward_type`;
CREATE TABLE `goal_reward_type` (
  `code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `description` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal_reward_type`
--

/*!40000 ALTER TABLE `goal_reward_type` DISABLE KEYS */;
INSERT INTO `goal_reward_type` (`code`,`description`) VALUES 
 ('AMOUNT','Amount will be given'),
 ('GIFT','Gift will be given');
/*!40000 ALTER TABLE `goal_reward_type` ENABLE KEYS */;


--
-- Definition of table `goal_status`
--

DROP TABLE IF EXISTS `goal_status`;
CREATE TABLE `goal_status` (
  `code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `description` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal_status`
--

/*!40000 ALTER TABLE `goal_status` DISABLE KEYS */;
INSERT INTO `goal_status` (`code`,`description`) VALUES 
 ('ACCEPTED','ACCEPTED goal completion request by parent'),
 ('ASSIGNED','ASSIGNED to Student'),
 ('COMPLETE','Completed goal.'),
 ('REJECTED','Rejected goal completion request by parent'),
 ('RESUBMIT','RESUBMIT rejected goal');
/*!40000 ALTER TABLE `goal_status` ENABLE KEYS */;


--
-- Definition of table `goals`
--

DROP TABLE IF EXISTS `goals`;
CREATE TABLE `goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET latin1 NOT NULL,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `reward_type` varchar(15) CHARACTER SET latin1 NOT NULL,
  `reward_amount` decimal(10,2) DEFAULT NULL,
  `reward_description` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `feedback` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `student_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status_code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parent_id_idx` (`parent_id`),
  KEY `fk_status_code_idx` (`status_code`),
  KEY `fk_goal_type_idx` (`reward_type`),
  CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `parent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reward_type` FOREIGN KEY (`reward_type`) REFERENCES `goal_reward_type` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_code` FOREIGN KEY (`status_code`) REFERENCES `goal_status` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goals`
--

/*!40000 ALTER TABLE `goals` DISABLE KEYS */;
/*!40000 ALTER TABLE `goals` ENABLE KEYS */;


--
-- Definition of table `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `chat_dialogue_id` varchar(45) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  `created_by` int(11) DEFAULT NULL,
  `is_public_group` char(1) DEFAULT 'N',
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group_branch1_idx` (`branch_id`),
  KEY `FK_group_userId` (`created_by`),
  KEY `FK_group_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_group_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_group_userId` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_group_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group`
--

/*!40000 ALTER TABLE `group` DISABLE KEYS */;
/*!40000 ALTER TABLE `group` ENABLE KEYS */;


--
-- Definition of table `group_forum`
--

DROP TABLE IF EXISTS `group_forum`;
CREATE TABLE `group_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `read_only` char(1) NOT NULL DEFAULT 'Y',
  `is_active` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `group_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `forum_group_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_group_id_idx` (`group_id`),
  KEY `branch_branch_id_idx` (`branch_id`),
  KEY `FK_group_forum_user` (`created_by`),
  KEY `FK_group_forum_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_group_forum_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_group_forum_user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `branch_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `group_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_forum`
--

/*!40000 ALTER TABLE `group_forum` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_forum` ENABLE KEYS */;


--
-- Definition of table `group_individual`
--

DROP TABLE IF EXISTS `group_individual`;
CREATE TABLE `group_individual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_groupindividual_groupid` (`group_id`),
  KEY `FK_groupindividual_userid` (`user_id`),
  KEY `FK_groupindividual_roleid` (`role_id`),
  CONSTRAINT `FK_groupindividual_groupid` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `FK_groupindividual_roleid` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_groupindividual_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_individual`
--

/*!40000 ALTER TABLE `group_individual` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_individual` ENABLE KEYS */;


--
-- Definition of table `group_standard`
--

DROP TABLE IF EXISTS `group_standard`;
CREATE TABLE `group_standard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_group_standard_group_1` (`group_id`),
  KEY `FK_group_standard_standard` (`standard_id`),
  KEY `FK_group_standard_division` (`division_id`),
  CONSTRAINT `FK_group_standard_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_group_standard_group_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `FK_group_standard_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_standard`
--

/*!40000 ALTER TABLE `group_standard` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_standard` ENABLE KEYS */;


--
-- Definition of table `group_tags`
--

DROP TABLE IF EXISTS `group_tags`;
CREATE TABLE `group_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `tag` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_grouptags_goup_id_idx` (`group_id`),
  CONSTRAINT `FK_grouptags_goup_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_tags`
--

/*!40000 ALTER TABLE `group_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_tags` ENABLE KEYS */;


--
-- Definition of table `group_user`
--

DROP TABLE IF EXISTS `group_user`;
CREATE TABLE `group_user` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `FK_groupuser_user` (`user_id`),
  CONSTRAINT `FK_groupuser_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `FK_groupuser_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_user`
--

/*!40000 ALTER TABLE `group_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_user` ENABLE KEYS */;


--
-- Definition of table `guardian_type`
--

DROP TABLE IF EXISTS `guardian_type`;
CREATE TABLE `guardian_type` (
  `guardianTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `guardianTypeName` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`guardianTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guardian_type`
--

/*!40000 ALTER TABLE `guardian_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `guardian_type` ENABLE KEYS */;


--
-- Definition of table `head`
--

DROP TABLE IF EXISTS `head`;
CREATE TABLE `head` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `branch_id` int(11) DEFAULT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`,`branch_id`) USING BTREE,
  KEY `FK_head_branchId` (`branch_id`),
  KEY `FK_head_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_head_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_head_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `head`
--

/*!40000 ALTER TABLE `head` DISABLE KEYS */;
/*!40000 ALTER TABLE `head` ENABLE KEYS */;


--
-- Definition of table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
CREATE TABLE `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_academic_year` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_holidays_currentYear` (`current_academic_year`),
  KEY `FK_holidays_branchId` (`branch_id`),
  CONSTRAINT `FK_holidays_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_holidays_currentYear` FOREIGN KEY (`current_academic_year`) REFERENCES `academic_year` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `holidays`
--

/*!40000 ALTER TABLE `holidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `holidays` ENABLE KEYS */;


--
-- Definition of table `institute`
--

DROP TABLE IF EXISTS `institute`;
CREATE TABLE `institute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `logo_url` varchar(255) DEFAULT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institute`
--

/*!40000 ALTER TABLE `institute` DISABLE KEYS */;
/*!40000 ALTER TABLE `institute` ENABLE KEYS */;


--
-- Definition of table `institute_branch`
--

DROP TABLE IF EXISTS `institute_branch`;
CREATE TABLE `institute_branch` (
  `instituteBranchId` int(11) NOT NULL AUTO_INCREMENT,
  `branchId` int(11) NOT NULL,
  `instituteId` int(11) NOT NULL,
  `active` char(1) DEFAULT NULL,
  PRIMARY KEY (`instituteBranchId`),
  KEY `fk_instituebranch_branch_idx` (`branchId`),
  KEY `fk_instituebranch_institute_idx` (`instituteId`),
  CONSTRAINT `fk_instituebranch_branch` FOREIGN KEY (`branchId`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instituebranch_institute` FOREIGN KEY (`instituteId`) REFERENCES `institute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institute_branch`
--

/*!40000 ALTER TABLE `institute_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `institute_branch` ENABLE KEYS */;


--
-- Definition of table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_description` varchar(100) DEFAULT NULL,
  `state_url` varchar(50) DEFAULT NULL,
  `menu_position` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_items`
--

/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`menu_id`,`menu_name`,`menu_description`,`state_url`,`menu_position`) VALUES 
 (1,'Institutes',NULL,'institute.list',1),
 (2,'Branch',NULL,'branch.list',2),
 (3,'Groups',NULL,'group.list',15),
 (4,'Standards',NULL,'standard.list',7),
 (5,'Division',NULL,'division.list',6),
 (6,'Teachers',NULL,'teacher.list',12),
 (7,'Vendors',NULL,'vendor.list',13),
 (8,'Students',NULL,'student.list',11),
 (9,'Fees',NULL,'fees.manage',10),
 (10,'Notice',NULL,'notice.list',17),
 (11,'Events',NULL,'event.list',16),
 (12,'Chat',NULL,'chat.list',19),
 (13,'Update Profile',NULL,'profile.teacher',20),
 (14,'Update Profile',NULL,'profile.vendor',21),
 (15,'Forum',NULL,'forum.list',18),
 (16,'Settings',NULL,'settings.email',8),
 (17,'Assign Permission',NULL,'role-access',14),
 (18,'UpdateProfile',NULL,'profile.student',22),
 (19,'About Us',NULL,'aboutus.page',23),
 (20,'Contact Us',NULL,'contactus.page',24),
 (21,'View Forum',NULL,'forum.view',25),
 (22,'Performance Report',NULL,'report.list',26),
 (23,'Head',NULL,'head.list',9),
 (24,'Dashboard',NULL,'dashboard.list',28),
 (25,'Academic Year',NULL,'academicyear.list',3),
 (26,'Holidays',NULL,'holiday.list',5),
 (27,'Working Days',NULL,'workingday.list',4),
 (28,'Fees Report',NULL,'feesreport.list',29),
 (29,'E-Diary',NULL,'ediary.list',30),
 (31,'Shopping',NULL,'shopping.view',31),
 (32,'Vendor Report',NULL,'vendorreport.list',27);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;


--
-- Definition of table `menu_permission`
--

DROP TABLE IF EXISTS `menu_permission`;
CREATE TABLE `menu_permission` (
  `menu_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `permission_name` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `rest_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`menu_permission_id`),
  KEY `fk_menu_id_permission` (`menu_id`),
  CONSTRAINT `fk_menu_id_permission` FOREIGN KEY (`menu_id`) REFERENCES `menu_items` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_permission`
--

/*!40000 ALTER TABLE `menu_permission` DISABLE KEYS */;
INSERT INTO `menu_permission` (`menu_permission_id`,`menu_id`,`permission_name`,`url`,`rest_url`) VALUES 
 (1,1,'Add Institute','institute.add','/school/institute/save'),
 (2,1,'Upload Institutes',NULL,NULL),
 (3,1,'Save',NULL,NULL),
 (4,1,'Cancel',NULL,NULL),
 (5,1,NULL,'institute.list','/school/institute'),
 (6,1,NULL,'login.logout',NULL),
 (7,1,NULL,'login.log',NULL),
 (8,2,NULL,'teacher.list','/school/teacher'),
 (9,2,NULL,NULL,'/school/teachers/getTeacherByBranch'),
 (10,10,NULL,'notice.list',NULL),
 (11,17,NULL,'role-access',NULL),
 (12,10,'Send Notice','notice.send','school/notice/save'),
 (13,10,'All Notice','notice.view',NULL),
 (14,9,NULL,'fees.manage',NULL),
 (15,9,'Add Fees','fees.add',NULL),
 (16,2,NULL,'branch.list','/school/branch'),
 (17,2,'Add Branch','branch.add','/school/branch'),
 (18,2,'Search',NULL,'school/masters/instituteMasters'),
 (19,2,'Upload Branch',NULL,NULL),
 (20,2,'Save',NULL,NULL),
 (21,2,'Edit','branch.edit',NULL),
 (22,2,'Cancel',NULL,NULL),
 (23,1,'Edit','institute.edit','/school/institute/save'),
 (24,6,'Add Teacher','teacher.add','school/teachers/save'),
 (25,6,'Upload Teachers',NULL,NULL),
 (26,6,'Save',NULL,NULL),
 (27,6,'Edit','teacher.edit','school/teachers/save'),
 (28,6,'Cancel',NULL,NULL),
 (29,6,NULL,'teacher.list',NULL),
 (30,6,NULL,NULL,'/school/teachers/getTeacherByBranch'),
 (31,6,NULL,NULL,'school/masters/instituteMasters'),
 (32,6,NULL,NULL,'school/masters/getFilterdBranch/{id}'),
 (33,3,'Add Group','group.add','school/groups'),
 (34,3,'Save',NULL,NULL),
 (35,3,'Edit','group.edit','school/groups'),
 (36,3,'Cancel',NULL,NULL),
 (37,3,NULL,'group.list',NULL),
 (38,4,'Add Standard','standard.add','school/standard'),
 (39,4,'Save',NULL,NULL),
 (40,4,'Edit','standard.edit','school/standard'),
 (41,4,'Cancel',NULL,NULL),
 (42,4,NULL,'standard.list',NULL),
 (43,5,'Add Division','division.add','school/division'),
 (44,5,'Save',NULL,NULL),
 (45,5,'Edit','division.edit','school/division'),
 (46,5,'Cancel',NULL,NULL),
 (47,5,NULL,'division.list',NULL),
 (48,7,'Add Vendor','vendor.add','school/vendors/save'),
 (49,7,'Save',NULL,NULL),
 (50,7,'Edit','vendor.edit','school/vendors/save'),
 (51,7,'Cancel',NULL,NULL),
 (52,7,NULL,'vendor.list',NULL),
 (53,8,'Add Student','student.add','school/students/save'),
 (54,8,'Upload Students',NULL,NULL),
 (55,8,'Save',NULL,NULL),
 (56,8,'Edit','student.edit','school/students/save'),
 (57,8,'Cancel',NULL,NULL),
 (58,8,NULL,'student.list',NULL),
 (59,9,'Edit','fees.edit',NULL),
 (60,9,'Cancel',NULL,NULL),
 (61,11,'Add Event','event.add','school/events/save'),
 (62,11,'Upload Events',NULL,NULL),
 (63,11,'Save',NULL,NULL),
 (64,11,'Edit','event.edit','school/events/save'),
 (65,11,'Cancel',NULL,NULL),
 (66,11,NULL,'event.list',NULL),
 (67,12,NULL,'chat.list',NULL),
 (68,15,'Add Forum','forum.add','school/forums'),
 (69,15,'Save',NULL,NULL),
 (70,15,'Edit','forum.edit','school/forums'),
 (71,15,'Cancel',NULL,NULL),
 (72,15,NULL,'forum.list',NULL),
 (73,4,NULL,NULL,'school/standard/getStandardByBranch'),
 (74,4,NULL,NULL,'school/masters/instituteMasters'),
 (75,4,NULL,NULL,'school/masters/getFilterdBranch/:id'),
 (76,13,'Update Profile','profile.teacher',NULL),
 (77,19,'About Us','aboutus.page',NULL),
 (78,16,'Email Setting','settings.email',NULL),
 (79,16,'Save',NULL,'school/settings/updateEmailSetting'),
 (80,20,NULL,'contactus.page',NULL),
 (81,21,NULL,'forum.view',NULL),
 (82,15,'View Forum','forum.view',NULL),
 (83,22,NULL,'report.list',NULL),
 (84,23,'Add Head','head.add','school/head'),
 (85,23,'Save',NULL,NULL),
 (86,23,'Edit','head.edit','school/save'),
 (87,23,'Cancel',NULL,NULL),
 (88,23,NULL,'head.list',NULL),
 (89,24,NULL,'dashboard.list',NULL),
 (90,25,'Add Academic Year','academicyear.add','school/academicyear'),
 (91,25,'Save',NULL,NULL),
 (92,25,'Edit','academicyear.edit','school/academicyear'),
 (93,25,'Cancel',NULL,NULL),
 (94,25,NULL,'academicyear.list',NULL),
 (96,26,'Add Holiday','holiday.add','school/holiday'),
 (97,26,'Save',NULL,NULL),
 (98,26,'Edit','holiday.edit','school/holiday'),
 (99,26,'Cancel',NULL,NULL),
 (100,26,NULL,'holiday.list',NULL),
 (101,27,'Add Working Days','workingday.add','school/workingday'),
 (102,27,'Save',NULL,NULL),
 (103,27,'Edit','workingday.edit','school/workingday'),
 (104,27,'Cancel',NULL,NULL),
 (105,27,NULL,'workingday.list',NULL),
 (106,28,NULL,'feesreport.list',NULL),
 (107,29,'Add E-Diary','ediary.add',NULL),
 (108,29,'Save',NULL,NULL),
 (109,29,'view','ediary.view',NULL),
 (110,29,'Cancel',NULL,NULL),
 (111,29,NULL,'ediary.list',NULL),
 (112,31,'View','shopping.view',NULL),
 (113,32,NULL,'vendorreport.list',NULL);
/*!40000 ALTER TABLE `menu_permission` ENABLE KEYS */;


--
-- Definition of table `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL COMMENT 'High and Low',
  `date` datetime NOT NULL,
  `image_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `detail_description` text,
  `sent_by` int(11) NOT NULL COMMENT 'user id ',
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notification_branch1_idx` (`branch_id`),
  KEY `FK_notice_user` (`sent_by`),
  KEY `FK_notice_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_notice_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_notice_user` FOREIGN KEY (`sent_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_notice_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;


--
-- Definition of table `notice_group`
--

DROP TABLE IF EXISTS `notice_group`;
CREATE TABLE `notice_group` (
  `notice_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`notice_id`,`group_id`),
  KEY `fk_noticegroup_notice_idx` (`notice_id`),
  KEY `fk_noticegroup_group_idx` (`group_id`),
  CONSTRAINT `fk_noticegroup_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_noticegroup_notice` FOREIGN KEY (`notice_id`) REFERENCES `notice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notice_group`
--

/*!40000 ALTER TABLE `notice_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice_group` ENABLE KEYS */;


--
-- Definition of table `notice_individual`
--

DROP TABLE IF EXISTS `notice_individual`;
CREATE TABLE `notice_individual` (
  `notice_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`notice_id`,`user_id`),
  KEY `fk_noticeindividual_noticeId_idx` (`notice_id`),
  KEY `fk_noticeindividual_userid_idx` (`user_id`),
  CONSTRAINT `fk_noticeindividual_noticeId` FOREIGN KEY (`notice_id`) REFERENCES `notice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_noticeindividual_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notice_individual`
--

/*!40000 ALTER TABLE `notice_individual` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice_individual` ENABLE KEYS */;


--
-- Definition of table `otp`
--

DROP TABLE IF EXISTS `otp`;
CREATE TABLE `otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otp` varchar(6) NOT NULL,
  `generatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `primaryContact` varchar(25) NOT NULL,
  `active` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp`
--

/*!40000 ALTER TABLE `otp` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp` ENABLE KEYS */;


--
-- Definition of table `parent`
--

DROP TABLE IF EXISTS `parent`;
CREATE TABLE `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `middlename` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `primaryContact` varchar(10) DEFAULT NULL,
  `secondaryContact` varchar(10) DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `pincode` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `taluka_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_parent_user1_idx` (`user_id`),
  KEY `fk_parent_taluka1_idx` (`taluka_id`),
  KEY `fk_parent_district1_idx` (`district_id`),
  KEY `fk_parent_state1_idx` (`state_id`),
  KEY `fk_parent_caste1_idx` (`caste_id`),
  KEY `fk_parent_religion1_idx` (`religion_id`),
  CONSTRAINT `fk_parent_caste1` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_religion1` FOREIGN KEY (`religion_id`) REFERENCES `religion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_state1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_taluka1` FOREIGN KEY (`taluka_id`) REFERENCES `taluka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parent`
--

/*!40000 ALTER TABLE `parent` DISABLE KEYS */;
/*!40000 ALTER TABLE `parent` ENABLE KEYS */;


--
-- Definition of trigger `parent_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `parent_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `parent_insert_trig` AFTER INSERT ON `parent` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

IF NOT EXISTS (SELECT id FROM contact WHERE user_id = NEW.user_id) THEN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.email, NEW.primaryContact, NEW.user_id, NEW.firstname, NEW.lastname, NEW.city, NEW.photo);
END IF;

END $$

DELIMITER ;

--
-- Definition of trigger `parent_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `parent_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `parent_update_trig` AFTER UPDATE ON `parent` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email, phone =NEW.primaryContact, photo = NEW.photo, 
firstname= NEW.firstname, lastname=NEW.lastname, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;

--
-- Definition of table `passbook`
--

DROP TABLE IF EXISTS `passbook`;
CREATE TABLE `passbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `date` date NOT NULL,
  `is_debit` char(1) CHARACTER SET latin1 NOT NULL,
  `is_credit` char(45) CHARACTER SET latin1 NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_transaction_id` int(11) DEFAULT NULL,
  `for_entity_name` varchar(100) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `for_entity_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_created_by_user_id_idx` (`user_id`),
  KEY `fk_parent_transaction_id_idx` (`parent_transaction_id`),
  KEY `fk_for_entity_user_id_idx` (`for_entity_user_id`),
  CONSTRAINT `fk_created_by_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_for_entity_user_id` FOREIGN KEY (`for_entity_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_transaction_id` FOREIGN KEY (`parent_transaction_id`) REFERENCES `passbook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passbook`
--

/*!40000 ALTER TABLE `passbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `passbook` ENABLE KEYS */;


--
-- Definition of table `payment_account_type`
--

DROP TABLE IF EXISTS `payment_account_type`;
CREATE TABLE `payment_account_type` (
  `code` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_account_type`
--

/*!40000 ALTER TABLE `payment_account_type` DISABLE KEYS */;
INSERT INTO `payment_account_type` (`code`,`description`) VALUES 
 ('BANK','Bank account'),
 ('PAYPAL','Paypal Account'),
 ('QFIXPAY','Qfix payment account');
/*!40000 ALTER TABLE `payment_account_type` ENABLE KEYS */;


--
-- Definition of table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `parent_id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `paid_date` datetime NOT NULL,
  PRIMARY KEY (`parent_id`,`fees_id`,`student_id`),
  KEY `parent_id_idx` (`parent_id`),
  KEY `fees_id_fk_idx` (`student_id`),
  KEY `fk_idx` (`fees_id`),
  CONSTRAINT `fees_id_fk` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parent_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `parent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_id_fk` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;


--
-- Definition of table `personal_task`
--

DROP TABLE IF EXISTS `personal_task`;
CREATE TABLE `personal_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `venue` varchar(100) DEFAULT NULL,
  `startdate` date NOT NULL,
  `enddate` date DEFAULT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `alldayevent` char(1) DEFAULT 'N',
  `reminder` char(1) DEFAULT 'N',
  `reminderbefore` int(10) DEFAULT NULL COMMENT 'reminder before will be stored in minutes basis',
  `forstudent` varchar(1) DEFAULT 'N',
  `student_id` int(11) DEFAULT NULL,
  `recurring` char(1) DEFAULT 'N',
  `frequency` varchar(15) DEFAULT NULL,
  `by_day` varchar(45) DEFAULT NULL,
  `by_date` varchar(45) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `is_schedule_updated` char(1) DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_student_id_idx` (`student_id`),
  KEY `fk_role_id_idx` (`role_id`),
  CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personal_task`
--

/*!40000 ALTER TABLE `personal_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_task` ENABLE KEYS */;


--
-- Definition of table `religion`
--

DROP TABLE IF EXISTS `religion`;
CREATE TABLE `religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `religion`
--

/*!40000 ALTER TABLE `religion` DISABLE KEYS */;
INSERT INTO `religion` (`id`,`code`,`name`,`active`) VALUES 
 (1,'HN','Hindu','Y');
/*!40000 ALTER TABLE `religion` ENABLE KEYS */;


--
-- Definition of table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`,`name`) VALUES 
 (1,'ADMIN'),
 (2,'STUDENT'),
 (3,'PARENT'),
 (4,'TEACHER'),
 (5,'VENDOR'),
 (6,'SCHOOL ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


--
-- Definition of table `role_menu`
--

DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`,`role_id`),
  KEY `fk_role_menu_role_id` (`role_id`),
  CONSTRAINT `fk_role_menu_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu_items` (`menu_id`),
  CONSTRAINT `fk_role_menu_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_menu`
--

/*!40000 ALTER TABLE `role_menu` DISABLE KEYS */;
INSERT INTO `role_menu` (`role_id`,`menu_id`) VALUES 
 (1,1),
 (1,2),
 (1,3),
 (1,4),
 (1,5),
 (1,6),
 (1,7),
 (1,8),
 (1,9),
 (1,10),
 (1,11),
 (1,12),
 (1,15),
 (1,16),
 (1,17),
 (1,22),
 (1,23),
 (1,25),
 (1,26),
 (1,27),
 (1,32),
 (2,1),
 (4,8),
 (4,10),
 (4,12),
 (4,13),
 (4,15),
 (4,22),
 (4,29),
 (4,31),
 (6,2),
 (6,3),
 (6,4),
 (6,5),
 (6,6),
 (6,7),
 (6,8),
 (6,9),
 (6,10),
 (6,11),
 (6,12),
 (6,15),
 (6,22),
 (6,23),
 (6,25),
 (6,26),
 (6,27),
 (6,28);
/*!40000 ALTER TABLE `role_menu` ENABLE KEYS */;


--
-- Definition of table `role_menu_permission`
--

DROP TABLE IF EXISTS `role_menu_permission`;
CREATE TABLE `role_menu_permission` (
  `menu_permission_id` int(11) NOT NULL,
  `role_id` varchar(50) NOT NULL,
  PRIMARY KEY (`menu_permission_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_menu_permission`
--

/*!40000 ALTER TABLE `role_menu_permission` DISABLE KEYS */;
INSERT INTO `role_menu_permission` (`menu_permission_id`,`role_id`) VALUES 
 (1,'1'),
 (1,'2'),
 (2,'1'),
 (2,'2'),
 (3,'1'),
 (3,'2'),
 (4,'1'),
 (4,'2'),
 (5,'1'),
 (5,'2'),
 (6,'1'),
 (6,'2'),
 (6,'4'),
 (6,'6'),
 (7,'1'),
 (7,'2'),
 (7,'4'),
 (7,'6'),
 (8,'1'),
 (8,'2'),
 (9,'1'),
 (10,'1'),
 (10,'4'),
 (10,'6'),
 (11,'1'),
 (12,'1'),
 (12,'4'),
 (12,'6'),
 (13,'1'),
 (13,'4'),
 (13,'6'),
 (14,'1'),
 (14,'2'),
 (14,'6'),
 (15,'1'),
 (15,'6'),
 (16,'1'),
 (16,'6'),
 (17,'1'),
 (17,'6'),
 (18,'1'),
 (18,'6'),
 (19,'1'),
 (19,'6'),
 (20,'1'),
 (20,'6'),
 (21,'1'),
 (21,'6'),
 (22,'1'),
 (22,'6'),
 (23,'1'),
 (24,'1'),
 (24,'6'),
 (25,'1'),
 (25,'6'),
 (26,'1'),
 (26,'6'),
 (27,'1'),
 (27,'6'),
 (28,'1'),
 (28,'6'),
 (29,'1'),
 (29,'6'),
 (30,'1'),
 (30,'6'),
 (31,'1'),
 (31,'6'),
 (32,'1'),
 (32,'6'),
 (33,'1'),
 (33,'6'),
 (34,'1'),
 (34,'6'),
 (35,'1'),
 (35,'6'),
 (36,'1'),
 (36,'6'),
 (37,'1'),
 (37,'6'),
 (38,'1'),
 (38,'6'),
 (39,'1'),
 (39,'6'),
 (40,'1'),
 (40,'6'),
 (41,'1'),
 (41,'6'),
 (42,'1'),
 (42,'6'),
 (43,'1'),
 (43,'6'),
 (44,'1'),
 (44,'6'),
 (45,'1'),
 (45,'6'),
 (46,'1'),
 (46,'6'),
 (47,'1'),
 (47,'6'),
 (48,'1'),
 (48,'6'),
 (49,'1'),
 (49,'6'),
 (50,'1'),
 (50,'6'),
 (51,'1'),
 (51,'6'),
 (52,'1'),
 (52,'6'),
 (53,'1'),
 (53,'4'),
 (53,'6'),
 (54,'1'),
 (54,'6'),
 (55,'1'),
 (55,'4'),
 (55,'6'),
 (56,'1'),
 (56,'4'),
 (56,'6'),
 (57,'1'),
 (57,'4'),
 (57,'6'),
 (58,'1'),
 (58,'4'),
 (58,'6'),
 (59,'1'),
 (59,'6'),
 (60,'1'),
 (60,'6'),
 (61,'1'),
 (61,'6'),
 (62,'1'),
 (62,'6'),
 (63,'1'),
 (63,'6'),
 (64,'1'),
 (64,'6'),
 (65,'1'),
 (65,'6'),
 (66,'1'),
 (66,'6'),
 (67,'1'),
 (67,'4'),
 (67,'6'),
 (68,'1'),
 (68,'4'),
 (68,'6'),
 (69,'1'),
 (69,'4'),
 (69,'6'),
 (70,'1'),
 (70,'4'),
 (70,'6'),
 (71,'1'),
 (71,'4'),
 (71,'6'),
 (72,'1'),
 (72,'4'),
 (72,'6'),
 (73,'6'),
 (74,'6'),
 (75,'6'),
 (76,'4'),
 (77,'1'),
 (77,'4'),
 (77,'6'),
 (78,'1'),
 (79,'1'),
 (80,'1'),
 (80,'4'),
 (80,'6'),
 (81,'4'),
 (82,'1'),
 (82,'4'),
 (82,'6'),
 (83,'1'),
 (83,'4'),
 (83,'6'),
 (84,'1'),
 (84,'6'),
 (85,'1'),
 (85,'6'),
 (86,'1'),
 (86,'6'),
 (87,'1'),
 (87,'6'),
 (88,'1'),
 (88,'6'),
 (89,'6'),
 (90,'1'),
 (90,'6'),
 (91,'1'),
 (91,'6'),
 (92,'1'),
 (92,'6'),
 (93,'1'),
 (93,'6'),
 (94,'1'),
 (94,'6'),
 (96,'1'),
 (96,'6'),
 (97,'1'),
 (97,'6'),
 (98,'1'),
 (98,'6'),
 (99,'1'),
 (99,'6'),
 (100,'1'),
 (100,'6'),
 (101,'1'),
 (101,'6'),
 (102,'1'),
 (102,'6'),
 (103,'1'),
 (103,'6'),
 (104,'1'),
 (104,'6'),
 (105,'1'),
 (105,'6'),
 (106,'6'),
 (107,'4'),
 (107,'6'),
 (108,'4'),
 (108,'6'),
 (109,'4'),
 (109,'6'),
 (110,'4'),
 (110,'6'),
 (111,'4'),
 (111,'6'),
 (112,'4'),
 (113,'1'),
 (113,'6');
/*!40000 ALTER TABLE `role_menu_permission` ENABLE KEYS */;


--
-- Definition of table `school_admin`
--

DROP TABLE IF EXISTS `school_admin`;
CREATE TABLE `school_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) NOT NULL,
  `dateOfBirth` varchar(45) DEFAULT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `primaryContact` varchar(45) NOT NULL,
  `secondaryContact` varchar(45) DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `pinCode` varchar(45) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` varchar(45) NOT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_schooladminuser_1_idx` (`user_id`),
  CONSTRAINT `FK_schooladminuser_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school_admin`
--

/*!40000 ALTER TABLE `school_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_admin` ENABLE KEYS */;


--
-- Definition of table `sms_config`
--

DROP TABLE IF EXISTS `sms_config`;
CREATE TABLE `sms_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendorUrl` varchar(255) NOT NULL,
  `username` varchar(65) NOT NULL,
  `password` varchar(45) NOT NULL,
  `sid` varchar(45) NOT NULL,
  `flash_message` varchar(1) NOT NULL DEFAULT 'N',
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_config`
--

/*!40000 ALTER TABLE `sms_config` DISABLE KEYS */;
INSERT INTO `sms_config` (`id`,`vendorUrl`,`username`,`password`,`sid`,`flash_message`,`active`) VALUES 
 (1,'http://cloud.smsindiahub.in/vendorsms/pushsms.aspx?','v2stech','XperiaSms@987','WEBSMS','0','Y');
/*!40000 ALTER TABLE `sms_config` ENABLE KEYS */;


--
-- Definition of table `standard`
--

DROP TABLE IF EXISTS `standard`;
CREATE TABLE `standard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `displayName` varchar(45) NOT NULL,
  `code` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_standard_branch_1` (`branch_id`),
  KEY `FK_standard_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_standard_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_standard_branch_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standard`
--

/*!40000 ALTER TABLE `standard` DISABLE KEYS */;
/*!40000 ALTER TABLE `standard` ENABLE KEYS */;


--
-- Definition of table `standard_division`
--

DROP TABLE IF EXISTS `standard_division`;
CREATE TABLE `standard_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_standard_division_branch` (`branch_id`),
  KEY `FK_standard_division_standard` (`standard_id`),
  KEY `FK_standard_division_division` (`division_id`),
  CONSTRAINT `FK_standard_division_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_standard_division_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_standard_division_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standard_division`
--

/*!40000 ALTER TABLE `standard_division` DISABLE KEYS */;
/*!40000 ALTER TABLE `standard_division` ENABLE KEYS */;


--
-- Definition of table `state`
--

DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` (`id`,`code`,`name`,`active`) VALUES 
 (1,'NH','Maharashtra','Y');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;


--
-- Definition of table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `father_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mother_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dob` date NOT NULL,
  `relation_with_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email_address` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mobile` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `registration_code` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `line1` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `pincode` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `religion` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `standard_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `roll_number` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_child` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `hobbies` varchar(100) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `user_id` int(11) NOT NULL,
  `student_upload_id` int(11) DEFAULT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_student_branch_idx` (`branch_id`),
  KEY `fk_student_casteId_idx` (`caste_id`),
  KEY `FK_student_uploadstudent` (`student_upload_id`),
  KEY `FK_student_standard` (`standard_id`),
  KEY `FK_student_division` (`division_id`),
  CONSTRAINT `FK_student_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_student_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_student_uploadstudent` FOREIGN KEY (`student_upload_id`) REFERENCES `student_upload` (`id`),
  CONSTRAINT `fk_student_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_casteId` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

/*!40000 ALTER TABLE `student` DISABLE KEYS */;
/*!40000 ALTER TABLE `student` ENABLE KEYS */;


--
-- Definition of trigger `student_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `student_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `student_insert_trig` AFTER INSERT ON `student` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.email_address, NEW.mobile, NEW.user_id, NEW.first_name, NEW.last_name, NEW.city, NEW.photo);


END $$

DELIMITER ;

--
-- Definition of trigger `student_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `student_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `student_update_trig` AFTER UPDATE ON `student` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email_address, phone =NEW.mobile, photo = NEW.photo, 
firstname= NEW.first_name, lastname=NEW.last_name, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;

--
-- Definition of table `student_event`
--

DROP TABLE IF EXISTS `student_event`;
CREATE TABLE `student_event` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'N' COMMENT 'Set it  to N for new, M for Modified and D for deleted',
  KEY `fk_event_student_event1_idx` (`event_id`),
  KEY `fk_event_user_event1_idx` (`user_id`),
  CONSTRAINT `fk_event_student_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_user_event1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_event`
--

/*!40000 ALTER TABLE `student_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_event` ENABLE KEYS */;


--
-- Definition of table `student_group`
--

DROP TABLE IF EXISTS `student_group`;
CREATE TABLE `student_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `joined` char(1) DEFAULT 'N' COMMENT 'if value set to ''N'' means group is suggested group.\nelse if value set to ''Y'' means group is joined by student.',
  `group_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_student_group_group1_idx` (`group_id`),
  KEY `fk_student_group_student1_idx` (`student_id`),
  CONSTRAINT `FK_student_group_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `fk_student_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_group`
--

/*!40000 ALTER TABLE `student_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_group` ENABLE KEYS */;


--
-- Definition of table `student_marks`
--

DROP TABLE IF EXISTS `student_marks`;
CREATE TABLE `student_marks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `theory` varchar(45) DEFAULT NULL,
  `practical` varchar(45) DEFAULT NULL,
  `obtained` varchar(45) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  `student_result_id` int(11) NOT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_student_marks_student` (`student_id`),
  KEY `FK_student_marks_subject` (`subject_id`),
  KEY `FK_student_marks_student_result` (`student_result_id`),
  CONSTRAINT `FK_student_marks_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `FK_student_marks_student_result` FOREIGN KEY (`student_result_id`) REFERENCES `student_result` (`id`),
  CONSTRAINT `FK_student_marks_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_marks`
--

/*!40000 ALTER TABLE `student_marks` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_marks` ENABLE KEYS */;


--
-- Definition of table `student_result`
--

DROP TABLE IF EXISTS `student_result`;
CREATE TABLE `student_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `result` varchar(45) DEFAULT NULL,
  `final_grade` varchar(45) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  `general_remark` varchar(100) DEFAULT NULL,
  `gross_total` varchar(45) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_student_result_student` (`student_id`),
  KEY `FK_student_result_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_student_result_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_student_result_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_result`
--

/*!40000 ALTER TABLE `student_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_result` ENABLE KEYS */;


--
-- Definition of table `student_upload`
--

DROP TABLE IF EXISTS `student_upload`;
CREATE TABLE `student_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `father_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mother_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dob` date NOT NULL,
  `relation_with_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `mobile` varchar(10) CHARACTER SET latin1 NOT NULL,
  `registration_code` varchar(10) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(10) NOT NULL,
  `line1` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `pincode` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `religion` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `standard_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `roll_number` varchar(45) CHARACTER SET latin1 NOT NULL,
  `goal_child` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `branch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `fk_studentupload_branchId_idx` (`branch_id`),
  KEY `fk_studentupload_userId_idx` (`user_id`),
  KEY `fk_studentupload_casteId_idx` (`caste_id`),
  KEY `fk_student_upload_standard` (`standard_id`),
  KEY `fk_student_upload_division` (`division_id`),
  CONSTRAINT `fk_student_upload_division` FOREIGN KEY (`division_id`) REFERENCES `division` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_upload_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_casteId` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_userId` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_upload`
--

/*!40000 ALTER TABLE `student_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_upload` ENABLE KEYS */;


--
-- Definition of table `subject`
--

DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `semester` varchar(45) NOT NULL,
  `name` varchar(65) NOT NULL,
  `total_marks` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `academic_year_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_subject_branch` (`branch_id`),
  KEY `FK_subject_standard` (`standard_id`),
  KEY `FK_subject_academicYear` (`academic_year_id`),
  CONSTRAINT `FK_subject_academicYear` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_subject_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_subject_standard` FOREIGN KEY (`standard_id`) REFERENCES `standard` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;


--
-- Definition of table `taluka`
--

DROP TABLE IF EXISTS `taluka`;
CREATE TABLE `taluka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `district_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DISTRICT_idx` (`district_id`),
  CONSTRAINT `FK_DISTRICT` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taluka`
--

/*!40000 ALTER TABLE `taluka` DISABLE KEYS */;
INSERT INTO `taluka` (`id`,`code`,`name`,`active`,`district_id`) VALUES 
 (1,'Kalyan','Kalyan','Y',1),
 (2,'Mumbai','Mumbai','Y',2);
/*!40000 ALTER TABLE `taluka` ENABLE KEYS */;


--
-- Definition of table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `emailAddress` varchar(45) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(1) CHARACTER SET latin1 NOT NULL,
  `primaryContact` varchar(45) CHARACTER SET latin1 NOT NULL,
  `secondaryContact` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pinCode` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `FK_teacher_1` (`user_id`),
  CONSTRAINT `FK_teacher_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher`
--

/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;


--
-- Definition of trigger `teacher_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `teacher_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `teacher_insert_trig` AFTER INSERT ON `teacher` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.emailAddress, NEW.primaryContact, NEW.user_id, NEW.firstName, NEW.lastName, NEW.city, NEW.photo);


END $$

DELIMITER ;

--
-- Definition of trigger `teacher_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `teacher_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `teacher_update_trig` AFTER UPDATE ON `teacher` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.emailAddress, phone =NEW.primaryContact, photo = NEW.photo, 
firstname= NEW.firstName, lastname=NEW.lastName, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;

--
-- Definition of table `token`
--

DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `token` varchar(30) CHARACTER SET latin1 NOT NULL,
  `created_date` datetime NOT NULL,
  `valid_for` int(11) NOT NULL DEFAULT '60' COMMENT 'Valid for in minutes after creation',
  `for_entity_id` int(11) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`token`),
  KEY `fk_token_role_id_idx` (`role_id`),
  CONSTRAINT `fk_token_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `token`
--

/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `institute_id` int(11) DEFAULT NULL,
  `gcm_id` varchar(255) DEFAULT NULL,
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  `authentication_type` varchar(45) DEFAULT 'APPLICATION',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `FK_user_institute` (`institute_id`),
  KEY `fk_authentication_type_idx` (`authentication_type`),
  CONSTRAINT `FK_user_institute` FOREIGN KEY (`institute_id`) REFERENCES `institute` (`id`),
  CONSTRAINT `fk_authentication_type` FOREIGN KEY (`authentication_type`) REFERENCES `authentication_type` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`,`username`,`password`,`active`,`institute_id`,`gcm_id`,`isDelete`,`authentication_type`) VALUES 
 (1,'admin','admin@556','Y',NULL,NULL,'N','APPLICATION');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


--
-- Definition of table `user_chat_profile`
--

DROP TABLE IF EXISTS `user_chat_profile`;
CREATE TABLE `user_chat_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `chat_user_profile_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `isDelete` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_user_profile_id_UNIQUE` (`chat_user_profile_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_chat_profile`
--

/*!40000 ALTER TABLE `user_chat_profile` DISABLE KEYS */;
INSERT INTO `user_chat_profile` (`id`,`login`,`password`,`email`,`chat_user_profile_id`,`user_id`,`active`,`isDelete`) VALUES 
 (1,'v2stech-sushant','Mumbai@556','sushant.sawant@v2stech.com',8831470,1,'Y','N');
/*!40000 ALTER TABLE `user_chat_profile` ENABLE KEYS */;


--
-- Definition of table `user_contact`
--

DROP TABLE IF EXISTS `user_contact`;
CREATE TABLE `user_contact` (
  `user_id` int(11) NOT NULL,
  `contact_user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`contact_user_id`),
  KEY `fk_user_contact_user_id_idx` (`user_id`),
  KEY `fk_user_contact_contact_user_id_idx` (`contact_user_id`),
  CONSTRAINT `fk_user_contact_contact_user_id` FOREIGN KEY (`contact_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_contact_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_contact`
--

/*!40000 ALTER TABLE `user_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_contact` ENABLE KEYS */;


--
-- Definition of table `user_ediary`
--

DROP TABLE IF EXISTS `user_ediary`;
CREATE TABLE `user_ediary` (
  `ediary_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ediary_id`,`user_id`),
  KEY `fk_ediaryindividual_ediary_idx` (`ediary_id`),
  KEY `fk_ediaryindividual_user_idx` (`user_id`),
  CONSTRAINT `fk_ediaryindividual_ediary` FOREIGN KEY (`ediary_id`) REFERENCES `ediary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediaryindividual_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_ediary`
--

/*!40000 ALTER TABLE `user_ediary` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_ediary` ENABLE KEYS */;


--
-- Definition of table `user_event`
--

DROP TABLE IF EXISTS `user_event`;
CREATE TABLE `user_event` (
  `user_id` int(11) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `entity_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `event_group_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to N for new, M for Modified and D for deleted',
  PRIMARY KEY (`id`),
  KEY `FK_userevent_user` (`user_id`),
  KEY `FK_userevent_role` (`role_id`),
  KEY `FK_userevent_event` (`event_id`),
  KEY `FK_userevent_eventgroup` (`event_group_id`),
  CONSTRAINT `FK_userevent_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `FK_userevent_eventgroup` FOREIGN KEY (`event_group_id`) REFERENCES `event_group` (`id`),
  CONSTRAINT `FK_userevent_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_userevent_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_event`
--

/*!40000 ALTER TABLE `user_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_event` ENABLE KEYS */;


--
-- Definition of table `user_forum_profile`
--

DROP TABLE IF EXISTS `user_forum_profile`;
CREATE TABLE `user_forum_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `forum_profile_id` int(11) NOT NULL,
  `is_active` char(1) DEFAULT 'Y',
  `is_delete` char(1) DEFAULT 'N',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `forum_profile_id_UNIQUE` (`forum_profile_id`),
  KEY `fk_user_id_forum_profile` (`user_id`),
  CONSTRAINT `fk_user_id_forum_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_forum_profile`
--

/*!40000 ALTER TABLE `user_forum_profile` DISABLE KEYS */;
INSERT INTO `user_forum_profile` (`id`,`login`,`password`,`forum_profile_id`,`is_active`,`is_delete`,`user_id`) VALUES 
 (1,'admin','admin@556',0,'Y','N',1);
/*!40000 ALTER TABLE `user_forum_profile` ENABLE KEYS */;


--
-- Definition of table `user_mall_profile`
--

DROP TABLE IF EXISTS `user_mall_profile`;
CREATE TABLE `user_mall_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `shopping_profile_id` int(11) DEFAULT NULL,
  `is_active` varchar(1) NOT NULL DEFAULT 'Y',
  `is_delete` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_user_mall_profile_user` (`user_id`),
  CONSTRAINT `FK_user_mall_profile_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_mall_profile`
--

/*!40000 ALTER TABLE `user_mall_profile` DISABLE KEYS */;
INSERT INTO `user_mall_profile` (`id`,`user_id`,`username`,`password`,`shopping_profile_id`,`is_active`,`is_delete`) VALUES 
 (1,1,'v2stech','v2stech',0,'Y','N');
/*!40000 ALTER TABLE `user_mall_profile` ENABLE KEYS */;


--
-- Definition of table `user_payment_account`
--

DROP TABLE IF EXISTS `user_payment_account`;
CREATE TABLE `user_payment_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_number` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_account_user_id_idx` (`user_id`),
  CONSTRAINT `fk_payment_account_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_payment_account`
--

/*!40000 ALTER TABLE `user_payment_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_payment_account` ENABLE KEYS */;


--
-- Definition of table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_role_role_id` (`role_id`),
  CONSTRAINT `fk_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_id`,`role_id`) VALUES 
 (1,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;


--
-- Definition of table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_legal_name` varchar(100) NOT NULL,
  `marketing_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `area` varchar(56) NOT NULL,
  `city` varchar(56) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `selling_category` varchar(45) NOT NULL,
  `contact_first_name` varchar(45) NOT NULL,
  `contact_last_name` varchar(45) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `email` varchar(56) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_vendor_user_id_idx` (`user_id`),
  CONSTRAINT `fk_vendor_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor`
--

/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;


--
-- Definition of trigger `vendor_insert_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `vendor_insert_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `vendor_insert_trig` AFTER INSERT ON `vendor` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, city) 
values(NEW.email, NEW.mobile, NEW.user_id, NEW.merchant_legal_name, NEW.city);


END $$

DELIMITER ;

--
-- Definition of trigger `vendor_update_trig`
--

DROP TRIGGER /*!50030 IF EXISTS */ `vendor_update_trig`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `vendor_update_trig` AFTER UPDATE ON `vendor` FOR EACH ROW -- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email, phone =NEW.mobile,  
firstname= NEW.merchant_legal_name, city=NEW.city
where user_id = NEW.user_id;

END $$

DELIMITER ;

--
-- Definition of table `working_days`
--

DROP TABLE IF EXISTS `working_days`;
CREATE TABLE `working_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_academic_year` int(11) NOT NULL,
  `monday` char(1) DEFAULT NULL,
  `tuesday` char(1) DEFAULT NULL,
  `wednesday` char(1) DEFAULT NULL,
  `thursday` char(1) DEFAULT NULL,
  `friday` char(1) DEFAULT NULL,
  `saturday` char(1) DEFAULT NULL,
  `sunday` char(1) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_working_days_currentYear` (`current_academic_year`),
  KEY `FK_working_days_branchId` (`branch_id`),
  CONSTRAINT `FK_working_days_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_working_days_currentYear` FOREIGN KEY (`current_academic_year`) REFERENCES `academic_year` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- Added 23-04-2016 --
ALTER TABLE `isg_sit`.`user` MODIFY COLUMN `password` BINARY(20) NOT NULL,
 ADD COLUMN `password_salt` BINARY(8) NOT NULL AFTER `password`;
 
 ALTER TABLE `isg_sit`.`user` ADD COLUMN `temp_password` VARCHAR(45) AFTER `authentication_type`;

--End 23-04-2016 --
--
-- Dumping data for table `working_days`
--

/*!40000 ALTER TABLE `working_days` DISABLE KEYS */;
/*!40000 ALTER TABLE `working_days` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
