(function(){
	angular.module('dashboard').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('dashboard', {
			url : '/dashboard',
			abstract :true,
			controller : 'DashboardModelController',
			data: {
				 breadcrumbProxy: 'dashboard.list'
			}
		}).state('dashboard.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/dashboard/html/dashboard-list.html',
					controller : 'DashboardController'
				}
			},
			data: {
	            displayName: 'Dashboard'
	        }
		});
	}]);
})();