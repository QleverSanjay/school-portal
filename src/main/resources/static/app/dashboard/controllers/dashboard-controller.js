(function(){
	angular.module('dashboard').controller('DashboardController',function($rootScope,$resource,$confirm,$stateParams,$scope,$state,$uibModal,CommonServices) {
		$scope.dashboard = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};
		
		function filterTableByBranch(branchId){
			if(branchId){
				$resource('dashboard/getDashboardByBranch?branchId='+$scope.branchId).query().$promise.then(function(dashboardList) {
					$scope.dashboard.length = 0;
					if(dashboardList && dashboardList.length > 0){
						angular.extend($scope.dashboard,dashboardList);
					}					
	    		});
			}
		}
		
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				$scope.branchId = (!$stateParams.branchId ? branchList[0].id : $stateParams.branchId);
        				if($rootScope.roleId == 6 && branchList.length == 1){
        					$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;
        				}
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}
		
		
     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $stateParams.instituteId){
	    			$scope.instituteId = $stateParams.instituteId;
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}
    	
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};
		
		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
						$scope.branchId = (isBranchChanged ? branchList[0].id  : 
							(!$stateParams.branchId ? branchList[0].id : $stateParams.branchId));
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
					//		changeHeadList();
						}
        			}
					angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
/*
[{"parent_id":0,"otp":null,
	"personal_details":
	{"id":47,"name":"Mayuresh  pradhan","dob":"2012-02-14",
		"relation_with_parent":"","email_address":"mayuresh@eduqfix.com",
		"mobile":"6666666666","registration_code":"002","religion":"Hindu",
		"caste_id":2,"photo":"/images/Qfix/documents/External/no-images.png",
		"address":{"line_1":"Thane","area":"Thane","city":"Thane","pincode":"400125"},"gender":"M"},
		"education_details":{"branch_id":20,"standard":44,"division":51,"roll_number":"02"},
		"other_details":{"goal_child":"","goal_parent":"","hobbies":null},
		"student_upload_id":null,"user_id":124}];	
		
		
*/		$scope.personal_details = {};
		$scope.education_details = {};
		var personal_details = {
				"name": $scope.personal_details.name,
				"feedback": $scope.education_details.standard
		};
			$resource('http://localhost:8080/user-service/report/fees/dues/student/list',{}, {get: {
		        method: 'GET',
		      }})
			.get().$promise.then(function(resp) {
			angular.extend($scope.personal_details,resp.personal_details);
			angular.extend($scope.education_details,resp.education_details);
		});
		
		
		
		/*$scope.changeGraph = function (){
			changeGraph();
		};*/
		
	//	function changeGraph(){	
				  $scope.head = [];
				  $scope.statDesc = [];
				  $scope.data = [[], [], []];
				  $resource('http://localhost:8080/user-service/report/fees/dues/stats', {}, {get: {
				        method: 'GET',
				        
				  }}).get().$promise.then(function(response) {
			    	angular.extend($scope.head,response.head);
			    	angular.extend($scope.statDesc,response.statDesc);
					angular.extend($scope.data,response.data);
				});
		//	}
		
	});
})();

