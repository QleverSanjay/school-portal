(function(){
	angular.module('standardSubject').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('standardSubject', {
			url : '/standardSubject',
			abstract :true,
			controller : 'StandardSubjectModelController',
			data: {
				 breadcrumbProxy: 'standardSubject.list'
	        }
		}).state('standardSubject.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/standardSubject/html/standardSubject-list.html',
					controller : 'StandardSubjectController'
				}
			},
			data: {
	            displayName: 'StandardSubject List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		})
		.state('standardSubject.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/standardSubject/html/standardSubject-add.html',
					controller : 'StandardSubjectAddController'
				}
			},
			data: {
	            displayName: 'Add StandardSubject'
	        }
		}).state('standardSubject.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/standardSubject/html/standardSubject-add.html',
					controller : 'StandardSubjectEditController'
				}
			},
			data: {
	            displayName: 'Edit StandardSubject'
	        },
		});
	}]);
})();