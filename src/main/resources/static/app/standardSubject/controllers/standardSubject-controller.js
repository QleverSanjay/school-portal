(function(){
	angular.module('standardSubject').controller('StandardSubjectController',function($rootScope, $localStorage, 
			$resource,$filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state, modalPopupService,CommonServices) {

		$scope.standardSubjectList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isUploadingFailed = false;
		// Datatable sorting...
	    $scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", false)
		.withOption("paging", true)
		.withOption("searching", true)
        .withOption("responsive", true)
        .withOption("scrollX", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isUploadingFailed = false;
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('standardSubject/getStandardSubjectByBranch?branchId='+$scope.branchId).query().$promise.then(function(standardSubjectList) {
					$scope.standardSubjectList.length = 0;
					if(standardSubjectList && standardSubjectList.length > 0){
						angular.extend($scope.standardSubjectList,standardSubjectList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
		if($rootScope.roleId != 1 && $rootScope.roleId != 4){
    		$scope.instituteId = ($scope.instituteId ? $scope.instituteId : $rootScope.instituteId);
    		changeBranchList();
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
	        				if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

	    /** This method is used to open Modal */ 
	    $scope.openUploadStandardSubjectDialog = function(){
	    	  var options = {
	    	      templateUrl: 'app/standardSubject/html/standardSubject-bulk-upload.html',
	    	      controller: 'StandardSubjectFileUploadController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

	     $scope.deleteData = function deleteStandardSubject(id){
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('standardSubject').remove({id : id}).$promise.then(function(resp) {
        	 		if(resp.status == "success"){
        	 			for(var i =0; i < $scope.standardSubjectList.length ;i ++ ){
          			  		var standardSubject = $scope.standardSubjectList[i];
          			  		if(standardSubject.standardId == id){
          			  			$scope.standardSubjectList.splice(i , 1);
          			  			break;
          			  		}
          			  	}	
        	 		}
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});


	angular.module('standardSubject').controller('StandardSubjectAddController',function($rootScope,$localStorage, 
			$scope,$filter,$state, modalPopupService,$stateParams,$resource) {

		$scope.standardSubject = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.standardSubjectHeaderValue = "Add Standard Subjects";
		$scope.subjectList = [];
		$scope.standardList = [];
		$scope.standardSubject.subjectIdList = [];

		$scope.multiSelectSettings = {
		    smartButtonMaxItems: 3,
		    smartButtonTextConverter: function(itemText, originalItem) {
		        if (itemText === 'Jhon') {
		        	return 'Jhonny!';
		        }

		        return itemText;
		    },
		    enableSearch: true,
		    scrollableHeight: '300px',
		    scrollable: true
		};


		$scope.standardSubject.instituteId = $localStorage.selectedInstitute;
		$scope.standardSubject.branchId = $localStorage.selectedBranch;

		$scope.cancel = function(){
			$state.transitionTo('standardSubject.list',{reload : true});
	    };

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			changeStandardList();
        			changeSubjectList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.standardSubject.instituteId || $scope.standardSubject.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.standardSubject.instituteId})
					.$promise.then(function(branchList) {
					$scope.branchList.length = 0;
					if(branchList && branchList.length > 0){
						angular.extend($scope.branchList,branchList);
						$scope.standardSubject.branchId = ($scope.standardSubject.branchId ? $scope.standardSubject.branchId : branchList[0].id);
						changeStandardList();
						changeSubjectList();	
					}
				});	
			}
		};

		$scope.branchChanged = function(){
			changeStandardList();
			changeSubjectList();
		};

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.standardSubject.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.standardSubject.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;
					angular.extend($scope.standardList,standardList);
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		$scope.isSubjectRequired = false;
		$scope.isStandardRequired = false;
		$scope.isFirstTime = true;

		$scope.standardChanged = function (){
			checkSubjects();
		};
		
		function checkSubjects(){
			if($scope.standardSubject.standardId){
				$scope.isStandardRequired = false;
				$scope.isSubjectRequired = ($scope.standardSubject.subjectIdList.length == 0) ? true : false;
			}
			else if(!$scope.isFirstTime){
				$scope.isSubjectRequired = ($scope.standardSubject.subjectIdList.length == 0) ? true : false;
				$scope.isStandardRequired = true;
			}
			$scope.isFirstTime = false;
		}

		$scope.$watch("standardSubject.subjectIdList", function(event){
			checkSubjects();
		}, true);


		// Change SubjectList as per the standard selected.
		function changeSubjectList(){
			if($scope.standardSubject.branchId){
				$resource('masters/subjectMasters/:id').query({id : $scope.standardSubject.branchId}).$promise.then(
				 function(subjectList) {
					$scope.subjectList.length = 0;
					angular.extend($scope.subjectList,subjectList);
				});
			}
			else {
				$scope.subjectList.length = 0;
			}
		}

		$scope.save = function() {
			var subjectIdList = [];
			for(var i=0; i < $scope.standardSubject.subjectIdList.length ; i++){
				var selectedSubject = $scope.standardSubject.subjectIdList[i];
				subjectIdList.push(selectedSubject.id);
			}

			var standardSubject = {
				standardId: $scope.standardSubject.standardId,
				subjectIdList : subjectIdList,
				branchId : $scope.standardSubject.branchId
			};

			$resource('standardSubject').save(standardSubject).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('standardSubject.list',
						{
							instituteId : $scope.standardSubject.instituteId,
		        			branchId : $scope.standardSubject.branchId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
        		}
			});
		};
	});


	/** This method is used for uploading data */
	angular.module('standardSubject').controller('StandardSubjectFileUploadController',function($rootScope,$scope, 
			Upload, modalPopupService, $uibModalStack,$state) {
		$scope.uploadStandardSubject = function (file) {
	        Upload.upload({
	            url: 'standardSubject/uploadStandardSubjectFile',
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.standardSubjectList.length = 0;
	        	$scope.$parent.isUploadingFailed = (resp.data.status == 'fail' ? true : false);
	        	$scope.$parent.message = resp.data.message;
	        	angular.extend($scope.standardSubjectList,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
        	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});


	/*This Controller is Used for StandardSubject Edit functionality*/
	angular.module('standardSubject').controller('StandardSubjectEditController',function($rootScope,$scope, 
			$filter,$state,modalPopupService, $stateParams,$resource) {

		$scope.standardSubject = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.standardList = [];
		$scope.standardSubjectHeaderValue = "Edit Standard Subjects";
		$scope.previousStandardId = "";
		$scope.standardSubject.subjectIdList = [];
		$scope.subjectList = [];

		$resource('standardSubject/:id').get({id : $stateParams.id}).$promise.then(function(standardSubjectToEdit) {
			$scope.standardSubject = standardSubjectToEdit;
			$scope.previousStandardId = standardSubjectToEdit.standardId;
			$rootScope.setSelectedData($scope.standardSubject.instituteId, $scope.standardSubject.branchId);
			changeBranchList();
		});

		$scope.cancel = function(){
			$state.transitionTo('standardSubject.list',{reload : true});
	    };

		// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			changeStandardList();
			changeSubjectList();
			if($scope.standardSubject.instituteId || $scope.standardSubject.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.standardSubject.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.branchChanged = function(){
			changeStandardList();
			changeSubjectList();
		};

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.standardSubject.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.standardSubject.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;
					angular.extend($scope.standardList,standardList);
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change SubjectList as per the standard selected.
		function changeSubjectList(){
			if($scope.standardSubject.branchId){
				$resource('masters/subjectMasters/:id').query({id : $scope.standardSubject.branchId}).$promise.then(
				 function(subjectList) {
					$scope.subjectList.length = 0;
					angular.extend($scope.subjectList,subjectList);
					if($scope.subjectList.length > 0){
						for(var i =0; i < $scope.subjectList.length; i++){
							var subject = $scope.subjectList[i];
							for(var j = 0; j < $scope.standardSubject.subjectIdList.length; j++){
								if($scope.standardSubject.subjectIdList[j] == subject.id){
									subject.ticked = true;
									break;
								}
							}
						}
					}
				});
			}
			else {
				$scope.subjectList.length = 0;
			}
		}

		$scope.isSubjectRequired = false;
		$scope.isStandardRequired = false;
		$scope.isFirstTime = true;

		$scope.standardChanged = function (){
			checkSubjects();
		};

		function checkSubjects(){
			if($scope.standardSubject.standardId){
				$scope.isStandardRequired = false;
				$scope.isSubjectRequired = ($scope.standardSubject.subjectIdList.length == 0) ? true : false;
			}
			else if(!$scope.isFirstTime){
				$scope.isSubjectRequired = ($scope.standardSubject.subjectIdList.length == 0) ? true : false;
				$scope.isStandardRequired = true;
			}
			$scope.isFirstTime = false;
		}
		$scope.$watch("standardSubject.subjectIdList", function(event){
			checkSubjects();
		}, true);

		$scope.save = function() {
			var subjectIdList = [];
			for(var i=0; i < $scope.standardSubject.subjectIdList.length ; i++){
				var selectedSubject = $scope.standardSubject.subjectIdList[i];
				subjectIdList.push(selectedSubject.id);
			}

			var standardSubject = {
				previousStandardId : $scope.previousStandardId,
				standardId: $scope.standardSubject.standardId,
				subjectIdList : subjectIdList,
				branchId : $scope.standardSubject.branchId
			};

			if(subjectIdList.length > 0){
				$resource('standardSubject').save(standardSubject).$promise.then(function(resp) {
					if(resp.status == 'success'){	
	        			$state.transitionTo('standardSubject.list',
							{
								instituteId : $scope.standardSubject.instituteId,
			        			branchId : $scope.standardSubject.branchId,
			        			status : resp.status, 
			        			message : resp.message
							},{reload : true});
	        			}
	        		else{
						if(resp.isValidationError){
							$scope.validationErrMsg = resp.records.title;
							$scope.validationErrors = resp.records.errors;
							 var options = {
								 templateUrl: "app/common/html/validationError.html",
					    	      controller: "ValidationErrorController",
					    	      scope : $scope
					    	 };
							 modalPopupService.openPopup(options);
						}
						else {
							$scope.status = resp.status;
							$scope.message = resp.message;	
						}
	        		}
				});
			}
		};
	});
})();

