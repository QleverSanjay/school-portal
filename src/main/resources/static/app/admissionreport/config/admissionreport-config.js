(function(){
	angular.module('admissionreport').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('admissionreport', {
			url : '/admissionreport',
			abstract :true,
			controller : 'AdmissionReportModelController',
			data: {
				 breadcrumbProxy: 'admissionreport.list'
	        }
		}).state('admissionreport.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/admissionreport/html/admissionreport-list.html',
					controller : 'AdmissionReportController'
				}
			},
			data: {
	            displayName: 'Admission Report'
	        },
	        params : {
	        	message : ""
	        }
		});
	}]);
})();