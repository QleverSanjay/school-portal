(function(){
	angular.module('admissionreport').controller('AdmissionReportController',function($resource,$filter, 
			DTOptionsBuilder, $window, $localStorage, $http, $rootScope,
			userserviceapiurl,$stateParams,$scope,$state, $uibModal, $sce, admissionportalapiurl) {
		$scope.admissionreports = [];
		$scope.isEditMode = false;
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.courseList = [];
		$scope.academicYearList  = [];

		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true)
        .withOption("scrollX", true);

     	// get all institutes.
    	// Mainly used when login user is super admin. 
		if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				if($rootScope.roleId == 6 && branchList.length == 1){
        					$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;
        				}
        				changeBranchList(false);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

		$scope.changeBranchList = function(){
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		// on Branch changed get standadrd and heads as per branch
		$scope.branchChanged = function(){
			changeCourseList();
			changeAcademicYearList();
		};
		
		function changeAcademicYearList(){

			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}

		function changeCourseList(){
			if($scope.branchId){
				$resource(userserviceapiurl+'branch/'+$scope.branchId+'/online/admission/course/list?category=all').query().$promise.then(function(response) {
					$scope.courseList.length = 0;
					angular.extend($scope.courseList,response);
				});
			}
			else {
				$scope.courseList.length = 0;
			}
		}

		$scope.admissionReportList = [];
		$scope.markVerifyMsg = "";
		$scope.markVerifiedUnVerified = function(admission, isMarkVerify){
			$scope.markVerifyMsg = "";
			if(admission.application_id){
				var data= {
						application_id : admission.application_id,
						is_mark_verify : isMarkVerify
				};

				$http.put(userserviceapiurl+ "online/admission/verifyUnverifyAdmissionForm", data).success(function(resp) {
					$scope.markVerifyMsg = "Application form verify status updated.";
					admission.schoolVerified = isMarkVerify ? 'Y' : 'N';
                })
                .error(function(error) {
                	$scope.markVerifyMsg = "Problem while updating verify status.";
                });;
			}
		};

		$scope.editAdmissionForm = function (applicationId){
			var token = localStorage.getItem("token");
			var url = admissionportalapiurl + "onlineformprocess/editByAdmin?token="+token+"&applicationId="+ applicationId;

			url = url + ("&count="+new Date().getTime());
			$scope.editFormUrl = $sce.trustAsResourceUrl(url);

			$scope.isEditMode = true;
		};

		$scope.searchApplicantReports = function (){
			$scope.isEditMode = false;
			var qfix_token = localStorage.getItem("token");
			var url = userserviceapiurl+ "online/admission/report/search?branch_id="+$scope.branchId 
						+ "&course_id="+$scope.courseId
						+ "&academic_year_id="+$scope.academicYearId
						+ "&token="+qfix_token;
			$resource(url).query().$promise.then(function(admissionReportList) {
				$scope.admissionReportList.length = 0;
				if(admissionReportList){
					for(var i =0; i< admissionReportList.length; i++){
						var admission = admissionReportList[i];
						if(admission.contact_details){
							$scope.admissionReportList.push(admission);
						}
					}
				}
			});
		};

		function getValue(){
		    return window.localStorage.getItem('isFormSubmittedSuccessfully');
		}

		$scope.$watch(getValue, function(newValue){
		    if (newValue === "true"){
		        $scope.$apply(function(){ $scope.isEditMode = false;});
		    }
		});

		$scope.downloadAttachment = function (id, applicantName){
			window.location.href = userserviceapiurl+ "online/admission/attachments/"
				+ id + "?applicantName=" + applicantName;
		}

		$scope.exportApplicantReport = function (){
			var qfix_token = localStorage.getItem("token");
			var url = userserviceapiurl+ "online/admission/report/export?branch_id="+$scope.branchId 
						+ "&course_id="+$scope.courseId
						+ "&academic_year_id="+$scope.academicYearId
						+ "&token="+qfix_token;
			$window.location.href = url;
		};
	});

})();