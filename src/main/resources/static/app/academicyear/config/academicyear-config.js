(function(){
	angular.module('academicyear').config([ '$stateProvider', function($stateProvider) {
		
		$stateProvider.state('academicyear', {
			url : '/academicyear',
			abstract :true,
			controller : 'AcademicYearModelController',
			data: {
				 breadcrumbProxy: 'academicyear.list'
	        }
		}).state('academicyear.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/academicyear/html/academicyear-list.html',
					controller : 'AcademicYearController'
				}
			},
			data: {
	            displayName: 'Academic Year List'
	        },
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        	message : '',
	        	status : ''
	        }
		}).state('academicyear.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/academicyear/html/academicyear-add.html',
					controller : 'AcademicYearAddController'
				}
			},
			data: {
	            displayName: 'Add Academic Year'
	        }
		}).state('academicyear.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/academicyear/html/academicyear-add.html',
					controller : 'AcademicYearEditController'
				}
			},
			data: {
	            displayName: 'Edit Academic Year'
	        }
		});
	}]);
})();