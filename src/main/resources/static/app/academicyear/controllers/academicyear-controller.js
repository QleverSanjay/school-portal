(function(){
	angular.module('academicyear').controller('AcademicYearController',function($rootScope, $localStorage, $resource, $confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.academicYearList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	    $scope.activeYearFound= false;
	    $scope.isAllLoaded = false;

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isUploadingFailed = false;
		// Datatable sorting...

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isUploadingFailed = false;
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('academicyear/getAcademicYearByBranch?branchId='+$scope.branchId).query().$promise.then(function(academicYearList) {
					$scope.academicYearList.length = 0;

					if(academicYearList && academicYearList.length > 0){
						$scope.activeYearFound = false;
						$scope.isAllLoaded = false;
						for(var i =0; i <academicYearList.length; i++ ){
							var academicYearObj = academicYearList[i];
							if(academicYearObj.isCurrentActiveYear == 'Y'){
								$scope.activeYearFound = true;
							}
						}
						$scope.isAllLoaded = true;
						angular.extend($scope.academicYearList,academicYearList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				if(branchList.length == 1){
        					$scope.branchId = (!$scope.branchId ? branchList[0].id : $scope.branchId);
        					/* $scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;*/
        				}
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.branchId = (isBranchChanged ? branchList[0].id  : (!$scope.branchId ? branchList[0].id : $scope.branchId));
	        				if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

	     $scope.deleteData = function deleteFees(id){
	    	 $scope.message = '';
	    	 var canDelete = true;

	    	 if($scope.academicYearList){
	    		 for(var i =0; i < $scope.academicYearList.length; i++){
	    			 var year = $scope.academicYearList[i];
	    			 if(year.id == id && year.isCurrentActiveYear == 'Y'){
	    				 $scope.message = 'This is currently active year can`t delete.';
	    				 canDelete = false;
	    				 break;
	    			 }
	    		 }
	    	 }

	    	 if(canDelete){
	    		 $confirm({text: 'Are you sure you want to delete?'})
		         .then(function() {

		        	 	$resource('academicyear').remove({id : id}).$promise.then(function(resp) {
				        		for(var i =0; i < $scope.academicYearList.length ;i ++ ){
			      			  		var academicYear = $scope.academicYearList[i];
			      			  		if(academicYear.id == id){
			      			  			$scope.academicYearList.splice(i , 1);
			      			  			break;
			      			  		}
			      			  	}	
				        		$scope.status = resp.status;
				        		$scope.message = resp.message;
		      			});
		         });
	    	 }
	     }; 
	});

	
	angular.module('academicyear').controller('validateAcademicForm',function($resource,$scope,$state) {
		$scope.$watch('academicyear.fromDate', validateDates);
		$scope.$watch('academicyear.toDate', validateDates);
		
		function validateDates() {
		    if (!$scope.academicyear) return;
		    if ($scope.addAcademicYear.fromDate.$error.invalidDate || $scope.addAcademicYear.toDate.$error.invalidDate) {
		        $scope.addAcademicYear.fromDate.$setValidity("invalidDate", true);  //already invalid (per validDate directive)
		    } else {
		        //depending on whether the user used the date picker or typed it, this will be different (text or date type).  
		        //creating a new date object takes care of that. 
		        var toDate = new Date($scope.academicyear.toDate);
		        var fromDate = new Date($scope.academicyear.fromDate);
		         if(!isNaN(toDate) && !isNaN(fromDate)){
		        	$scope.addAcademicYear.fromDate.$setValidity("endBeforeStart", toDate >= fromDate);
		        }
		    }
		}
	});

	angular.module('academicyear').controller('AcademicYearAddController',function($rootScope, $confirm, $localStorage, $scope,$filter,
			$state,$stateParams,$resource,CommonServices) {

		$scope.academicyear = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isOpenStartDate = false;
		$scope.isOpenEndDate = false;
		//$scope.minDate = "";
		$scope.academicYearHeaderValue = "Add Academic Year";

		$scope.academicyear.instituteId = $localStorage.selectedInstitute;
		$scope.academicyear.branchId = $localStorage.selectedBranch;

		//$resource('academicyear/getFirstDateOfCurrentYear').get()
			//$promise.then(function(resp) {
				//$scope.minDate = new Date(resp.minDate);
		//});                                                           changes by mith
		
		
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				$scope.academicyear.branchId = ($scope.academicyear.branchId ? $scope.academicyear.branchId : branchList[0].id);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
    	

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
 
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.academicyear.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.academicyear.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.academicyear.branchId = ($scope.academicyear.branchId ? $scope.academicyear.branchId : branchList[0].id);
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		};
		$scope.openEndDateDialog = function fnOpenDialog(){
			$scope.isOpenEndDate = true;
		};
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('academicyear.list');
		};
		
		function saveYear(){
			var isCurrentActiveYear = ($scope.academicyear.isCurrentActiveYear == true || $scope.academicyear.isCurrentActiveYear == 'Y' ? "Y" : "N");
			var academicyear = {
					id : $scope.academicyear.id,
					branchId: $scope.academicyear.branchId,
					fromDate : $filter('date')($scope.academicyear.fromDate, "yyyy-MM-dd"),
					toDate : $filter('date')($scope.academicyear.toDate, "yyyy-MM-dd"),
					isCurrentActiveYear: isCurrentActiveYear
			};				

			$resource('academicyear').save(academicyear).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('academicyear.list',
						{
							instituteId : $scope.academicyear.instituteId,
		        			branchId : $scope.academicyear.branchId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		}

		$scope.save = function() {
			var isCurrentActiveYear = ($scope.academicyear.isCurrentActiveYear == true || $scope.academicyear.isCurrentActiveYear == 'Y' ? "Y" : "N");

			if(isCurrentActiveYear == 'Y'){
				$confirm({text: 'This will have an impact on system functionality, are you sure you want to proceed?'})
		         .then(function() {
		        	 saveYear();
		         });
			}
			else {
				saveYear();
			}
		};
	});

	/*This Controller is Used for Year Edit functionality*/
	angular.module('academicyear').controller('AcademicYearEditController',function($rootScope,$scope,$filter, $confirm, 
			$state,$stateParams,$resource, CommonServices) {
		$scope.academicyear = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isOpenStartDate = false;
		$scope.isOpenEndDate = false;
		//$scope.minDate = new Date();
		$scope.academicYearHeaderValue = "Edit Academic Year";

		$resource('academicyear/:id').get({id : $stateParams.id}).$promise.then(function(academicYearToEdit) {
			academicYearToEdit.fromDate = new Date(academicYearToEdit.fromDate);
			academicYearToEdit.toDate = new Date(academicYearToEdit.toDate);
			
			angular.extend($scope.academicyear,academicYearToEdit);
			$rootScope.setSelectedData($scope.academicyear.instituteId,$scope.academicyear.branchId);
			
			$scope.academicyear.isCurrentActiveYear = (academicYearToEdit.isCurrentActiveYear == 'Y' ? true : false);
			changeBranchList();
		});

		$scope.isOpenStartDate = false;
		$scope.isOpenEndDate = false;
		$scope.minDate = new Date();

		//$resource('academicyear/getFirstDateOfCurrentYear').get()
			//.$promise.then(function(resp) {
				//$scope.minDate = new Date(resp.minDate);
		//});      -----changes by mithil.

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
		
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.academicyear.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.academicyear.instituteId})
					.$promise.then(function(branchList) {
						/*if(branchList){
							$scope.fees.branchId = branchList[0].id;
	        			}*/
					
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
			
		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		};
		$scope.openEndDateDialog = function fnOpenDialog(){
			$scope.isOpenEndDate = true;
		};
		$scope.openDueDateDialog = function fnOpenDialog(){
			$scope.isOpenDueDate = true;
		};
		
		$scope.cancel = function(){
			$state.transitionTo('academicyear.list',{reload : true});
		};
		
		$scope.save = function() {
			
			 $confirm({text: 'This will have an impact on system functionality, are you sure you want to proceed?'})
	         .then(function() {

	 			var academicyear = {
	 					id : $scope.academicyear.id,
	 					branchId: $scope.academicyear.branchId,
	 					fromDate : $filter('date')($scope.academicyear.fromDate, "yyyy-MM-dd"),
	 					toDate : $filter('date')($scope.academicyear.toDate, "yyyy-MM-dd"),
	 					isCurrentActiveYear: ($scope.academicyear.isCurrentActiveYear == true ? "Y" : "N")
	 			};				
	 			//CommonServices.saveFees('fees',fees, 'fees.manage');
	 			$resource('academicyear').save(academicyear).$promise.then(function(resp) {
	 				if(resp.status == 'success'){
	 					$state.transitionTo('academicyear.list',
	 					{
	 						instituteId : $scope.academicyear.instituteId,
	 	        			branchId : $scope.academicyear.branchId,
	 	        			status : resp.status, 
	 	        			message : resp.message
	 					},{reload : true});
	 					}
	 				else{
	 		        		$scope.status = resp.status;
	 		        		$scope.message = resp.message;
	 		        	}
	 				});
	         });
			};
		});
	})();