(function(){	
	angular.module('settings').controller('validateSmsSettingForm',function($resource,$scope,$state) {
		$scope.regExVendorUrl = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
		$scope.regExPort = /^[0-9]{3,8}$/;
		$scope.regExPassword = /^[a-zA-Z0-9\-_$@]{3,40}$/;
		$scope.regExSid = /^[A-Z a-z]{2,10}$/;
		$scope.regExUsername = /^[a-zA-Z0-9.\-_$@]{3,25}$/;
	});
	
	
	angular.module('settings').controller('validateEmailSettingForm',function($resource,$scope,$state) {
		$scope.regExServerName = /^([a-z0-9]+\.)?[a-z0-9][a-z0-9-]*\.[a-z]{2,6}$/;
		$scope.regExPort = /^[0-9]{3,8}$/;
		$scope.regExConnectionType = /^[A-Z a-z]{2,10}$/;
		$scope.regExSecurity = /^[A-Z a-z]{2,10}$/;
		$scope.regExAuthenticationMethod = /^[A-Z a-z]{2,10}$/;
		$scope.regExUsername = /^[a-zA-Z0-9.\-_$@]{3,25}$/;
		$scope.regExPassword = /^[a-zA-Z0-9\-_$@]{3,40}$/;
	});

	angular.module('settings').controller('SmsSettingController',function($scope,$state,$rootScope, Upload,$resource) {
		/** This is written to get single setting object to display on the edit screen. */
		$scope.smsSetting = {};
		$scope.isUpdateSetting = true;
		$resource('settings/getSmsSetting').get({id : $scope.$rootScope.entityId}).$promise.then(function(setting) {
			angular.extend($scope.smsSetting,setting);
		});

		/** This method is used to save edited object. Here we explicitly copy $scope.settings to normal setting object. This is done because $scope.setting contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			var smsSetting = {
				id: $scope.smsSetting.id,
				vendorUrl: $scope.smsSetting.vendorUrl,
				username: $scope.smsSetting.username,
				password: $scope.smsSetting.password,
				sid: $scope.smsSetting.sid,
				flashMessage: $scope.smsSetting.flashMessage,
				active: $scope.smsSetting.active,
			};

			/** This method is used to adding event. It will only call add utility method */
			$resource('settings/updateSMSSetting').save(smsSetting).$promise.then(function(resp) {
				$scope.status = resp.status;
        		$scope.message = resp.message;
			});
		};
	});

	angular.module('settings').controller('EmailSettingController',function($scope,$state,Upload,$resource) {
		/** This is written to get single setting object to display on the edit screen. */
		$scope.emailSetting = {};
		$scope.isUpdateSetting = true;
		$resource('settings/getEmailSetting').get().$promise.then(function(setting) {
			angular.extend($scope.emailSetting,setting);
		});

		/** This method is used to save edited object. Here we explicitly copy $scope.settings to normal setting object. This is done because $scope.setting contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			var emailSetting = {
					id: $scope.emailSetting.id,
					serverName : $scope.emailSetting.serverName,
					port: $scope.emailSetting.port,
					connectionType: $scope.emailSetting.connectionType,
					security: $scope.emailSetting.security,
					authenticationMethod: $scope.emailSetting.authenticationMethod,
					username: $scope.emailSetting.username,
					active: $scope.emailSetting.active,
					password: $scope.emailSetting.password
			};

			$resource('settings/updateEmailSetting').save(emailSetting).$promise.then(function(resp) {
				$scope.status = resp.status;
        		$scope.message = resp.message;
			});
		};
	});
})();


