(function(){
	angular.module('settings').config(['$stateProvider', function($stateProvider) {
		$stateProvider.state('settings', {
			url : '/settings',
			abstract :true,
			controller : 'SettingModelController',
		}).state('settings.sms', {
			url : '/sms',
			views : {
				'@' : {
					templateUrl : 'app/settings/html/sms-setting-update.html',
					controller : 'SmsSettingController'
				}
			},
			data: {
	            displayName: 'Update SMS Setting'
	        },
		}).state('settings.email', {
			url : '/email',
			views : {
				'@' : {
					templateUrl : 'app/settings/html/email-setting-update.html',
					controller : 'EmailSettingController'
				}
			},
			data: {
	            displayName: 'Update Email Setting'
	        },
		});
	}]);
})();