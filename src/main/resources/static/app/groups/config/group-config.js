(function(){
	angular.module('group').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('group', {
			url : '/group',
			abstract :true,
			controller : 'GroupModelController',
			data: {
				 breadcrumbProxy: 'group.list'
			}
		}).state('group.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/groups/html/group-list.html',
					controller : 'GroupController'
				}
			},
			data: {
	            displayName: 'Group List'
	        },
	        params : {
	        	message : '',
	        	status : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('group.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/groups/html/group-add.html',
					controller : 'GroupAddController'
				}
			},
			data: {
	            displayName: 'Add Group'
	        },
		}).state('group.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/groups/html/group-add.html',
					controller : 'GroupEditController'
				}
			},
			data: {
	            displayName: 'Edit Group'
	        },
		});
	}]);
})();