(function() {
	angular.module('group').controller('GroupController',function($rootScope,$resource,$stateParams,$filter, $confirm,DTOptionsBuilder, 
			$localStorage, $scope, $state, modalPopupService,CommonServices) {
		$scope.groups = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;

	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch;

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
			filterTableByBranch($scope.branchId);
		};

		///if teacher Login
		if($rootScope.roleId == 4){
    		filterTableByTeacher($rootScope.id);
		}

		function filterTableByTeacher(teacherUserId){
			if(teacherUserId){
				$resource('groups/getGroupsByTeacher?teacherUserId='+teacherUserId).query().$promise.then(function(groupList) {
					$scope.groups.length = 0;
					if(groupList && groupList.length > 0){
						angular.extend($scope.groups,groupList);
					}
	    		});
			}
		}

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('groups/getGroupsByBranch?branchId='+$scope.branchId).query().$promise.then(function(groupList) {
					$scope.groups.length = 0;
					if(groupList && groupList.length > 0){
						angular.extend($scope.groups,groupList);
					}
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId == 6){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			$scope.branchList.length = 0;
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        				changeYearList();
        				if($stateParams.branchId){
        					filterTableByBranch($scope.branchId);
        				}
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
    			changeBranchList(false);
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
							$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
							if($stateParams.branchId){
	        					filterTableByBranch($scope.branchId);
	        				}
	        			}
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		

		/** This method is used to open Modal */
		$scope.uploadGroups = function() {
			var options = {
				templateUrl : 'app/groups/html/group-bulk-upload.html',
				controller : 'GroupFileUploadController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		$scope.deleteData = function deleteGroup(id) {
			$confirm({
				text : 'Are you sure you want to delete?'
			}).then(function() {
				$resource('groups').remove({id : id}).$promise.then(function(resp) {
					if(resp.status == 'success'){
						for(var i =0; i < $scope.groups.length ;i ++ ){
	      			  		var group = $scope.groups[i];
	      			  		if(group.id == id){
	      			  			$scope.groups.splice(i , 1);
	      			  			break;
	      			  		}
	      			  	}
					}	
					$scope.status = resp.status;
	        		$scope.message = resp.message;
				});
			});
		};
	});


	angular.module('group').controller('validateGroupForm',function($resource, $scope, $state) {
		$scope.regExGroup = /^[A-Z a-z 0-9 , . () @ -]{2,56}$/;
	});

	angular.module('group').controller('GroupAddController',function($rootScope,$scope,$filter,$stateParams, modalPopupService, $uibModalStack, 
			$state, Upload,$resource, CommonServices, $localStorage) {
		$scope.group = {};

		$scope.group.tags = [];

		$scope.hasSearchRecords = false;
		$scope.individualIds = [];
		$scope.group.individuals = [];
		$scope.group.standardDivisionArray = [];
		$scope.isOpenEndDate = false;
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSaveDisabled = false;
		$scope.indivisualStudentButtonValue = "Select";
		$scope.isIndividualOrStandardDivisionSelected = false;
		$scope.standardList = [];
		$scope.groupHeaderValue = "Add Group And Community";

		$scope.group.instituteId = $localStorage.selectedInstitute;
		$scope.group.branchId = $localStorage.selectedBranch;
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);

		$scope.$watch("group.isPublicGroup", function() {
			checkIsIndividualOrStandardDivisionSelected();
		}, true);

		$scope.$watch("group.individuals", function() {
			checkIsIndividualOrStandardDivisionSelected();
		}, true);

		function checkIsIndividualOrStandardDivisionSelected (){
			if ($scope.group.individuals && $scope.group.individuals.length > 0) {
				$scope.isIndividualOrStandardDivisionSelected = true;
			} else if ($scope.group.standardDivisionArray && $scope.group.standardDivisionArray.length > 0) {
				$scope.isIndividualOrStandardDivisionSelected = true;
			} else if ($scope.group.isPublicGroup){
				$scope.isIndividualOrStandardDivisionSelected = true;
			} 
			else {
				$scope.isIndividualOrStandardDivisionSelected = false;
				$scope.group.isPublicGroup = false;
			}
		}

		$scope.$watch("group.standardDivisionArray", function() {
			checkIsIndividualOrStandardDivisionSelected();
		}, true);
		
		$scope.branchChanged = function(){
			$scope.group.individuals.length = 0;
			$scope.group.standardDivisionArray.length = 0;
			changeStandardList();
		};
		
		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.group.branchId = $rootScope.branchId;
    		filteStandardsByTeacher();
    		changeYearList();
		}

		function filteStandardsByTeacher(){
			if($rootScope.id){
				$resource('masters/getFilterdStandardsByTeacher?branchId='+$scope.group.branchId+'&teacherId='+$rootScope.id)
					.query().$promise.then(function(standardList) {

					$scope.standardList.length = 0;
					if(standardList && standardList.length > 0){
						angular.extend($scope.standardList,standardList);
					}
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId == 6){
        	$scope.group.instituteId =  $rootScope.instituteId;
        	changeBranchList();
/*    		$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        			}
        			
        			changeYearList();
        			angular.extend($scope.branchList,branchList);
    		});*/
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.group.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.group.instituteId})
					.$promise.then(function(branchList) {
						changeYearList();
						angular.extend($scope.branchList,branchList);
						changeStandardList();
				});	
			}
		};

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.group.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.group.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					if(standardList != null && standardList.length > 0){
						$scope.group.standard = standardList[0].id;
        			}
					changeYearList();
					angular.extend($scope.standardList,standardList);
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}
		
		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.group.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.group.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.group.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

		$scope.popup = function() {
			var options = {
				templateUrl : 'app/groups/html/group-popup.html',
				controller : 'GroupSearchController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		/** Remove item from selected individuals */
		$scope.removeIndividual = function(id) {
			for ( var i = 0; i < $scope.group.individuals.length; i++) {
				var individual = $scope.group.individuals[i];
				if (individual.id == id) {
					$scope.group.individuals.splice(i, 1);
					$scope.individualIds.splice(i, 1);
					break;
				}
			}
		};

		$scope.standardDivisionButtonValue = "Select";

		$scope.popupStd = function() {
			var options = {
						templateUrl : 'app/groups/html/group-standard-popup.html',
						controller : 'StandardSearchController',
						scope : $scope
					};
			modalPopupService.openPopup(options);
		};

		/** Remove item from selected standard and division */
		$scope.removeStandard = function(standard, division) {
			for ( var i = 0; i < $scope.group.standardDivisionArray.length; i++) {
				var stdDiv = $scope.group.standardDivisionArray[i];
				if (stdDiv.standard == standard
						&& stdDiv.division == division) {
					$scope.group.standardDivisionArray.splice(
							i, 1);
					break;
				}
			}			
		};

		/**
		 * This method is used to adding group. It will only
		 * call add utility method
		 */
		$scope.save = function() {
			var group = {};
			$scope.isSaveDisabled = true;
			group = {
				id : $scope.group.id,
				name : $scope.group.name,
				description : $scope.group.description,
				branchId : $scope.group.branchId,
				status : $scope.group.status,
				standardDivision : $scope.group.standardDivisionArray,
				individualIdList : $scope.individualIds,
				tags : $scope.group.tags,
				createdBy :$rootScope.id,
				ownerRoleId : $rootScope.roleId,
				isPublicGroup : ($scope.group.isPublicGroup == true || $scope.group.isPublicGroup == 'Y' ? "Y" : "N"),
				academicYearId : $scope.group.academicYearId
			};

			
			$resource('groups').save(group).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('group.list', {
						status : resp.status, 
	        			message : resp.message,
	        			instituteId : $scope.group.instituteId,
	        			branchId : $scope.group.branchId,
					},{reload : true});
				}
				else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};

		/**
		 * This method is used to cancel add operation. It will
		 * only call cancel utility method
		 */
		$scope.cancelPopup = function() {
			modalPopupService.closePopup(null, $uibModalStack);
		};

		/**
		 * This method is used to cancel add operation. It will
		 * only call cancel utility method
		 */
		$scope.cancel = function() {
			CommonServices.cancelOperation('group.list');
		};
	});

	angular.module('group').controller('GroupEditController',function($rootScope,$scope, $state,$filter, modalPopupService, $localStorage, 
			$stateParams, $resource, Upload, CommonServices) {
		$scope.group = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.hasSearchRecords = false;
		$scope.individualIds = [];
		$scope.group.individuals = [];
		$scope.academicYearList = [];
		$scope.isOpenEndDate = false;
		$scope.isSaveDisabled = false;
		$scope.group.standardDivisionArray = [];
		$scope.standardList = [];
		$scope.group.tags = [];
		$scope.isIndividualOrStandardDivisionSelected = false;
		$scope.groupHeaderValue = "Edit Group And Community";

		$scope.$watch("group.isPublicGroup", function() {
			checkIsIndividualOrStandardDivisionSelected();
		}, true);

		$scope.$watch("group.individuals", function() {
			checkIsIndividualOrStandardDivisionSelected();
		}, true);

		function checkIsIndividualOrStandardDivisionSelected (){
			if ($scope.group.individuals && $scope.group.individuals.length > 0) {
				$scope.isIndividualOrStandardDivisionSelected = true;
			} else if ($scope.group.standardDivisionArray && $scope.group.standardDivisionArray.length > 0) {
				$scope.isIndividualOrStandardDivisionSelected = true;
			} else if ($scope.group.isPublicGroup){
				$scope.isIndividualOrStandardDivisionSelected = true;
			} 
			else {
				$scope.isIndividualOrStandardDivisionSelected = false;
				$scope.group.isPublicGroup = false;
			}
		}

		$scope.$watch("group.standardDivisionArray", function() {
			checkIsIndividualOrStandardDivisionSelected();
		}, true);
		
		$scope.branchChanged = function(){
			$scope.group.individuals.length = 0;
			$scope.group.standardDivisionArray.length = 0;
		};

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.group.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.group.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					if(standardList != null && standardList.length > 0){
						$scope.group.standard = standardList[0].id;
        			}
					changeYearList();
					angular.extend($scope.standardList,standardList);
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.group.branchId = $rootScope.branchId;
    		filteStandardsByTeacher();
		}

		function filteStandardsByTeacher(){
			if($rootScope.id){
				$resource('masters/getFilterdStandardsByTeacher?branchId='+$scope.group.branchId+'&teacherId='+$rootScope.id).query().$promise.then(function(standardList) {
					$scope.standardList.length = 0;
					if(standardList && standardList.length > 0){
						angular.extend($scope.standardList,standardList);
					}
	    		});
			}
		}

    	// Change branch dropDown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.group.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.group.instituteId})
					.$promise.then(function(branchList) {
						changeStandardList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){	
			if($scope.group.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.group.branchId).
						query({id : $scope.group.branchId}).$promise.then(function(academicYearList) {
						$scope.academicYearList.length = 0;
				
					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.group.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

		$resource('groups/:id').get({id : $stateParams.id}).$promise.then(function(groups) {
			angular.extend($scope.group, groups);
			angular.extend($scope.group.standardDivisionArray,groups.standardDivision);
			
			if(groups.individuals){
				angular.extend($scope.group.individuals, groups.individuals);	
			}
			else {
				$scope.group.individuals = [];
			}
			if(groups.individualIdList){
				angular.extend($scope.individualIds, groups.individualIdList);	
			}
			else {
				$scope.individualIds = [];
			}

			$scope.group.isPublicGroup = (groups.isPublicGroup == 'Y' ? true : false);
			$scope.isIndividualOrStandardDivisionSelected = ($scope.group.standardDivisionArray.length > 0 || $scope.group.individuals.length > 0 ? true : false) ;
			changeBranchList();
		});

		console.log("$scope.individualIds = "+ $scope.individualIds);
		$scope.popup = function() {
			var options = {
				templateUrl : 'app/groups/html/group-popup.html',
				controller : 'GroupSearchController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		$scope.indivisualStudentButtonValue = "Select";
		/** Remove item from selected individuals */
		$scope.removeIndividual = function(id) {
			for ( var i = 0; i < $scope.group.individuals.length; i++) {
				var individual = $scope.group.individuals[i];
				if (individual.id == id) {
					$scope.group.individuals.splice(i, 1);
					$scope.individualIds.splice(i, 1);
					break;
				}
			}
		};

		$scope.popupStd = function() {
			var options = {
				templateUrl : 'app/groups/html/group-standard-popup.html',
				controller : 'StandardSearchController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		$scope.standardDivisionButtonValue = "Select";

		/** Remove item from selected standard and division */
		$scope.removeStandard = function(standard, division) {
			for ( var i = 0; i < $scope.group.standardDivisionArray.length; i++) {
				var stdDiv = $scope.group.standardDivisionArray[i];
				if (stdDiv.standard == standard
						&& stdDiv.division == division) {
					$scope.group.standardDivisionArray.splice(
							i, 1);
					break;
				}
			}
		};

		/**
		 * This method is used to save edited object. Here we
		 * explicitly copy $scope.chemists to normal chemist
		 * object. This is done because $scope.chemist contains
		 * angular specific properties.
		 */
		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var group = {
				id : $scope.group.id,
				name : $scope.group.name,
				description : $scope.group.description,
				branchId : $scope.group.branchId,
				status : $scope.group.status,
				standardDivision : $scope.group.standardDivisionArray,
				individualIdList : $scope.individualIds,
				tags : $scope.group.tags,
				createdBy :$scope.group.createdBy,
				ownerRoleId : $scope.group.ownerRoleId,
				isPublicGroup : ($scope.group.isPublicGroup == true ? "Y" : "N"),
				academicYearId : $scope.group.academicYearId
			};

			$resource('groups').save(group).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('group.list',
					{
						instituteId : $scope.group.instituteId,
	        			branchId : $scope.group.branchId,
						status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
	        	}
			});
		};

		/**
		 * This method is used to cancel add operation. It will
		 * only call cancel utility method
		 */
		$scope.cancel = function() {
			CommonServices.cancelOperation('group.list');
		};
	});

	/** This method is used for uploading data */
	angular.module('group').controller('GroupFileUploadController',
			function($scope, Upload, $uibModalStack) {
				$scope.upload = function(file) {
					Upload.upload({
						url : 'groups/uploadFile',
						data : {branchId : $scope.branchId},
						file : file
					}).then(function(resp) {
						modalPopupService.closePopup(null, $uibModalStack);
						$scope.$parent.status  = resp.data.status;
			        	$scope.$parent.message = resp.data.message;
						angular.extend($scope.groupss, resp.data);
					});
				};

				$scope.cancel = function() {
					modalPopupService.closePopup(null, $uibModalStack);
				};
			});

	angular.module('group').controller('GroupSearchController',['$scope','userserviceapiurl', '$rootScope', '$resource','modalPopupService','$uibModalStack',
            function($scope, userserviceapiurl, $rootScope, $resource, modalPopupService, $uibModalStack) {
		console.log($scope);
		$scope.clickedForSearch = false;
		$scope.searchedIndividuals = [];
		/**
		 * This method is used to search individuals based on
		 * provided details
		*/
		$scope.search = function() {
			console.log($scope.searchData);
			$scope.searchedIndividuals = [];
			var url = userserviceapiurl + 'user/search?branch_id='+$scope.$parent.group.branchId+'&search_text='+$scope.searchData.name+
			 '&exclude_user_id='+$scope.individualIds;

			if($rootScope.roleId == 4){
				/*url = 'individuals/searchForGroupWhenTeacher?teacherUserId='+$rootScope.id
				 +'&name='+$scope.searchData.name+ ( $scope.$parent.group.id ? '&groupId='+$scope.$parent.group.id : '')+
				 '&allreadySelectedIdss='+$scope.individualIds;*/

				url = userserviceapiurl + 'user/search?search_text='+$scope.searchData.name+
				 '&exclude_user_id='+$scope.individualIds;
			}

			$resource(url).query().$promise.then(function(data) {
				if (data.length > 0) {
					$scope.hasSearchRecords = true;
					$scope.clickedForSearch = false;
					angular.forEach(data, function(individual) {
						individual.isSelected = false;
					});
					angular.extend($scope.searchedIndividuals,data);
				}
				else {
	    			 $scope.hasSearchRecords = false;
	    			 $scope.clickedForSearch = true;
	    		 }
			});
		};

		/** Add individuals to an array */
		$scope.addIndividuals = function() {
			angular.forEach($scope.searchedIndividuals,function(individual) {
				if (individual.isSelected) {
					$scope.group.individuals.push(individual);
					
					$scope.individualIds.push(individual.id);
				}
			});
			modalPopupService.closePopup(null, $uibModalStack);
		};

			$scope.cancelPopup = function() {
				modalPopupService.closePopup(null, $uibModalStack);
			};
	}]);

	angular.module('group').controller('StandardSearchController',['$scope','$resource','modalPopupService','$uibModalStack',function($scope, $resource, modalPopupService,$uibModalStack) {
		$scope.tempStandardList = $scope.standardList;
		$scope.tempDivisionList = [];
		$scope.standard = [];
		$scope.division = [];
		
		$scope.changeDivisions = function (){
			changeDivisions();
		};
		// Change divisionList as per the standard selected.
		function changeDivisions(){
			//$scope.divisionList.length = 0;
			if($scope.standard){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standard){
						$scope.tempDivisionList = standard.divisionList;
						break;
					}
				}
			}
		}

		/** Add individuals to an array */
		$scope.addStandards = function() {
			if($scope.standard) {
				var isDivisionSelected = $scope.division !== null && $scope.division.length > 0 ? true : false;
				
				if($scope.tempDivisionList.length>0){

				angular.forEach((isDivisionSelected ? $scope.division : $scope.tempDivisionList),function(div) {
					if ($scope.group.standardDivisionArray.length > 0) {
						var flag = false;
						for ( var i = 0; i < $scope.group.standardDivisionArray.length; i++) {
							if ($scope.group.standardDivisionArray[i].standard === $scope.standard
									&& $scope.group.standardDivisionArray[i].division === (isDivisionSelected ? div : div.id)) {
								flag = true;
								break;
							}
						}

						if (!flag) {
							$scope.group.standardDivisionArray.push({
								standard : $scope.standard,
								division : isDivisionSelected ? div : div.id,
								standardName : getStandardNameByCode($scope.standard),
								divisionName : getDivisionNameByCode(isDivisionSelected ? div : div.id)
							});
						}
					}
					else {
						$scope.group.standardDivisionArray.push({
							standard : $scope.standard,
							division : isDivisionSelected ? div : div.id,
							standardName : getStandardNameByCode($scope.standard),
							divisionName : getDivisionNameByCode(isDivisionSelected ? div : div.id)
						});
					}
					});
				}else{
					$scope.group.standardDivisionArray.push({
						standard : $scope.standard,
						standardName : getStandardNameByCode($scope.standard),
					});
				}
				}
				modalPopupService.closePopup(null, $uibModalStack);
			};
				function getStandardNameByCode(standard){
					for(var i =0; i< $scope.tempStandardList.length ; i++){
						var standardObj = $scope.tempStandardList[i];
						if(standardObj.id == standard){
							return standardObj.displayName;
						}
					}
				}

				function getDivisionNameByCode(division){
					for(var i =0; i< $scope.tempDivisionList.length ; i++){
						var divisionObj = $scope.tempDivisionList[i];
						if(divisionObj.id == division){
							return divisionObj.displayName;
						}
					}
				}
				$scope.cancelPopup = function() {
					modalPopupService.closePopup(null, $uibModalStack);
				};
				
		} ]);
})();
