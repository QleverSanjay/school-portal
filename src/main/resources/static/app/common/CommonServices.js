angular.module('common').service('CommonServices',function($state,$resource){
	
	
	function getInstituteList () {
		
		$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    		return instituteList;
		});
	}
	
	function getBranchListByInstituteId(instituteId){
		if(instituteId){
			$resource('masters/getFilterdBranch/:id').query({id : instituteId}).$promise.then(function(branchList) {
				return branchList;	
			});	
		}
	}
	
	function deleteStudent(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}
	
	function saveStudent(url,obj,transitionPage){
		$resource(url).save(obj).$promise.then(function(data) {
			$state.transitionTo(transitionPage,{reload : true});
		});
	}
	
	function cancelOperation(transitionPage){
		$state.transitionTo(transitionPage,{reload : true});
	}
	
	/*function deleteFees(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}*/
	
	function deleteInstitute(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}
	
	function saveFees(url,obj,transitionPage){
		
	}
	
	function deleteNotice(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}
		
	function searchIndividuals(url, obj){
		$resource(url).save(obj).$promise.then(function(data) {
			return data;
		});
		
		
	}
	
	
	function deleteSchool(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}
	
	function saveSchool(url,obj,transitionPage){
		$resource(url).save(obj).$promise.then(function(data) {
			$state.transitionTo(transitionPage,{reload : true});
		});
	}
	
	/*function deleteGroup(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}*/
	
	function saveGroup(url,obj,transitionPage){
		
	}
/*	
	function deleteTeacher(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}
	*/
/*	function saveTeacher(url,obj,transitionPage){
		
	}*/
	
	function deleteVendor(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}
	function saveVendor(url,obj,transitionPage){
		
	}
	
	function deleteForum(url,id){
		$resource(url).remove({id : id}).$promise.then(function() {
			  $state.go($state.current, {}, {reload: true});
			});
	}
	function saveForum(url,obj,transitionPage){
		
	}
	
	function submitLogin(url,obj,transitionPage){
		
	}
	
	return {
		deleteStudent : deleteStudent,
		saveStudent : saveStudent,
	//	deleteFees : deleteFees,
		saveFees : saveFees,	
		deleteNotice : deleteNotice,
		deleteInstitute : deleteInstitute,
		getInstituteList : getInstituteList,
		getBranchListByInstituteId : getBranchListByInstituteId,
		deleteSchool : deleteSchool,
		saveSchool : saveSchool,
	//	deleteTeacher : deleteTeacher,
	//	saveTeacher : saveTeacher,
	//	deleteGroup : deleteGroup,
		saveGroup : saveGroup,
		deleteVendor : deleteVendor,
		saveVendor : saveVendor,
		deleteForum : deleteForum,
		saveForum : saveForum,
		submitLogin : submitLogin,
		cancelOperation : cancelOperation
	};
});