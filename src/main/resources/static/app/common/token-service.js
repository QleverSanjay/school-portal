/**
* Created by nemade_g on 11-11-2015.
*/
(function () {
 'use strict';

 angular.module('school').factory('tokenInjector', tokenInjector);
	function tokenInjector($injector) {	
		var tokenInjector = {
			request: function(config) {
				var url = config.url;
				if(url != null &&  (url.indexOf("user-service") > -1 || url.indexOf("school/") > -1)) {
					var state = $injector.get("$state");
					if(state.current.name != 'login.log' && state.current.name != 'login.logout'){
						var lastRequestedTime = localStorage.getItem('lastRequestedTime');
						var currTime = new Date();
						currTime = currTime.getTime();

						if(lastRequestedTime && (currTime - parseInt(lastRequestedTime)) > 60000 * 15){
							state.go('login.logout');
						}
						else {
							localStorage.lastRequestedTime = currTime;
						}	
					}
				}

//				if(url != null &&  url.indexOf("user-service") > -1) {
					var qfix_token = localStorage.getItem('token');
					config.headers['token'] = qfix_token;
//				}
/*				else if(url.indexOf("http") == -1){
					if(url.indexOf("/") == 0 && url != '/angucomplete-alt/index.html'){
 						url = "https://www.eduqfix.com/SchoolPortal"+url;
//						url = "http://test.eduqfix.com/SchoolPortal"+url
					}else if(url.indexOf("vendor/breadcrumb") == -1 && 
							url.indexOf("isteven-multi-select") == -1 &&
							url.indexOf("ngTagsInput/tags-input") == -1 &&
							url.indexOf("ngTagsInput/tag-item") == -1 &&
							url.indexOf("template/alert/alert.html") == -1 &&
							url.indexOf("template/typeahead") == -1 &&
							url.indexOf("selectize/") == -1 &&
							url != '/angucomplete-alt/index.html' &&
							url.indexOf("template/date-picker") == -1 &&
							url.indexOf("template/timepicker") == -1 &&
							url.indexOf("template/time-pi") == -1 &&
							url.indexOf("template/modal") == -1 &&
							url.indexOf("template/datepicker") == -1){
						url = "https://www.eduqfix.com/SchoolPortal/"+url;
//						url = "http://test.eduqfix.com/SchoolPortal/"+url;

					}
					config.url = url;
				}
*/
				return config;
			}
		};
		return tokenInjector;
	}
 })();