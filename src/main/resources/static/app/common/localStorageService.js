/**
* Created by nemade_g on 11-11-2015.
*/
(function () {
 'use strict';

 angular
   .module('school')
   .factory('localStorageService', localStorageService);

	localStorageService.$inject = ['$window', 'expireTimeInMinuites'];
	
	function localStorageService($window, expireTimeInMinuites) {	
	
		return {
			set: function(key, value) {
			  $window.localStorage[key] = value;
			},
			get: function(key, defaultValue) {
			  return $window.localStorage[key] || defaultValue;
			},
			setObject: function(key, value) {
			  $window.localStorage[key] = JSON.stringify(value);
			},
			getObject: function(key) {
			   return JSON.parse($window.localStorage[key] || '{}');
			},
			removeObject: function(key) {
			   return $window.localStorage.removeItem(key);
			},
			getArrayObject: function(key) {
			  return JSON.parse($window.localStorage[key] || '[]');
			},
			clear: function () {
				$window.localStorage.clear();
			},
			setObjectWithExpiry: function(key, value) {
				//console.log("setObjectWithExpiry:: "+ key);
				var timestamp = new Date().getTime();
				var expireTimeInMilliseconds = expireTimeInMinuites * 60 * 1000;
				$window.localStorage[key] = 
					JSON.stringify({
						data: JSON.stringify(value),
						timestamp: timestamp,
						expireTimeInMilliseconds: expireTimeInMilliseconds
					});
				var item = JSON.parse($window.localStorage[key] || '{}');
				//console.log("set timestamp:: "+item.timestamp );
				//console.log("set expiretime :: "+ item.expireTimeInMilliseconds);
			   //$window.localStorage[key]= JSON.stringify(value);
			},
			getObjectWithExpiry: function(key, isArray) {
				//console.log("getObjectWithExpiry:: "+ key);
				var item = $window.localStorage[key]; 
				if(item != null) {
					item = JSON.parse(item);
					//console.log("current timestamp:: "+ new Date().getTime());
					//console.log("get timestamp:: "+item.timestamp );
					//console.log("get expiretime :: "+ item.expireTimeInMilliseconds);
				   
					//console.log("old timestamp:: "+(item.timestamp + item.expireTimeInMilliseconds))
					//console.log("difference :: "+ (new Date().getTime() - item.timestamp + item.expireTimeInMilliseconds));
					if(!item || new Date().getTime() > (item.timestamp + item.expireTimeInMilliseconds)) {
						console.log("object expired::");
						return null
					} else {
					  console.log("object alive");
					  if(isArray) {
						  return JSON.parse(item.data || '{}'); 
					  } else {
						  return JSON.parse(item.data || '[]'); 
					  }
					}
				}
				return null;
			}
		}
	}
 
})();
