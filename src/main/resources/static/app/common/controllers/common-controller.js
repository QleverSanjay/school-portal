(function(){
	angular.module('common').controller('ValidationErrorController',function($scope,$state, $uibModalStack, modalPopupService) {
    	$scope.validationErrMsg = $scope.$parent.validationErrMsg;
		$scope.validationErrors = $scope.$parent.validationErrors;

	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
})();