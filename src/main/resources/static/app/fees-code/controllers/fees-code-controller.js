(function(){
	angular.module('fees-code').controller('FeesCodeController',function($rootScope,$resource, $localStorage, $filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.feesCodeList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isSingleBranch = false;	
		$scope.branchName = "";

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('fees/fees-code/list/'+$scope.branchId).query().$promise.then(function(feesCodeList) {
					$scope.feesCodeList.length = 0;
					if(feesCodeList && feesCodeList.length > 0){
						angular.extend($scope.feesCodeList,feesCodeList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
				});	
			}
		};

	     $scope.deleteData = function deleteFeesCode(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('fees/fees-code').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.feesCodeList.length ;i ++ ){
      			  		var feesCode = $scope.feesCodeList[i];
      			  		if(feesCode.id == id){
      			  			$scope.feesCodeList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});

	angular.module('fees-code').controller('FeesCodeAddController',function($rootScope,$filter, $localStorage, $scope,$state,$stateParams,$resource,CommonServices) {
		$scope.feesCode = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.feesCodeHeaderValue = "Add Fees Code";

		$scope.feesCode.instituteId = $localStorage.selectedInstitute;
		$scope.feesCode.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.feesCode.instituteId){
	    			changeBranchList(false);
	    		}
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.feesCode.instituteId || $scope.feesCode.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.feesCode.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);	
				});	
			}
		};

		$scope.cancel = function(){
			CommonServices.cancelOperation('fees-code.list');
		};

		$scope.save = function() {
			var feesCode = {
				id : $scope.feesCode.id,
				name : $scope.feesCode.name,
				description : $scope.feesCode.description,
				branchId : $scope.feesCode.branchId,
			};			

			$resource('fees/fees-code').save(feesCode).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('fees-code.list',
					{
						instituteId : $scope.feesCode.instituteId,
	        			branchId : $scope.feesCode.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});

	/*This Controller is Used for FeesCode Edit functionality*/
	angular.module('fees-code').controller('FeesCodeEditController',function($rootScope,$filter,$scope,$state,$stateParams,$resource, CommonServices) {
		
		$scope.feesCode = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.feesCodeHeaderValue = "Edit Fees Code";

		$resource('fees/fees-code/:id').get({id : $stateParams.id}).$promise.then(function(feesCodeToEdit) {
			angular.extend($scope.feesCode,feesCodeToEdit);
			$rootScope.setSelectedData($scope.feesCode.instituteId,$scope.feesCode.branchId);
			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.feesCode.instituteId || $scope.feesCode.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.feesCode.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.cancel = function(){
			$state.transitionTo('fees-code.list',{reload : true});
		};

		$scope.save = function() {
			var feesCode = {
				id : $scope.feesCode.id,
				name : $scope.feesCode.name,
				description : $scope.feesCode.description,
				branchId : $scope.feesCode.branchId,
			};

			$resource('fees/fees-code').save(feesCode).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('fees-code.list',
					{
						instituteId : $scope.feesCode.instituteId,
	        			branchId : $scope.feesCode.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
		        }
			});
		};
	});
})();

