(function(){
	angular.module('fees-code').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('fees-code', {
			url : '/fees-code',
			abstract :true,
			controller : 'FeesCodeModelController',
			data: {
				 breadcrumbProxy: 'fees-code.list'
			}
		}).state('fees-code.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/fees-code/html/fees-code-list.html',
					controller : 'FeesCodeController'
				}
			},
			data: {
	            displayName: 'Fees Code List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('fees-code.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/fees-code/html/fees-code-add.html',
					controller : 'FeesCodeAddController'
				}
			},
			data: {
	            displayName: 'Add Fees Code'
	        }
		}).state('fees-code.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/fees-code/html/fees-code-add.html',
					controller : 'FeesCodeEditController'
				}
			},
			data: {
	            displayName: 'Edit Fees Code'
	        }
		});
	}]);
})();