(function(){
	angular.module('forum').controller('ForumController',function($rootScope,$localStorage,$resource,$filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {
	    $scope.forums = [];
	    $scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	    $scope.instituteId = $stateParams.instituteId;
	    $scope.branchId = $stateParams.branchId;

	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.branchName = "";
		
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);
		
		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch;
		
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('forums/getForumByBranch?branchId='+$scope.branchId+'&userId='+$rootScope.id+'&roleId='+$rootScope.roleId)
				.query().$promise.then(function(forumList) {
					$scope.forums.length = 0;
					if(forumList && forumList.length > 0){
						angular.extend($scope.forums,forumList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId == 6){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        				changeYearList();
        				if($stateParams.branchId){
        				filterTableByBranch($scope.branchId);
        				}
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all forums based on teacher login.
    	if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		changeYearList();
        	filterTableByBranch($rootScope.branchId);
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
	    			changeBranchList(false);
	    		//}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){							
							$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
							
							if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}
						}		
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		

	    /** This method is used to delete forum from the screen. It will only call delete utility method */
	    $scope.deleteData = function deleteForum(id){
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
	        	 	$resource('forums').remove({id : id}).$promise.then(function(resp) {
		        		if(resp.status == 'success'){
		        			for(var i =0; i < $scope.forums.length ;i ++ ){
		      			  		var forum = $scope.forums[i];
		      			  		if(forum.id == id){
		      			  			$scope.forums.splice(i , 1);
		      			  			break;
		      			  		}
		      			  	}	
		        		}
		        		$scope.status = resp.status;
		        		$scope.message = resp.message;
	      			});
	         });
	     }; 
	});

	angular.module('forum').controller('validateForumForm',function($resource,$scope,$state) {
		$scope.regExTitle = /^[A-Z a-z 0-9 , . () @ _ * & ? -]{2,100}$/;
	});
	
	
	angular.module('forum').controller('ForumViewController',function($resource,$sce,$scope,forumserviceapiurl,$state) {
		//$resource('http://localhost:80/collab/').get().$promise.then(function(branchList) {
		$scope.isError = false;
		var sid = localStorage.getItem('forumSID');
		if(sid != null && sid != ""){
			$scope.url = $sce.trustAsResourceUrl(forumserviceapiurl+"index.php?sid=" +sid);
		}
		else {
			$scope.isError = true;
		}
	});

	
	angular.module('forum').controller('ForumAddController',function($rootScope,$localStorage,$scope,$filter,$state,Upload,$resource,$stateParams,CommonServices) {
		$scope.forum = {}; 
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.groupList = [];
		$scope.forumHeaderValue = "Add Forum";
		
		$scope.forum.instituteId = $localStorage.selectedInstitute;
		$scope.forum.branchId = $localStorage.selectedBranch;
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);
		
	     $scope.cancelPopup = function(){
		    	$uibModalInstance.dismiss('cancel');
		    };
		
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId == 6){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        			}
        			changeYearList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			
	    		$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
		
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
		
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.forum.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.forum.instituteId})
					.$promise.then(function(branchList) {						
							changeYearList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		// get all forums based on teacher login.
    	if($rootScope.roleId == 4){
    		$scope.forum.branchId = $rootScope.branchId;
    		changeYearList();
    	}
    	
		$scope.changeGroupList = function (){
			changeGroupList();
		};
		
		function changeGroupList(){
			if($scope.forum.branchId){		
				$resource('groups/getGroupsByBranch?branchId='+$scope.forum.branchId).query()
					.$promise.then(function(groupList) {
					 $scope.groupList.length = 0;
					if(groupList != null && groupList.length > 0){
						$scope.forum.groupId = groupList[0].id; 
					}	
					angular.extend($scope.groupList,groupList);									
	    		});
			}
		}
		
		$scope.changeYearList = function(){
			changeYearList();
		};
			
		function changeYearList(){
			if($scope.forum.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.forum.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.forum.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
						changeGroupList();
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}


		/** This method is used to adding forum. It will only call add utility method */
		$scope.save = function() {
			
			var forum = {
					id: $scope.forum.id,
					title: $scope.forum.title,
					description: $scope.forum.description,
					branchId: $scope.forum.branchId,
					groupId : $scope.forum.groupId,
					forumId : $scope.forum.forumId,
					canOneWay: ($scope.forum.canOneWay == true || $scope.forum.canOneWay == 'Y' ? "Y" : "N"),
					active: $scope.forum.active,
					isDelete: $scope.forum.isDelete,
					createdBy : $rootScope.id,
					academicYearId : $scope.forum.academicYearId
			};
			
			$resource('forums').save(forum).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('forum.list',
						{
							instituteId : $scope.forum.instituteId,
		        			branchId : $scope.forum.branchId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('forum.list');
		};
	});
	
	angular.module('forum').controller('ForumEditController',function($rootScope,$localStorage,$scope,$filter,$state,Upload,$stateParams,$resource,CommonServices) {
		/** This is written to get single forum object to display on the edit screen. */
		$scope.forum = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.groupList = [];
		$scope.forumHeaderValue = "Edit Forum";
		
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);

		
		$scope.status = "";
		$scope.message = "";
		
		$resource('forums/:id').get({id : $stateParams.id}).$promise.then(function(forums) {
			angular.extend($scope.forum,forums);
			$scope.forum.canOneWay = (forums.canOneWay == 'Y' ? true : false);
			
	    	if($rootScope.roleId != 4){
	    		changeBranchList();
	    	}
	    	else {
	    		changeYearList();
	    		changeGroupList();
	    	}
		});
		
		
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.forum.instituteId || $scope.forum.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.forum.instituteId})
					.$promise.then(function(branchList) {
						/*if(branchList != null && branchList.length > 0){
							$scope.forum.branchId = branchList[0].id;*/
							changeYearList();
							angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.changeGroupList = function (){
			changeGroupList();
		};

		function changeGroupList(){
			 $scope.groupList.length = 0;
			 if($scope.forum.branchId){		
				$resource('groups/getGroupsByBranch?branchId='+$scope.forum.branchId).query()
					.$promise.then(function(groupList) {
					 angular.extend($scope.groupList,groupList);									
	    		});
			}
		}
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){	
			if($scope.forum.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.forum.branchId).
						query({id : $scope.forum.branchId}).$promise.then(function(academicYearList) {
						$scope.academicYearList.length = 0;
				
					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.forum.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
						changeGroupList();
					}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
		
		/** This method is used to save edited object. Here we explicitly copy $scope.forums to normal forum object. This is done because $scope.forum contains angular specific properties. */
		$scope.save = function() {
			var forum = {
					id: $scope.forum.id,
					title: $scope.forum.title,
					description: $scope.forum.description,
					branchId: $scope.forum.branchId,
					groupId : $scope.forum.groupId,
					forumId : $scope.forum.forumId,
					canOneWay: ($scope.forum.canOneWay == true ? "Y" : "N"),
					active: $scope.forum.active,
					isDelete: $scope.forum.isDelete,
					createdBy : $rootScope.id,
					academicYearId : $scope.forum.academicYearId
			};
			
			/** This method is used to adding forum. It will only call add utility method */
		
			
			$resource('forums').save(forum).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('forum.list',
						{
							instituteId : $scope.forum.instituteId,
		        			branchId : $scope.forum.branchId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('forum.list');
		};
	});
	
})();


