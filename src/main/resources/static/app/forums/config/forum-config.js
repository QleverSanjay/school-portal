(function(){
	angular.module('forum').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('forum', {
			url : '/forum',
			abstract :true,
			controller : 'ForumModelController',
			data: {
				 breadcrumbProxy: 'forum.list'
	        }
		}).state('forum.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/forums/html/forum-list.html',
					controller : 'ForumController'
				}
			},
			data: {
	            displayName: 'Forum List'
	        },
	      /*  resolve : {
	        	TeacherList : function($resource){
	        		return $resource('teachers').query().$promise;
	        	}
	        }*/
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : '',
	        	message : '',
	        	status : ''
	        }
		}).state('forum.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/forums/html/forum-add.html',
					controller : 'ForumAddController'
				}
			},
			 params : {
		        	groupId : '',
		        },
			data: {
	            displayName: 'Add Forum'
	        }
			
		}).state('forum.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/forums/html/forum-add.html',
					controller : 'ForumEditController'
				}
			},
			params : {
	        	groupId : '',
	        },
			data: {
	            displayName: 'Edit Forum'
	        }
		}).state('forum.view', {
			url : '/view',
			views : {
				'@' : {
					templateUrl : 'app/forums/html/forum-view.html',
					controller : 'ForumViewController'
				}
			},
			params : {
	        	groupId : '',
	        },
			data: {
	            displayName: 'View Forum'
	        }
		});
	}]);
})();