(function(){
	angular.module('exportData').controller('exportDataController',function($http,$rootScope, $localStorage, $resource, $confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {

		
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	   

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.standardList = [];
		$scope.divisionList = [];
		
		
		
		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.standardId = $stateParams.standardId ? $stateParams.standardId : $localStorage.selectedStandard ;
		$scope.divisionId = $stateParams.divisionId ? $stateParams.divisionId : $localStorage.selectedDivision ;

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isUploadingFailed = false;
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('academicyear/getAcademicYearByBranch?branchId='+$scope.branchId).query().$promise.then(function(academicYearList) {
					$scope.academicYearList.length = 0;

					if(academicYearList && academicYearList.length > 0){
						$scope.activeYearFound = false;
						$scope.isAllLoaded = false;
						for(var i =0; i <academicYearList.length; i++ ){
							var academicYearObj = academicYearList[i];
							if(academicYearObj.isCurrentActiveYear == 'Y'){
								$scope.activeYearFound = true;
							}
						}
						$scope.isAllLoaded = true;
						angular.extend($scope.academicYearList,academicYearList);
					}					
	    		});
			}
		}
		
		$scope.changeStandardList = function(){
			$scope.isFirstTimeLoaded = false;
			$scope.status = "";
    		$scope.message = "";
			changeStandardList();
		};

		$scope.changeDivisionList = function(){
			$scope.isFirstTimeLoaded = false;
			$scope.status = "";
    		$scope.message = "";
			changeDivisionList();
		};

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				if(branchList.length == 1){
        					$scope.branchId = (!$scope.branchId ? branchList[0].id : $scope.branchId);
        					/* $scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;*/
        				}
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							
							$scope.branchId = (isBranchChanged ? branchList[0].id  : (!$scope.branchId ? branchList[0].id : $scope.branchId));
	        				if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
								changeStandardList();
							}
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;

					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
				if($scope.isFirstTimeLoaded && $scope.divisionId){
					filterTableByBranch($scope.branchId);
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}

		$scope.exportStudentData=function(){
			var params = 'branchId='+$scope.branchId 
				+ ($scope.standardId ? '&standardId='+$scope.standardId : '') 
				+ ($scope.divisionId ? '&divisionId='+$scope.divisionId : '');

			window.location.href = 'students/exportStudentData?'+params;
		};

           $scope.exportParentData=function(){
       			var params = 'branchId='+$scope.branchId 
       				+ ($scope.standardId ? '&standardId='+$scope.standardId : '') 
       				+ ($scope.divisionId ? '&divisionId='+$scope.divisionId : '');

       			window.location.href = 'students/exportParentData?'+params;
       		};
			
			
		
       $scope.exportTeacherData=function(){
    		var params = 'branchId='+$scope.branchId 
				+ ($scope.standardId ? '&standardId='+$scope.standardId : '') 
				+ ($scope.divisionId ? '&divisionId='+$scope.divisionId : '');

			window.location.href = 'teachers/exportTeacherData?'+params;
		};
	
       
			
			
		

	    

	   	});   


	
	
	

	
	})();