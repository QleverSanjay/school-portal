(function(){
	angular.module('exportData').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('exportData', {
			url : '/exportData',
			abstract :true,
			controller : 'exportDataModelController',
			data: {
				 breadcrumbProxy: 'exportData.list'
	        }
		}).state('exportData.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/exportData/html/exportData-list.html',
					controller : 'exportDataController'
				}
			},
			data: {
	            displayName: 'Export Report'
	        },
	      
		});
	}]);
})();

