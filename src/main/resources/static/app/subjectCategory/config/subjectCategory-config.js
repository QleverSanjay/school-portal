(function(){
	angular.module('subjectCategory').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('subjectCategory', {
			url : '/subjectCategory',
			abstract :true,
			controller : 'SubjectCategoryModelController',
			data: {
				 breadcrumbProxy: 'subjectCategory.list'
	        }
		}).state('subjectCategory.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/subjectCategory/html/subjectCategory-list.html',
					controller : 'SubjectCategoryController'
				}
			},
			data: {
	            displayName: 'SubjectCategory List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		})
		.state('subjectCategory.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/subjectCategory/html/subjectCategory-add.html',
					controller : 'SubjectCategoryAddController'
				}
			},
			data: {
	            displayName: 'Add SubjectCategory'
	        }
		}).state('subjectCategory.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/subjectCategory/html/subjectCategory-add.html',
					controller : 'SubjectCategoryEditController'
				}
			},
			data: {
	            displayName: 'Edit SubjectCategory'
	        },
		});
	}]);
})();