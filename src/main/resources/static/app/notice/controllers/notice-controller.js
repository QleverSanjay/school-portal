	(function(){
	angular.module('notice').controller('NoticeController',function($rootScope,$localStorage,$resource,$filter,$confirm,$stateParams,DTOptionsBuilder,$scope,$state,$uibModal,CommonServices) {
		$rootScope.loginCount=2;
		$scope.branchId = '';
		$scope.noticeList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	    $scope.title = "";
	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		$scope.istecherSpecificNotices = false;
		$scope.sendByUser = false;
		$scope.isAllNotice = false;
		// Datatable sorting...

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", false)
		.withOption("paging", true)
		.withOption("searching", true)
        .withOption("aaSorting", [])
        .withOption("responsive", true)
        .withOption("aoColumnDefs", [ { "sType": "date-eu", "aTargets": [ 3 ] } ]);
		
		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch;

		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		$scope.title = "Sent Notices";
    		$scope.isAllNotice = true;
    		changeYearList();
    		filterTableByBranch($scope.branchId);
		}
		
		$scope.showMyNotices = function(){
			$scope.title = "Received Notices";
			$scope.isAllNotice = false;
			$scope.message = "";
			$scope.istecherSpecificNotices = true;
			filterTableByTeacherBranch($scope.branchId);
		};

		$scope.showAllNotices = function(){
			$scope.istecherSpecificNotices = false;
			$scope.message = "";
			$scope.isAllNotice = true;
			$scope.title = "Sent Notices";
			filterTableByBranch($scope.branchId);
		};

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
			filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			$scope.noticeList.length = 0;
			if(branchId){
				$resource('notice/getNoticeByBranch?branchId='+branchId+'&roleId='
						+$rootScope.roleId+'&userId='+$rootScope.id).query().$promise.then(function(noticeList) {
					$scope.noticeList.length = 0;
					if(noticeList && noticeList.length > 0){
						angular.extend($scope.noticeList,noticeList);
					}
	    		});
			}
		}

		function filterTableByTeacherBranch(branchId){
			$scope.noticeList.length = 0;
			if($rootScope.id){
				$resource('notice/getNoticeByBranchForTeacher?branchId='+branchId+'&teacherId='+$rootScope.id).query().$promise.then(function(noticeList) {
					$scope.noticeList.length = 0;
					if(noticeList && noticeList.length > 0){
						angular.extend($scope.noticeList,noticeList);
					}
	    		});
			}
		}
		
		
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			$scope.branchList.length = 0;
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        				changeYearList();
        				if($stateParams.branchId){
        				filterTableByBranch($scope.branchId);
        				}
        				angular.extend($scope.branchList,branchList);
        			}
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		
	    			$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
	    			changeBranchList(false);
	    	
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
							
							$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
							if($stateParams.branchId){
	        					filterTableByBranch($scope.branchId);
							}
	        				if(!isBranchChanged){
	        					filterTableByBranch($scope.branchId);
	        				}
	        			}
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		
		$scope.deleteData = function deleteNotice(id){
		   	 $confirm({text: 'Are you sure you want to delete?'})
		         .then(function() {
		        	 $resource('notice').remove({id : id}).$promise.then(function(resp) {
		    	 			if(resp.status == 'success'){
		            	 		for(var i =0; i < $scope.noticeList.length ;i ++ ){
		          			  		var notice = $scope.noticeList[i];
		          			  		if(notice.id == id){
		          			  			$scope.noticeList.splice(i , 1);
		          			  			break;
		          			  		}
		          			  	}	
		    	 			}
			        		$scope.status = resp.status;
			        		$scope.message = resp.message;	
		      			});
		         });
		     };
		
	});


	angular.module('notice').controller('validateNoticeForm',function($resource,$scope,$state) {
		$scope.regExTitle = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});

	/* Notice View Controller..*/	
	angular.module('notice').controller('NoticeViewController',function(CommonServices, $rootScope, userserviceapiurl, $scope,$uibModal,$state,$stateParams,$resource) {
		$scope.notice = {};
		var teacherId = $rootScope.id;

		$resource('notice/getById?id=' + $stateParams.id+ ($rootScope.roleId == 4 ? '&teacherId='+teacherId : '')).get().$promise.then(function(notice) {
			angular.extend($scope.notice,notice);
			if($scope.notice.readStatus == 'N'){
				var notificationToMarkRead  = {
					notification_id :  $scope.notice.id,
					for_user_id : $scope.notice.for_user_id
				};

				$rootScope.markAsRead(userserviceapiurl + 'notification/mark/read', notificationToMarkRead).then(function(data){
					$scope.notice.readStatus = 'Y';
			        console.log(data);
			    }, function (err){
			    	console.log(err);
			    });
			}
		});

		$scope.cancel = function(){
			CommonServices.cancelOperation('notice.list');
		};
	});
	
	angular.module('notice').controller('NoticeSendController',function($localStorage,CommonServices,Upload,$filter,$rootScope,$scope,modalPopupService,$state,$stateParams,$resource) {
		$scope.notice = {};
		$scope.hasSearchRecords = false;
		$scope.hasGroupSearchRecords = false;
		$scope.individualIds = [];
		$scope.notice.individuals = [];
	
		$scope.notice.groupList = [];
		$scope.notice.groupIdList = [];
		$scope.academicYearList = [];

		$scope.status = "";
		$scope.message = "";
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isSaveDisabled = false;
		$scope.isIndividualOrGroupSelected = false;
		
		$scope.notice.instituteId = $localStorage.selectedInstitute;
		$scope.notice.branchId = $localStorage.selectedBranch;
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);


		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        			}
        			changeYearList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}
    	
    	// Used when teacher login.
    	if($rootScope.roleId == 4){
    		$scope.notice.branchId = $rootScope.branchId;
    		changeYearList();
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			
	    		angular.extend($scope.instituteList,instituteList);
	    		changeBranchList();
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			if($scope.notice.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.notice.instituteId})
					.$promise.then(function(branchList) {
						changeYearList();
					angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){
			if($scope.notice.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.notice.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.notice.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

		$scope.$watch("notice.individuals", function() {
			checkIsIndividualOrGroupSelected();
		}, true);
		
		function checkIsIndividualOrGroupSelected (){
			if ($scope.notice.individuals && $scope.notice.individuals.length > 0) {
				$scope.isIndividualOrGroupSelected = true;
			} else if ($scope.notice.groupList && $scope.notice.groupList.length > 0) {
				$scope.isIndividualOrGroupSelected = true;
			} else {
				$scope.isIndividualOrGroupSelected = false;
			}
		}
		
		$scope.$watch("notice.groupList", function() {
			checkIsIndividualOrGroupSelected();
		}, true);
		
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('notice.list');
		};
		
		$scope.indivisualStudentButtonValue = "Add Individuals";
		
		$scope.popup = function(){
	    	  var options = {
	    	      templateUrl: 'app/notice/html/notice-popup.html',
	    	      controller: 'NoticeSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

	     $scope.groupPopup = function(){
	    	  var options = {
	    	      templateUrl: 'app/notice/html/notice-groupPopup.html',
	    	      controller: 'NoticeGroupSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };
	     
	     /** Remove item from selected individuals */
	     $scope.removeIndividual = function(id){
	    	 for (var i = 0; i< $scope.notice.individuals.length; i++){
	    		 var individual = $scope.notice.individuals[i];
    			 if (individual.id == id) {
    		    	$scope.notice.individuals.splice(i, 1);
    		    	$scope.individualIds.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };
		
	     /** Remove item from selected individuals */
	     $scope.removeGroup = function(id){
	    	 for (var i = 0; i< $scope.notice.groupList.length; i++){
	    		 var group = $scope.notice.groupList[i];
    			 if (group.id == id) {
    		    	$scope.notice.groupList.splice(i, 1);
    		    	$scope.notice.groupIdList.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };
	     
		$scope.send = function() {
			var notice = {
				title : $scope.notice.title,
				description : $scope.notice.description,
				category : $scope.notice.category,
				priority : $scope.notice.priority,
				groupIdList : $scope.notice.groupIdList,
				individualIdList : $scope.individualIds,
				branchId : $scope.notice.branchId,
				sentBy : $rootScope.id,
				fileChanegd : $scope.notice.fileChanegd,
				academicYearId : $scope.notice.academicYearId
			};
			
			/** This method is used to adding event. It will only call add utility method */
			Upload.upload({
	            url: 'notice/save',
	            method: 'POST',
                data : notice,
	            file: $scope.notice.image,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
					$state.transitionTo('notice.list', {
						status : resp.data.status, 
	        			message : resp.data.message,
	        			instituteId : $scope.notice.instituteId,
	        			branchId : $scope.notice.branchId,
					},{reload : true});
				}
				else{
        			$scope.status = resp.data.status;
	        		$scope.message = resp.data.message;
        		}
	        });			
			
			//CommonServices.saveNotice('notice', $scope.notice, 'notice.list');
		};
	});
	
	angular.module('notice').controller('NoticeSearchController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl',
	   function($rootScope,$scope,$resource, modalPopupService,$uibModalStack,userserviceapiurl) {

		$scope.clickedForSearch = false;
		$scope.searchedIndividuals = [];	
		/** This method is used to search individuals based on provided details*/
		$scope.search = function(){
			$scope.searchedIndividuals = [];
			console.log($scope.searchData);
			if($rootScope.roleId == 4){
				$resource(userserviceapiurl + 'user/search?search_text='+$scope.searchData.name+
						 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {
	
		    		 if(data.length > 0){
			    		 $scope.hasSearchRecords = true;
			    		 $scope.clickedForSearch = false;
			    		 angular.forEach( data, function(individual){
			    			 individual.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedIndividuals,data);
			    	 }
		    		 else {
		    			 $scope.hasSearchRecords = false;
		    			 $scope.clickedForSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/search?branch_id='+$scope.$parent.notice.branchId+'&search_text='+$scope.searchData.name+
						 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {
					 if(data.length > 0){
			    		 $scope.hasSearchRecords = true;
			    		 $scope.clickedForSearch = false;
			    		 angular.forEach( data, function(individual){
			    			 individual.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedIndividuals,data);
			    	 }
		    		 else {
		    			 $scope.hasSearchRecords = false;
		    			 $scope.clickedForSearch = true;
		    		 }
				});
			}
	     };
	     
	     
	     /** Add individuals to an array */
	     $scope.addIndividuals  = function(){	    
	    	 console.log($scope.notice.individuals);
	    	 angular.forEach( $scope.searchedIndividuals, function(individual){
	    		 if (individual.isSelected) {
    		    	$scope.notice.individuals.push(individual);
    		    	$scope.individualIds.push(individual.id);
    		    }
	    	 });
	    	 modalPopupService.closePopup(null, $uibModalStack);
	     };
	     
	     $scope.cancelPopup = function(){
	    	 modalPopupService.closePopup(null, $uibModalStack);
		    };
	}]);
	
	
	angular.module('notice').controller('NoticeGroupSearchController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl',
	  function($rootScope,$scope,$resource,modalPopupService,$uibModalStack,userserviceapiurl) {
		$scope.clickedForGroupSearch = false;
		$scope.searchedGroups = []; 
		/** This method is used to search groups based on provided details*/
		$scope.searchGroup = function(){
			$scope.searchedGroups = [];
			console.log($scope.searchData);
			if($rootScope.roleId == 4){	
				$resource(userserviceapiurl + 'user/groups?search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.notice.groupIdList).query()
					.$promise.then(function(data) {
		    		 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/groups?branch_id='+$scope.$parent.notice.branchId+'&search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.notice.groupIdList).query().
				$promise.then(function(data) {
					 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
				});
			}
				
				
	     };
	     
	     /** Add Groups to an array */
	     $scope.addGroups  = function(){
	    	 angular.forEach( $scope.searchedGroups, function(group){
	    		 if (group.isSelected) {
    		    	$scope.notice.groupList.push(group);
    		    	$scope.notice.groupIdList.push(group.id);
    		    }
	    	 });
	    	 modalPopupService.closePopup(null, $uibModalStack);
	     };
	     
	     $scope.cancelPopup = function(){
	    	 modalPopupService.closePopup(null, $uibModalStack);
		    };
	}]);
})();