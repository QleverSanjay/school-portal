(function(){
	angular.module('notice').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('notice', {
			url : '/notice',
			abstract :true,
			controller : 'NoticeModelController',
			data: {
				 breadcrumbProxy: 'notice.list'
	        }
		}).state('notice.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/notice/html/notice-list.html',
					controller : 'NoticeController'
				}
			},
			data: {
	            displayName: 'Notice List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('notice.view', {
			url : '/view/:id',
			views : {
				'@' : {
					templateUrl : 'app/notice/html/notice-view.html',
					controller : 'NoticeViewController'
				}
			},
			data: {
	            displayName: 'View Notice'
	        }
	    })
		.state('notice.send', {
			url : '/send',
			views : {
				'@' : {
					templateUrl : 'app/notice/html/notice-send.html',
					controller : 'NoticeSendController'
				}
			},
			data: {
	            displayName: 'Send Notice'
	        }
		});
	}]);
})();