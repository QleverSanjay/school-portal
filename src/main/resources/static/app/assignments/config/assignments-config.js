(function(){
	angular.module('assignments').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('assignment', {
			url : '/list',
			abstract :true,
			controller : 'assignmentsModelController',
			data: {
				 breadcrumbProxy: 'assignments.list'
	        }
		}).state('assignment.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/assignments/html/assignments-list.html',
					controller : 'AssignmentsController'
				}
			},
			data: {
	            displayName: 'Assignments List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('assignment.view', {
			url : '/view/:id',
			views : {
				'@' : {
					templateUrl : 'app/assignments/html/assignments-view.html',
					controller : 'AssignmentsViewController'
				}
			},
			data: {
	            displayName: 'View Assignments'
	        }
	    })
		.state('assignment.send', {
			url : '/send',
			views : {
				'@' : {
					templateUrl : 'app/assignments/html/assignments-send.html',
					controller : 'AssignmentsSendController'
				}
			},
			data: {
	            displayName: 'Send Assignments'
	        }
		/*})..state('assignment.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/assignments/html/assignments-list.html',
					controller : 'AssignmentsController'
				}
			},
			data: {
	            displayName: 'Assignment List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }*/
		});
	}]);
})();