(function(){
	angular.module('assignments').controller('AssignmentsController',function($rootScope,$localStorage,$resource,$filter,$confirm,$stateParams,DTOptionsBuilder,$scope,$state,$uibModal,CommonServices) {
		$rootScope.loginCount=2;
		$scope.branchId = '';
		$scope.assignmentsList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	    $scope.title = "";
	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		$scope.istecherSpecificAssignments = false;
		$scope.sendByUser = false;
		$scope.isAllAssignments = false;
	
		// Datatable sorting...

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", false)
		.withOption("paging", true)
		.withOption("searching", true)
        .withOption("aaSorting", [])
        .withOption("responsive", true)
        .withOption("aoColumnDefs", [ { "sType": "date-eu", "aTargets": [ 3 ] } ]);
		
		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch;

		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		$scope.title = "Sent assignmentss";
    		$scope.isAllAssignments = true;
    		changeYearList();
    		filterTableByBranch($scope.branchId);
		}
		
		$scope.showMyAssignments = function(){
			$scope.title = "Received Assignments";
			$scope.isAllAssignments = false;
			$scope.message = "";
			$scope.istecherSpecificassignmentss = true;
			filterTableByTeacherBranch($scope.branchId);
		};

		$scope.showAllAssignments = function(){
			$scope.istecherSpecificassignmentss = false;
			$scope.message = "";
			$scope.isAllAssignments = true;
			$scope.title = "Sent Assignments";
			filterTableByBranch($scope.branchId);
		};

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
			filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			$scope.assignmentsList.length = 0;
			if(branchId){
				$resource('notice/getAssignmentsByBranch?branchId='+branchId+'&roleId='
						+$rootScope.roleId+'&userId='+$rootScope.id).query().$promise.then(function(assignmentsList) {
					$scope.assignmentsList.length = 0;
					if(assignmentsList && assignmentsList.length > 0){
						angular.extend($scope.assignmentsList,assignmentsList);
					}
	    		});
			}
		}

		function filterTableByTeacherBranch(branchId){
			$scope.assignmentsList.length = 0;
			if($rootScope.id){
				$resource('assignments/getAssignmentseByBranchForTeacher?branchId='+branchId+'&teacherId='+$rootScope.id).query().$promise.then(function(assignmentsList) {
					$scope.assignmentsList.length = 0;
					if(assignmentsList && assignmentsList.length > 0){
						angular.extend($scope.assignmentsList,assignmentsList);
					}
	    		});
			}
		}
		
		
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			$scope.branchList.length = 0;
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        				changeYearList();
        				if($stateParams.branchId){
        				filterTableByBranch($scope.branchId);
        				}
        				angular.extend($scope.branchList,branchList);
        			}
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		
	    			$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
	    			changeBranchList(false);
	    	
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
							
							$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
							if($stateParams.branchId){
	        					filterTableByBranch($scope.branchId);
							}
	        				if(!isBranchChanged){
	        					filterTableByBranch($scope.branchId);
	        				}
	        			}
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		
		$scope.deleteData = function deleteassignments(id){
		   	 $confirm({text: 'Are you sure you want to delete?'})
		         .then(function() {
		        	 $resource('notice').remove({id : id}).$promise.then(function(resp) {
		    	 			if(resp.status == 'success'){
		            	 		for(var i =0; i < $scope.assignmentsList.length ;i ++ ){
		          			  		var assignments = $scope.assignmentsList[i];
		          			  		if(assignments.id == id){
		          			  			$scope.assignmentsList.splice(i , 1);
		          			  			break;
		          			  		}
		          			  	}	
		    	 			}
			        		$scope.status = resp.status;
			        		$scope.message = resp.message;	
		      			});
		         });
		     };
		
	});


	angular.module('assignments').controller('validateAssignmentsForm',function($resource,$scope,$state) {
		$scope.regExTitle = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});

	/* assignments View Controller..*/	
	angular.module('assignments').controller('AssignmentsViewController',function(CommonServices, $rootScope, userserviceapiurl, $scope,$uibModal,$state,$stateParams,$resource) {
		$scope.assignments = {};
		var teacherId = $rootScope.id;

		$resource('assignments/getById?id=' + $stateParams.id+ ($rootScope.roleId == 4 ? '&teacherId='+teacherId : '')).get().$promise.then(function(assignments) {
			angular.extend($scope.assignments,assignments);
			if($scope.assignments.readStatus == 'N'){
				var notificationToMarkRead  = {
					notification_id :  $scope.assignments.id,
					for_user_id : $scope.assignments.for_user_id
				};

				$rootScope.markAsRead(userserviceapiurl + 'notification/mark/read', notificationToMarkRead).then(function(data){
					$scope.assignments.readStatus = 'Y';
			        console.log(data);
			    }, function (err){
			    	console.log(err);
			    });
			}
		});

		$scope.cancel = function(){
			CommonServices.cancelOperation('assignments.list');
		};
	});
	
	angular.module('assignments').controller('AssignmentsSendController',function($localStorage,CommonServices,Upload,$filter,$rootScope,$scope,modalPopupService,$state,$stateParams,$resource) {
		$scope.assignments = {};
		$scope.hasSearchRecords = false;
		$scope.hasGroupSearchRecords = false;
		$scope.individualIds = [];
		$scope.assignments.individuals = [];
	
		$scope.assignments.groupList = [];
		$scope.assignments.groupIdList = [];
		$scope.academicYearList = [];

		$scope.status = "";
		$scope.message = "";
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isSaveDisabled = false;
		$scope.isIndividualOrGroupSelected = false;
		
		$scope.assignments.instituteId = $localStorage.selectedInstitute;
		$scope.assignments.branchId = $localStorage.selectedBranch;
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);


		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        			}
        			changeYearList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}
    	
    	// Used when teacher login.
    	if($rootScope.roleId == 4){
    		$scope.assignments.branchId = $rootScope.branchId;
    		changeYearList();
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			
	    		angular.extend($scope.instituteList,instituteList);
	    		changeBranchList();
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			if($scope.assignments.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.assignments.instituteId})
					.$promise.then(function(branchList) {
						changeYearList();
					angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){
			if($scope.assignments.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.assignments.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.assignments.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

		$scope.$watch("assignments.individuals", function() {
			checkIsIndividualOrGroupSelected();
		}, true);
		
		function checkIsIndividualOrGroupSelected (){
			if ($scope.assignments.individuals && $scope.assignments.individuals.length > 0) {
				$scope.isIndividualOrGroupSelected = true;
			} else if ($scope.assignments.groupList && $scope.assignments.groupList.length > 0) {
				$scope.isIndividualOrGroupSelected = true;
			} else {
				$scope.isIndividualOrGroupSelected = false;
			}
		}
		
		$scope.$watch("assignments.groupList", function() {
			checkIsIndividualOrGroupSelected();
		}, true);
		
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('assignments.list');
		};
		
		$scope.indivisualStudentButtonValue = "Add Individuals";
		
		$scope.popup = function(){
	    	  var options = {
	    	      templateUrl: 'app/assignments/html/assignments-popup.html',
	    	      controller: 'assignmentsSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

	     $scope.groupPopup = function(){
	    	  var options = {
	    	      templateUrl: 'app/assignments/html/assignments-groupPopup.html',
	    	      controller: 'assignmentsGroupSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };
	     
	     /** Remove item from selected individuals */
	     $scope.removeIndividual = function(id){
	    	 for (var i = 0; i< $scope.assignments.individuals.length; i++){
	    		 var individual = $scope.assignments.individuals[i];
    			 if (individual.id == id) {
    		    	$scope.assignments.individuals.splice(i, 1);
    		    	$scope.individualIds.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };
		
	     /** Remove item from selected individuals */
	     $scope.removeGroup = function(id){
	    	 for (var i = 0; i< $scope.assignments.groupList.length; i++){
	    		 var group = $scope.assignments.groupList[i];
    			 if (group.id == id) {
    		    	$scope.assignments.groupList.splice(i, 1);
    		    	$scope.assignments.groupIdList.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };
	     
		$scope.send = function() {
			var assignments = {
				title : $scope.assignments.title,
				description : $scope.assignments.description,
				category : 'assignment',
				priority : $scope.assignments.priority,
				groupIdList : $scope.assignments.groupIdList,
				individualIdList : $scope.individualIds,
				branchId : $scope.assignments.branchId,
				sentBy : $rootScope.id,
				fileChanegd : $scope.assignments.fileChanegd,
				academicYearId : $scope.assignments.academicYearId
			};
			
			/** This method is used to adding event. It will only call add utility method */
			Upload.upload({
	            url: 'notice/save',
	            method: 'POST',
                data : assignments,
	            file: $scope.assignments.image,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
					$state.transitionTo('assignment.list', {
						status : resp.data.status, 
	        			message : resp.data.message,
	        			instituteId : $scope.assignments.instituteId,
	        			branchId : $scope.assignments.branchId,
					},{reload : true});
				}
				else{
        			$scope.status = resp.data.status;
	        		$scope.message = resp.data.message;
        		}
	        });			
			
			//CommonServices.saveassignments('assignments', $scope.assignments, 'assignments.list');
		};
	});
	
	angular.module('assignments').controller('assignmentsSearchController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl',
	   function($rootScope,$scope,$resource, modalPopupService,$uibModalStack,userserviceapiurl) {

		$scope.clickedForSearch = false;
		$scope.searchedIndividuals = [];	
		/** This method is used to search individuals based on provided details*/
		$scope.search = function(){
			$scope.searchedIndividuals = [];
			console.log($scope.searchData);
			if($rootScope.roleId == 4){
				$resource(userserviceapiurl + 'user/search?search_text='+$scope.searchData.name+
						 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {
	
		    		 if(data.length > 0){
			    		 $scope.hasSearchRecords = true;
			    		 $scope.clickedForSearch = false;
			    		 angular.forEach( data, function(individual){
			    			 individual.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedIndividuals,data);
			    	 }
		    		 else {
		    			 $scope.hasSearchRecords = false;
		    			 $scope.clickedForSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/search?branch_id='+$scope.$parent.assignments.branchId+'&search_text='+$scope.searchData.name+
						 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {
					 if(data.length > 0){
			    		 $scope.hasSearchRecords = true;
			    		 $scope.clickedForSearch = false;
			    		 angular.forEach( data, function(individual){
			    			 individual.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedIndividuals,data);
			    	 }
		    		 else {
		    			 $scope.hasSearchRecords = false;
		    			 $scope.clickedForSearch = true;
		    		 }
				});
			}
	     };
	     
	     
	     /** Add individuals to an array */
	     $scope.addIndividuals  = function(){	    
	    	 console.log($scope.assignments.individuals);
	    	 angular.forEach( $scope.searchedIndividuals, function(individual){
	    		 if (individual.isSelected) {
    		    	$scope.assignments.individuals.push(individual);
    		    	$scope.individualIds.push(individual.id);
    		    }
	    	 });
	    	 modalPopupService.closePopup(null, $uibModalStack);
	     };
	     
	     $scope.cancelPopup = function(){
	    	 modalPopupService.closePopup(null, $uibModalStack);
		    };
	}]);
	
	
	angular.module('assignments').controller('assignmentsGroupSearchController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl',
	  function($rootScope,$scope,$resource,modalPopupService,$uibModalStack,userserviceapiurl) {
		$scope.clickedForGroupSearch = false;
		$scope.searchedGroups = []; 
		/** This method is used to search groups based on provided details*/
		$scope.searchGroup = function(){
			$scope.searchedGroups = [];
			console.log($scope.searchData);
			if($rootScope.roleId == 4){	
				$resource(userserviceapiurl + 'user/groups?search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.assignments.groupIdList).query()
					.$promise.then(function(data) {
		    		 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/groups?branch_id='+$scope.$parent.assignments.branchId+'&search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.assignments.groupIdList).query().
				$promise.then(function(data) {
					 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
				});
			}
				
				
	     };
	     
	     /** Add Groups to an array */
	     $scope.addGroups  = function(){
	    	 angular.forEach( $scope.searchedGroups, function(group){
	    		 if (group.isSelected) {
    		    	$scope.assignments.groupList.push(group);
    		    	$scope.assignments.groupIdList.push(group.id);
    		    }
	    	 });
	    	 modalPopupService.closePopup(null, $uibModalStack);
	     };
	     
	     $scope.cancelPopup = function(){
	    	 modalPopupService.closePopup(null, $uibModalStack);
		    };
	}]);
})();