(function(){
	angular.module('shopping').config(['$stateProvider', function($stateProvider) {
		$stateProvider.state('shopping', {
			url : 'shopping',
			abstract :true,
			controller : 'ShoppingModelController',
		}).state('shopping.view', {
			url : '/view',
			views : {
				'@' : {
					templateUrl : 'app/shopping/html/shopping-view.html',
					controller : 'ShoppingController'
				}
			},
			data: {
	            displayName: 'Shopping'
	        },
		});
	}]);
})();