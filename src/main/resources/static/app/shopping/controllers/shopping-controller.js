(function(){	
	angular.module('shopping').controller('ShoppingController',function($scope,$state,Upload, shoppingurl, 
			InitializationService, $resource,$sce) {
		InitializationService.initialize();
		/** This is written to get single Shopping object to display on the edit screen. */
		$scope.shopping = {};
		$scope.isUpdateShopping = true;		
		$scope.currentProjectUrl = $sce.trustAsResourceUrl(shoppingurl);
	});
})();