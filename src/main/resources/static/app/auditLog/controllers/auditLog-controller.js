(function() {
	angular.module('auditLog').controller('auditLogController',

	function($resource, $scope, $state,$rootScope, $uibModal,$uibModalStack, $localStorage, CommonServices, Upload, StateList, BoardList, $filter, modalPopupService,$http,DTOptionsBuilder) {
	
		$scope.entityType = "students";
		$scope.operationType = "update";
		$scope.newData = [];
		$scope.auditEntityList = [{id:1, entityType: 'Student',entityLabel:'students'},{id:2, entityType: 'Fees',entityLabel:'fees'}];
		$scope.auditOperationList = [{id:1, operationType: 'Update',operationLabel:'update'},{id:2, operationType: 'Delete',operationLabel:'delete'} ];
		$scope.enityId = $scope.auditEntityList[0].entityLabel;
		$scope.operationId = $scope.auditOperationList[0].operationLabel;
		var draw = 0;
		$scope.instituteList = [];
		$scope.branchList = [];
		
		
		$scope.auditList = function(){

			$resource($scope.entityType+'/audit/'+$scope.operationType).query().$promise.then(
				function(res){
					console.log(res);
					$scope.auditData = res;
				},
				function(err){
				console.log(err);
			})
		}
		function getFilterObject(){
			var filter = {'instituteId' : $scope.instituteId,
		        	'branchId' : $scope.branchId,
		        	'standardId' : $scope.standardId,
		        	'divisionId' : $scope.divisionId,
		        	'rollNumber' : $scope.rollNumber,
		        	'registrationCode':$scope.registrationCode,
		        	'status' : $scope.status,
		        	'headId' : $scope.headId,
		        	'feesId' : $scope.feesId,
		        	'casteId' : $scope.casteId,
		        	'fromDate' : $scope.fromDateStr,
		        	'toDate' : $scope.toDateStr,
		        	'isSearchClicked' : $scope.isSearchClicked +''
		    };
			return filter;
		}
		
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", true)
		.withOption("scrollX", true)
        .withOption("responsive", true);

	    /*.withOption('ajax', {
	         url: 'fees/report',
	         type: 'POST',
	         contentType: 'application/json',

	         data: function(data, dtInstance) {
	        	 
	        	 var filter = getFilterObject();
	        	 $scope.isSearchClicked = 'N';
	        	 data.columns.length = 0;
	        	 filter.tableOptions = data;
	        	 return JSON.stringify(filter);
	         },
	         error:function (xhr, error, thrown) {
	        	 $scope.message = thrown;
	        	 $scope.$apply();
	        	 var processingDialog = document.getElementById("feesReport_processing");
	        	 processingDialog.style.visibility='hidden' ;
	        	 if(xhr.responseText.indexOf("Login Page") != -1){
	        		 $state.transitionTo('login.logout',{},{reload : true});
	        	 }
	         }
	     })*/
		
		$scope.auditList();
		$scope.changeEntityList = function(typeName){
			$scope.entityType  = typeName;
			if($scope.branchID)
				$scope.changeStandardList($scope.branchID);
			else
				$scope.auditList();
		}
		$scope.changeOperationList = function(typeName){
			$scope.operationType  = typeName;
			if($scope.branchID)
				$scope.changeStandardList($scope.branchID);
			else
				$scope.auditList();
		}
		$scope.checkData = function(data){
	    	$scope.isUploadStatus = false;
	    	var options = {
	    		templateUrl: 'app/auditLog/html/auditDataPopUp.html',
	   	      	scope : $scope,
    	    };
	    	modalPopupService.openPopup(options);
	    	$scope.newData = JSON.parse(data.newData);
	    	$scope.oldData = JSON.parse(data.oldData);
	    };
	    
	    $scope.downloadReport = function(){
	    	var url = $scope.entityType+'/auditLog/'+$scope.operationType+'/'+$scope.branchID;
			window.location = url;
	    }
	    
	   
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			$scope.changeBranchList();	
    			}
	    		angular.extend($scope.instituteList,instituteList);
			});
    	
	    
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			//$scope.changeStandardList();
        			angular.extend($scope.branchList,branchList);
    		});
    	
	    
	    $scope.changeBranchList = function(institute_id){
			$scope.branchList.length = 0;
			if(institute_id){
				$resource('masters/getFilterdBranch/:id').query({id : institute_id})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
						//$scope.changeStandardList();
				});	
			}
		};
		
		$scope.changeStandardList = function(branchId){
			$scope.branchID = branchId;
			if($scope.branchID){
				$resource($scope.entityType+'/audit/'+$scope.operationType+'/'+$scope.branchID).query().$promise.then(
						function(res){
							console.log(res);
							$scope.auditData = res;
						},
						function(err){
						console.log(err);
					})
			}
			
		}
	    
	    
});

})();