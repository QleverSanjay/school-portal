(function(){
	angular.module('auditLog').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('auditLog', {
			url : '',
			abstract :true,
			controller : 'auditLogController',
		}).state('auditLog.page', {
			url : '/auditLog/page',
			views : {
				'@' : {
					templateUrl : 'app/auditLog/html/auditLog.html',
					controller : 'auditLogController'
				}
			},
			data: {
	            displayName: 'About Us'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	BoardList : function($resource){
		        	return $resource('masters/boards').query().$promise;
		        }
	        }
		});
	}]);
})();