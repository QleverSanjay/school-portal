(function(){
	angular.module('report').controller('ReportController',function($rootScope,$resource,$filter, userserviceapiurl , $confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,modalPopupService, CommonServices) {
	    
		$scope.reportList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	//	angular.extend($scope.reportList,ReportListData);
	    $scope.downloadReportUrl = userserviceapiurl + "report/download?resultId=";

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.branchName = "";
		$scope.standardList = [];
		$scope.divisionList = [];
		$scope.resultTypeList = [];

		$scope.idsToDelete = [];

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true)
        .withOption("aaSorting", [])
        .withOption("scrollX", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.idsToDelete.length = 0;
    		filterTableByBranch($scope.branchId);
		};
		
		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		changeYearList();
    		filteStandardsByTeacher($scope.branchId);
		}


		//TODO Ask correct or not
		function filteStandardsByTeacher(branchId){
			if($rootScope.id){
				$resource('masters/getFilterdStandardsByTeacher?branchId='+branchId+'&teacherId='+$rootScope.id).query().$promise.then(function(standardList) {
					$scope.standardList.length = 0;
					if(standardList && standardList.length > 0){
						angular.extend($scope.standardList,standardList);
						$scope.standardId = $scope.standardList[0].id;
						changeDivisionList();
						changeResultTypeList();
					}
	    		});
			}
		}

		function filterTableByBranch(){
			if($scope.branchId){
				
				var url = 'reports/getReports?branchId='+$scope.branchId+'&academicYearId='+$scope.academicYearId
				+'&standardId='+$scope.standardId
				+'&divisionId='+$scope.divisionId+'&semester='+$scope.semester+'&resultTypeId='+$scope.resultTypeId;
				
				if($scope.rollNumber) {
					url = url + '&rollNumber='+$scope.rollNumber;
				}
				$resource(url)
						.query().$promise.then(function(reportList) {

					$scope.reportList.length = 0;
					if(reportList && reportList.length > 0){
						angular.extend($scope.reportList,reportList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId == 6){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				changeYearList();
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $stateParams.instituteId){
	    			$scope.instituteId = $stateParams.instituteId;
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
						$scope.branchId = (isBranchChanged ? branchList[0].id  : 
							(!$stateParams.branchId ? branchList[0].id : $stateParams.branchId));
        				if(!isBranchChanged){
							filterTableByBranch();
						}
        			}
					angular.extend($scope.branchList,branchList);
					changeYearList();
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}

					if($rootScope.roleId != 4){
						changeStandardList();
					}
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		
		$scope.changeStandardList = function(){
			changeStandardList();
		};
		
		$scope.changeDivisionList = function(){
			changeDivisionList();
		};
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					if(standardList != null && standardList.length > 0){
						$scope.standardId = standardList[0].id;
        			}
					
					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					changeResultTypeList();
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
							$scope.divisionId = divisionList[0].id;
						}
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.changeResultTypeList = function(){
			changeResultTypeList();
		};
		
		// Change divisionList as per the standard selected.
		function changeResultTypeList(){
			//$scope.divisionList.length = 0;
			if($scope.standardId){
				$resource('masters/resultTypeMasters').query().$promise.then(
						 function(resultTypeList) {
							 $scope.resultTypeList.length = 0;
							if(resultTypeList != null && resultTypeList.length > 0){
								$scope.resultTypeId = resultTypeList[0].id;
		        			}
							
							angular.extend($scope.resultTypeList,resultTypeList);
						});
			}
			else {
				$scope.resultTypeList.length = 0;
			}
		}

		$scope.downloadReportTemplate = function (){
			var url = "reports/downloadReportTemplate?branchId="+$scope.branchId+
				"&academicYearId="+$scope.academicYearId+"&standardId=" + $scope.standardId+
				"&divisionId="+$scope.divisionId+"&semester="+$scope.semester+"&resultTypeId="+$scope.resultTypeId;
			window.location.href = url;
		};

	    /** This method is used to open Modal */ 
	    $scope.openUploadReportDialog = function(){
	    	  var options = {
	    	      templateUrl: 'app/reports/html/report-bulk-upload.html',
	    	      controller: 'ReportFileUploadController',
	    	      scope : $scope
	    	     
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

	     $scope.selectToDelete = function(item){
	    	 if(item.checked == true){
	    		 $scope.idsToDelete.push(item.id);	 
	    	 }
	    	 else {
	    		 $scope.idsToDelete.splice($scope.idsToDelete.indexOf(item.id));
	    	 }
	     };

	     $scope.deleteData = function deleteReport(){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('reports').remove({ids : $scope.idsToDelete}).$promise.then(function(resp) {
        	 		if(resp.status == 'success'){
        	 			var indexToDelete = [];
    	        		for(var i =0; i < $scope.reportList.length ;i ++ ){
          			  		var report = $scope.reportList[i];
          			  		for(var j =0; j< $scope.idsToDelete.length; j++){
    	      			  		if(report.id == $scope.idsToDelete[j]){
    	      			  			indexToDelete.push(i);
    	      			  			$scope.idsToDelete.splice(j , 1);
    	      			  			break;
    	      			  		}
          			  		}
          			  	}
    	        		for(var i =0; i < indexToDelete.length; i++){
    	        			$scope.reportList.splice(indexToDelete[i] , 1);
    	        		}
        	 		}
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});


	/** This method is used for uploading data */
	angular.module('report').controller('ReportFileUploadController',function($scope,Upload, $uibModalStack, modalPopupService,$state) {
		$scope.uploadReport = function (file) {
	        Upload.upload({
	            url: 'reports/uploadReportFile',
	            data : {
	            	instituteId : $scope.instituteId,
	            	branchId : $scope.branchId,
	            	academicYearId : $scope.academicYearId,
	            	standardId : $scope.standardId,
	            	divisionId : $scope.divisionId,
	            	semester : $scope.semester,
	            	resultTypeId : $scope.resultTypeId,
	            	title : $scope.title
	            },
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}

	        	$scope.$parent.status = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	angular.extend($scope.reportList,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
	

/*
  	angular.module('event').controller('validateReportForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,35}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});
	
	
  	angular.module('report').controller('ReportAddController',function($scope,$state,$stateParams,$resource,CommonServices) {
		$scope.report = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.divisionList = [];
		$scope.reportHeaderValue = "Add Report";
		
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($scope.$root.loggedInUser.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $scope.$root.loggedInUser.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				$scope.branchId = branchList[0].id;
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($scope.$root.loggedInUser.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			$scope.report.instituteId = instituteList[0].id;
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
 
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.report.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.report.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.report.branchId = branchList[0].id;
							changeDivisionList();
	        			}
						else {
							$scope.divisionList.length = 0;
						}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		$scope.changeDivisionList = function (){
			changeDivisionList();
		};
		
		function changeDivisionList(){
			if($scope.report.branchId){
				$resource('division/getDivisionByBranch?branchId='+$scope.report.branchId).query()
					.$promise.then(function(divisionList) {
						
					$scope.divisionList.length = 0;
					angular.extend($scope.divisionList,divisionList);									
	    		});
			}
		}
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('report.list');
		};
		
		$scope.save = function() {
			var report = {
				id : $scope.report.id,
				displayName : $scope.report.displayName,
				code : $scope.report.code,
				branchId : $scope.report.branchId,
				divisionIdList : $scope.report.divisionIdList
			};			
		
			$resource('report').save(report).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('report.list',
					{
						instituteId : $scope.report.instituteId,
	        			branchId : $scope.report.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});
	
	This Controller is Used for Report Edit functionality
	angular.module('report').controller('ReportEditController',function($scope,$state,$stateParams,$resource, CommonServices) {
		
		$scope.report = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.divisionList = [];
		$scope.reportHeaderValue = "Edit Report";
		
		$resource('reports/:id').get({id : $stateParams.id}).$promise.then(function(reportToEdit) {
			angular.extend($scope.report,reportToEdit);
			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($scope.$root.loggedInUser.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.report.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.report.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.report.branchId = branchList[0].id;
						}
						changeDivisionList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
	
		$scope.changeDivisionList = function (){
			changeDivisionList();
		};
		
		function changeDivisionList(){
			if($scope.report.branchId){
				$resource('division/getDivisionByBranch?branchId='+$scope.report.branchId).query()
					.$promise.then(function(divisionList) {
						
					$scope.divisionList.length = 0;
					angular.extend($scope.divisionList,divisionList);									
	    		});
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.cancel = function(){
			$state.transitionTo('report.list',{reload : true});
		};
		
		$scope.save = function() {
			var report = {
				id : $scope.report.id,
				displayName : $scope.report.displayName,
				code : $scope.report.code,
				branchId : $scope.report.branchId,
				divisionIdList : $scope.report.divisionIdList
			};

			$resource('report').save(report).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('report.list',
					{
						instituteId : $scope.report.instituteId,
	        			branchId : $scope.report.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
		        }
			});
		};
	});
*/
})();

