(function(){
	angular.module('report').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('report', {
			url : '/report',
			abstract :true,
			controller : 'ReportModelController',
			data: {
				 breadcrumbProxy: 'report.list'
			}
		}).state('report.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/reports/html/report-list.html',
					controller : 'ReportController'
				}
			},
			data: {
	            displayName: 'Report List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('report.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/reports/html/report-add.html',
					controller : 'ReportAddController'
				}
			},
			data: {
	            displayName: 'Add Report'
	        }
		})/*.state('report.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/report/html/report-add.html',
					controller : 'ReportEditController'
				}
			},
			data: {
	            displayName: 'Edit Report'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	DistrictList : function($resource){
	        		return $resource('masters/districtMasters').query().$promise;
	        	},
	        	TalukaList : function($resource){
	        		return $resource('masters/talukaMasters').query().$promise;
	        	}
	        }
		})*/;
	}]);
})();