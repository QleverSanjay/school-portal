(function(){
	angular.module('fees').config([ '$stateProvider', function($stateProvider) {
		
		$stateProvider.state('fees', {
			url : '/fees',
			abstract :true,
			controller : 'FeesModelController',
			data: {
				 breadcrumbProxy: 'fees.manage'
	        }
		}).state('fees.manage', {
			url : '/manage',
			views : {
				'@' : {
					templateUrl : 'app/fees/html/manage-fees.html',
					controller : 'FeesController'
				}
			},
			data: {
	            displayName: 'Manage Fees'
	        },
			/*resolve : {
	        	FeesListData : function($resource){
	        		return $resource('fees').query().$promise;
	        	}	        	
	        }*/
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : '',
	        	message : '',
	        	status : ''
	        }
		}).state('fees.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/fees/html/fees-add.html',
					controller : 'FeesAddController'
				}
			},
			data: {
	            displayName: 'Add Fees'
	        },
			resolve : {
	        	CasteList : function($resource){
	        		return $resource('masters/casteMasters').query().$promise;
	        	},
		        CurrencyList : function($resource){
		        	return $resource('masters/currencies').query().$promise;
		        }
	        }
		}).state('fees.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/fees/html/fees-add.html',
					controller : 'FeesEditController'
				}
			},
			data: {
	            displayName: 'Edit Fees'
	        },
			resolve : {
	        	CasteList : function($resource){
	        		return $resource('masters/casteMasters').query().$promise;
	        	},
		        CurrencyList : function($resource){
		        	return $resource('masters/currencies').query().$promise;
		        }
	        }
		});
	}]);
})();