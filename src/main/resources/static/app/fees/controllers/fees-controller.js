(function(){
		angular.module('fees').controller('FeesController',function($rootScope, $resource,$filter,$confirm, $localStorage,
				DTOptionsBuilder,$stateParams,$scope,$state, modalPopupService,CommonServices) {

			$scope.feesList = [];
			$scope.message = $stateParams.message;
		    $scope.status = $stateParams.status;

	    	$scope.branchList = [];
			$scope.instituteList = [];
			$scope.academicYearList = [];
			$scope.isSingleBranch = false;
			$scope.branchName = "";
			$scope.isUploadingFailed = false;
			$scope.editDueDateAllowed = true;

			$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
			$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

			$scope.dtOptions =  DTOptionsBuilder.newOptions()
			.withOption("bAutoWidth", false)
			.withOption("paging", true)
			.withOption("searching", true)
	        .withOption("responsive", true)
	        .withOption("scrollX", true);

			$scope.filterTableByBranch = function(){
				$scope.status = "";
	    		$scope.message = "";
	    		$scope.isUploadingFailed = false;
	    		filterTableByBranch($scope.branchId);
			};

			function filterTableByBranch(branchId){
				if(branchId){
					$resource('fees/getFeesByBranch?branchId='+$scope.branchId).query().$promise.then(function(feesList) {
					$scope.feesList.length = 0;
					if(feesList && feesList.length > 0){
						angular.extend($scope.feesList,feesList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
	        				if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}
	        			}
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
								$scope.academicYearId = acadmicYear.id;
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}

	    /** This method is used to open Modal */ 
	    $scope.openUploadFeesDialog = function(){
	    	$scope.isUploadUpdate = false;
	    	var options = {
	    			templateUrl : 'app/fees/html/fees-bulk-upload.html',
	    			controller : 'FeesFileUploadController',
	    			scope : $scope
	    	  };
	    	  modalPopupService.openPopup(options);
	     };

	    $scope.uploadUpdateFees = function(){
	    	$scope.isUploadUpdate = true;
	    	  var options = {
	    			templateUrl : 'app/fees/html/fees-bulk-upload.html',
	    			controller : 'FeesFileUploadController',
	    			scope : $scope
	    	  };
	    	  modalPopupService.openPopup(options);
	     };

		 $scope.deleteData = function deleteFees(id){
			 $confirm({text: 'Are you sure you want to delete?'}).then(function() {
        	 	$resource('fees').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.feesList.length ;i ++ ){
      			  		var fees = $scope.feesList[i];
      			  		if(fees.id == id){
      			  			$scope.feesList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
		      });
	     }; 
	});

	angular.module('fees').controller('FeesDisplayTemplateInfoController',function($rootScope,$scope, $resource, modalPopupService, $uibModalStack, $state) {
		$scope.template = $scope.$parent.fees.displayTemplates[0];
	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
	
	angular.module('fees').controller('FeesDisplayTemplateController',function($rootScope,$scope, $resource, modalPopupService, $uibModalStack, $state) {
		$scope.displayTemplateList = [];
		$scope.selectedTemplate = {};
		if($scope.$parent.displayTemplates){
			var layouts = JSON.stringify($scope.$parent.displayTemplates);
			layouts = JSON.parse(layouts);
			for(var i=0; i < layouts.length ; i++){
				var layObj = layouts[i];
				$scope.displayTemplateList.push(layObj);
			}
		}

		$scope.addLayout = function (){
			if($scope.displayTemplateList){
				$scope.fees.displayTemplateId = $scope.selectedTemplate.id;
				for(var i=0; i < $scope.displayTemplateList.length; i++){
					var obj = $scope.displayTemplateList[i];
					if(obj.id == $scope.selectedTemplate.id){
						$resource('displayTemplate?displayTemplateId='+$scope.selectedTemplate.id).get()
								.$promise.then(function(template) {

							$scope.$parent.fees.displayTemplates.length = 0;
							var totalAmount = 0;
							for(var i=0; i<template.displayHeadList.length; i++){
								var head = template.displayHeadList[i];
								totalAmount = head.amount + totalAmount;
							}
							template.totalAmount = totalAmount;
							$scope.$parent.fees.displayTemplates.push(template);
							modalPopupService.closePopup(null, $uibModalStack);
			    		});
						break;
					}
				}
			}
			else {
				modalPopupService.closePopup(null, $uibModalStack);
			}
		};

	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});

	angular.module('fees').controller('validateFeesForm',function($resource,$scope,$state) {
		$scope.regExAmount = /^(0|[1-9]\d*)$/;

		angular.element('').trigger('focus');
		$scope.focusOn = true;
		$scope.validateReminder = function (){
		validateReminder()
		
		};
		function validateReminder() {
			if($scope.fees.getReminderStartDate()){
				if($scope.fees.getIsEmail().equals("N") && 
						$scope.fees.getIsSms().equals("N") && 
						$scope.fees.getIsNotification().equals("N")){

						$scope.message= "Either email or sms or notification is required.";
					}
				else if($scope.fees.getFrequency()==" " || $scope.fees.getFrequency()=="undefined"){
					$scope.message= "Frequency is required.";
					
				}
			}
			else if($scope.fees.getFrequency()){
				if($scope. fees.getIsEmail().equals("N") && 
						$scope.fees.getIsSms().equals("N") && 
						$scope.fees.getIsNotification().equals("N")){

					$scope.message= "Either email or sms or notification is required.";
					}
				else if($scope.fees.getReminderStartDate()==" " || $scope.fees.getReminderStartDate()=="undefined"){
					$scope.message= "reminder start date is required.";
				}
				
			}
			else if(!$scope.fees.getIsEmail()==("N") || !$scope.fees.getIsSms()==("N") || !$scope.fees.getIsNotification()==("N")){
				 if($scope.fees.getReminderStartDate()==" " || $scope.fees.getReminderStartDate()=="undefined"){
					 $scope.message ="reminder start date is required.";
				}
				 else if($scope.fees.getFrequency()==" " || $scope.fees.getFrequency()=="undefined"){
					 $scope.message = "Frequency is required.";
						
					}
			}
			
			
		}
		
		
		
		
		$scope.validateDates = function (){
			validateDates();
		};
		function validateDates() {
		    if (!$scope.fees) return;
		    if ($scope.addFees.fromDate.$error.invalidDate || $scope.addFees.toDate.$error.invalidDate || 
		    		$scope.addFees.dueDate.$error.invalidDate) {
		        $scope.addFees.fromDate.$setValidity("invalidDate", true);  //already invalid (per validDate directive)
		    } else {
		        //depending on whether the user used the date picker or typed it, this will be different (text or date type).  
		        //creating a new date object takes care of that.
		    	$scope.addFees.fromDate.$error = {};
		    	$scope.addFees.toDate.$error = {};
		    	$scope.addFees.dueDate.$error = {};
		    	$scope.formValid = true;
		    	
		    	var dueDate = new Date($scope.fees.dueDate);
		        var toDate = new Date($scope.fees.toDate);
		        var fromDate = new Date($scope.fees.fromDate);
		        var todaysDate = new Date();
		        
		        var tempDueDate = dueDate;
		        tempDueDate.setHours(0,0,0);
		        todaysDate.setHours(0, 0, 0);
		        if(todaysDate > tempDueDate){
		        	$scope.hideReminder = true;
		        }
		        else {
		        	$scope.hideReminder = false;
		        }

		        var validationFailed = false;

		        if(isNaN(fromDate)){
		        	$scope.addFees.fromDate.$error.fromDateRequired =  true;
		        	validationFailed = true;
		        	$scope.formValid = false;
		        }
		        else {
		        	$scope.addFees.fromDate.$error.fromDateRequired =  false;
		        }
		        if(isNaN(toDate)){
		        	$scope.addFees.toDate.$error.toDateRequired =  true;
		        	validationFailed = true;
		        	$scope.formValid = false;
		        }
		        else {
		        	$scope.addFees.toDate.$error.toDateRequired =  false;
		        }

		        if(!validationFailed){
		        	fromDate.setHours(0, 0, 0, 0);
		        	toDate.setHours(0, 0, 0, 0);
		        	var isToDateInValid = (toDate <= fromDate);
		        	if(isToDateInValid){
		        		$scope.formValid = false;	
		        	}
		        	
		        	$scope.addFees.toDate.$error.endBeforeStart =  isToDateInValid;
		        }

		        if(isNaN(dueDate)){
		        	$scope.addFees.dueDate.$error.dueDateRequired =  true;
		        	validationFailed = true;
		        	$scope.formValid = false;
		        }
		        else {
		        	$scope.addFees.dueDate.$error.dueDateRequired =  false;
		        }

		        if(!validationFailed){
		        	dueDate.setHours(0, 0, 0, 0);
		        	//var isDueDateInValid = (dueDate <= fromDate || dueDate >= toDate);
		        	var isDueDateInValid = (dueDate >= toDate);
		        	if(isDueDateInValid){
		        		$scope.formValid = false;
		        	}
		        	$scope.addFees.dueDate.$error.dueBeforeEnd = isDueDateInValid;
		        }
		    }
		}
	});

	angular.module('fees').controller('FeesAddController',function(
			$rootScope,$scope,$filter,$state, $localStorage, modalPopupService, CasteList, CurrencyList, $stateParams,$resource,CommonServices) {
		$scope.fees = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.headList = [];
		$scope.isOpenStartDate = false;
		$scope.isOpenEndDate = false;
		$scope.isOpenDueDate = false;
		$scope.isBranchSelected = false;
		$scope.casteList = CasteList;
		$scope.currencyList = CurrencyList;
		$scope.minDate = new Date();
		$scope.feesHeaderValue = "Add Fees";
		$scope.standardList = [];
		$scope.divisionList = [];
		$scope.studentList = [];
		$scope.schemeCodes = [];
		$scope.feesCodes = [];
		$scope.filteredStudents = [];
		$scope.displayTemplates = [];
		$scope.fees.displayTemplates = [];
		$scope.lateFeesList = [];
		$scope.fees.currency = 'INR';

		$scope.acadmicStartDate; $scope.acadmicEndDate;
		

		$scope.removeLayout = function (id){
			$scope.fees.displayTemplateId = "";
			$scope.fees.displayTemplates.length = 0;
		};

		$scope.viewTemplate = function (id){
			var options = {
		    	      templateUrl: 'app/fees/html/fees-template-info.html',
		    	      controller: 'FeesDisplayTemplateInfoController',
		    	      scope : $scope
	    	    };
	    	    modalPopupService.openPopup(options);
		};

		$scope.fees.instituteId = $localStorage.selectedInstitute;
		$scope.fees.branchId = $localStorage.selectedBranch;

		$scope.validateSchemeCode = function (){
			if($scope.fees.schemeCodeId){
				var schemeCodeGatewayId = "";
				var seedSchemeCodeGatewayId = "";
				for(var i = 0; i< $scope.schemeCodes.length; i++){
					var scheme = $scope.schemeCodes[i];
					if(scheme.id == $scope.fees.schemeCodeId){
						schemeCodeGatewayId = scheme.gatewayId;
					}
					if(scheme.id == $scope.fees.seedSchemeCodeId){
						seedSchemeCodeGatewayId = scheme.gatewayId;
					}
				}
				if(seedSchemeCodeGatewayId && seedSchemeCodeGatewayId != schemeCodeGatewayId){
					$scope.seedSchemeCodeErrMsg = "Split Payment cannot be possible with these scheme codes.";
					return false;
				}
			}
			return true;
		}

		$scope.changeStandardList = function(){
			changeStudentList();
			$scope.fees.displayTemplates.length = 0;
			$scope.fees.displayTemplateId = "";
			$scope.displayTemplates.length = 0;
			changeDisplayLayouts();
			changeLateFeesDetail();
			changeStandardList();
		};

		$scope.changeDivisionList = function(){
			changeDivisionList();
		};

		$scope.filterStudents = function(){
			$scope.filteredStudents.length = 0;
			if($scope.studentList){
				for(var i =0; i< $scope.studentList.length; i++){
					var student = $scope.studentList[i];
					var addObj = true;
					if($scope.fees.casteId && student.casteId != $scope.fees.casteId){
						addObj = false;
					}
					else if($scope.fees.standardId && student.standardId != $scope.fees.standardId){
						addObj = false;
					}
					else if($scope.fees.divisionId && student.divisionId != $scope.fees.divisionId){
						addObj = false;
					}
					else if($scope.fees.feesCodeId && student.feesCodeId != $scope.fees.feesCodeId){
						addObj = false;
					}
					if(addObj){
						$scope.filteredStudents.push(student);
					}
				}
			}
		};

		function changeDivisionList(){
			if($scope.fees.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.fees.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
//							$scope.fees.divisionId = $scope.fees.divisionId ? $scope.fees.divisionId 
//									: '';
						}
						$scope.filterStudents();
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
				$scope.filterStudents();
			}
		}

		$scope.changeStudentList = function(){
			changeStudentList();
		};

		function changeStudentList(){
			$scope.fees.studentId = '';
			$scope.studentList.length = 0;
			if($scope.fees.branchId){
				$scope.$broadcast('angucomplete-alt:clearInput');
				var branchId = $scope.fees.branchId;

				if(branchId){
					var requestParams = 'branchId='+branchId;
					$resource('students/getStudentsByStandardDivision?'+requestParams).query().$promise.then(function(matchingStudentList) {

						if(matchingStudentList && matchingStudentList.length > 0){
							angular.extend($scope.studentList, matchingStudentList);
							angular.extend($scope.filteredStudents, $scope.studentList);
							$scope.isBranchSelected = false;
						}				
		    		});
					
				} else {
					$scope.isBranchSelected = true;
				}
			}
		}

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.fees.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.fees.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					 getSchemeCodes();
					 getFeesCodes();
					 changeYearList();
					 changeHeadList();

					angular.extend($scope.standardList,standardList);
					changeDivisionList();
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		function getFeesCodes(){
			if($scope.fees.branchId){
				$scope.feesCodes.length = 0;
				$resource('fees/fees-code/list/:id').query({id : $scope.fees.branchId}).$promise.then(
				 function(feesCodeList) {
					 $scope.feesCodes.length = 0;
					 angular.extend($scope.feesCodes, feesCodeList);
				});
			}
			else {
				$scope.feesCodes.length = 0;
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.fees.instituteId = $rootScope.instituteId;
        	changeBranchList();
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	function getSchemeCodes(){
    		if($scope.fees.branchId){
    			$scope.schemeCodes.length = 0;
    			$resource('fees/schemeCodes?branchId='+$scope.fees.branchId)
    			.query().$promise.then(function(instituteList) {
    				
    	    		angular.extend($scope.schemeCodes,instituteList);
    			});
    		}
    		else {
    			$scope.schemeCodes.length = 0;
    		}
    	}

    	// Change Year dropdown according to branch.
    	$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.fees.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.fees.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								if(!acadmicYear.fromDate instanceof Date){
									$scope.acadmicStartDate = new Date(acadmicYear.fromDate);
									acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
								}
								else {
									$scope.acadmicStartDate = acadmicYear.fromDate;
								}
								if(!acadmicYear.toDate instanceof Date){
									$scope.acadmicEndDate = new Date(acadmicYear.toDate);
									acadmicYear.toDate = new Date(acadmicYear.toDate);
								}
								else {
									$scope.acadmicEndDate = acadmicYear.toDate;
								}
								$scope.fees.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.fees.instituteId || $scope.fees.instituteId ==0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.fees.instituteId})
					.$promise.then(function(branchList) {
						changeStandardList();
						changeDisplayLayouts();
						changeLateFeesDetail();
						changeStudentList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.changeHeadList = function (){
			changeHeadList();
		};

		function changeHeadList(){
			if($scope.fees.branchId){		
				$resource('head/getHeadByBranchForDashboard?branchId='+$scope.fees.branchId).query()
					.$promise.then(function(headList) {
					 $scope.headList.length = 0;
					if(headList != null && headList.length > 0){
						$scope.fees.headId = headList[0].id; 
					}	
					angular.extend($scope.headList,headList);									
	    		});
			}
		}

		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		};
		$scope.openEndDateDialog = function fnOpenDialog(){
			$scope.isOpenEndDate = true;
		};
		$scope.openDueDateDialog = function fnOpenDialog(){
			$scope.isOpenDueDate = true;
		};
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('fees.manage');
		};

		$scope.searchStudentByStr = function(str) {
			var matches = [];
			$scope.filteredStudents.forEach(function(student) {
			    //var fullName = person.firstName + ' ' + person.surname;\
				var description = 'Name : '+ student.firstname + ' ' + student.surname + (student.rollNumber ? ', Roll No. : ' + student.rollNumber : '') + (student.registrationCode ? ', Reg No. :  ' +  student.registrationCode : '');

				if (( student.firstname && student.firstname.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
			        (student.surname && student.surname.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
			        (student.rollNumber && student.rollNumber.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
			        (student.registrationCode && student.registrationCode.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {

			    		student.description = description;
			    		matches.push(student);
			    }
			  });
			  return matches;
		};


		 $scope.dayArray = [{"label" : "Monday", "value" : "1"},
                   {"label" : "Tuesday", "value" : "2"},
       		    {"label" : "Wednesday", "value" : "3"},
       		    {"label" : "Thursday", "value" : "4"},
       		    {"label" : "Friday", "value" : "5"},
       		    {"label" : "Saturday", "value" : "6"},
       		    {"label" : "Sunday", "value" : "7"}
       	    ];
		  $scope.monthDayArray = [{"label" : "1", "value" : "1"}, {"label" : "2", "value" : "2"},
		          			    {"label" : "3", "value" : "3"}, {"label" : "4", "value" : "4"},
		          			    {"label" : "5", "value" : "5"}, {"label" : "6", "value" : "6"},
		          			    {"label" : "7", "value" : "7"}, {"label" : "8", "value" : "8"},
		          			    {"label" : "9", "value" : "9"}, {"label" : "10", "value" : "10"},
		          			    {"label" : "11", "value" : "11"}, {"label" : "12", "value" : "12"},
		          			    {"label" : "13", "value" : "13"}, {"label" : "14", "value" : "14"},
		          			    {"label" : "15", "value" : "15"}, {"label" : "16", "value" : "16"},
		          			    {"label" : "17", "value" : "17"}, {"label" : "18", "value" : "18"},
		          			    {"label" : "19", "value" : "19"}, {"label" : "20", "value" : "20"},
		          			    {"label" : "21", "value" : "21"}, {"label" : "22", "value" : "22"},
		          			    {"label" : "23", "value" : "23"}, {"label" : "24", "value" : "24"},
		          			    {"label" : "25", "value" : "25"}, {"label" : "26", "value" : "26"},
		          			    {"label" : "27", "value" : "27"}, {"label" : "28", "value" : "28"},
		          			    {"label" : "29", "value" : "29"}, {"label" : "30", "value" : "30"},
		          			    {"label" : "31", "value" : "31"}
		          		    ];
		          		    $scope.isDaySelected = function(){
		          		    	 var flag = false;
		          		    	 if($scope.fees.feesWeekDays){
		          		    		 angular.forEach($scope.fees.feesWeekDays, function(isSelected, day) {
		          			             if(isSelected){
		          			            	 flag = true;
		          			             }
		          			         });	 
		          		    	 }
		          		    	 return flag;
		          		     };
		 

		$scope.openSelectLayoutPopup = function (){
			var options = {
	    	      templateUrl: 'app/fees/html/fees-template-select.html',
	    	      controller: 'FeesDisplayTemplateController',
	    	      scope : $scope
    	    };
    	    modalPopupService.openPopup(options);
		};

		function changeDisplayLayouts() {
			if($scope.fees.branchId){
				$resource('displayTemplates?branchId='+$scope.fees.branchId).query()
					.$promise.then(function(displayTemplates) {
						angular.extend($scope.displayTemplates,displayTemplates);
				});
			}
		}

 		function changeLateFeesDetail(){
 			if($scope.fees.branchId){
 				$scope.lateFeesList.length = 0;
 				$resource('fees/getAllLateFees?branchId='+$scope.fees.branchId).query()
 					.$promise.then(function(lateFeesList) {
 						angular.extend($scope.lateFeesList ,lateFeesList);
 				});
 			}
 		}

		$scope.selectedStudent = function($item) {
			if($item){
				  //$item.title // or description, or image - from your angucomplete attribute configuration
				  //$item.originalObject // the actual object which was selected
				  console.log("$item.title:: "+$item.title);
				  console.log(" $item.originalObject:: "+JSON.stringify($item.originalObject));
				  $scope.fees.studentId = $item.originalObject.id;
				  console.log(" $item.studentId:: "+$scope.fees.studentId);
				  //this.$parent // the control which caused the change, contains useful things like $index for use in ng-repeat.				
			}
		};

		$scope.validateSplitPayment = function (){
			var flag = true;
			$scope.isSplitPaymentNotAllowwedWithPartial = false;
			$scope.isSplitPaymentError = false;
			$scope.isSchemeCodeError = "";
			$scope.seedSchemeCodeErrMsg = "";

			if($scope.fees.isSplitPayment && $scope.fees.isPartialPaymentAllowed){
				flag = false;
				$scope.isSplitPaymentNotAllowwedWithPartial = true;
			}

			if($scope.fees.isSplitPayment){
				if($scope.fees.seedSplitAmount && parseInt($scope.fees.seedSplitAmount) >= parseInt($scope.fees.amount)){
					flag = false;
					$scope.isSplitPaymentError = true;
				}
			}

			if($scope.fees.schemeCodeId){
				var schemeCodeGatewayId = "";
				var seedSchemeCodeGatewayId = "";
				for(var i = 0; i< $scope.schemeCodes.length; i++){
					var scheme = $scope.schemeCodes[i];
					if(scheme.id == $scope.fees.schemeCodeId){
						schemeCodeGatewayId = scheme.gatewayId;
					}
					if(scheme.id == $scope.fees.seedSchemeCodeId){
						seedSchemeCodeGatewayId = scheme.gatewayId;
					}
				}
				if(seedSchemeCodeGatewayId && seedSchemeCodeGatewayId != schemeCodeGatewayId){
					$scope.seedSchemeCodeErrMsg = "Split Payment cannot be possible with these scheme codes.";
					flag = false;
				}
			}

			if($scope.fees.schemeCodeId && $scope.fees.schemeCodeId == $scope.fees.seedSchemeCodeId){
				flag = false;
				$scope.isSchemeCodeError = "Scheme code and seed scheme code must be different.";
			}
			return flag;
		}

		$scope.splitPaymentChanged = function (){
			if(!$scope.fees.isSplitPayment){
				$scope.fees.seedSchemeCodeId = '';
				$scope.fees.seedSplitAmount = '';
			}
		};

		$scope.save = function() {
			if(!$scope.validateSplitPayment()){
				return;
			}

			if($scope.fees.studentId){
				for(var i =0; i<  $scope.studentList.length ; i++){
					var student =  $scope.studentList[i];
			    	if ($scope.fees.studentId == student.id){
			    		$scope.fees.casteId = student.casteId;
			    		break;
				    }
			    }
			}

			var fees = {
				id : $scope.fees.id,
				headId: $scope.fees.headId,
				description: $scope.fees.description,
				branchId: $scope.fees.branchId,
				casteId: ($scope.fees.casteId == 0 ? null : $scope.fees.casteId),
				fromDate: $filter('date')($scope.fees.fromDate, "yyyy-MM-dd"),
				toDate: $filter('date')($scope.fees.toDate, "yyyy-MM-dd"),
				dueDate: $filter('date')($scope.fees.dueDate, "yyyy-MM-dd"),
				amount : $scope.fees.amount,
				standardId : ($scope.fees.standardId ? $scope.fees.standardId : null),
				divisionId : ($scope.fees.divisionId ? $scope.fees.divisionId : null),
				isEmail: ($scope.fees.isEmail == true || $scope.fees.isEmail == 'Y' ? "Y" : "N"),
				isSms: ($scope.fees.isSms == true || $scope.fees.isSms == 'Y' ? "Y" : "N"),
				isNotification: ($scope.fees.isNotification == true || $scope.fees.isNotification == 'Y' ? "Y" : "N"),
				isSplitPayment: ($scope.fees.isSplitPayment == true ? "Y" : "N"),
				isAllowUserEnterAmt:($scope.fees.isAllowUser == true ? "Y" : "N"),
				isPartialPaymentAllowed : ($scope.fees.isPartialPaymentAllowed == true ? "Y" : "N"),
				frequency : $scope.fees.frequency,
				academicYearId : $scope.fees.academicYearId,
				studentId : ($scope.fees.studentId ? $scope.fees.studentId : null),
				schemeCodeId : ($scope.fees.schemeCodeId ? $scope.fees.schemeCodeId : null),
				splitAmount : $scope.fees.splitAmount, 
				seedSchemeCodeId : ($scope.fees.seedSchemeCodeId ? $scope.fees.seedSchemeCodeId : null),
				feesCodeId : ($scope.fees.feesCodeId? $scope.fees.feesCodeId : null),
				seedSplitAmount : $scope.fees.seedSplitAmount,
				latePaymentCharges :  $scope.fees.latePaymentCharges,

				// new changes 
				recurringDetail : $scope.fees.recurringDetail,
				recurring : ( $scope.fees.recurring == true ? "Y" : "N"),
				feeType : $scope.fees.feeType,
				lateFeeDetailId : $scope.fees.lateFeeDetailId,
				displayTemplateId : $scope.fees.displayTemplateId,
				reminderStartDate : $filter('date')($scope.fees.reminderStartDate, "yyyy-MM-dd"),
				currency : $scope.fees.currency,
				allowRepeatEntries : $scope.fees.allowRepeatEntries
			};

			if(!$scope.fees.isReminder){
				fees.isSms = 'N';
				fees.isEmail = 'N';
				fees.isNotification = 'N';
				fees.reminderStartDate = undefined;
				fees.frequency = undefined;
			}

			//CommonServices.saveFees('fees',fees, 'fees.manage');
			$resource('fees').save(fees).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('fees.manage',
						{
							instituteId : $scope.fees.instituteId,
		        			branchId : $scope.fees.branchId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
        			
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	     controller: "ValidationErrorController",
				    	     scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
        		}
			});
		};
	});


	/** This method is used for uploading data */
	angular.module('fees').controller('FeesFileUploadController',function($rootScope,$scope,Upload, modalPopupService, $uibModalStack, $state) {
		$scope.uploadPopupTitle = "Upload Fees ";
		if($scope.$parent.isUploadUpdate){
			$scope.uploadPopupTitle = "Bulk Upload Update Fees ";
		}
		$scope.uploadFees = function (file) {
	        Upload.upload({
	            url: $scope.$parent.isUploadUpdate ? 'fees/uploadUpdateFile' : 'fees/uploadFeesFile',
	            data : {branchId : $scope.branchId,academicYearId : $scope.academicYearId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.feesList.length = 0;
	        	$scope.$parent.isUploadingFailed = (resp.data.status == 'fail' ? true : false);
	        	$scope.$parent.message = resp.data.message;
	        	angular.extend($scope.feesList,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
	
	/*This Controller is Used for Fees Edit functionality*/
	angular.module('fees').controller('FeesEditController',function($rootScope,$scope,$filter,$state, modalPopupService, CasteList, CurrencyList, $stateParams,$resource, CommonServices) {

		$scope.fees = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.headList = [];
		$scope.standardList = [];
		$scope.divisionList = [];
		$scope.feesHeaderValue = "Edit Fees";
		$scope.acadmicStartDate; $scope.acadmicEndDate;
		$scope.studentList = [];
		$scope.isFirstTime = true;
		$scope.schemeCodes = [];
		$scope.feesCodes = [];
		$scope.filteredStudents = [];
		$scope.displayTemplates = [];
		$scope.fees.displayTemplates = [];
		$scope.lateFeesList = [];
		$scope.minDueDate = "";
		$scope.isReminder;
		$scope.isStudentSpecific=false;

		$scope.changeStandardList = function(){
			changeStudentList();
			$scope.fees.displayTemplates.length = 0;
			$scope.fees.displayTemplateId = "";
			$scope.displayTemplates.length = 0;
			changeDisplayLayouts();
			changeLateFeesDetail();
			changeStandardList();
		};

		$scope.filterStudents = function(){
			$scope.filteredStudents.length = 0;
			if($scope.studentList){
				for(var i =0; i< $scope.studentList.length; i++){
					var student = $scope.studentList[i];
					var addObj = true;
					if($scope.fees.casteId && student.casteId != $scope.fees.casteId){
						addObj = false;
					}
					else if($scope.fees.standardId && student.standardId != $scope.fees.standardId){
						addObj = false;
					}
					else if($scope.fees.divisionId && student.divisionId != $scope.fees.divisionId){
						addObj = false;
					}
					else if($scope.fees.feesCodeId && student.feesCodeId != $scope.fees.feesCodeId){
						addObj = false;
					}
					if(addObj){
						$scope.filteredStudents.push(student);
					}
				}
			}
		};

		$scope.removeLayout = function (id){
			$scope.fees.displayTemplateId = "";
			$scope.fees.displayTemplates.length = 0;
		};

		$scope.viewTemplate = function (id){
			var options = {
		    	      templateUrl: 'app/fees/html/fees-template-info.html',
		    	      controller: 'FeesDisplayTemplateInfoController',
		    	      scope : $scope
	    	    };
	    	    modalPopupService.openPopup(options);
		};

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.fees.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.fees.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					 getSchemeCodes();
					 getFeesCodes();
					 changeYearList();
					 changeHeadList();
					 angular.extend($scope.standardList,standardList);
					 changeDivisionList();
					 $scope.isFirstTime = false;
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		$scope.changeDivisionList = function(){
			changeDivisionList();
		};
		
		$scope.changeStudentList = function(){
			changeStudentList();
		};
		 $scope.dayArray = [{"label" : "Monday", "value" : "1"},
		                    {"label" : "Tuesday", "value" : "2"},
		        		    {"label" : "Wednesday", "value" : "3"},
		        		    {"label" : "Thursday", "value" : "4"},
		        		    {"label" : "Friday", "value" : "5"},
		        		    {"label" : "Saturday", "value" : "6"},
		        		    {"label" : "Sunday", "value" : "7"}
		        	    ]; 

		    $scope.monthDayArray = [{"label" : "1", "value" : "1"}, {"label" : "2", "value" : "2"},
			    {"label" : "3", "value" : "3"}, {"label" : "4", "value" : "4"},
			    {"label" : "5", "value" : "5"}, {"label" : "6", "value" : "6"},
			    {"label" : "7", "value" : "7"}, {"label" : "8", "value" : "8"},
			    {"label" : "9", "value" : "9"}, {"label" : "10", "value" : "10"},
			    {"label" : "11", "value" : "11"}, {"label" : "12", "value" : "12"},
			    {"label" : "13", "value" : "13"}, {"label" : "14", "value" : "14"},
			    {"label" : "15", "value" : "15"}, {"label" : "16", "value" : "16"},
			    {"label" : "17", "value" : "17"}, {"label" : "18", "value" : "18"},
			    {"label" : "19", "value" : "19"}, {"label" : "20", "value" : "20"},
			    {"label" : "21", "value" : "21"}, {"label" : "22", "value" : "22"},
			    {"label" : "23", "value" : "23"}, {"label" : "24", "value" : "24"},
			    {"label" : "25", "value" : "25"}, {"label" : "26", "value" : "26"},
			    {"label" : "27", "value" : "27"}, {"label" : "28", "value" : "28"},
			    {"label" : "29", "value" : "29"}, {"label" : "30", "value" : "30"},
			    {"label" : "31", "value" : "31"}
		    ];
		    $scope.isDaySelected = function(){
		    	 var flag = false;
		    	 if($scope.fees.feesWeekDays){
		    		 angular.forEach($scope.fees.feesWeekDays, function(isSelected, day) {
			             if(isSelected){
			            	 flag = true;
			             }
			         });	 
		    	 }
		    	 return flag;
		     };
		  

 		$scope.openSelectLayoutPopup = function (){
 			var options = {
 	    	      templateUrl: 'app/fees/html/fees-template-select.html',
 	    	      controller: 'FeesDisplayTemplateController',
 	    	      scope : $scope
     	    };
     	    modalPopupService.openPopup(options);
 		};

 		function changeDisplayLayouts() {
 			if($scope.fees.branchId){
 				$resource('displayTemplates?branchId='+$scope.fees.branchId).query()
 					.$promise.then(function(displayTemplates) {
 						angular.extend($scope.displayTemplates,displayTemplates);
 				});
 			}
 		}

 		function changeLateFeesDetail(){
 			if($scope.fees.branchId){
 				$scope.lateFeesList.length = 0;
 				$resource('fees/getAllLateFees?branchId='+$scope.fees.branchId).query()
 					.$promise.then(function(lateFeesList) {
 						angular.extend($scope.lateFeesList,lateFeesList);
 				});
 			}
 		}

		$scope.isFirstTimeBranchChanged = true;
		function changeStudentList(){
			$scope.fees.studentId = $scope.isFirstTimeBranchChanged ? $scope.fees.studentId : '';
			$scope.studentList.length = 0;
			if($scope.fees.branchId){
				$scope.$broadcast('angucomplete-alt:clearInput');
				var branchId = $scope.fees.branchId;

				if(branchId){
					var requestParams = 'branchId='+branchId;
					$resource('students/getStudentsByStandardDivision?'+requestParams).query().$promise.then(function(matchingStudentList) {

						if(matchingStudentList && matchingStudentList.length > 0){
							angular.extend($scope.studentList, matchingStudentList);
							angular.extend($scope.filteredStudents, $scope.studentList);
							$scope.isBranchSelected = false;
						}

						if($scope.isFirstTimeBranchChanged && $scope.fees.studentId){
							changeStudentById($scope.fees.studentId);
						}
		    		});
					
				} else {
					$scope.isBranchSelected = true;
				}
			}
		}

		function getSchemeCodes(){
    		if($scope.fees.branchId){
    			$scope.schemeCodes.length = 0;
    			$resource('fees/schemeCodes?branchId='+$scope.fees.branchId)
    			.query().$promise.then(function(instituteList) {
    	    		angular.extend($scope.schemeCodes,instituteList);
    			});
    		}
    		else {
    			$scope.schemeCodes.length = 0;
    		}
    	}

		function changeStudentById(id) {
			var matches = [];
			$scope.studentList.forEach(function(student) {
				var description = 'Name : '+ student.firstname + ' ' + student.surname + ', Roll No. : ' + student.rollNumber + ', Reg No. :  ' + student.registrationCode;
			    if (student.id == id) {
			    	//$scope.isStudentSpecific = true;
			    	student.description = description;
			    	$scope.initialStudent = description;
			    }
		    });
			return matches;
		}

		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.fees.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.fees.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
//							$scope.fees.divisionId = $scope.fees.divisionId ? $scope.fees.divisionId 
//									: '';
						}
						$scope.filterStudents();
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
				$scope.filterStudents();
			}
		}

		function getFeesCodes(){
			if($scope.fees.branchId){
				$scope.feesCodes.length = 0;
				$resource('fees/fees-code/list/:id').query({id : $scope.fees.branchId}).$promise.then(
				 function(feesCodeList) {
					 $scope.feesCodes.length = 0;
					 angular.extend($scope.feesCodes, feesCodeList);
				});
			}
			else {
				$scope.feesCodes.length = 0;
			}
		}

		$resource('fees/:id').get({id : $stateParams.id}).$promise.then(function(feesToEdit) {
			feesToEdit.fromDate = new Date(feesToEdit.fromDate);
			feesToEdit.toDate = new Date(feesToEdit.toDate);
			feesToEdit.dueDate = new Date(feesToEdit.dueDate);
			feesToEdit.reminderStartDate = feesToEdit.reminderStartDate?new Date(feesToEdit.reminderStartDate):feesToEdit.reminderStartDate;
			$scope.minEndDate = feesToEdit.toDate;
			var curDate = new Date();
			$scope.minDueDate = feesToEdit.fromDate < curDate ? curDate : feesToEdit.fromDate;
			angular.extend($scope.fees,feesToEdit);
			$scope.fees.isEmail = (feesToEdit.isEmail == 'Y' ? true : false);
			$scope.fees.isSms  = (feesToEdit.isSms == 'Y' ? true : false);
			$scope.fees.isNotification = (feesToEdit.isNotification == 'Y' ? true : false);
			$scope.fees.isSplitPayment = (feesToEdit.isSplitPayment == 'Y' ? true : false);
			$scope.fees.recurring = (feesToEdit.recurring == 'Y' ? true : false);
			$scope.feeCodeName = (feesToEdit.feeCodeName);
			$scope.fees.isAllowUser=feesToEdit.isAllowUserEnterAmt=='Y'?true:false;
			$scope.fees.isPartialPaymentAllowed = (feesToEdit.isPartialPaymentAllowed == 'Y' ? true : false);
			$rootScope.setSelectedData($scope.fees.instituteId,$scope.fees.branchId);
			 if($scope.fees.reminderStartDate && ($scope.fees.isNotification || $scope.fees.isSms || $scope.fees.isEmail)){
				 $scope.fees.isReminder = true;
			 }
			changeBranchList();
		});

		$scope.casteList = CasteList;
		$scope.currencyList = CurrencyList;
		$scope.isOpenStartDate = false;
		$scope.isOpenEndDate = false;
		$scope.isOpenDueDate = false;
		$scope.minDate = new Date();

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){	
			$scope.academicYearList.length = 0;
			if($scope.fees.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.fees.branchId).query({id : $scope.fees.branchId})
					.$promise.then(function(academicYearList) {
						
					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								if(!acadmicYear.fromDate instanceof Date){
									$scope.acadmicStartDate = new Date(acadmicYear.fromDate);
									acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
								}
								else {
										$scope.acadmicStartDate = acadmicYear.fromDate;
								}
								if(!acadmicYear.toDate instanceof Date){
									$scope.acadmicEndDate = new Date(acadmicYear.toDate);
									acadmicYear.toDate = new Date(acadmicYear.toDate);
								}
								else {
									$scope.acadmicEndDate = acadmicYear.toDate;
								}
								$scope.fees.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.isFirstTimeBranchChanged = false;
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.fees.instituteId || $scope.fees.instituteId ==0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.fees.instituteId})
					.$promise.then(function(branchList) {
						changeDisplayLayouts();
						changeLateFeesDetail();
						changeStudentList();
						changeStandardList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		$scope.changeHeadList = function (){
			changeHeadList();
		};
		
		function changeHeadList(){
			if($scope.fees.branchId){		
				$resource('head/getHeadByBranchForDashboard?branchId='+$scope.fees.branchId).query()
					.$promise.then(function(headList) {
					 $scope.headList.length = 0;
					angular.extend($scope.headList,headList);									
	    		});
			}
		}
		
		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		};
		$scope.openEndDateDialog = function fnOpenDialog(){
			$scope.isOpenEndDate = true;
		};
		$scope.openDueDateDialog = function fnOpenDialog(){
			$scope.isOpenDueDate = true;
		};
		
		$scope.cancel = function(){
			$state.transitionTo('fees.manage',{reload : true});
		};

		$scope.searchStudentByStr = function(str) {
			var matches = [];
			$scope.filteredStudents.forEach(function(student) {
			    //var fullName = person.firstName + ' ' + person.surname;\
				  var description = 'Name : '+ student.firstname + ' ' + student.surname + (student.rollNumber ? ', Roll No. : ' + student.rollNumber : '') + (student.registrationCode ? ', Reg No. :  ' +  student.registrationCode : '');

				if (( student.firstname && student.firstname.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
			        (student.surname && student.surname.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
			        (student.rollNumber && student.rollNumber.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
			        (student.registrationCode && student.registrationCode.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {

			    		student.description = description;
			    		matches.push(student);
			    }
			  });
			  return matches;
		};

		$scope.selectedStudent = function($item) {
			if($item){
				//$item.title // or description, or image - from your angucomplete attribute configuration				
			  //$item.originalObject // the actual object which was selected
			  console.log("$item.title:: "+$item.title);
			  console.log(" $item.originalObject:: "+JSON.stringify($item.originalObject));
			  $scope.isStudentSpecific=true;
			  $scope.fees.studentId = $item.originalObject.id;
			  console.log(" $item.studentId:: "+$scope.fees.studentId);
			}
		};

		$scope.validateSplitPayment = function (){
			var flag = true;
			$scope.isSplitPaymentNotAllowwedWithPartial = false;
			$scope.isSplitPaymentError = false;
			$scope.isSchemeCodeError = "";
			$scope.seedSchemeCodeErrMsg = "";

			if($scope.fees.isSplitPayment && $scope.fees.isPartialPaymentAllowed){
				flag = false;
				$scope.isSplitPaymentNotAllowwedWithPartial = true;
			}

			if($scope.fees.isSplitPayment){
				if($scope.fees.seedSplitAmount && parseInt($scope.fees.seedSplitAmount) >= parseInt($scope.fees.amount)){
					flag = false;
					$scope.isSplitPaymentError = true;
				}
			}

			if($scope.fees.schemeCodeId){
				var schemeCodeGatewayId = "";
				var seedSchemeCodeGatewayId = "";
				for(var i = 0; i< $scope.schemeCodes.length; i++){
					var scheme = $scope.schemeCodes[i];
					if(scheme.id == $scope.fees.schemeCodeId){
						schemeCodeGatewayId = scheme.gatewayId;
					}
					if(scheme.id == $scope.fees.seedSchemeCodeId){
						seedSchemeCodeGatewayId = scheme.gatewayId;
					}
				}
				if(seedSchemeCodeGatewayId && seedSchemeCodeGatewayId != schemeCodeGatewayId){
					$scope.seedSchemeCodeErrMsg = "Split Payment cannot be possible with these scheme codes.";
					flag = false;
				}
			}

			if($scope.fees.schemeCodeId && $scope.fees.schemeCodeId == $scope.fees.seedSchemeCodeId){
				flag = false;
				$scope.isSchemeCodeError = "Scheme code and seed scheme code must be different.";
			}
			return flag;
		}

		$scope.splitPaymentChanged = function (){
			if(!$scope.fees.isSplitPayment){
				$scope.fees.seedSchemeCodeId = '';
				$scope.fees.seedSplitAmount = '';
			}
		};

		$scope.save = function() {
			if(!$scope.validateSplitPayment()){
				return;
			}

			if($scope.fees.studentId){
				for(var i =0; i<  $scope.studentList.length ; i++){
					var student =  $scope.studentList[i];
			    	if ($scope.fees.studentId == student.id){
			    		$scope.fees.casteId = student.casteId;
			    		break;
				    }
			    }
			}
		

			var fees = {
				id : $scope.fees.id,
				headId: $scope.fees.headId,
				description: $scope.fees.description,
				branchId: $scope.fees.branchId,
				casteId: ($scope.fees.casteId == 0 ? null : $scope.fees.casteId),
				fromDate: $filter('date')($scope.fees.fromDate, "yyyy-MM-dd"),
				toDate: $filter('date')($scope.fees.toDate, "yyyy-MM-dd"),
				dueDate: $filter('date')($scope.fees.dueDate, "yyyy-MM-dd"),
				amount : $scope.fees.amount,
				standardId : ($scope.fees.standardId ? $scope.fees.standardId : null),
				divisionId : ($scope.fees.divisionId ? $scope.fees.divisionId : null),
				isEmail: ($scope.fees.isEmail == true || $scope.fees.isEmail == 'Y' ? "Y" : "N"),
				isSms: ($scope.fees.isSms == true || $scope.fees.isSms == 'Y' ? "Y" : "N"),
				isNotification: ($scope.fees.isNotification == true || $scope.fees.isNotification == 'Y' ? "Y" : "N"),
				isSplitPayment: ($scope.fees.isSplitPayment == true ? "Y" : "N"),
				isPartialPaymentAllowed : ($scope.fees.isPartialPaymentAllowed == true ? "Y" : "N"),
				frequency : $scope.fees.frequency,
				academicYearId : $scope.fees.academicYearId,
				studentId : ($scope.fees.studentId ? $scope.fees.studentId : null),
				schemeCodeId : ($scope.fees.schemeCodeId ? $scope.fees.schemeCodeId : null),
				splitAmount : $scope.fees.splitAmount, 
				seedSchemeCodeId : ($scope.fees.seedSchemeCodeId ? $scope.fees.seedSchemeCodeId : null),
				feesCodeId : ($scope.fees.feesCodeId? $scope.fees.feesCodeId : null),
//				feesCodeName:($scope.fees.feesCodeName?$scope.fees.feesCodeName:null),
				seedSplitAmount : $scope.fees.seedSplitAmount,
				latePaymentCharges :  $scope.fees.latePaymentCharges,

				// new changes 
				recurringDetail : $scope.fees.recurringDetail,
				recurring : ( $scope.fees.recurring == true ? "Y" : "N"),
				feeType : $scope.fees.feeType,
				lateFeeDetailId : $scope.fees.lateFeeDetailId == 0 ? null : $scope.fees.lateFeeDetailId,
				displayTemplateId : $scope.fees.displayTemplateId,
				reminderStartDate : $filter('date')($scope.fees.reminderStartDate, "yyyy-MM-dd"),
				currency : $scope.fees.currency,
				allowRepeatEntries : $scope.fees.allowRepeatEntries,
				isAllowUserEnterAmt:$scope.fees.isAllowUser==true? "Y":"N"
			};

			if(!$scope.fees.isReminder){
				fees.isSms = 'N';
				fees.isEmail = 'N';
				fees.isNotification = 'N';
				fees.reminderStartDate = undefined;
				fees.frequency = undefined;
			}

			$resource('fees').save(fees).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('fees.manage',
					{
						instituteId : $scope.fees.instituteId,
	        			branchId : $scope.fees.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
					}
				else{
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						
						var options = {
								 templateUrl: "app/common/html/validationError.html",
					    	     controller: "ValidationErrorController",
					    	     scope : $scope	 
						 };
						modalPopupService.openPopup(options);
						 
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
	        	}
			});
		};
	});
})();