(function(){
	angular.module('aboutus').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('aboutus', {
			url : '',
			abstract :true,
			controller : 'AboutUsModelController',
			
		}).state('aboutus.page', {
			url : '/aboutus/page',
			views : {
				'@' : {
					templateUrl : 'app/aboutus/html/aboutus-page.html',
					controller : 'AboutUsController'
				}
			},
			data: {
	            displayName: 'About Us'
	        }
		});
	}]);
})();