(function(){
	angular.module('standard').controller('StandardController',function($rootScope, $localStorage, 
			$resource,$filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {
	    
		$scope.standardList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	//	angular.extend($scope.standardList,StandardListData);
	    
    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);
		
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('standard/getStandardByBranch?branchId='+$scope.branchId).query().$promise.then(function(standardList) {
					$scope.standardList.length = 0;
					if(standardList && standardList.length > 0){
						angular.extend($scope.standardList,standardList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				if($rootScope.roleId == 6 && branchList.length == 1){
        					$scope.branchId = (!$scope.branchId ? branchList[0].id : $scope.branchId);
        					/*$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;*/
        				}
        				changeYearList();
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};
		
		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
						$scope.branchId = (isBranchChanged ? branchList[0].id  : 
							(!$scope.branchId ? branchList[0].id : $scope.branchId));
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
					changeYearList();
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		
		/*
	    *//** This method is used to open Modal *//* 
	    $scope.openUploadStandardDialog = function(){
	    	  $uibModal.open({
	    	      templateUrl: 'app/standard/html/standard-bulk-upload.html',
	    	      controller: 'StandardFileUploadController',
	    	      scope : $scope
	    	     
	    	    });
	     };*/

	     $scope.deleteData = function deleteStandard(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('standard').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.standardList.length ;i ++ ){
      			  		var standard = $scope.standardList[i];
      			  		if(standard.id == id){
      			  			$scope.standardList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});
	
	angular.module('event').controller('validateStandardForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,35}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});
	
	angular.module('standard').controller('StandardAddController',function($rootScope, $localStorage, $scope,$filter,$state,$stateParams,$resource,CommonServices) {
		$scope.standard = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.divisionList = [];
		$scope.academicYearList = [];
		$scope.standardHeaderValue = "Add Standard";

		$scope.standard.instituteId = $localStorage.selectedInstitute;
		$scope.standard.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			changeDivisionList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
 
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.standard.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.standard.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							changeDivisionList();
	        			}
						else {
							$scope.divisionList.length = 0;
						}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.changeDivisionList = function (){
			changeDivisionList();
		};
		
		function changeDivisionList(){
			if($scope.standard.branchId){
				$resource('division/getDivisionByBranch?branchId='+$scope.standard.branchId).query()
					.$promise.then(function(divisionList) {
					$scope.divisionList.length = 0;
					changeYearList();
					angular.extend($scope.divisionList,divisionList);									
	    		});
			}
		}

		$scope.changeYearList = function(){
			changeYearList();
		};
		
		
		function changeYearList(){
			if($scope.standard.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.standard.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.standard.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('standard.list');
		};
		
		$scope.save = function() {
			var standard = {
				id : $scope.standard.id,
				displayName : $scope.standard.displayName,
				code : $scope.standard.code,
				branchId : $scope.standard.branchId,
				divisionIdList : $scope.standard.divisionIdList,
				academicYearId : $scope.standard.academicYearId
			};			
		
			$resource('standard').save(standard).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('standard.list',
					{
						instituteId : $scope.standard.instituteId,
	        			branchId : $scope.standard.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});
	
	/*This Controller is Used for Standard Edit functionality*/
	angular.module('standard').controller('StandardEditController',function($rootScope,$scope,$filter,$state,$stateParams,$resource, CommonServices) {
		
		$scope.standard = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.divisionList = [];
		$scope.academicYearList = [];
		$scope.standardHeaderValue = "Edit Standard";
		
		$resource('standard/:id').get({id : $stateParams.id}).$promise.then(function(standardToEdit) {
			angular.extend($scope.standard,standardToEdit);
			$rootScope.setSelectedData($scope.standard.instituteId,$scope.standard.branchId);
			
			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.standard.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.standard.instituteId})
					.$promise.then(function(branchList) {
						/*if(branchList != null && branchList.length > 0){
							$scope.standard.branchId = branchList[0].id;
						}*/
						changeDivisionList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
	
		$scope.changeDivisionList = function (){
			changeDivisionList();
		};
		
		function changeDivisionList(){
			if($scope.standard.branchId){
				$resource('division/getDivisionByBranch?branchId='+$scope.standard.branchId).query()
					.$promise.then(function(divisionList) {
					changeYearList();
					$scope.divisionList.length = 0;
					angular.extend($scope.divisionList,divisionList);									
	    		});
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){	
			if($scope.standard.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.standard.branchId).
						query({id : $scope.standard.branchId}).$promise.then(function(academicYearList) {
						$scope.academicYearList.length = 0;

					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.standard.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
		
		$scope.cancel = function(){
			$state.transitionTo('standard.list',{reload : true});
		};
		
		$scope.save = function() {
			var standard = {
				id : $scope.standard.id,
				displayName : $scope.standard.displayName,
				code : $scope.standard.code,
				branchId : $scope.standard.branchId,
				divisionIdList : $scope.standard.divisionIdList,
				academicYearId : $scope.standard.academicYearId
			};

			$resource('standard').save(standard).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('standard.list',
					{
						instituteId : $scope.standard.instituteId,
	        			branchId : $scope.standard.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
		        }
			});
		};
	});

	
	/** This method is used for uploading data *//*
	angular.module('standard').controller('StandardFileUploadController',function($scope,Upload, $uibModalInstance,$state) {
		$scope.uploadStandard = function (file) {
	        Upload.upload({
	            url: 'standard/uploadStandardFile',
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	$uibModalInstance.dismiss('cancel');
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	        	angular.extend($scope.standardList,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
	    	$uibModalInstance.dismiss('cancel');
	    };
	});*/
})();

