(function(){
	angular.module('standard').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('standard', {
			url : '/standard',
			abstract :true,
			controller : 'StandardModelController',
			data: {
				 breadcrumbProxy: 'standard.list'
			}
		}).state('standard.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/standard/html/standard-list.html',
					controller : 'StandardController'
				}
			},
			data: {
	            displayName: 'Standard List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('standard.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/standard/html/standard-add.html',
					controller : 'StandardAddController'
				}
			},
			data: {
	            displayName: 'Add Standard'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	DistrictList : function($resource){
	        		return $resource('masters/districtMasters').query().$promise;
	        	},
	        	TalukaList : function($resource){
	        		return $resource('masters/talukaMasters').query().$promise;
	        	}
	        }
		}).state('standard.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/standard/html/standard-add.html',
					controller : 'StandardEditController'
				}
			},
			data: {
	            displayName: 'Edit Standard'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	DistrictList : function($resource){
	        		return $resource('masters/districtMasters').query().$promise;
	        	},
	        	TalukaList : function($resource){
	        		return $resource('masters/talukaMasters').query().$promise;
	        	}
	        }
		});
	}]);
})();