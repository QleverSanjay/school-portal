(function(){
	angular.module('role-access').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('role-access', {
			url : '/role-access',
			views : {
				'@' : {
					templateUrl : 'app/role-access/html/roles.html',
					controller : 'RoleAssignController'
				}
			},
			data: {
	            displayName: 'Assign Permission'
	        }
		});
	}]);
})();