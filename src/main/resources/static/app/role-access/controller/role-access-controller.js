(function(){
	
	angular.module('role-access').controller('RoleAssignController',['$scope','modalPopupService','$resource',function($scope,modalPopupService,$resource){
		$resource('roles/menu').get().$promise.then(function(data) {
			$scope.roles = data.roleList;
		});
		
		$scope.assignPrivileges = fnAssignPrivileges;
		$scope.moduleList = fnGetModuleList;
		$scope.save = fnSave;
		$scope.cancel = fnCancel;
		var copyModules = [];
		
		function fnCancel(){
			fnGetModuleList($scope.role);
		}
		function fnGetModuleList(roleId){
			$resource('roles/menu-list').query({'roleId' : roleId}).$promise.then(function(data) {
				$scope.modules = data;
				copyModules = angular.copy($scope.modules);
			});
		}
		
		function fnSave(){
			var dataToSave = [];
			
			for (var i = 0; i < copyModules.length; i++) {
				for (var i = 0; i < $scope.modules.length; i++) {
					if(copyModules[i].menuName == $scope.modules[i].menuName && copyModules[i].allow != $scope.modules[i].allow){
						$scope.modules[i].roleId = $scope.role;
						dataToSave.push($scope.modules[i]);
					}
				}
			}
			
			$resource('roles/save').save(dataToSave).$promise.then(function(data) {
				copyModules = angular.copy($scope.modules);
			});
		}
		
		function fnAssignPrivileges(menuId){
			$scope.menuID = menuId;
			var options = {
			      templateUrl: 'app/role-access/html/privileges.html',
			      controller: 'PrivilegeController',
			      scope : $scope
		    };
			modalPopupService.openPopup(options);
		}
	}]);
	
	angular.module('role-access').controller('PrivilegeController',['$stateParams','$scope','modalPopupService','$uibModalStack','$resource',function($stateParams, $scope, modalPopupService, $uibModalStack,$resource){
		var copyPermissions = [];
		$resource('roles/menu-permissions').query({'menuId' : $scope.menuID, 'roleId' : $scope.role}).$promise.then(function(data) {
			$scope.privileges = data;
			copyPermissions = angular.copy($scope.privileges);
		});
		
		$scope.cancel = fnCancel;
		$scope.save = fnSave;
		
		var dataToSavePermission = [];
		function fnSave(){
			for (var i = 0; i < copyPermissions.length; i++) {
				for (var i = 0; i < $scope.privileges.length; i++) {
					if(copyPermissions[i].permissionName == $scope.privileges[i].permissionName && copyPermissions[i].allow != $scope.privileges[i].allow){
						$scope.privileges[i].roleId = $scope.role;
						dataToSavePermission.push($scope.privileges[i]);
					}
				}
			}
			
			$resource('roles/save-permission').save(dataToSavePermission).$promise.then(function(data) {
				copyPermissions = angular.copy($scope.privileges);
				modalPopupService.closePopup(null, $uibModalStack);
			});	
		}
		
		function fnCancel(){
			modalPopupService.closePopup(null, $uibModalStack);
		}
		
	}]);
	

})();