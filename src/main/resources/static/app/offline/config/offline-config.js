(function(){
	angular.module('offline').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('offline', {
			url : '/offline',
			abstract :true,
			controller : 'OfflineModelController',
			data: {
				 breadcrumbProxy: 'offline.list'
	        }
		}).state('offline.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/offline/html/offline-list.html',
					controller : 'OfflineController'
				}
			},
			data: {
	            displayName: 'Offline Payments Report'
	        },
	        params : {
	        	message : ""
	        }
		});
	}]);
})();