(function(){
	angular.module('workingday').controller('WorkingDayController',function($rootScope,$resource,$filter,$confirm,DTOptionsBuilder,$filter, 
			$localStorage,  $stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.workingDayList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true).withOption("scrollX", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('workingday/getWorkDayByBranch?branchId='+$scope.branchId).query().$promise.then(function(workingDayList) {
					$scope.workingDayList.length = 0;
					if(workingDayList && workingDayList.length > 0){
						for(var i = 0; i < workingDayList.length; i++){
							var currentYear = workingDayList[i].currentAcademicYearStr;
							var dates = currentYear.split(" - ");
							var startDate = new Date(dates[0]);
							var endDate = new Date(dates[1]);
							currentYear = $filter('date')(startDate, "dd/MM/yyyy")+" - "+$filter('date')(endDate, "dd/MM/yyyy");
							workingDayList[i].currentAcademicYearStr = currentYear;
						}						
						angular.extend($scope.workingDayList,workingDayList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				if(branchList.length == 1){
        					$scope.branchId = (!$scope.branchId ? branchList[0].id : $scope.branchId);
        					/*$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;*/
        				}
        				changeYearList();
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}
		
		
     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
						$scope.branchId = (isBranchChanged ? branchList[0].id  : (!$scope.branchId ? branchList[0].id : $scope.branchId));
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
					changeYearList();
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}

	    $scope.deleteData = function deleteHead(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('workingday').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.workingDayList.length ;i ++ ){
      			  		var workingday = $scope.workingDayList[i];
      			  		if(workingday.id == id){
      			  			$scope.workingDayList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});
		
	angular.module('workingday').controller('validateWorkDaysForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,45}$/;
	});
	
	angular.module('workingday').controller('WorkingDayAddController',function($rootScope,$scope,$filter,
			$localStorage, $state,$stateParams,$resource,CommonServices) {
		$scope.workingday = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.workingDayHeaderValue = "Add Working Days";
		$scope.isDayRequired = false;

		$scope.workingday.instituteId = $localStorage.selectedInstitute;
		$scope.workingday.branchId = $localStorage.selectedBranch;

		$scope.$watch("workingday.monday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.tuesday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.wednesday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.thursday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.friday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.saturday", function(){
			checkIsDaySelected();
		});
		$scope.$watch("workingday.sunday", function(){
			checkIsDaySelected();
		});
		
		function checkIsDaySelected(){
			if($scope.workingday.monday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.tuesday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.wednesday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.thursday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.friday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.saturday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.sunday == true){
				$scope.isDayRequired = false;
			}
			else {
				$scope.isDayRequired = true;
			}
		}
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			changeYearList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.workingday.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.workingday.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.workingday.currentAcademicYearId = acadmicYear.id;
								if(!acadmicYear.fromDate instanceof Date){
									acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
								}
								if(!acadmicYear.toDate instanceof Date){
									acadmicYear.toDate = new Date(acadmicYear.toDate);
								}
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
    	
    	
    	
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
 
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.workingday.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.workingday.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							changeYearList();
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.cancel = function(){
			CommonServices.cancelOperation('workingday.list');
		};

		$scope.save = function() {
			var workingday = {
				id : $scope.workingday.id,
				currentAcademicYearId : $scope.workingday.currentAcademicYearId,
				monday : ($scope.workingday.monday == true || $scope.workingday.monday == 'Y' ? "Y" : "N"),
				tuesday : ($scope.workingday.tuesday == true || $scope.workingday.tuesday == 'Y' ? "Y" : "N"),
				wednesday : ($scope.workingday.wednesday == true || $scope.workingday.wednesday == 'Y' ? "Y" : "N"),
				thursday : ($scope.workingday.thursday == true || $scope.workingday.thursday == 'Y' ? "Y" : "N"),
				friday : ($scope.workingday.friday == true || $scope.workingday.friday == 'Y' ? "Y" : "N"),
				saturday : ($scope.workingday.saturday == true || $scope.workingday.saturday == 'Y' ? "Y" : "N"),
				sunday : ($scope.workingday.sunday == true || $scope.workingday.sunday == 'Y' ? "Y" : "N"),
				branchId : $scope.workingday.branchId,
				skipTimetableOnHoliday : ( $scope.workingday.skipTimetableOnHoliday == true ? "Y" : "N")
			};			

			$resource('workingday').save(workingday).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('workingday.list',
					{
						instituteId : $scope.workingday.instituteId,
	        			branchId : $scope.workingday.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});

	/*This Controller is Used for Division Edit functionality*/
	angular.module('workingday').controller('WorkingDayEditController',function($rootScope,$scope,$state,$stateParams,$resource, CommonServices, $filter) {
		
		$scope.workingday = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.workingDayHeaderValue = "Edit Working Days";
		$scope.isDayRequired = false;
		
		$scope.$watch("workingday.monday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.tuesday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.wednesday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.thursday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.friday", function(){
			checkIsDaySelected();
		});
		
		$scope.$watch("workingday.saturday", function(){
			checkIsDaySelected();
		});
		$scope.$watch("workingday.sunday", function(){
			checkIsDaySelected();
		});
		
		function checkIsDaySelected(){
			if($scope.workingday.monday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.tuesday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.wednesday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.thursday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.friday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.saturday == true){
				$scope.isDayRequired = false;
			}
			else if($scope.workingday.sunday == true){
				$scope.isDayRequired = false;
			}
			else {
				$scope.isDayRequired = true;
			}
		}
		
		$resource('workingday/:id').get({id : $stateParams.id}).$promise.then(function(workingDayToEdit) {
			angular.extend($scope.workingday,workingDayToEdit);
			$rootScope.setSelectedData($scope.workingday.instituteId,$scope.workingday.branchId);

			$scope.workingday.monday = (workingDayToEdit.monday == 'Y' ? true : false);
			$scope.workingday.tuesday = (workingDayToEdit.tuesday == 'Y' ? true : false);
			$scope.workingday.wednesday = (workingDayToEdit.wednesday == 'Y' ? true : false);
			$scope.workingday.thursday = (workingDayToEdit.thursday == 'Y' ? true : false);
			$scope.workingday.friday = (workingDayToEdit.friday == 'Y' ? true : false);
			$scope.workingday.saturday = (workingDayToEdit.saturday == 'Y' ? true : false);
			$scope.workingday.sunday = (workingDayToEdit.sunday == 'Y' ? true : false);
			$scope.workingday.skipTimetableOnHoliday = (workingDayToEdit.skipTimetableOnHoliday == 'Y' ? true : false);

			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
    	
    	
    	$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.workingday.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.workingday.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					 if(academicYearList != null && academicYearList.length > 0){
							for(var i = 0; i < academicYearList.length; i++){
								var acadmicYear = academicYearList[i];
								if(acadmicYear.isCurrentActiveYear == 'Y'){
									$scope.workingday.currentAcademicYearId = acadmicYear.id;
									if(!acadmicYear.fromDate instanceof Date){
										acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
									}
									if(!acadmicYear.toDate instanceof Date){
										acadmicYear.toDate = new Date(acadmicYear.toDate);
									}
									acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
									acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
								}
							}
						}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

		// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.workingday.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.workingday.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};
	
		$scope.cancel = function(){
			$state.transitionTo('workingday.list',{reload : true});
		};
		
		$scope.save = function() {
			var workingday = {
					id : $scope.workingday.id,
					currentAcademicYearId : $scope.workingday.currentAcademicYearId,
					monday : ($scope.workingday.monday == true ? "Y" : "N"),
					tuesday : ($scope.workingday.tuesday == true ? "Y" : "N"),
					wednesday : ($scope.workingday.wednesday == true ? "Y" : "N"),
					thursday : ($scope.workingday.thursday == true ? "Y" : "N"),
					friday : ($scope.workingday.friday == true ? "Y" : "N"),
					saturday : ($scope.workingday.saturday == true ? "Y" : "N"),
					sunday : ($scope.workingday.sunday == true ? "Y" : "N"),
					branchId : $scope.workingday.branchId,
					skipTimetableOnHoliday : ( $scope.workingday.skipTimetableOnHoliday == true ? "Y" : "N")
				};	
			
			$resource('workingday').save(workingday).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('workingday.list',
					{
						instituteId : $scope.workingday.instituteId,
	        			branchId : $scope.workingday.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
		        }
			});
		};
	});
})();

