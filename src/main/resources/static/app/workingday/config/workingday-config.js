(function(){
	angular.module('workingday').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('workingday', {
			url : '/workingday',
			abstract :true,
			controller : 'WorkingDayModelController',
			data: {
				 breadcrumbProxy: 'workingday.list'
			}
		}).state('workingday.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/workingday/html/workingday-list.html',
					controller : 'WorkingDayController'
				}
			},
			data: {
	            displayName: 'Working Days List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('workingday.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/workingday/html/workingday-add.html',
					controller : 'WorkingDayAddController'
				}
			},
			data: {
	            displayName: 'Add Working Days'
	        }
		}).state('workingday.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/workingday/html/workingday-add.html',
					controller : 'WorkingDayEditController'
				}
			},
			data: {
	            displayName: 'Edit Working Days'
	        },
		});
	}]);
})();