(function(){
	angular.module('lateFeesDetail').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('lateFeesDetail', {
			url : '/lateFeesDetail',
			abstract :true,
			controller : 'lateFeesDetailModelController',
			data: {
				 breadcrumbProxy: 'lateFeesDetail.list'
			}
		}).state('lateFeesDetail.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/lateFeesDetail/html/lateFeesDetail-list.html',
					controller : 'LateFeesDetailController'
				}
			},
			data: {
	            displayName: 'late Fees List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : ''
	        }
		}).state('lateFeesDetail.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/lateFeesDetail/html/lateFeesDetail-add.html',
					controller : 'LateFeesDetailAddController'
				}
			},
			data: {
	            displayName: 'Add late Fees'
	        },
	        params : {
	        	instituteId : ''
	        }
		}).state('lateFeesDetail.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/lateFeesDetail/html/lateFeesDetail-add.html',
					controller : 'LateFeesDetailEditController'
				}
			},
			data: {
	            displayName: 'Edit late Fees'
	        }
		});
	}]);
})();