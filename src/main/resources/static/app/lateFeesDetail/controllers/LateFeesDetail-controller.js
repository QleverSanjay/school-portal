(function(){
	angular.module('lateFeesDetail').controller('LateFeesDetailController',function($rootScope,$resource, $localStorage, 
			$filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.lateFeesDetailList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isSingleBranch = false;	
		$scope.branchName = "";

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;
		$rootScope.setSelectedData($scope.instituteId,$scope.branchId);

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('fees/getAllLateFees?branchId='+$scope.branchId).query().$promise.then(function(lateFeesDetailList) {
					$scope.lateFeesDetailList.length = 0;
					if(lateFeesDetailList && lateFeesDetailList.length > 0){
						angular.extend($scope.lateFeesDetailList,lateFeesDetailList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {	    		
	    			$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
	    			changeBranchList(false);
	    		
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
				});	
			}
		};

	     $scope.deleteData = function deletelateFeesDetail(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('fees/deleteLateFees?lateFeesId='+id).remove().$promise.then(function(resp) {
	        		for(var i =0; i < $scope.lateFeesDetailList.length ;i ++ ){
      			  		var lateFeesDetail = $scope.lateFeesDetailList[i];
      			  		if(lateFeesDetail.id == id){
      			  			$scope.lateFeesDetailList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});
		
	angular.module('lateFeesDetail').controller('validatelateFeeForm',function($resource,$scope,$state) {
		$scope.regExName =/^[0-9 .]{1,12}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});

	angular.module('lateFeesDetail').controller('LateFeesDetailAddController',function($rootScope,$filter, 
			$localStorage, $scope,$state,$stateParams,$resource,CommonServices) {
		$scope.lateFeesDetail = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.lateFeesDetailHeaderValue = "Add Late Fees";

		$scope.lateFeesDetail.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.lateFeesDetail.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;


		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
    			changeBranchList(false);
    		
			angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.lateFeesDetail.instituteId || $scope.lateFeesDetail.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.lateFeesDetail.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);	
				});	
			}
		};
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('lateFeesDetail.list');
		};
		$scope.dayArray = [{"label" : "Monday", "value" : "1"},
            {"label" : "Tuesday", "value" : "2"},
		    {"label" : "Wednesday", "value" : "3"},
		    {"label" : "Thursday", "value" : "4"},
		    {"label" : "Friday", "value" : "5"},
		    {"label" : "Saturday", "value" : "6"},
		    {"label" : "Sunday", "value" : "7"}
	    ]; 

	    $scope.monthDayArray = [{"label" : "1", "value" : "1"}, {"label" : "2", "value" : "2"},
		    {"label" : "3", "value" : "3"}, {"label" : "4", "value" : "4"},
		    {"label" : "5", "value" : "5"}, {"label" : "6", "value" : "6"},
		    {"label" : "7", "value" : "7"}, {"label" : "8", "value" : "8"},
		    {"label" : "9", "value" : "9"}, {"label" : "10", "value" : "10"},
		    {"label" : "11", "value" : "11"}, {"label" : "12", "value" : "12"},
		    {"label" : "13", "value" : "13"}, {"label" : "14", "value" : "14"},
		    {"label" : "15", "value" : "15"}, {"label" : "16", "value" : "16"},
		    {"label" : "17", "value" : "17"}, {"label" : "18", "value" : "18"},
		    {"label" : "19", "value" : "19"}, {"label" : "20", "value" : "20"},
		    {"label" : "21", "value" : "21"}, {"label" : "22", "value" : "22"},
		    {"label" : "23", "value" : "23"}, {"label" : "24", "value" : "24"},
		    {"label" : "25", "value" : "25"}, {"label" : "26", "value" : "26"},
		    {"label" : "27", "value" : "27"}, {"label" : "28", "value" : "28"},
		    {"label" : "29", "value" : "29"}, {"label" : "30", "value" : "30"},
		    {"label" : "31", "value" : "31"}
	    ];

		$scope.removeFromArr = function (arr, index){
			if(arr.length > 0){
				arr.splice(index, 1);
			}
		}

		$scope.combinationArr = [];
		var combinationObj = {
				frequency : '',
				start : '',
				end : ''
		};

		function validateTiers (index){
			var valid = true;
			var combination = $scope.combinationArr[index];
			var preCombination = index != 0 ? $scope.combinationArr[index-1] : null;
			combination.startDayErrMsg = "";
			combination.endDayErrMsg = "";
			combination.amountErrMsg = "";

			if(!combination.start || combination.start <= 0){
				combination.startDayErrMsg = "Start of tier is required.";
				valid = false;
			}else if(preCombination){
				if(preCombination.end >= combination.start){
					combination.startDayErrMsg = "Start of tier must be greater than previous tier end.";
					valid = false;
				}
			}

			if(!combination.end || combination.end <= 0){
				combination.endDayErrMsg = "End of tier is required.";
				valid = false;
			} else if(combination.end == 9999){
				combination.endDayErrMsg = "Can`t add one more tier as this tier will continue infinite time.";
				valid = false;
			} else if(combination.end < combination.start){
				combination.endDayErrMsg = "End of tier must be greater than start.";
				valid = false;
			}

			if(!combination.amount){
				combination.amountErrMsg = "Amount is required.";
				valid = false;
			}

			if(combination.frequency){
				if(combination.frequency == 'DAYLY' && !$scope.isDaySelected(combination.weekDays)){
					valid = false;
					combination.daysErrMsg = "Select Days when Daily";
				}
				if(combination.frequency == 'MONTHLY' && !combination.byDate){
					valid = false;
				}
			}
			return valid;
		}

		$scope.addCombination = function (index){
			var tempCombination = JSON.stringify(combinationObj);
			tempCombination = JSON.parse(tempCombination);
			if(index || index == 0){
				var valid = validateTiers(index);
				if(valid)
					$scope.combinationArr.push(tempCombination);
			}else {
				$scope.combinationArr.push(tempCombination);
			}
		}
		$scope.addCombination(null);

/*		$scope.addTier = function (){
			$scope.daysErrorMessage = "";
			$scope.amountErrorMessage = "";
			if (!$scope.lateFeesDetail.combination.amount || $scope.lateFeesDetail.combination.amount <= 0){
				$scope.amountErrorMessage = "Amount must be greater than zero.";
			}
			if (!$scope.lateFeesDetail.combination.days || $scope.lateFeesDetail.combination.days <= 0){
				$scope.daysErrorMessage = "Must be valid days.";
			}
			if($scope.lateFeesDetail.combination.amount > 0 && $scope.lateFeesDetail.combination.days > 0){
				var combination = JSON.stringify($scope.lateFeesDetail.combination);
				combination = JSON.parse(combination);
				$scope.combinationList.push(combination);
				$scope.lateFeesDetail.combination.days = '';
				$scope.lateFeesDetail.combination.amount = '';
			}
		}
*/
	     $scope.isDaySelected = function(weekDays){
	    	 var flag = false;
	    	 if(weekDays){
	    		 angular.forEach(weekDays, function(isSelected, day) {
		             if(isSelected){
		            	 flag = true;
		             }
		         });	 
	    	 }
	    	 return flag;
	     };

		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var lateFeesDetail = {
				 "branchId" : $scope.lateFeesDetail.branchId,
				 "type": $scope.lateFeesDetail.type,
				 "frequency" : $scope.lateFeesDetail.frequency,
				 "weekDays" : $scope.lateFeesDetail.frequency == 'DAILY' ? $scope.lateFeesDetail.weekDays : null,
				 "byDate" : $scope.lateFeesDetail.frequency == 'MONTHLY' ? $scope.lateFeesDetail.byDate : null,
				 "maximumAmount" : $scope.lateFeesDetail.maximumAmount,
				 "description":$scope.lateFeesDetail.description,
				 "amount": $scope.lateFeesDetail.amount,
				 "baseAmount": $scope.lateFeesDetail.baseAmount,
				 "reccuring" : ($scope.lateFeesDetail.reccuring == true || $scope.lateFeesDetail.reccuring == 'Y' ? "Y" : "N"),
			}

			if($scope.lateFeesDetail.type != 'RECURRING'){
				lateFeesDetail.frequency = null;
				lateFeesDetail.weekDays = null;
				lateFeesDetail.byDate = null;
			}

			if(lateFeesDetail.type == 'COMBINATION'){
				var combinationRule = "";
				for( var i = 0; i < $scope.combinationArr.length; i ++){
					var valid = validateTiers(i);
					if(valid){
						var rule = $scope.combinationArr[i];
						var tempCombinationRule = rule.start + "-" +rule.end + "-" + rule.amount;
						if(rule.frequency == 'DAILY'){
							tempCombinationRule +=  "-" + rule.frequency +  "-";
							angular.forEach(rule.weekDays, function(isSelected, day) {
					             if(isSelected){
					            	 tempCombinationRule +=  day + ",";
					             }
					         });
							tempCombinationRule = (tempCombinationRule.subStr(0, tempCombinationRule.lastIndexOf(",")));
						}else if(rule.frequency == 'MONTHLY'){
							tempCombinationRule +=  "-" + rule.frequency +  "-" + rule.byDate;
						}else if(rule.frequency){
							tempCombinationRule +=  "-" + rule.frequency;
						}
						combinationRule += tempCombinationRule + (i < $scope.combinationArr.length -1 ? ';' : '');
					}
					else {
						return;
					}
				}
				lateFeesDetail.combinationRule = combinationRule;
			}

			$resource('fees/saveLateFeesDetail').save(lateFeesDetail).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('lateFeesDetail.list',{
						instituteId : $scope.lateFeesDetail.instituteId,
	        			branchId : $scope.lateFeesDetail.branchId,
						status : resp.status,
						message: resp.message
					},{reload : true});
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});
		}

	});

	/*This Controller is Used for lateFeesDetail Edit functionality*/
	angular.module('lateFeesDetail').controller('LateFeesDetailEditController',function($rootScope,$filter,$scope,$state,$stateParams,$resource, CommonServices) {

		$scope.lateFeesDetail = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.lateFeesDetailHeaderValue = "Edit Late Fees";
		$scope.combinationArr = [];

		$resource('fees/editLateFees?lateFeesId='+$stateParams.id).get().$promise.then(function(lateFeesDetailToEdit) {
			console.log(lateFeesDetailToEdit.weekDays);

			angular.extend($scope.lateFeesDetail,lateFeesDetailToEdit);
			console.log("by date"+$scope.lateFeesDetail.byDate);
			$rootScope.setSelectedData($scope.lateFeesDetail.instituteId,$scope.lateFeesDetail.branchId);
			if(lateFeesDetailToEdit.combinationRule){
				var tempRuleArr = lateFeesDetailToEdit.combinationRule.split(";");
				for (var i =0 ; i< tempRuleArr.length; i++){
					var rule = tempRuleArr [i];
					var ruleArr = rule.split("-");
					var combinationRule = {
						start : parseInt(ruleArr[0]),	
						end : parseInt(ruleArr[1]),	
						amount : parseInt(ruleArr[2]),
						frequency : ruleArr.length > 3 ? ruleArr[3] : '',
						weekDays : ruleArr.length > 4 ? ruleArr[4] : '',
						byDate : ruleArr.length > 4 ? ruleArr[4] : '',
					}
					var weekDayObj = {};
					if(combinationRule.weekDays){
						var tempWeekDayArr = combinationRule.weekDays.split(",");
						for (var j=1; j<= 7; j++){
							if(tempWeekDayArr.indexOf(j+"") != -1){
								weekDayObj[j] = true;
							}
						}
					}
					combinationRule.weekDays = weekDayObj;
					$scope.combinationArr.push(combinationRule);
				}
			}else {
				$scope.addCombination(null);
			}
			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
    	$scope.dayArray = [{"label" : "Monday", "value" : "1"},
    	                   {"label" : "Tuesday", "value" : "2"},
    	       		    {"label" : "Wednesday", "value" : "3"},
    	       		    {"label" : "Thursday", "value" : "4"},
    	       		    {"label" : "Friday", "value" : "5"},
    	       		    {"label" : "Saturday", "value" : "6"},
    	       		    {"label" : "Sunday", "value" : "7"}
    	       	    ];

    	  $scope.monthDayArray = [{"label" : "1", "value" : "1"}, {"label" : "2", "value" : "2"},
    	              		    {"label" : "3", "value" : "3"}, {"label" : "4", "value" : "4"},
    	              		    {"label" : "5", "value" : "5"}, {"label" : "6", "value" : "6"},
    	              		    {"label" : "7", "value" : "7"}, {"label" : "8", "value" : "8"},
    	              		    {"label" : "9", "value" : "9"}, {"label" : "10", "value" : "10"},
    	              		    {"label" : "11", "value" : "11"}, {"label" : "12", "value" : "12"},
    	              		    {"label" : "13", "value" : "13"}, {"label" : "14", "value" : "14"},
    	              		    {"label" : "15", "value" : "15"}, {"label" : "16", "value" : "16"},
    	              		    {"label" : "17", "value" : "17"}, {"label" : "18", "value" : "18"},
    	              		    {"label" : "19", "value" : "19"}, {"label" : "20", "value" : "20"},
    	              		    {"label" : "21", "value" : "21"}, {"label" : "22", "value" : "22"},
    	              		    {"label" : "23", "value" : "23"}, {"label" : "24", "value" : "24"},
    	              		    {"label" : "25", "value" : "25"}, {"label" : "26", "value" : "26"},
    	              		    {"label" : "27", "value" : "27"}, {"label" : "28", "value" : "28"},
    	              		    {"label" : "29", "value" : "29"}, {"label" : "30", "value" : "30"},
    	              		    {"label" : "31", "value" : "31"}
    	              	    ];

 		$scope.removeFromArr = function (arr, index){
 			if(arr.length > 0){
 				arr.splice(index, 1);
 			}
 		}

		var combinationObj = {
				frequency : '',
				start : '',
				end : ''
		};

		function validateTiers (index){
			var valid = true;
			var combination = $scope.combinationArr[index];
			var preCombination = index != 0 ? $scope.combinationArr[index-1] : null;
			combination.startDayErrMsg = "";
			combination.endDayErrMsg = "";
			combination.amountErrMsg = "";

			if(!combination.start || combination.start <= 0){
				combination.startDayErrMsg = "Start of tier is required.";
				valid = false;
			}else if(preCombination){
				if(preCombination.end >= combination.start){
					combination.startDayErrMsg = "Start of tier must be greater than previous tier end.";
					valid = false;
				}
			}

			if(!combination.end || combination.end <= 0){
				combination.endDayErrMsg = "End of tier is required.";
				valid = false;
			} else if(combination.end == 9999){
				combination.endDayErrMsg = "Can`t add one more tier as this tier will continue infinite time.";
				valid = false;
			} else if(combination.end < combination.start){
				combination.endDayErrMsg = "End of tier must be greater than start.";
				valid = false;
			}

			if(!combination.amount){
				combination.amountErrMsg = "Amount is required.";
				valid = false;
			}

			if(combination.frequency){
				if(combination.frequency == 'DAYLY' && !$scope.isDaySelected(combination.weekDays)){
					valid = false;
					combination.daysErrMsg = "Select Days when Daily";
				}
				if(combination.frequency == 'MONTHLY' && !combination.byDate){
					valid = false;
				}
			}
			return valid;
		}

		$scope.addCombination = function (index){
			var tempCombination = JSON.stringify(combinationObj);
			tempCombination = JSON.parse(tempCombination);
			if(index || index == 0){
				var valid = validateTiers(index);
				if(valid)
					$scope.combinationArr.push(tempCombination);
			}else {
				$scope.combinationArr.push(tempCombination);
			}
		}

/*		$scope.addTier = function (){
			$scope.daysErrorMessage = "";
			$scope.amountErrorMessage = "";
			if (!$scope.lateFeesDetail.combination.amount || $scope.lateFeesDetail.combination.amount <= 0){
				$scope.amountErrorMessage = "Amount must be greater than zero.";
			}
			if (!$scope.lateFeesDetail.combination.days || $scope.lateFeesDetail.combination.days <= 0){
				$scope.daysErrorMessage = "Must be valid days.";
			}
			if($scope.lateFeesDetail.combination.amount > 0 && $scope.lateFeesDetail.combination.days > 0){
				var combination = JSON.stringify($scope.lateFeesDetail.combination);
				combination = JSON.parse(combination);
				$scope.combinationList.push(combination);
				$scope.lateFeesDetail.combination.days = '';
				$scope.lateFeesDetail.combination.amount = '';
			}
		}
*/
	     $scope.isDaySelected = function(weekDays){
	    	 var flag = false;
	    	 if(weekDays){
	    		 angular.forEach(weekDays, function(isSelected, day) {
		             if(isSelected){
		            	 flag = true;
		             }
		         });	 
	    	 }
	    	 return flag;
	     };

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.lateFeesDetail.instituteId || $scope.lateFeesDetail.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.lateFeesDetail.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.cancel = function(){
			$state.transitionTo('lateFeesDetail.list',{reload : true});
		};

		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var lateFeesDetail = {
				 "branchId" : $scope.lateFeesDetail.branchId,
				 "type": $scope.lateFeesDetail.type,
				 "frequency" : $scope.lateFeesDetail.frequency,
				 "weekDays" : $scope.lateFeesDetail.frequency == 'DAILY' ? $scope.lateFeesDetail.weekDays : null,
				 "byDate" : $scope.lateFeesDetail.frequency == 'MONTHLY' ? $scope.lateFeesDetail.byDate : null,
				 "maximumAmount" : $scope.lateFeesDetail.maximumAmount,
				 "description":$scope.lateFeesDetail.description,
				 "id":$scope.lateFeesDetail.id,
				 "amount": $scope.lateFeesDetail.amount,
				 "baseAmount": $scope.lateFeesDetail.baseAmount,
				 "reccuring" : ($scope.lateFeesDetail.reccuring == true || $scope.lateFeesDetail.reccuring == 'Y' ? "Y" : "N"),
			}

			if($scope.lateFeesDetail.type != 'RECURRING'){
				lateFeesDetail.frequency = null;
				lateFeesDetail.weekDays = null;
				lateFeesDetail.byDate = null;
			}

			if(lateFeesDetail.type == 'COMBINATION'){
				var combinationRule = "";
				for( var i = 0; i < $scope.combinationArr.length; i ++){
					var valid = validateTiers(i);
					if(valid){
						var rule = $scope.combinationArr[i];
						var tempCombinationRule = rule.start + "-" +rule.end + "-" + rule.amount;
						if(rule.frequency == 'DAILY'){
							tempCombinationRule +=  "-" + rule.frequency +  "-";
							angular.forEach(rule.weekDays, function(isSelected, day) {
					             if(isSelected){
					            	 tempCombinationRule +=  day + ",";
					             }
					         });
							tempCombinationRule = (tempCombinationRule.substr(0, tempCombinationRule.lastIndexOf(",")));
						}else if(rule.frequency == 'MONTHLY'){
							tempCombinationRule +=  "-" + rule.frequency +  "-" + rule.byDate;
						}else if(rule.frequency){
							tempCombinationRule +=  "-" + rule.frequency;
						}
						combinationRule += tempCombinationRule + (i < $scope.combinationArr.length -1 ? ';' : '');
					}
					else {
						return;
					}
				}
				lateFeesDetail.combinationRule = combinationRule;
			}

			$resource('fees/saveLateFeesDetail').save(lateFeesDetail).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('lateFeesDetail.list',{
						instituteId : $scope.lateFeesDetail.instituteId,
	        			branchId : $scope.lateFeesDetail.branchId,
						status : resp.status,
						message: resp.message
					},{reload : true});
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});
		}
	});
})();

