(function(){
	angular.module('division').controller('DivisionController',function($rootScope,$resource, $localStorage, $filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.divisionList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSingleBranch = false;	
		$scope.branchName = "";

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('division/getDivisionByBranch?branchId='+$scope.branchId).query().$promise.then(function(divisionList) {
					$scope.divisionList.length = 0;
					if(divisionList && divisionList.length > 0){
						angular.extend($scope.divisionList,divisionList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
					changeYearList();
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		/*
	    *//** This method is used to open Modal *//* 
	    $scope.openUploadDivisionDialog = function(){
	    	  $uibModal.open({
	    	      templateUrl: 'app/division/html/division-bulk-upload.html',
	    	      controller: 'DivisionFileUploadController',
	    	      scope : $scope
	    	     
	    	    });
	     };*/

	     $scope.deleteData = function deleteDivision(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('division').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.divisionList.length ;i ++ ){
      			  		var division = $scope.divisionList[i];
      			  		if(division.id == id){
      			  			$scope.divisionList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});

	angular.module('division').controller('validateDivisionForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,35}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});
	
	angular.module('division').controller('DivisionAddController',function($rootScope,$filter, $localStorage, $scope,$state,$stateParams,$resource,CommonServices) {
		$scope.division = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.divisionHeaderValue = "Add Division";

		$scope.division.instituteId = $localStorage.selectedInstitute;
		$scope.division.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			changeYearList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.division.instituteId){
	    			changeBranchList(false);
	    		}
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.division.instituteId || $scope.division.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.division.instituteId})
					.$promise.then(function(branchList) {
						changeYearList();
						angular.extend($scope.branchList,branchList);	
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.division.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.division.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.division.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);
	    		});
			}
		}
		
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('division.list');
		};
		
		$scope.save = function() {
			var division = {
				id : $scope.division.id,
				displayName : $scope.division.displayName,
				code : $scope.division.code,
				branchId : $scope.division.branchId,
				academicYearId : $scope.division.academicYearId
			};			
		
			$resource('division').save(division).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('division.list',
					{
						instituteId : $scope.division.instituteId,
	        			branchId : $scope.division.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});

	/*This Controller is Used for Division Edit functionality*/
	angular.module('division').controller('DivisionEditController',function($rootScope,$filter,$scope,$state,$stateParams,$resource, CommonServices) {
		
		$scope.division = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.divisionHeaderValue = "Edit Division";
		
		$resource('division/:id').get({id : $stateParams.id}).$promise.then(function(divisionToEdit) {
			angular.extend($scope.division,divisionToEdit);
			$rootScope.setSelectedData($scope.division.instituteId,$scope.division.branchId);
			
			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.division.instituteId || $scope.division.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.division.instituteId})
					.$promise.then(function(branchList) {
						/*if(branchList != null && branchList.length > 0){
							$scope.division.branchId = branchList[0].id;
						}*/
						changeYearList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){	
			if($scope.division.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.division.branchId).
						query({id : $scope.division.branchId}).$promise.then(function(academicYearList) {
						$scope.academicYearList.length = 0;
				
					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.division.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
		
		$scope.cancel = function(){
			$state.transitionTo('division.list',{reload : true});
		};
		
		$scope.save = function() {
			var division = {
				id : $scope.division.id,
				displayName : $scope.division.displayName,
				code : $scope.division.code,
				branchId : $scope.division.branchId,
				academicYearId : $scope.division.academicYearId
			};
			
			$resource('division').save(division).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('division.list',
					{
						instituteId : $scope.division.instituteId,
	        			branchId : $scope.division.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
		        }
			});
		};
	});
})();

