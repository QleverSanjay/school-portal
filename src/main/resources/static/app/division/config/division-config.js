(function(){
	angular.module('division').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('division', {
			url : '/division',
			abstract :true,
			controller : 'DivisionModelController',
			data: {
				 breadcrumbProxy: 'division.list'
			}
		}).state('division.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/division/html/division-list.html',
					controller : 'DivisionController'
				}
			},
			data: {
	            displayName: 'Division List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('division.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/division/html/division-add.html',
					controller : 'DivisionAddController'
				}
			},
			data: {
	            displayName: 'Add Division'
	        }
		}).state('division.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/division/html/division-add.html',
					controller : 'DivisionEditController'
				}
			},
			data: {
	            displayName: 'Edit Division'
	        }
		});
	}]);
})();