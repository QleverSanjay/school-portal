(function(){
	angular.module('branch').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('branch', {
			url : '/branch',
			abstract :true,
			controller : 'BranchModelController',
			data: {
				 breadcrumbProxy: 'branch.list'
			}
		}).state('branch.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/branch/html/branch-list.html',
					controller : 'BranchController'
				}
			},
			data: {
	            displayName: 'Branch List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : ''
	        }
		}).state('branch.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/branch/html/branch-add.html',
					controller : 'BranchAddController'
				}
			},
			data: {
	            displayName: 'Add Branch'
	        },
	        params : {
	        	instituteId : ''
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	}
	        }
			
		}).state('branch.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/branch/html/branch-add.html',
					controller : 'BranchEditController'
				}
			},
			data: {
	            displayName: 'Edit Branch'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	}
	        }
		}).state('branch.refund', {
			url : '/refund/config/',
			views : {
				'@' : {
					templateUrl : 'app/branch/html/refund-config.html',
					controller : 'RefundConfigController'
				}
			},
			data: {
	            displayName: 'Payment Refund Configuration'
	        }
		});
	}]);
})();