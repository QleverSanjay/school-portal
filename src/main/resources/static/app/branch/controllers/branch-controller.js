(function(){
	angular.module('branch').controller('BranchController',function($rootScope, $localStorage, ValidateButtonLinkUrl,$resource,$scope,$confirm,$stateParams,DTOptionsBuilder,$state,modalPopupService, CommonServices) {
    	$scope.branchList = [];
    	$scope.status = $stateParams.status;
    	$scope.message = $stateParams.message;
    	$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : {} );
//    	$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
    	$scope.instituteList = [];
    	$scope.isUploadFailed = false;
    	$scope.selectedInstitute = {};

    	$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true)
        .withOption("scrollX", true);

    	$scope.showButtons = function(buttonName){
    		return ValidateButtonLinkUrl.validateButtonUrl(buttonName);
    	};

	    /** This method is used to open Modal */ 
	    $scope.uploadBranches = function(){
	    	  modalPopupService.openPopup({
	    	      templateUrl: 'app/branch/html/branch-bulk-upload.html',
	    	      controller: 'BranchFileUploadController',
	    	      scope : $scope
	    	    });
	     };

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 8){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList();
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			//$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : instituteList[0].id);
		    		changeBranchList();
					angular.extend($scope.instituteList,instituteList);
	    		}
			});
    	}
    	$scope.changeBranchList = function(){
    		$scope.status = "";
        	$scope.message = "";
        	$scope.isUploadFailed = false;
    		changeBranchList();
    	};
		function changeBranchList(){
			if($scope.instituteId.id){
				$resource('branch?instituteId='+$scope.instituteId.id).query().$promise.then(function(branchList) {
						$scope.branchList.length = 0;
						angular.extend($scope.branchList,branchList);
				});
			}
		};

	     /** This method is used to delete school from the screen. It will only call delete utility method */
	     $scope.deleteData = function deleteBranch(id){
	    	 $confirm({text: 'Are you sure you want to delete?'}).then(function() {
		    	 $resource('branch').remove({id : id}).$promise.then(function(resp) {
			   	 		if(resp.status == 'success'){
				   	 		for(var i =0; i < $scope.branchList.length ;i ++ ){
			  			  		var branch = $scope.branchList[i];
			  			  		if(branch.id == id){
			  			  			$scope.branchList.splice(i , 1);
			  			  			break;
			  			  		}
			  			  	}
			   	 		}
		        		$scope.status = resp.status;
		        		$scope.message = resp.message;
					 });
		         });
		     };
		});

	angular.module('branch').controller('validateBranchForm',function($resource,$scope,$state) {
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExContact = /^[0-9]{10,10}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExAddress = /^[a-zA-Z0-9][a-zA-Z0-9.,& \- \/]+$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,45}$/;
		$scope.regExCity = /^[A-Z a-z]{2,45}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExEmailAdmin = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExUrl = /^(http(s?):\/\/)?(www\.)+[a-zA-Z0-9\.\-\_]+(\.[a-zA-Z]{2,3})+(\/[a-zA-Z0-9\_\-\s\.\/\?\%\#\&\=]*)?$/;
		$scope.regExBranch = /^[a-zA-Z][ A-Za-z0-9_@./#&%*+-]{2,45}$/;
	});


	angular.module('branch').controller('BranchAddController',function($rootScope, $localStorage, $stateParams,Upload, modalPopupService, $scope,$state,StateList,$resource,CommonServices) {
		$scope.branch = {};
		$scope.uniqueIdentifiers = [{name : 'Mobile', value : 'mobile'}, {name : 'Registration Number', value : 'registration_code'}, {name : "Email", value : 'email_address'}];
		$scope.branchAdminList = [];
		$scope.isSaveDisabled = false;
		$scope.stateList = StateList;
		$scope.districtList = [];
		$scope.talukaList = [];
		$scope.instituteList = [];
		$scope.branchHeaderValue = "Add Branch";
		$scope.branch.instituteId = $localStorage.selectedInstitute;
		$scope.validationErrMsg = "";
		$scope.validationErrors = {};
		$scope.showUniversity = true;
		
		//$scope.branch.instituteType = "COLLEGE"

		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.branch.instituteId = $rootScope.instituteId;
    	}
    	
    	var branchAdminPopupOptions = {
				templateUrl : 'app/branch/html/branch-admin-add-popup.html',
				controller : 'BranchAdminController',
				scope : $scope,
			};
    	
    	$scope.popup = function(){
    		$scope.branchAdminToEdit = {};
			modalPopupService.openPopup(branchAdminPopupOptions);
    	}
    	
    	$scope.editbranchAdmin = function(branchAdmin) {
    		$scope.branchAdminToEdit = {};
			var branchAdminToEdit = JSON.stringify(branchAdmin);
			branchAdminToEdit = JSON.parse(branchAdminToEdit);
			$scope.branchAdminToEdit = branchAdminToEdit;
			
			modalPopupService.openPopup(branchAdminPopupOptions);
		};

    	$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		function changeDistrictList(){
			$scope.districtList.length = 0;
			if($scope.branch.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.branch.stateId})
					.$promise.then(function(districtList) {
						if(districtList != null && districtList.length > 0){
							$scope.branch.districtId = ($scope.branch.districtId  ? $scope.branch.districtId  : districtList[0].id);
	        			}
						angular.extend($scope.districtList, districtList);
				});	
			}
		};
		
		$scope.instituteTypeChanged = function (){
			if($scope.branch.instituteType == 'SCHOOL'){
			
				$scope.showUniversity = false;
				$scope.showBoardAffilation = true;
			}
			else {
				$scope.showBoardAffilation = false;
				$scope.showUniversity = true;
			}
			$scope.getBoradUniverityList(); 
		}
		
		$scope.getBoradUniverityList = function(){
			
			var type = $scope.branch.instituteType;
			if(type == 'SCHOOL')
				var url = '/school/boards'
			else if(type == 'COLLEGE')
				var url = '/college/universities';
			$resource(url).query().$promise.then(function(res) {
				$scope.boardList = res;
			});	
		}
		
		$scope.getSelectedBoardUniversityDetails = function(boardUniversityId){
			
			console.log(boardUniversityId)
			$scope.branch.boardUniversityId = boardUniversityId;
		}
		
		
		$scope.getTalukaByState = function(){
			getTalukaList();
		}
		
		function getTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.branch.stateId){
				$resource('masters/talukaMasters/state/:id').query({id : $scope.branch.stateId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.branch.talukaId = ($scope.branch.talukaId  ? $scope.branch.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		}
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		$scope.removeBranchAdmin = function(email){
			for(var i = 0; i < $scope.branchAdminList.length; i++){
				var branchAdmin = $scope.branchAdminList[i];
				var flag = false;
				if(branchAdmin.email == email){
					flag = true;
					if(flag){
						$scope.branchAdminList.splice(i, 1);
					}
				}
			}
		}
		
		function changeTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.branch.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.branch.districtId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.branch.talukaId = ($scope.branch.talukaId  ? $scope.branch.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};


    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
					angular.extend($scope.instituteList,instituteList);
	    		}
			});
    	}

		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var branch = {
				active: $scope.branch.active,
				addressLine: $scope.branch.addressLine,
				area: $scope.branch.area,
				city: $scope.branch.city,
				contactEmail: $scope.branch.contactEmail,
				contactNumber: $scope.branch.contactNumber,
				districtId: $scope.branch.districtId,
				id: $scope.branch.id,
				instituteId: $scope.branch.instituteId,
				name: $scope.branch.name,
				pinCode: $scope.branch.pinCode,
				stateId: $scope.branch.stateId,
				talukaId: $scope.branch.talukaId,
				websiteUrl: $scope.branch.websiteUrl,
				active : $scope.branch.active,
				branchAdminList : $scope.branchAdminList,
				offlinePaymentsEnabled : ($scope.branch.offlinePaymentsEnabled || $scope.branch.offlinePaymentsEnabled =="Y") ? "Y":"N",
				principalName : $scope.branch.principalName,
				principalMobile :$scope.branch.principalMobile,
				principalEmailId :$scope.branch.principalEmailId,
				principalGender :$scope.branch.principalGender,
				isPayDirectEnabled:($scope.branch.isPayDirectEnabled)?'Y':'N',
				showLogoOnParentPortal:($scope.branch.showLogoOnParentPortal)?'Y':'N',
				isOtpEnabled:($scope.branch.isOtpEnabled)?'Y':'N',
				uniqueIdentifierName:$scope.branch.uniqueIdentifier,
				uniqueIdentifierLabel:$scope.branch.uniqueidentifierdisplayname,		
				instituteType : $scope.branch.instituteType
			};
			if($scope.branch.instituteType == 'COLLEGE')
				branch.university = $scope.branch.boardUniversityId
			else if($scope.branch.instituteType == 'SCHOOL')
				branch.boardAffiliation = $scope.branch.boardUniversityId;
			/*$resource('branch').save(branch).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('branch.list',{
						status : resp.status,
						message : resp.message,
						instituteId : $scope.branch.instituteId
					},{reload : true});
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});*/

			Upload.upload({
	            url: 'branch',
	            method: 'POST',
                data : branch,
	            file: $scope.branch.logoUrl,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
		        	$state.transitionTo('branch.list',
			        		{
			        			status : resp.data.status, 
			        			message : resp.data.message
			        		},
			        		{reload : true}
			        );
	        	}
	        	else{
	        		$scope.isSaveDisabled = false;
	        		if(resp.data.isValidationError){

						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	     controller: "ValidationErrorController",
				    	     scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });
			
			
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			$state.transitionTo('branch.list',{
				instituteId : $stateParams.instituteId
			},{reload : true});
		};
	});

	angular.module('branch').controller('BranchEditController',function($rootScope,$scope,$state,StateList,Upload,
			modalPopupService,  $stateParams,$resource,CommonServices) {
		/** This is written to get single school object to display on the edit screen. */
		$scope.branch = {};
		$scope.uniqueIdentifiers = [{name : 'Mobile', value : 'Mobile'}, {name : 'Registration Number', value : 'Registration Number'}, {name : "Email", value : 'Email'}, {name : "User Name", value : 'username'}];
		$scope.branchAdminList = [];
		$scope.isSaveDisabled = false;
		$scope.districtList = [];
		$scope.talukaList = [];
		$scope.stateList = StateList;
		//$scope.districtList = DistrictList;
		//$scope.talukaList = TalukaList;
		$scope.instituteList = [];
		$scope.branchHeaderValue = "Edit Branch";
		$scope.branchAdminToEdit = {};
		$scope.validationErrMsg = "";
		$scope.validationErrors = {};
		
		
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.branch.instituteId = $rootScope.instituteId;
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
//	    			$scope.branch.instituteId = instituteList[0].id;
					angular.extend($scope.instituteList,instituteList);
	    		}
			});
    	}
    	
		$resource('branch/:id').get({id : $stateParams.id}).$promise.then(function(branch) {
			if(branch.branchAdminList){
				for(var i = 0; i< branch.branchAdminList.length; i ++){
					var branchAdmin =  branch.branchAdminList[i];
					branchAdmin.uuId = i+1;
				}
			}
			if(branch.instituteType == 'COLLLEGE')
				{
					$scope.showUniversity = true;
					branch.boardUniversityId = branch.university
				}
			else if(branch.instituteType == 'SCHOOL'){
				$scope.showBoardAffilation = true;
				branch.boardUniversityId = branch.boardAffiliation
			} else if (branch.instituteType == null){
				$scope.showUniversity = true;
			}
				
			$scope.branchAdminList = branch.branchAdminList;
			
			angular.extend($scope.branch,branch);
			$scope.branch.offlinePaymentsEnabled = branch.offlinePaymentsEnabled == "Y" ? true : false;
			$scope.branch.isPayDirectEnabled=(branch.isPayDirectEnabled == 'Y') ? true:false;
			$scope.branch.showLogoOnParentPortal=(branch.showLogoOnParentPortal == 'Y') ? true:false;
			$scope.branch.isOtpEnabled=(branch.isOtpEnabled == 'Y') ? true:false;
			$scope.branch.uniqueIdentifier = branch.uniqueIdentifierName;
			$scope.branch.uniqueidentifierdisplayname = branch.uniqueIdentifierLabel;
			
			$rootScope.setSelectedData($scope.branch.instituteId,$scope.branch.branchId);
			$scope.getTalukaByState();
			$scope.getBoradUniverityList();
		});
		$scope.instituteTypeChanged = function (){
			if($scope.branch.instituteType == 'SCHOOL'){
			
				$scope.showUniversity = false;
				$scope.showBoardAffilation = true;
			}
			else {
				$scope.showBoardAffilation = false;
				$scope.showUniversity = true;
			}
			$scope.getBoradUniverityList(); 
		}
		
		$scope.getBoradUniverityList = function(){
			
			var type = $scope.branch.instituteType;
			if(type == 'SCHOOL')
				var url = '/school/boards'
			else if(type == 'COLLEGE')
				var url = '/college/universities';
			$resource(url).query().$promise.then(function(res) {
				$scope.boardList = res;
			});	
		}
		
		$scope.getSelectedBoardUniversityDetails = function(boardUniversityId){
			
			console.log(boardUniversityId)
			$scope.branch.boardUniversityId = boardUniversityId;
		}
		
		
		
		var branchAdminPopupOptions = {
				templateUrl : 'app/branch/html/branch-admin-add-popup.html',
				controller : 'BranchAdminController',
				scope : $scope,
			};

		$scope.popup = function(){
			$scope.branchAdminToEdit = {};
			modalPopupService.openPopup(branchAdminPopupOptions);
    	};
		
		$scope.editbranchAdmin = function(branchAdmin) {
			$scope.branchAdminToEdit = {};
			var branchAdminToEdit = JSON.stringify(branchAdmin);
			branchAdminToEdit = JSON.parse(branchAdminToEdit);
			$scope.branchAdminToEdit = branchAdminToEdit;
			modalPopupService.openPopup(branchAdminPopupOptions);
		};

		$scope.removeBranchAdmin = function(email){
			for(var i = 0; i < $scope.branchAdminList.length; i++){
				var branchAdmin = $scope.branchAdminList[i];
				var flag = false;
				if(branchAdmin.email == email){
					flag = true;
					if(flag){
						$scope.branchAdminList.splice(i, 1);
					}
				}
			}
		}
		
		$scope.getTalukaByState = function(){
			getTalukaList();
		}
		
		function getTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.branch.stateId){
				$resource('masters/talukaMasters/state/:id').query({id : $scope.branch.stateId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.branch.talukaId = ($scope.branch.talukaId  ? $scope.branch.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		}
		
		$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		function changeDistrictList(){
			
			if($scope.branch.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.branch.stateId})
					.$promise.then(function(districtList) {
						$scope.districtList.length = 0;
						angular.extend($scope.districtList, districtList);
						changeTalukaList();
				});	
			} 
		};
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		function changeTalukaList(){
			
			if($scope.branch.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.branch.districtId})
					.$promise.then(function(talukaList) {
						$scope.talukaList.length = 0;
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};
		
		function changeTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.branch.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.branch.districtId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.branch.talukaId = ($scope.branch.talukaId  ? $scope.branch.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};

		/*$scope.showPopupWhenInactive = function (){
			if($scope.institute.active == 'N'){
				$confirm({title: 'Are you sure ?', text : 'If branch is inactive then all '
					+'data related to this branch will be inactivated.'}).then(function() {
		    	 },
		    	 function(){
		    		 $scope.institute.active = 'Y';
		    	 });
			}
		}*/

		/** This method is used to save edited object. Here we explicitly copy $scope.schools to normal school object. This is done because $scope.school contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var branch = {
					active: $scope.branch.active,
					addressLine: $scope.branch.addressLine,
					area: $scope.branch.area,
					city: $scope.branch.city,
					contactEmail: $scope.branch.contactEmail,
					contactNumber: $scope.branch.contactNumber,
					districtId: $scope.branch.districtId,
					id: $scope.branch.id,
					instituteId: $scope.branch.instituteId,
					name: $scope.branch.name,
					pinCode: $scope.branch.pinCode,
					stateId: $scope.branch.stateId,
					talukaId: $scope.branch.talukaId,
					websiteUrl: $scope.branch.websiteUrl,
					active : $scope.branch.active,
					branchAdminList : $scope.branchAdminList,
					offlinePaymentsEnabled : ($scope.branch.offlinePaymentsEnabled || $scope.branch.offlinePaymentsEnabled =="Y") ? "Y":"N",
				    principalName : $scope.branch.principalName,
					principalMobile :$scope.branch.principalMobile,
					principalEmailId :$scope.branch.principalEmailId,
					principalGender :$scope.branch.principalGender,
					isPayDirectEnabled:($scope.branch.isPayDirectEnabled)?'Y':'N',
					showLogoOnParentPortal:($scope.branch.showLogoOnParentPortal)?'Y':'N',
				    isOtpEnabled:($scope.branch.isOtpEnabled)?'Y':'N',
				    uniqueIdentifierName:$scope.branch.uniqueIdentifier,
					uniqueIdentifierLabel:$scope.branch.uniqueidentifierdisplayname
			};

			/*$resource('branch').save(branch).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('branch.list',{
						status : resp.status,
						message : resp.message,
						instituteId : $scope.branch.instituteId
					},{reload : true});	
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
				    	      templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});*/
			
			Upload.upload({
	            url: 'branch',
	            method: 'POST',
                data : branch,
	            file: $scope.branch.logoUrl,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
		        	$state.transitionTo('branch.list',
			        		{
			        			status : resp.data.status, 
			        			message : resp.data.message
			        		},
			        		{reload : true}
			        );
	        	}
	        	else{
	        		$scope.isSaveDisabled = false;
	        		if(resp.data.isValidationError){

						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	     controller: "ValidationErrorController",
				    	     scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });
		};

		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('branch.list');
		};
	});

	/** This method is used for uploading data */
	angular.module('branch').controller('BranchFileUploadController',function($scope,Upload, modalPopupService, $uibModalStack) {
		$scope.$parent.status = "";
    	$scope.$parent.message = "";

    	$scope.upload = function (file) {
	        Upload.upload({
	            url: 'branch/uploadFile',
	            file: file,
	            data : {instituteId : $scope.instituteId},
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}

	        	$scope.$parent.status = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	
	        	$scope.branchList.length = 0;
	        	angular.extend($scope.branchList,resp.data.records);
	        });
	    };
	    
	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});

	/* This module is use to add branch admin profile in individual branch*/
	angular.module('branch').controller('BranchAdminController', ['$scope','$uibModalInstance', '$uibModalStack', 'modalPopupService', '$filter', function($scope, $uibModalInstance, $uibModalStack, modalPopupService, $filter){
		$scope.genders = 
		[	
		 	{value : 'M',
		 	text : 'MALE'},
		 	{value : 'F',
		 	text : 'FEMALE'}
	 	];

		$scope.branchAdmin = {};

		var branchAdminToEdit = JSON.stringify($scope.branchAdminToEdit);

		branchAdminToEdit = JSON.parse(branchAdminToEdit);
		$scope.branchAdmin = (branchAdminToEdit ? branchAdminToEdit : {});
		$scope.isEdit = ($scope.branchAdmin.firstName ? true : false);

		if($scope.isEdit){
			if($scope.branchAdmin.dateOfBirth){
				$scope.branchAdmin.dateOfBirth = new Date($scope.branchAdmin.dateOfBirth);
			}
		}

		$scope.headerName = ($scope.isEdit ? "Edit" : "Add");

		$scope.addBranchAdmin = function(){
			if($scope.branchAdmin.firstName) {
				var isBranchAdminExist = false;
				var branchAdminIndex = -1;

				try {
					branchAdminIndex = branchAdminExist();
				}catch(err){
					isBranchAdminExist = true;
				}

				$scope.branchAdmin.dateOfBirth = $filter('date')($scope.branchAdmin.dateOfBirth, "yyyy-MM-dd");
				if(!isBranchAdminExist){

					var branchAdmin = $scope.branchAdmin;
					branchAdmin = JSON.stringify(branchAdmin);
					branchAdmin = JSON.parse(branchAdmin);

					$scope.branchAdminList[branchAdminIndex] = branchAdmin;
					modalPopupService.closePopup(null, $uibModalStack);
				}
			}

			function branchAdminExist() {
				var branchAdminIndex = -1;

				if($scope.branchAdminList && $scope.branchAdminList.length > 0){
					for(var i = 0; i < $scope.branchAdminList.length; i++){
						var branchAdmin = $scope.branchAdminList[i];
						if(branchAdmin.uuId != $scope.branchAdmin.uuId && 
								(branchAdmin.email == $scope.branchAdmin.email 
								|| branchAdmin.primaryContact == $scope.branchAdmin.primaryContact)) {

							$scope.selectionErrorMessage = "Branch Admin already exists.";
							new Exception("Match Found Exist!!");
							break;
						}
						else if(branchAdmin.uuId == $scope.branchAdmin.uuId){
							branchAdminIndex = i;
						}
					}
				}

				if(branchAdminIndex == -1){
					branchAdminIndex = $scope.branchAdminList.length;
					$scope.branchAdmin.uuId = $scope.branchAdminList.length + 1;
				}
				return branchAdminIndex;
			}
		};

		$scope.cancelPopup = function() {
			modalPopupService.closePopup(null, $uibModalStack);
		};
		
	}]);


	angular.module('branch').controller('RefundConfigController',function($rootScope,$filter, $localStorage, $scope,$state,$stateParams,$resource,CommonServices) {
		$scope.refundConfig = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.refundConfigHeaderValue = "Add Division";

		$scope.refundConfig.instituteId = $localStorage.selectedInstitute;
		$scope.refundConfig.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.refundConfig.instituteId){
	    			changeBranchList(false);
	    		}
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.refundConfig.instituteId || $scope.refundConfig.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.refundConfig.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);	
				});
			}
		};

		$scope.getBranchRepeatConfiguration = function(){
			if($scope.refundConfig.branchId){
				$resource('branch/refund-config/:branchId').get({branchId : $scope.refundConfig.branchId})
					.$promise.then(function(refundConfig) {
						var instituteId = $scope.refundConfig.instituteId;
						$scope.refundConfig = {
								instituteId : instituteId,
								branchId : refundConfig.branchId,
								allowCreditCardRefund : refundConfig.allowCreditCardRefund == "Y" ? true : false, 
								allowCreditCardPartialRefund : refundConfig.allowCreditCardPartialRefund == "Y" ? true : false, 
								allowDebitCardRefund : refundConfig.allowDebitCardRefund == "Y" ? true : false, 
								allowDebitCardPartialRefund : refundConfig.allowDebitCardPartialRefund == "Y" ? true : false, 
								allowNetBankingRefund : refundConfig.allowNetBankingRefund == "Y" ? true : false, 
								allowNetBankingPartialRefund : refundConfig.allowNetBankingPartialRefund == "Y" ? true : false, 
								allowNeftRtgsRefund : refundConfig.allowNeftRtgsRefund == "Y" ? true : false, 
								allowNeftRtgsPartialRefund : refundConfig.allowNeftRtgsPartialRefund == "Y" ? true : false, 
								allowChequeDDRefund : refundConfig.allowChequeDDRefund == "Y" ? true : false, 
								allowChequeDDPartialRefund : refundConfig.allowChequeDDPartialRefund == "Y" ? true : false, 
								allowCashRefund : refundConfig.allowCashRefund == "Y" ? true : false, 
								allowCashPartialRefund : refundConfig.allowCashPartialRefund == "Y" ? true : false, 
						};
				});
			}
		}

		$scope.save = function() {
			var refundConfig = {
				branchId : $scope.refundConfig.branchId,
				allowCreditCardRefund : $scope.refundConfig.allowCreditCardRefund ? "Y" : "N",
				allowCreditCardPartialRefund : $scope.refundConfig.allowCreditCardPartialRefund ? "Y" : "N",
				allowDebitCardRefund : $scope.refundConfig.allowDebitCardRefund ? "Y" : "N",
				allowDebitCardPartialRefund : $scope.refundConfig.allowDebitCardPartialRefund ? "Y" : "N",
				allowNetBankingRefund : $scope.refundConfig.allowNetBankingRefund ? "Y" : "N",
				allowNetBankingPartialRefund : $scope.refundConfig.allowNetBankingPartialRefund ? "Y" : "N",
				allowNeftRtgsRefund : $scope.refundConfig.allowNeftRtgsRefund ? "Y" : "N",
				allowNeftRtgsPartialRefund : $scope.refundConfig.allowNeftRtgsPartialRefund ? "Y" : "N",
				allowChequeDDRefund : $scope.refundConfig.allowChequeDDRefund ? "Y" : "N",
				allowChequeDDPartialRefund : $scope.refundConfig.allowChequeDDPartialRefund ? "Y" : "N",
				allowCashRefund : $scope.refundConfig.allowCashRefund ? "Y" : "N",
				allowCashPartialRefund : $scope.refundConfig.allowCashPartialRefund ? "Y" : "N",
			};			

			$resource('branch/refund-config').save(refundConfig).$promise.then(function(resp) {
	        		$scope.message = resp.message;
			});
		};
	});
})();

