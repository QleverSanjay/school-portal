(function(){
	angular.module('event').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('event', {
			url : '/event',
			abstract :true,
			controller : 'EventModelController',
			data: {
				 breadcrumbProxy: 'event.list'
			}
		}).state('event.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/events/html/event-list.html',
					controller : 'EventController'
				}
			},
			data: {
	            displayName: 'Event List'
	        },
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : '',
	        	message : '',
	        	status : ''
	        }
		}).state('event.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/events/html/event-add.html',
					controller : 'EventAddController'
				}
			},
			data: {
	            displayName: 'Add Event'
	        },
	        resolve : {
	        	GroupList : function($resource){
	        		return $resource('masters/groupMasters').query().$promise;
	        	}
	        }
		}).state('event.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/events/html/event-add.html',
					controller : 'EventEditController'
				}
			},
			data: {
	            displayName: 'Edit Event'
	        },
	        resolve : {
	        	GroupList : function($resource){
	        		return $resource('masters/groupMasters').query().$promise;
	        	}
	        }
		}).state('event.view', {
			url : '/view/:id',
			views : {
				'@' : {
					templateUrl : 'app/events/html/event-view.html',
					controller : 'EventViewController'
				}
			},
			data: {
	            displayName: 'View Event'
	        }
	    });
	}]);
})();