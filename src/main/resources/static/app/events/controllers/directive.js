var myApp = angular.module('myApp', []);

myApp.controller('MyCtrl', function ($scope) {
    $scope.roles = [{
        roleId: 1,
        roleName: "Administrator"
    }, {
        roleId: 2,
        roleName: "Super User"
    }];

    $scope.user = {
        userId: 1,
        username: "JimBob",
        roles: [$scope.roles[0]]
    };
});

