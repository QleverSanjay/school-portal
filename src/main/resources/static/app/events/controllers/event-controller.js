(function(){

	angular.module('event').controller('EventController',function($rootScope,$resource,$filter,$scope,DTOptionsBuilder,$localStorage, $stateParams,$confirm,$state, modalPopupService,CommonServices) {
	    $scope.events = [];
	    $scope.branchId = '';
	    $scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	    $scope.title = "";
	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		$scope.istecherSpecificEvents = false;
		$scope.sendByUser = false;
		$scope.isAllEvent = false;
	
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", false)
		.withOption("paging", true)
		.withOption("searching", true)
        .withOption("responsive", true)
        .withOption("scrollX", true);
		
		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch;

		
		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		$scope.title = "Sent Events";
    		$scope.isAllEvent = true;
    		changeYearList();
    		filterTableBySendEvent($scope.branchId);
		}
		
		$scope.showMyEvents = function(){
			$scope.events.length = 0;
			$scope.title = "Received Events";
			$scope.isAllEvent = false;
			$scope.message = "";
			$scope.istecherSpecificEvents = true;
			filterTableByTeacherBranch($scope.branchId);
		};

		$scope.showAllEvents = function(){
			$scope.events.length = 0;
			$scope.istecherSpecificEvents = false;
			$scope.message = "";
			$scope.isAllEvent = true;
			$scope.title = "Sent Events";
			filterTableBySendEvent($scope.branchId);
		};

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
			filterTableByBranch($scope.branchId);
		};

		// get the list of events received by teacher 
		function filterTableByTeacherBranch(branchId){
			if($rootScope.id){
				$resource('events/getEventsByBranchForTeacher?branchId='+branchId+'&teacherId='+$rootScope.id).query().$promise.then(function(eventList) {
					if(eventList && eventList.length > 0){
						angular.extend($scope.events,eventList);
					}
	    		});
			}
		}

		// get the event list sent by teacher
		function filterTableBySendEvent(branchId){
			if(branchId){
				$resource('events/getSentTeacherEvent?branchId='+branchId+'&roleId='
						+$rootScope.roleId+'&userId='+$rootScope.id).query().$promise.then(function(eventList) {
					if(eventList && eventList.length > 0){
						angular.extend($scope.events,eventList);
					}
	    		});
			}
		}
		
		function filterTableByBranch(branchId){
			if(branchId){
				$resource('events/getEventsByBranch?branchId='+$scope.branchId).query().$promise.then(function(eventList) {
					$scope.events.length = 0;
					if(eventList && eventList.length > 0){
						var now = new Date();
						angular.extend($scope.events,eventList);
						angular.forEach(eventList, function(event){
				    		event.startTime = new Date(now.getFullYear()+"/"+(now.getMonth() + 1)+"/"+now.getDate()+" "+event.startTime);
				    		event.endTime = new Date(now.getFullYear()+"/"+(now.getMonth() + 1)+"/"+now.getDate()+" "+event.endTime);
				    	});
					}
	    		});
			}
		}
		

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
		if($rootScope.roleId == 6){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			$scope.branchList.length = 0;
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);   				
        				changeYearList();
        				if($stateParams.branchId){
        				filterTableByBranch($scope.branchId);
        				}
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {	    		
	    			$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
	    			changeBranchList(false);
	    		
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
							$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
	        				
							if($stateParams.branchId){
							filterTableByBranch($scope.branchId);
	        				}
	        			}
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}

	    /** This method is used to open Modal */ 
	    $scope.uploadEvents = function(){
	    	  var options = {
	    	      templateUrl: 'app/events/html/event-bulk-upload.html',
	    	      controller: 'EventFileUploadController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

	     /** This method is used to delete event from the screen. It will only call delete utility method */
	     $scope.deleteData = function deleteEvent(id){
	    	 $confirm({text: 'Are you sure you want to delete?'}).then(function() {
        	 	$resource('events').remove({id : id}).$promise.then(function(resp) {
    	 			if(resp.status == 'success'){
            	 		for(var i =0; i < $scope.events.length ;i ++ ){
          			  		var event = $scope.events[i];
          			  		if(event.id == id){
          			  			$scope.events.splice(i , 1);
          			  			break;
          			  		}
          			  	}	
    	 			}

	        		$scope.status = resp.status;
	        		$scope.message = resp.message;	
      			});
	         });
	     };
	});

	angular.module('event').controller('validateEventForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,45}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
		$scope.regExReminder = /^[0-9]{1,10}$/;
		$scope.$watch('event.startDate', validateDates);
		$scope.$watch('event.endDate', validateDates);
		
		$scope.$watch('event.startTime', validateTimes);
		$scope.$watch('event.endTime', validateTimes);
		
		function validateTimes() {
		    if (!$scope.event) return;
		    if ($scope.addEventForm.startTime.$error.invalidTime || $scope.addEventForm.endTime.$error.invalidTime) {
		        $scope.addEventForm.startTime.$setValidity("invalidTime", true);  //already invalid (per validDate directive)
		    } else {
		    	var startDate = new Date($scope.event.startDate);
		    	var endDate = new Date($scope.event.endDate);
		    	if(startDate.getDate() != endDate.getDate() && startDate.getMonth() != endDate.getMonth() 
		    			&& startDate.getYear() != endDate.getYear()){

		    		if($scope.event.endTime && $scope.event.startTime)  {
			        	
			        	var endTime = new Date($scope.event.endTime);
				        var startTime = new Date($scope.event.startTime);
				        if(!isNaN(endTime) && !isNaN(startTime)){
				        	$scope.addEventForm.endTime.$setValidity("endBeforeStart", endTime > startTime);
				        }
			        }
			        else if($scope.isEndTimeChanged && !$scope.event.endTime){
			        	
			        	$scope.addEventForm.endTime.$setValidity("endBeforeStart", true);
			        }
			        else if($scope.isStartTimeChanged && !$scope.event.startTime){
			        	$scope.addEventForm.endTime.$setValidity("endBeforeStart", true);
			        }
		    	}
		    }
		}

		$scope.isEndDateChanged = false;
		$scope.isStartTimeChanged = false;
		$scope.isEndTimeChanged = false;
		
		function validateDates() {
		    if (!$scope.event) return;
		    if ($scope.addEventForm.startDate.$error.invalidDate || $scope.addEventForm.endDate.$error.invalidDate) {
		        $scope.addEventForm.startDate.$setValidity("invalidDate", true);  //already invalid (per validDate directive)
		    } else {
		        if($scope.event.endDate && $scope.event.startDate){
			        var endDate = new Date($scope.event.endDate);
			        var startDate = new Date($scope.event.startDate);
			        if(!isNaN(endDate) && !isNaN(startDate) && ($scope.event.reccuring == undefined || $scope.event.reccuring == 'N' || $scope.event.reccuring == false)){
			        	$scope.addEventForm.endDate.$setValidity("endBeforeStart", endDate >= startDate);
			        }
			        else if(!isNaN(endDate) && ($scope.event.reccuring == 'Y' || $scope.event.reccuring == true)){
			        	$scope.addEventForm.endDate.$setValidity("endBeforeStart", endDate >= startDate);
			        }
		        }
		        else if($scope.isEndDateChanged && !$scope.event.endDate && 
		        		($scope.event.reccuring == undefined || $scope.event.reccuring == 'N' || $scope.event.reccuring == false)){
		        	
		        	$scope.addEventForm.endDate.$setValidity("endBeforeStart", true);
		        }
		    }
		}
	});

	angular.module('event').controller('EventAddController',function($rootScope,$scope,$filter,$stateParams, modalPopupService, $uibModalStack, $state,Upload,$resource,$localStorage,CommonServices) {
		$scope.event = {}; 
		$scope.hasSearchRecords = false;
		$scope.individualIds = [];
		$scope.event.individuals = [];
    	$scope.branchList = [];
		$scope.event.groupList = [];
		$scope.event.groupIdList = [];
		$scope.minDate = new Date();
		$scope.event.groupList = [];
		$scope.event.reminderSet = false;
		$scope.isOpenStartDate = false;
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.eventHeaderValue = "Add Event";
		$scope.isSaveDisabled = false;
		$scope.isOpenEndDate = false;
		$scope.acadmicStartDate;$scope.acadmicEndDate;
		$scope.isStartTimeEnabled;$scope.isEndTimeEnabled;

		$scope.indivisualStudentButtonValue = "Add Individuals";
		
		$scope.event.instituteId = $localStorage.selectedInstitute;
		$scope.event.branchId = $localStorage.selectedBranch;
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);


		$scope.validateDate = function (){
			validateDate();
		};

		$scope.validateTime = function (){
			validateTime();
		};

		// Used when teacher login.
		if($rootScope.roleId == 4){
    		$scope.event.branchId = $rootScope.branchId;
    		changeYearList();
    	}
		
		function validateDate() {
			$scope.formValid = true;
    		$scope.isStartTimeEnabled = true;
    		$scope.isEndTimeEnabled = true;
    		$scope.addEventForm.startDate.$error = {};
    		$scope.addEventForm.endDate.$error = {};
    		var today = new Date();
    		today.setHours(0, 0, 0, 0);
    		var startDate = ($scope.event.startDate != '' && $scope.event.startDate != null && $scope.event.startDate != undefined ? 
        			new Date( $scope.event.startDate) : 'Invalid Date' );

        	var endDate = ($scope.event.end_date != '' && $scope.event.end_date != null && $scope.event.end_date != undefined ? 
        			new Date( $scope.event.end_date) : 'Invalid Date' );

        	if(startDate == 'Invalid Date'){
        		$scope.addEventForm.startDate.$error.startDateRequired =  'true';
        		$scope.isStartTimeEnabled = false;
        		$scope.formValid = false;
        	}
        	else if(startDate < $scope.minDate){
        		if(startDate < $scope.acadmicStartDate){
        			$scope.addEventForm.startDate.$error.beforeAcadmicYearStart =  'true';	
        		}
        		else {
        			$scope.addEventForm.startDate.$error.beforeToday =  'true';
        		}
        		$scope.formValid = false;
        	}

        	if(endDate == 'Invalid Date' && $scope.event.recurring == false){
        		$scope.addEventForm.endDate.$error.endDateRequired =  'true';
        		$scope.isEndTimeEnabled = false;
        		$scope.formValid = false;
        	}
        	else if(endDate < startDate){
        		$scope.addEventForm.endDate.$error.beforeStartDate =  'true';
        		$scope.formValid = false;
        	}
        	else if(endDate > $scope.acadmicEndDate){
        		$scope.addEventForm.endDate.$error.afterAcadmicYearEnd =  'true';
        		$scope.formValid = false;
        	}
        	if(($scope.event.recurring == true || endDate != 'Invalid Date') && startDate != 'Invalid Date'){
        		validateTime();
        	}
		}

	/*	$scope.eventForChanged = function (checkedValue){
			$scope.parentRequiredWhenStudent = false;
			if(checkedValue == 'parent'){
				if($scope.event.forStudent && !$scope.event.forParent){
					$scope.event.forParent = true;
					$scope.parentRequiredWhenStudent = true;
				}
			}
			else if(checkedValue == 'student'){
				if($scope.event.forStudent){
					$scope.event.forStudent = true;
					$scope.event.forParent = true;
				}
			}
		};*/

		$scope.popup = function(){
	    	  var options = {
	    	      templateUrl: 'app/events/html/event-popup.html',
	    	      controller: 'EventSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	    	  
	     };

	     $scope.groupPopup = function(){
	    	  var options = {
	    	      templateUrl: 'app/events/html/event-groupPopup.html',
	    	      controller: 'EventGroupSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };
	     
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
	     if($rootScope.roleId == 6){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        			}
        			changeYearList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.event.instituteId || $scope.event.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.event.instituteId})
					.$promise.then(function(branchList) {
							changeYearList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.changeYearList = function(){
			changeYearList();
		};


		function changeYearList(){
			if($scope.event.branchId){
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.event.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i< academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.event.academicYearId = acadmicYear.id;
								if(!acadmicYear.fromDate instanceof Date){
									$scope.acadmicStartDate = new Date(acadmicYear.fromDate);
									acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
								}
								else {
									$scope.acadmicStartDate = acadmicYear.fromDate;
								}
								if(!acadmicYear.toDate instanceof Date){
									$scope.acadmicEndDate = new Date(acadmicYear.toDate);
									acadmicYear.toDate = new Date(acadmicYear.toDate);
								}
								else {
									$scope.acadmicEndDate = acadmicYear.toDate;
								}
								var date = new Date();
								date.setHours(0,0,0,0);
								if($scope.acadmicStartDate < date){
									$scope.minDate = date;
								}
								else {
									$scope.minDate = $scope.acadmicStartDate;
								}
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

	     /** Remove item from selected individuals */
	     $scope.removeIndividual = function(id){
	    	 for (var i = 0; i< $scope.event.individuals.length; i++){
	    		 var individual = $scope.event.individuals[i];
    			 if (individual.id == id) {
    		    	$scope.event.individuals.splice(i, 1);
    		    	$scope.individualIds.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };

	     /** Remove item from selected individuals */
	     $scope.removeGroup = function(id){
	    	 for (var i = 0; i< $scope.event.groupList.length; i++){
	    		 var group = $scope.event.groupList[i];
    			 if (group.id == id) {
    		    	$scope.event.groupList.splice(i, 1);
    		    	$scope.event.groupIdList.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };

	    $scope.dayArray = [{"label" : "Monday", "value" : "1"},
            {"label" : "Tuesday", "value" : "2"},
		    {"label" : "Wednesday", "value" : "3"},
		    {"label" : "Thursday", "value" : "4"},
		    {"label" : "Friday", "value" : "5"},
		    {"label" : "Saturday", "value" : "6"},
		    {"label" : "Sunday", "value" : "7"}
	    ]; 

	    $scope.monthDayArray = [{"label" : "1", "value" : "1"}, {"label" : "2", "value" : "2"},
		    {"label" : "3", "value" : "3"}, {"label" : "4", "value" : "4"},
		    {"label" : "5", "value" : "5"}, {"label" : "6", "value" : "6"},
		    {"label" : "7", "value" : "7"}, {"label" : "8", "value" : "8"},
		    {"label" : "9", "value" : "9"}, {"label" : "10", "value" : "10"},
		    {"label" : "11", "value" : "11"}, {"label" : "12", "value" : "12"},
		    {"label" : "13", "value" : "13"}, {"label" : "14", "value" : "14"},
		    {"label" : "15", "value" : "15"}, {"label" : "16", "value" : "16"},
		    {"label" : "17", "value" : "17"}, {"label" : "18", "value" : "18"},
		    {"label" : "19", "value" : "19"}, {"label" : "20", "value" : "20"},
		    {"label" : "21", "value" : "21"}, {"label" : "22", "value" : "22"},
		    {"label" : "23", "value" : "23"}, {"label" : "24", "value" : "24"},
		    {"label" : "25", "value" : "25"}, {"label" : "26", "value" : "26"},
		    {"label" : "27", "value" : "27"}, {"label" : "28", "value" : "28"},
		    {"label" : "29", "value" : "29"}, {"label" : "30", "value" : "30"},
		    {"label" : "31", "value" : "31"}
	    ];
	     // To check if either day is selected.
	   
	     $scope.isDaySelected = function(){
	    	 var flag = false;
	    	 if($scope.event.eventWeekDays){
	    		 angular.forEach($scope.event.eventWeekDays, function(isSelected, day) {
		             if(isSelected){
		            	 flag = true;
		             }
		         });	 
	    	 }
	    	 return flag;
	     };

	     /*validation for either group or individual must select*/
	     $scope.isGroupOrIndividualSelected = false;
	     
	     $scope.$watch("event.individuals", function () {
	    	 checkIsGroupOrIndividualSelected();
	    }, true);
	     
	     $scope.$watch("event.groupList", function () {
	    	 checkIsGroupOrIndividualSelected();
	    }, true);
	     
	     function checkIsGroupOrIndividualSelected(){
	    	 if($scope.event.individuals && $scope.event.individuals.length){
	    		 $scope.isGroupOrIndividualSelected = true;	
	    	 }
	    	 else if($scope.event.groupList && $scope.event.groupList.length){
	    		 $scope.isGroupOrIndividualSelected = true;
	    	 }
	    	 else{
	    		 $scope.isGroupOrIndividualSelected = false;
	    	 }
	     }

		/** This method is used to adding event. It will only call add utility method */
		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var startTimeObj = new Date($scope.event.startTime); 
			var startTime = (startTimeObj != null ? startTimeObj.getHours() + ":"+startTimeObj.getMinutes()+":"+startTimeObj.getSeconds() : null);
			
			var endTimeObj = new Date($scope.event.endTime); 
			
			console.log("endTimeObj = "+endTimeObj);
			var endTime = (endTimeObj!= null ? endTimeObj.getHours() + ":"+endTimeObj.getMinutes()+":"+endTimeObj.getSeconds() : null);
			var groupList = [];
			if($scope.event.groupList != null && $scope.event.groupList.length > 0){
				angular.forEach($scope.event.groupList, function(group){
					var groupObj = {id : group.id, name : group.name};
					groupList.push(groupObj);
				});
			}
			
			var event = {
					id : $scope.event.id,
					description: $scope.event.description,
					endDate : $filter('date')($scope.event.endDate, "yyyy-MM-dd"),
					endTime: endTime,
					forParent: ($scope.event.forParent == true || $scope.event.forParent == 'Y' ? "Y" : "N"),
					forStudent: ($scope.event.forStudent == true || $scope.event.forStudent == 'Y' ? "Y" : "N"),
					reminderSet: ($scope.event.reminderSet == true || $scope.event.reminderSet == 'Y' ? "Y" : "N"),
					reccuring : ($scope.event.reccuring == true || $scope.event.reccuring == 'Y' ? "Y" : "N"),
					forTeacher : ($scope.event.forTeacher == true || $scope.event.forTeacher == 'Y' ? "Y" : "N"),
					forVendor : ($scope.event.forVendor == true || $scope.event.forVendor == 'Y' ? "Y" : "N"),
					groupList: groupList,
					groupIdList : $scope.event.groupIdList,
					individualIdList: $scope.individualIds,
					title: $scope.event.title,
					reminderBefore: $scope.event.reminderBefore,
					startDate : $filter('date')($scope.event.startDate, "yyyy-MM-dd"),
					startTime: startTime,
					summary: $scope.event.summary,
					venue: $scope.event.venue,
					branchId : $scope.event.branchId,
					frequency : $scope.event.frequency,
					eventWeekDays : $scope.event.eventWeekDays,
					fileChanegd : $scope.event.fileChanegd,
					academicYearId : $scope.event.academicYearId,
					byDate : ($scope.event.reccuring && $scope.event.frequency == 'MONTHLY' ? $scope.event.byDate : ''),
					sentBy : $rootScope.id
				};

				/** This method is used to adding event. It will only call add utility method */
				Upload.upload({
		            url: 'events/save',
		            method: 'POST',
                    data : event,
		            file: $scope.event.image,
		        }).then(function(resp) {
		        	if(resp.data.status == 'success'){
			        	$state.transitionTo('event.list',
				        		{
				        			instituteId : $scope.event.instituteId,
				        			branchId : $scope.event.branchId,
				        			status : resp.data.status, 
				        			message : resp.data.message
				        		},{reload : true});
		        	}
		        	else{
		        		$scope.isSaveDisabled = false;
						if(resp.data.isValidationError){
							$scope.validationErrMsg = resp.data.records.title;
							$scope.validationErrors = resp.data.records.errors;
							 var options = {
								 templateUrl: "app/common/html/validationError.html",
					    	      controller: "ValidationErrorController",
					    	      scope : $scope
					    	 };
					    	 modalPopupService.openPopup(options);
						}
						else {
							$scope.status = resp.data.status;
							$scope.message = resp.data.message;	
						}
		        	}
		        });
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancelPopup = function(){
			modalPopupService.closePopup(null, $uibModalStack);
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('event.list');
		};
	});
	
	
	
	angular.module('event').controller('EventEditController',function($rootScope,$localStorage,$scope,$filter,$state,GroupList, modalPopupService, $stateParams,$resource,Upload,CommonServices) {
		/** This is written to get single event object to display on the edit screen. */
		$scope.event = {};
		$scope.isSaveDisabled = false;
		$scope.event.groupList = [];
		$scope.event.groupIdList = [];
		$scope.individualIds = [];
		$scope.event.individuals = [];
		$scope.minDate = new Date();
		$scope.isOpenStartDate = false;
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isOpenEndDate = false;
		$scope.eventHeaderValue = "Edit Event";
		$scope.acadmicStartDate;$scope.acadmicEndDate;

		$resource('events/:id').get({id : $stateParams.id}).$promise.then(function(events) {
		
			events.startDate = new Date(events.startDate);
			events.endDate = (events.endDate != null ? new Date(events.endDate) : "" );
		//	events.startTime = new Date(events.startTime);
		//	events.endTime = new Date(events.endTime);

			angular.extend($scope.event,events);

			var currentDate = new Date();
			
			$scope.event.startTime = new Date(currentDate.getFullYear()+"/"+(currentDate.getMonth() + 1)+"/"+currentDate.getDate()+" "+ $scope.event.startTime);
			$scope.event.endTime = new Date(currentDate.getFullYear()+"/"+(currentDate.getMonth() + 1)+"/"+currentDate.getDate()+" "+ $scope.event.endTime);
			
			
			$scope.event.groupList = ($scope.event.groupList == null) ? [] : $scope.event.groupList ;

			$scope.event.reccuring = (events.reccuring == 'Y' ? true : false);
			$scope.event.reminderSet = (events.reminderSet == 'Y' ? true : false);
			$scope.event.forParent = (events.forParent == 'Y' ? true : false);
			$scope.event.forStudent = (events.forStudent == 'Y' ? true : false);
			$scope.event.forTeacher = (events.forTeacher == 'Y' ? true : false);
			$scope.event.forVendor = (events.forVendor == 'Y' ? true : false);
			changeBranchList();
			angular.extend($scope.individualIds,events.individualIdList);
		});


		/*$scope.eventForChanged = function (checkedValue){
			$scope.parentRequiredWhenStudent = false;
			if(checkedValue == 'parent'){
				if($scope.event.forStudent && !$scope.event.forParent){
					$scope.event.forParent = true;
					$scope.parentRequiredWhenStudent = true;
				}
			}
			else if(checkedValue == 'student'){
				if($scope.event.forStudent){
					$scope.event.forStudent = true;
					$scope.event.forParent = true;
				}
			}
		};*/

		console.log("$scope.individualIds = "+$scope.individualIds);
		$scope.popup = function(){
	    	  var options = {
	    	      templateUrl: 'app/events/html/event-popup.html',
	    	      controller: 'EventSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

	     $scope.groupPopup = function(){
	    	  var options = {
	    	      templateUrl: 'app/events/html/event-groupPopup.html',
	    	      controller: 'EventGroupSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };
	       $rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);  //mith
	     
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.event.instituteId || $scope.event.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.event.instituteId})
					.$promise.then(function(branchList) {
						changeYearList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
	    
		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.event.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.event.branchId).
						query({id : $scope.event.branchId}).$promise.then(function(academicYearList) {
						$scope.academicYearList.length = 0;
				
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i< academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.event.academicYearId = acadmicYear.id;
								if(!acadmicYear.fromDate instanceof Date){
									$scope.acadmicStartDate = new Date(acadmicYear.fromDate);
									acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
								}
								else {
									$scope.acadmicStartDate = acadmicYear.fromDate;
								}
								if(!acadmicYear.toDate instanceof Date){
									$scope.acadmicEndDate = new Date(acadmicYear.toDate);
									acadmicYear.toDate = new Date(acadmicYear.toDate);
								}
								else {
									$scope.acadmicEndDate = acadmicYear.toDate;
								}
								var date = new Date();
								date.setHours(0,0,0,0);
								if($scope.acadmicStartDate < date){
									$scope.minDate = date;
								}
								else {
									$scope.minDate = $scope.acadmicStartDate;
								}
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
				 	angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
		
		$scope.indivisualStudentButtonValue = "Change Individuals";
	    
		/** Remove item from selected individuals */
	     $scope.removeIndividual = function(id){
	    	 for (var i = 0; i< $scope.event.individuals.length; i++){
	    		 var individual = $scope.event.individuals[i];
   			 if (individual.id == id) {
   		    	$scope.event.individuals.splice(i, 1);
   		    	$scope.individualIds.splice(i, 1);
   		    	break;
	    		 }
	    	 }
	     };
	     
	     /** Remove item from selected individuals */
	     $scope.removeGroup = function(id){
	    	 for (var i = 0; i< $scope.event.groupList.length; i++){
	    		 var group = $scope.event.groupList[i];
    			 if (group.id == id) {
    		    	$scope.event.groupList.splice(i, 1);
    		    	$scope.event.groupIdList.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };

	     $scope.dayArray = [{"label" : "Monday", "value" : "1"},
            {"label" : "Tuesday", "value" : "2"},
		    {"label" : "Wednesday", "value" : "3"},
		    {"label" : "Thursday", "value" : "4"},
		    {"label" : "Friday", "value" : "5"},
		    {"label" : "Saturday", "value" : "6"},
		    {"label" : "Sunday", "value" : "7"}
		 ];

	     $scope.monthDayArray = [{"label" : "1", "value" : "1"}, {"label" : "2", "value" : "2"},
 		    {"label" : "3", "value" : "3"}, {"label" : "4", "value" : "4"},
 		    {"label" : "5", "value" : "5"}, {"label" : "6", "value" : "6"},
 		    {"label" : "7", "value" : "7"}, {"label" : "8", "value" : "8"},
 		    {"label" : "9", "value" : "9"}, {"label" : "10", "value" : "10"},
 		    {"label" : "11", "value" : "11"}, {"label" : "12", "value" : "12"},
 		    {"label" : "13", "value" : "13"}, {"label" : "14", "value" : "14"},
 		    {"label" : "15", "value" : "15"}, {"label" : "16", "value" : "16"},
 		    {"label" : "17", "value" : "17"}, {"label" : "18", "value" : "18"},
 		    {"label" : "19", "value" : "19"}, {"label" : "20", "value" : "20"},
 		    {"label" : "21", "value" : "21"}, {"label" : "22", "value" : "22"},
 		    {"label" : "23", "value" : "23"}, {"label" : "24", "value" : "24"},
 		    {"label" : "25", "value" : "25"}, {"label" : "26", "value" : "26"},
 		    {"label" : "27", "value" : "27"}, {"label" : "28", "value" : "28"},
 		    {"label" : "29", "value" : "29"}, {"label" : "30", "value" : "30"},
 		    {"label" : "31", "value" : "31"}
 	    ];

	     // To check if either day is selected.
	   
	     $scope.isDaySelected = function(){
	    	 var flag = false;
	    	 if($scope.event.eventWeekDays){
	    		 angular.forEach($scope.event.eventWeekDays, function(isSelected, day) {
		             if(isSelected){
		            	 flag = true;
		             }
		         });	 
	    	 }
	    	 return flag;
	     };
	     
	     /*validation for either group or individual must select*/
	     $scope.isGroupOrIndividualSelected = false;
	     
	     $scope.$watch("event.individuals", function () {
	    	 checkIsGroupOrIndividualSelected();
	    }, true);
	     
	     $scope.$watch("event.groupList", function () {
	    	 checkIsGroupOrIndividualSelected();
	    }, true);
	     
	     function checkIsGroupOrIndividualSelected(){
	    	 if($scope.event.individuals && $scope.event.individuals.length){
	    		 $scope.isGroupOrIndividualSelected = true;	
	    	 }
	    	 else if($scope.event.groupList && $scope.event.groupList.length){
	    		 $scope.isGroupOrIndividualSelected = true;
	    	 }
	    	 else{
	    		 $scope.isGroupOrIndividualSelected = false;
	    	 }
	     }

	    
		/** This method is used to save edited object. Here we explicitly copy $scope.events to normal event object. This is done because $scope.event contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var startTimeObj = new Date($scope.event.startTime); 
			var startTime = (startTimeObj != "Invalid Date" ? startTimeObj.getHours() + ":"+startTimeObj.getMinutes()+":"+startTimeObj.getSeconds() : null);
			
			var endTimeObj = new Date($scope.event.endTime); 
			
			console.log("endTimeObj = "+endTimeObj);
			var endTime = (endTimeObj!= "Invalid Date" ? endTimeObj.getHours() + ":"+endTimeObj.getMinutes()+":"+endTimeObj.getSeconds() : null);
			var groupList = [];
			if($scope.event.groupList != null && $scope.event.groupList.length > 0){
				angular.forEach($scope.event.groupList, function(group){
					var groupObj = {id : group.id, name : group.name};
					groupList.push(groupObj);
				});
			}
			
			var event = {
					id : $scope.event.id,
					description: $scope.event.description,
					endDate : $filter('date')($scope.event.endDate, "yyyy-MM-dd"),
					endTime: endTime,
					forParent: ($scope.event.forParent == true ? "Y" : "N"),
					forStudent: ($scope.event.forStudent == true ? "Y" : "N"),
					reminderSet: ($scope.event.reminderSet == true ? "Y" : "N"),
					reccuring : ($scope.event.reccuring == true ? "Y" : "N"),
					forTeacher : ($scope.event.forTeacher == true ? "Y" : "N"),
					forVendor : ($scope.event.forVendor == true ? "Y" : "N"),
					groupList: groupList,
					groupIdList : $scope.event.groupIdList,
					individualIdList: $scope.individualIds,
					title: $scope.event.title,
					reminderBefore: $scope.event.reminderBefore,
					startDate : $filter('date')($scope.event.startDate, "yyyy-MM-dd"),
					startTime: startTime,
					summary: $scope.event.summary,
					venue: $scope.event.venue,
					branchId : $scope.event.branchId,
					frequency : $scope.event.frequency,
					eventWeekDays : $scope.event.eventWeekDays,
					fileChanegd : $scope.event.fileChanegd,
					academicYearId : $scope.event.academicYearId,
					byDate : ($scope.event.reccuring && $scope.event.frequency == 'MONTHLY' ? $scope.event.byDate : ''),
					sentBy : $rootScope.id
				};
				
				/** This method is used to adding event. It will only call add utility method */
				Upload.upload({
		            url: 'events/save',
		            method: 'POST',
                    data : event,
		            file: $scope.event.image,
		        }).then(function(resp) {
		        	if(resp.data.status == 'success'){
			        	$state.transitionTo('event.list', 
				        		{
				        			instituteId : $scope.event.instituteId,
				        			branchId : $scope.event.branchId,
				        			status : resp.data.status, 
				        			message : resp.data.message
				        		},{reload : true});
		        	}
		        	else{
		        		$scope.isSaveDisabled = false;
						if(resp.data.isValidationError){
							$scope.validationErrMsg = resp.data.records.title;
							$scope.validationErrors = resp.data.records.errors;
							 var options = {
								 templateUrl: "app/common/html/validationError.html",
					    	      controller: "ValidationErrorController",
					    	      scope : $scope
					    	 };
					    	 modalPopupService.openPopup(options);
						}
						else {
							$scope.status = resp.data.status;
							$scope.message = resp.data.message;	
						}
		        	}
		        });
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('event.list');
		};
	});
	
	
	/** This method is used for uploading data */
	angular.module('event').controller('EventFileUploadController',function($scope,$rootScope,Upload,modalPopupService, $uibModalStack) {
		$scope.upload = function (file) {
	        Upload.upload({
	            url: 'events/uploadFile',
	            data : {branchId : $scope.branchId, userId : $rootScope.id, roleId : $scope.roleId, academicYearId : $scope.academicYearId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
        		angular.extend($scope.events,resp.data.records);
	        });
	    };
	    
	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
	
	angular.module('event').controller('EventSearchController',['$rootScope','$scope','$resource','$uibModalStack','modalPopupService','userserviceapiurl',
	                                                    function($rootScope,$scope,$resource,$uibModalStack,modalPopupService,userserviceapiurl) {
		console.log($scope);
		$scope.clickedForSearch = false;
		$scope.searchedIndividuals = [];
		/** This method is used to search individuals based on provided details*/
		$scope.search = function(){
			console.log($scope.searchData);
			console.log($scope.$parent.event.branchId);
			$scope.searchedIndividuals = [];
			if($rootScope.roleId == 4){
				$resource(userserviceapiurl + 'user/search?search_text='+$scope.searchData.name+
						 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {
		    		 if(data.length > 0){
			    		 $scope.hasSearchRecords = true;
			    		 $scope.clickedForSearch = false;
			    		 angular.forEach( data, function(individual){
			    			 individual.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedIndividuals,data);
			    	 }
		    		 else {
		    			 $scope.hasSearchRecords = false;
		    			 $scope.clickedForSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/search?branch_id='+$scope.$parent.event.branchId+'&search_text='+$scope.searchData.name+
						 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {
		    		 if(data.length > 0){
			    		 $scope.hasSearchRecords = true;
			    		 $scope.clickedForSearch = false;
			    		 angular.forEach( data, function(individual){
			    			 individual.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedIndividuals,data);
			    	 }
		    		 else {
		    			 $scope.hasSearchRecords = false;
		    			 $scope.clickedForSearch = true;
		    		 }
		 		 });
			}
			  
			 
	     };
	     
	     /** Add individuals to an array */
	     $scope.addIndividuals  = function(){	    
	    	 console.log($scope.event.individuals);
	    	 angular.forEach( $scope.searchedIndividuals, function(individual){
	    		 if (individual.isSelected) {
    		    	$scope.event.individuals.push(individual);
    		    	$scope.individualIds.push(individual.id);
    		    }
	    	 });
	    	 modalPopupService.closePopup(null, $uibModalStack);
	     };
	     
	     $scope.cancelPopup = function(){
		    	 modalPopupService.closePopup(null, $uibModalStack);
		    };
	}]);
	

	angular.module('event').controller('EventGroupSearchController',['$rootScope','$scope','$resource','$uibModalStack','modalPopupService','userserviceapiurl',
	      function($rootScope,$scope,$resource,$uibModalStack,modalPopupService,userserviceapiurl) {
		$scope.clickedForGroupSearch = false;
		$scope.searchedGroups = []; 
		/** This method is used to search groups based on provided details*/
		$scope.searchGroup = function(){
			console.log($scope.$parent.event.branchId);
			$scope.searchedGroups = [];
			console.log($scope.searchData);
			if($rootScope.roleId == 4){	
				$resource(userserviceapiurl + 'user/groups?search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.event.groupIdList).query().
						$promise.then(function(data) {
			
		    		 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/groups?branch_id='+$scope.$parent.event.branchId+'&search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.event.groupIdList).query().
					$promise.then(function(data) {
					 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
				
				});
			}		
	     };

	     /** Add Groups to an array */
	     $scope.addGroups  = function(){
	    	 angular.forEach( $scope.searchedGroups, function(group){
	    		 if (group.isSelected) {
    		    	$scope.event.groupList.push(group);
    		    	$scope.event.groupIdList.push(group.id);
    		    }
	    	 });
	    	 modalPopupService.closePopup(null, $uibModalStack);
	     };
	     
	     $scope.cancelPopup = function(){
	    	 modalPopupService.closePopup(null, $uibModalStack);
		    };
	}]);

	/* Event View Controller..*/	
	angular.module('event').controller('EventViewController',function(CommonServices,$scope,$state,$stateParams,$resource, $rootScope, userserviceapiurl) {
		$scope.event = {};
		$resource('events/view?id='+$stateParams.id+'&teacherUserId='+$rootScope.id).get().$promise.then(function(events) {
			angular.extend($scope.event,events);
			if($scope.event.readStatus == 'N'){
				var notificationToMarkRead  = {
					event_id :  $scope.event.id,
					for_user_id : $scope.event.for_user_id
				};
				$rootScope.markAsRead(userserviceapiurl + 'event/mark/read', notificationToMarkRead).then(function(data){
					$scope.event.readStatus = 'Y';
			        console.log(data);
			    }, function (err){
			    	console.log(err);
			    });				
			}
		});
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('event.list');
		};
	});
})();

