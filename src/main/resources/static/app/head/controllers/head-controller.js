(function(){
	angular.module('head').controller('HeadController',function($rootScope,$resource,$filter,$confirm, 
			$localStorage, DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.headList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);
		
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('head/getHeadByBranch?branchId='+$scope.branchId).query().$promise.then(function(headList) {
					$scope.headList.length = 0;
					if(headList && headList.length > 0){
						angular.extend($scope.headList,headList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};
		
		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
						/*$scope.branchId = (isBranchChanged ? branchList[0].id  : 
							(!$stateParams.branchId ? branchList[0].id : $stateParams.branchId));*/
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
//					changeYearList();
				});	
			}
		};
		
		/*$scope.changeYearList = function(){
			changeYearList();
		};*/

/*		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
*/		
	     $scope.deleteData = function deleteHead(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('head').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.headList.length ;i ++ ){
      			  		var head = $scope.headList[i];
      			  		if(head.id == id){
      			  			$scope.headList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});
		
	angular.module('head').controller('validateHeadForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,45}$/;
	});
	
	angular.module('head').controller('HeadAddController',function($rootScope,$scope, $localStorage, 
			$filter,$state,$stateParams,$resource,CommonServices) {
		$scope.head = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.headHeaderValue = "Add Head";
		
		$scope.head.instituteId = $localStorage.selectedInstitute;
		$scope.head.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.head.instituteId = $rootScope.instituteId;
    		changeBranchList();
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
    	
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
 
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.head.instituteId || $scope.head.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.head.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
//							changeYearList();
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		/*$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.head.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.head.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.head.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}*/

		$scope.cancel = function(){
			CommonServices.cancelOperation('head.list');
		};

		$scope.save = function() {
			var head = {
				id : $scope.head.id,
				name : $scope.head.name,
				active : $scope.head.active,
				isDelete : $scope.head.isDelete,
				branchId : $scope.head.branchId,
//				academicYearId : $scope.head.academicYearId
			};			
		
			$resource('head').save(head).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('head.list',
					{
						instituteId : $scope.head.instituteId,
	        			branchId : $scope.head.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});

	/*This Controller is Used for Division Edit functionality*/
	angular.module('head').controller('HeadEditController',function($rootScope,$scope,$filter,$state, 
			$stateParams,$resource, CommonServices) {

		$scope.head = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.headHeaderValue = "Edit Head";

		$resource('head/:id').get({id : $stateParams.id}).$promise.then(function(headToEdit) {
			angular.extend($scope.head,headToEdit);
			$rootScope.setSelectedData($scope.head.instituteId,$scope.head.branchId);
			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.head.instituteId || $scope.head.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.head.instituteId})
					.$promise.then(function(branchList) {
//						changeYearList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		/*$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){	
			if($scope.head.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.head.branchId).
						query({id : $scope.head.branchId}).$promise.then(function(academicYearList) {
						$scope.academicYearList.length = 0;
				
					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.head.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}*/
		
		$scope.cancel = function(){
			$state.transitionTo('head.list',{reload : true});
		};
		
		$scope.save = function() {
			var head = {
					id : $scope.head.id,
					name : $scope.head.name,
					active : $scope.head.active,
					isDelete : $scope.head.isDelete,
					branchId : $scope.head.branchId,
//					academicYearId : $scope.head.academicYearId
			};
			
			$resource('head').save(head).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('head.list',
					{
						instituteId : $scope.head.instituteId,
	        			branchId : $scope.head.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
		        }
			});
		};
	});
})();

