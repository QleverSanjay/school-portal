(function(){
	angular.module('head').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('head', {
			url : '/head',
			abstract :true,
			controller : 'HeadModelController',
			data: {
				 breadcrumbProxy: 'head.list'
			}
		}).state('head.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/head/html/head-list.html',
					controller : 'HeadController'
				}
			},
			data: {
	            displayName: 'Fees Head List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('head.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/head/html/head-add.html',
					controller : 'HeadAddController'
				}
			},
			data: {
	            displayName: 'Add Fees Head'
	        }
		}).state('head.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/head/html/head-add.html',
					controller : 'HeadEditController'
				}
			},
			data: {
	            displayName: 'Edit Fees Head'
	        },
		});
	}]);
})();