(function(){
	angular.module('holiday').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('holiday', {
			url : '/holiday',
			abstract :true,
			controller : 'HolidayModelController',
			data: {
				 breadcrumbProxy: 'holiday.list'
			}
		}).state('holiday.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/holiday/html/holiday-list.html',
					controller : 'HolidayController'
				}
			},
			data: {
	            displayName: 'Holiday List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('holiday.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/holiday/html/holiday-add.html',
					controller : 'HolidayAddController'
				}
			},
			data: {
	            displayName: 'Add Holiday'
	        }
		}).state('holiday.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/holiday/html/holiday-add.html',
					controller : 'HolidayEditController'
				}
			},
			data: {
	            displayName: 'Edit Holiday'
	        }
		});
	}]);
})();