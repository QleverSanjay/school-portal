(function(){
	angular.module('holiday').controller('HolidayController',function($rootScope, $localStorage, modalPopupService, $resource,$filter,$confirm,DTOptionsBuilder,$stateParams,
			$scope,$state,$uibModal,CommonServices) {

		$scope.holidayList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	//	angular.extend($scope.divisionList,DivisionListData);

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", true)
		.withOption("paging", true)
		.withOption("searching", true)
		.withOption("sorting", true)
        .withOption("responsive", true)
        .withOption("scrollX", true);

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		 /** This method is used to open Modal */ 
	    $scope.openUploadHolidayDialog = function(){
	    	  var options = {
	    	      templateUrl: 'app/holiday/html/holiday-bulk-upload.html',
	    	      controller: 'HolidayFileUploadController',
	    	      scope : $scope
	    	     
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('holiday/getHolidayByBranch?branchId='+$scope.branchId).query().$promise.then(function(holidayList) {
					$scope.holidayList.length = 0;
					if(holidayList && holidayList.length > 0){
						angular.extend($scope.holidayList,holidayList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				if(branchList.length == 1){
        					$scope.branchId = (!$scope.branchId ? branchList[0].id : $scope.branchId);
        					/*$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;*/
        				}
        				changeYearList();
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    			setSelected();
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};
		
		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
						$scope.branchId = (isBranchChanged ? branchList[0].id  : 
							(!$scope.branchId ? branchList[0].id : $scope.branchId));
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
					changeYearList();
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		/*$scope.holidayList = [ { "id": 1, "title": "Republic Day","description":"description1" ,"toDate":"26-01-2017","fromDate":"26-01-2017"},
		            	         { "id": 2, "title": "Maha Shivratri" ,"description":"description2","toDate":"24-02-2017","fromDate":"24-02-2017"},
		            	         { "id": 3, "title": "Holi" ,"description":"description3","toDate":"13-03-2017","fromDate":"13-03-2017"},
		            	         { "id": 4, "title": "Ram Navami","description":"description4","toDate":"04-04-2017","fromDate":"04-04-2017"},
		            	         { "id": 5, "title": "Mahavir Jayanti","description":"description4","toDate":"09-04-2017","fromDate":"09-04-2017"},
		            	         { "id": 6, "title": "Good Friday ","description":"description4","toDate":"14-04-2017","fromDate":"14-04-2017"},
		            	         { "id": 7, "title": "Buddha Purnima","description":"description4","toDate":"10-05-2017","fromDate":"10-05-2017"},
		            	         { "id": 8, "title": "Idu'l Fitr","description":"description4","toDate":"26-06-2017","fromDate":"26-06-2017"},
		            	         { "id": 9, "title": "Independence Day","description":"description4","toDate":"15-08-2017","fromDate":"15-08-2017"},
		            	         { "id": 10, "title": "Id-ul-Zuha(Bakrid)","description":"description4","toDate":"02-09-2017","fromDate":"02-09-2017"},
		            	         { "id": 11, "title": "Dussehra","description":"description4","toDate":"30-09-2017","fromDate":"30-09-2017"},
		            	         { "id": 12, "title": "Muharram","description":"description4","toDate":"01-10-2017","fromDate":"01-10-2017"},
		            	         { "id": 13, "title": "Mahatma Gandhi's Birthday ","description":"description4","toDate":"02-10-2017","fromDate":"02-10-2017"},
		            	         { "id": 14, "title": "Diwali (Deepavali) ","description":"description4","toDate":"19-10-2017","fromDate":"19-10-2017"},
		            	         { "id": 15, "title": "Guru Nanak's Birthday ","description":"description4","toDate":"04-11-2017","fromDate":"04-11-2017"},
		            	         { "id": 16, "title": "Milad-Un-Nabi or Id-E-Milad (Birthday of Prophet Mohammad) *","description":"description4","toDate":"02-12-2017","fromDate":"02-12-2017"},
		            	         { "id": 17, "title": "Christmas","description":"description4","toDate":"25-12-2017","fromDate":"25-12-2017"}
		                   ];*/
		
		$scope.holidayList=[];
		function setSelected(){
			$resource('holiday/publicHolidayList').query()
			.$promise.then(function(holidayList) {
				$scope.holidayList = holidayList;
				console.log($scope.holidayList);
		});
		}

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}

	     $scope.deleteData = function deleteDivision(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('holiday').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.holidayList.length ;i ++ ){
      			  		var holiday = $scope.holidayList[i];
      			  		if(holiday.id == id){
      			  			$scope.holidayList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});
		
	angular.module('holiday').controller('validateHolidayForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,35}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
		
		$scope.$watch('holiday.fromDate', validateDates);
		$scope.$watch('holiday.toDate', validateDates);

		function validateDates() {
		    if (!$scope.holiday) return;
		    if ($scope.addHoliday.fromDate.$error.invalidDate || $scope.addHoliday.toDate.$error.invalidDate) {
		        $scope.addHoliday.fromDate.$setValidity("invalidDate", true);  //already invalid (per validDate directive)
		    } else {
		        //depending on whether the user used the date picker or typed it, this will be different (text or date type).  
		        //creating a new date object takes care of that. 
		        var toDate = new Date($scope.holiday.toDate);
		        var fromDate = new Date($scope.holiday.fromDate);
		        if(!isNaN(toDate) && !isNaN(fromDate)){
		        	$scope.addHoliday.fromDate.$setValidity("endBeforeStart", toDate >= fromDate);
		        }
		    }
		}
	});

	angular.module('holiday').controller('HolidayAddController',function($rootScope,$scope, modalPopupService, $localStorage, $filter,$state,$stateParams,$resource,CommonServices) {
		$scope.holiday = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isOpenStartDate = false;
		$scope.isOpenEndDate = false;
		$scope.holidayHeaderValue = "Add Holiday";
		$scope.acadmicStartDate; $scope.acadmicEndDate;


		$scope.holiday.instituteId = $localStorage.selectedInstitute;
		$scope.holiday.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				changeYearList();
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
	    		setSelected();
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
 
		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.holiday.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.holiday.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.holiday.currentAcademicYearId = acadmicYear.id;
								if(!acadmicYear.fromDate instanceof Date){
									$scope.acadmicStartDate = new Date(acadmicYear.fromDate);
									acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
								}
								else {
									$scope.acadmicStartDate = acadmicYear.fromDate;
								}
								if(!acadmicYear.toDate instanceof Date){
									$scope.acadmicEndDate = new Date(acadmicYear.toDate);
									acadmicYear.toDate = new Date(acadmicYear.toDate);
								}
								else {
									$scope.acadmicEndDate = acadmicYear.toDate;
								}
								
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
		$scope.setSelected=function(){
			setSelected();
			
		}
		
		$scope.holidayList=[];
		function setSelected(){
			$resource('holiday/publicHolidayList').query()
			.$promise.then(function(holidayList) {
				$scope.holidayList = holidayList;
				console.log($scope.holidayList);
		});
		}
		
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.holiday.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.holiday.instituteId})
					.$promise.then(function(branchList) {
						changeYearList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('holiday.list');
		};
		
		$scope.save = function() {
			
			$scope.selectHoliday = function (holiday){
				if(holiday.checked){
					$scope.selectedHolidayList.push(holiday);
				}
				/*else if($scope.selectedHolidayList.length > 0){
					for(var i =0; i < $scope.selectedHolidayList.length; i++){
						var tempHoliday = $scope.selectedHolidayList[i];
						if(holiday.id == tempHoliday.id){
							$scope.selectedHolidayList.splice(i, 1);
							break;
						}
					}*/
			
			
			
			
			
			saveData('holiday/multiple', tempHolidays, function (resp) {
				$scope.holidayMsglabel = resp.data.message;
	        	if(resp.data.status == 'success'){
	        		$scope.disable.holiday = true;
	        		setTimeout (function (){
	        			toggleView('holiday', 'feesCode');
	        			$scope.holidayMsglabel  = "";
	        		}, 900)
	        		return resp;
	        	}
	        	else{
	        		showErrors(resp);
	        	}
			}, function (err){
				$scope.holidayMsglabel = "Problem while saving holidays";
			});
			
			
			
			/*var holiday = {
				id : $scope.holiday.id,
				currentAcademicYearId : $scope.holiday.currentAcademicYearId,
				fromDate : $filter('date')($scope.holiday.fromDate, "yyyy-MM-dd"),
				toDate : $filter('date')($scope.holiday.toDate, "yyyy-MM-dd"),
				title : $scope.holiday.title,
				description : $scope.holiday.description,
				branchId : $scope.holiday.branchId
			};			*/

			/*$resource('holiday').save(holiday).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('holiday.list',
					{
						instituteId : $scope.holiday.instituteId,
	        			branchId : $scope.holiday.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
        		}
			});*/
		};
		
			}
	});


	/** This method is used for uploading data */
	angular.module('holiday').controller('HolidayFileUploadController',function($rootScope,$scope,Upload, modalPopupService, $uibModalStack,$state) {
		$scope.uploadHoliday = function (file) {
	        Upload.upload({
	            url: 'holiday/uploadHoliday',
	            data : {branchId : $scope.branchId, academicYearId : $scope.academicYearId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.holidayList.length = 0;
	        	$scope.$parent.isUploadingFailed = (resp.data.status == 'fail' ? true : false);
	        	$scope.$parent.message = resp.data.message;
	        	angular.extend($scope.holidayList,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
	
	/*This Controller is Used for Division Edit functionality*/
	angular.module('holiday').controller('HolidayEditController',function($rootScope, modalPopupService, $scope,$state,$filter, 
			$stateParams,$resource, CommonServices) {

		$scope.holiday = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isOpenStartDate = false;
		$scope.isOpenEndDate = false;
		$scope.holidayHeaderValue = "Edit Holiday";
		$scope.acadmicStartDate; 
		$scope.acadmicEndDate;
		
		$resource('holiday/:id').get({id : $stateParams.id}).$promise.then(function(holidayToEdit) {
			holidayToEdit.fromDate = new Date(holidayToEdit.fromDate);
			holidayToEdit.toDate = new Date(holidayToEdit.toDate);
			angular.extend($scope.holiday,holidayToEdit);
			$rootScope.setSelectedData($scope.holiday.instituteId,$scope.holiday.branchId);
			changeBranchList();
		});
		
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){
			if($scope.holiday.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.holiday.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.holiday.currentAcademicYearId = acadmicYear.id;
								if(!acadmicYear.fromDate instanceof Date){
									$scope.acadmicStartDate = new Date(acadmicYear.fromDate);
									acadmicYear.fromDate  = new Date(acadmicYear.fromDate);
								}
								else {
									$scope.acadmicStartDate = acadmicYear.fromDate;
								}
								if(!acadmicYear.toDate instanceof Date){
									$scope.acadmicEndDate = new Date(acadmicYear.toDate);
									acadmicYear.toDate = new Date(acadmicYear.toDate);
								}
								else {
									$scope.acadmicEndDate = acadmicYear.toDate;
								}
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
    	
    	
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.holiday.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.holiday.instituteId})
					.$promise.then(function(branchList) {
						changeYearList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
	
		$scope.cancel = function(){
			$state.transitionTo('holiday.list',{reload : true});
		};
		
		$scope.save = function() {
			var holiday = {
					id : $scope.holiday.id,
					currentAcademicYearId : $scope.holiday.currentAcademicYearId,
					fromDate : $filter('date')($scope.holiday.fromDate, "yyyy-MM-dd"),
					toDate : $filter('date')($scope.holiday.toDate, "yyyy-MM-dd"),
					title : $scope.holiday.title,
					description : $scope.holiday.description,
					branchId : $scope.holiday.branchId
				};	
			
			
			$resource('holiday').save(holiday).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('holiday.list',
					{
						instituteId : $scope.holiday.instituteId,
	        			branchId : $scope.holiday.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
		        }
			});
		};
		
	});
})();

