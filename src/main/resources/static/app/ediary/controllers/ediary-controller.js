(function(){
	angular.module('ediary').controller('EdiaryController',function($rootScope,userserviceapiurl,$filter,$resource,$confirm,$stateParams,DTOptionsBuilder,$scope,$state,$uibModal,CommonServices) {
		$scope.branchId = '';
		$scope.ediaryList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	    $scope.title = "";
	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		$scope.istecherSpecificNotices = false;
		$scope.sendByUser = false;
		$scope.isAllDiary = false;
		
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);
	
		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		$scope.title = "All E-diary";
    		$scope.isAllDiary = true;
    		changeYearList();
    		filterTableByBranch($scope.branchId);
		}
		
		/*$scope.showMyEdiary = function(){
			$scope.title = "My E-diary";
			$scope.isAllDiary = false;
			$scope.istecherSpecificNotices = true;
			filterTableByTeacherBranch();
		};*/
		
		$scope.showAllEdiarys = function(){
			$scope.istecherSpecificNotices = false;
			$scope.isAllDiary = true;
			$scope.title = "All E-diary";
			changeYearList();
			filterTableByBranch($scope.branchId);
		};
		
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
			filterTableByBranch($scope.branchId);
		};

	
		function filterTableByBranch(branchId){
			if(branchId){
				method: 'GET',
				$resource(userserviceapiurl+'ediary/list').query().$promise.then(function(response) {
						angular.extend($scope.ediaryList,response);
				});
			}
		}
		
		function filterTableByTeacherBranch(){
			if($rootScope.id){
				$resource('ediary/getNoticeByBranchForTeacher?teacherId='+$rootScope.id).query().$promise.then(function(ediaryList) {
					$scope.ediaryList.length = 0;
					if(ediaryList && ediaryList.length > 0){
						angular.extend($scope.ediaryList,ediaryList);
					}
	    		});
			}
		}
		
		
		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			$scope.branchList.length = 0;
        			if(branchList != null && branchList.length > 0){
        				$scope.branchId = (!$stateParams.branchId ? branchList[0].id : $stateParams.branchId);
        				if($rootScope.roleId == 6  && branchList.length == 1){
        					$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;
        				}
        				changeYearList();
        				filterTableByBranch($scope.branchId);
        				angular.extend($scope.branchList,branchList);
        			}
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $stateParams.instituteId){
	    			$scope.instituteId = $stateParams.instituteId;
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.branchId = (isBranchChanged ? branchList[0].id  : 
								(!$stateParams.branchId ? branchList[0].id : $stateParams.branchId));
							
	        				if(!isBranchChanged){
	        					filterTableByBranch($scope.branchId);
	        				}
	        			}
						angular.extend($scope.branchList,branchList);
						changeYearList();
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}
		
		$scope.view = function(ediary){
			$state.transitionTo('ediary.view',{ediary : ediary},{reload : true});
		};
		
	/*	
		$scope.deleteData = function deleteEdiary(id){
		   	 $confirm({text: 'Are you sure you want to delete?'})
		         .then(function() {
		        	 $resource('ediary').remove({id : id}).$promise.then(function(resp) {
		    	 			if(resp.status == 'success'){
		            	 		for(var i =0; i < $scope.ediaryList.length ;i ++ ){
		          			  		var ediary = $scope.ediaryList[i];
		          			  		if(ediary.id == id){
		          			  			$scope.ediaryList.splice(i , 1);
		          			  			break;
		          			  		}
		          			  	}	
		    	 			}
			        		$scope.status = resp.status;
			        		$scope.message = resp.message;	
		      			});
		         });
		     };*/
	});

	angular.module('ediary').controller('validateEdiaryForm',function($resource,$scope,$state) {
		$scope.regExTitle = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});
	
	
	angular.module('ediary').controller('EdiaryViewController',function($scope,$state,$stateParams, userserviceapiurl,
			$http,$resource,CommonServices, $rootScope) {

		$scope.ediary = $stateParams.ediary;
/*		var notificationToMarkRead  = {
			ediary_id :  $scope.ediary.id,
			for_user_id : $scope.ediary.for_user_id
		};

		$rootScope.markAsRead(userserviceapiurl + 'ediary/mark/read', notificationToMarkRead).then(function(data){
			$scope.ediary.read_status = 'Y';
	        console.log(data);
	    }, function (err){
	    	console.log(err);
	    });*/

		$scope.fileName = "";
		if($scope.ediary.image_url != null){
			var fileName = $scope.ediary.image_url;
			fileName = fileName.substring(fileName.lastIndexOf("/")+1, fileName.length);
			$scope.fileName = fileName;
		}
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('ediary.list');
		};
	});
	
	
	angular.module('ediary').controller('EdiaryAddController',function(CommonServices,$filter,userserviceapiurl,Upload,$rootScope,$scope,$uibModal,$state,$stateParams,$resource) {
		$scope.ediary = {};
		$scope.hasSearchRecords = false;
		$scope.hasGroupSearchRecords = false;
		$scope.individualIds = [];
		$scope.ediary.individuals = [];
	
		$scope.ediary.groupList = [];
		$scope.ediary.groupIdList = [];

		$scope.status = "";
		$scope.message = "";
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.isSaveDisabled = false;
		$scope.isIndividualOrGroupSelected = false;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				$scope.branchId = branchList[0].id;
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}
    	
    	// Used when teacher login.
    	if($rootScope.roleId == 4){
    		$scope.ediary.branchId = $rootScope.branchId;
    		changeYearList();
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			$scope.ediary.instituteId = instituteList[0].id;
	    		angular.extend($scope.instituteList,instituteList);
	    		changeBranchList();
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			if($scope.ediary.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.ediary.instituteId})
					.$promise.then(function(branchList) {
						
					$scope.branchList.length = 0;
					if(branchList != null && branchList.length > 0){
						$scope.ediary.branchId = branchList[0].id;
						changeYearList();
        			}
					angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		$scope.changeYearList = function(){
			changeYearList();
		};
		
		
		function changeYearList(){
			if($scope.ediary.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.ediary.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.ediary.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}
		
		
		$scope.$watch("ediary.individuals", function() {
			checkIsIndividualOrGroupSelected();
		}, true);
		
		function checkIsIndividualOrGroupSelected (){
			if ($scope.ediary.individuals && $scope.ediary.individuals.length > 0) {
				$scope.isIndividualOrGroupSelected = true;
			} else if ($scope.ediary.groupList && $scope.ediary.groupList.length > 0) {
				$scope.isIndividualOrGroupSelected = true;
			} else {
				$scope.isIndividualOrGroupSelected = false;
			}
		}
		
		$scope.$watch("ediary.groupList", function() {
			checkIsIndividualOrGroupSelected();
		}, true);
		
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('ediary.list');
		};
		
		$scope.indivisualStudentButtonValue = "Add Individuals";
		
		$scope.popup = function(){
	    	  $uibModal.open({
	    	      templateUrl: 'app/ediary/html/ediary-popup.html',
	    	      controller: 'EdiarySearchController',
	    	      scope : $scope
	    	    });
	     };

	     $scope.groupPopup = function(){
	    	  $uibModal.open({
	    	      templateUrl: 'app/ediary/html/ediary-groupPopup.html',
	    	      controller: 'EdiaryGroupSearchController',
	    	      scope : $scope
	    	    });
	     };
	     
	     /** Remove item from selected individuals */
	     $scope.removeIndividual = function(id){
	    	 for (var i = 0; i< $scope.ediary.individuals.length; i++){
	    		 var individual = $scope.ediary.individuals[i];
    			 if (individual.id == id) {
    		    	$scope.ediary.individuals.splice(i, 1);
    		    	$scope.individualIds.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };
		
	     /** Remove item from selected individuals */
	     $scope.removeGroup = function(id){
	    	 for (var i = 0; i< $scope.ediary.groupList.length; i++){
	    		 var group = $scope.ediary.groupList[i];
    			 if (group.id == id) {
    		    	$scope.ediary.groupList.splice(i, 1);
    		    	$scope.ediary.groupIdList.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };
	     
		$scope.save = function() {
			
			var ediary = {
				"date" : $filter('date')($scope.ediary.ediaryDate,"yyyy-MM-dd"),
				"description" : $scope.ediary.description,
				"group_id" : ($scope.ediary.groupIdList != null && $scope.ediary.groupIdList.length > 0 ? $scope.ediary.groupIdList : null),
				"individual_user_id" : $scope.individualIds,
				"attachment" : {
					"file_name_with_extension": ($scope.ediary.image != null ? $scope.ediary.image.filename : ""),
					"data": ($scope.ediary.image != null ? $scope.ediary.image.base64 : ""),
				}
			};
			
			
			$resource(userserviceapiurl+'ediary').save(ediary).$promise.then(function(response) {
				$state.transitionTo('ediary.list',{},{reload : true});
			});
		};
	});
	
	angular.module('ediary').controller('EdiarySearchController',['$rootScope','$scope','$resource','$uibModalInstance','userserviceapiurl',
	      function($rootScope,$scope,$resource,$uibModalInstance,userserviceapiurl) {

		$scope.clickedForSearch = false;
		$scope.searchedIndividuals = []; 
		/** This method is used to search individuals based on provided details*/
		$scope.search = function(){
			$scope.searchedIndividuals = [];
			console.log($scope.searchData);
			if($rootScope.roleId == 4){
				$resource(userserviceapiurl + 'user/search?search_text='+$scope.searchData.name+
							 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {

					if(data.length > 0){
			    		 $scope.hasSearchRecords = true;
			    		 $scope.clickedForSearch = false;
			    		 console.log("BEFORE :::: "+data.length);
			    		 console.log(JSON.stringify(data));
			    		 
			    		 var tempData = [];
			    		 for(var i = 0; i < data.length; i++){
			    			 var individual = data[i];
			    			 individual.isSelected = false;
			    			 if(individual.role != "TEACHER"){
			    				 tempData.push(individual);
			    			 }
			    		 }

			    		 console.log("AFTER :::::::::::::::::::::::::::::::::: "+tempData.length);
			    		 console.log(""+JSON.stringify(tempData));
			    		 angular.extend($scope.searchedIndividuals,tempData);
			    	 }
		    		 else {
		    			 $scope.hasSearchRecords = false;
		    			 $scope.clickedForSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/search?branch_id='+$scope.$parent.ediary.branchId+'&search_text='+$scope.searchData.name+
						 '&exclude_user_id='+$scope.individualIds).query().$promise.then(function(data) {
			
				if(data.length > 0){
		    		 $scope.hasSearchRecords = true;
		    		 $scope.clickedForSearch = false;
		    		 angular.forEach( data, function(individual){
		    			 individual.isSelected = false;
			    	 });
		    		 angular.extend($scope.searchedIndividuals,data);
		    	 }
	    		 else {
	    			 $scope.hasSearchRecords = false;
	    			 $scope.clickedForSearch = true;
	    		 }
	 		 });
			}	
	     };
	     
	     
	     /** Add individuals to an array */
	     $scope.addIndividuals  = function(){	    
	    	 console.log($scope.ediary.individuals);
	    	 angular.forEach( $scope.searchedIndividuals, function(individual){
	    		 if (individual.isSelected) {
    		    	$scope.ediary.individuals.push(individual);
    		    	$scope.individualIds.push(individual.id);
    		    }
	    	 });
	    	 $uibModalInstance.dismiss('cancel');
	     };
	     
	     $scope.cancelPopup = function(){
		    	$uibModalInstance.dismiss('cancel');
		    };
	}]);
	
	
	angular.module('ediary').controller('EdiaryGroupSearchController',['$rootScope','$scope','$resource','$uibModalInstance','userserviceapiurl',
	     function($rootScope,$scope,$resource,$uibModalInstance,userserviceapiurl) {
		$scope.clickedForGroupSearch = false;
		$scope.searchedGroups = []; 
		/** This method is used to search groups based on provided details*/
		$scope.searchGroup = function(){
			$scope.searchedGroups = [];
			console.log($scope.searchData);

			if($rootScope.roleId == 4){	
				$resource(userserviceapiurl + 'user/groups?search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.ediary.groupIdList).query().$promise.then(function(data) {
		    		 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
		 		 });
			}else if($rootScope.roleId != 4){
				$resource(userserviceapiurl + 'user/groups?branch_id='+$scope.$parent.ediary.branchId+'&search_text='+$scope.searchData.name+'&exclude_group_id='+$scope.ediary.groupIdList).query().
				$promise.then(function(data) {
					 if(data.length > 0){
			    		 $scope.hasGroupSearchRecords = true;
			    		 $scope.clickedForGroupSearch = false;
			    		 angular.forEach( data, function(group){
			    			 group.isSelected = false;
				    	 });
			    		 angular.extend($scope.searchedGroups,data);
			    	 }
		    		 else {
		    			 $scope.hasGroupSearchRecords = false;
			    		 $scope.clickedForGroupSearch = true;
		    		 }
				});
			}
	     };
	     
	     /** Add Groups to an array */
	     $scope.addGroups  = function(){
	    	 angular.forEach( $scope.searchedGroups, function(group){
	    		 if (group.isSelected) {
    		    	$scope.ediary.groupList.push(group);
    		    	$scope.ediary.groupIdList.push(group.id);
    		    }
	    	 });
	    	 $uibModalInstance.dismiss('cancel');
	     };
	     
	     $scope.cancelPopup = function(){
		    	$uibModalInstance.dismiss('cancel');
		    };
	}]);
	
})();

