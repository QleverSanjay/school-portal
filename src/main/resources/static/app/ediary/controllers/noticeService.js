/**
 * 1. user visits dashboard
	get notifications from rest service which will use lastnoticeid if available
		which will add record to db and return list from db
		it will also store lastnoticeid in generic database
		
	sort n set first four records from list return by above services..
	also set unread notification count
	
2. user visits alert list
	get notifications from rest service which will use lastnoticeid if available
		which will add/update record to db and return list from db
		it will also store lastnoticeid in generic database

3. if notification arrived.. check against lastnoticeid.. 
	if id is greater than lastnoticeid then add/update this to db 
	and update for isread flag
	and update dashboard unread notification count and dashboard news display

	
 */
(function () {
  'use strict';

  angular
    .module('school')
    .factory('noticeService', noticeService);

	noticeService.$inject = ['$http', '$q', 'userserviceapiurl', '$filter'];
	
	function noticeService($http, $q,userserviceapiurl,$filter) {	

		
		return {
			getUserGroups : getUserGroups
			
		}


		
		function getUserGroups() {
		
			var url = userserviceapiurl + "user/groups";
			var defer = $q.defer();
			$http.get(url)
			.success(function (response) {
				////alert(response);
				defer.resolve(response);
			})
			.error(function (err) {
				defer.reject(err);
			})
			return defer.promise;
		}

		
	}
})();
