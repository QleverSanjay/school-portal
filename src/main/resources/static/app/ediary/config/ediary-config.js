(function(){
	angular.module('ediary').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('ediary', {
			url : '/ediary',
			abstract :true,
			controller : 'EdiaryModelController',
			data: {
				 breadcrumbProxy: 'ediary.list'
	        }
		}).state('ediary.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/ediary/html/ediary-list.html',
					controller : 'EdiaryController'
				}
			},
			data: {
	            displayName: 'E-Diary List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('ediary.view', {
			url : '/view',
			views : {
				'@' : {
					templateUrl : 'app/ediary/html/ediary-view.html',
					controller : 'EdiaryViewController'
				}
			},
			data: {
	            displayName: 'View E-Diary'
	        },
	        params : {
	        	ediary : {}
	        }
	    }).state('ediary.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/ediary/html/ediary-add.html',
					controller : 'EdiaryAddController'
				}
			},
			data: {
	            displayName: 'Add E-Diary'
	        }
		});
	}]);
})();