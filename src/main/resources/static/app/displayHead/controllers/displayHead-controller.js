(function(){
	angular.module('displayHead').controller('DisplayHeadController',function($rootScope,$resource, $localStorage, 
			$filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.displayHeadList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isSingleBranch = false;	
		$scope.branchName = "";

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('displayHeads?branchId='+$scope.branchId).query().$promise.then(function(displayHeadList) {
					$scope.displayHeadList.length = 0;
					if(displayHeadList && displayHeadList.length > 0){
						angular.extend($scope.displayHeadList,displayHeadList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
				});	
			}
		};

	     $scope.deleteData = function deleteDisplayHead(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('displayHead?displayHeadId='+id).remove().$promise.then(function(resp) {
	        		for(var i =0; i < $scope.displayHeadList.length ;i ++ ){
      			  		var displayHead = $scope.displayHeadList[i];
      			  		if(displayHead.id == id){
      			  			$scope.displayHeadList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});
		
	/*angular.module('displayHead').controller('validateDisplayHeadForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,35}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});
	*/
	angular.module('displayHead').controller('DisplayHeadAddController',function($rootScope,$filter, 
			$localStorage, $scope,$state,$stateParams,$resource,CommonServices) {
		$scope.displayHead = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.displayHeadHeaderValue = "Add Display Head";

		$scope.displayHead.instituteId = $localStorage.selectedInstitute;
		$scope.displayHead.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.displayHead.instituteId){
	    			changeBranchList(false);
	    		}
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.displayHead.instituteId || $scope.displayHead.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.displayHead.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);	
				});	
			}
		};
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('displayHead.list');
		};

		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var displayHead = {
				 "branchId" : $scope.displayHead.branchId,
				 "name": $scope.displayHead.name,
				 "active" : $scope.displayHead.active,
				 "description" : $scope.displayHead.description
			}

			$resource('displayHead').save(displayHead).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('displayHead.list',{
							 status : resp.status,
							 message: resp.message
					},{reload : true});
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});
		}
	});

	/*This Controller is Used for DisplayHead Edit functionality*/
	angular.module('displayHead').controller('DisplayHeadEditController',function($rootScope,$filter,$scope,$state,$stateParams,$resource, CommonServices) {
		
		$scope.displayHead = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.displayHeadHeaderValue = "Edit Display Head";

		$resource('displayHead?displayHeadId='+$stateParams.id).get().$promise.then(function(displayHeadToEdit) {
			angular.extend($scope.displayHead,displayHeadToEdit);
			$rootScope.setSelectedData($scope.displayHead.instituteId,$scope.displayHead.branchId);
			changeBranchList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.displayHead.instituteId || $scope.displayHead.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.displayHead.instituteId})
					.$promise.then(function(branchList) {
						/*if(branchList != null && branchList.length > 0){
							$scope.displayHead.branchId = branchList[0].id;
						}*/
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.cancel = function(){
			$state.transitionTo('displayHead.list',{reload : true});
		};

		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var displayHead = {
				 "id" : $scope.displayHead.id,
				 "branchId" : $scope.displayHead.branchId,
				 "name": $scope.displayHead.name,
				 "active" : $scope.displayHead.active,
				 "description" : $scope.displayHead.description
			}

			$resource('displayHead').save(displayHead).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('displayHead.list',{
							 status : resp.status,
							 message: resp.message
					},{reload : true});
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});
		}
	});
})();

