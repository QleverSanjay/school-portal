(function(){
	angular.module('displayHead').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('displayHead', {
			url : '/displayHead',
			abstract :true,
			controller : 'DisplayHeadModelController',
			data: {
				 breadcrumbProxy: 'displayHead.list'
			}
		}).state('displayHead.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/displayHead/html/displayHead-list.html',
					controller : 'DisplayHeadController'
				}
			},
			data: {
	            displayName: 'Display Head List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : ''
	        }
		}).state('displayHead.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/displayHead/html/displayHead-add.html',
					controller : 'DisplayHeadAddController'
				}
			},
			data: {
	            displayName: 'Add Display Head'
	        },
	        params : {
	        	instituteId : ''
	        }
		}).state('displayHead.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/displayHead/html/displayHead-add.html',
					controller : 'DisplayHeadEditController'
				}
			},
			data: {
	            displayName: 'Edit Display Head'
	        }
		});
	}]);
})();