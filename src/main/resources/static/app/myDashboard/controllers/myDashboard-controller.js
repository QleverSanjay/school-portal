(function() {
	angular.module('myDashboard').controller('myDashboardController',

	function($resource,$scope,$state,$rootScope,DTOptionsBuilder) {
	
	$scope.instituteList = [];
	$scope.branchList = [];
	$scope.pieLabels  = [];
	$scope.pieData = [];
	$scope.lineLabels = [];
	$scope.lineSeries = [];
	$scope.lineData = [];
	$scope.feesStatusTableData = [];
	var instituteID;
	$scope.branchId = 1;
	$scope.acadmicStartDate;
	$scope.acadmicEndDate;
	$scope.minDate = new Date();
	$scope.isOpenStartDate = false;
	$scope.startDate = new Date();
	$scope.endDate = new Date();
	var formatStartDate = moment($scope.startDate).format('YYYY-MM-DD');
	$scope.unixStartDate = moment(formatStartDate).unix();
	var formatEndDate = moment($scope.endDate).format('YYYY-MM-DD');
	$scope.unixEndDate = moment(formatEndDate).unix();
	$scope.isOpenEndDate = false;
	$scope.isDisabled = 1;
	$scope.currentDate = moment().format('YYYY-MM-DD');
	
	$scope.feesUnpaid = 0;
    $scope.feesPaid = 0;
    $scope.feesPaidWithLate = 0;
	$scope.projectedCollection = 0;
	// Get Institute List
	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
		if(instituteList && $scope.instituteId){
			$scope.changeBranchList($scope.instituteId);	
		}
		$scope.instituteId  = instituteList[0].id;
		angular.extend($scope.instituteList,instituteList);
		getBranchList($scope.instituteId);
	},function(err){
		console.log("error while on instititue level..... ")
	});
   
	// Get Branch List
   	function getBranchList(instituteId){
   	 	$resource('masters/getFilterdBranch/:id').query({id : instituteId})
		.$promise.then(function(branchList) {
			angular.extend($scope.branchList,branchList);
			$scope.branchId  = branchList[0].id;
			$scope.getFeesStatus($scope.branchId);
			$scope.totalCollectedFees($scope.branchId,1)
		},function(err){
			console.log("error while on get filter branch level..... ")
		});
   	}
  
   	
   	$scope.changeBranchList= function(institute_id){
   		$scope.branchList.length = 0;
   		if(institute_id){
   			$resource('masters/getFilterdBranch/:id').query({id : institute_id})
			.$promise.then(function(branchList) {
				angular.extend($scope.branchList,branchList);
				$scope.totalCollectedFees($scope.branchId,1);
		   		$scope.getFeesStatus($scope.branchId)
			},function(err){
				console.log("error while on get filter branch level..... ")
			});	
   		}
   	};
   	
   	
   	// Get chart based on Start and end Date
   	$scope.save = function(){
   		var formatStartDate = moment($scope.startDate).format('YYYY-MM-DD');
   		$scope.unixStartDate = moment(formatStartDate).unix();
   		var formatEndDate = moment($scope.endDate).format('YYYY-MM-DD');
   		$scope.unixEndDate = moment(formatEndDate).unix();
   		$scope.totalCollectedFees($scope.branchId,1);
   		$scope.getFeesStatus($scope.branchId)
   	}
   	
   	$scope.showList = function(){
   		$scope.totalCollectedFees($scope.branchId,1);
   		$scope.getFeesStatus($scope.branchId)
   	}
   	
   	var formatDate = function(date){
   		return moment(date).format('DD MMMM YYYY');
   	}

   	// Get Pie Chart for Fees Paid
   	$scope.getFeesStatus = function(branchId){
   		$scope.branchId = branchId;
   		if($scope.branchId){
   			//$resource('dashboard/feeStatus/'+$scope.branchId+'?startDate='+$scope.unixStartDate+'&endDate='+$scope.unixEndDate).query().$promise.then(
   			$resource('dashboard/feeStatus/'+$scope.branchId).query().$promise.then(
   				function(res){
					$scope.feesStatusData = res;
					if(res && res.length > 0){
						console.log(res);
						$scope.feesStatusData = res;
						$scope.getFeesStatusTableData($scope.branchId); 
						$scope.pieLabels = [];
						$scope.pieData = [];
						$scope.displayData = {};
						$scope.projectedCollection = 0;
						angular.forEach($scope.feesStatusData, function(value,key){
							
							var amount = value.amount;
                            var type = value.feeType
                            $scope.projectedCollection = $scope.projectedCollection + amount;  
                            if(type == 'Fees_Unpaid'){
                                $scope.feesUnpaid = amount.toLocaleString('en-IN');
                            }else if(type == 'Fees_Paid'){
                                $scope.feesPaid = amount.toLocaleString('en-IN');
                            }else if(type == 'Paid_With_Late_Fees'){                                
                                $scope.feesPaidWithLate = amount.toLocaleString('en-IN');
                            }
                            
                            							
							var labels =(value.feeType).replace(/_/g, ' ');
								$scope.pieLabels.push(labels); 
								$scope.pieData.push(value.amount);
							    //$scope.pieLabels.push(labels); 
						})
						$scope.projectedCollection = $scope.projectedCollection.toLocaleString('en-IN')
						$scope.pieOption = { 
							legend: {
	                        display: true,
	                        position: 'bottom',
	                        fontSize: 11
							}
						}
						$scope.piedatasetOverride = {
					        backgroundColor: [
					          "rgba(0,0,255, 0.75)", // blue
					          "rgba(0,255,0, 0.75)", // green
					          "rgba(255,0,0, 0.75)"
					        ],
					        hoverBackgroundColor: [
					          "rgba(0,0,255, 1.5)",
					          "rgba(0,255,0, 1.5)",
					          "rgba(255,0,0, 1.5)"
					        ] 
					    }
					}  else if(res.length == 0) {
							$scope.noDataInFeesStatus = "No Data Avalaible"
					}
				},
				function(err){
				console.log(err);
   			})
   		}
   	}
   	
   	// DataTable For Pie Chart
   	$scope.getFeesStatusTableData = function(branchId){
   		$scope.branchId = branchId;
   		if($scope.branchId){
   			$resource('dashboard/feeHeadTable/'+$scope.branchId).query().$promise.then(
   				function(res){
					if(res && res.length > 0){
						console.log(res);
						angular.extend($scope.feesStatusTableData , res);
					}  else if(res.length == 0) {
							$scope.nofeesStatusTableData = "No Data Avalaible"
					}
				},
				function(err){
				console.log(err);
   			})
   		}
   	}
  	$scope.dtOptions =  DTOptionsBuilder.newOptions()
	.withOption("bAutoWidth", true)
	.withOption("scrollX", true)
    .withOption("responsive", true);

   	// Get Line chart for Total Collected Fees
   	$scope.totalCollectedFees = function(branchId,days){
		$scope.branchId = branchId;
   		if($scope.branchId){
   			$resource('dashboard/payments/'+$scope.branchId +'?days='+days).query().$promise.then(
				function(res){
					console.log(res);
					$scope.collectFeesData = res;
					if(res && res.length > 0){
						console.log(res);
						if(days == 1)
				   			$scope.graphMessage = "Transaction From  "+moment($scope.currentDate).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");
				   		$scope.collectFeesData = res;
						$scope.lineLabels = [];
						$scope.lineSeries = [];
						$scope.lineData = [];
						var temptransactionCount = [];
					    var temptransactionSum = [];
						angular.forEach($scope.collectFeesData, function(value,key){
							$scope.lineLabels.push(formatDate(value.transactionDate));
							$scope.lineSeries = ["Transaction Sum", "Total Count"]
							temptransactionSum.push(value.transactionSum)
							temptransactionCount.push(value.transactionCount);
						})
						$scope.lineData.push(temptransactionSum);
						$scope.lineData.push(temptransactionCount);
						$scope.lineOption = {
							responsive: true,
							elements: { point: {radius: .75 } },
							legend: {
		                        display: true,
		                        position: 'bottom',
		                        fontSize: 11
		                    },
		                    scales: {
		                        xAxes: [{ ticks: {maxTicksLimit:3} }],
		                        yAxes: [{ ticks: { maxTicksLimit:4 } }],
		                    }
						};
					    $scope.lineColors = [ {
			  					backgroundColor: "rgba(0,255,0, 0.5)",
			                	backgroundColor: "rgba(0,0,255, 0.5)"
						    }
					    ];
					    /* $scope.onClick = function (points, evt) {
					    	console.log(points, evt);
					   };*/
					    
					} else if(res.length == 0) {
						if(days == 1)
				   			$scope.graphMessage = "Transaction From  "+moment($scope.currentDate).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");
				   			$scope.noDataInCollectedFees = "No Data Avalaible"
							 
					}
				},
				function(err){
				console.log(err);
   			})
   		}
	}
   	
   	// Get total collected fees based on Selected Date Range
   	$scope.selectDays = function(days){
   		 
   		if(days == 1)
   			$scope.graphMessage = "Transaction From  "+moment($scope.currentDate).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");
   		if(days == 7)
   			$scope.graphMessage = "Transaction From  "+moment($scope.getDate(7)).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");
   		if(days == 30)
   			$scope.graphMessage = "Transaction From  "+moment($scope.getDate(30)).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");
   		if(days == 90)
   			$scope.graphMessage = "Transaction From  "+moment($scope.getDate(90)).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");
   		if(days == 180)
   			$scope.graphMessage = "Transaction From  "+moment($scope.getDate(180)).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");
   		if(days == 365)
   			$scope.graphMessage = "Transaction From  "+moment($scope.getDate(365)).format("DD MMMM YYYY") +" To "+ moment($scope.currentDate).format("DD MMMM YYYY");	
   		$scope.totalCollectedFees($scope.branchId,days)
   	}
   	$scope.getDate = function(days){
   		if(days == 7)
   			return  moment().subtract(7,'d').format('YYYY-MM-DD');
   		if(days == 30)
   			return moment().subtract(30,'d').format('YYYY-MM-DD');
   		if(days == 90)
   			return moment().subtract(90,'d').format('YYYY-MM-DD');
   		if(days == 180)
   			return moment().subtract(180,'d').format('YYYY-MM-DD');
   		if(days == 365)
   			return moment().subtract(365,'d').format('YYYY-MM-DD');
   	}
 
});

})();