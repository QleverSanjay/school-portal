(function(){
	angular.module('myDashboard').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('myDashboard', {
			url : '',
			abstract :true,
			controller : 'myDashboardController',
		}).state('dashboard.page', {
			url : '/',
			views : {
				'@' : {
					templateUrl : 'app/myDashboard/html/myDashboard.html',
					controller : 'myDashboardController'
				}
			},
			data: {
	            displayName: 'About Us'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	BoardList : function($resource){
		        	return $resource('masters/boards').query().$promise;
		        }
	        }
		});
	}]);
})();