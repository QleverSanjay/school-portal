(function(){
	angular.module('payment-settings').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('payment-settings', {
			url : '/payment-settings',
			abstract :true,
			controller : 'PaymentSettingsModelController',
			data: {
				 breadcrumbProxy: 'payment-settings.list'
	        }
		}).state('payment-settings.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/payment-settings/html/payment-settings-list.html',
					controller : 'PaymentSettingsController'
				}
			},
			data: {
	            displayName: 'Manage Payment Settings'
	        },
	        params : {
	        	instituteId : '',
	        	message : '',
	        	status : ''
	        },
	        resolve : {
	        	InstituteList : function($resource){
	        		return $resource('masters/instituteMasters').query().$promise;
	        	}
	        }
		}).state('payment-settings.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/payment-settings/html/payment-settings-add.html',
					controller : 'PaymentSettingsAddController'
				}
			},
			data: {
	            displayName: 'Add Payment Settings'
	        },
	        resolve : {
	        	InstituteList : function($resource){
	        		return $resource('masters/instituteMasters').query().$promise;
	        	}
	        }
		}).state('payment-settings.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/payment-settings/html/payment-settings-add.html',
					controller : 'PaymentSettingsEditController'
				}
			},
			data: {
	            displayName: 'Edit Payment Settings'
	        },
	        resolve : {
	        	InstituteList : function($resource){
	        		return $resource('masters/instituteMasters').query().$promise;
	        	}
	        }
		});
	}]);
})();