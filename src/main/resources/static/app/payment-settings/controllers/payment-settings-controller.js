(function(){
	angular.module('payment-settings').controller('PaymentSettingsController',function($rootScope, $resource,$filter,$confirm,DTOptionsBuilder,InstituteList,$stateParams,$scope,$state,$uibModal,CommonServices) {

		$scope.paymentSettingList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	    $scope.instituteId = $stateParams.instituteId;
	    
	    $scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

	    if($scope.instituteId){
	    	filterTableByBranch();
	    }
	    $scope.instituteList = InstituteList;

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch();
		};

		function filterTableByBranch(){
			if($scope.instituteId){
				$resource('paymentSetting?instituteId='+$scope.instituteId).query().$promise.then(function(paymentSettingList) {
					$scope.paymentSettingList.length = 0;
					if(paymentSettingList && paymentSettingList.length > 0){
						angular.extend($scope.paymentSettingList,paymentSettingList);
					}					
	    		});
			}
		}

		$scope.deleteData = function deletePaymentSettings(id){
		    $confirm({text: 'Are you sure you want to delete?'}).then(function() {
        	 	$resource('paymentSetting').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.paymentSettingList.length ;i ++ ){
      			  		var paymentSettings = $scope.paymentSettingList[i];
      			  		if(paymentSettings.id == id){
      			  			$scope.paymentSettingList.splice(i , 1);
      			  			break;
      			  		}
      			  	}
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
		    });
		}; 
	});

	angular.module('payment-settings').controller('DefaultPaymentSettingsController',function($resource,$scope,$timeout,$state) {
		$scope.regExAmount = /^[0-9]{2,45}$/;
		$scope.regExPercentage = /^(100(\.0{1,2})?|([0-9]?[0-9](\.[0-9]{1,2})))$/;
		
		$scope.defaultPaymentSettingButtonVal = "Save";
		
		$scope.paymentSettings = {};
		
		$resource('paymentSetting/getDefaultPaymentSetting').get().$promise.then(function(paymentSetting) {

			if(paymentSetting){
				$scope.defaultPaymentSettingButtonVal = "Change";
				angular.extend($scope.paymentSettings, paymentSetting);
			}					
		});

		$scope.save = function() {
			var paymentSettings = {
				id : $scope.paymentSettings.id,
				amount : $scope.paymentSettings.amount,
				percentage : $scope.paymentSettings.percentage,
				instituteId : $scope.paymentSettings.instituteId,
				gatewayId : $scope.paymentSettings.gatewayId
			};


			//CommonServices.savePaymentSettings('paymentSetting',paymentSettings, 'paymentSettings.list');
			$resource('paymentSetting/setDefaultPaymentSetting').save(paymentSettings).$promise.then(function(resp) {

    			$scope.status = resp.status;
        		$scope.defaultSettingMessage = resp.message;

				$timeout(function(){
					$scope.defaultSettingMessage = ""
					}, 3000
				);
			});
		};
	});


	angular.module('payment-settings').controller('validatePaymentSettingsForm',function($resource,$scope,$state) {
		$scope.regExAmount = /^[0-9]{2,45}$/;
		$scope.regExPercentage = /^(100(\.0{1,2})?|([0-9]?[0-9](\.[0-9]{1,2})))$/;
	});

	angular.module('payment-settings').controller('PaymentSettingsAddController',function($rootScope,$scope,$filter,$state,InstituteList, $stateParams,$resource,CommonServices) {
		$scope.paymentSettings = {}; 

		$scope.instituteList = InstituteList;
		
		$scope.cancelSave = function (){
			$state.transitionTo('payment-settings.list',{reload : true});
		};

		$scope.save = function() {
			var paymentSettings = {
				id : $scope.paymentSettings.id,
				amount : $scope.paymentSettings.amount,
				percentage : $scope.paymentSettings.percentage,
				instituteId : $scope.paymentSettings.instituteId,
				gatewayId : $scope.paymentSettings.gatewayId
			};


			//CommonServices.savePaymentSettings('paymentSetting',paymentSettings, 'paymentSettings.list');
			$resource('paymentSetting').save(paymentSettings).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('payment-settings.list',
						{
							instituteId : $scope.paymentSettings.instituteId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});

	/*This Controller is Used for PaymentSettings Edit functionality*/
	angular.module('payment-settings').controller('PaymentSettingsEditController',function($rootScope,$scope,$filter,InstituteList, $state, $stateParams,$resource, CommonServices) {

		$scope.paymentSettings = {};
		$scope.instituteList = InstituteList;

		$resource('paymentSetting/:id').get({id : $stateParams.id}).$promise.then(function(paymentSettingsToEdit) {
			$scope.paymentSettings = paymentSettingsToEdit;
		});

    	$scope.cancelSave = function (){
			$state.transitionTo('payment-settings.list',{reload : true});
		};

		$scope.save = function() {
			var paymentSettings = {
				id : $scope.paymentSettings.id,
				amount : $scope.paymentSettings.amount,
				percentage : $scope.paymentSettings.percentage,
				instituteId : $scope.paymentSettings.instituteId,
				gatewayId : $scope.paymentSettings.gatewayId
			};

			$resource('paymentSetting').save(paymentSettings).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('payment-settings.list',
					{
						instituteId : $scope.paymentSettings.instituteId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
					}
				else{
		        		$scope.status = resp.status;
		        		$scope.message = resp.message;
		        	}
				});
			};
		});
})();

