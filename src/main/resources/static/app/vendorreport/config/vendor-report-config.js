(function(){
	angular.module('vendorreport').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('vendorreport', {
			url : '/vendorreport',
			abstract :true,
			controller : 'VendorModelController',
			data: {
				 breadcrumbProxy: 'vendorreport.list'
	        }
		}).state('vendorreport.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/vendorreport/html/vendor-report-list.html',
					controller : 'VendorReportController'
				}
			},
			data: {
	            displayName: 'Vendor Report'
	        },
		});
	}]);
})();