(function(){
	angular.module('vendorreport').controller('VendorReportController',function($rootScope,$resource,
			$filter, $confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {
	    $scope.vendorReportList = [];
	    $scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	 //   angular.extend($scope.vendorreport,VendorList);
	    $scope.maxDate = new Date();

	    $scope.isOpenEndDate = false;
	    $scope.isOpenStartDate = false;

	    $scope.instituteList = [];
	    $scope.branchList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);
	    
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};
		
		function filterTableByBranch(branchId){
			if(branchId){
				var fromDate = $filter('date')($scope.fromDate, "MM/dd/yyyy");
				var toDate = $filter('date')($scope.toDate, "MM/dd/yyyy");

				$resource('vendorreport/getFilteredReport?branchId='+$scope.branchId+
						'&fromDate='+fromDate+'&toDate='+toDate).get().$promise.then(function(report) {
					$scope.vendorReportList.length = 0;
					if(report && report.itemList && report.itemList.length > 0){
						angular.extend($scope.vendorReportList,report.itemList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				$scope.branchId = (!$stateParams.branchId ? branchList[0].id : $stateParams.branchId);
        				if($rootScope.roleId == 6 && branchList.length == 1){
        					$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;
        				}
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $stateParams.instituteId){
	    			$scope.instituteId = $stateParams.instituteId;
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.branchId = (isBranchChanged ? branchList[0].id  : 
								(!$stateParams.branchId ? branchList[0].id : $stateParams.branchId));
	        				if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};   
	});
})();