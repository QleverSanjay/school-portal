(function(){
	angular.module('student').config(['$stateProvider', function($stateProvider) {
		$stateProvider.state('student', {
			url : '/student',
			abstract :true,
			controller : 'StudentModelController',
			data: {
				 breadcrumbProxy: 'student.list'
	        }
		}).state('student.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/students/html/student-list.html',
					controller : 'StudentController'
				}
			},
			data: {
	            displayName: 'Student List'
	        },
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        	message : '',
	        	status : ''
	        }
		}).state('student.report', {
			url : '/report',
			views : {
				'@' : {
					templateUrl : 'app/students/html/student-report.html',
					controller : 'StudentReportController'
				}
			},
			data: {
	            displayName: 'Student Report'
	        },
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        }
		}).state('student.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/students/html/student-add.html',
					controller : 'StudentAddController'
				}
			},
			data: {
	            displayName: 'Add Student'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	IncomeRangeList : function($resource){
	        		return $resource('masters/incomeRangeMasters').query().$promise;
	        	},
	        	ProfessionList : function($resource){
	        		return $resource('masters/professionMasters').query().$promise;
	        	},
	        	ReligionList : function($resource){
	        		return $resource('masters/religionMasters').query().$promise;
	        	},
	        	CasteList : function($resource){
	        		return $resource('masters/casteMasters').query().$promise;
	        	}
	        }
		}).state('student.view', {
			url : '/view/:id',
			views : {
				'@' : {
					templateUrl : 'app/students/html/student-add.html',
					controller : 'StudentViewController'
				}
			},
			data: {
	            displayName: 'View Student'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	ReligionList : function($resource){
	        		return $resource('masters/religionMasters').query().$promise;
	        	},
	        	CasteList : function($resource){
	        		return $resource('masters/casteMasters').query().$promise;
	        	}
	        }
		}).state('student.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/students/html/student-add.html',
					controller : 'StudentEditController'
				}
			},
			data: {
	            displayName: 'Edit Student'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	IncomeRangeList : function($resource){
	        		return $resource('masters/incomeRangeMasters').query().$promise;
	        	},
	        	ProfessionList : function($resource){
	        		return $resource('masters/professionMasters').query().$promise;
	        	},
	        	ReligionList : function($resource){
	        		return $resource('masters/religionMasters').query().$promise;
	        	},
	        	CasteList : function($resource){
	        		return $resource('masters/casteMasters').query().$promise;
	        	}
	        }
		});
	}]);
})();