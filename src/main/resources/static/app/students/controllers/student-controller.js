(function(){
	angular.module('student').controller('StudentController',function($rootScope,$window, $compile, DTColumnBuilder, $http, $resource,$scope,$stateParams, $localStorage, $confirm,DTOptionsBuilder,$state, modalPopupService,CommonServices) {
	    $scope.students = [];
	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.standardList = [];
		$scope.divisionList = [];
		$scope.status = $stateParams.status;
		$scope.message = $stateParams.message;
		$scope.isUploadingFailed = false;
		$scope.isFirstTimeLoaded = true;

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;
		$scope.standardId = $stateParams.standardId ? $stateParams.standardId : $localStorage.selectedStandard ;
		$scope.divisionId = $stateParams.divisionId ? $stateParams.divisionId : $localStorage.selectedDivision ;

		$scope.dtColumns = [
            DTColumnBuilder.newColumn("firstname", "First Name").withOption('name', 'firstname'),
            DTColumnBuilder.newColumn("surname", "Surname").withOption('name', 'surname'),
            DTColumnBuilder.newColumn("dateOfBirth", "Date Of Birth").withOption('name', 'dateOfBirth'),
            DTColumnBuilder.newColumn(null).withTitle('Gender').notSortable()
            .renderWith(function(report, type, full, meta) {
				if(report.gender != 'M'){
					report.gender = 'Female';
				}else {
					report.gender = 'Male';
				}
                return report.gender;
            }),
            DTColumnBuilder.newColumn("mobile", "Mobile No").withOption('name', 'mobile'),
            DTColumnBuilder.newColumn("emailAddress", "Email").withOption('name', 'emailAddress'),
            DTColumnBuilder.newColumn("standardName", "Standard/Course").withOption('name', 'standardName'),
            DTColumnBuilder.newColumn("divisionName", "Division").withOption('name', 'divisionName'),
            DTColumnBuilder.newColumn("feesCodeName", "Fees Code").withOption('name', 'feesCodeName'),
            DTColumnBuilder.newColumn("rollNumber", "Roll Number").withOption('name', 'rollNumber'),
            DTColumnBuilder.newColumn("registrationCode", "Registration Code").withOption('name', 'registrationCode'),
            DTColumnBuilder.newColumn("addressLine1", "Address").withOption('name', 'addressLine1'),
            DTColumnBuilder.newColumn("area", "Area").withOption('name', 'area'),
            DTColumnBuilder.newColumn("city", "Town/Village").withOption('name', 'city'),
            DTColumnBuilder.newColumn("pincode", "Pin Code").withOption('name', 'pincode'),
            DTColumnBuilder.newColumn("religion", "Religion").withOption('name', 'religion'),
            DTColumnBuilder.newColumn("caste", "Caste").withOption('name', 'caste'),
            DTColumnBuilder.newColumn("state", "State").withOption('name', 'state'),
            DTColumnBuilder.newColumn("district", "District").withOption('name', 'district'),
            DTColumnBuilder.newColumn("taluka", "Taluka").withOption('name', 'taluka'),
            DTColumnBuilder.newColumn(null).withTitle('Active').notSortable()
            .renderWith(function(report, type, full, meta) {
				if(report.active != 'N'){
					report.active = 'Yes';
				}else {
					report.active = 'No';
				}
                return report.active;
            }),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable()
            .renderWith(function(student, type, full, meta) {
            	var action = '';
            	if($rootScope.roleId == 4){
					action += '<a href = "#" class="btn btn-xs btn-default" title="Delete" ui-sref="student.view({id :'+student.id+' })" ><i class="fa fa-eye"></i></a>';
				}
            	else if($rootScope.roleId != 8 ){
            		action += '<a href = "#" class="btn btn-xs btn-default" title="Edit" ui-sref="student.edit({id :'+student.id+' })"><i class="fa fa-pencil"></i></a> ';
            		action += '<a href = "#" class="btn btn-xs btn-default" title="Delete" ui-sref="student.list" ng-click="deleteData('+student.id+')"><i class="fa fa-trash"></i></a>';
            	}
                return action;
            })
        ];

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true)
        .withOption("scrollX", true)
        .withOption("deferLoading", false)
        .withOption('createdRow', function(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        /*.withOption('headerCallback', function(header) {
            if (!$scope.headerCompiled) {
            	$scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })*/
        .withOption('ajax', {
	         url: 'students/report',
	         type: 'POST',
	         contentType: 'application/json',

	         data: function(data, dtInstance) {
	        	 var filter = getFilterObject();
	        	 data.columns.length = 0;
	        	 filter.tableOptions = data;
	        	 return JSON.stringify(filter);
	         },
	         error:function (xhr, error, thrown) {
	        	 $scope.message = thrown;
	        	 $scope.$apply();
	        	 var processingDialog = document.getElementById("feesReport_processing");
	        	 processingDialog.style.visibility='hidden' ;
	        	 if(xhr.responseText.indexOf("Login Page") != -1){
	        		 $state.transitionTo('login.logout',{},{reload : true});
	        	 }
	         }
	     })
	    .withDataProp('students')
	    .withOption('processing', true) 
        .withOption('serverSide', true) 
        .withOption('ordering', false) 
        .withPaginationType('full_numbers') 
        .withDisplayLength(10);
//        .withOption('aaSorting',[3,'asc']);

		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		filteStandardsByTeacher($scope.branchId);
		}

		function filteStandardsByTeacher(branchId){
			if($rootScope.id){
				$resource('masters/getFilterdStandardsByTeacher?branchId='+branchId+'&teacherId='+$rootScope.id).query().$promise.then(function(standardList) {
					$scope.standardList.length = 0;
					if(standardList && standardList.length > 0){
						angular.extend($scope.standardList,standardList);
						changeDivisionList();
					}
	    		});
			}
		}

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isUploadingFailed = false;
    		$rootScope.setSelectedData($scope.instituteId, $scope.branchId, $scope.standardId, $scope.divisionId);
    		filterTableByBranch($scope.branchId);
		};

		$scope.changeStandardList = function(){
			$scope.isFirstTimeLoaded = false;
			$scope.status = "";
    		$scope.message = "";
			changeStandardList();
		};

		$scope.changeDivisionList = function(){
			$scope.isFirstTimeLoaded = false;
			$scope.status = "";
    		$scope.message = "";
			changeDivisionList();
		};
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;

					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
				if($scope.isFirstTimeLoaded && $scope.divisionId){
					filterTableByBranch($scope.branchId);
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}

		function getFilterObject(){
			var filter = {};
			if($rootScope.roleId == 4){
				filter = {'instituteId' : $scope.instituteId,
		        	'branchId' : $scope.branchId,
		        	'standardId' : $scope.standardId,
		        	'divisionId' : $scope.divisionId,
		        	'teacherUserId' : $rootScope.id
			    };
			}else {
				filter = {'instituteId' : $scope.instituteId,
		        	'branchId' : $scope.branchId,
		        	'standardId' : $scope.standardId,
		        	'divisionId' : $scope.divisionId,
			    };	
			}
			return filter;
		}


		function filterTableByBranch(branchId){
			if(branchId){
				var table = angular.element( document.querySelector('#students')).DataTable();

				table.ajax.reload( function ( json ) {
				} );
			}
			/*$scope.students.length = 0;
			if(branchId != null ){
				$http({
					url: 'students/report',
			        method: "POST",
			        data: getFilterObject()
			    })
			    .then(function(response) {
			        console.log(response);
			        $scope.students = response.data;
			    },
			    function(err) { // optional
			    	console.log(err);
			    });
			}*/
		}

		$scope.sendNotifications = function (){
			var url = "students/sendNotifications?branchId="+$scope.branchId +
			($scope.standardId ? "&standardId="+$scope.standardId : "")
			+($scope.divisionId ? "&divisionId="+ $scope.divisionId : "");
			
			$resource(url).get().$promise.then(function(branchList) {
				$scope.message = "Notifications sent to all student and parents.";
				$scope.status = "success";
			});
		};


		// Download excel records according to filters.
		$scope.exportToExcel = function (){
			var url = "students/downloadReport?instituteId="+ ($scope.instituteId  ? $scope.instituteId : "0") +
				($scope.branchId ? "&branchId="+$scope.branchId : "")+
				($scope.standardId ? "&standardId="+$scope.standardId : "")
				+($scope.divisionId ? "&divisionId="+ $scope.divisionId : "")
				+($rootScope.roleId == 4 ? "&teacherUserId="+$rootScope.id : "");

			$window.location.href = url;
		};

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
    		$scope.instituteId = ($scope.instituteId ? $scope.instituteId : $rootScope.instituteId);
    		changeBranchList();
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			changeBranchList();
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isFirstTimeLoaded = false;
			changeBranchList();
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						changeStandardList();
						if($scope.isFirstTimeLoaded && !$scope.divisionId){
							filterTableByBranch($scope.branchId);
						}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

	    $scope.uploadStudents = function(){
	    	$scope.isUploadUpdate = false;
	    	var options = {
	    	      templateUrl: 'app/students/html/student-bulk-upload.html',
	    	      controller: 'StudentFileUploadController',
	    	      scope : $scope
	    	    };
	    	modalPopupService.openPopup(options);
	     };
	    
	    $scope.batchUploadStudents = function(){
	    	$scope.isUploadUpdate = false;
	    	var options = {
	    	      templateUrl: 'app/students/html/student-bulk-upload.html',
	    	      controller: 'StudentFileBatchUploadController',
	    	      scope : $scope
	    	    };
	    	modalPopupService.openPopup(options);
	     };

	     $scope.checkUploadStatus = function(){
	    	$scope.isUploadStatus = false;
	    	var options = {
	    		templateUrl: 'app/students/html/upload-status.html',
    	      	controller: 'StudentUploadStatusController',
    	      	scope : $scope
    	    };

	    	$rootScope.$emit("CallstatusUpload", {});
	    	modalPopupService.openPopup(options);
	     };

	     $scope.uploadUpdateStudents = function(){
	    	 $scope.isUploadUpdate = true;
	    	 var options = {
	    			 templateUrl: 'app/students/html/student-bulk-upload.html',
	    			 controller: 'StudentFileUploadController',
	    			 scope : $scope
	    	 };
	    	 modalPopupService.openPopup(options);
	     };

	     /** This method is used to delete student from the screen. */
	     $scope.deleteData = function deletestudent(id){
	    	 $confirm({text: 'Are you sure you want to delete?'}).then(function() {
        	 	$resource('students').remove({id : id}).$promise.then(function(resp) {
	        		if(resp.status == 'success'){
	        			for(var i =0; i < $scope.students.length ;i ++ ){
	      			  		var student = $scope.students[i];
	      			  		if(student.id == id){
	      			  			$scope.students.splice(i , 1);
	      			  			break;
	      			  		}
	      			  	}	
	        		}
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     };
	});

	angular.module('student').controller('validateStudentForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z]{2,45}$/;
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,45}$/;
		$scope.regExCity = /^[A-Z a-z]{2,45}$/;
		$scope.regExRollNo = /^[0-9]{1,45}$/;
		$scope.regExRegtCode = /^[A-Z a-z 0-9]{2,10}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.maxDate = new Date();
	});
	

	angular.module('student').controller('StudentAddController',function($rootScope,$scope,$state, $localStorage, $filter,
			modalPopupService , ReligionList,CasteList, IncomeRangeList, ProfessionList, Upload,$resource,CommonServices, StateList) {
		$scope.student = {}; 
		$scope.isOpen = false;

		$scope.religionList = ReligionList;
		$scope.casteList = CasteList;

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.standardList = [];
		$scope.divisionList = [];
		$scope.stateList = StateList;
		$scope.districtList = [];
		$scope.talukaList = [];		
		$scope.studentHeaderValue = "Add Student";
		$scope.student.studentParentList = [];
		$scope.feesCodes = [];

    	$scope.incomeRangeList = IncomeRangeList;
    	$scope.professionList = ProfessionList;

		$scope.student.instituteId = $localStorage.selectedInstitute;
		$scope.student.branchId = $localStorage.selectedBranch;
		$scope.student.standardId = $localStorage.selectedStandard ;
		$scope.student.divisionId = $localStorage.selectedDivision ;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			changeStandardList();
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if($scope.student.instituteId || $scope.student.instituteId == 0){
    				changeBranchList();	
    			}
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
		
		// Used when teacher login.
    	if($rootScope.roleId == 4){
    		$scope.student.branchId = $rootScope.branchId;
    		changeStandardList();
    	}

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.student.instituteId || $scope.student.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.student.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.student.branchId = ($scope.student.branchId  ? $scope.student.branchId  : branchList[0].id);
	        			}
						changeStandardList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		$scope.isBranchChanged = false;
		$scope.changeStandardList = function(){
			$scope.isBranchChanged = true;
			changeStandardList();
		};

		$scope.changeDivisionList = function(){
			changeDivisionList();
		};
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.student.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.student.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					if(standardList != null && standardList.length > 0){
						$scope.student.standardId = ($scope.student.standardId && !$scope.isBranchChanged) ? $scope.student.standardId 
							: standardList[0].id;
        			}

					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					getFeesCodes();
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}
		
		function getFeesCodes(){
			if($scope.student.branchId){
				$resource('fees/fees-code/list/:id').query({id : $scope.student.branchId}).$promise.then(
				 function(feesCodeList) {
					 $scope.feesCodes.length = 0;
					 angular.extend($scope.feesCodes, feesCodeList);
				});
			}
			else {
				$scope.feesCodes.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.student.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.student.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
							$scope.student.divisionId = ($scope.student.divisionId && !$scope.isBranchChanged) ? $scope.student.divisionId 
									:"";
						}
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		function changeDistrictList(){
			$scope.districtList.length = 0;
			if($scope.student.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.student.stateId})
					.$promise.then(function(districtList) {
						if(districtList != null && districtList.length > 0){
							$scope.student.districtId = ($scope.student.districtId  ? $scope.student.districtId  : districtList[0].id);
	        			}
						angular.extend($scope.districtList, districtList);
				});	
			}
		};
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		function changeTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.student.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.student.districtId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.student.talukaId = ($scope.student.talukaId  ? $scope.student.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};

		/** This method is used to save edited object. Here we explicitly copy $scope.students to normal student object. This is done because $scope.student contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			$scope.status = '';
			$scope.message = '';
			var student = {
					id: $scope.student.id,
					firstname: $scope.student.firstname,
					fatherName: $scope.student.fatherName,
					motherName: $scope.student.motherName,
					surname: $scope.student.surname,
					gender: $scope.student.gender,
					dateOfBirth: $filter('date')($scope.student.dateOfBirth, "yyyy-MM-dd"),
					relationship: $scope.student.relationship,
					studentUserId:$scope.student.studentUserId,
					emailAddress : $scope.student.emailAddress,
					mobile:$scope.student.mobile,
					addressLine1: $scope.student.addressLine1,
					area: $scope.student.area,
					city: $scope.student.city,
					pincode : $scope.student.pincode,
					religion : $scope.student.religion,
					casteId : $scope.student.casteId,
					standardId : $scope.student.standardId,
					standardName : $scope.student.standardName,
					divisionId : $scope.student.divisionId,
					feesCodeId : $scope.student.feesCodeId,
					rollNumber : $scope.student.rollNumber,
					registrationCode : $scope.student.registrationCode,
					branchId : $scope.student.branchId,
					active: $scope.student.active,
					isDelete: $scope.student.isDelete,
					fileChanegd : $scope.student.fileChanegd,
					stateId: $scope.student.stateId,
					talukaId: $scope.student.talukaId,
					districtId: $scope.student.districtId,
					createStudentLogin : $scope.student.createStudentLogin,
					studentParentList : $scope.student.studentParentList
			};

			/** This method is used to adding event. It will only call add utility method */
			Upload.upload({
	            url: 'students/save',
	            method: 'POST',
                data : student,
	            file: $scope.student.photo,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
	        		$state.transitionTo('student.list',{
						instituteId : $scope.student.instituteId,
	        			branchId : $scope.student.branchId,
	        			status : resp.data.status, 
	        			message : resp.data.message
					},{reload : true});
	        	}
	        	else {
	        		if(resp.data.isValidationError){
						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	};

						modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });
		};		

		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('student.list');
		};

		var studentParentPopupOptions = {
			templateUrl : 'app/students/html/branch-parent-popup.html',
			controller : 'StudentParentController',
			scope : $scope,
		};

		$scope.popup = function() {
			$scope.studentParentToEdit = {};
			var studentParentPopupOptions = {
				templateUrl : 'app/students/html/branch-parent-popup.html',
				controller : 'StudentParentController',
				scope : $scope,
			};
			modalPopupService.openPopup(studentParentPopupOptions);
		};

		$scope.editStudentParent = function(studentParent) {
			var studentParentToEdit = JSON.stringify(studentParent);
			studentParentToEdit = JSON.parse(studentParentToEdit);
			$scope.studentParentToEdit = studentParentToEdit;
			var studentParentPopupOptions = {
				templateUrl : 'app/students/html/branch-parent-popup.html',
				controller : 'StudentParentController',
				scope : $scope,
			};
			modalPopupService.openPopup(studentParentPopupOptions);
		};

		$scope.removeStudentParent = function(email) {
			for ( var i = 0; i < $scope.student.studentParentList.length; i++) {
				var studentParent = $scope.student.studentParentList[i];
				var flag = false;
				if (studentParent.email == email) {
						flag = true;
					if(flag){
						$scope.student.studentParentList.splice(i, 1);
					}
				}
			}
		};

	});

	
	angular.module('student').controller('StudentViewController',function($rootScope,$scope,$state,ReligionList,
			$uibModal, $filter, CasteList,Upload,$stateParams,$resource,CommonServices, StateList) {
		/** This is written to get single student object to display on the edit screen. */
		
		$scope.student = {};
		$scope.isView = true;

    	$scope.branchList = [];
		$scope.instituteList = [];
    	$scope.standardList = [];
    	$scope.stateList = StateList;
    	$scope.divisionList = [];
    	$scope.districtList = [];
		$scope.talukaList = [];    	
		$scope.feesCodes = [];
    	$scope.studentHeaderValue = "Edit Student";

		// Used when teacher login.
    	if($rootScope.roleId == 4){
    		$scope.student.branchId = $rootScope.branchId;
    	}

		$resource('students/:id').get({id : $stateParams.id}).$promise.then(function(student) {
			if(student.dateOfBirth){
				student.dateOfBirth = new Date(student.dateOfBirth);
			}

			if(student.studentParentList){
				for(var i = 0; i< student.studentParentList.length; i ++){
					var studentParent =  student.studentParentList[i];
					studentParent.uuId = i+1;
				}
			}

			angular.extend($scope.student,student);
			$rootScope.setSelectedData($scope.student.instituteId, $scope.student.branchId, $scope.student.standardId, $scope.student.divisionId);
			changeBranchList();
			changeDistrictList();
		});

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.student.instituteId || $scope.student.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.student.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
						changeStandardList();
				});	
			}
		};

		$scope.changeStandardList = function(){
			changeStandardList();
		};
		
		$scope.changeDivisionList = function(){
			changeDivisionList();
		};
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.student.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.student.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					getFeesCodes();
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			if($scope.student.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.student.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		function changeDistrictList(){
			
			if($scope.student.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.student.stateId})
					.$promise.then(function(districtList) {
						$scope.districtList.length = 0;
						angular.extend($scope.districtList, districtList);
						changeTalukaList();
				});	
			} 
		};
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		function changeTalukaList(){
			
			if($scope.student.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.student.districtId})
					.$promise.then(function(talukaList) {
						$scope.talukaList.length = 0;
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};
		
		function getFeesCodes(){
			if($scope.student.branchId){
				$resource('fees/fees-code/list/:id').query({id : $scope.student.branchId}).$promise.then(
				 function(feesCodeList) {
					 $scope.feesCodes.length = 0;
					 angular.extend($scope.feesCodes, feesCodeList);
				});
			}
			else {
				$scope.feesCodes.length = 0;
			}
		}

		$scope.religionList = ReligionList;
		$scope.casteList = CasteList;

		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('student.list');
		};
	});
	
	
	angular.module('student').controller('StudentEditController',function($rootScope,$scope,$state,ReligionList,
			modalPopupService, $filter, CasteList, IncomeRangeList, ProfessionList, Upload,$stateParams,$resource,CommonServices, StateList) {
		/** This is written to get single student object to display on the edit screen. */
		$scope.student = {};
		
    	$scope.branchList = [];
		$scope.instituteList = [];
    	$scope.standardList = [];
    	$scope.divisionList = [];
    	$scope.districtList = [];
		$scope.talukaList = [];
		$scope.stateList = StateList;
		$scope.feesCodes = [];
    	$scope.studentHeaderValue = "Edit Student";
    	
    	$scope.incomeRangeList = IncomeRangeList;
    	$scope.professionList = ProfessionList;
    	$scope.isCreateLoginAllowed = true;
    	var studentParentPopupOptions = {
			templateUrl : 'app/students/html/branch-parent-popup.html',
			controller : 'StudentParentController',
			scope : $scope,
		};

    	$scope.popup = function() {
			$scope.studentParentToEdit = {};
			modalPopupService.openPopup(studentParentPopupOptions);
		};

		$scope.editStudentParent = function(studentParent) {
			var studentParentToEdit = JSON.stringify(studentParent);
			studentParentToEdit = JSON.parse(studentParentToEdit);
			$scope.studentParentToEdit = studentParentToEdit;
			modalPopupService.openPopup(studentParentPopupOptions);
		};
		
		$scope.removeStudentParent = function(email) {
			for ( var i = 0; i < $scope.student.studentParentList.length; i++) {
				var studentParent = $scope.student.studentParentList[i];
				var flag = false;
				if (studentParent.email == email) {
						flag = true;
					if(flag){
						$scope.student.studentParentList.splice(i, 1);
					}
				}
			}
		};

		// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		// Used when teacher login.
    	if($rootScope.roleId == 4){
    		$scope.student.branchId = $rootScope.branchId;
    	}

		$resource('students/:id').get({id : $stateParams.id}).$promise.then(function(student) {
			if(student.dateOfBirth){
				student.dateOfBirth = new Date(student.dateOfBirth);
			}
			$scope.isCreateLoginAllowed = student.createStudentLogin == 'Y';

			if(student.studentParentList){
				for(var i = 0; i< student.studentParentList.length; i ++){
					var studentParent =  student.studentParentList[i];
					studentParent.uuId = i+1;
				}
			}

			angular.extend($scope.student,student);
			changeBranchList();
			changeDistrictList();
		});

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.student.instituteId || $scope.student.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.student.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
						changeStandardList();
				});	
			}
		};

		$scope.changeStandardList = function(){
			changeStandardList();
		};
		
		$scope.changeDivisionList = function(){
			changeDivisionList();
		};
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.student.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.student.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					/*if(standardList != null && standardList.length > 0){
						$scope.student.standardId = standardList[0].id;
        			}*/
					
					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					getFeesCodes();
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}
		
		function getFeesCodes(){
			if($scope.student.branchId){
				$resource('fees/fees-code/list/:id').query({id : $scope.student.branchId}).$promise.then(
				 function(feesCodeList) {
					 $scope.feesCodes.length = 0;
					 angular.extend($scope.feesCodes, feesCodeList);
				});
			}
			else {
				$scope.feesCodes.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.student.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.student.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		function changeDistrictList(){
			
			if($scope.student.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.student.stateId})
					.$promise.then(function(districtList) {
						$scope.districtList.length = 0;
						angular.extend($scope.districtList, districtList);
						changeTalukaList();
				});	
			} 
		};
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		function changeTalukaList(){
			
			if($scope.student.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.student.districtId})
					.$promise.then(function(talukaList) {
						$scope.talukaList.length = 0;
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};

		$scope.religionList = ReligionList;
		$scope.casteList = CasteList;

		/** A directive can be written for the below 4 lines. Will be done in future. */
		$scope.isOpen = false;

		/** This method is used to save edited object. Here we explicitly copy $scope.students to normal student object. This is done because $scope.student contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			$scope.status = '';
			$scope.message = '';
			var student = {
					id: $scope.student.id,
					firstname: $scope.student.firstname,
					fatherName: $scope.student.fatherName,
					motherName: $scope.student.motherName,
					surname: $scope.student.surname,
					gender: $scope.student.gender,
					dateOfBirth: $filter('date')($scope.student.dateOfBirth, "yyyy-MM-dd"),
					relationship: $scope.student.relationship,
					studentUserId:$scope.student.studentUserId,
					emailAddress : $scope.student.emailAddress,
					mobile:$scope.student.mobile,
					addressLine1: $scope.student.addressLine1,
					area: $scope.student.area,
					city: $scope.student.city,
					pincode : $scope.student.pincode,
					religion : $scope.student.religion,
					casteId : $scope.student.casteId,
					standardId : $scope.student.standardId,
					standardName : $scope.student.standardName,
					divisionId : $scope.student.divisionId,
					feesCodeId : $scope.student.feesCodeId,
					rollNumber : $scope.student.rollNumber,
					registrationCode : $scope.student.registrationCode,
					branchId : $scope.student.branchId,
					active: $scope.student.active,
					isDelete: $scope.student.isDelete,
					fileChanegd : $scope.student.fileChanegd,
					districtId: $scope.student.districtId,
					createStudentLogin : $scope.student.createStudentLogin,
					stateId: $scope.student.stateId,
					talukaId: $scope.student.talukaId,
					studentParentList : $scope.student.studentParentList
			};

			/** This method is used to adding event. It will only call add utility method */
			Upload.upload({
	            url: 'students/save',
	            method: 'POST',
                data : student,
	            file: $scope.student.photo,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
	        		$state.transitionTo('student.list',{
						instituteId : $scope.student.instituteId,
	        			branchId : $scope.student.branchId,
	        			status : resp.data.status, 
	        			message : resp.data.message
					},{reload : true});
	        	}
	        	else {
					if(resp.data.isValidationError){
						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorContro ler",
				    	      scope : $scope
				    	 };
						modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });
		};

		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('student.list');
		};
	});



	angular.module('student').controller('StudentReportController',function($rootScope,$resource,$scope, 
			$window, $http, $localStorage, $confirm,DTOptionsBuilder,$state,$uibModal,CommonServices) {

	    $scope.students = [];
	    
	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.standardList = [];
		$scope.divisionList = [];

		$scope.instituteId =  $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true).withOption("scrollX", true);

		///if teacher Login
		if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		changeStandardList();
		}

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isUploadingFailed = false;
    		filterTableByBranch($scope.branchId);
		};


		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1 && $rootScope.roleId != 4){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList != null && branchList.length > 0){
        				if(branchList.length == 1){
        					$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;
        				}
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};


		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						changeStandardList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};


		$scope.changeStandardList = function(){
			changeStandardList();
		};

		$scope.changeDivisionList = function(){
			changeDivisionList();
		};
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;

					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}

		function getFilterObject(){
			var filter = {'instituteId' : $scope.instituteId,
		        	'branchId' : $scope.branchId,
		        	'standardId' : $scope.standardId,
		        	'divisionId' : $scope.divisionId,
		    };
			return filter;
		}

		function filterTableByBranch(branchId){
			if(branchId != null ){
				$http({
			        url: 'students/report',
			        method: "POST",
			        data: getFilterObject()
			    })
			    .then(function(response) {
			        $scope.students.length = 0;
			        $scope.students = response.data.students;
			    },
			    function(err) { // optional
			    	console.log(err);
			    });
			}
			else {
				$scope.feesreports.length = 0;
			}
		}

		// Download excel records according to filters.
		$scope.exportToExcel = function (){
			var url = "students/downloadReport?instituteId="+ ($scope.instituteId  ? $scope.instituteId : "0") +
				($scope.branchId ? "&branchId="+$scope.branchId : "")+
				($scope.standardId ? "&standardId="+$scope.standardId : "")
				+($scope.divisionId ? "&divisionId="+ $scope.divisionId : "");

			$window.location.href = url;
		};
	});



	/** This method is used for uploading data */
	angular.module('student').controller('StudentFileUploadController',function($scope,Upload, $uibModalInstance, $uibModalStack, modalPopupService) {
		$scope.uploadPopupTitle = "Upload Student Details";
		if($scope.$parent.isUploadUpdate){
			$scope.uploadPopupTitle = "Bulk Upload Update Student Details";
		}
		
		$scope.upload = function (file) {
	        Upload.upload({
	        	url: 'students/' + ($scope.$parent.isUploadUpdate ? 'uploadUpdateFile' : 'uploadFile'),
	            //url: ($scope.$parent.isUploadUpdate ? 'students/' : 'fileUpload/') + ($scope.$parent.isUploadUpdate ? 'uploadUpdateFile' : 'student'),
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			if($scope.$parent.isExpressOnboard){
	        				$scope.studentUploadErrMsg = resp.data.message;
	        			}else{	        				
	        				$scope.validationErrorMsg = resp.data.message;
	        			}
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	$scope.$parent.status = resp.data.status;
	        	if($scope.$parent.isExpressOnboard){
	        		$scope.$parent.studentUploadErrMsg = resp.data.message;
	        	}else {
	        		$scope.$parent.message = resp.data.message;
	        	}
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.students.length = 0;
	        	angular.extend($scope.students,resp.data.records);
	        });
	    };

	    	$scope.cancel = function(){
	    		modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
	
	angular.module('student').controller('StudentFileBatchUploadController',function($scope,Upload, $uibModalInstance, $uibModalStack, modalPopupService) {
		$scope.uploadPopupTitle = " Batch Upload Student Details";
		if($scope.$parent.isUploadUpdate){
			$scope.uploadPopupTitle = "Bulk Batch Upload Student Details";
		}
		$scope.upload = function (file) {
	        Upload.upload({
	            url: 'students/' + ($scope.$parent.isBatchUpload ? 'BatchUploadFile' : 'batchuploadFile'),
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	$scope.$parent.status = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.students.length = 0;
	        	angular.extend($scope.students,resp.data.records);
	        });
	    };

    	$scope.cancel = function(){
    		modalPopupService.closePopup(null, $uibModalStack);
	    };
	});

	angular.module('student').controller('StudentUploadStatusController',function($resource,$scope,$rootScope,Upload, $uibModalInstance, $uibModalStack, modalPopupService) {
		$scope.uploadstatusPopupTitle = "Upload Status Details";
		$scope.uploadJobs = [];
		$scope.showLogView = false;
		$scope.log = {};

		$scope.showLog = function (id){
			$resource('fileUpload/error/log?id='+id).get().$promise.then(
			 function(response) {
				 $scope.showLogView = true;
				 $scope.log.validationErrMsg = response.message;
				 $scope.log.errorRecords = response.records;
			});
		}

		$scope.goBack = function (){
			$scope.showLogView = false;
		}
		
		$scope.downloadFile = function (id){
			var url = 'fileUpload/error/download?id='+id;
			document.location.href = url;
		}

		function uploadStatus(){
			if($scope.branchId){
				$resource('fileUpload/status?branchId='+$scope.branchId).query().$promise.then(
				 function(response) {
					 $scope.uploadJobs = response;
				});
			}
		}
		uploadStatus();
		$scope.cancel = function(){
	    		modalPopupService.closePopup(null, $uibModalStack);
	    };

	});
	
	
	

	angular.module('student').controller('StudentFileBatchUploadController',function($scope,Upload, $uibModalInstance, $uibModalStack, modalPopupService) {
		$scope.uploadPopupTitle = " Batch Upload Student Details";
		if($scope.$parent.isUploadUpdate){
			$scope.uploadPopupTitle = "Bulk Batch Upload Student Details";
		}
		$scope.upload = function (file) {
			Upload.upload({
				url: 'fileUpload/' + ($scope.$parent.isBatchUpload ? 'studentUpdate' : 'student'),
				data : {branchId : $scope.branchId},
				file: file
			}).then(function(resp) {
				if(resp.data.status == 'fail'){
					if(resp.data.isValidationError == 'true'){
						$scope.validationErrorMsg = resp.data.message;
						$scope.validationStatus = resp.data.status;
						$scope.records = resp.data.records;
						return;
					}
					$scope.$parent.isUploadingFailed = true;
				}
				$scope.$parent.status = resp.data.status;
				$scope.$parent.message = resp.data.message;
				modalPopupService.closePopup(null, $uibModalStack);
				$scope.students.length = 0;
				angular.extend($scope.students,resp.data.records);
			});
		};
		
		$scope.cancel = function(){
		modalPopupService.closePopup(null, $uibModalStack);
		};
	});

	

	angular.module('group').controller('StudentParentController',['$scope','$resource','$uibModalInstance', '$uibModalStack', '$filter', 'modalPopupService',
        function($scope, $resource, $uibModalInstance, $uibModalStack,$filter, modalPopupService) {

		$scope.isStudentParentExistMsg = null;
		$scope.profile = {};
		$scope.genders = [{
	        value: 'M',
	        text: 'MALE'
	    }, {
	        value: 'F',
	        text: 'FEMALE'
	    }];

		$scope.relations = [{
	        value: 1,
	        text: 'FATHER'
	    }, {
	        value: 2,
	        text: 'MOTHER'
	    }, {
	        value: 3,
	        text: 'GUARDIAN'
	    }];

		var studentParentToEdit = JSON.stringify($scope.studentParentToEdit);
		studentParentToEdit = JSON.parse(studentParentToEdit);

		$scope.studentParent = (studentParentToEdit ? studentParentToEdit : {});

		$scope.isEdit = ($scope.studentParent.firstname ? true : false);

		$scope.isParentCreateLoginAllowed = $scope.studentParent.createLogin == 'Y';
		
		if($scope.isEdit){
			if($scope.studentParent.dob){
				$scope.studentParent.dateOfBirth = new Date($scope.studentParent.dob);
			}
		}

		/** Add individuals to an array */
		$scope.addParent = function() {
			$scope.isStudentParentExistMsg = null;
			// console.log("TO SAVE ::::: "+JSON.stringify($scope.studentParent));
			if($scope.studentParent.firstname) {
				var isStudentParentExist = false;
				var studentParentIndex = -1;
				try {
					studentParentIndex = studentParentExist();
				}catch(err){
					isStudentParentExist = true;
				}
				$scope.studentParent.dob = $filter('date')($scope.studentParent.dateOfBirth, "yyyy-MM-dd");
				if(!isStudentParentExist){

					var studentParent = $scope.studentParent;
					studentParent = JSON.stringify(studentParent);
					studentParent = JSON.parse(studentParent);

					$scope.student.studentParentList[studentParentIndex] = studentParent;
					modalPopupService.closePopup(null, $uibModalStack);
				}
			}
		};

		function studentParentExist() {
			var studentParentIndex = -1;
			if($scope.student.studentParentList && $scope.student.studentParentList.length > 0){

				for(var i = 0; i < $scope.student.studentParentList.length; i++){
					var studentParent = $scope.student.studentParentList[i];
					if(studentParent.uuId != $scope.studentParent.uuId) {
						var isExist = false;
						if(studentParent.email  && 
								studentParent.email == $scope.studentParent.email){
							$scope.isStudentParentExistMsg = "Parent already already exists with this email.";
							isExist = true;
						}
						else if(studentParent.primaryContact && 
								studentParent.primaryContact == $scope.studentParent.primaryContact){
							isExist = true;
							$scope.isStudentParentExistMsg = "Parent already already exists with this contact number.";
						}
						/*else if(studentParent.firstname == $scope.studentParent.firstname && 
								studentParent.lastname == $scope.studentParent.lastname){
							isExist = true;
							$scope.isStudentParentExistMsg = "Parent already already exists with this name.";
						}*/
						if(isExist){
							new Exception("Match Found Exist!!");
							break;
						}
					}
					else if(studentParent.uuId == $scope.studentParent.uuId){
						studentParentIndex = i;
					}
				}
			}
			if(studentParentIndex == -1){
				studentParentIndex = $scope.student.studentParentList.length;
				$scope.studentParent.uuId = $scope.student.studentParentList.length + 1;
			}
			return studentParentIndex;
		}

		$scope.isGenderReadOnly= true;
		$scope.cancelPopup = function() {
			modalPopupService.closePopup(null, $uibModalStack);
		};

		/** Add individuals to an array */
		$scope.updateGender = function() {
			
			var relationId = $scope.studentParent.relationId;
			
			if(relationId == 1) {//father
				$scope.studentParent.gender = 'M';
				$scope.isGenderReadOnly= true;
				$scope.genders = [{
			        value: 'M',
			        text: 'MALE'
			    }];
			} else if(relationId == 2) {//mother
				$scope.studentParent.gender = 'F';
				$scope.isGenderReadOnly= true;
				$scope.genders = [{
			        value: 'F',
			        text: 'FEMALE'
			    }];
			} else {//guardian
				$scope.isGenderReadOnly= false;
				$scope.genders = [{
			        value: 'M',
			        text: 'MALE'
			    }, {
			        value: 'F',
			        text: 'FEMALE'
			    }];
			}
			
		};
			
	} ]);

})();