(function(){
	angular.module('feesreport').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('feesreport', {
			url : '/feesreport',
			abstract :true,
			controller : 'FeesReportModelController',
			data: {
				 breadcrumbProxy: 'feesreport.list'
	        }
		}).state('feesreport.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/feesreport/html/feesreport-list.html',
					controller : 'FeesReportController'
				}
			}
		});
	}]);
})();