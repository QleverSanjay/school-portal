(function(){
	
		
	angular.module('feesreport').controller('FeesInfoController',function($resource,userserviceapiurl,$scope,
			$filter, $state,InitializationService,$sce,$http,$stateParams, $compile, $uibModalInstance, $uibModalStack, modalPopupService) {
		$scope.feesInfo = {};
		$scope.feesInfo.displayLayouts = [];
		if($scope.$parent.selectedPaymentInfoId){
			$resource(userserviceapiurl+'parent/fees/feesInfo?feeScheduleId='+$scope.$parent.selectedPaymentInfoId).get()
			.$promise.then(function(paymentObj) {
				$scope.feesInfo = paymentObj;
				$scope.feesInfo.displayLayouts = paymentObj.display_layouts;
				$scope.feesInfo.feeSchedules = paymentObj.fees_charges;		
			});
		}

		$scope.closePopup = function (){
			modalPopupService.closePopup($uibModalInstance, $uibModalStack);
		};
	});

	angular.module('feesreport').controller('FeesReportController',function($resource,$filter, $compile,
			DTOptionsBuilder, DTColumnBuilder, $window, $localStorage, $http, $rootScope, modalPopupService,
			userserviceapiurl,$stateParams,$scope,$state, $uibModal, $filter,$uibModalStack,Upload,$rootScope) {
		$scope.feesreports = [];

		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.headList = [];
		$scope.feesList = [];
		$scope.standardList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
		$scope.casteList = [];
		$scope.latePayment = {};
		$scope.latePayment.studentFeesArray =[];
		$scope.latePayment.feeScheduleIds = [];

		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch ;

		/* Fees upload dummy code*/
		   $scope.uploadPaymentReport = function(){
		    	$scope.isUploadUpdate = false;
		    	var options = {
		    	      templateUrl: 'app/feesreport/html/payment-report-upload.html',
		    	      controller: 'FeesReportController',
		    	      scope : $scope
		    	    };
		    	modalPopupService.openPopup(options);
		     };
		     $scope.upload = function (file) {
			        Upload.upload({
			        	url: '/fileUpload/paymentReport/',
			            file: file
			        }).then(function(resp) {
			        	if(resp.data.status == 'fail'){
			        		$scope.messages = resp.data.message;
				        } else if(resp.data.status == 'success'){
				        	
				        	$rootScope.messages = resp.data.message;
				        }
			        	modalPopupService.closePopup(null, $uibModalStack);
			        	
			        },function(err){
			        	console.log(err);
			        });
			    };
		 	$scope.cancel = function(){
	    		modalPopupService.closePopup(null, $uibModalStack);
		 	};
		
		
		/* fees upload dummy code*/
		 	
		/* Awaiting settlement date report upload*/
		 	
		 	$scope.uploadAwatingSettlementDateReport = function(){
		    	$scope.isUploadUpdate = false;
		    	var options = {
		    	      templateUrl: 'app/feesreport/html/awaiting-settlement-upload.html',
		    	      controller: 'FeesReportController',
		    	      scope : $scope
		    	    };
		    	modalPopupService.openPopup(options);
		     };
		     
		     $scope.uploadAwaitingSettlement = function (file) {
			        Upload.upload({
			        	url: '/fees/uploadAwaitingSettlementDate/',
			            file: file
			        }).then(function(resp) {
			        	if(resp.data.status == 'fail'){
			        		$scope.messages = resp.data.message;
				        } else if(resp.data.status == 'success'){
				        	
				        	$rootScope.messages = resp.data.message;
				        }
			        	modalPopupService.closePopup(null, $uibModalStack);
			        	
			        },function(err){
			        	console.log(err);
			        });
			    };
		     /* Awaiting settlement date report upload*/	
		 	
		$scope.dtColumns = [
            DTColumnBuilder.newColumn(null).withTitle('Select').notSortable()
            .renderWith(function(report, type, full, meta) {
            	var select = '';
				if((report.status == 'PARTIALPAID' || report.status == 'UNPAID') && !report.recordDeleted){
					select = '<input type="checkbox" name="marklatePayment" ng-model="data.markLatepayment['+report.feeScheduleId+']" ';
					select += 'ng-change="selectToMark('+report.feeScheduleId+', '+report.feesId+', '+report.studentId+', data.markLatepayment['+report.feeScheduleId+'], '+report.feeAmount+', '+report.latePaymentCharges+', '+report.discountAmount+')" />';
				}
				/*else if(report.status == "PAID" || report.recordDeleted){
					select += '<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
				}*/
				select += ' <a style = "float: right; padding-right: 10px;"ng-click="getFeesInfo('+report.feeScheduleId+')"><i class="fa fa-info-circle" aria-hidden="true">';
                return select;
            }),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable()
            .renderWith(function(report, type, full, meta) {
            	var action = '';
            	if(report.feesDescription){
            		report.feesDescription = report.feesDescription.replace(/'/g, '`');	
            	}

				if(!report.recordDeleted && report.status != 'PAID' && $rootScope.roleId !=8){
					action += '<span> ' ;
					if(report.canPay){
						action += '<input type="button" value="Mark As Paid" ng-click="markAsPaid('+report.feeScheduleId+', '+report.feesId+', '+report.studentId+', \''+report.feesDescription+'\', '+report.partialPaymentAllowed+', '+report.remainingAmount+')" />';
						console.log(action);
					}
					action +='<input type="button" value="Change Fee Charge" ng-click="markLatePayment('+report.feeScheduleId+', '+report.feesId+', '+report.studentId+', \''+report.feesDescription+'\', '+report.feeAmount+', '+report.latePaymentCharges+', '+report.discountAmount+')" /> '
							+'</span>';
				}
				if(report.status != 'UNPAID' && $rootScope.roleId !=8){
					action += '<span> '
								+' <input type="button" value="Payment Details" ng-click="markAsUnpaid('+report.feeScheduleId+', '+report.feesId+', '+report.studentId+', \''+report.studentName+'\', \''+report.feesDescription+'\', '+report.partialPaymentAllowed+')" />'
							+'</span>';
				}
                return action;
            }),
            DTColumnBuilder.newColumn("status", "Status").withOption('name', 'status'),
            DTColumnBuilder.newColumn("studentName", "Student Name").withOption('name', 'studentName'),
            DTColumnBuilder.newColumn("standard", "Standard/Course").withOption('name', 'standard'),
            DTColumnBuilder.newColumn("division", "Division").withOption('name', 'division'),
            DTColumnBuilder.newColumn("rollNumber", "Roll Number").withOption('name', 'rollNumber'),
            DTColumnBuilder.newColumn("registrationCode", "Registration Code").withOption('name', 'registrationCode'),
            DTColumnBuilder.newColumn("casteName", "Caste").withOption('name', 'Caste'),
            DTColumnBuilder.newColumn("head", "Fees Type").withOption('name', 'head'),
            DTColumnBuilder.newColumn("paidDate", "Fee Paid Date").withOption('name', 'paidDate'),
            DTColumnBuilder.newColumn("dueDate", "Fee Due Date").withOption('name', 'dueDate'),
            DTColumnBuilder.newColumn(null).withTitle('Fee Amount').notSortable()
            .renderWith(function(report, type, full, meta) {
            	var select = '<i class = "fa fa-'+report.currencyCssClass+'" ></i> ' + report.feeAmount;
                return select;
            }),
//          DTColumnBuilder.newColumn("feeAmount", "Fee Amount").withOption('name', 'feeAmount'),
            DTColumnBuilder.newColumn("discountAmount", "Discount Amount").withOption('name', 'discountAmount'),
            DTColumnBuilder.newColumn("latePaymentCharges", "Late Payment Charges").withOption('name', 'latePaymentCharges'),
            DTColumnBuilder.newColumn("totalAmount", "Total Amount").withOption('name', 'totalAmount'),
            DTColumnBuilder.newColumn("paidAmount", "Paid Amount").withOption('name', 'paidAmount'),
            DTColumnBuilder.newColumn("remainingAmount", "Remaining Amount").withOption('name', 'remainingAmount'),
        ];
		var draw = 0;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true)
        .withOption("scrollX", true)
        .withOption("deferLoading", false)
        .withOption('createdRow', function(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
            if (!$scope.headerCompiled) {
            	$scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('ajax', {
	         url: 'fees/report',
	         type: 'POST',
	         contentType: 'application/json',

	         data: function(data, dtInstance) {
	        	 draw ++;
	        	 var filter = getFilterObject();
	        	 $scope.isSearchClicked = 'N';
	        	 data.columns.length = 0;
	        	 filter.tableOptions = data;
	        	 return JSON.stringify(filter);
	         },
	         error:function (xhr, error, thrown) {
	        	 $scope.message = thrown;
	        	 $scope.$apply();
	        	 var processingDialog = document.getElementById("feesReport_processing");
	        	 processingDialog.style.visibility='hidden' ;
	        	 if(xhr.responseText.indexOf("Login Page") != -1){
	        		 $state.transitionTo('login.logout',{},{reload : true});
	        	 }
	         }
	     })
	    .withDataProp('feesReports')
	    .withOption('processing', true) 
        .withOption('serverSide', true) 
        .withOption('ordering', false)
        .withPaginationType('full_numbers') 
        .withDisplayLength(10);
//        .withOption('aaSorting',[3,'asc']);

		$scope.getFeesInfo = function (feeScheduleId){
			$scope.selectedPaymentInfoId  =  feeScheduleId;
				 var options = {
	            	templateUrl : 'app/feesreport/html/payment-popup.html' ,
	            	controller : 'FeesInfoController',
	               	scope : $scope
	    	     };
				modalPopupService.openPopup(options);
		};


		$scope.fromDateStr = "";
		$scope.toDateStr = "";
     	// get all institutes.
    	// Mainly used when login user is super admin. 
		if($rootScope.roleId == 1 || $rootScope.roleId == 8){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				if($rootScope.roleId == 6 && branchList.length == 1){
        					$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;
        				}
        				changeBranchList(false);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}
		
		// Get all caste.

    	$resource('masters/casteMasters').query().$promise.then(function(casteList) {
    		angular.extend($scope.casteList,casteList);
		});

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		// on Branch changed get standadrd and heads as per branch
		$scope.branchChanged = function(){
			changeStandardList();
			changeHeadList();
		};

		// Change standardList as per the branch selected.
		function changeStandardList(){
			if($scope.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;
					angular.extend($scope.standardList,standardList);
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		$scope.changeDivisionList = function(){
			changeDivisionList();
		};

		function changeDivisionList(){
			if($scope.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		function changeHeadList(){
			if($scope.branchId){		
				$resource('head/getHeadByBranchForDashboard?branchId='+$scope.branchId).query()
					.$promise.then(function(headList) {

					$scope.headList.length = 0;
					angular.extend($scope.headList,headList);
					
	    		});
			}
		}

		$scope.changeFeeslist = function(){
			if($scope.headId){		
				$resource('masters/getFeesByHead/:id').query({id : $scope.headId})
					.$promise.then(function(feesList) {

					$scope.feesList.length = 0;
					angular.extend($scope.feesList, feesList);									
	    		});
			}
		};

		function validateForm(){
			$scope.fromDate = ($scope.fromDate == null || $scope.fromDate == "") ? undefined : $scope.fromDate;
			$scope.toDate = ($scope.toDate == null || $scope.toDate == "") ? undefined : $scope.toDate;
			$scope.dateErrorMessgae = "";

			if(!isNaN($scope.fromDate) && !isNaN($scope.toDate)){
				if($scope.fromDate > $scope.toDate){
					$scope.dateErrorMessgae = "To date must be after from date.";
					throw new Exception("To date must be after from date.");
				}
				else {
					$scope.fromDateStr = $filter('date')($scope.fromDate, "yyyy-MM-dd");
					$scope.toDateStr = $filter('date')($scope.toDate, "yyyy-MM-dd");
				}
			}
			else if(!isNaN($scope.fromDate) || !isNaN($scope.toDate)){
				$scope.dateErrorMessgae = "From date and to date required for date wise filter.";
				throw new Exception("From date and to date required for date wise filter.");
			}
		}

		$scope.filterTableByBranch = function(){
			$scope.message = "";
			validateForm();
			$scope.isSearchClicked = 'Y';
			var table = angular.element( document.querySelector('#feesReport')).DataTable();
			
			table.ajax.reload( function ( json ) {
			} );
		};

		function getFilterObject(){
			var filter = {'instituteId' : $scope.instituteId,
		        	'branchId' : $scope.branchId,
		        	'standardId' : $scope.standardId,
		        	'divisionId' : $scope.divisionId,
		        	'rollNumber' : $scope.rollNumber,
		        	'registrationCode':$scope.registrationCode,
		        	'status' : $scope.status,
		        	'headId' : $scope.headId,
		        	'feesId' : $scope.feesId,
		        	'casteId' : $scope.casteId,
		        	'fromDate' : $scope.fromDateStr,
		        	'toDate' : $scope.toDateStr,
		        	'isSearchClicked' : $scope.isSearchClicked +''
		    };
			return filter;
		}

		// Download excel records according to filters.
		$scope.exportSummaryReport = function (){
			validateForm();
			var url = "fees/downloadFeeSummaryReport?" + getDownloadUrlData();
			$window.location.href = url;
		};

		function getDownloadUrlData(){
			var url = "instituteId="+ ($scope.instituteId  ? $scope.instituteId : "0") +
				($scope.branchId ? "&branchId="+$scope.branchId : "")+
				($scope.standardId ? "&standardId="+$scope.standardId : "")
				+($scope.divisionId ? "&divisionId="+ $scope.divisionId : "")
				+($scope.rollNumber ? "&rollNumber="+ $scope.rollNumber : "")
				+($scope.registrationCode ? "&registrationCode="+$scope.registrationCode : "")
				+($scope.status ? "&status="+$scope.status : "")
				+($scope.headId ?  "&headId="+$scope.headId : "")
				+($scope.feesId ? "&feesId="+$scope.feesId : "")
				+($scope.casteId ? "&casteId="+$scope.casteId : "")
				+($scope.fromDateStr ? "&fromDate="+$scope.fromDateStr : "")
				+($scope.toDateStr ? "&toDate="+$scope.toDateStr : "");
			return url;
		}

		$scope.exportDetailReport = function (){
			validateForm();
			var url = "fees/downloadFeeDetailReport?" + getDownloadUrlData();
			$window.location.href = url;
		};
		
		$scope.incorrectTransactionIdReport = function(){
			validateForm();
			var url = "fees/incorrectTransactonId";
			$window.location.href = url;
		}
		
		$scope.emptySettlementDateReport = function(){
			validateForm();
			var url = "fees/emptySettlementDate/" + $scope.branchId;
			$window.location.href = url;
		}
		
		$scope.exprotFailedTransactionReport = function(){
			validateForm();
			var url = "fees/report/failure-transactions?branchId=" + $scope.branchId;
			$window.location.href = url;
		}

		// Get records according to filters.
		function filterTableByBranch(branchId){
			if(branchId != null ){
				var filter = getFilterObject();
				filter.tableOptions = $scope.tableOptions;
				$http({
			        url: 'fees/report',
			        method: "POST",
			        data: filter
			    })
			    .then(function(response) {
			        console.log(response);
			        $scope.feesreports.length = 0;
			        if(response.data){
			        	/*for(var i =0 ;i<response.data.length; i++){
			        		var report = response.data[i];
			        		if(report){
			        			report.discountAmount = (report.discountAmount ? parseInt(report.discountAmount) : 0);
			        			report.latePaymentCharges = (report.latePaymentCharges ? parseInt(report.latePaymentCharges) : 0);
			        		}
			        	}*/
			        }
			        $scope.feesreports = response.data.dataq;
			    },
			    function(err) { // optional
			    	console.log(err);
			    });
			}
			else {
				$scope.feesreports.length = 0;
			}
		}

//		$scope.latePayment.studentFeesArray = [];
//		$scope.latePayment.feeScheduleIds = [];

		$scope.selectToMark = function (feeScheduleId, feesId, studentId, isChecked, amount, lateFeesCharges, discountAmount){
			console.log("fees id :: "+feesId + ", StudentId ::: "+studentId + ", isChecked ::: "+isChecked);
			var studentFees = {"feesId" : feesId, "studentId" : studentId, "amount" : amount, 
					"lateFeesCharges" : lateFeesCharges, "discountAmount" :discountAmount};

			if(isChecked){
				$scope.latePayment.studentFeesArray.push(studentFees);
				$scope.latePayment.feeScheduleIds.push(feeScheduleId);
			}
			else {
				for(var i =0; i < $scope.latePayment.feeScheduleIds.length; i++){
					var feeScheduleIdToDelete = $scope.latePayment.feeScheduleIds[i];
					if(feeScheduleIdToDelete  == feeScheduleId){
						$scope.latePayment.studentFeesArray.splice(i, 1);
						$scope.latePayment.feeScheduleIds.splice(i, 1);
						break;
					}
				}
			}
		};

		$scope.feesToUnpaid = {};
		$scope.markAsUnpaid = function (feeScheduleId, feesId, studentId, studentName, feesDescription, partialPaymentAllowed){
			$scope.feesToUnpaid = {};
			$scope.feesToUnpaid.feeScheduleId = feeScheduleId;
			$scope.feesToUnpaid.studentName = studentName;
			$scope.feesToUnpaid.feesDescription = feesDescription;
			$scope.feesToUnpaid.partialPaymentAllowed = partialPaymentAllowed;

			$resource('fees/getFeesPaymentDescription?feeScheduleId='+feeScheduleId+'&roleId='+$rootScope.roleId+'&userId='+$rootScope.id)
				.query().$promise.then(function(resp) {

				if(resp.length > 0){
					for(var i =0; i < resp.length; i ++){
						resp[i].checked = false;
					}
					console.log("FEES DESCRIPTIONS ::: ");
					console.log(JSON.stringify(resp));

					$scope.feesToUnpaid.feesPaymentList = resp;

					$uibModal.open({
			    	      templateUrl: 'app/feesreport/html/fees-mark-unpaid.html',
			    	      controller: 'MarkFeesUnpaidOrRefundController',
			    	      scope : $scope
			    	});
				}
			});
	    };

		$scope.markLatePayment = function (feeScheduleId, feesId, studentId, feesDescription, amount, lateFeesCharges, discountAmount){
//			$scope.latePayment = {};
			$scope.latePayment.studentFeesArray.length =0;
			$scope.latePayment.feeScheduleIds.length =0;

			var studentFees = {"feesId" : feesId, "studentId" : studentId, "amount" : amount, 
					"lateFeesCharges" : lateFeesCharges, "discountAmount" :discountAmount};
			$scope.latePayment.studentFeesArray.push(studentFees);
			$scope.latePayment.feeScheduleIds.push(feeScheduleId);
			$scope.latePayment.feesDescription = feesDescription;

			openLatepaymentDialogue();
	    };

	    $scope.markMultipleLatePayment = function (){
			$scope.latePayment.feesDescription = "Changing remaining amount for "+$scope.latePayment.studentFeesArray.length + " entries";
			openLatepaymentDialogue();
	    };

	    function openLatepaymentDialogue(){
	    	 $uibModal.open({
	    	      templateUrl: 'app/feesreport/html/fees-mark-late-payment.html',
	    	      controller: 'MarkLateFeesController',
	    	      scope : $scope
	    	 });
	    }

		$scope.markAsPaid = function (feeScheduleId, feesId, studentId, feesDescription, partialPaymentAllowed, remainingAmount){
			 $scope.feesIdToMarkAsPaid = feesId;
			 $scope.studentId = studentId;
			 $scope.feesDescription = feesDescription;
			 $scope.partialPaymentAllowed = partialPaymentAllowed;
			 $scope.objToMarkPaid = {};
			 $scope.objToMarkPaid.feeScheduleId = feeScheduleId;
			 $scope.objToMarkPaid.remainingAmount = remainingAmount;
	    	 $uibModal.open({
	    	      templateUrl: 'app/feesreport/html/fees-mark-paid.html',
	    	      controller: 'MarkFeesPaidController',
	    	      scope : $scope
    	    });
	     };
	});


	angular.module('feesreport').controller('MarkFeesUnpaidOrRefundController',function($rootScope,$scope,$filter, 
			$uibModalInstance,$state, $resource, userserviceapiurl) {

		$scope.feesPaymentIdsToDelete = [];
		$scope.temp = {};
		$scope.temp.feesDescriptions = $scope.feesToUnpaid.feesPaymentList;

		$scope.setChecked  = function (payment){
			if (payment.checked){
				$scope.feesPaymentIdsToDelete.push(payment.feeScheduleId);
			}
			else {
				$scope.feesPaymentIdsToDelete.splice($scope.feesPaymentIdsToDelete.indexOf(payment.feeScheduleId), 1);
			}
		};

		$scope.downloadReceipt = function(paymentId){
			window.location.href = userserviceapiurl +"payment/"+paymentId+"/receipt?token="+localStorage.getItem('token');
		};

		$scope.deletePaidFees = function () {
			 if($scope.feesPaymentIdsToDelete.length > 0){
				 $resource('fees/deletePaidFees').save($scope.feesPaymentIdsToDelete).$promise.then(function(resp) {
					$scope.$parent.filterTableByBranch($scope.$parent.branchId);
					$scope.$parent.message = "Payment details updated.";
					$uibModalInstance.dismiss('cancel');
				}, function (err){
					$scope.unpaidErrorMessage = "Problem while marking fees as unpaid.";
					console.log(err);
				});
			 }
		};

		$scope.refundFees = function (paymentId){
			var refundFeesObj = {};
			var feesToRefund = {};
			for(var i = 0; i < $scope.temp.feesDescriptions.length; i++){
	    		var feesDesc = $scope.temp.feesDescriptions[i];
	    		feesDesc.errorMessage = "";
	    		if(paymentId == feesDesc.id && feesDesc.pgEntry){
	    			if(!feesDesc.newAmount || feesDesc.newAmount == 0) {
	    				feesDesc.errorMessage = "Amount is required to refund.";
	    				break;
	    			}else if(feesDesc.newAmount > feesDesc.amount ){
	    				feesDesc.errorMessage = "Refund amount cannot be greater than paid amount.";
	    				break;
	    			}
	    			feesToRefund = feesDesc;
	    			break;
	    		}
	    	}

			if(feesToRefund.newAmount > 0){
				var refundFeesObj = {
    				paymentId : paymentId,
    				amount : feesToRefund.newAmount
    			};

				$resource(userserviceapiurl + 'payment/refund?paymentId=' + refundFeesObj.paymentId 
						+'&amount=' + refundFeesObj.amount).get().$promise.then(function(resp) {
					$scope.$parent.filterTableByBranch($scope.$parent.branchId);
					$scope.$parent.message = "Payment details updated.";
					$uibModalInstance.dismiss('cancel');
				}, function(err){
					$scope.unpaidErrorMessage = err.data.message;
					console.log(err);
				});
			}
		}

	    $scope.updatePaidFees = function (feeScheduleId) {
	    	var feesDescriptionList = [];
	    	for(var i = 0; i < $scope.temp.feesDescriptions.length; i++){
	    		var feesDesc = $scope.temp.feesDescriptions[i];
	    		if(feeScheduleId == feesDesc.feeScheduleId && !feesDesc.pgEntry){
	    			feesDesc.newAmount = feesDesc.newAmount ? feesDesc.newAmount : 0;
	    			feesDescriptionList.push(feesDesc);
	    			break;
	    		}
	    	}

	    	if(feesDescriptionList.length > 0){
	    		$resource('fees/updatePaidFees').save(feesDescriptionList).$promise.then(function(resp) {
					$scope.$parent.filterTableByBranch($scope.$parent.branchId);
					$scope.$parent.message = "Payment details updated.";
					$uibModalInstance.dismiss('cancel');
				}, function(err){
					$scope.unpaidErrorMessage = "Problem while marking fees as unpaid.";
					console.log(err);
				});
	    	}
		};

		$scope.cancel = function(){
	    	$uibModalInstance.dismiss('cancel');
	    };
	});
	
	
	angular.module('fees').controller('MarkLateFeesController',function($rootScope,$scope,$filter, 
			$uibModalInstance,$state, $resource) {

		$scope.amountRegEXP = /^[0-9]{1,45}$/;
		$scope.latePayment.lateFeesCharges = ($scope.latePayment.studentFeesArray.length > 1 ? 0 : $scope.latePayment.studentFeesArray[0].lateFeesCharges);
		$scope.latePayment.discountAmount = ($scope.latePayment.studentFeesArray.length > 1 ? 0 : $scope.latePayment.studentFeesArray[0].discountAmount);

	    $scope.setFeeCharge = function (useLateFeeEngine) {
	    	$scope.lateFeeAmountError = "";
	    	$scope.discountAmountError = "";

	    	if(!useLateFeeEngine){
	    		var isValid = true;

		    	if($scope.latePayment.discountAmount < 0){
					$scope.discountAmountError = "Must be valid amount.";
					isValid = false;
				}
		    	if($scope.latePayment.discountAmount){
		    		for(var i = 0; i< $scope.latePayment.studentFeesArray.length; i++){
		    			var studentFeesObj = $scope.latePayment.studentFeesArray[i];
		    			if(studentFeesObj.amount < $scope.latePayment.discountAmount){
		    				$scope.discountAmountError = "Discount amount must be less than actual fee amount.";
		    				isValid = false;
		    			}
		    		}
		    	}
		    	else if($scope.latePayment.lateFeesCharges < 0){
					$scope.lateFeeAmountError = "Must be valid amount.";
					isValid = false;
				}

		    	if(!isValid){
		    		return;
		    	}
	    	}
	    	else {
	    		$scope.latePayment.lateFeesCharges = "0";
	    		$scope.latePayment.discountAmount = "0";
	    	}

	    	var latePayment = {
	    		feeScheduleIds: $scope.latePayment.feeScheduleIds,
	    		studentFeesList: $scope.latePayment.studentFeesArray,
	    		lateFeesCharges : $scope.latePayment.lateFeesCharges,
	    		discountAmount : ($scope.latePayment.discountAmount ? $scope.latePayment.discountAmount : undefined)
	    	};

			$resource('fees/markLatePayment').save(latePayment).$promise.then(function(resp) {
				$scope.$parent.filterTableByBranch($scope.$parent.branchId);
				$scope.$parent.message = "Remaining amount changed.";
				//$scope.latePayment = {};
				$scope.latePayment.lateFeesCharges = undefined;
	    		$scope.latePayment.discountAmount = undefined;
				$scope.latePayment.studentFeesArray.length = 0;
				$uibModalInstance.dismiss('cancel');
			});
		};

		$scope.cancel = function(){
	    	$uibModalInstance.dismiss('cancel');
	    };
	});


	angular.module('fees').controller('MarkFeesPaidController',function($rootScope,$scope,$filter, 
			$uibModalInstance,$state, $resource) {

//		$scope.objToMarkPaid = {};
		$scope.maxDate = new Date();
	    $scope.markAsPaidSubmit = function() {
	    	if($scope.objToMarkPaid.feeAmountToMarkPaid > (parseInt($scope.objToMarkPaid.remainingAmount + ""))){
	    		$scope.objToMarkPaid.remainingAmountError = "Amount cannot be greater than remaining amount.";
	    		return;
	    	}

	    	var feePayment = {
					feeId : $scope.feesIdToMarkAsPaid,
					studentId: $scope.studentId,
					amount : $scope.objToMarkPaid.feeAmountToMarkPaid,
					createdBy : $rootScope.id,
					paymentDate : $filter('date')($scope.objToMarkPaid.paymentDate, "yyyy-MM-dd"),
					paymentDetail : $scope.fees.paymentDetail,
					paymentReferenceDetail : $scope.fees.paymentReferenceDetail,

					feeScheduleId : $scope.objToMarkPaid.feeScheduleId,
					bankBranchName : $scope.fees.branchName,
					bankName : $scope.fees.bankName,
					ifscCode : $scope.fees.ifscCode,
					checkOrDDNumber : $scope.fees.checkNo,

					

			};

			$resource('fees/markFeePaid').save(feePayment).$promise.then(function(resp) {
				$scope.$parent.filterTableByBranch($scope.$parent.branchId);
				$scope.$parent.message = "Fees marked as paid.";
				$uibModalInstance.dismiss('cancel');
			});
		};
				
		$scope.cancel = function(){
	    	$uibModalInstance.dismiss('cancel');
	    };
	});
})();
