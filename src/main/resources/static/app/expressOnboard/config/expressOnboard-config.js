(function(){
	angular.module('expressOnboard').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('expressOnboard', {
			url : '',
			abstract :true,
			controller : 'ExpressOnboardModelController',
		}).state('expressOnboard.page', {
			url : '/expressOnboard/page',
			views : {
				'@' : {
					templateUrl : 'app/expressOnboard/html/expressOnboard-page.html',
					controller : 'ExpressOnboardController'
				}
			},
			data: {
	            displayName: 'About Us'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	},
	        	BoardList : function($resource){
		        	return $resource('masters/boards').query().$promise;
		        }
	        }
		});
	}]);
})();