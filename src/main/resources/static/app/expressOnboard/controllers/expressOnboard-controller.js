(function() {
	angular.module('expressOnboard').controller('ExpressOnboardController',

	function($resource, $scope, $state, $uibModal,$uibModalStack, CommonServices, Upload, StateList, BoardList, $filter, modalPopupService,$http,$timeout) {
		$scope.expressOnboard = {};
		$scope.holidayList = [];
		$scope.instituteList = [];
		$scope.branchList = [];
		$scope.instituteId = {};
		$scope.institute = {logo:{},errorMessage:{}}
		$scope.branch = {admin:{},isSameAsInstituteAdmin:false,logo:{},instituteType:null};
		$scope.branchId = null;
		$scope.predefinedData = {};
		$scope.stateList = StateList;
		$scope.districtList = [];
		$scope.talukaList = [];
		$scope.subjectsList = [];
		$scope.holiday={};
		$scope.selectedHoliday = [];
		$scope.stdDiv = {};
		$scope.stdDiv.divisionList = [];
		$scope.stdDiv.standardList = [];
		$scope.selectedStdDivList = [];
		$scope.academicYear = {};
		$scope.isCurrenAYActive = 'true';
		$scope.currentAcademicyear = {};
		$scope.nextAcademicyear = {};
		$scope.workingday = {};
		$scope.displayHeads = [];
		$scope.latefees = [];
		$scope.feescodes = [];
		$scope.feesheads = [];
		$scope.standardSaved = false;
		$scope.savedFeesHeads = false;
		$scope.savedDisplayHeads = false;
		$scope.toBeRemovedFeesHeads = [];
		$scope.toBeRemovedDisplayHeads = [];
		$scope.toBeRemovedFeesCodes = [];
		$scope.toBeRemovedLateFees = [];
		$scope.academicyearMsglabel = null;
		$scope.instituteHolidays = [];
		$scope.studentUploadErr = null;
		$scope.student = {dataAvailable:'N'};
		$scope.getInstituteData = []
		$scope.selectedSubjects = [];
		$scope.selectedStandardForSubjects = [];
		$scope.showUniversity = true;
		$scope.standardsToRemove = [];
		$scope.selectedDivLists = {}; 
		$scope.isStdSaved = false;
		$scope.isShowTable = false;
		$scope.standardsList = $scope.predefinedData.standards
		
		$scope.selectedStdSubList = [];
		$scope.selectedSubjects = [];
		$scope.tempStdSub = [];
		$scope.subjectId;
		$scope.listOfSubjcts = {subjects: []};
		var standerd = {}
		var subjects = [];
		$scope.formData = {};
		$scope.rowWiseData = [];
		$scope.rowItems = [];
		$scope.yearList = [];
		var count = 0;
		$scope.subjecsToRemove = [];
		$scope.savedStdSubject = false;
		$scope.isCourseListAvailable  = false;
		
		//to open displaytemplate popup
		$scope.uploadTemplate = function(){
			var options = {
	  	    	      templateUrl: 'app/displayTemplate/html/display-template-upload-popup.html',
	  	    	      controller: 'DisplayTemplateUploadController',
	  	    	      scope : $scope
	  	    	    };
	  	    	  modalPopupService.openPopup(options);
	     };
	     //to download displaytemplate
	     $scope.downloadDisplayTemplate = function (){
				if($scope.branchId){
								var url = "displayTemplate/download?branch_id="+$scope.branchId;
					document.location.href = url;
				}
			}
	     //to open fee-popup
	     $scope.openUploadFeesDialog = function(){
		    	$scope.isUploadUpdate = false;
		    	var options = {
		    			templateUrl : 'app/fees/html/fees-bulk-upload.html',
		    			controller : 'FeesFileUploadController',
		    			scope : $scope
		    	  };
		    	  modalPopupService.openPopup(options);
		     };
	     

		function displayNameUnique (array) {
		    var a = array.concat();
		    for(var i=0; i<a.length; ++i) {
		        for(var j=i+1; j<a.length; ++j) {
		            if(a[i].displayName === a[j].displayName)
		                a.splice(j--, 1);
		        }
		    }
		    return a;
		}

		function nameUnique (array) {
			var a = array.concat();
			for(var i=0; i<a.length; ++i) {
				for(var j=i+1; j<a.length; ++j) {
					if(a[i].name === a[j].name)
						a.splice(j--, 1);
				}
			}
			return a;
		}
		
		$scope.getInstituteButtonText = function(){
			return $scope.institute.id == undefined ? 'Save':'Update';
		}
		
		$scope.getBranchButtonText = function(){
			return $scope.branchId == undefined ? 'Save':'Update';
		}
		
		$scope.getAcademicyearButtonText = function(){
			return $scope.academicYear.id == undefined ? 'Save':'Update';
		}
			
		$scope.toggleBranchAdminDetail = function(isSame){
			$scope.branch.isSameAsInstituteAdmin = isSame;
		}
		
		$scope.toggleBranchAdminLogo = function(isSame){
			$scope.branch.isSameAsInstituteLogo = isSame;
		}
		
		$scope.onSelectCurrentAcademicYear = function(){
			$scope.isCurrenAYActive = 'true';
			$scope.nextAcademicyear = {};
		}
		
		$scope.onSelectNextAcademicYear = function(){
			$scope.isCurrenAYActive = 'false';
			$scope.currentAcademicyear = {};
		}
		
		$scope.shouldDisableCurrentAY = function(){
			return $scope.isCurrenAYActive !== 'true';
		}
		
		$scope.shouldDisableNextAY = function(){
			return $scope.isCurrenAYActive == 'true';
		}
		
		$scope.getBranchAdminNgModel = function(){
			return $scope.branch.isSameAsInstituteAdmin ? $scope.institute : $scope.branch.admin;
		}
		
		$scope.getBranchLogoNgModel = function(){
			return $scope.branch.isSameAsInstituteLogo ? $scope.institute :$scope.branch;
		}
		
		$scope.disableFeesTempOptions = function(){
			return !$scope.savedFeesHeads;
		}
		
		$scope.disableDisplayTempButton = function(){
			return !$scope.savedDisplayHeads;
		}
		
		$scope.getFeesDetailsSaveButtonText = function(){
			return !$scope.savedFeesHeads ? 'Save':'Update';
		}
		
		$scope.disableFeesDetailsSaveButton = function(){
			return $scope.feesheads.length == 0;
		}
		
		$scope.getStdDivButtonText = function(){
			return $scope.standardSaved ? "Update" : "Save";
		}
		
		$scope.getStdSubButtonText = function(){
			return $scope.savedStdSubject ? "Update" : "Save";
		}

		$scope.selectedDivisions = [];

		// Add The Standar Divistion
		
		function concatCourseAndDiv(){
			var selectedCourseList = $scope.stdDiv.courseList;
			var selectedDivisions = $scope.stdDiv.divisionList;
			var courseDivisions = [];
			if(selectedDivisions != undefined && selectedDivisions!=null && selectedDivisions.length > 0){
				for(var i=0;i<selectedCourseList.length;i++){
					var course = selectedCourseList[i];
					for(var j=0;j<selectedDivisions.length;j++){
						var div = selectedDivisions[j];
						
						var courseDiv = course.name + "-" + div.text;
						courseDivisions.push({name:courseDiv});
					}
					
				}
				return courseDivisions;
			}else{
				return $scope.stdDiv.courseList;
			}
		}
		
		$scope.addStdDiv = function () {
			console.log($scope.branchId);
			if($scope.stdDiv.standardList.length > 0){
				$scope.isShowTable = true;
				$scope.yearMessage  = "";
				if($scope.branch.instituteType == 'SCHOOL'){
				
						angular.forEach($scope.stdDiv.standardList, function(value,key){
							var standard = value.name;
							var stdDivisions = $scope.selectedDivLists[standard];
							if(stdDivisions == undefined || stdDivisions == null){
								$scope.selectedDivLists[standard] = $scope.stdDiv.divisionList; 
							}else{
								var selectedStdDivs = $scope.stdDiv.divisionList;
								for(var i =0; i< selectedStdDivs.length;i++){
									var div = selectedStdDivs[i];
									var isExists = false;
									for(var j =0;j<stdDivisions.length;j++){
										if(stdDivisions[j].text == div.text){
											isExists = true;
											break;
										}	
									}
									if(!isExists){
										$scope.selectedDivLists[standard].push(div);
									}
								}
							}
						});
				} else if($scope.branch.instituteType == 'COLLEGE'){
					
					var courseDivList = concatCourseAndDiv();
					angular.forEach($scope.stdDiv.standardList, function(value,key){
						var standard = value.name;
						var stdDivisions = $scope.selectedDivLists[standard];
						if(stdDivisions == undefined || stdDivisions == null){
							$scope.selectedDivLists[standard] = courseDivList; 
						}else{
							for(var i =0; i< courseDivList.length;i++){
								var div = courseDivList[i];
								var isExists = false;
								for(var j =0;j<stdDivisions.length;j++){
									if(stdDivisions[j].name == div.name){
										isExists = true;
										break;
									}	
								}
								if(!isExists){
									$scope.selectedDivLists[standard].push(div);
								}
							}
						}
					})
				
				}
				
				angular.forEach($scope.predefinedData.standards, function(value,key){
					value.ticked = false;
				});
				
				if($scope.branch.instituteType == 'COLLEGE'){				
					angular.forEach($scope.predefinedData.divisions, function(value,key){
						value.ticked = false;
					})
				}
				
				$scope.stdDiv.standardList = [];
				$scope.stdDiv.divisionList = [];
			} else {
				$scope.yearMessage = "Please select first"
			}
		}
	
		$scope.removeStdDiv = function(index,std){
			$scope.standardsToRemove.push(std);
			
			delete $scope.selectedDivLists[std];
		}
		
		$scope.saveStdDiv = function (){
			
			var standards = []; 
			
			angular.forEach($scope.selectedDivLists, function(value,key){
				standards.push({
					name:key,
					divisionList:value
				});
			});
			
			var stdDiv = {
					standards : standards,
					standardsToRemove  : $scope.standardsToRemove 
			}
			
			var uri = 'standard/express?ayId='+$scope.academicYear.id + "&branchId="+$scope.branchId;
			
			saveData(uri, stdDiv, function (resp) {
				$scope.stdDivMsglabel = resp.data.message;
	        	if(resp.data.status == 'success'){
	        		$scope.standardSaved = true;
	        		$scope.selectedStdDivList = resp.data.lastInsertedStandrads;
	        		$scope.standardsForSub = $scope.selectedStdDivList;
	        		$scope.disable.stdDiv = true;
	        		$scope.isStdSaved = true;
	        		setTimeout (function (){
	        			toggleView('stdDiv', 'subject');
	        			$scope.stdDivMsglabel = "";
	        		}, 900)
	        		return resp;
	        	}
	        	else{
	        		showErrors(resp);
	        	}
			}, function (err){
				$scope.stdDivMsglabel = "Problem while Standard divisions";
			});
		}

		$scope.standardsForSub = [];
		var years;
		var courses;
		var divisions;
		var standards;
		
		function getCollegeYears(){
			
			$resource('college/years').query().$promise.then(function(collegeYears) {
				years = collegeYears;
				getCollegeCourses();
			});
		}
		
		function getCollegeCourses(){

			$resource('college/courses').query().$promise.then(function(collegeCourses) {
				courses = collegeCourses;
				getStandardList();
			});
		}
		function getStandardList(){

			$resource('standard/list').query().$promise.then(function(standards) {
				standards = standards;
				$scope.standards = standards
				$scope.getAllList = true;
				instituteTypeChanges();
			//	$scope.instituteTypeChanged();
			});
		}
		
		$scope.addStdSub = function () {
			
			var std =	$scope.selectedStdDivList
			var subjects = $scope.selectedSubjects;
			var stds =	$scope.selectedStdDivList
			//var subjects =	selectedSubject;
			//var stds = $scope.selectedStandardForSubjects;
			
			var tempStdSubjects = [];
			
			for(var i=0;i<stds.length;i++){
				var std = stds[i];
				var stdSubjects = [];
				for(var j=0;j<subjects.length;j++){
					var subject = subjects[j];
					stdSubjects.push(subject)
				}
				tempStdSubjects.push({standard:std,subjects:stdSubjects});
			}
			$scope.tempStdSub.push(tempStdSubjects);
			console.log($scope.tempStdSub);
		}

		
		$scope.addNewRow = function() {
			var years = [];
			var courses = [];
			 if($scope.isCourseListAvailable == true){
			    	//var rowItems = $scope.selectedDivLists;
				  var selectedDivListsKeys = Object.keys($scope.selectedDivLists);
				  var rowlength = selectedDivListsKeys.length;
				  	angular.forEach($scope.selectedDivLists , function(value,key){
						years.push(key);
						courses.push(value);
					});
				  	for(var i =0; i < rowlength ; i++){
			    	 	var currentIndex = 0;
			    	 	$scope.rowItems.push({
					    	'id':'rows'+(++currentIndex),
					    	'name':years[i],
					    	'selectedCourses' : courses[i],
					    	years:years,
					    	courses:courses
					    });
			    	}
				  	
				  	$scope.isCourseListAvailable =false;
			 }else {
				var currentIndex = $scope.rowItems.length; 
		    	angular.forEach($scope.selectedDivLists , function(value,key){
					years.push(key);
					courses = value;
				});
		    	$scope.rowItems.push({
			    	'id':'rows'+(++currentIndex),
			    	years:years,
			    	courses:courses
			    });
			 }
		   
		};
		
		$scope.removeRow = function(index) {
				var subjects = [];
				var standard;
			    var lastItem = $scope.rowItems.length-1;
			    $scope.rowItems.splice(index,1);
			    if($scope.rowWiseData[index] != undefined || $scope.rowWiseData[index] != null){
			    	$scope.subjecsToRemove.push($scope.rowWiseData[index]);
					$scope.rowWiseData.splice(index,1);
				 }
				console.log($scope.rowWiseData);
				console.log($scope.subjecsToRemove);
		};

		// by Amit
		$scope.onSelectYear = function(index,yearName){
			
			var year = $scope.rowWiseData[index];
			if(year == undefined || year == null){
				$scope.rowWiseData.push({
					name:yearName,
					courses:[],
					subjects:[]
				});
			}else{
				year.name = yearName
				$scope.rowWiseData[index] = year;
			}
		}
		
		$scope.onSelectCourse = function(index,courseName){
			var courses = $scope.rowWiseData[index];
			if(courses == undefined || courses == null){
				$scope.rowWiseData.push({
					name:"",
					courses:[courseName],
					subjects:[]
				});
			}else{
				courses.courses.push(courseName);
			}
		}
		
		$scope.onRemoveCourse = function(index,courseName){
			
			var courseIndex = $scope.rowWiseData[index].courses.indexOf(courseName)
			$scope.rowWiseData[index].courses.splice(courseIndex,1);
		}
		
		$scope.onSelectSubject = function(index,subjectName){
			
//			var standard = $scope.formData[std];
//			if( (standard == undefined || standard == null) ){ // check whether key exist or not
//					
//					$scope.formData[std] = [{"id":subjectId,"name": subject, "tag": tag}];
//			} else{
//					$scope.formData[std].push({"id":subjectId,"name": subject, "tag": tag});
//			}
			var subjects = $scope.rowWiseData[index];
			
			if(subjects == undefined || subjects == null){
				$scope.rowWiseData.push({
					name:"",
					courses:[],
					subjects:[subjectName]
				});
			}else{
				subjects.subjects.push(subjectName);
			}
		}
		
		$scope.onRemoveSubject = function(index, subjectName){
			
//			var index = ($scope.formData[std]).findIndex(x => x.subject== subject)
//			//var index = ($scope.formData[std]).indexOf({"id":subjectId,"subject": subject, "tag": tag});
//			if(std in $scope.formData){
//				var  deletedSubject = ($scope.formData[std]).splice(index, 1);
//			}

			var subjectIndex = $scope.rowWiseData[index].subjects.indexOf(subjectName)
			$scope.rowWiseData[index].subjects.splice(subjectIndex,1);
		}
		
		$scope.saveStdSubject = function(){
			console.log($scope.branchId);
			var formData = {
					subjectsToAdd : $scope.rowWiseData,
					subjectsToRemove : $scope.subjecsToRemove
			}
			
				$scope.selectYearMessage = "";
				var uri =  "standardSubject/express?branchId="+$scope.branchId+"&ayId="+$scope.academicYear.id +"&type="+$scope.branch.instituteType;
				saveData(uri,formData, function (resp) {
					$scope.stdSubMsglabel = resp.data.message;
					if(resp.data.status == 'success'){
						$scope.savedStdSubject= true;
						$scope.disable.stdSub = true;
						$scope.selectedStdSubList = resp.data.lastInsertedStandrads;
						$scope.standardsForSub = $scope.selectedStdSubList;
						setTimeout (function (){
							toggleView('subject','Fees');
							$scope.stdSubMsglabel = "";
						}, 900)
					
						return resp;
					}
					else{
						showErrors(resp);
					}
				}, function (err){
					$scope.stdSubMsglabel = "Problem while creating standard subjects";
				});
			
		}
		
		$scope.saveStdSub = function (){
			var stdSub = {
					branchId:$scope.branchId,
					standards : $scope.selectedStdSubList,
					subjects : $scope.selectedSubjects
			}
			saveData('standardSubject/express?branchId=1', stdSub, function (resp) {
				$scope.stdSubMsglabel = resp.data.message;
				if(resp.data.status == 'success'){
					$scope.disable.stdSub = true;
					$scope.selectedStdSubList = resp.data.lastInsertedStandrads;
					$scope.standardsForSub = $scope.selectedStdSubList;
					setTimeout (function (){
						toggleView('subject');
						$scope.stdSubMsglabel = "";
					}, 900)
					return resp;
				}
				else{
					showErrors(resp);
				}
			}, function (err){
				$scope.stdSubMsglabel = "Problem while creating standard subjects";
			});
		}
		
		/*var standards = [{"displayName" : "Play Group", "qfixDefinedName" : "Play Group"}, {"displayName" :"Pre Nursery", "qfixDefinedName" : "Pre Nursery"}, {"displayName" :"Nursery","qfixDefinedName" : "Nursery"}, {"displayName" :"Junior KG","qfixDefinedName" : "Junior KG"}, {"displayName" :"Senior KG","qfixDefinedName" : "Senior KG"}, {"displayName" :"1","qfixDefinedName" : "1"}, {"displayName" :"2","qfixDefinedName" : "1"}, {"displayName" :"3","qfixDefinedName" : "1"}, {"displayName" :"4","qfixDefinedName" : "1"}, {"displayName" :"5","qfixDefinedName" : "1"}, {"displayName" :"6","qfixDefinedName" : "6"}, {"displayName" :"7","qfixDefinedName" : "7"}, {"displayName" :"8","qfixDefinedName" : "8"}, {"displayName" :"9","qfixDefinedName" : "9"}, {"displayName" :"10","qfixDefinedName" : "10"}, {"displayName" :"11","qfixDefinedName" : "11"}, {"displayName" :"12","qfixDefinedName" : "12"}];

		var years = [{"displayName" :"First Year", "qfixDefinedName" : "First Year"}, {"displayName" :"Second Year", "qfixDefinedName" : "Second Year"}, {"displayName" :"Third Year", "qfixDefinedName" : "Third Year"}, {"displayName" :"Fourth Year", "qfixDefinedName" : "Fourth Year"}, {"displayName" :"Fifth Year", "qfixDefinedName" : "Fifth Year"}, {"displayName" :"Final Year", "qfixDefinedName" : "Final Year"}];
*/
		var divisions = [{"displayName" :"A", "qfixDefinedName" : "A"}, {"displayName" :"B", "qfixDefinedName" : "B"}, {"displayName" :"C", "qfixDefinedName" : "C"}, {"displayName" :"D", "qfixDefinedName" : "D"}, {"displayName" :"E", "qfixDefinedName" : "E"}, {"displayName" :"F", "qfixDefinedName" : "F"}, {"displayName" :"G", "qfixDefinedName" : "G"}, {"displayName" :"H", "qfixDefinedName" : "H"}, {"displayName" :"I", "qfixDefinedName" : "I"}, {"displayName" :"J", "qfixDefinedName" : "J"}, {"displayName" :"K", "qfixDefinedName" : "K"}, {"displayName" :"L", "qfixDefinedName" : "L"}, {"displayName" :"M", "qfixDefinedName" : "M"}, {"displayName" :"N", "qfixDefinedName" : "N"}, {"displayName" :"O", "qfixDefinedName" : "O"}, {"displayName" :"P", "qfixDefinedName" : "P"}, {"displayName" :"Q", "qfixDefinedName" : "Q"}, {"displayName" :"R", "qfixDefinedName" : "R"}, {"displayName" :"S", "qfixDefinedName" : "S"}, {"displayName" :"T", "qfixDefinedName" : "T"}, {"displayName" :"U", "qfixDefinedName" : "U"}, {"displayName" :"V", "qfixDefinedName" : "V"}, {"displayName" :"W", "qfixDefinedName" : "W"}, {"displayName" :"X", "qfixDefinedName" : "X"}, {"displayName" :"Y", "qfixDefinedName" : "Y"}, {"displayName" :"Z", "qfixDefinedName" : "Z"}];

	/*	var courses = [{"displayName" :"Arts", "qfixDefinedName" : "Arts"}, {"displayName" :"Commerce", "qfixDefinedName" : "Commerce"}, {"displayName" :"Science", "qfixDefinedName" : "Science"}, {"displayName" :"BCA", "qfixDefinedName" : "BCA"}, {"displayName" :"MCA", "qfixDefinedName" : "MCA"}, {"displayName" :"Computer", "qfixDefinedName" : "Computer"}, {"displayName" :"Information Technology", "qfixDefinedName" : "nformation Technology"}, 
					   {"displayName" :"Mechanical", "qfixDefinedName" : "Mechanical"}, {"displayName" :"Electronics", "qfixDefinedName" : "Electronics"}, {"displayName" :"Civil"}, {"displayName" :"Chemical", "qfixDefinedName" : "Chemical"}];
*/
		$scope.predefinedData.subject = [{"name" :"English"}, {"name" :"Math"}, {"name" :"Hindi"}, {"name" :"French"}, {"name" :"Botany"}, {"name" :"Biology"}];
		$scope.predefinedData.subjectSize = $scope.predefinedData.subject.length;
		$scope.predefinedData.standardHeaderLabel = "Standard / Division";
		
		$scope.addCustom = function (element){
			if(element){
				$scope.elementForCustomName = element;
				modalPopupService.openPopup({
	    	      templateUrl: 'app/expressOnboard/html/custom-element-popup.html',
	    	      controller: 'ExpressOnboardCustomDataController',
	    	      scope : $scope
	    	    });
			}
		}

		$scope.setSelected = function (currElement){
			if($scope.holidayList.length == 0){
				$resource('holiday/publicHolidays').query().$promise.then(function(holidayList) {
					$scope.holidayList = holidayList;
				});
			}
			
			if($scope.instituteList.length == 0){
				$resource('institute/expressSessionInstitutes').query().$promise.then(function(response) {
					$scope.instituteList = response;
					//getInstituteDetails(false);
				});
			}
		}
		
		$scope.getBranchList = function(instituteId){
			//if($scope.branchList.length == 0){
				$resource('branch/expressSessionBranches/'+instituteId).query().$promise.then(function(response) {
					$scope.branchList = response;
					//getInstituteDetails(false);
				});
			//}
		}
		
		// Mainly used when login user is super admin.
		$scope.getInstituteDetails = function(instituteId){
			getInstituteDetails(instituteId);
			$scope.getBranchList(instituteId) 
		};

		function getInstituteDetails(instituteId){
			console.log($scope.instituteId.id);
		//	$scope.getInstituteData.length = 0;
			if(instituteId){
				$http.get('institute/expressSessionInstituteDetail/'+instituteId).then(function(instituteData) {
						console.log(instituteData)
						//$scope.institute = instituteData.data
						angular.extend($scope.institute ,instituteData.data);
						
				},function(error){
					console.log(error);
				});	
			}
		};
		$scope.getSelectedBoardUniversityDetails = function(boardUniversityId){
			
			$scope.branch.boardUniversityId = boardUniversityId;
		}
		
		// Get Branch Deatail Data on Branch selection 
		$scope.getBranchDetails = function(branchId){
			
			getBranchDetails(branchId);
		};
		
		

		// Get Data on Branch Selection
		function getBranchDetails(branchId){
			
		//	$scope.getInstituteData.length = 0;
			if(branchId){
				$http.get('branch/expressSessionBranchDetail/'+branchId).then(function(branchData) {
						
						//$scope.institute = instituteData.data
						
						angular.extend($scope.branch ,branchData.data.branch);	
						$scope.branch.addressLine1  =$scope.branch.addressLine;
						$scope.branch.addressLine2  =$scope.branch.area;
						$scope.branch.addressLine3  =$scope.branch.city;
						$scope.branch.boardAfiliation = $scope.branch.boardName;
						$scope.branchId = branchId;
						$scope.branch.stateId = $scope.branch.stateId
						$scope.branch.isSameAsInstituteAdmin = $scope.branch.branchAdminList[0];
						$scope.currentAcademicyear = branchData.data.academicYear;
						$scope.academicYear = branchData.data.academicYear;
						$scope.workingday = branchData.data.workingDay;
						
						
						$scope.branch.isPayDirectEnabled = $scope.branch.isPayDirectEnabled == 'Y' ? true : false;
						$scope.branch.offlinePaymentsEnabled = $scope.branch.offlinePaymentsEnabled == 'Y' ? true : false;
						
						if($scope.workingday !== undefined && $scope.workingday !== null){							
							$scope.workingday.monday = $scope.workingday.monday == 'Y' ? true : false;
							$scope.workingday.tuesday = $scope.workingday.tuesday == 'Y' ? true : false;
							$scope.workingday.wednesday = $scope.workingday.wednesday == 'Y' ? true : false;
							$scope.workingday.thursday = $scope.workingday.thursday == 'Y' ? true : false;
							$scope.workingday.friday = $scope.workingday.friday == 'Y' ? true : false;
							$scope.workingday.saturday = $scope.workingday.saturday == 'Y' ? true : false;
							$scope.workingday.sunday = $scope.workingday.sunday == 'Y' ? true : false;
						}
						
						$scope.feesheads = branchData.data.heads;
						$scope.displayHeads = branchData.data.displayHeads;
						$scope.feescodes = branchData.data.feeCodes;
						$scope.latefees = branchData.data.lateFees;
						$scope.student.uniqueidentifierdisplayname = $scope.branch.uniqueIdentifierLabel;
						$scope.student.uniqueIdentifier = $scope.branch.uniqueIdentifierName;
						
						if($scope.feesheads !== undefined && $scope.feesheads !== null && $scope.feesheads.length > 0){
							$scope.savedFeesHeads = true;
							$scope.savedDisplayHeads = true;
						}
						
						$scope.branch.holidays;
						
						// For Course /Standard
						$scope.stdData = branchData.data.standards;
						console.log($scope.branch.instituteType)
						getCourseList();
						getTalukaList();
						$scope.instituteTypeChanged();
						
						if($scope.branch.instituteType === 'SCHOOL'){
							$scope.getSelectedBoardUniversityDetails($scope.branch.boardAffiliation);
						}else if($scope.branch.instituteType === 'COLLEGE'){
							$scope.getSelectedBoardUniversityDetails($scope.branch.university);
						}
						
				},function(error){
					console.log(error);
				});	
			}
		};
		
		function getCourseList(){
			if($scope.branch.instituteType != null){
				$scope.tableMessage = "";
				if($scope.stdData.length > 0)
				{
					$scope.isStdSaved = true;
					console.log($scope.stdData);
						angular.forEach($scope.stdData ,function(value, key){
							angular.forEach(value.divisionList,function(res, key){
								if($scope.selectedDivLists[value.name] == undefined || $scope.selectedDivLists[value.name] == null){
									$scope.selectedDivLists[value.name] = [{"name": res.name, "text":res.text}];
								} else {
									$scope.selectedDivLists[value.name].push({"name":res.name,"text":res.text}); 
								}
							})
						})
						console.log($scope.selectedDivLists);
						$scope.isShowTable = true;
						$scope.isCourseListAvailable = true;
						$scope.addNewRow();
						
				}
			} else if($scope.branch.instituteType == null){
				$scope.isShowTable =false;
				$scope.tableMessage = "Please select the Insitute Type in Branch Section"
			}
		}
		
		function setPublicHolidays(instituteHolidays){
			for(var i=0;i<instituteHolidays.length;i++){
				var holiday = instituteHolidays[i];
				var isPublicHoliday = (holiday.isPublicHoliday === 'Y')?true:false;
				if(isPublicHoliday){
					checkPublicHoliday(holiday)
					$scope.selectedHolidays.push(holiday);
				}else{
					$scope.instituteHolidays.push(holidys);
				}
			}
		}
		
		function checkPublicHoliday(holiday){
			var publicHolidays = $scope.holidayList;
			for(var i=0;i<publicHolidays.length;i++){
				var publicHoliday = publicHolidays[i];
				
				if((publicHoliday.title === holiday.title) && 
						(publicHoliday.fromDate === holiday.fromDate) && (publicHoliday.toDate === holiday.toDate)){
					$scope.holidayList[i].checked=true;
				}
			}
		}
		
		
		$scope.setSelected();	

		$scope.getInstitutes = function(){
			listSubjects();
			$scope.getBoradUniverityList();
		}
		
		
		$scope.instituteTypeChanged = function (){
			getCollegeYears();
			$scope.getInstitutes();
			$scope.getBoradUniverityList();
			$scope.isShowTable = true;
			$scope.rowItems = [];
			$scope.isStdSaved  ==false;
			console.log($scope.branch.instituteType);
			getCourseList()
		}
		
		function instituteTypeChanges(){
			
			if($scope.branch.instituteType == 'SCHOOL'){
				$scope.predefinedData.divisions = divisions;
				$scope.predefinedData.standards = $scope.standards;
				$scope.predefinedData.standardSize = $scope.predefinedData.standards.length;
				$scope.predefinedData.divisionSize = $scope.predefinedData.divisions.length;
				$scope.predefinedData.standardHeaderLabel = "Standard / Division";
				$scope.predefinedData.standardLabel = "Standard";
				$scope.predefinedData.divisionLabel = "Division";
				$scope.showUniversity = false;
				$scope.showBoardAffilation = true;
				
			}
			else {
				$scope.predefinedData.divisions = courses;
				$scope.predefinedData.standards = years;
				$scope.predefinedData.standardSize = $scope.predefinedData.standards.length;
				$scope.predefinedData.divisionSize = $scope.predefinedData.divisions.length;
				$scope.predefinedData.standardHeaderLabel = "Course / Year";
				$scope.predefinedData.standardLabel = "Year";
				$scope.predefinedData.divisionLabel = "Course";
				$scope.showBoardAffilation = false;
				$scope.showUniversity = true;
			
			}
		}
		
		$scope.getBoradUniverityList = function(){
			
			var type = $scope.branch.instituteType;
			
			if(type == undefined || type == null){
				return;
			}
			
			if(type == 'SCHOOL')
				var url = '/school/boards'
			else if(type == 'COLLEGE')
				var url = '/college/universities';
			$resource(url).query().$promise.then(function(res) {
				$scope.boardList = res;
			});	
		}

		$scope.instituteTypeChanged ();

    	$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		
		function listSubjects(){
			
			var type = $scope.branch.instituteType;
			
			$resource('standardSubject/all?type='+type).query().$promise.then(function(subjects) {
				$scope.subjectsList =  subjects;
			});	
		}
		
		$scope.deleteInstituteHoliday = function(id){
			var holidays = $scope.instituteHolidays;
			for(var i=0;i<holidays.length;i++){
				var holiday = holidays[i];
				if(holiday.id == id){
					$scope.instituteHolidays.splice(i,1);
				}x	
			}
		}

		function changeDistrictList(){
			$scope.districtList.length = 0;
			if($scope.branch.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.branch.stateId})
					.$promise.then(function(districtList) {
						if(districtList != null && districtList.length > 0){
							$scope.branch.districtId = ($scope.branch.districtId  ? $scope.branch.districtId  : districtList[0].id);
	        			}
						angular.extend($scope.districtList, districtList);
						changeTalukaList();
				});	
			}
		};

		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
      function changeTalukaList(){
			
			if($scope.branch.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.branch.districtId})
					.$promise.then(function(talukaList) {
						$scope.talukaList.length = 0;
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};

		$scope.disable = {};

		function changeTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.branch.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.branch.districtId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.branch.talukaId = ($scope.branch.talukaId  ? $scope.branch.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};
		
		$scope.getTalukaByState = function(){
			getTalukaList();
		}
		
		function getTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.branch.stateId){
				$resource('masters/talukaMasters/state/:id').query({id : $scope.branch.stateId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.branch.talukaId = ($scope.branch.talukaId  ? $scope.branch.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		}
		
		
		$scope.validateInstituteLogo = function(){
			
			var logoUrl = $scope.institute.logoUrl;
			
			var hasError = false;
			var isValidLogoSizeValid = true;
			var isImage = true;
			
			if(logoUrl !== undefined && logoUrl !== null){
				isValidLogoSizeValid = logoUrl.size <= 5242880;
			}
			
			if(!isValidLogoSizeValid){
				hasError = true;
				$scope.institute.logo.invalid = true;
				$scope.institute.logo.errorMessage = "Logo size should not be more than 5MB";
				return hasError;
			}
			
			if(logoUrl !== undefined && logoUrl !== null){
				var type = logoUrl.type;
				var imageRegx = /^image\/[jpeg|png|jpg]*/g;
				isImage = type.match(imageRegx);
			}
			
			if(!isImage){
				hasError = true;
				$scope.institute.logo.invalid = true;
				$scope.institute.logo.errorMessage = "Selected file must be a valid jpg, jpeg or png file";
				return hasError;
			}
			
			if(!hasError){
				$scope.institute.logo.invalid = false;
				$scope.institute.errorMessage = "";
			}
			
			return hasError;
		}

		$scope.validateBranchLogo = function(){
			
			var logoUrl = $scope.branch.logoUrl;
			var hasError = false;
			var isValidLogoSizeValid = true;
			if(logoUrl !== undefined && logoUrl !== null){				
				isValidLogoSizeValid = logoUrl.size <= 5242880;
			}
			
			if(!isValidLogoSizeValid){
				hasError = true;
				$scope.branch.logo.invalid = true;
				$scope.branch.logo.errorMessage = "Logo size should not be more than 5MB";
				return hasError;
			}
			var isImage = true;
			if(logoUrl !== undefined && logoUrl !== null){				
				var type = logoUrl.type;
				
				var imageRegx = /^image\/[jpeg|png|jpg]*/g;
				
				isIamge = type.match(imageRegx);
			}
			
			if(!isImage){
				hasError = true;
				$scope.branch.logo.invalid = true;
				$scope.branch.logo.errorMessage = "Selected file must be a valid jpg, jpeg or png file";
				return hasError;
			}
			
			if(!hasError){
				$scope.branch.logo.invalid = false;
				$scope.branch.errorMessage = "";
			}
			
			return hasError;
		}
		
		$scope.saveInstitute = function (){
			var logoHasError = $scope.validateInstituteLogo();
			
			if(logoHasError){
				return;
			}
			
			var institute = {
					adminCity: $scope.institute.adminCity,
					adminEmail: $scope.institute.adminEmail,
					adminPinCode: $scope.institute.adminPinCode,
					firstName: $scope.institute.firstName,
					gender: $scope.institute.gender,
					id: $scope.institute.id,
					code : $scope.institute.code,
					instituteName: $scope.institute.instituteName,
					lastName: $scope.institute.lastName,
					primaryContact: $scope.institute.primaryContact,
					active: 'Y',
					isDelete: $scope.institute.isDelete
			};

			$scope.instituteMsglabel = "";
			uploadData('institute/save', institute,  $scope.institute.logoUrl, function (resp) {
				$scope.instituteMsglabel = resp.data.message;

	        	if(resp.data.status == 'success'){
	        		$scope.institute.id = resp.data.lastInsertedId;
	        		$scope.disable.institute = true;
	        		setTimeout (function (){
	        			toggleView('institute', 'branch');
	        			$scope.instituteMsglabel = "";
	        		}, 900)
	        		return resp;
	        	}
	        	else{
	        		showErrors(resp);
	        	}
			}, function (err){
				console.log(err)
				$scope.instituteMsglabel = "Problem while creating institute";
			});
		}
		
		$scope.saveBranch = function (){
			if($scope.institute.id){
				
				var isSameAsInstitute = $scope.branch.isSameAsInstituteLogo;
				if(!isSameAsInstitute){
					$scope.validateBranchLogo();
				}
				var logoUrl = (!isSameAsInstitute)?$scope.branch.logoUrl:$scope.institute.logoUrl;
				
//				var address = $scope.branch.addressLine1 + "\n" + $scope.branch.addressLine2 + "\n" + $scope.branch.addressLine3;
				
				var branch = {
						active: $scope.branch.active,
						addressLine: $scope.branch.addressLine1,
						area: $scope.branch.addressLine2,
						city: $scope.branch.addressLine3,
						contactEmail: $scope.branch.contactEmail,
						contactNumber: $scope.branch.contactNumber,
						districtId: $scope.branch.districtId,
						id: $scope.branchId,
						instituteId: $scope.institute.id,
						name: $scope.branch.name,
						pinCode: $scope.branch.pinCode,
						stateId: $scope.branch.stateId,
						talukaId: $scope.branch.talukaId,
						websiteUrl: $scope.branch.websiteUrl,
						branchAdminList : $scope.branchAdminList,
						boardId : $scope.branch.boardAfiliation,
						instituteType : $scope.branch.instituteType,
						principalName : $scope.branch.principalName,
						principalMobile :$scope.branch.principalMobile,
						principalEmailId :$scope.branch.principalEmailId,
						principalGender :$scope.branch.principalGender,
						offlinePaymentsEnabled : ($scope.branch.offlinePaymentsEnabled || $scope.branch.offlinePaymentsEnabled =="Y") ? "Y":"N",
						isPayDirectEnabled:($scope.branch.isPayDirectEnabled)?'Y':'N',
						showLogoOnParentPortal:'N',
						isExpressOnboard : 'Y',
						active : 'Y',
						instituteType : $scope.branch.instituteType
				};
				if($scope.branch.instituteType == 'COLLEGE')
					branch.university = $scope.branch.boardUniversityId
				else if($scope.branch.instituteType == 'SCHOOL')
					branch.boardAffiliation = $scope.branch.boardUniversityId;
				
				$scope.branchMsglabel  = "";
				uploadData('branch', branch,  logoUrl, function (resp) {
					$scope.branchMsglabel = resp.data.message;
		        	if(resp.data.status == 'success'){
		        		$scope.branchId = resp.data.lastInsertedId;
		        		$scope.disable.branch = true;
		        		setTimeout (function (){
		        			toggleView('branch', 'academicYear');
		        			$scope.branchMsglabel  = "";
		        		}, 900)
		        		return resp;
		        	}
		        	else{
		        		showErrors(resp);
		        	}
				}, function (err){
					$scope.branchMsglabel = "Problem while creating institute";
				});
			}
		}

		$scope.dayTypeChanged = function(){
			$scope.workingday.dayType = '';
		}
		
		$scope.saveAcademicyear = function (){
			var holidayList = $scope.holidayList.concat($scope.instituteHolidays);
			var tempHolidays = [];
			
			for(var i=0; i< holidayList.length; i++){
				var tempHoliday = holidayList[i];
				if(tempHoliday.checked){
					var fromDateArr = tempHoliday.fromDate.split("-");
					var toDateArr = tempHoliday.toDate.split("-");
					
					var fromDate = fromDateArr[2] + "-" + fromDateArr[1] + "-" + fromDateArr[0];
					var toDate = toDateArr[2] + "-" + toDateArr[1] + "-" + toDateArr[0];
					
					var holiday ={
						academicYearId : $scope.academicYear.id,
						fromDate : fromDate,
						toDate : toDate,
						title : tempHoliday.title,
						description : tempHoliday.description,
						branchId:$scope.branchId
						
					};
					tempHolidays.push(holiday);
				}
			}
			
			var currentAY = $scope.currentAcademicyear;
			var nextAY = $scope.nextAcademicyear;
			
			var ay;
			$scope.isCurrenAYActive = 'true';
			if((currentAY.fromDate == undefined && currentAY.toDate == undefined) 
					&& (nextAY.fromDate !== undefined && nextAY.toDate !== undefined)){
				ay = nextAY;
			}else if((currentAY.fromDate !== undefined && currentAY.toDate !== undefined) 
					&& (nextAY.fromDate == undefined && nextAY.toDate == undefined)){
				ay = currentAY;
			}else{
				$scope.academicyearMsglabel = "Please enter start and end date for an academic year";
				return;
			}

			var academicYear ={
					branchId:$scope.branchId,
 					academicYear:{
 						id : $scope.academicYear.id,
 						fromDate : $filter('date')(ay.fromDate, "yyyy-MM-dd"),
 						toDate : $filter('date')(ay.toDate, "yyyy-MM-dd"),
 						isCurrentActiveYear: "Y",
 						branchId:$scope.branchId
 					},
 					workingDay:($scope.workingday !== undefined && $scope.workingday !== null)?{
 						id : $scope.workingday.id,
 						monday : ($scope.workingday.monday == true || $scope.workingday.monday == 'Y' ? "Y" : "N"),
 						tuesday : ($scope.workingday.tuesday == true || $scope.workingday.tuesday == 'Y' ? "Y" : "N"),
 						wednesday : ($scope.workingday.wednesday == true || $scope.workingday.wednesday == 'Y' ? "Y" : "N"),
 						thursday : ($scope.workingday.thursday == true || $scope.workingday.thursday == 'Y' ? "Y" : "N"),
 						friday : ($scope.workingday.friday == true || $scope.workingday.friday == 'Y' ? "Y" : "N"),
 						saturday : ($scope.workingday.saturday == true || $scope.workingday.saturday == 'Y' ? "Y" : "N"),
 						sunday : ($scope.workingday.sunday == true || $scope.workingday.sunday == 'Y' ? "Y" : "N"),
 						branchId:$scope.branchId,
 						skipTimetableOnHoliday : ($scope.workingday.skipTimetableOnHoliday == true ? "Y" : "N"),
 						roundRobinTimetableDays: ($scope.workingday.skipTimetableOnHoliday == true ? $scope.workingday.noOfDaysInRoundRobin : 0)
 					}:null,
 					holidays:tempHolidays
 					
			};

			saveData('academicyear/express', academicYear, function (resp) {
				$scope.academicyearMsglabel = resp.data.message;
				$timeout(function(){
					$scope.academicyearMsglabel = '';
				},5000);
				
	        	if(resp.data.status == 'success'){
	        		$scope.academicyearMsglabel = 'Academic year saved successfully';
	        		$scope.academicYear.id = resp.data.academicYearId;
	        		$scope.workingday.id = resp.data.workingDayId;
	        		
	        		toggleView('academicYear', 'stdDiv');

	        		/*var workingDaysSelected = false;
	        		if($scope.workingday && ($scope.workingday.monday  || $scope.workingday.tuesday  || $scope.workingday.wednesday  || $scope.workingday.thursday 
	        			|| $scope.workingday.friday  || $scope.workingday.saturday  || $scope.workingday.sunday ) ){
	        			workingDaysSelected = true;
	        		}

        			if(workingDaysSelected){
        				saveWorkingDays();
        			}*/
        			/*else {
        				$scope.disable.academicYear = true;
        				setTimeout (function (){
    	        			toggleView('academicYear', 'stdDiv');
    	        			$scope.academicyearMsglabel  = "";
    	        		}, 900);
        			}*/
	        	}else{
	        		$scope.academicYear.id = resp.data.academicYearId;
	        		showErrors(resp);
	        	}
			}, function (err){
				$scope.academicyearMsglabel = "Couldn't save details, try again after sometime";
			});
		}
		//changes made by pitabas
		$scope.addHoliday = function (){
			var holiday = {
				"checked" : true,
				"id" : $scope.instituteHolidays.length + 2,
				"title" : $scope.holiday.title,
				"description" : $scope.holiday.description,
				"toDate" : $filter('date')($scope.holiday.toDate, "dd-MM-yyyy"),
				"fromDate" : $filter('date')($scope.holiday.fromDate, "dd-MM-yyyy")
			}
			
			$scope.holiday.toDate = null;
			$scope.holiday.fromDate = null;

			$scope.holiday.checked = true;
			$scope.holiday.id = $scope.instituteHolidays.length + 2;
			$scope.instituteHolidays.push(holiday);
		}

		$scope.selectHoliday = function (holiday){
			if(holiday.checked){
				$scope.selectedHoliday.push(holiday);
			}
			else if($scope.selectedHoliday.length > 0){
				for(var i =0; i < $scope.selectedHoliday.length; i++){
					var tempHoliday = $scope.selectedHoliday[i];
					if(holiday.id == tempHoliday.id){
						$scope.selectedHoliday.splice(i, 1);
						break;
					}
				}
			}
		}//changes made by pitabas

		$scope.saveHoliday = function (){
			var tempHolidays = [];
			for(var i=0; i< $scope.holidayList.length; i++){
				var tempHoliday = $scope.holidayList[i];
				if(tempHoliday.checked){
					var fromDateArr = tempHoliday.fromDate.split("-");
					var toDateArr = tempHoliday.toDate.split("-");
					
					var fromDate = fromDateArr[2] + "-" + fromDateArr[1] + "-" + fromDateArr[0];
					var toDate = toDateArr[2] + "-" + toDateArr[1] + "-" + toDateArr[0];
					
					var holiday ={
						academicYearId : $scope.academicYear.id,
						fromDate : fromDate,
						toDate : toDate,
						title : tempHoliday.title,
						description : tempHoliday.description,
						branchId : $scope.branchId
					};
					tempHolidays.push(holiday);
				}
			}
			
			
			saveData('holiday/multiple', tempHolidays, function (resp) {
				$scope.holidayMsglabel = resp.data.message;
	        	if(resp.data.status == 'success'){
	        		$scope.disable.holiday = true;
	        		setTimeout (function (){
	        			toggleView('holiday', 'feesCode');
	        			$scope.holidayMsglabel  = "";
	        		}, 900)
	        		return resp;
	        	}
	        	else{
	        		showErrors(resp);
	        	}
			}, function (err){
				$scope.holidayMsglabel = "Problem while saving holidays";
			});
		}
		
		/*late fee pop added */
		$scope.addLateFeesDetails = function addLateFeesDetails(){
			console.log("late fee call");
			
			var options = {
					 templateUrl: 'app/expressOnboard/html/latefeespop-up.html',
		    	      controller: 'ExpressOnboardLateFeesController',
		    	      scope : $scope
		    	    };
			modalPopupService.openPopup(options);
			
		}

		$scope.feesCodeList = [];
	
		$scope.addFeescode = function (){
			$scope.feesCodeMsgLabel = "";
			/*$scope.feesCode={};
			var feesCode = {
				name : $scope.feesCode.name,
				description : $scope.feesCode.description,
				branchId : $scope.branchId,
			};*/
			
			$scope.feesCode = {
					name : $scope.feesCode.name,
					description : $scope.feesCode.description,
					branchId : $scope.branchId,
				
				};

			var present = false;
			for(var i = 0; i < $scope.feesCodeList.length; i++){
				var tempFeesCode = $scope.feesCodeList[i];
				if(tempFeesCode.name == $scope.feesCode.name){
					present = true;
					break;
				}
			}
			if(!present){
				$scope.feesCodeList.push(feesCode);				
				$scope.feesCode.name = "";
				$scope.feesCode.description = "";
			}
			else {
				$scope.feesCodeMsgLabel = "Fees Code with this name already exist.";
			}
		}
		
		$scope.onAddFeesHead = function(tag){
			tag.name = tag.text;
			tag.active = "Y";
			tag.branchId = $scope.branchId;
			tag.academicYearId = $scope.academicYear.id;
			tag.isDelete = 'N';
			return true;
		}
		
		$scope.onAddDisplayHead = function(tag){
			tag.name = tag.text;
			tag.active = "Y";
			tag.branchId = $scope.branchId;
			tag.academicYearId = $scope.academicYear.id;
			tag.isDelete = 'N';
			return true;
		}
		
		$scope.onAddFeesCode = function(tag){
			tag.name = tag.text;
			tag.active = "Y";
			tag.branchId = $scope.branchId;
			tag.academicYearId = $scope.academicYear.id;
			tag.isDelete = 'N';
			return true;
		}
		
		$scope.onRemoveFeesHead = function(tag){
			if(tag.id !== undefined){
				$scope.toBeRemovedFeesHeads.push(tag);
				return true;
			}
		}
		
		$scope.onRemoveFeesCode = function(tag){
			if(tag.id !== undefined){				
				$scope.toBeRemovedFeesCodes.push(tag);
				return true;
			}
		}
		
		$scope.onRemoveDisplayHead = function(tag){
			if(tag.id !== undefined){	
				$scope.toBeRemovedDisplayHeads.push(tag);
				return true;
			}
		}
		
		$scope.onRemoveLateFees = function(tag){
			if(tag.id !== undefined){
				$scope.toBeRemovedLateFees.push(tag);
				return true;
			}
		}
		
		$scope.onChangeUniqueIdentifier = function(){
			$scope.student.uniqueidentifierdisplayname = null;
		}
		
		//console.log($scope.feescode.tags);
		$scope.saveFeesDetails = function (){
			
			if($scope.feesheads.length == 0){
				$scope.feesMsglabel = "To save fees details, at least one fees head is required";
				return;
			}
			
			var feesDetails = {
					branchId:$scope.branchId,
					deleteHeads:$scope.toBeRemovedFeesHeads,
					deleteDisplayHeads:$scope.toBeRemovedDisplayHeads,
					deleteFeesCodes:$scope.toBeRemovedFeesCodes,
					deleteLateFeesDeatil:$scope.toBeRemovedLateFees,
					heads:$scope.feesheads,
					displayHeads:$scope.displayHeads,
					feesCodes:$scope.feescodes,
					lateFeesDetails:$scope.latefees
			}
			var isUpdate = $scope.savedFeesHeads;
			saveData('fees/express-onboard/fees-details/'+$scope.branchId+"?update="+isUpdate, feesDetails, function (resp) {
				
	        	if(resp.data.status == 'success'){
	        		
	        		$scope.latefees = [];
	        		$scope.feescodes = [];
	        		$scope.feesheads = [];
	        		
	        		$scope.feesMsglabel = "Fees details added successfully";
	        		$scope.savedFeesHeads = resp.data.feeHeads.length > 0;
	        		$scope.savedDisplayHeads = resp.data.displayHeads.length > 0;
	        		
	        		$scope.feesheads = resp.data.feeHeads;
	        		$scope.displayHeads = resp.data.displayHeads;
	        		$scope.feescodes = resp.data.feeCodes;
	        		$scope.latefees = resp.data.lateFees;
	        		setTimeout (function (){
	        			$scope.feesMsglabel = "";
	        			toggleView('fees', 'student');
	        		}, 900)
	        		
	        		
	        	}
	        	else{
	        		$scope.feesMsglabel = resp.data.message;
	        		showErrors(resp);
	        	}
			}, function (err){
				$scope.feesMsglabel = "Problem while saving the fees details";
			});
		}
		
		
		$scope.saveLateFees = function (lateFeesDetail){
			
			var latefees ={
					description: lateFeesDetail.description,
					type :lateFeesDetail.type,
					amount :lateFeesDetail.amount,
					branchId:$scope.branchId,
					type: lateFeesDetail.type,
					frequency : lateFeesDetail.frequency,
					weekDays : lateFeesDetail.weekDays,
					byDate : lateFeesDetail.byDate,
					byDays : lateFeesDetail.byDays
				}
//			$scope.latefees.tags = lateFeesDetail.description;
			
			$scope.latefees.push(latefees)
			
			modalPopupService.closePopup(null, $uibModalStack);
			//$state.go('')
			
		}
		
		$scope.addFeesHead = function(){
			console.log(feesHead)
			return true;
		}
		
		$scope.cancelPopUp = function(){
			modalPopupService.closePopup(null, $uibModalStack);
		}
		
		
		$scope.saveFeesCode = function (id){
			console.log(id)
			saveData('fees/fees-code/multiple', $scope.feesCodeList, function (resp) {
				$scope.feesCodeMsglabel = resp.data.message;
	        	if(resp.data.status == 'success'){
	        		$scope.disable.feesCode = true;
	        		setTimeout (function (){
	        			toggleView('feesCode', 'student');
	        			$scope.feesCodeMsglabel  = "";
	        		}, 900)
	        		return resp;
	        	}
	        	else{
	        		showErrors(resp);
	        	}
			}, function (err){
				$scope.feesCodeMsglabel = "Problem while creating holidays";
			});
		}

		$scope.uniqueIdentifiers = [{name : 'Mobile', value : 'Mobile'}, {name : 'Registration Number', value : 'Registration Number'}, {name : "Email", value : 'Email'}, {name : "User Name", value : 'username'}];

		$scope.saveStudentConfig = function (){
			var config = {
				id : $scope.branch.configId,
				branchId : $scope.branchId,
				studentDataAvailable : ($scope.student.dataAvailable ? $scope.student.dataAvailable : 'N'),
				uniqueIdentifierName : $scope.student.uniqueIdentifier,
				uniqueIdentifierLable : $scope.student.uniqueidentifierdisplayname
			};

			saveData ('configuration/student', config, function (resp){
				$scope.studentConfigMsglabel = resp.data.message;
	        	if(resp.data.status == 'success'){
	        		$scope.disable.student = true;
	        		return resp;
	        	}
	        	else{
	        		showErrors(resp);
	        	}
			}, function (err){
				$scope.studentConfigMsglabel = "Problem while saving student configuration.";
			});
		}

		$scope.uploadStudents = function (){
			$scope.branchId = $scope.branchId;
			$scope.studentUploadErrMsg = "";
			$scope.isExpressOnboard = true;
			var options = {
    	      templateUrl: 'app/students/html/student-bulk-upload.html',
    	      controller: 'StudentFileUploadController',
    	      scope : $scope
    	    };
	    	modalPopupService.openPopup(options);
		}
		$scope.batchUploadStudents = function(){
	    	$scope.isUploadUpdate = false;
	    	var options = {
	    	      templateUrl: 'app/students/html/student-bulk-upload.html',
	    	      controller: 'StudentFileBatchUploadController',
	    	      scope : $scope
	    	    };
	    	modalPopupService.openPopup(options);
	     };

	     $scope.checkUploadStatus = function(){
	    	$scope.isUploadStatus = false;
	    	var options = {
	    		templateUrl: 'app/students/html/upload-status.html',
    	      	controller: 'StudentUploadStatusController',
    	      	scope : $scope
    	    };

	    	$scope.$emit("CallstatusUpload", {});
	    	modalPopupService.openPopup(options);
	     };
	     angular.module('student').controller('StudentFileBatchUploadController',function($scope,Upload, $uibModalInstance, $uibModalStack, modalPopupService) {
	 		$scope.uploadPopupTitle = " Batch Upload Student Details";
	 		if($scope.$parent.isUploadUpdate){
	 			$scope.uploadPopupTitle = "Bulk Batch Upload Student Details";
	 		}
	 		$scope.upload = function (file) {
	 	        Upload.upload({
	 	            url: 'students/' + ($scope.$parent.isBatchUpload ? 'BatchUploadFile' : 'batchuploadFile'),
	 	            data : {branchId : $scope.branchId},
	 	            file: file
	 	        }).then(function(resp) {
	 	        	if(resp.data.status == 'fail'){
	 	        		if(resp.data.isValidationError == 'true'){
	 	        			$scope.validationErrorMsg = resp.data.message;
	 	        			$scope.validationStatus = resp.data.status;
	 	        			$scope.records = resp.data.records;
	 	        			return;
	 	        		}
	 	        		$scope.$parent.isUploadingFailed = true;
	 	        	}
	 	        	$scope.$parent.status = resp.data.status;
	 	        	$scope.$parent.studentUploadErrMsg = resp.data.message;
	 	        	modalPopupService.closePopup(null, $uibModalStack);
	 	        	$scope.students.length = 0;
	 	        	angular.extend($scope.students,resp.data.records);
	 	        });
	 	    };

	     	$scope.cancel = function(){
	     		modalPopupService.closePopup(null, $uibModalStack);
	 	    };
	 	});

	 	angular.module('student').controller('StudentUploadStatusController',function($resource,$scope,$rootScope,Upload, $uibModalInstance, $uibModalStack, modalPopupService) {
	 		$scope.uploadstatusPopupTitle = "Upload Status Details";
	 		$scope.uploadJobs = [];
	 		$scope.showLogView = false;
	 		$scope.log = {};

	 		$scope.showLog = function (id){
	 			$resource('fileUpload/error/log?id='+id).get().$promise.then(
	 			 function(response) {
	 				 $scope.showLogView = true;
	 				 $scope.log.validationErrMsg = response.message;
	 				 $scope.log.errorRecords = response.records;
	 			});
	 		}

	 		$scope.goBack = function (){
	 			$scope.showLogView = false;
	 		}
	 		
	 		$scope.downloadFile = function (id){
	 			var url = 'fileUpload/error/download?id='+id;
	 			document.location.href = url;
	 		}

	 		function uploadStatus(){
	 			if($scope.branchId){
	 				$resource('fileUpload/status?branchId='+$scope.branchId).query().$promise.then(
	 				 function(response) {
	 					 $scope.uploadJobs = response;
	 				});
	 			}
	 		}
	 		uploadStatus();
	 		$scope.cancel = function(){
	 	    		modalPopupService.closePopup(null, $uibModalStack);
	 	    };

	 	});
	 	
	 	
	 	

	 	angular.module('student').controller('StudentFileBatchUploadController',function($scope,Upload, $uibModalInstance, $uibModalStack, modalPopupService) {
	 		$scope.uploadPopupTitle = " Batch Upload Student Details";
	 		if($scope.$parent.isUploadUpdate){
	 			$scope.uploadPopupTitle = "Bulk Batch Upload Student Details";
	 		}
	 		$scope.upload = function (file) {
	 			Upload.upload({
	 				url: 'fileUpload/' + ($scope.$parent.isBatchUpload ? 'studentUpdate' : 'student'),
	 				data : {branchId : $scope.branchId},
	 				file: file
	 			}).then(function(resp) {
	 				if(resp.data.status == 'fail'){
	 					if(resp.data.isValidationError == 'true'){
	 						$scope.validationErrorMsg = resp.data.message;
	 						$scope.validationStatus = resp.data.status;
	 						$scope.records = resp.data.records;
	 						return;
	 					}
	 					$scope.$parent.isUploadingFailed = true;
	 				}
	 				$scope.$parent.status = resp.data.status;
	 				$scope.$parent.studentUploadErrMsg = resp.data.message;
	 				modalPopupService.closePopup(null, $uibModalStack);
	 				$scope.students.length = 0;
	 				angular.extend($scope.students,resp.data.records);
	 			});
	 		};
	 		
	 		$scope.cancel = function(){
	 		modalPopupService.closePopup(null, $uibModalStack);
	 		};
	 	});

	 	$scope.selectedStdSubList = [];
		$scope.selectedSubjects = [];
		$scope.tempStdSub = [];
		$scope.subjectId;
		$scope.listOfSubjcts = {subjects: []};
		var standerd = {}
		var subjects = [];
		$scope.formData = {};
		$scope.rowWiseData = [];
		$scope.rowItems = [];
		$scope.yearList = [];
		var count = 0;
		$scope.subjecsToRemove = [];
		//$scope.coursesList = [];
		$scope.academicYear = {};
		
		

		$scope.removeFromArr = function (arr, index){
			if(arr.length > 0){
				arr.splice(index, 1);
			}
		}
		
		function toggleView(currentElement, nextElement){
			var currentDomElement = document.getElementById(currentElement);
			var nextDomElement = document.getElementById( nextElement);

			currentDomElement.classList.remove("in");
			if(nextDomElement){
				nextDomElement.classList.add("in");
			}

			var currentCicrleDomElement = document.getElementById(currentElement+"Circle");
			var nextCicrleDomElement = document.getElementById( nextElement+"Circle");
			
			currentCicrleDomElement.className = "circle green circleGroup";
			if(nextCicrleDomElement){
				nextCicrleDomElement.className = "circle blue circleGroup";
			}
		}

		function saveData(url, objToSave, successFunc, errorFunc){
			$resource(url).save(objToSave).$promise.then(function(resp) {
	        	if(!resp.data){
					resp.data = resp;
				}
	        	successFunc(resp);
	        }, function (err) {
	        	errorFunc (err);
	        });
		}

		function uploadData(url, objToSave, file, successFunc, errorFunc) {
			Upload.upload({
	            url: url,
	            method: 'POST',
                data : objToSave,
	            file: file,
	        }).then(function(resp) {
	        	if(!resp.data){
					resp.data = resp;
				}
	        	successFunc(resp);
	        }, function (err) {
	        	errorFunc (err);
	        });
		}

		function showErrors (resp){
			if(resp.data.isValidationError){
				$scope.validationErrMsg = resp.data.records.title;
				$scope.validationErrors = resp.data.records.errors;
				var options = {
					 templateUrl: "app/common/html/validationError.html",
		    	     controller: "ValidationErrorController",
		    	     scope : $scope
		    	 };
				 modalPopupService.openPopup(options);
			}
		}

		$scope.stdSub = {};
		$scope.selectedStdSubArr = [];

		$scope.addSubject=function(){
			
		}
	});

	/*changes made by pitabas*/
	angular.module('expressOnboard').controller('validateInstituteForm',function($resource,$scope,$state) {
		/*$scope.regExInstitute = /^[A-Z a-z , . () -]{2,100}$/;*/
		$scope.regExName = /^[A-Z a-z.`]{2,45}$/;
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,45}$/;
		$scope.regExCity = /^[A-Z a-z]{2,45}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExEmailAdmin = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExUrl = /^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$/;
		$scope.regExCode = /^[a-zA-Z0-9]{4,16}$/;
	});	
	/*changes made by pitabas*/

	angular.module('expressOnboard').controller('ExpressOnboardCustomDataController',function($resource,$scope,$state,modalPopupService, $uibModalStack,CommonServices) {
		$scope.data = {};
		var newQfixDefinedName = undefined;

		if($scope.$parent.elementForCustomName == 'standard'){
			$scope.customLabel = $scope.predefinedData.standardLabel;
		}
		else if($scope.$parent.elementForCustomName == 'division'){
			$scope.customLabel = $scope.predefinedData.divisionLabel;
		}
		else if($scope.$parent.elementForCustomName == 'subject'){
			$scope.customLabel = "Subject";
		}

		$scope.add = function (){
			var customElement = {
				displayName : $scope.displayName
			};
			if($scope.$parent.elementForCustomName == 'standard'){
				$scope.customLabel = "Standard";
				for(var i =0; i< $scope.predefinedData.standards.length; i++){
					var tempStd = $scope.predefinedData.standards[i];
					if(tempStd.displayName == $scope.displayName){
						$scope.errorMessage = "Standard by this name already exist.";
						return;
					}
				}
				var tempStd = $scope.predefinedData.standards[$scope.predefinedData.standards.length - $scope.predefinedData.standardSize];
				newQfixDefinedName = tempStd.qfixDefinedName;
				customElement.qfixDefinedName = tempStd.qfixDefinedName;
				$scope.predefinedData.standards.push(customElement);
			}
			else if($scope.$parent.elementForCustomName == 'division'){
				$scope.customLabel = "Division";
				for(var i =0; i< $scope.predefinedData.divisions.length; i++){
					var tempStd = $scope.predefinedData.divisions[i];
					if(tempStd.displayName == $scope.displayName){
						$scope.errorMessage = "Division by this name already exist.";
						return;
					}
				}
				var tempDiv = $scope.predefinedData.divisions[$scope.predefinedData.divisions.length - $scope.predefinedData.divisionSize];
				newQfixDefinedName = tempDiv.qfixDefinedName;
				customElement.qfixDefinedName = tempDiv.qfixDefinedName;
				$scope.predefinedData.divisions.push(customElement);
			}
			else if($scope.$parent.elementForCustomName == 'subject'){
				$scope.customLabel = "Subject";
				for(var i =0; i< $scope.predefinedData.subject.length; i++){
					var tempStd = $scope.predefinedData.subject[i];
					if(tempStd == $scope.displayName){
						$scope.errorMessage = "Subject by this name already exist.";
						return;
					}
				}
				customElement.name = $scope.displayName;
				$scope.predefinedData.subject.push(customElement);
			}
			modalPopupService.closePopup(null, $uibModalStack);
		}
		
		
	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };

	});
	
	
	/*expressOnboard lateFee PopUP controller*/
	
angular.module('expressOnboard').controller('ExpressOnboardLateFeesController',function($stateParams,$localStorage,$rootScope,$resource,$scope,$state,modalPopupService, $uibModalStack,CommonServices) {   
		
		$scope.data = {};
		$scope.lateFeesDetail = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.lateFeesDetailHeaderValue = "Add late Fees";

		$scope.lateFeesDetail.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.lateFeesDetail.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;


		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
    			changeBranchList(false);
    		
			angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.lateFeesDetail.instituteId || $scope.lateFeesDetail.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.lateFeesDetail.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);	
				});	
			}
		};
		
		$scope.cancel = function(){
			CommonServices.cancelOperation('lateFeesDetail.list');
		};
		$scope.dayArray = [{"label" : "Monday", "value" : "1"},
            {"label" : "Tuesday", "value" : "2"},
		    {"label" : "Wednesday", "value" : "3"},
		    {"label" : "Thursday", "value" : "4"},
		    {"label" : "Friday", "value" : "5"},
		    {"label" : "Saturday", "value" : "6"},
		    {"label" : "Sunday", "value" : "7"}
	    ]; 

	    $scope.monthDayArray = [{"label" : "1", "value" : "1"}, {"label" : "2", "value" : "2"},
		    {"label" : "3", "value" : "3"}, {"label" : "4", "value" : "4"},
		    {"label" : "5", "value" : "5"}, {"label" : "6", "value" : "6"},
		    {"label" : "7", "value" : "7"}, {"label" : "8", "value" : "8"},
		    {"label" : "9", "value" : "9"}, {"label" : "10", "value" : "10"},
		    {"label" : "11", "value" : "11"}, {"label" : "12", "value" : "12"},
		    {"label" : "13", "value" : "13"}, {"label" : "14", "value" : "14"},
		    {"label" : "15", "value" : "15"}, {"label" : "16", "value" : "16"},
		    {"label" : "17", "value" : "17"}, {"label" : "18", "value" : "18"},
		    {"label" : "19", "value" : "19"}, {"label" : "20", "value" : "20"},
		    {"label" : "21", "value" : "21"}, {"label" : "22", "value" : "22"},
		    {"label" : "23", "value" : "23"}, {"label" : "24", "value" : "24"},
		    {"label" : "25", "value" : "25"}, {"label" : "26", "value" : "26"},
		    {"label" : "27", "value" : "27"}, {"label" : "28", "value" : "28"},
		    {"label" : "29", "value" : "29"}, {"label" : "30", "value" : "30"},
		    {"label" : "31", "value" : "31"}
	    ];
	     // To check if either day is selected.
	   
	     $scope.isDaySelected = function(){
	    	 var flag = false;
	    	 if($scope.lateFeesDetail.weekDays){
	    		 angular.forEach($scope.lateFeesDetail.weekDays, function(isSelected, day) {
		             if(isSelected){
		            	 flag = true;
		             }
		         });	 
	    	 }
	    	 return flag;
	     };
	     
	     //To open fee-upload pop-up
//	     $scope.openUploadFeesDialog = function(){
//		    	$scope.isUploadUpdate = false;
//		    	var options = {
//		    			templateUrl : 'app/fees/html/fees-bulk-upload.html',
//		    			controller : 'FeesFileUploadController',
//		    			scope : $scope
//		    	  };
//		    	  modalPopupService.openPopup(options);
//		 };
		//to open displaytemplate
//		     $scope.downloadDisplayTemplate = function (){
//					if($scope.branchId){
//						var url = "displayTemplate/download?branch_id="+$scope.branchId;
//						document.location.href = url;
//					}
//				}
//		$scope.uploadTemplate = function(){
//			var options = {
//	  	    	      templateUrl: 'app/displayTemplate/html/display-template-upload-popup.html',
//	  	    	      controller: 'DisplayTemplateUploadController',
//	  	    	      scope : $scope
//	  	    	    };
//	  	    	  modalPopupService.openPopup(options);
//	     };
});   
 angular.module('displayTemplate').controller('DisplayTemplateUploadController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl', 'Upload',
  function($rootScope,$scope,$resource,modalPopupService,$uibModalStack,userserviceapiurl, Upload) {
	
	$scope.upload = function (file) {
        Upload.upload({
            url: 'displayTemplate/upload',
            data : {branchId : $scope.branchId},
            file: file
        }).then(function(resp) {
        	if(resp.data.status == 'fail'){
        		if(resp.data.isValidationError == 'true'){
        			$scope.validationErrorMsg = resp.data.message;
        			$scope.validationStatus = resp.data.status;
        			$scope.records = resp.data.records;
        			return;
        		}
        		$scope.$parent.isUploadingFailed = true;
        	}
        	modalPopupService.closePopup(null, $uibModalStack);
        	$scope.$parent.status  = resp.data.status;
        	$scope.$parent.message = resp.data.message;
       		angular.extend($scope.events,resp.data.records);
        });
    };

    $scope.cancel = function(){
    	modalPopupService.closePopup(null, $uibModalStack);
    };

}]);
})();