(function(){
	angular.module('teacher').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('teacher', {
			url : '/teacher',
			abstract :true,
			controller : 'TeacherModelController',
			data: {
				 breadcrumbProxy: 'teacher.list'
	        }
		}).state('teacher.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/teachers/html/teacher-list.html',
					controller : 'TeacherController'
				}
			},
			data: {
	            displayName: 'Teacher List'
	        },
	      /*  resolve : {
	        	TeacherList : function($resource){
	        		return $resource('teachers').query().$promise;
	        	}
	        }*/
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        	message : '',
	        	status : ''
	        }
		}).state('teacher.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/teachers/html/teacher-add.html',
					controller : 'TeacherAddController'
				}
			},
			data: {
	            displayName: 'Add Teacher'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	}
	        }
			
		}).state('teacher.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/teachers/html/teacher-add.html',
					controller : 'TeacherEditController'
				}
			},
			data: {
	            displayName: 'Edit Teacher'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	}
	        }
		});
	}]);
})();