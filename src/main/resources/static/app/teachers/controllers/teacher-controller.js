(function(){
	angular.module('teacher').controller('TeacherController',function($rootScope,$resource,$http,$confirm,
			$localStorage, DTOptionsBuilder,$stateParams,$scope,$state, modalPopupService,CommonServices) {
	    $scope.teachers = [];
	    $scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;

		$scope.instituteId = $localStorage.selectedInstitute;
		$scope.branchId = $localStorage.selectedBranch ;

	    $scope.branchList = [];
		$scope.instituteList = [];
		$scope.standardList = [];
		$scope.divisionList = [];		
		$scope.branchName = "";
		$scope.isFirstTimeLoaded = true;
		// DataTable sorting...

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", false)
		.withOption("paging", true)
		.withOption("searching", true)
        .withOption("aaSorting", [])
        .withOption("responsive", true)
        .withOption("scrollX", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};
		
		function getFilterObject(){
			var filter = {'instituteId' : $scope.instituteId,
	        	'branchId' : $scope.branchId,
	        	'standardId' : $scope.standardId,
	        	'divisionId' : $scope.divisionId,
		    };
			return filter;
		}

		/*function filterTableByBranch(branchId){
			if(branchId){
				$resource('teachers/getTeacherByBranch?branchId='+$scope.branchId).query().$promise.then(function(teacherList) {
					$scope.teachers.length = 0;
					if(teacherList && teacherList.length > 0){
						angular.extend($scope.teachers,teacherList);
					}					
	    		});
			}
		}*/
		
		function filterTableByBranch(branchId){
			if(branchId != null ){
				$http({
			        url: 'teachers/getTeacherByBranch',
			        method: "POST",
			        data: getFilterObject()
			    })
			    .then(function(response) {
			        console.log(response);
			        $scope.teachers.length = 0;
			        $scope.teachers = response.data;
			    },
			    function(err) { // optional
			    	console.log(err);
			    });
			}
			else {
				$scope.feesreports.length = 0;
			}
		}
		

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				//$scope.branchId = (!$stateParams.branchId ? branchList[0].id : $stateParams.branchId);
        				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
        				if($stateParams.branchId){
        				filterTableByBranch($scope.branchId);
        				}
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		//if(instituteList && $stateParams.instituteId){
	    			//$scope.instituteId = $stateParams.instituteId;
	    		$scope.instituteId = $stateParams.instituteId ? $stateParams.instituteId : $scope.instituteId;
	    		changeBranchList(false);
	    		
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isFirstTimeLoaded = false;
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
								//$scope.branchId = (!$scope.branchId ? branchList[0].id : $scope.branchId);
							$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);	
						
							changeStandardList();
							if($scope.isFirstTimeLoaded && !$scope.divisionId){
								filterTableByBranch($scope.branchId);
							}
							/*$scope.branchId = (isBranchChanged ? branchList[0].id  : 
								(!$stateParams.branchId ? branchList[0].id : $stateParams.branchId));
	        				if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}*/
							if($stateParams.branchId){
								filterTableByBranch($scope.branchId);
							}
						}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		
		$scope.changeStandardList = function(){
			$scope.isFirstTimeLoaded = false;
			$scope.status = "";
    		$scope.message = "";
			changeStandardList();
		};

		$scope.changeDivisionList = function(){
			$scope.isFirstTimeLoaded = false;
			$scope.status = "";
    		$scope.message = "";
			changeDivisionList();
		};
		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;
					console.log("standardList:: "+standardList);
					angular.extend($scope.standardList,standardList);
					changeDivisionList();//division
					
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
				if($scope.isFirstTimeLoaded && $scope.divisionId){
					filterTableByBranch($scope.branchId);
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}


	    /** This method is used to open Modal */ 
	    $scope.uploadTeachers = function(){
	    	  var options = {
	    	      templateUrl: 'app/teachers/html/teacher-bulk-upload.html',
	    	      controller: 'TeacherFileUploadController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	    	  
	     };
	     
	     /** This method is used to open Modal */ 
	     $scope.uploadTeacherSubjects = function(){
	     	  var options = {
	     	      templateUrl: 'app/teachers/html/teacher-subject-bulk-upload.html',
	     	      controller: 'TeacherSubjectFileUploadController',
	     	      scope : $scope
	     	    };
	     	  modalPopupService.openPopup(options);
	      };

	    /** This method is used to delete teacher from the screen. It will only call delete utility method */
	    $scope.deleteData = function deleteTeacher(id){
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
	        	 	$resource('teachers').remove({id : id}).$promise.then(function(resp) {
		        		if(resp.status == 'success'){
		        			for(var i =0; i < $scope.teachers.length ;i ++ ){
		      			  		var teacher = $scope.teachers[i];
		      			  		if(teacher.id == id){
		      			  			$scope.teachers.splice(i , 1);
		      			  			break;
		      			  		}
		      			  	}	
		        		}
		        		$scope.status = resp.status;
		        		$scope.message = resp.message;
	      			});
	         });
	     }; 
	});

	/*angular.module('teacher').controller('validateTeacherForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z]{2,45}$/;
		$scope.regExMiddleName = /^[A-Z a-z]{1,45}$/;
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,45}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExPassword =  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;
	});*/
	
	
	angular.module('teacher').controller('TeacherAddController',function($rootScope,$scope,$state,Upload, 
			$filter, $resource,$stateParams, modalPopupService,$localStorage, CommonServices, StateList) {
		$scope.teacher = {}; 
		$scope.isOpenStartDate = false;
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.teacherHeaderValue = "Add Teacher";
		$scope.teacher.teacherStandardList = [];
		$scope.stateList = StateList;
		$scope.districtList = [];
		$scope.talukaList = [];	

		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		};
		
		$scope.teacher.instituteId = $localStorage.selectedInstitute;
		$scope.teacher.branchId = $localStorage.selectedBranch;
		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);

		$scope.popup = function() {
			$scope.teacherStandardToEdit = {};
			var options = {
				templateUrl : 'app/teachers/html/teacher-popup.html',
				controller : 'TeacherStandardController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		$scope.editTeacherStandard = function(teacherStandard) {
			var teacherStdToEdit = JSON.stringify(teacherStandard);
			teacherStdToEdit = JSON.parse(teacherStdToEdit);
			$scope.teacherStandardToEdit = teacherStdToEdit;
			var options = {
				templateUrl : 'app/teachers/html/teacher-popup.html',
				controller : 'TeacherStandardController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		$scope.removeTeacherStandard = function(standardId, divisionId) {
			for ( var i = 0; i < $scope.teacher.teacherStandardList.length; i++) {
				var teacherStandard = $scope.teacher.teacherStandardList[i];
				if (teacherStandard.standardId == standardId) {
					var flag = false;
					if(divisionId){
						if (teacherStandard.divisionId == divisionId) {
							flag = true;
						}
					}
					else {
						flag = true;
					}
					if(flag){
						$scope.teacher.teacherStandardList.splice(i, 1);
					}
				}
			}
		};

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		
    		$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
    		.$promise.then(function(branchList) {
    			if(branchList){
    				$scope.branchId = (!$stateParams.branchId ? $scope.branchId : $stateParams.branchId);
    			}	
    				angular.extend($scope.branchList,branchList);
    			
    		});
    		
    		//$scope.teacher.instituteId = $rootScope.instituteId;
        	//changeBranchList();
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			//$scope.teacher.instituteId = instituteList[0].id;
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		$scope.branchChanged = function (){
			changeStandardList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.teacher.instituteId || $scope.teacher.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.teacher.instituteId})
					.$promise.then(function(branchList) {
						//if(branchList){
							//$scope.teacher.branchId = branchList[0].id;
	        			//}
						changeStandardList();
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.standardList = [];
		$scope.changeStandardList = function (){changeStandardList();};

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.teacher.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.teacher.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;
					angular.extend($scope.standardList,standardList);
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}
		
		$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		function changeDistrictList(){
			$scope.districtList.length = 0;
			if($scope.teacher.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.teacher.stateId})
					.$promise.then(function(districtList) {
						if(districtList != null && districtList.length > 0){
							$scope.teacher.districtId = ($scope.teacher.districtId  ? $scope.teacher.districtId  : districtList[0].id);
	        			}
						angular.extend($scope.districtList, districtList);
				});	
			}
		};
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		function changeTalukaList(){
			$scope.talukaList.length = 0;
			if($scope.teacher.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.teacher.districtId})
					.$promise.then(function(talukaList) {
						if(talukaList != null && talukaList.length > 0){
							$scope.teacher.talukaId = ($scope.teacher.talukaId  ? $scope.teacher.talukaId  : talukaList[0].id);
	        			}
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};

		/** This method is used to adding teacher. It will only call add utility method */
		$scope.save = function() {
			
			var teacher = {
					id: $scope.teacher.id,
					firstName: $scope.teacher.firstName,
					middleName: $scope.teacher.middleName,
					lastName: $scope.teacher.lastName,
					dateOfBirth : $filter('date')($scope.teacher.dateOfBirth, "yyyy-MM-dd"),
					emailAddress : $scope.teacher.emailAddress,
					gender : $scope.teacher.gender,
					primaryContact:$scope.teacher.primaryContact,
					secondaryContact:$scope.teacher.secondaryContact,
					addressLine:$scope.teacher.addressLine,
					area:$scope.teacher.area,
					city:$scope.teacher.city,
					pinCode:$scope.teacher.pinCode,
					userId:$scope.teacher.userId,
					password:$scope.teacher.password,
					roleId:$scope.teacher.roleId,  
					active:$scope.teacher.active,
					branchId: $scope.teacher.branchId,
					isDelete: $scope.teacher.isDelete,
					teacherStandardList : $scope.teacher.teacherStandardList,
					districtId: $scope.teacher.districtId,
					stateId: $scope.teacher.stateId,
					talukaId: $scope.teacher.talukaId,
					instituteId: $scope.teacher.instituteId
			};
	
			Upload.upload({
	            url: 'teachers/save',
	            method: 'POST',
                data : teacher,
	            file: $scope.teacher.photo,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
	        		$state.transitionTo('teacher.list',
	        		{
						instituteId : $scope.teacher.instituteId,
	        			branchId : $scope.teacher.branchId,
	        			status : resp.data.status, 
	        			message : resp.data.message
					},{reload : true});
	        	}
	        	else {
	        		if(resp.data.isValidationError){
	        			
						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 };
						modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });
		};

		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('teacher.list');
		};
	});

	angular.module('teacher').controller('TeacherEditController',function($rootScope,$scope,$state,
			$filter, Upload,$stateParams, modalPopupService, $resource,$localStorage,CommonServices, StateList) {
		
		/** This is written to get single teacher object to display on the edit screen. */
		$scope.teacher = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.teacherHeaderValue = "Edit Teacher";
		$scope.stateList = StateList;
		$scope.districtList = [];
		$scope.talukaList = [];

		$resource('teachers/:id').get({id : $stateParams.id}).$promise.then(function(teachers) {
			teachers.dateOfBirth = new Date(teachers.dateOfBirth);
			angular.extend($scope.teacher,teachers);
			changeBranchList();
			changeDistrictList();
		});
		
		/** A directive can be written for the below 4 lines. Will be done in future. */
		$scope.isOpenStartDate = false;
		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		};

		$rootScope.setSelectedData($localStorage.selectedInstitute, $localStorage.selectedBranch);
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};


		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.teacher.instituteId || $scope.teacher.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.teacher.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
						changeStandardList();
				});	
			}
		};

		$scope.popup = function() {
			$scope.teacherStandardToEdit = {};
			var options = {
				templateUrl : 'app/teachers/html/teacher-popup.html',
				controller : 'TeacherStandardController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};
		
		$scope.editTeacherStandard = function(teacherStandard) {
			var teacherStdToEdit = JSON.stringify(teacherStandard);
			teacherStdToEdit = JSON.parse(teacherStdToEdit);
			$scope.teacherStandardToEdit = teacherStdToEdit;

			var options = {
				templateUrl : 'app/teachers/html/teacher-popup.html',
				controller : 'TeacherStandardController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		$scope.removeTeacherStandard = function(standardId, divisionId) {
			for ( var i = 0; i < $scope.teacher.teacherStandardList.length; i++) {
				var teacherStandard = $scope.teacher.teacherStandardList[i];
				if (teacherStandard.standardId == standardId) {
					var flag = false;
					if(divisionId){
						if (teacherStandard.divisionId == divisionId) {
							flag = true;
						}
					}
					else {
						flag = true;
					}
					if(flag){
						$scope.teacher.teacherStandardList.splice(i, 1);
					}
				}
			}
		};
		
		$scope.changeDistrictList = function(){
    		changeDistrictList();
		};
		
		function changeDistrictList(){
			
			if($scope.teacher.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.teacher.stateId})
					.$promise.then(function(districtList) {
						$scope.districtList.length = 0;
						angular.extend($scope.districtList, districtList);
						changeTalukaList();
				});	
			} 
		};
		
		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		function changeTalukaList(){
			
			if($scope.teacher.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.teacher.districtId})
					.$promise.then(function(talukaList) {
						$scope.talukaList.length = 0;
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};

		$scope.standardList = [];
		$scope.changeStandardList = function (){changeStandardList();};

		// Change standardList as per the standard selected.
		function changeStandardList(){
			if($scope.teacher.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.teacher.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;
					angular.extend($scope.standardList,standardList);
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}
		
		/** This method is used to save edited object. Here we explicitly copy $scope.teachers to normal teacher object. This is done because $scope.teacher contains angular specific properties. */
		$scope.save = function() {
			var teacher = {
					id: $scope.teacher.id,
					firstName: $scope.teacher.firstName,
					middleName: $scope.teacher.middleName,
					lastName: $scope.teacher.lastName,
					dateOfBirth : $filter('date')($scope.teacher.dateOfBirth, "yyyy-MM-dd"),
					emailAddress : $scope.teacher.emailAddress,
					gender : $scope.teacher.gender,
					primaryContact:$scope.teacher.primaryContact,
					secondaryContact:$scope.teacher.secondaryContact,
					addressLine:$scope.teacher.addressLine,
					area:$scope.teacher.area,
					city:$scope.teacher.city,
					pinCode:$scope.teacher.pinCode,
					userId:$scope.teacher.userId,
					password:$scope.teacher.password,
					roleId:$scope.teacher.roleId,  
					active:$scope.teacher.active,
					branchId : $scope.teacher.branchId,
					isDelete: $scope.teacher.isDelete,
					teacherStandardList : $scope.teacher.teacherStandardList,
					districtId: $scope.teacher.districtId,
					stateId: $scope.teacher.stateId,
					talukaId: $scope.teacher.talukaId,
					instituteId: $scope.teacher.instituteId
			};

			/** This method is used to adding teacher. It will only call add utility method */
		
			Upload.upload({
	            url: 'teachers/save',
	            method: 'POST',
                data : teacher,
	            file: $scope.teacher.photo,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
	        		$state.transitionTo('teacher.list',
	        		{
						instituteId : $scope.teacher.instituteId,
	        			branchId : $scope.teacher.branchId,
	        			status : resp.data.status, 
	        			message : resp.data.message
					},{reload : true});
	        	}
	        	else{
	        		if(resp.data.isValidationError){
						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						 var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('teacher.list');
		};
	});
	
	/** This method is used for uploading data */
	angular.module('teacher').controller('TeacherFileUploadController',function($scope,Upload, modalPopupService, $uibModalStack) {
		$scope.upload = function (file) {
	        Upload.upload({
	            url: 'teachers/uploadFile',
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	    		modalPopupService.closePopup(null, $uibModalStack);	
	    		
	        	$scope.teachers.length = 0;
	        	angular.extend($scope.teachers,resp.data.records);
	        });
	    };
	    	$scope.cancel = function(){
	    		modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
	
	
	
	/** This method is used for uploading data */
	angular.module('teacher').controller('TeacherSubjectFileUploadController',function($scope,Upload, modalPopupService,$uibModalStack) {
		$scope.upload = function (file) {
	        Upload.upload({
	            url: 'teachers/uploadTeacherSubjectFile',
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	    		modalPopupService.closePopup(null, $uibModalStack);
	    		
	        	$scope.teachers.length = 0;
	        	angular.extend($scope.teachers,resp.data.records);
	        });
	    };
	    	$scope.cancel = function(){
	    		modalPopupService.closePopup(null, $uibModalStack);
	    };
	});


	angular.module('group').controller('TeacherStandardController',['$scope','$resource','modalPopupService','$uibModalStack',
        function($scope, $resource, modalPopupService, $uibModalStack) {

		$scope.tempStandardList = $scope.standardList;
		$scope.tempDivisionList = [];
		$scope.teacherStandard = {};

		var teacherStandardToEdit = JSON.stringify($scope.teacherStandardToEdit);
		teacherStandardToEdit = JSON.parse(teacherStandardToEdit);

		$scope.teacherStandard = (teacherStandardToEdit ? teacherStandardToEdit : {});
		$scope.subjectList = [];
		$scope.selectedSubjects = [];
		// $scope.teacher.teacherStandardList = [];
		console.log("TO EDIT ::::: "+JSON.stringify($scope.teacherStandard));
		$scope.isEdit = ($scope.teacherStandard.standardId ? true : false);

		if($scope.teacherStandard.standardId){
			changeDivisions();
			changeSubjectList();
		}

		$scope.changeDivisions = function (){
			changeDivisions();
			changeSubjectList();
		};

		// Change divisionList as per the standard selected.
		function changeDivisions(){
			//$scope.divisionList.length = 0;
			if($scope.teacherStandard.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.teacherStandard.standardId){
						$scope.tempDivisionList = standard.divisionList;
						break;
					}
				}
			}
		}

		// Change SubjectList as per the standard selected.
		function changeSubjectList(){
			if($scope.teacherStandard.standardId){
				$resource('masters/subjectByStandard/:id').query({id : $scope.teacherStandard.standardId}).$promise.then(
				 function(subjectList) {
					$scope.subjectList.length = 0;
					angular.extend($scope.subjectList,subjectList);

					if($scope.teacherStandard.subjectList){
						if($scope.teacherStandard.subjectList.length > 0){
							for(var i =0; i < $scope.subjectList.length; i++){
								var subject = $scope.subjectList[i];
								for(var j =0; j < $scope.teacherStandard.subjectList.length; j++){
									var selectedSubj = $scope.teacherStandard.subjectList[j];
									if(subject.id == selectedSubj.id){
										subject.ticked = true;
										break;
									}
								}
							}
						}	
					}
				});
			}
			else {
				$scope.subjectList.length = 0;
			}
		}

		/** Add individuals to an array */
		$scope.addStandards = function() {
			console.log("TO SAVE ::::: "+JSON.stringify($scope.teacherStandard));
			if($scope.teacherStandard.standardId) {
				var isTeacherStandardExist = false;
				var teacherStdIndex = -1;
				try {
					teacherStdIndex = teacherStandardExist();
				}catch(err){
					isTeacherStandardExist = true;
				}

				if(!isTeacherStandardExist){
					$scope.teacherStandard.standard = getStandardNameById($scope.teacherStandard.standardId);

					if($scope.teacherStandard.divisionId){
						$scope.teacherStandard.division = getDivisionNameById($scope.teacherStandard.divisionId);	
					}
					if($scope.selectedSubjects){
						var selectedSubjects = "";
						for(var i =0;i < $scope.selectedSubjects.length; i++){
							var subject = $scope.selectedSubjects[i];
							selectedSubjects = selectedSubjects + (i != 0 ? ", " : "") + subject.name;
						}

						$scope.teacherStandard.subjectList = $scope.selectedSubjects;
						$scope.teacherStandard.subjects = selectedSubjects;
					}

					var teacherStd = $scope.teacherStandard;
					teacherStd = JSON.stringify(teacherStd);
					teacherStd = JSON.parse(teacherStd);

					console.log("Index to update ::::::"+teacherStdIndex);

					if($scope.isEdit && teacherStdIndex >= 0 ){
						$scope.teacher.teacherStandardList[teacherStdIndex] = teacherStd;
					}else {
						$scope.teacher.teacherStandardList.push(teacherStd);	
					}
		    		modalPopupService.closePopup(null, $uibModalStack);
				}
			}
		};

		function teacherStandardExist() {
			var teacherStdIndex = -1;
			if($scope.teacher.teacherStandardList && $scope.teacher.teacherStandardList.length > 0){

				for(var i = 0; i < $scope.teacher.teacherStandardList.length; i++){
					var teacherStandard = $scope.teacher.teacherStandardList[i];
					var isSameObj = false;

					if($scope.isEdit && teacherStandard.standardId == $scope.teacherStandardToEdit.standardId 
							&& teacherStandard.divisionId == $scope.teacherStandardToEdit.divisionId) {

						isSameObj = true;
						teacherStdIndex = i;
					}

					if(!isSameObj) {
						if(teacherStandard.standardId == $scope.teacherStandard.standardId){
							if((!teacherStandard.divisionId && $scope.teacherStandard.divisionId > 0)){
								$scope.selectionErrorMessage = "Standard is already exist with no divisions.";
								new Exception("Match Found Exist!!");
								break;
							}

							if((!$scope.teacherStandard.divisionId > 0 && teacherStandard.divisionId )){
								$scope.selectionErrorMessage = "Standard is already exist with some divisions.";
								new Exception("Match Found Exist!!");
								break;
							}

							if(teacherStandard.divisionId == $scope.teacherStandard.divisionId  ){
								$scope.selectionErrorMessage = "You have already selected this division for this standard.";
								new Exception("Match Found Exist!!");
								break;
							}
						}
					}
					else if($scope.isEdit){
						
					}
				}
			}
			return teacherStdIndex;
		}


		function getStandardNameById(standard){
			for(var i =0; i< $scope.tempStandardList.length ; i++){
				var standardObj = $scope.tempStandardList[i];
				if(standardObj.id == standard){
					return standardObj.displayName;
				}
			}
		}

		function getDivisionNameById(division){
			for(var i =0; i< $scope.tempDivisionList.length ; i++){
				var divisionObj = $scope.tempDivisionList[i];
				if(divisionObj.id == division){
					return divisionObj.displayName;
				}
			}
		}

		$scope.cancelPopup = function(){
    		modalPopupService.closePopup(null, $uibModalStack);
    };
			
	} ]);
	
})();


