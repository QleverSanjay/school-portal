(function(){
	angular.module('displayTemplate').controller('DisplayTemplateController',function($rootScope,$resource, $localStorage, 
			$filter,$confirm,DTOptionsBuilder,$stateParams,modalPopupService,$scope,$state,$uibModal,CommonServices) {

		$scope.displayTemplateList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isSingleBranch = false;	
		$scope.branchName = "";

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('displayTemplates?branchId='+$scope.branchId).query().$promise.then(function(displayTemplateList) {
					$scope.displayTemplateList.length = 0;
					if(displayTemplateList && displayTemplateList.length > 0){
						angular.extend($scope.displayTemplateList,displayTemplateList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}
    	
     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.instituteId){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};
		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
        				if(!isBranchChanged){
							filterTableByBranch($scope.branchId);
						}
        			}
					angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.downloadDisplayTemplate = function (){
			if($scope.branchId){
				var url = "displayTemplate/download?branch_id="+$scope.branchId;
				document.location.href = url;
			}
		}
		$scope.uploadTemplate = function(){
			var options = {
	  	    	      templateUrl: 'app/displayTemplate/html/display-template-upload-popup.html',
	  	    	      controller: 'DisplayTemplateUploadController',
	  	    	      scope : $scope
	  	    	    };
	  	    	  modalPopupService.openPopup(options);
	     };

	     $scope.deleteData = function(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('displayTemplate?displayTemplateId='+id).remove().$promise.then(function(resp) {
        	 		if(resp.status != 'fail'){
						$state.transitionTo('displayTemplate.list',{
								 status : resp.status,
								 message: resp.message,
								 instituteId : $rootScope.instituteId,
				        		 branchId : $scope.branchId
						},{reload : true});
					}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});
		
	/*angular.module('displayTemplate').controller('validatedisplayTemplateForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,35}$/;
		$scope.regExVenue = /^[A-Z a-z 0-9 , . () - @]{2,100}$/;
	});
	*/
	angular.module('displayTemplate').controller('DisplayTemplateAddController',function($rootScope,$filter, 
			$localStorage, $scope,$state,$stateParams,$resource,CommonServices,modalPopupService) {
		$scope.displayTemplate = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.displayTemplateHeaderValue = "Add Display Template";

		$scope.displayTemplate.instituteId = $localStorage.selectedInstitute;
		$scope.displayTemplate.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.displayTemplate.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $scope.displayTemplate.instituteId){
	    			changeBranchList(false);
	    		}
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
    	
    	$scope.addNewDislayHead = function(){
    		var options = {
  	    	      templateUrl: 'app/displayTemplate/html/display-template-add-new-head-popup.html',
  	    	      controller: 'DisplayTemplateAddNewDisplayHeadController',
  	    	      scope : $scope
  	    	    };
  	    	  modalPopupService.openPopup(options);
	    	  
	     };
	     
	     $scope.addExistingTemplate = function(){
	    		var options = {
	  	    	      templateUrl: 'app/displayTemplate/html/display-template-add-existing-template-popup.html',
	  	    	      controller: 'DisplayTemplateSearchTemplateController',
	  	    	      scope : $scope
	  	    	    };
	  	    	  modalPopupService.openPopup(options);
		    	  
		     };

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.displayTemplate.instituteId || $scope.displayTemplate.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.displayTemplate.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);	
						 $scope.getDisplayHeadList();
				});	
			}
		};

		$scope.cancel = function(){
			CommonServices.cancelOperation('displayTemplate.list');
		};

		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var displayTemplate = {
				 "name":$scope.displayTemplate.name,
				 "branchId" : $scope.displayTemplate.branchId,
				 "displayHeadList": $scope.displayTemplate.displayHeadList,
				 "description":$scope.displayTemplate.description,
				 "active": $scope.displayTemplate.active?'Y':'N'
			}
			$scope.message = "";
			$resource('displayTemplate').save(displayTemplate).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('displayTemplate.list',{
							 status : resp.status,
							 message: resp.message,
							 instituteId : $scope.displayTemplate.instituteId,
			        		 branchId : $scope.displayTemplate.branchId
					},{reload : true});
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});
		}

		$scope.popup = function(){
	    	  var options = {
	    	      templateUrl: 'app/displayTemplate/html/display-template-popup.html',
	    	      controller: 'DisplayTemplateSearchController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };

	     $scope.getDisplayHeadList = function(){
	    	 if($scope.displayTemplate.branchId){
	    		 $scope.displayHeadList = [];
		    		$resource('displayHeads?branchId='+$scope.displayTemplate.branchId).query().$promise.then(function(displayHeadList) {
	        			angular.extend($scope.displayHeadList,displayHeadList);
	        			$scope.backup = $scope.displayHeadList;
	        			$scope.displayTemplate.displayHeadList = $scope.displayHeadList;
	        			$scope.AutocompletedisplayHeadList = $scope.backup;
	    		});
	    	 }else {
	    		 $scope.backup.length = 0;
     			$scope.displayTemplate.displayHeadList.length = 0;
     			$scope.AutocompletedisplayHeadList.length = 0;
	    	 }
	     }

	     $scope.addDisplayHead = function(data){
	    	 var idx = $scope.displayTemplate.displayHeadList.indexOf(data);
	    	 if(idx > -1){
	    		 $scope.message = "Display head already exist in this display template."
	    			 $scope.status = "fail"
	    			 
	    	 }else{
	    		 $scope.message = "";
	    		 $scope.searchDisplayHead = '';
	    		 $scope.displayTemplate.displayHeadList.push(data);
	    	 }
	    	 
	     }

	     $scope.removeDisplayHead=function(obj){
	    	 var idx = $scope.displayHeadList.indexOf(obj);
	    	 $scope.displayTemplate.displayHeadList.splice(idx,1);
	     }
	     
	  // Move list items up or down or swap
	     $scope.moveItem = function (origin, destination) {
	         var temp = $scope.displayTemplate.displayHeadList[destination];
	         $scope.displayTemplate.displayHeadList[destination] = $scope.displayTemplate.displayHeadList[origin];
	         $scope.displayTemplate.displayHeadList[origin] = temp;
	     };

	     // Move list item Up
	     $scope.listItemUp = function (itemIndex) {
	         $scope.moveItem(itemIndex, itemIndex - 1);
	     };

	     // Move list item Down
	     $scope.listItemDown = function (itemIndex) {
	         $scope.moveItem(itemIndex, itemIndex + 1);
	     };

	});

	/*This Controller is Used for displayTemplate Edit functionality*/
	angular.module('displayTemplate').controller('DisplayTemplateEditController',function($rootScope,$filter,$scope,$state,$stateParams,$resource, CommonServices) {
		
		$scope.displayTemplate = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.displayTemplateHeaderValue = "Edit Display Template";

		$resource('displayTemplate?displayTemplateId='+$stateParams.id).get().$promise.then(function(displayTemplateToEdit) {
			angular.extend($scope.displayTemplate,displayTemplateToEdit);
			$rootScope.setSelectedData($scope.displayTemplate.instituteId,$scope.displayTemplate.branchId);
			$scope.displayTemplate.active=$scope.displayTemplate.active == 'Y'?true:false;
			changeBranchList();
			$scope.getDisplayHeadList();
		});

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.displayTemplate.instituteId || $scope.displayTemplate.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.displayTemplate.instituteId})
					.$promise.then(function(branchList) {
						/*if(branchList != null && branchList.length > 0){
							$scope.displayTemplate.branchId = branchList[0].id;
						}*/
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.cancel = function(){
			$state.transitionTo('displayTemplate.list',{reload : true});
		};

	     $scope.getDisplayHeadList = function(){
	    	 if($scope.displayTemplate.branchId){
	    		 $scope.displayHeadList = [];
		    		$resource('displayHeads?branchId='+$scope.displayTemplate.branchId).query().$promise.then(function(displayHeadList) {
	        			angular.extend($scope.displayHeadList,displayHeadList);
	        			$scope.backup = $scope.displayHeadList;
	        			//$scope.displayTemplate.displayHeadList = $scope.displayHeadList;
	        			$scope.AutocompletedisplayHeadList = $scope.backup;
	    		});
	    	 }else {
	    		 $scope.backup.length = 0;
     			$scope.displayTemplate.displayHeadList.length = 0;
     			$scope.AutocompletedisplayHeadList.length = 0;
	    	 }
	     }

		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var displayTemplate = {
				 "id" : $scope.displayTemplate.id,
				 "name":$scope.displayTemplate.name,
				 "branchId" : $scope.displayTemplate.branchId,
				 "displayHeadList": $scope.displayTemplate.displayHeadList,
				 "description":$scope.displayTemplate.description,
				 "active": $scope.displayTemplate.active?'Y':'N'
			}

			$resource('displayTemplate').save(displayTemplate).$promise.then(function(resp) {
				if(resp.status != 'fail'){
					$state.transitionTo('displayTemplate.list',{
							 status : resp.status,
							 message: resp.message,
							 instituteId : $scope.displayTemplate.instituteId,
			        		 branchId : $scope.displayTemplate.branchId
					},{reload : true});
				}
				else {
					$scope.isSaveDisabled = false;
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 modalPopupService.openPopup({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
				}
			});
		}
		
		 $scope.addDisplayHead = function(data){
	    	 var idx = $scope.displayTemplate.displayHeadList.indexOf(data);
	    	 if(idx > -1){
	    		 $scope.message = "Display head already exist in this display template."
	    			 $scope.status = "fail"
	    			 
	    	 }else{
	    		 $scope.message = "";
	    		 $scope.displayTemplate.displayHeadList.push(data);
	    	 }
	    	 
	     }
	     
	     $scope.removeDisplayHead=function(obj){
	    	 var idx = $scope.displayHeadList.indexOf(obj);
	    	 $scope.displayTemplate.displayHeadList.splice(idx,1);
	     }
	     
	  // Move list items up or down or swap
	     $scope.moveItem = function (origin, destination) {
	         var temp = $scope.displayTemplate.displayHeadList[destination];
	         $scope.displayTemplate.displayHeadList[destination] = $scope.displayTemplate.displayHeadList[origin];
	         $scope.displayTemplate.displayHeadList[origin] = temp;
	     };

	     // Move list item Up
	     $scope.listItemUp = function (itemIndex) {
	         $scope.moveItem(itemIndex, itemIndex - 1);
	     };

	     // Move list item Down
	     $scope.listItemDown = function (itemIndex) {
	         $scope.moveItem(itemIndex, itemIndex + 1);
	     };
	});
	
	angular.module('displayTemplate').controller('DisplayTemplateSearchController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl',
     	  function($rootScope,$scope,$resource,modalPopupService,$uibModalStack,userserviceapiurl) {
     		$scope.clickedForTemplateSearch = false;
     		$scope.searchedDisplayTemplate = []; 
     		
     		/** This method is used to search groups based on provided details*/
     		$scope.searchDisplayTemplate = function(){
     			$scope.searchedDisplayTemplate = [];
     			console.log($scope.searchData);
     			
     				$resource(userserviceapiurl + 'displayTemplate/search?search_text='+$scope.searchData.name+'&exclude_head_id='+$scope.displayTemplate.displayHeadIdList||""+'&branch_id='+$scope.displayTemplate.branchId).query()
     					.$promise.then(function(data) {
     		    		 if(data.length > 0){
     			    		 $scope.hasDisplayTemplateSearchRecords = true;
     			    		 $scope.clickedForDisplayTemplateSearch = false;
     			    		 angular.forEach( data, function(head){
     			    			head.isSelected = false;
     				    	 });
     			    		 angular.extend($scope.searchedDisplayHead,data);
     			    	 }
     		    		 else {
     		    			 $scope.hasDisplayHeadSearchRecords = false;
     			    		 $scope.clickedForDisplayHeadSearch = true;
     		    		 }
     		 		 });
     			
     				
     				
     	     };
     	     
     	     /** Add Groups to an array */
     	     $scope.addDisplayHead  = function(){
     	    	 angular.forEach( $scope.searchedDisplayHead, function(head){
     	    		 if (head.isSelected) {
         		    	$scope.displayTemplate.displayHeadList.push(head);
         		    	$scope.displayTemplate.displayHeadIdList.push(head.id);
         		    }
     	    	 });
     	    	 modalPopupService.closePopup(null, $uibModalStack);
     	     };
     	     
     	     $scope.cancelPopup = function(){
     	    	 modalPopupService.closePopup(null, $uibModalStack);
     		    };
     	}]);
	
	angular.module('displayTemplate').controller('DisplayTemplateAddNewDisplayHeadController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl',
   	  function($rootScope,$scope,$resource,modalPopupService,$uibModalStack,userserviceapiurl) {
		newDisplayHeadObj = {};
		$scope.errorNewHeadMessage = '';
   		$scope.addnewDisplayhead = []; 
   		
   		/** This method is used to search groups based on provided details*/
   		$scope.addNewDisplayHeadTemplate = function(Obj){
   			$scope.addnewDisplayhead = [];
   			sendData = {};
   			sendData = Obj;
   			sendData.name = Obj.headName;
   			sendData.active = 'Y';
   			sendData.branchId = $scope.$parent.displayTemplate.branchId;
   			
   				$resource('displayHead').save(sendData)
   					.$promise.then(function(data) {
   		    		 if(data.status == 'success'){
   		    			sendData.id = data.generated_id;
   		    			$scope.displayTemplate.displayHeadList.push(sendData);
   		    			$scope.cancelPopup();
   			    	 }else{
   			    		 $scope.errorNewHeadMessage = data.message;
   			    	 }
   		    		 
   		 		 });
   				
   	     };
   	     
   	     $scope.cancelPopup = function(){
   	    	 modalPopupService.closePopup(null, $uibModalStack);
   		    };
   	}]);
	
	angular.module('displayTemplate').controller('DisplayTemplateSearchTemplateController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl',
	  function($rootScope,$scope,$resource,modalPopupService,$uibModalStack,userserviceapiurl) {
		newTemplateObj = {};
		$scope.errorNewHeadMessage = '';
		$scope.searchedTemplate = [];
		$scope.addExistingTemplate = [];
		$scope.showSearchTemplateRecord = false;
		$scope.noSearchRecord = false;
			
			/** This method is used to search groups based on provided details*/
			$scope.searchTemplate = function(data){
				$scope.branchId = $scope.$parent.displayTemplate.branchId;
					$resource('displayTemplate/search?search_text='+data+'&branch_id='+$scope.branchId).query()
						.$promise.then(function(data) {
			    		 if(data.length > 0){
			    			 $scope.showSearchTemplateRecord = true;
			    			$scope.searchedTemplate = data;
			    			//$scope.cancelPopup();
				    	 }else{
				    		 $scope.noSearchRecord = true;
				    		 $scope.errorNewHeadMessage = data.message;
				    		 $scope.searchedTemplate = [];
				    	 }
			    		 
			 		 });
					
		     };
		     
		     $scope.addTemplate = function(id){
						$resource('displayTemplate?displayTemplateId='+id).get()
							.$promise.then(function(data) {
								if(data){
									$scope.displayTemplate.name = data.name;
									$scope.displayTemplate.description = data.description;
					    			$scope.displayTemplate.displayHeadList = data.displayHeadList;
					    			$scope.cancelPopup();
						    	 }else{
						    		 $scope.noSearchRecord = true;
						    		 $scope.errorNewHeadMessage = data.message;
						    		 $scope.addExistingTemplate = [];
						    	 }
				    		 
				 		 },function(error){
				 			 console.log(error);
				 		 });
						
			     };
		     
		     $scope.cancelPopup = function(){
		    	 modalPopupService.closePopup(null, $uibModalStack);
			    };
	}]);


	angular.module('displayTemplate').controller('DisplayTemplateUploadController',['$rootScope','$scope','$resource','modalPopupService','$uibModalStack','userserviceapiurl', 'Upload',
	  function($rootScope,$scope,$resource,modalPopupService,$uibModalStack,userserviceapiurl, Upload) {
		
		$scope.upload = function (file) {
	        Upload.upload({
	            url: 'displayTemplate/upload',
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
        		angular.extend($scope.events,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };

	}]);
})();