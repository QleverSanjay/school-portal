(function(){
	angular.module('displayTemplate').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('displayTemplate', {
			url : '/displayTemplate',
			abstract :true,
			controller : 'displayTemplateModelController',
			data: {
				 breadcrumbProxy: 'displayTemplate.list'
			}
		}).state('displayTemplate.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/displayTemplate/html/displayTemplate-list.html',
					controller : 'DisplayTemplateController'
				}
			},
			data: {
	            displayName: 'Display Template List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : ''
	        }
		}).state('displayTemplate.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/displayTemplate/html/displayTemplate-add.html',
					controller : 'DisplayTemplateAddController'
				}
			},
			data: {
	            displayName: 'Add Display Template'
	        },
	        params : {
	        	instituteId : ''
	        }
		}).state('displayTemplate.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/displayTemplate/html/displayTemplate-add.html',
					controller : 'DisplayTemplateEditController'
				}
			},
			data: {
	            displayName: 'Edit Display Template'
	        }
		});
	}]);
})();