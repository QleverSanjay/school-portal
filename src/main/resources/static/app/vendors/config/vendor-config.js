(function(){
	angular.module('vendor').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('vendor', {
			url : '/vendor',
			abstract :true,
			controller : 'VendorModelController',
			data: {
				 breadcrumbProxy: 'vendor.list'
	        }
		}).state('vendor.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/vendors/html/vendor-list.html',
					controller : 'VendorController'
				}
			},
			data: {
	            displayName: 'Vendor List'
	        },
	        params : {
	        	instituteId : '',
	        	branchId : '',
	        	message : '',
	        	status : ''
	        },
	        resolve : {
	        	VendorList : function($resource){
	        		return $resource('vendors').query().$promise;
	        	}
	        }
		}).state('vendor.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/vendors/html/vendor-add.html',
					controller : 'VendorAddController'
				}
			},
			data: {
	            displayName: 'Add Vendor'
	        }
			
		}).state('vendor.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/vendors/html/vendor-add.html',
					controller : 'VendorEditController'
				}
			},
			data: {
	            displayName: 'Edit Vendor'
	        }
		});
	}]);
})();