(function(){
	angular.module('vendor').controller('VendorController',function($rootScope,$resource,DTOptionsBuilder,VendorList,$confirm,$stateParams,$scope,$state,$uibModal,CommonServices) {
	    $scope.vendors = [];
	    $scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	 //   angular.extend($scope.vendors,VendorList);

	    $scope.instituteList = [];
	    $scope.branchList = [];
		$scope.isSingleBranch = false;
		$scope.branchName = "";
	    
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);
		
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch($scope.branchId);
		};
		
		function filterTableByBranch(branchId){
			if(branchId){
				$resource('vendors/getVendorByBranch?branchId='+$scope.branchId).query().$promise.then(function(vendorList) {
					$scope.vendors.length = 0;
					if(vendorList && vendorList.length > 0){
						angular.extend($scope.vendors,vendorList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				/*$scope.branchId = (!$stateParams.branchId ? branchList[0].id : $stateParams.branchId);
        				if($rootScope.roleId == 6 && branchList.length == 1){
        					$scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;
        				}*/
        				filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList && $stateParams.instituteId){
	    			$scope.instituteId = $stateParams.instituteId;
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}
    	
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};
		
		
		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.branchId = (isBranchChanged ? branchList[0].id  : 
								(!$stateParams.branchId ? branchList[0].id : $stateParams.branchId));
	        				if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
    	
		
	    /** This method is used to open Modal */ 
	    $scope.uploadVendors = function(){
	    	  $uibModal.open({
	    	      templateUrl: 'app/vendors/html/vendor-bulk-upload.html',
	    	      controller: 'VendorFileUploadController',
	    	      scope : $scope
	    	     
	    	    });
	     };
	     
		 $scope.attachVendor = function attachVendor(vendor){
	    	 	$resource('vendors/attachVendorByBranch?userId='+vendor.userId+'&branchId='+$scope.branchId).get().$promise.then(function(resp) {
	    	 	if(resp.status == 'success'){
	    	 		vendor.vendorExist = true;
	    	 	}	
    	 		$scope.status = resp.status;
        		$scope.message = resp.message;
	 		 });
	     }; 
			     
	     $scope.deleteData = function deleteVendor(id){
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {

	        	 	$resource('vendors').remove({id : id}).$promise.then(function(resp) {
			        		for(var i =0; i < $scope.vendors.length ;i ++ ){
		      			  		var vendor = $scope.vendors[i];
		      			  		if(vendor.id == id){
		      			  			$scope.vendors.splice(i , 1);
		      			  			break;
		      			  		}
		      			  	}	
			        		$scope.status = resp.status;
			        		$scope.message = resp.message;
	      			});
	         });
	     }; 

		$scope.removeData = function removeVendor(vendor){
	   	 $confirm({text: 'Are you sure you want to remove?'})
	        .then(function() {

	       	 	$resource('vendors/removeVendor?userId='+vendor.userId+'&branchId='+$scope.branchId).get().$promise.then(function(resp) {
	       	 	if(resp.status == 'success'){
	    	 		vendor.vendorExist = false;
	    	 	}
	       	 		$scope.status = resp.status;
	        		$scope.message = resp.message;
	 			});
	        });
	    };	     
	});

		
	
	
	angular.module('vendor').controller('validateVendorForm',function($resource,$scope,$state) {
		$scope.regExName = /^[A-Z a-z 0-9]{2,100}$/;
		$scope.regExBankAcc = /^[A-Z a-z 0-9]{2,20}$/;
		$scope.regExCode= /^[A-Z a-z 0-9]{1,6}$/;
		$scope.regExTid= /^[A-Z a-z 0-9]{1,8}$/;
		$scope.regExAddressLine = /^[A-Z a-z 0-9 , . () & _ @ # -]{2,100}$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,56}$/;
		$scope.regExCity = /^[A-Z a-z 0-9]{2,56}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExFaxNo = /^[0-9]{8,12}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExProprietorName = /^[A-Z a-z 0-9]{2,256}$/;
		$scope.regExMerchantPan = /^[0-9]{10,12}$/;
		$scope.regExAnnualTurnOver = /^[0-9]{3,15}$/;
		$scope.regExYear = /^[0-9]{4,4}$/;
		
	});

	
	angular.module('vendor').controller('VendorAddController',function($scope,$uibModal,Upload,$stateParams,$state,$resource,CommonServices) {
		$scope.vendor = {}; 
		$scope.isOpenStartDate = false;
		$scope.hasSearchRecords = false;
		$scope.instituteIds = [];
		$scope.vendor.institutes = [];
		$scope.vendorHeaderValue = "Add Vendor";
		
		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		}
		
		$scope.instituteButtonValue = "Select";
		
		$scope.popup = function(){
	    	  $uibModal.open({
	    	      templateUrl: 'app/vendors/html/vendor-popup.html',
	    	      controller: 'InstituteSearchController',
	    	      scope : $scope
	    	    });
	     };
		
	     
	     /** Remove item from selected institutes */
	     $scope.removeInstitute = function(id){
	    	 for (var i = 0; i< $scope.vendor.institutes.length; i++){
	    		 var institute = $scope.vendor.institutes[i];
    			 if (institute.branchId == id) {
    		    	$scope.vendor.institutes.splice(i, 1);
    		    	$scope.instituteIds.splice(i, 1);
    		    	break;
	    		 }
	    	 }
	     };
		
		/** This method is used to adding vendor. It will only call add utility method */
		$scope.save = function() {
		
			var vendor = {
					id: $scope.vendor.id,
					merchantLegalName: $scope.vendor.merchantLegalName,
					merchantMarketingName: $scope.vendor.merchantMarketingName,
					bankAccountNumber: $scope.vendor.bankAccountNumber,
					meCode : $scope.vendor.meCode,
					tid : $scope.vendor.tid,
					lgCode : $scope.vendor.lgCode,
					lcCode:$scope.vendor.lcCode,
					branch:$scope.vendor.branch,
					addressLine1:$scope.vendor.addressLine1,
					area:$scope.vendor.area,
					city:$scope.vendor.city,
					pinCode:$scope.vendor.pinCode,
					primaryPhone:$scope.vendor.primaryPhone,
					secondaryPhone:$scope.vendor.secondaryPhone,
					faxNo:$scope.vendor.faxNo,  
					email:$scope.vendor.email,
					shopOwnership: $scope.vendor.shopOwnership,
					otherGroup: $scope.vendor.otherGroup,
					otherGroupAddress:$scope.vendor.otherGroupAddress,
					proprietorName:$scope.vendor.proprietorName,
					proprietorAddressLine1:$scope.vendor.proprietorAddressLine1,
					proprietorAddressArea:$scope.vendor.proprietorAddressArea,
					proprietorAddressCity:$scope.vendor.proprietorAddressCity,
					proprietorAddressPincode:$scope.vendor.proprietorAddressPincode,
					proprietorPrimaryPhone:$scope.vendor.proprietorPrimaryPhone,
					proprietorSecondaryPhone:$scope.vendor.proprietorSecondaryPhone,
					proprietorFaxNo:$scope.vendor.proprietorFaxNo,
					ownership:$scope.vendor.ownership,
					ownershipOther:$scope.vendor.ownershipOther,
					businessStartDate:$scope.vendor.businessStartDate,
					merchantPan:$scope.vendor.merchantPan,
					creditCardAccepted:$scope.vendor.creditCardAccepted,
					creditCardAcceptedOther:$scope.vendor.creditCardAcceptedOther,
					annualTurnOver:$scope.vendor.annualTurnOver,
					annualTotalOnCreditCard:$scope.vendor.annualTotalOnCreditCard,
					averagePerTransaction:$scope.vendor.averagePerTransaction,
					creditCardBank:$scope.vendor.creditCardBank,
					creditCardSinceYear:$scope.vendor.creditCardSinceYear,
					ebSavingAccount:($scope.vendor.ebSavingAccount == true || $scope.vendor.ebSavingAccount == 'Y' ? "Y" : "N"),
					ebCurrentAccount:($scope.vendor.ebCurrentAccount == true || $scope.vendor.ebCurrentAccount == 'Y' ? "Y" : "N"),
					ebTermDeposit:($scope.vendor.ebTermDeposit == true || $scope.vendor.ebTermDeposit == 'Y' ? "Y" : "N"),
					ebDemat:($scope.vendor.ebDemat == true || $scope.vendor.ebDemat == 'Y' ? "Y" : "N"),
					ebAutoLoan:($scope.vendor.ebAutoLoan == true || $scope.vendor.ebAutoLoan == 'Y' ? "Y" : "N"),
					ebPersonalLoan:($scope.vendor.ebPersonalLoan == true || $scope.vendor.ebPersonalLoan == 'Y' ? "Y" : "N"),
					ebTwoWheeler:($scope.vendor.ebTwoWheeler == true || $scope.vendor.ebTwoWheeler == 'Y' ? "Y" : "N"),
					ebCreditCard:($scope.vendor.ebCreditCard == true || $scope.vendor.ebCreditCard == 'Y' ? "Y" : "N"),
					ebLoanAgainstShares:($scope.vendor.ebLoanAgainstShares == true || $scope.vendor.ebLoanAgainstShares == 'Y' ? "Y" : "N"),
					ebBusinessBanking:($scope.vendor.ebBusinessBanking ==true || $scope.vendor.ebBusinessBanking == 'Y' ? "Y" : "N"),
					ebInsurance:($scope.vendor.ebInsurance == true || $scope.vendor.ebInsurance == 'Y' ? "Y" : "N"),
					ebMutualFund:($scope.vendor.ebMutualFund == true || $scope.vendor.ebMutualFund == 'Y' ? "Y" : "N"),
					cardAcceptTypeVisa:($scope.vendor.cardAcceptTypeVisa == true || $scope.vendor.cardAcceptTypeVisa == 'Y' ? "Y" : "N"),
					cardAcceptTypeMaster:($scope.vendor.cardAcceptTypeMaster == true || $scope.vendor.cardAcceptTypeMaster == 'Y' ? "Y" : "N"),
					cardAcceptTypeDinner:($scope.vendor.cardAcceptTypeDinner == true || $scope.vendor.cardAcceptTypeDinner == 'Y' ? "Y" : "N"),
					physicalMpr:($scope.vendor.physicalMpr == true || $scope.vendor.physicalMpr == 'Y' ? "Y" : "N"),
					emailMpr:($scope.vendor.emailMpr == true || $scope.vendor.emailMpr == 'Y' ? "Y" : "N"),
					emailMprText:$scope.vendor.emailMprText,
					mprFrequency:$scope.vendor.mprFrequency,
					sellingCategory:$scope.vendor.sellingCategory,
					userId:$scope.vendor.userId,
					active:$scope.vendor.active,
					isDelete: $scope.vendor.isDelete
			};
			
			Upload.upload({
	            url: 'vendors/save',
	            method: 'POST',
                data : vendor,
	            file: $scope.vendor.photo,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
	        		$state.transitionTo('vendor.list',
	        		{
						status : resp.data.status, 
	        			message : resp.data.message
					},{reload : true});
	        	}
	        	else {
	        		$scope.status = resp.data.status;
	        		$scope.message = resp.data.message;
	        	}
	        });
		};
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancelPopup = function(){
			$uibModal.dismiss('cancel');
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('vendor.list');
		};
	});

	angular.module('vendor').controller('VendorEditController',function($scope,$state,$uibModal,Upload,$stateParams,$resource,CommonServices) {
		/** This is written to get single vendor object to display on the edit screen. */
		$scope.vendor = {};
		$scope.instituteIds = [];
		$scope.vendor.institutes = [];
		$scope.vendorHeaderValue = "Edit Vendor";
		
		$resource('vendors/:id').get({id : $stateParams.id}).$promise.then(function(vendors) {
			angular.extend($scope.vendor,vendors);
			$scope.vendor.ebSavingAccount = (vendors.ebSavingAccount == 'Y' ? true : false);
			$scope.vendor.ebCurrentAccount  = (vendors.ebCurrentAccount == 'Y' ? true : false);
			$scope.vendor.ebTermDeposit = (vendors.ebTermDeposit == 'Y' ? true : false);
			$scope.vendor.ebDemat = (vendors.ebDemat == 'Y' ? true : false);
			$scope.vendor.ebAutoLoan = (vendors.ebAutoLoan == 'Y' ? true : false);
			$scope.vendor.ebPersonalLoan = (vendors.ebPersonalLoan == 'Y' ? true : false);
			$scope.vendor.ebTwoWheeler = (vendors.ebTwoWheeler == 'Y' ? true : false);
			$scope.vendor.ebCreditCard = (vendors.ebCreditCard == 'Y' ? true : false);
			$scope.vendor.ebLoanAgainstShares = (vendors.ebLoanAgainstShares == 'Y' ? true : false);
			$scope.vendor.ebBusinessBanking = (vendors.ebBusinessBanking == 'Y' ? true : false);
			$scope.vendor.ebInsurance = (vendors.ebInsurance == 'Y' ? true : false);
			$scope.vendor.ebMutualFund = (vendors.ebMutualFund == 'Y' ? true : false);
			$scope.vendor.cardAcceptTypeVisa = (vendors.cardAcceptTypeVisa == 'Y' ? true : false);
			$scope.vendor.cardAcceptTypeMaster = (vendors.cardAcceptTypeMaster == 'Y' ? true : false);
			$scope.vendor.cardAcceptTypeDinner = (vendors.cardAcceptTypeDinner == 'Y' ? true : false);
			$scope.vendor.physicalMpr = (vendors.physicalMpr == 'Y' ? true : false);
			$scope.vendor.emailMpr = (vendors.emailMpr == 'Y' ? true : false);
			
			
			$scope.vendor.institutes = (vendors.institutes != null ? vendors.institutes : []);
			angular.extend($scope.instituteIds,vendors.instituteIdList);
		});
		
		/** A directive can be written for the below 4 lines. Will be done in future. */
		$scope.isOpenStartDate = false;
		$scope.openStartDateDialog = function fnOpenDialog(){
			$scope.isOpenStartDate = true;
		}
	
		console.log("$scope.instituteIds = "+$scope.instituteIds);
		$scope.instituteButtonValue = "Select";
		$scope.popup = function(){
	    	  $uibModal.open({
	    	      templateUrl: 'app/vendors/html/vendor-popup.html',
	    	      controller: 'InstituteSearchController',
	    	      scope : $scope
	    	    });
	     };
		
	     /** Remove item from selected institutes */	     
	     $scope.removeInstitute = function(id){
	    	 for (var i = 0; i< $scope.vendor.institutes.length; i++){
	    		 var institute = $scope.vendor.institutes[i];
   			 if (institute.branchId == id) {
   		    	$scope.vendor.institutes.splice(i, 1);
   		    	$scope.instituteIds.splice(i, 1);
   		    	break;
	    		 }
	    	 }
	     };
	     
	     
		/** This method is used to save edited object. Here we explicitly copy $scope.vendors to normal vendor object. This is done because $scope.vendor contains angular specific properties. */
		$scope.save = function() {
			var vendor = {
					id: $scope.vendor.id,
					merchantLegalName: $scope.vendor.merchantLegalName,
					merchantMarketingName: $scope.vendor.merchantMarketingName,
					bankAccountNumber: $scope.vendor.bankAccountNumber,
					meCode : $scope.vendor.meCode,
					tid : $scope.vendor.tid,
					lgCode : $scope.vendor.lgCode,
					lcCode:$scope.vendor.lcCode,
					branch:$scope.vendor.branch,
					addressLine1:$scope.vendor.addressLine1,
					area:$scope.vendor.area,
					city:$scope.vendor.city,
					pinCode:$scope.vendor.pinCode,
					primaryPhone:$scope.vendor.primaryPhone,
					secondaryPhone:$scope.vendor.secondaryPhone,
					faxNo:$scope.vendor.faxNo,  
					email:$scope.vendor.email,
					shopOwnership: $scope.vendor.shopOwnership,
					otherGroup: $scope.vendor.otherGroup,
					otherGroupAddress:$scope.vendor.otherGroupAddress,
					proprietorName:$scope.vendor.proprietorName,
					proprietorAddressLine1:$scope.vendor.proprietorAddressLine1,
					proprietorAddressArea:$scope.vendor.proprietorAddressArea,
					proprietorAddressCity:$scope.vendor.proprietorAddressCity,
					proprietorAddressPincode:$scope.vendor.proprietorAddressPincode,
					proprietorPrimaryPhone:$scope.vendor.proprietorPrimaryPhone,
					proprietorSecondaryPhone:$scope.vendor.proprietorSecondaryPhone,
					proprietorFaxNo:$scope.vendor.proprietorFaxNo,
					ownership:$scope.vendor.ownership,
					ownershipOther:$scope.vendor.ownershipOther,
					businessStartDate:$scope.vendor.businessStartDate,
					merchantPan:$scope.vendor.merchantPan,
					creditCardAccepted:$scope.vendor.creditCardAccepted,
					creditCardAcceptedOther:$scope.vendor.creditCardAcceptedOther,
					annualTurnOver:$scope.vendor.annualTurnOver,
					annualTotalOnCreditCard:$scope.vendor.annualTotalOnCreditCard,
					averagePerTransaction:$scope.vendor.averagePerTransaction,
					creditCardBank:$scope.vendor.creditCardBank,
					creditCardSinceYear:$scope.vendor.creditCardSinceYear,
					ebSavingAccount:($scope.vendor.ebSavingAccount == true ? "Y" : "N"),
					ebCurrentAccount:($scope.vendor.ebCurrentAccount == true ? "Y" : "N"),
					ebTermDeposit:($scope.vendor.ebTermDeposit == true ? "Y" : "N"),
					ebDemat:($scope.vendor.ebDemat == true ? "Y" : "N"),
					ebAutoLoan:($scope.vendor.ebAutoLoan == true ? "Y" : "N"),
					ebPersonalLoan:($scope.vendor.ebPersonalLoan == true ? "Y" : "N"),
					ebTwoWheeler:($scope.vendor.ebTwoWheeler == true ? "Y" : "N"),
					ebCreditCard:($scope.vendor.ebCreditCard == true ? "Y" : "N"),
					ebLoanAgainstShares:($scope.vendor.ebLoanAgainstShares == true ? "Y" : "N"),
					ebBusinessBanking:($scope.vendor.ebBusinessBanking ==true ? "Y" : "N"),
					ebInsurance:($scope.vendor.ebInsurance == true ? "Y" : "N"),
					ebMutualFund:($scope.vendor.ebMutualFund == true ? "Y" : "N"),
					cardAcceptTypeVisa:($scope.vendor.cardAcceptTypeVisa == true ? "Y" : "N"),
					cardAcceptTypeMaster:($scope.vendor.cardAcceptTypeMaster == true ? "Y" : "N"),
					cardAcceptTypeDinner:($scope.vendor.cardAcceptTypeDinner == true ? "Y" : "N"),
					physicalMpr:($scope.vendor.physicalMpr == true ? "Y" : "N"),
					emailMpr:($scope.vendor.emailMpr == true ? "Y" : "N"),
					emailMprText:$scope.vendor.emailMprText,
					mprFrequency:$scope.vendor.mprFrequency,
					sellingCategory:$scope.vendor.sellingCategory,
					userId:$scope.vendor.userId,
					active:$scope.vendor.active,
					isDelete: $scope.vendor.isDelete
			};
			
			Upload.upload({
	            url: 'vendors/save',
	            method: 'POST',
                data : vendor,
	            file: $scope.vendor.photo,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
	        		$state.transitionTo('vendor.list',
	        		{
						status : resp.data.status, 
	        			message : resp.data.message
					},{reload : true});
	        	}
	        	else {
	        		$scope.status = resp.data.status;
	        		$scope.message = resp.data.message;
	        	}
	        });
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancelPopup = function(){
			$uibModal.dismiss('cancel');
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('vendor.list');
		};
	});
	
	/** This method is used for uploading data */
	/*angular.module('vendor').controller('VendorFileUploadController',function($scope,Upload, $uibModalInstance) {
	
		$scope.upload = function (file) {
	        Upload.upload({
	            url: 'vendors/uploadFile',
	            file: file
	        }).then(function(resp) {
	        	$uibModalInstance.dismiss('cancel');
	        //	$scope.$parent.isUploadFailed = (resp.data.status == 'fail' ? true : false);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	        	angular.extend($scope.vendors,resp.data.records);       	
	        });
	    };
	    
	    $scope.cancel = function(){
	    	$uibModalInstance.dismiss('cancel');
	    };
	});*/
	
	
	angular.module('vendor').controller('InstituteSearchController',['$scope','$resource','$uibModalInstance',function($scope,$resource,$uibModalInstance) {
		console.log($scope);
		$scope.clickedForSearch = false;
		$scope.searchedInstitutes = [];
		/** This method is used to search institutes based on provided details*/
		$scope.search = function(){
			console.log($scope.searchData);
			$scope.searchedInstitutes = [];
			 $resource('masters/searchForInstitute?name='+$scope.searchData.name+
					 '&allreadySelectedIdss='+$scope.instituteIds).query().$promise.then(function(data) {
	    		 if(data.length > 0){
		    		 $scope.hasSearchRecords = true;
		    		 $scope.clickedForSearch = false;
		    		 angular.forEach( data, function(institute){
		    			 institute.isSelected = false;
			    	 });
		    		 angular.extend($scope.searchedInstitutes,data);
		    	 }
	    		 else {
	    			 $scope.hasSearchRecords = false;
	    			 $scope.clickedForSearch = true;
	    		 }
	 		 });
	     };
	     
	     /** Add institutes to an array */
	     $scope.addInstitutes  = function(){	    
	    	 console.log($scope.vendor.institutes);
	    	 angular.forEach( $scope.searchedInstitutes, function(institute){
	    		 if (institute.isSelected) {
    		    	$scope.vendor.institutes.push(institute);
    		    	$scope.instituteIds.push(institute.branchId);
    		    }
	    	 });
	    	 $uibModalInstance.dismiss('cancel');
	     };
	     
	     $scope.cancelPopup = function(){
		    	$uibModalInstance.dismiss('cancel');
		    };
	}]);
})();