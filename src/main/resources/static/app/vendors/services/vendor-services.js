(function(){
	angular.module('fees').service('FeesService',function($resource) {
		
		return $resource('getFeesList.do');
	});

	angular.module('fees').service('FeesCRUDService',function(FeesService) {
		var feesList = [];
		
		function saveFeesData(fees){
			console.log(fees);
			if(fees.id == null) {
		        // if this is new contact, add it in contacts array
				fees.id = feesList.length + 1;
		        students.push(fees);
		        } else {
		        //for existing contact, find this contact using id
		        //and update it.
		        for(i in feesList) {
		            if(feesList[i].id == fees.id) {
		            	feesList[i] = fees;
		            }
		        }                
		        }
		       
		}
		
		function setFeesList(feesList1){
			feesList = feesList1;
		}
		
		function getFeesData(id){
			//var students = StudentService;
			var fees = {};
			for(i in feesList) {
		         if(feesList[i].id == id) {
		             fees =feesList[i];
		         }
		     }
			return fees;
		}
		
		return  {
			saveFeesData : saveFeesData,
			setFeesList : setFeesList,
			getFeesData : getFeesData
		};
	});
	
	
	/*angular.module('student').service('StudentService',function($rootScope) {
		
	});*/
	 

})();