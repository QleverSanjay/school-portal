(function(){

/*	angular.module('profile').directive("compareTo", function() {
	    return {
	        require: "ngModel",
	        scope: {
	            otherModelValue: "=compareTo"
	        },
	        link: function(scope, element, attributes, ngModel) {
	             
	            ngModel.$validators.compareTo = function(modelValue) {
	                return modelValue == scope.otherModelValue;
	            };
	            scope.$watch("otherModelValue", function() {
	                ngModel.$validate();
	            });
	        }
	    };
	});*/
	
	angular.module('profile').controller('validateTeacherForm',function($resource,$scope,$state) {
		
		$scope.regExName = /^[A-Z a-z]{2,45}$/;
		$scope.regExMiddleName = /^[A-Z a-z]{1,45}$/;
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,45}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExPassword =  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}/;
	});
	
	/*angular.module('profile').controller('validateVendorForm',function($resource,$scope,$state) {
		alert("Profile controlelr..");
		$scope.regExName = /^[A-Z a-z]{2,45}$/;
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,45}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExPassword =  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;
	});*/

	
	angular.module('profile').controller('TeacherProfileController',function($scope,$rootScope, $state,$uibModal, Upload,$resource, StateList) {
		/** This is written to get single profile object to display on the edit screen. */
		$scope.teacher = {};
		$scope.isUpdateProfile = true;
		$scope.stateList = StateList;
		$scope.districtList = [];
		$scope.talukaList = [];

		$resource('teachers/:id').get({id : $rootScope.entityId}).$promise.then(function(teacher) {
			teacher.dateOfBirth = new Date(teacher.dateOfBirth);
			angular.extend($scope.teacher,teacher);
			changeDistrictList();
		});

		$scope.changeDistrictList = function(){
    		changeDistrictList();
		};

		function changeDistrictList(){
			
			if($scope.teacher.stateId){
				$resource('masters/districtMasters/:id').query({id : $scope.teacher.stateId})
					.$promise.then(function(districtList) {
						$scope.districtList.length = 0;
						angular.extend($scope.districtList, districtList);
						changeTalukaList();
				});	
			} 
		};

		$scope.changeTalukaList = function(){
			changeTalukaList();
		};
		
		function changeTalukaList(){
			
			if($scope.teacher.districtId){
				$resource('masters/talukaMasters/:id').query({id : $scope.teacher.districtId})
					.$promise.then(function(talukaList) {
						$scope.talukaList.length = 0;
						angular.extend($scope.talukaList, talukaList);
				});	
			}
		};

		/** This method is used to save edited object. Here we explicitly copy $scope.profiles to normal profile object. This is done because $scope.profile contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			var teacher = {
					id: $scope.teacher.id,
					emailAddress : $scope.teacher.emailAddress,
					primaryContact : $scope.teacher.primaryContact,
					secondaryContact : $scope.teacher.secondaryContact,
					addressLine : $scope.teacher.addressLine,
					password : $scope.teacher.password,
					area: $scope.teacher.area,
					city: $scope.teacher.city,
					pinCode : $scope.teacher.pinCode,
					firstName: $scope.teacher.firstName,
					userId : $scope.teacher.userId
			};

			/** This method is used to adding event. It will only call add utility method */
			Upload.upload({
	            url: 'teachers/updateProfile',
	            method: 'POST',
                data : teacher,
	            file: $scope.teacher.photo,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
	        		$scope.status = resp.data.status;
	        		$scope.message = resp.data.message;
	        	}
	        	else {
	        		if(resp.data.isValidationError){
						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						 $uibModal.open({
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 });
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });
		};
	});
	
	
	angular.module('profile').controller('VendorProfileController',function($scope,$rootScope,$state,Upload,$resource) {
		/** This is written to get single profile object to display on the edit screen. */
		$scope.vendor = {};

		$resource('vendors/:id').get({id : $rootScope.id}).$promise.then(function(vendor) {
			angular.extend($scope.vendor,vendor);
		});

		/** This method is used to save edited object. Here we explicitly copy $scope.profiles to normal profile object. This is done because $scope.profile contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			var profile = {
					id: $scope.vendor.id,
					userId: $scope.vendor.userId,
					email : $scope.vendor.email,
					mobile:$scope.vendor.mobile,
					address: $scope.vendor.address,
					area: $scope.vendor.area,
					city: $scope.vendor.city,
					pinCode : $scope.vendor.pinCode,
			};

			/** This method is used to adding event. It will only call add utility method */
			Upload.upload({
	            url: 'student/profile/save',
	            method: 'POST',
                data : profile,
	            file: $scope.profile.photo,
	        }).then(function(resp) {
        		$scope.status = resp.data.status;
        		$scope.message = resp.data.message;
	        });
		};
	});


	angular.module('profile').controller('UpdateProfileControllerFirstTimeLogin',function($scope, $filter, 
			$rootScope,$state,Upload,$resource,$http, userserviceapiurl,$localStorage) {
		$scope.maxDate=new Date();
		$scope.username=$localStorage.user;
		$scope.profile = {};
		$rootScope.hideLinks = true;

		$resource(userserviceapiurl + '/teacher').get().$promise.then(function(response) {
			console.log("success=====");
			$scope.profile.firstname=response.firstname;
			$scope.profile.lastname=response.lastname;
			$scope.profile.email=response.email;
			$scope.profile.primaryContact=response.primaryContact;
			$scope.profile.dob=new Date(response.dob);
			$scope.profile.gender=response.gender;
			
		}, function (err){
			console.log("err-----"+err);
			$scope.message = "Error please contact admin";
		});
		
		//on click save and continue
		$scope.save = function() {
		$scope.status = '';
		$scope.message = '';
		
		var url="teachers/updateProfileFirstTime";
		var teacherProfile = {
				loginCount:$rootScope.loginCount,
				userId:$rootScope.id,
				roleId:$rootScope.roleId,
				firstName: $scope.profile.firstname,
				lastName: $scope.profile.lastname,
				emailAddress: $scope.profile.email,
				primaryContact: $scope.profile.primaryContact,
				dateOfBirth: $filter('date')($scope.profile.dob, "yyyy-MM-dd"),
				gender: $scope.profile.gender
		};

		$resource(url).save(teacherProfile).$promise.then(function(response) {
			console.log("success=====");
			$rootScope.hideLinks =false;
			$rootScope.loginCount=2;
			
			localStorage.setItem('updateProfile',true);
			$state.transitionTo('notice.list',{reload : true});	
		}, function (err){
			console.log("err-----"+err);
			$scope.message = "Error while updating profile.";
		});

	
		};
	
		
		
		
		
	});
	
	
	angular.module('profile').controller('validateProfileFirstTimeLogin',function($resource,$scope,$state) {
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.maxDate=new Date();
		
	});
	
	
	
})();




