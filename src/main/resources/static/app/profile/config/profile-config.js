(function(){
	angular.module('profile').config(['$stateProvider', function($stateProvider) {
		$stateProvider.state('profile', {
			url : '/profile',
			abstract :true,
			controller : 'ProfileModelController',
			/*data: {
				 breadcrumbProxy: 'student.list'
	        }*/
		}).state('profile.teacher', {
			url : '/update/teacher/',
			views : {
				'@' : {
					templateUrl : 'app/teachers/html/teacher-add.html',
					controller : 'TeacherProfileController'
				}
			},
			data: {
	            displayName: 'Update Profile'
	        },
	        resolve : {
	        	StateList : function($resource){
	        		return $resource('masters/stateMasters').query().$promise;
	        	}
	        }
		}).state('profile.vendor', {
			url : '/update/vendor/',
			views : {
				'@' : {
					templateUrl : 'app/vendors/html/vendor-add.html',
					controller : 'VendorProfileController'
				}
			},
			data: {
	            displayName: 'Update Profile'
	        },
		}).state('profile.updateFirstTime', {
			url : '',
			views : {
				'@' : {
					templateUrl : 'app/profile/html/updateProfileFirstTimeLogin.html',
					controller : 'UpdateProfileControllerFirstTimeLogin'
				}
			},
			data: {
	            displayName: 'updateProfile'
	        },
			
			 params : {
		        	message : '',
		        	status : ''
		        }
			});
	}]);
	
	
	
})();