'use strict';

angular.module('school')
    .factory('AuthServerProvider', function loginService($http) {
        return {
            login: function(credentials) {
                var data = 'j_username=' + encodeURIComponent(credentials.username) +
                    '&j_password=' + encodeURIComponent(credentials.password) +
                    '&submit=Login';
                return $http.post('api/authentication', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },
            logout: function() {
                // logout from the server
                $http.post('api/logout').success(function (response) {
                    return response;
                });
            }
        };
    });
    
angular.module('student').factory('InitializationService', function($localStorage,$rootScope,$state) {

	function fnInitialize(){
		var data = $localStorage.user;
		if(data != undefined){
			$rootScope.login = true;
		}else{
			$rootScope.login = false;
		}
	
	}
	$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams){
		console.log(toState);
		if($localStorage.user == undefined && toState.name != 'login.log'){
			$state.transitionTo('login.log',{reload : true});
		}
	});
  // factory function body that constructs shinyNewServiceInstance
  return {
	  initialize : fnInitialize
  };
});

angular.module('student').factory('CheckRole', function($localStorage,$rootScope) {

	function checkRoleExist(roleName){
		var roles = $localStorage.roles;
		for(var i = 0;i<roles.length;i++){
			if(roles[i].name == roleName){
				return true;
			}
		}
		return false;
		
	}
	
	function roleName(){
		var roleNames = {
			"ADMIN" : "ADMIN",
			"STUDENT" : "STUDENT",
			"PARENT" : "PARENT",
			"TEACHER" : "TEACHER",
			"VENDOR" : "VENDOR",
			"SCHOOL_ADMIN" : "SCHOOL ADMIN"
		};
		
		
		return roleNames;
	}
	
  return {
	  validateRole : checkRoleExist,
	  roleName : roleName
  };
});
