(function(){
	angular.module('login').controller('LoginController',function($resource,$scope,$rootScope,$state,$uibModal,CommonServices) {
		
		$scope.logins = [];
		$scope.$root.loginErrorMessage = "";
		$rootScope.instituteLogoUrl = null;
		$rootScope.instituteName = null;
		angular.extend($scope.logins);
		$scope.$root.loggedIn = false;		
	});


	angular.module('login').controller('ValidLoginController',function(MenuFactory,$scope,$state, $stateParams,$resource,forumserviceapiurl,
			CommonServices,AuthServerProvider,$rootScope,$localStorage, userserviceapiurl, Idle) {

		$scope.login = {}; 
		$rootScope.menus = [];
		$scope.$root.loginErrorMessage = "";
		$scope.menus = [];
		$scope.message = $stateParams.message;
		localStorage.clear();

		function loadUnreadData (){
			$resource(userserviceapiurl + 'notification/unread').get().$promise.then(function(response) {
				$scope.alertsLoaded = true;
				//unread_notifications
				if(response != null){
					$rootScope.unreadNotifications = response.unread_notifications;
					$localStorage.unreadNotifications = JSON.stringify($rootScope.unreadNotifications);
				}
			});
		}

		$scope.save = function() {
//			AuthServerProvider.login({
//                username: $scope.login.username,
//                password: $scope.login.password
//            }).success(function (data) {
				$resource('/users/user-role').save({username : $scope.login.username, password : $scope.login.password}).$promise.then(function(data) {
					$localStorage.user_info = data;
					$scope.$root.loggedIn = true;
					$rootScope.login = true;
					$localStorage.user = $scope.login.username;
					$rootScope.menus = data.menus;
					$rootScope.rolePermissions = data.rolePermissions;
					$rootScope.role = data.roleId;
					$rootScope.roleId = data.roleId;
					$rootScope.instituteId = data.instituteId;
					$rootScope.branchId = data.branchId;
					$rootScope.id = data.id;
					$rootScope.instituteLogoUrl = data.instituteLogoUrl;
					$rootScope.instituteName = data.instituteName;
					$rootScope.loggedIn = true;
					$rootScope.loggedInUser = data;
					$rootScope.entityId = $rootScope.loggedInUser.entityId;
					$rootScope.username =  $rootScope.loggedInUser.username;
					$rootScope.name = $localStorage.user_info.name;
					$rootScope.loginCount = data.loginCount;
					if(data.loginCount==1){
						//purpose to preventing user to access page url without change password and update profile
					localStorage.setItem('changePassword',false);
					localStorage.setItem('updateProfile',false);
					}
					MenuFactory.setMenus(data.menus);
					angular.extend($scope.menus,data.menus);

					localStorage.setItem('token', data.token);
					if(data.roleId == 4){
						loadUnreadData();	
					}
					$scope.$root.loginErrorMessage = "";
					
					if(data.chatProfile != null){
						var chatSession = data.chatProfile.session;
						localStorage.setItem('chatSessionId', chatSession.chat_user_profile_id);
						localStorage.setItem('chatSessionPass', chatSession.password);
						localStorage.setItem('chatSessionToken', chatSession.token);
						localStorage.setItem('chatSessionAppId', chatSession.application_id);
					}
					
					if(data.forumProfile != null){
						$resource(forumserviceapiurl+'api/auth.php?action=login').save( {	
							username : data.forumProfile.login,
							user_password : data.forumProfile.password
							}).$promise.then(function(resp) {
							
							console.log(resp);
							if(resp.success != null){
								localStorage.setItem('forumSID', resp.success.sid);
							}
						});
					}

					var shoppingProfile = data.shoppingProfile;
					if(shoppingProfile != null){
						localStorage.setItem('shoppingCookiesExpiresOn', shoppingProfile.cookiesExpiresOn);
						console.log("This is for you....", shoppingProfile.cookies);
						document.cookie = shoppingProfile.cookies+"path=/shopping";
						document.cookie =  "app_code=defand1;path=/shopping";
						document.cookie = "screen_size=1600x381; path=/shopping";	
					}
					
					if(data.roleId == 1){
						//$state.transitionTo('institute.list',{reload : true});
						$state.transitionTo('dashboard.page',{reload : true});
					}
					else if(data.roleId == 6){
						$state.transitionTo('dashboard.page',{reload : true});
						//$state.transitionTo('student.list',{reload : true});						
					}else if(data.roleId == 8){
						$state.transitionTo('auditLog.page',{reload : true});						
					} 
					else if(data.roleId == 4){
						localStorage.setItem('loginCount', data.loginCount);
						if(data.loginCount>1){
							$resource(userserviceapiurl + 'teacher').get().$promise.then(function(response) {
								console.log("success=====");
								if(response.firstname == undefined || response.lastname == undefined || response.email == undefined ||response.primaryContact == undefined || response.dob == undefined || response.gender == undefined ){
									$state.go('profile.updateFirstTime');	
								}
								else {
									$state.transitionTo('notice.list',{reload : true});	
									
								}
								
							}, function (err){
								console.log("err-----"+err);
								$scope.message = "Error please contact admin";
							});
						}
						else{
							$rootScope.hideLinks = true;
							$state.transitionTo('login.changePassword',{reload : false});	
						}
					}else{
						$scope.$root.loginErrorMessage = "User is not authorized to access the URL";
						$scope.$root.loggedIn = false;
						$rootScope.login = false;
					}
					$rootScope.welcomeMessage = "Welcome ";
					Idle.watch();
				}, function (data) {
	            	if(data.status == 401){
	            		$scope.$root.loginErrorMessage = "You are unauthorized to access this page !";
	            	}
	            	else {
	            		$scope.$root.loginErrorMessage = "Username password does not match";	
	            	}
	            });
		};
	});

	angular.module('login').controller('LogoutController',function($rootScope,$localStorage,$scope,$state,forumserviceapiurl, 
			$resource,CommonServices,AuthServerProvider, shoppingurl, Idle, userserviceapiurl, $http) {
		AuthServerProvider.logout();

		var sid = localStorage.getItem('forumSID');
		$resource(forumserviceapiurl+"ucp.php?mode=logout&sid="+sid).get().$promise.then(function(data) {});
		$resource(shoppingurl+"Customer/account/logout").get().$promise.then(function(data) {});

		$http({
	        url: userserviceapiurl+"user/logout",
	        method: "PUT",
	    }).then(function(response) {console.log("LOGGEDD OUT FROM SERVICE----");}, function(err) {});

		$localStorage.user = null;
		$localStorage.user_info = null;
		$scope.$root.loggedIn = false;
		$rootScope.login = false;
		$rootScope.instituteLogoUrl = null;
		$rootScope.instituteName = null;
		$scope.$root.loggedInUser = {};
		Idle.unwatch();
		localStorage.clear();
		$state.transitionTo('login.log',{reload : true});
	});
	
	angular.module('login').controller('ResetPasswordController',function($resource,$scope,$state, userserviceapiurl ) {
		$scope.email = "";
		$scope.message = "";
		$scope.otp = "";
		$scope.newPassword = "";
		$scope.regExPassword =  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$~!@#$%^&*()+=_-{}])[A-Za-z\d$~!@#$%^&*()+=_-{}]{8,15}/;

		$scope.submitforgotPasswordFormOTP = function (){
			var passwordDetails = {
				otp : $scope.otp,
				new_password : $scope.newPassword
			};

			$resource(userserviceapiurl + "user/password/reset").save(passwordDetails)
				.$promise.then(function(data) {
					var message = "Your password has been changed please login.";
					$state.transitionTo('login.log',{message : message},{reload : true});    
			},
			function (err){
				console.log("Error while resetting password.."+err);
			});
		}
	});


	angular.module('login').controller('PasswordValidationController',function($resource,$scope,$state) {
		$scope.regExPassword =  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$~!@#$%^&*()+=_-{}])[A-Za-z\d$~!@#$%^&*()+=_-{}]{8,15}/;
	});
	
	angular.module('login').controller('ForgotPasswordController',function($scope,$state,$resource,$http, $uibModal, userserviceapiurl) {
		$scope.username = "";
		$scope.message = "";

		$scope.cancel = function (){
			$state.go("login.log");
		};

		$scope.save = function (){
			var url = userserviceapiurl + "user/password?email="+$scope.username;
			var headers = { 'Content-Type': 'application/json' };

			$http.get(url, headers).success(function (response) {
				var message = "One time password (OTP) has been sent on "+$scope.username;
				$state.transitionTo('login.resetPassword',{message : message},{reload : true});
			}).error(function (err) {
				console.log("Error While checking email for otp....", err);
			});
		};
	});

	angular.module('login').controller('ChangePasswordController',function($scope,$rootScope,$state,$http, userserviceapiurl) {
		$scope.changePasswordObj = {};
		var loginCount = localStorage.getItem("loginCount");
		if(loginCount == 1){
			$rootScope.hideLinks = true;
		}
		$scope.changeMyPassword = function (){
			var url = userserviceapiurl + "user/password";

			var headers = { 'Content-Type': 'application/json' };
			var passwordDetails = {
				current_password : $scope.changePasswordObj.currentPassword,
				new_password : $scope.changePasswordObj.newPassword
			};

			$http.put(url, passwordDetails, headers)
				.success(function (response) {
					console.log(response);
					var loginCount = localStorage.getItem("loginCount");
					if(loginCount == 1){
						localStorage.setItem("loginCount", null);
						localStorage.setItem('changePassword',true);
					
						$state.transitionTo('profile.updateFirstTime',{reload : true});
					}
					else {
						$scope.message="Your password has been changed.";
					}
				})
				.error(function (err) {
					$scope.message="Please enter the valid current password";
					console.log(err);
				});
		};

		$scope.sendNewOtp = function (){
			$scope.message = "";
			$resource('users/forgotPassword').save($scope.username).$promise.then(function(data) {
				$scope.message = data.message;
			});
		};
	});
	
	
})();

