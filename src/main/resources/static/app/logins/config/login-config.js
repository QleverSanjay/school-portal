(function(){
	angular.module('login').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('login', {
			url : '',
			abstract :true,
			controller : 'LoginModelController',
			data: {
				 breadcrumbProxy: 'login.log'
	        }
		}).state('login.log', {
			url : '/login',
			views : {
				'@' : {
					templateUrl : 'app/logins/html/login-page.html',
					controller : 'ValidLoginController'
				}
			},
			params : {message : ''},
			data: {
	            displayName: 'Login'
	        }
		}).state('login.logout',{
			url : '/logout',
			views : {
				'@' : {
					/*templateUrl : 'app/logins/html/login-page.html',*/
					controller : 'LogoutController'
				}
			},
			data: {
	            displayName: 'Logout'
	        }			
		}).state('login.forgotPassword',{
			url : '/forgotPassword',
			views : {
				'@' : {
					templateUrl : 'app/logins/html/forgot-password.html',
					controller : 'ForgotPasswordController'
				}
			}
		}).state('login.resetPassword',{
			url : '/reset/password',
			views : {
				'@' : {
					templateUrl : 'app/logins/html/reset-password-OTP.html',
					controller : 'ResetPasswordController'
				}
			},
			params : {
				message : "",
				username : ""
			}
		}).state('login.changePassword',{
			url : '/change/password',
			views : {
				'@' : {
					templateUrl : 'app/logins/html/change-password-page.html',
					controller : 'ChangePasswordController'
				}
			},
			params : {
				message : "",
				username : ""
			}
		})/*.state('login.changePasswordFirstTime',{
			url : '/change/password',
			views : {
				'@' : {
					templateUrl : 'app/logins/html/change-password-page.html',
					controller : 'ChangePasswordFirstTime'
				}
			},
			params : {
				message : "",
				username : ""
			}
		})*/;
	}]);
})();