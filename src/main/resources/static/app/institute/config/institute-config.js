(function(){
	angular.module('institute').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('institute', {
			url : '/institute',
			abstract :true,
			controller : 'InstituteModelController',
			data: {
				 breadcrumbProxy: 'institute.list'
			}
		}).state('institute.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/institute/html/institute-list.html',
					controller : 'InstituteController'
				}
			},
			data: {
	            displayName: 'Institute List'
	        },
	        params : {
	        	message : '',
	        	status : ''
	        },
	        resolve : {
	        	InstituteList : function($resource){
	        		return $resource('institute').query().$promise;
	        	}
	        }
		}).state('institute.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/institute/html/institute-add.html',
					controller : 'InstituteAddController'
				}
			},
			data: {
	            displayName: 'Add Institute'
	        }
		}).state('institute.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/institute/html/institute-add.html',
					controller : 'InstituteEditController'
				}
			},
			data: {
	            displayName: 'Edit Institute'
	        }
		});
	}]);
})();