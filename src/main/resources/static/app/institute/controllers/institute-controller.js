(function(){
	angular.module('institute').controller('InstituteController',function(ValidateButtonLinkUrl,$resource,$stateParams,DTOptionsBuilder,$confirm,InstituteList,$scope,$state,modalPopupService , CommonServices) {
    	$scope.institutes = [];

    	$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", true)
		.withOption("scrollX", true)
        .withOption("responsive", true);

    	angular.extend($scope.institutes,InstituteList);

    	$scope.showButtons = function(buttonName){
    		return ValidateButtonLinkUrl.validateButtonUrl(buttonName);
    	};
    	
    	$scope.status = $stateParams.status;
    	$scope.message = $stateParams.message;
    	$scope.isUploadingFailed = false;


    	/** This method is used to open Modal */ 
	    $scope.uploadInstitutes = function(){
	    	var options = {
	    	      templateUrl: 'app/institute/html/institute-bulk-upload.html',
	    	      controller: 'InstituteFileUploadController',
	    	      scope : $scope
	    	};
	    	modalPopupService.openPopup(options);
	     };

	     /** This method is used to delete school from the screen. It will only call delete utility method */
	     $scope.deleteData = function deleteInstitute(id){
	    	 //CommonServices.deleteInstitute('institute',id);
	    	 $confirm({text: 'Are you sure you want to delete?'}).then(function() {
		    	 $resource('institute').remove({id : id}).$promise.then(function(resp) {
		   	 		for(var i =0; i < $scope.institutes.length ;i ++ ){
	  			  		var institute = $scope.institutes[i];
	  			  		if(institute.id == id){
	  			  			$scope.institutes.splice(i , 1);
	  			  			break;
	  			  		}
	  			  	}
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
				 });
	    	 });
	     };
	});


	angular.module('institute').controller('validateInstituteForm',function($resource,$scope,$state) {
		/*$scope.regExInstitute = /^[A-Z a-z , . () -]{2,100}$/;*/
		$scope.regExName = /^[A-Z a-z]{2,45}$/;
		$scope.regExMobile= /^[0-9]{10,10}$/;
		$scope.regExPinCode = /^[0-9]{6,6}$/;
		$scope.regExArea = /^[A-Z a-z 0-9 , . () -]{2,45}$/;
		$scope.regExCity = /^[A-Z a-z]{2,45}$/;
		$scope.regExEmail = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExEmailAdmin = /^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$/;
		$scope.regExUrl = /^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$/;
		$scope.regExCode = /^[a-zA-Z0-9]{4,16}$/;

	});

	angular.module('institute').controller('InstituteAddController',function($scope,$state,Upload,modalPopupService , $resource,CommonServices) {
		$scope.institute = {}; 
		$scope.isSaveDisabled = false;
		$scope.instituteNameValue = "Add Institute";
		
		$scope.validationErrMsg = "";
		$scope.validationErrors = {};
		
		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var institute = {
					adminCity: $scope.institute.adminCity,
					adminEmail: $scope.institute.adminEmail,
					adminPinCode: $scope.institute.adminPinCode,
					firstName: $scope.institute.firstName,
					gender: $scope.institute.gender,
					id: $scope.institute.id,
					code : $scope.institute.code,
					instituteName: $scope.institute.instituteName,
					lastName: $scope.institute.lastName,
					primaryContact: $scope.institute.primaryContact,
					active: $scope.institute.active,
					isDelete: $scope.institute.isDelete
			};
			
			/** This method is used to adding institute. */
			Upload.upload({
	            url: 'institute/save',
	            method: 'POST',
                data : institute,
	            file: $scope.institute.logoUrl,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
		        	$state.transitionTo('institute.list',
			        		{
			        			status : resp.data.status, 
			        			message : resp.data.message
			        		},
			        		{reload : true}
			        );
	        	}
	        	else{
	        		$scope.isSaveDisabled = false;
	        		if(resp.data.isValidationError){

						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	     controller: "ValidationErrorController",
				    	     scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });		
		};

		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('institute.list');
		};
	});
	
	
	angular.module('institute').controller('InstituteEditController',function($scope,$state,modalPopupService , $confirm, Upload, $stateParams,$resource,CommonServices) {
		/** This is written to get single school object to display on the edit screen. */
		$scope.institute = {};
		$scope.isSaveDisabled = false;
		$scope.instituteNameValue = "Edit Institute";

		$scope.validationErrMsg = "";
		$scope.validationErrors = {};

		$resource('institute/:id').get({id : $stateParams.id}).$promise.then(function(institute) {
			angular.extend($scope.institute,institute);
		});

		$scope.showPopupWhenInactive = function (){
			if($scope.institute.active == 'N'){
				$confirm({title: 'Are you sure ?', text : 'If institute is inactive then all '
					+'data related to this institute will be inactivated.'}).then(function() {
		    	 },
		    	 function(){
		    		 $scope.institute.active = 'Y';
		    	 });
			}
		}

		/** This method is used to save edited object. Here we explicitly copy $scope.schools to normal school object. This is done because $scope.school contains angular specific properties. This will be removed once I get the solution for it. */
		$scope.save = function() {
			$scope.isSaveDisabled = true;
			var institute = {
					adminCity: $scope.institute.adminCity,
					adminEmail: $scope.institute.adminEmail,
					adminPinCode: $scope.institute.adminPinCode,
					firstName: $scope.institute.firstName,
					gender: $scope.institute.gender,
					id: $scope.institute.id,
					code : $scope.institute.code,
					instituteName: $scope.institute.instituteName,
					lastName: $scope.institute.lastName,
					primaryContact: $scope.institute.primaryContact,
					active: $scope.institute.active,
					isDelete: $scope.institute.isDelete
			};

			/** This method is used to adding institute. */
			Upload.upload({
	            url: 'institute/save',
	            method: 'POST',
                data : institute,
	            file: $scope.institute.logoUrl,
	        }).then(function(resp) {
	        	if(resp.data.status == 'success'){
		        	$state.transitionTo('institute.list',
			        		{
			        			status : resp.data.status, 
			        			message : resp.data.message
			        		},
			        		{reload : true}
			        );
	        	}
	        	else{
	        		$scope.isSaveDisabled = false;
	        		if(resp.data.isValidationError){
						$scope.validationErrMsg = resp.data.records.title;
						$scope.validationErrors = resp.data.records.errors;
						var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	     controller: "ValidationErrorController",
				    	     scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.data.status;
						$scope.message = resp.data.message;	
					}
	        	}
	        });		
		};
		
		/** This method is used to cancel add operation. It will only call cancel utility method */
		$scope.cancel = function(){
			CommonServices.cancelOperation('institute.list');
		};
	});
	
	/** This method is used for uploading data */
	angular.module('institute').controller('InstituteFileUploadController',function($scope,Upload, modalPopupService, $uibModalStack) {

    	$scope.upload = function (file) {
	        Upload.upload({
	            url: 'institute/uploadFile',
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}

	        	$scope.$parent.status = resp.data.status;
	        	$scope.$parent.message = resp.data.message;
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.institutes.length = 0;
	        	angular.extend($scope.institutes,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
	    	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
})();