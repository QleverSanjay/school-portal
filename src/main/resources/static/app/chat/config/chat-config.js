(function(){
	angular.module('chat').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('chat', {
			url : '/chat',
			abstract :true,
			controller : 'ChatModelController',
			data: {
				 breadcrumbProxy: 'chat.list'
			}
		}).state('chat.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/chat/quickblox.html',
					controller : 'ChatController'
				}
			},
			data: {
	            displayName: 'Chat'
	        }

		});
	}]);
})();