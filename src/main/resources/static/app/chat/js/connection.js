//"use strict";

var currentUser;

$(document).ready(function() {

	//var sessionToken = '653f26021e49cc3cbb0600df2075e9476704fd17';
	//var user_id = '7225876';
	
	/* currentUser = {
        id: 7225876,
        pass: 'wqSjgPkri3',
        token: '7566cff0aa7a0587596bce929fcac989ecd98d81',
		appId: 30029
    } */
	
	currentUser = localStorage.getItem('chatSessionDetails');
	if(currentUser != null){
		QB.initCustom(currentUser.token,currentUser.appId);
		 //g.creds.appId = user.appId;
		
		 // save session token
	      token = currentUser.token;

	      //user.id = currentUser.id;
	      mergeUsers([{user: currentUser}]);

	      QB.chat.connect({userId: currentUser.id, password: currentUser.pass}, function(err, roster) {
	        if (err) {
	          console.log(err);
	        } else {
	          console.log(roster);
	          // load chat dialogs
	          //
	          retrieveChatDialogs();

	          // setup message listeners
	          //
	          setupAllListeners();

	          // setup scroll events handler
	          //
	          setupMsgScrollHandler();
	        }
	      });
	}
	 

  /* $("#loginForm").modal("show");
  $('#loginForm .progress').hide();

  // User1 login action
  //
  $('#user1').click(function() {
    currentUser = QBUser1;
    connectToChat(QBUser1);
  });

  // User2 login action
  //
  $('#user2').click(function() {
    currentUser = QBUser2;
    connectToChat(QBUser2);
  }); */
});

function connectToChat(user) {
  //$('#loginForm button').hide();
  //$('#loginForm .progress').show();

  // Create session and connect to chat
  //
  QB.createSession({login: user.login, password: user.pass}, function(err, res) {
    if (res) {
      // save session token
      token = res.token;

      user.id = res.user_id;
      mergeUsers([{user: user}]);

      QB.chat.connect({userId: user.id, password: user.pass}, function(err, roster) {
        if (err) {
          console.log(err);
        } else {
          console.log(roster);
          // load chat dialogs
          //
          retrieveChatDialogs();

          // setup message listeners
          //
          setupAllListeners();

          // setup scroll events handler
          //
          setupMsgScrollHandler();
        }
      });
    }
  });
}

function setupAllListeners() {
  QB.chat.onDisconnectedListener    = onDisconnectedListener;
  QB.chat.onReconnectListener       = onReconnectListener;
  QB.chat.onMessageListener         = onMessage;
  QB.chat.onSystemMessageListener   = onSystemMessageListener;
  QB.chat.onDeliveredStatusListener = onDeliveredStatusListener;
  QB.chat.onReadStatusListener      = onReadStatusListener;
  setupIsTypingHandler();
}

// reconnection listeners
function onDisconnectedListener(){
  console.log("onDisconnectedListener");
}

function onReconnectListener(){
  console.log("onReconnectListener");
}


// niceScroll() - ON
$(document).ready(
    function() {
        $("html").niceScroll({cursorcolor:"#02B923", cursorwidth:"7", zindex:"99999"});
        $(".nice-scroll").niceScroll({cursorcolor:"#02B923", cursorwidth:"7", zindex:"99999"});
    }
);
