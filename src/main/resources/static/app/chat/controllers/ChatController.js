(function(){
	angular.module('chat').controller('ChatController',function($resource,$scope,$http,$state,$uibModal,CommonServices) {
		
		currentUser = {
		        id: localStorage.getItem('chatSessionId'),
		        pass: localStorage.getItem('chatSessionPass'),
		        token: localStorage.getItem('chatSessionToken'),
				appId: localStorage.getItem('chatSessionAppId')
		    }
		
				
		 QB.initCustom(currentUser.token,currentUser.appId);
		 //g.creds.appId = user.appId;
		
		 // save session token
	      token = currentUser.token;

	      //user.id = currentUser.id;
	      mergeUsers([{user: currentUser}]);

	      QB.chat.connect({userId: currentUser.id, password: currentUser.pass}, function(err, roster) {
	        if (err) {
	          console.log(err);
	        } else {
	          console.log(roster);
	          // load chat dialogs
	          //
	          retrieveChatDialogs();

	          // setup message listeners
	          //
	          setupAllListeners();

	          // setup scroll events handler
	          //
	          setupMsgScrollHandler();
	        }
	      });
		
	});
	
	
})();

