(function(){
	angular.module('receipt').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('receipt', {
			url : '',
			abstract :true,
			controller : 'ReceiptController',
			data: {
				 breadcrumbProxy: 'Receipt.log'
	        }
		}).state('receipt.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/reciept/html/receipt-configuration.html',
					controller : 'receiptController'
				}
			},
			params : {message : ''},
			data: {
	            displayName: 'receipt'
	        }
		});
	}]);
})();