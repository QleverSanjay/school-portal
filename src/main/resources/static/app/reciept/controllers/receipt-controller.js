(function(){
	angular.module('receipt').controller('receiptController',function($rootScope,Upload, $localStorage, $resource, $confirm,DTOptionsBuilder,$stateParams,$scope,$state,$uibModal,CommonServices) {
	
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
    	$scope.branchList = [];
		$scope.instituteList = [];
		
		
		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

	
/*
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isUploadingFailed = false;
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('academicyear/getAcademicYearByBranch?branchId='+$scope.branchId).query().$promise.then(function(academicYearList) {
					$scope.academicYearList.length = 0;

					if(academicYearList && academicYearList.length > 0){
						$scope.activeYearFound = false;
						$scope.isAllLoaded = false;
						for(var i =0; i <academicYearList.length; i++ ){
							var academicYearObj = academicYearList[i];
							if(academicYearObj.isCurrentActiveYear == 'Y'){
								$scope.activeYearFound = true;
							}
						}
						$scope.isAllLoaded = true;
						angular.extend($scope.academicYearList,academicYearList);
					}					
	    		});
			}
		}*/

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				if(branchList.length == 1){
        					$scope.branchId = (!$scope.branchId ? branchList[0].id : $scope.branchId);
        					/* $scope.isSingleBranch = true;
        					$scope.branchName = branchList[0].name;*/
        				}
        				//filterTableByBranch($scope.branchId);
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							$scope.branchId = (isBranchChanged ? branchList[0].id  : (!$scope.branchId ? branchList[0].id : $scope.branchId));
	        				/*if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}*/
							
		        				
		        				//filterTableByBranch($scope.branchId);
		        		}
	        			
						angular.extend($scope.branchList,branchList);
				});	
			}
		};
		$scope.getBranchReceiptConfigration = function(){
			
    		getBranchReceiptConfigration();
		};

		/*getBranch receipt configuration*/
		function getBranchReceiptConfigration(){
			$resource('configuration/receipt?branchId='+$scope.branchId).get()
			.$promise.then(function(receiptConfig) {
				$scope.hideLogo = receiptConfig.hideLogo;
				$scope.useSchoolLogAsWatermarkImage = receiptConfig.useSchoolLogAsWatermarkImage;
				$scope.trailerRecord = receiptConfig.trailerRecord;
			});	
		};
	

		//on click save and continue
		$scope.save = function() {
			var receiptConfig = {
					instituteId:$scope.instituteId,
					branchId:$scope.branchId,
					hideLogo:$scope.hideLogo,
					useSchoolLogAsWatermarkImage:$scope.useSchoolLogAsWatermarkImage,
					trailerRecord: $scope.trailerRecord
			};
			$resource('configuration/receipt').save(receiptConfig)
			.$promise.then(function(resp) {
				if(resp.status == 'success'){
	        		$scope.message=resp.message;
	        		console.log("success");
	        	}
	        	else {
	        		$scope.message=resp.message;
	        		console.log("error*****************************eror");
	        	}
			});
		};
		
		
	});

})();

