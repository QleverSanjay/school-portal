(function(){
	angular.module('timetable').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('timetable', {
			url : '/timetable',
			abstract :true,
			controller : 'TimetableModelController',
			data: {
				 breadcrumbProxy: 'timetable.list'
			}
		}).state('timetable.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/timetable/html/timetable-list.html',
					controller : 'TimetableController'
				}
			},
			data: {
	            displayName: 'timetable List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		}).state('timetable.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/timetable/html/timetable-add.html',
					controller : 'TimetableAddController'
				}
			},
			data: {
	            displayName: 'Add Timetable'
	        }
		})
		.state('timetable.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/timetable/html/timetable-add.html',
					controller : 'TimetableEditController'
				}
			},
			data: {
	            displayName: 'Edit Timetable'
	        }
		})
		.state('timetable.view', {
			url : '/view/:id',
			views : {
				'@' : {
					templateUrl : 'app/timetable/html/timetable-add.html',
					controller : 'TitmetableViewController'
				}
			},
			data: {
	            displayName: 'Edit Timetable'
	        }
		});
	}]);
})();