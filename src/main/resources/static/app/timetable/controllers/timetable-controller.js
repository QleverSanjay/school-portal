(function(){
	angular.module('timetable').controller('TimetableController',function($rootScope, $localStorage, 
			$resource,$filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state,modalPopupService,CommonServices) {

		$scope.standardList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;
	//	angular.extend($scope.standardList,StandardListData);
	    
    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.academicYearList = [];
		$scope.timetableList = [];
		$scope.divisionList = [];

		$scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		$scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;

		$scope.dtOptions =  DTOptionsBuilder.newOptions()
        .withOption("responsive", true);
		
		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		filterTableByBranch();
		};

		$scope.openUploadTimetableDialog = function(){
	    	  var options = {
	    	      templateUrl: 'app/timetable/html/timetable-bulk-upload.html',
	    	      controller: 'TimetableFileUploadController',
	    	      scope : $scope
	    	    };
	    	  modalPopupService.openPopup(options);
	     };
	     
		function filterTableByBranch(){
			if($scope.branchId){
				var url = 'timetable/getTimetableByBranchStandard?branchId='+$scope.branchId;			
				if($scope.standardId) {
					url = url + "&standardId="+$scope.standardId;
			}
				if($scope.divisionId) {
					url = url + "&divisionId="+$scope.divisionId;
				}
				$resource(url).query().$promise.then(function(timetableList) {
					$scope.timetableList.length = 0;
					if(timetableList && timetableList.length > 0){
						angular.extend($scope.timetableList,timetableList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId == 6){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList();
    	}

     	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	if($rootScope.roleId == 4){
    		$scope.branchId = $rootScope.branchId;
    		changeYearList();
    		filteStandardsByTeacher($scope.branchId);
    	}

    	function filteStandardsByTeacher(branchId){
			if($rootScope.id){
				$resource('masters/getFilterdStandardsByTeacher?branchId='+branchId+'&teacherId='+$rootScope.id).query().$promise.then(function(standardList) {
					$scope.standardList.length = 0;
					if(standardList && standardList.length > 0){
						angular.extend($scope.standardList,standardList);
						$scope.standardId = $scope.standardList[0].id;
						changeDivisionList();
					}
	    		});
			}
		}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId || $scope.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId}).$promise.then(function(branchList) {
					if(branchList != null && branchList.length > 0){
        				if(!isBranchChanged){
							filterTableByBranch();
						}
        			}
					angular.extend($scope.branchList,branchList);
					changeYearList();
					changeStandardList();
				});	
			}
		};


		$scope.changeYearList = function(){
			changeYearList();
			changeStandardList();
		};

		function changeYearList(){
			if($scope.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.academicYearId = acadmicYear.id;
								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}	
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
			else {
				$scope.academicYearList.length = 0;
			}
		}

		function changeStandardList(){
			if($scope.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.branchId}).$promise.then(
				 function(standardList) {
					$scope.standardList.length = 0;
					console.log("standardList:: "+standardList);
					angular.extend($scope.standardList,standardList);
					changeDivisionList();
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		$scope.changeDivisionList = function(){
			changeDivisionList();
		};

		// Change divisionList as per the standard selected.
		function changeDivisionList(){
			//$scope.divisionList.length = 0;
			if($scope.standardId){
				for(var i = 0; i< $scope.standardList.length; i++){
					var standard = $scope.standardList[i];
					if(standard.id == $scope.standardId){
						if(standard.divisionList){
							var divisionList = JSON.stringify(standard.divisionList);
							divisionList = JSON.parse(divisionList);
							$scope.divisionList = divisionList;
						}
						break;
					}
				}
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.uploadTimetable = function(){
	    	  var options = {
	    	      templateUrl: 'app/branch/html/branch-bulk-upload.html',
	    	      controller: 'BranchFileUploadController',
	    	      scope : $scope
	    	    };
	    	  modalPopupServie.openPopup(options);
	     };
		
	     $scope.deleteData = function deleteStandard(id){ 
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('timetable').remove({id : id}).$promise.then(function(resp) {
	        		for(var i =0; i < $scope.timetableList.length ;i ++ ){
      			  		var timetable = $scope.timetableList[i];
      			  		if(timetable.id == id){
      			  			$scope.timetableList.splice(i , 1);
      			  			break;
      			  		}
      			  	}	
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});




	angular.module('timetable').controller('TimetableAddController',function($rootScope, $localStorage, $scope,$filter,$state,$stateParams,$resource,CommonServices, modalPopupService, $compile) {

		$scope.standardList = [];
		$scope.timetable = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.divisionList = [];
		$scope.academicYearList = [];
		$scope.timetableHeaderValue = "Add Timetable";

		$scope.nonWorkingDays = [];
		$scope.isSkipTimetableOnHoliday = false;
		$scope.dayNamesWhenSkippedHolidays = ["Day7", "Day1", "Day2", "Day3", "Day4", "Day5", "Day6"];
		$scope.dayNamesWhenNotSkippedHolidays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

		$scope.calendarMinTime = "00:00:00";
		$scope.calendarMaxTime = "24:00:00";

		$scope.alertOnEventClick = function( date, jsEvent, view, index){
	    	console.log(date.title + " ::subject:: "+ date.subjectId);
	        $scope.alertMessage = (date.title + ' was clicked ');
	        date.standardId = $scope.timetable.standardId;
	        editLecturePopup(date);
	    };
	    /* alert on Drop */
	     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
	       $scope.alertMessage = ('Event Dropped to make dayDelta ' + delta);
	    };
	    /* alert on Resize */
	    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
	       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
	    };

	    editLecturePopup = function(lecture) {
	    	lecture._start = undefined;
			lecture._end = undefined;

			var lectureToEdit = JSON.stringify(lecture);
			lectureToEdit = JSON.parse(lectureToEdit);
/*			lectureToEdit._start = lectureToEdit._start.replace("T", " ");
			lectureToEdit._start = lectureToEdit._start .substring(0, lectureToEdit._start.indexOf("."));

			lectureToEdit._end = lectureToEdit._end.replace("T", " ");
			lectureToEdit._end = lectureToEdit._end .substring(0, lectureToEdit._end.indexOf("."));

*/			lectureToEdit.start = lectureToEdit.start.replace("T", " ");
			lectureToEdit.start = lectureToEdit.start .substring(0, lectureToEdit.start.indexOf("."));

			lectureToEdit.end = lectureToEdit.end .replace("T", " ");
			lectureToEdit.end = lectureToEdit.end  .substring(0, lectureToEdit.end .indexOf("."));

			$scope.lectureToEdit = lectureToEdit;

			var options = {
				templateUrl : 'app/timetable/html/lecture-popup.html',
				controller : 'LectureSaveController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};
		
		$scope.addLecturePopup = function() {
			$scope.lectureToEdit = {};
			$scope.lectureToEdit.standardId = $scope.timetable.standardId;
			var options = {
				templateUrl : 'app/timetable/html/lecture-popup.html',
				controller : 'LectureSaveController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		$scope.eventRender = function( event, element, view ) {
	        element.attr({'tooltip': event.title, 'tooltip-append-to-body': true});
	        $compile(element)($scope);
	    };

		/* calendar config object */
	    $scope.uiConfig = {
	      calendar:{
	        height: 450,
	        defaultDate: '2016-09-12',
			firstDay: 1,
			allDaySlot: false,
	        editable: false,
	        header : false,
	        minTime : $scope.calendarMinTime,
			maxTime : $scope.calendarMaxTime,
	        eventClick: $scope.alertOnEventClick,
	        dayNamesShort : $scope.dayNamesWhenNotSkippedHolidays,
	        eventRender: $scope.eventRender,
	        views: {
	            settimana: {
	                type: 'agendaWeek',
	                duration: {
	                    days: 7
	                },
	                title: 'Apertura',
	                columnFormat: 'ddd', // Format the day to only show like 'Monday'
	            }
	        },
	        defaultView: 'settimana',
	      }
	    };


	    $scope.lectures = [];

	    $scope.lectureSource = {};
	    $scope.lectureSources = [$scope.lectures];

		$scope.timetable.instituteId = $localStorage.selectedInstitute;
		$scope.timetable.branchId = $localStorage.selectedBranch;

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId == 6){
    		$scope.timetable.instituteId = $rootScope.instituteId;
    		changeBranchList();
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.timetable.instituteId || $scope.timetable.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.timetable.instituteId})
					.$promise.then(function(branchList) {
						if(branchList != null && branchList.length > 0){
							changeStandardList();
	        			}
						else {
							$scope.standardList.length = 0;
						}
						angular.extend($scope.branchList,branchList);
						getCurrentWorkingDays();
				});	
			}
		};

		$scope.dayNamesForSelectBox = [];

		function getCurrentWorkingDays(){
			if($scope.timetable.branchId){
				 $resource('workingday/getCurrentWorkingDayForBranch?branchId='+$scope.timetable.branchId).get()
					.$promise.then(function(workingDay) {
						$scope.dayNamesForSelectBox.length = 0;
						if(workingDay){
							$scope.nonWorkingDays.length = 0;
							$scope.isSkipTimetableOnHoliday = workingDay.skipTimetableOnHoliday == 'Y';

							if(workingDay.monday == 'N'){
								$scope.nonWorkingDays.push(1);
							}
							else {
								var day = {value : '2016-09-12', text : $scope.isSkipTimetableOnHoliday ? 'Day1' : 'Mon'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.tuesday == 'N'){
								$scope.nonWorkingDays.push(2);
							}
							else {
								var day = {value : '2016-09-13', text : $scope.isSkipTimetableOnHoliday ? 'Day2' : 'Tue'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.wednesday == 'N'){
								$scope.nonWorkingDays.push(3);
							}
							else {
								var day = {value : '2016-09-14', text : $scope.isSkipTimetableOnHoliday ? 'Day3' : 'Wed'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.thursday == 'N'){
								$scope.nonWorkingDays.push(4);
							}
							else {
								var day = {value : '2016-09-15', text : $scope.isSkipTimetableOnHoliday ? 'Day4' : 'Thu'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.friday == 'N'){
								$scope.nonWorkingDays.push(5);
							}
							else {
								var day = {value : '2016-09-16', text : $scope.isSkipTimetableOnHoliday ? 'Day5' : 'Fri'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.saturday == 'N'){
								$scope.nonWorkingDays.push(6);
							}
							else {
								var day = {value : '2016-09-17', text : $scope.isSkipTimetableOnHoliday ? 'Day6' : 'Sat'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.sunday == 'N'){
								$scope.nonWorkingDays.push(0);
							}
							else {
								var day = {value : '2016-09-18', text : $scope.isSkipTimetableOnHoliday ? 'Day7' : 'Sun'};
								$scope.dayNamesForSelectBox.push(day);
							}
							
							$scope.uiConfig.calendar.dayNamesShort = $scope.isSkipTimetableOnHoliday ? $scope.dayNamesWhenSkippedHolidays  : $scope.dayNamesWhenNotSkippedHolidays;
						}
						else {
							$scope.message = "Working days not specified for this branch.";
						}
				});
			}
		}

		$scope.changeStandardList = function(){
			changeStandardList();
			$scope.lectures.length = 0;
			getCurrentWorkingDays();
		};

		function changeStandardList(){
			if($scope.timetable.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.timetable.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					if(standardList != null && standardList.length > 0){
						$scope.timetable.standardId = $scope.timetable.standardId ? $scope.timetable.standardId 
							: standardList[0].id;
						changeDivisionList();
        			}
					angular.extend($scope.standardList,standardList);
					changeYearList();
				});
			}
			else {
				$scope.standardList.length = 0;
			}
		}

		$scope.changeDivisionList = function (){
			changeDivisionList();
		};
		
		function changeDivisionList(){
			if($scope.timetable.standardId){
				$resource('division/getDivisionByStandardTimetable?standardId='+$scope.timetable.standardId).query()
					.$promise.then(function(divisionList) {
					
					$scope.divisionList.length = 0;
					angular.extend($scope.divisionList,divisionList);									
	    		});
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		

		$scope.changeYearList = function(){
			changeYearList();
		};

		$scope.minDate = new Date();
		$scope.maxDate = new Date();

		function changeYearList(){
			if($scope.timetable.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.timetable.branchId).query()
					.$promise.then(function(academicYearList) {
					 $scope.academicYearList.length = 0;
					if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.timetable.academicYearId = acadmicYear.id;
								$scope.minDate = new Date(acadmicYear.fromDate);
								$scope.maxDate = new Date(acadmicYear.toDate);
								$scope.timetable.startDate = $scope.minDate;
								$scope.timetable.endDate = $scope.maxDate;

								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}	
					angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

		$scope.cancel = function(){
			CommonServices.cancelOperation('timetable.list');
		};

		$scope.save = function() {
			var timeTableData = { 
				calendarData : $scope.lectures,
				nonWorkingDays : $scope.nonWorkingDays,
				dayNamesForSelectBox : $scope.dayNamesForSelectBox,
				startDate : $filter('date')($scope.timetable.startDate, "dd-MM-yyyy"),
				endDate : $filter('date')($scope.timetable.endDate, "dd-MM-yyyy"),
				startTime : $filter('date')($scope.timetable.startTime, "HH:mm:ss"),
				endTime :  $filter('date')($scope.timetable.endTime, "HH:mm:ss"),
				isSkipTimetableOnHoliday : $scope.isSkipTimetableOnHoliday
			};

			var data = JSON.stringify(timeTableData);

			var timetable = {
				id : $scope.timetable.id,
				branchId : $scope.timetable.branchId,
				standardId : $scope.timetable.standardId,
				divisionIdList : $scope.timetable.divisionIdList,
				academicYearId : $scope.timetable.academicYearId,
				data : data
			};	

			$resource('timetable').save(timetable).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('timetable.list',
					{
						instituteId : $scope.timetable.instituteId,
	        			branchId : $scope.timetable.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
        		}
        		else{
        			$scope.status = resp.status;
	        		$scope.message = resp.message;
        		}
			});
		};
	});
	
	/*This Controller is Used for timetable Edit functionality*/
	angular.module('timetable').controller('TimetableEditController',function($rootScope,$scope,$filter,$state,$stateParams,$resource, CommonServices, modalPopupService, $compile) {
		
		$scope.timetable = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.divisionList = [];
		$scope.academicYearList = [];
		$scope.standardList = [];
		
	    $scope.lectures = [];

		$scope.nonWorkingDays = [];
		$scope.isSkipTimetableOnHoliday = false;
		$scope.dayNamesWhenSkippedHolidays = ["Day7", "Day1", "Day2", "Day3", "Day4", "Day5", "Day6"];
		$scope.dayNamesWhenNotSkippedHolidays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

		$scope.timetableHeaderValue = "Edit timetable";

		$scope.calendarMinTime = "00:00:00";
		$scope.calendarMaxTime = "24:00:00";

		$resource('timetable/:id').get({id : $stateParams.id}).$promise.then(function(timetableToEdit) {
			angular.extend($scope.timetable,timetableToEdit);
			var data = JSON.parse(timetableToEdit.data);
			$scope.lecturesData =  data.calendarData;

			angular.forEach($scope.lecturesData,function(value,index) {
				console.log(" lecturedata:: "+index+":: "+JSON.stringify(value));
				 $scope.lectures.push(value);
			});

			$scope.isSkipTimetableOnHoliday = data.isSkipTimetableOnHoliday;
			$scope.uiConfig.calendar.dayNamesShort = data.isSkipTimetableOnHoliday 
				? $scope.dayNamesWhenSkippedHolidays  : $scope.dayNamesWhenNotSkippedHolidays;

			var date = new Date();
			var startDateArr = data.startDate.split("-");
			var endDateArr = data.endDate.split("-");

			$scope.timetable.startDate = new Date(startDateArr[2], (parseInt(startDateArr[1]) - 1) , startDateArr[0]);
			$scope.timetable.endDate = new Date(endDateArr[2], (parseInt(endDateArr[1]) - 1) , endDateArr[0]);
			$scope.timetable.startTime = new Date(date.getFullYear() + "/"+(date.getMonth() + 1)+"/"+date.getDate()+" "+ data.startTime);
			$scope.timetable.endTime = new Date(date.getFullYear() + "/"+(date.getMonth() + 1)+"/"+date.getDate()+" "+ data.endTime);

			$scope.nonWorkingDays = data.nonWorkingDays;
			setWeekEnds();
			$scope.dayNamesForSelectBox =  data.dayNamesForSelectBox;
			$rootScope.setSelectedData($scope.timetable.instituteId, $scope.timetable.branchId);
			changeBranchList();
		});

		function setWeekEnds(){
			
		}
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};

		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.timetable.instituteId || $scope.timetable.instituteId == 0){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.timetable.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
				});	
			}
			changeYearList();
			changeStandardList();
		};
		
		$scope.changeStandardList = function(){
			changeStandardList();
			$scope.lectures.length = 0;
			getCurrentWorkingDays();
		};

		function changeStandardList(){
			if($scope.timetable.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.timetable.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					if(standardList != null && standardList.length > 0){
						$scope.timetable.standardId = $scope.timetable.standardId ? $scope.timetable.standardId 
							: standardList[0].id;
        			}

					angular.extend($scope.standardList,standardList);
					changeYearList();
				});
			}
			else {
				$scope.standardList.length = 0;
			}
			changeDivisionList();
		}

	
		$scope.changeDivisionList = function (){
			changeDivisionList();
		};
		
		function changeDivisionList(){
			if($scope.timetable.standardId){
				$resource('division/getUnusedDivisionsByStandardTimetable?timetableId='+$stateParams.id).query()
					.$promise.then(function(divisionList) {
					
					$scope.divisionList.length = 0;
					angular.extend($scope.divisionList,divisionList);									
	    		});
			}
			else {
				$scope.divisionList.length = 0;
			}
		}
		
		$scope.changeYearList = function(){
			changeYearList();
		};

		function changeYearList(){	
			if($scope.timetable.branchId){		
				$resource('academicyear/getActiveYearByBranch?branchId='+$scope.timetable.branchId).
						query({id : $scope.timetable.branchId}).$promise.then(function(academicYearList) {
						$scope.academicYearList.length = 0;

					 if(academicYearList != null && academicYearList.length > 0){
						for(var i = 0; i < academicYearList.length; i++){
							var acadmicYear = academicYearList[i];
							if(acadmicYear.isCurrentActiveYear == 'Y'){
								$scope.timetable.academicYearId = acadmicYear.id;
								$scope.minDate = new Date(acadmicYear.fromDate);
								$scope.maxDate = new Date(acadmicYear.toDate);

								acadmicYear.fromDate = $filter('date')(acadmicYear.fromDate, "dd/MM/yyyy");
								acadmicYear.toDate = $filter('date')(acadmicYear.toDate, "dd/MM/yyyy");
							}
						}
					}
				 	 angular.extend($scope.academicYearList,academicYearList);									
	    		});
			}
		}

		$scope.cancel = function(){
			$state.transitionTo('timetable.list',{reload : true});
		};

		$scope.save = function() {
			var timeTableData = { 
				calendarData : $scope.lectures,
				nonWorkingDays : $scope.nonWorkingDays,
				dayNamesForSelectBox : $scope.dayNamesForSelectBox,
				startDate : $filter('date')($scope.timetable.startDate, "dd-MM-yyyy"),
				endDate : $filter('date')($scope.timetable.endDate, "dd-MM-yyyy"),
				startTime : $filter('date')($scope.timetable.startTime, "HH:mm:ss"),
				endTime :  $filter('date')($scope.timetable.endTime, "HH:mm:ss"),
				isSkipTimetableOnHoliday : $scope.isSkipTimetableOnHoliday
			};
			var data = JSON.stringify(timeTableData);

			var timetable = {
				id : $scope.timetable.id,
				branchId : $scope.timetable.branchId,
				standardId : $scope.timetable.standardId,
				divisionIdList : $scope.timetable.divisionIdList,
				academicYearId : $scope.timetable.academicYearId,
				data : data
			};

			$resource('timetable').save(timetable).$promise.then(function(resp) {
				if(resp.status == 'success'){
					$state.transitionTo('timetable.list',
					{
						instituteId : $scope.timetable.instituteId,
	        			branchId : $scope.timetable.branchId,
	        			status : resp.status, 
	        			message : resp.message
					},{reload : true});
				}
				else{
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
		        }
			});
		};

		$scope.eventRender = function( event, element, view ) {
	        element.attr({'tooltip': event.title, 'tooltip-append-to-body': true});
	        $compile(element)($scope);
	    };

	    $scope.alertOnEventClick = function( date, jsEvent, view, index){
	    	console.log(date.title + " ::subject:: "+ date.subjectId);
	        $scope.alertMessage = (date.title + ' was clicked ');
	        date.standardId = $scope.timetable.standardId;
	        editLecturePopup(date);
	    };
	    /* alert on Drop */
	     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
	       $scope.alertMessage = ('Event Dropped to make dayDelta ' + delta);
	    };
	    /* alert on Resize */
	    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
	       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
	    };

	    editLecturePopup = function(lecture) {
	    	lecture._start = undefined;
			lecture._end = undefined;
			lecture.source = undefined;
			console.log(lecture);
			var lectureToEdit = JSON.stringify(lecture);
			lectureToEdit = JSON.parse(lectureToEdit);

/*			lectureToEdit._start = lectureToEdit._start.replace("T", " ");
			lectureToEdit._start = lectureToEdit._start .substring(0, lectureToEdit._start.indexOf("."));

			lectureToEdit._end = lectureToEdit._end.replace("T", " ");
			lectureToEdit._end = lectureToEdit._end .substring(0, lectureToEdit._end.indexOf("."));
*/
			lectureToEdit.start = lectureToEdit.start.replace("T", " ");
			lectureToEdit.start = lectureToEdit.start .substring(0, lectureToEdit.start.indexOf("."));

			lectureToEdit.end = lectureToEdit.end .replace("T", " ");
			lectureToEdit.end = lectureToEdit.end  .substring(0, lectureToEdit.end .indexOf("."));

			$scope.lectureToEdit = lectureToEdit;
			
			var options = {
				templateUrl : 'app/timetable/html/lecture-popup.html',
				controller : 'LectureSaveController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};
		
		$scope.addLecturePopup = function() {
			$scope.lectureToEdit = {};
			$scope.lectureToEdit.standardId = $scope.timetable.standardId;
			var options = {
				templateUrl : 'app/timetable/html/lecture-popup.html',
				controller : 'LectureSaveController',
				scope : $scope
			};
			modalPopupService.openPopup(options);
		};

		/* calendar config object */
	    $scope.uiConfig = {
	      calendar:{
	        height: 450,
	        defaultDate: '2016-09-12',
			firstDay: 1,
			allDaySlot: false,
	        editable: false,
	        header : false,
	        minTime : $scope.calendarMinTime,
			maxTime : $scope.calendarMaxTime,
	        eventClick: $scope.alertOnEventClick,
	        dayNamesShort : $scope.dayNamesWhenNotSkippedHolidays,
	        eventRender: $scope.eventRender,
	        views: {
	            settimana: {
	                type: 'agendaWeek',
	                duration: {
	                    days: 7
	                },
	                title: 'Apertura',
	                columnFormat: 'ddd', // Format the day to only show like 'Monday'
	            }
	        },
	        defaultView: 'settimana',
	      }
	    };

	    $scope.lectureSources = [$scope.lectures];

		$scope.dayNamesForSelectBox = [];
		
		function setWeekends (){
			
		}

		function getCurrentWorkingDays(){
			if($scope.timetable.branchId){
				 $resource('workingday/getCurrentWorkingDayForBranch?branchId='+$scope.timetable.branchId).get()
					.$promise.then(function(workingDay) {
						$scope.dayNamesForSelectBox.length = 0;
						if(workingDay){
							$scope.nonWorkingDays.length = 0;
							$scope.isSkipTimetableOnHoliday = workingDay.skipTimetableOnHoliday == 'Y';

							if(workingDay.monday == 'N'){
								$scope.nonWorkingDays.push(1);
							}
							else {
								var day = {value : '2016-09-12', text : $scope.isSkipTimetableOnHoliday ? 'Day1' : 'Mon'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.tuesday == 'N'){
								$scope.nonWorkingDays.push(2);
							}
							else {
								var day = {value : '2016-09-13', text : $scope.isSkipTimetableOnHoliday ? 'Day2' : 'Tue'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.wednesday == 'N'){
								$scope.nonWorkingDays.push(3);
							}
							else {
								var day = {value : '2016-09-14', text : $scope.isSkipTimetableOnHoliday ? 'Day3' : 'Wed'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.thursday == 'N'){
								$scope.nonWorkingDays.push(4);
							}
							else {
								var day = {value : '2016-09-15', text : $scope.isSkipTimetableOnHoliday ? 'Day4' : 'Thu'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.friday == 'N'){
								$scope.nonWorkingDays.push(5);
							}
							else {
								var day = {value : '2016-09-16', text : $scope.isSkipTimetableOnHoliday ? 'Day5' : 'Fri'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.saturday == 'N'){
								$scope.nonWorkingDays.push(6);
							}
							else {
								var day = {value : '2016-09-17', text : $scope.isSkipTimetableOnHoliday ? 'Day6' : 'Sat'};
								$scope.dayNamesForSelectBox.push(day);
							}

							if(workingDay.sunday == 'N'){
								$scope.nonWorkingDays.push(0);
							}
							else {
								var day = {value : '2016-09-18', text : $scope.isSkipTimetableOnHoliday ? 'Day7' : 'Sun'};
								$scope.dayNamesForSelectBox.push(day);
							}
							$scope.uiConfig.calendar.dayNamesShort = $scope.isSkipTimetableOnHoliday 
								? $scope.dayNamesWhenSkippedHolidays  : $scope.dayNamesWhenNotSkippedHolidays;
							setWeekends ();
						}
						else {
							$scope.message = "Working days not specified for this branch.";
						}
				});
			}
		}
	});


	angular.module('timetable').controller('TitmetableViewController', function($scope, $resource, $rootScope, $stateParams, $compile, $state) {

		$scope.isView = true;
		$scope.timetable = {};
		$scope.lectures = [];
		$scope.divisionList = [];
		$scope.standardList = [];

		$scope.nonWorkingDays = [];
		$scope.isSkipTimetableOnHoliday = false;
		$scope.dayNamesWhenSkippedHolidays = ["Day7", "Day1", "Day2", "Day3", "Day4", "Day5", "Day6"];
		$scope.dayNamesWhenNotSkippedHolidays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

		$scope.calendarMinTime = "00:00:00";
		$scope.calendarMaxTime = "24:00:00";

		$resource('timetable/:id').get({id : $stateParams.id}).$promise.then(function(timetableToEdit) {
			angular.extend($scope.timetable,timetableToEdit);
			var data = JSON.parse(timetableToEdit.data);
			$scope.lecturesData =  data.calendarData;

			angular.forEach($scope.lecturesData,function(value,index) {
				console.log(" lecturedata:: "+index+":: "+JSON.stringify(value));
				 $scope.lectures.push(value);
			});
			$scope.uiConfig.calendar.dayNamesShort = data.isSkipTimetableOnHoliday 
				? $scope.dayNamesWhenSkippedHolidays  : $scope.dayNamesWhenNotSkippedHolidays;
			
			var date = new Date();
			var startDateArr = data.startDate.split("-");
			var endDateArr = data.endDate.split("-");

			$scope.timetable.startDate = new Date(startDateArr[2], (parseInt(startDateArr[1]) - 1) , startDateArr[0]);
			$scope.timetable.endDate = new Date(endDateArr[2], (parseInt(endDateArr[1]) - 1) , endDateArr[0]);
			$scope.timetable.startTime = new Date(date.getFullYear() + "/"+(date.getMonth() + 1)+"/"+date.getDate()+" "+ data.startTime);
			$scope.timetable.endTime = new Date(date.getFullYear() + "/"+(date.getMonth() + 1)+"/"+date.getDate()+" "+ data.endTime);

			$scope.nonWorkingDays = data.nonWorkingDays;
			setWeekEnds();
			$scope.dayNamesForSelectBox =  data.dayNamesForSelectBox;
			$rootScope.setSelectedData($scope.timetable.instituteId, $scope.timetable.branchId);
			changeStandardList();
		});

		function setWeekEnds(){

		}

		function changeStandardList(){
			if($scope.timetable.branchId){
				$resource('masters/getFilterdStandards/:id').query({id : $scope.timetable.branchId}).$promise.then(
				 function(standardList) {
					 $scope.standardList.length = 0;
					if(standardList != null && standardList.length > 0){
						$scope.timetable.standardId = $scope.timetable.standardId ? $scope.timetable.standardId 
							: standardList[0].id;
        			}

					angular.extend($scope.standardList,standardList);
				});
			}
			else {
				$scope.standardList.length = 0;
			}
			changeDivisionList();
		}

	
		$scope.changeDivisionList = function (){
			changeDivisionList();
		};
		
		function changeDivisionList(){
			if($scope.timetable.standardId){
				$resource('division/getUnusedDivisionsByStandardTimetable?timetableId='+$stateParams.id).query()
					.$promise.then(function(divisionList) {
					
					$scope.divisionList.length = 0;
					angular.extend($scope.divisionList,divisionList);									
	    		});
			}
			else {
				$scope.divisionList.length = 0;
			}
		}

		$scope.cancel = function(){
			$state.transitionTo('timetable.list',{reload : true});
		};

		$scope.eventRender = function( event, element, view ) {
	        element.attr({'tooltip': event.title,
	                      'tooltip-append-to-body': true});
	        $compile(element)($scope);
	    };

		/* calendar config object */
	    $scope.uiConfig = {
	      calendar:{
	        height: 450,
	        defaultDate: '2016-09-12',
			firstDay: 1,
			allDaySlot: false,
	        editable: false,
	        header : false,
	        minTime : $scope.calendarMinTime,
			maxTime : $scope.calendarMaxTime,
	        eventRender: $scope.eventRender,
	        dayNamesShort : $scope.dayNamesWhenNotSkippedHolidays,
	        views: {
	            settimana: {
	                type: 'agendaWeek',
	                duration: {
	                    days: 7
	                },
	                title: 'Apertura',
	                columnFormat: 'ddd', // Format the day to only show like 'Monday'
	            }
	        },
	        defaultView: 'settimana',
	      }
	    };

	    $scope.lectureSource = {
	            currentTimezone: 'local' // an option!
	    };

	    $scope.lectureSources = [$scope.lectures];

	});

	angular.module('timetable').controller('LectureSaveController',['$scope','$resource','$uibModalStack','modalPopupService', '$filter',
	                                                                function($scope, $resource, $uibModalStack, modalPopupService, $filter) {
	
		$scope.subjectList = [];
		
		$scope.lecture = {};

		var lectureToEdit = JSON.stringify($scope.lectureToEdit);
		lectureToEdit = JSON.parse(lectureToEdit);
		
		//console.log('ëvent id:: '+$scope.calendarEvent.id);

		$scope.lecture = (lectureToEdit ? lectureToEdit : {});
		$scope.isEdit = ($scope.lecture.standardId ? true : false);
		
		if($scope.lecture.standardId){
			changeSubjectList();
		}

		if($scope.lecture.start) {
			var day = moment($scope.lecture.start).utc().format("YYYY-MM-DD");
			console.log("start day = "+day);
			$scope.lecture.dayCode = day;
			 var dt = new Date();
	         var tz = dt.getTimezoneOffset(); 
	         console.log("offset:: "+tz);
	         console.log("$scope.lecture.startTime offset:: "+moment.utc($scope.lecture.start).zone(tz).format('MM/DD/YYYY h:mm A'));

			$scope.lecture.startTime = new Date($scope.lecture.start);
		}
		
		if($scope.lecture.end) {
			$scope.lecture.endTime = new Date($scope.lecture.end);
		}

		function changeSubjectList(){
			if($scope.lecture.standardId){
				$resource('masters/subjectByStandard/:id').query({id : $scope.lecture.standardId}).$promise.then(
				 function(subjectList) {
					$scope.subjectList.length = 0;
					var breakItem = {
					    	name: 'BREAK',
					        text: 'Day4'
					    };
					
					subjectList.push(breakItem);
					angular.extend($scope.subjectList,subjectList);
					
					if($scope.lecture.title) {
						$scope.lecture.subjectId = $scope.lecture.title;
					}
				});
			}
			else {
				$scope.subjectList.length = 0;
			}
		}

		/** Add individuals to an array */
		$scope.saveLecture = function() {

			console.log("TO SAVE ::::: "+JSON.stringify($scope.lecture));
			var startTimeObj = new Date($scope.lecture.startTime); 
			var startTime = (startTimeObj != null ? startTimeObj.getHours() + ":"+startTimeObj.getMinutes()+":"+startTimeObj.getSeconds() : null);
			var endTimeObj = new Date($scope.lecture.endTime); 
			var endTime = (endTimeObj!= null ? endTimeObj.getHours() + ":"+endTimeObj.getMinutes()+":"+endTimeObj.getSeconds() : null);
			console.log("startTimeObj = "+startTime);
			console.log("endTimeObj = "+endTime);
			var dateObj = new Date($scope.lecture.dayCode); 
			var y = dateObj.getFullYear();
			var m = dateObj.getMonth();
			var d = dateObj.getDate();
			var startHour = startTimeObj.getHours();
			var startMins = startTimeObj.getMinutes();
			var endHour = endTimeObj.getHours();
			var endMins = endTimeObj.getMinutes();
			
			console.log("years = "+y);
			console.log("months = "+m);
			console.log("date = "+d);
			console.log("startHour = "+startHour);
			console.log("endHour = "+endHour);

			
			var lectureStartDateTime = moment().year(y).month(m).date(d).hour(startHour)
				.minute(startMins).second(0)
				.utc().local().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
			var lectureEndDateTime = moment().year(y).month(m).date(d).hour(endHour)
				.minute(endMins).second(0)
				.utc().local().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
			console.log("lectureStartDateTime = "+lectureStartDateTime);

						
			var lectureJson = {};
			
			if($scope.lecture._id) {
				lectureJson._id = $scope.lecture._id;
			}			
			lectureJson.title = $scope.lecture.subjectId;
			lectureJson.start = lectureStartDateTime;
			lectureJson.end = lectureEndDateTime;
			lectureJson.className =  ['openSesame'];

			var checkIfLectureOverlapFlag =  checkIfLectureOverlap(lectureJson);
			
			if(!checkIfLectureOverlapFlag) {
				
				if($scope.lecture._id) {
					console.log("uuid of existing elcture::"+$scope.lecture._id);
					var existingEventRemoved = removeExistingLecture(lectureJson._id);
					if(existingEventRemoved) {
						lectureJson._id = generateGUID();
						$scope.$parent.lectures.push(lectureJson);
					}
				} else {
					lectureJson._id = generateGUID();
					$scope.$parent.lectures.push(lectureJson);
					
				}
				modalPopupService.closePopup(null, $uibModalStack);
			} 
			
		};
		
		function generateGUID() {

	        /// <summary>Generate 128-bit, 32 hexadecimal, globally unique UUID</summary>
	        //Generate 4-digit random hex value
	        function nibble() {
	            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	        }
	        //Foramt to GUID standard layout
	        var guid = (nibble() + nibble() + "-" + nibble() + nibble() + "-" + nibble() + "-" + nibble() + nibble() + nibble()).toLowerCase();
	        //Update UI
	        return guid;

	    }
		
		$scope.removeExistingLecture = function() {
			var uuid = $scope.lecture._id;
			var lectureDeleted = removeExistingLecture(uuid);
			if(lectureDeleted) {
				modalPopupService.closePopup(null, $uibModalStack);
			}
		};

		removeExistingLecture = function(deleteByUuid) {

			var ifEventExist = true;

			angular.forEach($scope.$parent.lectures,function(value,index){
				if(value._id == deleteByUuid) {
					console.log("to be removed title = "+JSON.stringify(value));
					$("div#calendar").fullCalendar('removeEvents', value._id);
					$scope.$parent.lectures.splice(index,1);
					return ifEventExist;
				}
			});
			return ifEventExist;
		};
		
		
		checkIfLectureOverlap = function(lectureJson) {
			
			 var currentLectureUuid = lectureJson._id;
			 var formatStr = 'HH.mm';
			 var lectureOverlap = false;
			 var currentLectureStartDate = moment(lectureJson.start);
			 
			 var currentLectureEndDate = moment(lectureJson.end);
			
			  angular.forEach($scope.$parent.lectures,function(value,index) {
				  
				  	if(value._id != currentLectureUuid) {
				  		
				  		var lectureTitle = value.title;
					  	console.log('event  Title::'+lectureTitle);
					  	var startDate = moment(value.start);
					  	var startTime = startDate.format(formatStr);
					  	console.log('startDate::'+startDate);
					  	console.log('startTime::'+startTime);
					  	
					  	var endDate = moment(value.end);
					  	var endTime = endDate.format(formatStr);
					  	console.log('endDate::'+endDate);
					  	console.log('endTime::'+endTime);
					  	
					  	if(currentLectureStartDate.isAfter(startDate) && currentLectureStartDate.isBefore(endDate)) {
					  		alert('New entry start time is overlapping existing lecture '+lectureTitle);
					  		lectureOverlap = true;
					  		return lectureOverlap;
					  	} else if(currentLectureEndDate.isAfter(startDate) && currentLectureEndDate.isBefore(endDate)) {
					  		alert('New entry end time is overlapping existing lecture '+lectureTitle);
					  		lectureOverlap = true;
					  		return lectureOverlap;
					  	} else if(currentLectureStartDate.isBefore(startDate) && currentLectureEndDate.isAfter(endDate)) {
					  		alert('New entry is overlapping existing lecture '+lectureTitle);
					  		lectureOverlap = true;
					  		return lectureOverlap;
					  	}
				  	}
			  });
			  
			  return lectureOverlap;
		};

		$scope.cancelPopup = function() {
			modalPopupService.closePopup(null, $uibModalStack);
		};
			
	} ]);


	angular.module('timetable').controller('validateTimetableForm',function($resource,$scope,$state, $filter) {
		$scope.$watch('timetable.startTime', validateTimes);
		$scope.$watch('timetable.endTime', validateTimes);

		function validateTimes() {
	        if($scope.timetable.endTime && $scope.timetable.startTime)  {
	        	var endTime = new Date($scope.timetable.endTime);
		        var startTime = new Date($scope.timetable.startTime);
		        startTime.setSeconds(00);
	        	endTime.setSeconds(00);
	        	if(!isNaN(startTime)){
	        		$scope.calendarMinTime = $filter('date')(startTime, "HH");
	        	}

	        	if(!isNaN(endTime)){
	        		$scope.calendarMaxTime = $filter('date')(endTime, "HH");
	        	}
		        if(!isNaN(endTime) && !isNaN(startTime)){
		        	$scope.addTimetable.endTime.$setValidity("endBeforeStart", endTime > startTime);
		        	if($scope.calendarMinTime < $scope.calendarMaxTime){
		        		$scope.uiConfig.calendar.height = (parseInt($scope.calendarMaxTime) - parseInt($scope.calendarMinTime) < 6 ? 275 : 450);
		        		$scope.uiConfig.calendar.minTime = $scope.calendarMinTime +":00:00";
						$scope.uiConfig.calendar.maxTime = (parseInt($scope.calendarMaxTime) + 1) +":00:00";	
		        	}
		        }
	        }
		}

		$scope.isStartTimeChanged = false;
		$scope.isEndTimeChanged = false;
		
	});


	angular.module('timetable').controller('validateLectureForm',function($resource,$scope,$state, $filter) {

		$scope.$watch('lecture.startTime', validateTimes);
		$scope.$watch('lecture.endTime', validateTimes);

		function validateTimes() {
		    if (!$scope.lecture) return;
		    if ($scope.lectureForm.startTime.$error.invalidTime || $scope.lectureForm.endTime.$error.invalidTime) {
		        $scope.lectureForm.startTime.$setValidity("invalidTime", true);  //already invalid (per validDate directive)
		    } else {
		        if($scope.lecture.endTime || $scope.lecture.startTime)  {
		        	var endTime = $scope.lecture.endTime ? new Date($filter('date')(new Date(), 'yyyy-MM-dd' ) + ' ' + $filter('date')($scope.lecture.endTime, 'HH:mm')) : undefined;
			        var startTime = $scope.lecture.startTime ? new Date($filter('date')(new Date(), 'yyyy-MM-dd' ) + ' ' + $filter('date')($scope.lecture.startTime, 'HH:mm')) : undefined;

			        var minStartTime = $scope.$parent.timetable.startTime;
		        	var minEndTime = $scope.$parent.timetable.endTime;
		        	minStartTime.setSeconds(00);
		        	minEndTime.setSeconds(00);

			        if(!isNaN(startTime)){
			        	startTime.setSeconds(00);
			        	var startTimeHours = startTime.getHours();
			        	var minStartTimeHours = minStartTime.getHours();
			        	var startTimeMinutes = startTime.getMinutes();
			        	var minStartTimeMinutes = minStartTime.getMinutes();
			        	var flag = true;

			        	if(startTimeHours <= minStartTimeHours){
			        		if(startTimeHours == minStartTimeHours){
			        			if(startTimeMinutes < minStartTimeMinutes){
			        				flag = false;
			        			}
			        		}
			        		else {
			        			flag = false;
			        		}
			        	}
			        	$scope.lectureForm.startTime.$setValidity("beforeMinTime", flag);
			        }

		        	if(!isNaN(endTime) && !isNaN(startTime)){
			        	endTime.setSeconds(00);
			        	var endTimeHours = endTime.getHours();
			        	var minEndTimeHours = minEndTime.getHours();
			        	var endTimeMinutes = endTime.getMinutes();
			        	var minEndTimeMinutes = minEndTime.getMinutes();
			        	var flag = true;

			        	if(endTimeHours >= minEndTimeHours){
			        		if(endTimeHours == minEndTimeHours){
			        			if(endTimeMinutes > minEndTimeMinutes){
			        				flag = false;
			        			}
			        		}
			        		else {
			        			flag = false;
			        		}
			        	}
			        	$scope.lectureForm.endTime.$setValidity("afterMaxTime", flag);
			        	$scope.lectureForm.endTime.$setValidity("endBeforeStart", endTime > startTime);
			        }
		        }
		        else if($scope.isEndTimeChanged && !$scope.lecture.endTime){
		        	
		        	$scope.lectureForm.endTime.$setValidity("endBeforeStart", true);
		        }
		        else if($scope.isStartTimeChanged && !$scope.lecture.startTime){
		        	$scope.lectureForm.endTime.$setValidity("endBeforeStart", true);
		        }
		    }
		}

		$scope.isStartTimeChanged = false;
		$scope.isEndTimeChanged = false;
		
	});
	
	/** This method is used for uploading data */
	angular.module('timetable').controller('TimetableFileUploadController',function($scope,Upload, modalPopupService, $uibModalStack,$state) {
		$scope.uploadTimetable = function (file) {
	        Upload.upload({
	            url: 'timetable/uploadTimetableFile',
	            data : {branchId : $scope.branchId,
	            	branchId : $scope.branchId,
	            	academicYearId : $scope.academicYearId,
	            	standardId : $scope.standardId,
	            	divisionId : $scope.divisionId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
				modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.timetableList.length = 0;
	        	$scope.$parent.isUploadingFailed = (resp.data.status == 'fail' ? true : false);
	        	$scope.$parent.message = resp.data.message;
	        	angular.extend($scope.timetableList,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
			modalPopupService.closePopup(null, $uibModalStack);
	    };
	});
})();

