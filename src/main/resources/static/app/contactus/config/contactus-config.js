(function(){
	angular.module('contactus').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('contactus', {
			url : '',
			abstract :true,
			controller : 'ContactUsModelController',
			
		}).state('contactus.page', {
			url : '/contactus/page',
			views : {
				'@' : {
					templateUrl : 'app/contactus/html/contactus-page.html',
					controller : 'ContactUsController'
				}
			},
			data: {
	            displayName: 'Contact Us'
	        }
		});
	}]);
})();