(function(){
	angular.module('subject').config([ '$stateProvider', function($stateProvider) {
		$stateProvider.state('subject', {
			url : '/subject',
			abstract :true,
			controller : 'SubjectModelController',
			data: {
				 breadcrumbProxy: 'subject.list'
	        }
		}).state('subject.list', {
			url : '/list',
			views : {
				'@' : {
					templateUrl : 'app/subject/html/subject-list.html',
					controller : 'SubjectController'
				}
			},
			data: {
	            displayName: 'Subject List'
	        },
	        params : {
	        	status : '',
	        	message : '',
	        	instituteId : '',
	        	branchId : '',
	        	academicYearId : ''
	        }
		})
		.state('subject.add', {
			url : '/add',
			views : {
				'@' : {
					templateUrl : 'app/subject/html/subject-add.html',
					controller : 'SubjectAddController'
				}
			},
			data: {
	            displayName: 'Add Subject'
	        }
		}).state('subject.edit', {
			url : '/edit/:id',
			views : {
				'@' : {
					templateUrl : 'app/subject/html/subject-add.html',
					controller : 'SubjectEditController'
				}
			},
			data: {
	            displayName: 'Edit Subject'
	        },
		});
	}]);
})();