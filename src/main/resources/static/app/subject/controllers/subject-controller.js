(function(){
	angular.module('subject').controller('SubjectController',function($rootScope, $localStorage, 
			$resource,$filter,$confirm,DTOptionsBuilder,$stateParams,$scope,$state, modalPopupService,CommonServices) {

		$scope.subjectList = [];
		$scope.message = $stateParams.message;
	    $scope.status = $stateParams.status;

    	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.isUploadingFailed = false;
		// Datatable sorting...
	   $scope.instituteId = ($stateParams.instituteId ? $stateParams.instituteId : $localStorage.selectedInstitute );
		 $scope.branchId = $stateParams.branchId ? $stateParams.branchId : $localStorage.selectedBranch ;
		
		$scope.dtOptions =  DTOptionsBuilder.newOptions()
		.withOption("bAutoWidth", false)
		.withOption("paging", true)
		.withOption("searching", true)
        .withOption("responsive", true)
        .withOption("scrollX", true);

		$scope.filterTableByBranch = function(){
			$scope.status = "";
    		$scope.message = "";
    		$scope.isUploadingFailed = false;
    		filterTableByBranch($scope.branchId);
		};

		function filterTableByBranch(branchId){
			if(branchId){
				$resource('subject/getSubjectByBranch?branchId='+$scope.branchId).query().$promise.then(function(subjectList) {
					$scope.subjectList.length = 0;
					if(subjectList && subjectList.length > 0){
						angular.extend($scope.subjectList,subjectList);
					}					
	    		});
			}
		}

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
    		$scope.instituteId = $rootScope.instituteId;
    		changeBranchList(false);
    	}

    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		if(instituteList){
	    			changeBranchList(false);
	    		}
				angular.extend($scope.instituteList,instituteList);
			});
    	}

    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			$scope.status = "";
    		$scope.message = "";
			changeBranchList(true);
		};

		function changeBranchList(isBranchChanged){
			$scope.branchList.length = 0;
			if($scope.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.instituteId})
					.$promise.then(function(branchList) {
						if(branchList){
							if(!isBranchChanged){
								filterTableByBranch($scope.branchId);
							}
	        			}
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

	    /** This method is used to open Modal */ 
	    $scope.openUploadSubjectDialog = function(){
	    	  var options = {
	    	      templateUrl: 'app/subject/html/subject-bulk-upload.html',
	    	      controller: 'SubjectFileUploadController',
	    	      scope : $scope
	    	     
	    	    };
	    	  modalPopupService.openPopup(options);
	    	  
	     };

	     $scope.deleteData = function deleteSubject(id){
	    	 $confirm({text: 'Are you sure you want to delete?'})
	         .then(function() {
        	 	$resource('subject').remove({id : id}).$promise.then(function(resp) {
        	 		if(resp.status == "success"){
        	 			for(var i =0; i < $scope.subjectList.length ;i ++ ){
          			  		var subject = $scope.subjectList[i];
          			  		if(subject.id == id){
          			  			$scope.subjectList.splice(i , 1);
          			  			break;
          			  		}
          			  	}	
        	 		}
	        		$scope.status = resp.status;
	        		$scope.message = resp.message;
      			});
	         });
	     }; 
	});


	angular.module('subject').controller('SubjectAddController',function($rootScope,$localStorage, $scope,$filter,$state, modalPopupService,$stateParams,$resource) {
		$scope.subject = {}; 
	   	$scope.branchList = [];
		$scope.instituteList = [];
		$scope.subjectHeaderValue = "Add Subject";

		$scope.subject.instituteId = $localStorage.selectedInstitute;
		$scope.subject.branchId = $localStorage.selectedBranch;
		
		$scope.cancel = function(){
			$state.transitionTo('subject.list',{reload : true});
	    };

		// Get all branch of school Admin.
		// Mainly used when login user is school admin.
    	if($rootScope.roleId != 1){
        	$resource('masters/getFilterdBranch/:id').query({id : $rootScope.instituteId})
        		.$promise.then(function(branchList) {
        			if(branchList){
        				$scope.branchId = branchList[0].id;
        				
        			}
        			angular.extend($scope.branchList,branchList);
    		});
    	}
    	
    	// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
    			changeBranchList();
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
    	
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
 
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.subject.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.subject.instituteId})
					.$promise.then(function(branchList) {
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.save = function() {
			var subject = {
					id : $scope.subject.id,
					name: $scope.subject.name,
					branchId : $scope.subject.branchId,
					code : $scope.subject.code,
					theoryMarks : $scope.subject.theoryMarks,
					practicalMarks : $scope.subject.practicalMarks,
			};

			$resource('subject').save(subject).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('subject.list',
						{
							instituteId : $scope.subject.instituteId,
		        			branchId : $scope.subject.branchId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	     controller: "ValidationErrorController",
				    	     scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
        		}
			});
		};
	});

	
	/** This method is used for uploading data */
	angular.module('subject').controller('SubjectFileUploadController',function($rootScope,$scope,Upload, modalPopupService, $uibModalStack,$state) {
		$scope.uploadSubject = function (file) {
	        Upload.upload({
	            url: 'subject/uploadSubjectFile',
	            data : {branchId : $scope.branchId},
	            file: file
	        }).then(function(resp) {
	        	if(resp.data.status == 'fail'){
	        		if(resp.data.isValidationError == 'true'){
	        			$scope.validationErrorMsg = resp.data.message;
	        			$scope.validationStatus = resp.data.status;
	        			$scope.records = resp.data.records;
	        			return;
	        		}
	        		$scope.$parent.isUploadingFailed = true;
	        	}
	        	modalPopupService.closePopup(null, $uibModalStack);
	        	$scope.$parent.status  = resp.data.status;
	        	$scope.subjectList.length = 0;
	        	$scope.$parent.isUploadingFailed = (resp.data.status == 'fail' ? true : false);
	        	$scope.$parent.message = resp.data.message;
	        	angular.extend($scope.subjectList,resp.data.records);
	        });
	    };

	    $scope.cancel = function(){
        	modalPopupService.closePopup(null, $uibModalStack);
	    };
	});


	/*This Controller is Used for Subject Edit functionality*/
	angular.module('subject').controller('SubjectEditController',function($rootScope,$scope,$filter,$state,modalPopupService, $stateParams,$resource) {
		
		$scope.subject = {};
		$scope.branchList = [];
		$scope.instituteList = [];
		$scope.subjectHeaderValue = "Edit Subject";
		
		$resource('subject/:id').get({id : $stateParams.id}).$promise.then(function(subjectToEdit) {
			$scope.subject = subjectToEdit;
			changeBranchList();
		});

		$scope.cancel = function(){
			$state.transitionTo('subject.list',{reload : true});
	    };

		// get all institutes.
    	// Mainly used when login user is super admin.    	
    	if($rootScope.roleId == 1){
	    	$resource('masters/instituteMasters').query().$promise.then(function(instituteList) {
	    		angular.extend($scope.instituteList,instituteList);
			});
    	}
    	
    	// Change branch dropdown according to institute.
    	// Mainly used when login user is super admin.
		$scope.changeBranchList = function(){
			changeBranchList();
		};
		
		function changeBranchList(){
			$scope.branchList.length = 0;
			if($scope.subject.instituteId){
				$resource('masters/getFilterdBranch/:id').query({id : $scope.subject.instituteId})
					.$promise.then(function(branchList) {
						/*if(branchList){
							$scope.subject.branchId = branchList[0].id;
	        			}*/
						
						angular.extend($scope.branchList,branchList);
				});	
			}
		};

		$scope.save = function() {
			var subject = {
					id : $scope.subject.id,
					name: $scope.subject.name,
					branchId : $scope.subject.branchId,
					code : $scope.subject.code,
					theoryMarks : $scope.subject.theoryMarks,
					practicalMarks : $scope.subject.practicalMarks,
			};

			$resource('subject').save(subject).$promise.then(function(resp) {
				if(resp.status == 'success'){	
        			$state.transitionTo('subject.list',
						{
							instituteId : $scope.subject.instituteId,
		        			branchId : $scope.subject.branchId,
		        			status : resp.status, 
		        			message : resp.message
						},{reload : true});
        			}
        		else{
					if(resp.isValidationError){
						$scope.validationErrMsg = resp.records.title;
						$scope.validationErrors = resp.records.errors;
						 var options = {
							 templateUrl: "app/common/html/validationError.html",
				    	      controller: "ValidationErrorController",
				    	      scope : $scope
				    	 };
						 modalPopupService.openPopup(options);
					}
					else {
						$scope.status = resp.status;
						$scope.message = resp.message;	
					}
        		}
			});
		};
	});
})();

