angular.module('institute',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('branch',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('event',['ui.router','ngAnimate','datatables','ui.bootstrap.datetimepicker','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('student',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('teacher',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('vendor',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('fees',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource', 'angucomplete-alt']);
angular.module('payment-settings',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('division',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('subject',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
//angular.module('subjectCategory',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('standardSubject',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('standard',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('timetable',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);

angular.module('vendorreport',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('notice',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('group',['ui.router','datatables','ngTagsInput','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('login',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('chat',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('aboutus',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('contactus',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('profile',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('forum',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('settings',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('report',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('head',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('dashboard',['chart.js','ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('academicyear',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('holiday',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('workingday',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('feesreport',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('offline',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('admissionreport',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('ediary',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('shopping',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('displayHead',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('lateFeesDetail',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('exportData',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('displayTemplate',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('assignments',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('expressOnboard',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('fees-code',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
angular.module('auditLog',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);
//angular.module('myDashboard',['ui.router','naif.base64','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource','chart.js']);
angular.module('myDashboard',['chart.js','ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ui.bootstrap.datetimepicker','ngFileUpload','ngResource']);

angular.module('common',[]);
angular.module('role-access',['ui.router','ngResource']);
angular.module('receipt',['ui.router','datatables','angularUtils.directives.uiBreadcrumbs','ui.bootstrap','ngFileUpload','ngResource']);

angular.module('school',['shopping', 'subject', 
 	'standardSubject', 'ngStorage','role-access','ediary','feesreport', 'offline', 'admissionreport',
 	'workingday', 'vendorreport', 'holiday','academicyear','dashboard','head',
 	'report','angular-loading-bar','ngAnimate','branch','forum','settings',
 	'angular-confirm','profile','student','standard','division','institute',
 	'event','fees', 'payment-settings','login','notice','group','teacher','vendor',
 	'common', 'chat', 'aboutus','contactus', 'ngAnimate', 'isteven-multi-select', 'timetable', 'ui.calendar', 
 	'receipt', 'ngIdle','displayHead','lateFeesDetail', 'exportData', 'displayTemplate', 'assignments', 'expressOnboard', 'fees-code','auditLog','myDashboard','ui.select', 'ngSanitize']);
//FORUM-SERVICE API URL..... // http://localhost/collab/api/auth/login
angular.module('school').value('forumserviceapiurl', 'https://www.eduqfix.com/collab/');


//USER-SERVICE API URL..... FOR LOCAL

  angular.module('school').value('userserviceapiurl', 'http://localhost:8080/');

// USER-SERVICE API URL....
angular.module('school').value('userserviceapiurl', 'http://test.eduqfix.com/user-service/');

//angular.module('school').value('userserviceapiurl', 'https://www.eduqfix.com/user-service/');

// ONLINE ADMISSION FORM URL
angular.module('school').value('admissionportalapiurl', 'https://www.eduqfix.com/OnlineAdmissionPortal/#/');

//SHOPPING-SERVICE URL..... 
angular.module('school').value('shoppingurl', '/shopping/');

angular.module('school').config(function($urlRouterProvider,$stateProvider,$httpProvider,$locationProvider, 
		KeepaliveProvider, IdleProvider){

	$locationProvider.html5Mode(false);
	$urlRouterProvider.when('','login');
	$httpProvider.interceptors.push('tokenInjector');
	//InitializationService.initialize();
	$stateProvider.state('home', {
        url: '/home',
        templateUrl: 'app/home/html/home.html'
    });

	IdleProvider.idle(60*15);
    IdleProvider.timeout(30);
    KeepaliveProvider.interval(10);
});

//written by prathamesh

angular.module('school').run(['$state','$rootScope','$uibModal','$localStorage', '$q', '$resource', 
	function($state,$rootScope,$uibModal,$localStorage, $q, $resource) {
	$rootScope.hideLinks = false;

	if($localStorage.user != null){
		$rootScope.login = 'true';
	}
	
	$rootScope.$on('IdleTimeout', function() {
		console.log("Session is timed out.");
		$state.go('login.logout');
    });

	$rootScope.markAsRead = function (url, object){
		var deferred = $q.defer();
		$resource(url).save(object).$promise.then(function(response) {
			if(response != null){
				$rootScope.unreadNotifications.assignment = ((response.assignment || response.assignment ==0) ? response.assignment : $rootScope.unreadNotifications.assignment);
				$rootScope.unreadNotifications.payment = ((response.payment || response.payment ==0) ? response.payment : $rootScope.unreadNotifications.payment );
				$rootScope.unreadNotifications.news = ((response.news || response.news == 0) ? response.news : $rootScope.unreadNotifications.news ) ;
				$rootScope.unreadNotifications.event = ((response.event || response.event ==0) ? response.event : $rootScope.unreadNotifications.event ) ;
				$rootScope.unreadNotifications.alert = ((response.alert || response.alert ==0 ) ? response.alert : $rootScope.unreadNotifications.alert ) ;
				$rootScope.unreadNotifications.ediary = ((response.ediary || response.ediary ==0 ) ? response.ediary : $rootScope.unreadNotifications.ediary ) ;
				$localStorage.unreadNotifications = JSON.stringify($rootScope.unreadNotifications);
				deferred.resolve(response);
			}
			else {
				deferred.reject("RESPONSE NOT FOUND..");
			}
		}, function (err){
			deferred.reject(err);
		});
		return deferred.promise;
	}

	if($localStorage.user_info != null){
		$rootScope.menus = $localStorage.user_info.menus;
		$rootScope.rolePermissions = $localStorage.user_info.rolePermissions;
		$rootScope.role = $localStorage.user_info.roleId;
		$rootScope.roleId = $localStorage.user_info.roleId;
		$rootScope.instituteId = $localStorage.user_info.instituteId;
		$rootScope.branchId = $localStorage.user_info.branchId;
		$rootScope.id = $localStorage.user_info.id;
		$rootScope.entityId = $localStorage.user_info.entityId;
		$rootScope.username = $localStorage.user_info.username;
		$rootScope.name = $localStorage.user_info.name;
		$rootScope.instituteLogoUrl = $localStorage.user_info.instituteLogoUrl;
		$rootScope.instituteName = $localStorage.user_info.instituteName;
		
	}
	
	$rootScope.clearSelectedData = function (){
		$localStorage.selectedInstitute = '';
		$localStorage.selectedBranch = '';
		$localStorage.selectedStandard = '';
		$localStorage.selectedDivision = '';
	};

	$rootScope.setSelectedData = function (selectedInstitute, selectedBranch, selectedStandard, selectedDivision){
		$localStorage.selectedInstitute = selectedInstitute;
		$localStorage.selectedBranch = selectedBranch;
		$localStorage.selectedStandard = selectedStandard;
		$localStorage.selectedDivision = selectedDivision;
	};

	$rootScope.unreadNotifications = {};

	var unreadNotifications = $localStorage.unreadNotifications;
	if(unreadNotifications){
		try {
			$rootScope.unreadNotifications = JSON.parse(unreadNotifications);
		}catch(e){
			console.log(e);
		}
	}

	$rootScope.downloadTemplate = function (url){
		document.location.href = url;
	};

	$rootScope.downloadTemplateToUpdate = function (url){
		document.location.href = url;
	};

	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 
		if($localStorage.user == null && (toState.name != "login.logout" )) {
			if(toState.name != "login.log"){
				event.preventDefault();
				$state.transitionTo('login.logout');
			}
			}else{
		var allow = false;
		var rolePermissions = $rootScope.rolePermissions;
		var roleId = $rootScope.roleId;
		if(fromState.name != "login.log"){
			$rootScope.welcomeMessage = "";
		}
		
		if(rolePermissions != null){
			for(var i= 0; i< rolePermissions.length; i++){
    			if(toState.name == rolePermissions[i].url && roleId == rolePermissions[i].roleId){
 				   allow = true;
 			   }
    		}	
		}
		if ($localStorage.getItem("changePassword") == 'true' 
			&& !(toState.name == 'login.changePassword')){

			event.preventDefault();
			console.log("must chnage passwod......................");
			$state.go('login.changePassword');
		}
		else  if(localStorage.getItem("updateProfile") == 'true' 
			&& !(toState.name == 'profile.updateFirstTime') 
			&& !(toState.name == 'login.changePassword') ){

			event.preventDefault();
			console.log("must edit profile......................");
			$state.go('profile.updateFirstTime');
		}
		
		console.log($localStorage.user);

	
		
		//var stateNamesToIgnore = ['login.log','aboutus.page']; 
		if(!(toState.name == 'login.forgotPassword' || toState.name == 'login.changePassword' || 
				toState.name == 'login.log' || toState.name == 'login.resetPassword'|| 
				toState.name == 'aboutus.page' || toState.name == 'contactus.page') && !allow){

			event.preventDefault();
			   $uibModal.open({
				   templateUrl: 'app/role-access/html/unauthorized-access.html',
				     controller: function($scope,$uibModalInstance){
				    	 $scope.ok = fnOk;
				    	 function fnOk(){
				    		 $uibModalInstance.dismiss();
				    	 }
				     }
			});
		}
		}
	});	
}]);

angular.module('school').factory('ValidateButtonLinkUrl',['$rootScope',function($rootScope){
	
	function fnShowButtons(buttonName){
		var rolePermissions = $rootScope.rolePermissions;
		
		for(var i = 0;i < rolePermissions.length; i++ ){
				if(rolePermissions[i].permissionName == buttonName && rolePermissions[i].roleId == $rootScope.role){
					return true;
				}
		}
		return false;
   	}
	
	return{
		validateButtonUrl : fnShowButtons
	};
}]);

angular.module('login').factory('MenuFactory',function(){
	var menuItems = [];
	
	function setMenuItems(menus){
		menuItems = menus;
	}
	
	function getMenuItems(){
		return menuItems;
	}
	
	return{
		setMenus : setMenuItems,
		getMenus : getMenuItems
	}
});

//End by prathamesh


angular.module('login').service('modalPopupService' ,function($window, $uibModal){
	function getBrowser(){
		 	var userAgent = $window.navigator.userAgent;
		 	console.log("userAgent :::: "+userAgent);
	        var browsers = {chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i};
	        for(var key in browsers) {
	            if (browsers[key].test(userAgent)) {
	                return key;
	            }
	       };
	       return 'unknown';
	}

 	var browser = getBrowser();
	var isIEBrowser = (browser == 'ie');
	console.log("Browser....."+browser);

	function openPopup(options, disableBackdrop){
		if(isIEBrowser || disableBackdrop){
			options.backdrop = 'static';
		    options.keyboard = false;
		}

		$uibModal.open(options);
	}

	function closePopup($uibModalInstance, $uibModalStack){
		if($uibModalStack){
			$uibModalStack.dismissAll('closing');
		}
		else if($uibModalInstance){
			$uibModalInstance.dismiss('cancel');
		}
	}

	return{
		openPopup : openPopup,
		closePopup : closePopup
	};
});

//sets an error invalidDate when user types the date.
angular.module('school').directive('validDate', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, control) {
        	control.$parsers.push(function (viewValue) {
                var newDate = control.$viewValue;
                control.$setValidity("invalidDate", true);  
                if (typeof newDate === "object" || newDate == "") return newDate;  // pass through if we clicked date from popup
                if (!newDate.match(/^\d{1,2}\/\d{1,2}\/((\d{2})|(\d{4}))$/))
                    control.$setValidity("invalidDate", false);
                return viewValue;
            });
        }
    };
});

angular.module('school').directive("compareTo", function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});

angular.module('school').directive('validTime', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, control) {
        	control.$parsers.push(function (viewValue) {
                var newDate = control.$viewValue;
                control.$setValidity("invalidTime", true);  
                if (typeof newDate === "object" || newDate == "") return newDate;  // pass through if we clicked date from popup
                if (!newDate.match(/^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/))
                    control.$setValidity("invalidTime", false);
                return viewValue;
            });
        }
    };
});


angular.module('school').directive('multiSelect', function ($q) {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            selectedLabel: "@",
            availableLabel: "@",
            displayAttr: "@",
            available: "=",
            model: "=ngModel"
        },
        
        templateUrl: 'app/events/html/multi-select.html',
        link: function (scope, elm, attrs) {
            scope.selected = {
                available: [],
                current: []
            };

            /* Handles cases where scope data hasn't been initialized yet */
            var dataLoading = function (scopeAttr) {
                var loading = $q.defer();
                if (scope[scopeAttr]) {
                    loading.resolve(scope[scopeAttr]);
                } else {
                    scope.$watch(scopeAttr, function (newValue, oldValue) {
                        if (newValue !== undefined) loading.resolve(newValue);
                    });
                }
                return loading.promise;
            };

            /* Filters out items in original that are also in toFilter. Compares by reference. */
            var filterOut = function (original, toFilter) {
                var filtered = [];
                angular.forEach(original, function (entity) {
                    var match = false;
                    for (var i = 0; i < toFilter.length; i++) {
                        if (toFilter[i][attrs.displayAttr] == entity[attrs.displayAttr]) {
                            match = true;
                            break;
                        }
                    }
                    if (!match) {
                        filtered.push(entity);
                    }
                });
                return filtered;
            };

            scope.refreshAvailable = function () {
                scope.available = filterOut(scope.available, scope.model);
                scope.selected.available = [];
                scope.selected.current = [];
            };

            scope.add = function () {
                scope.model = scope.model.concat(scope.selected.available);
                scope.refreshAvailable();
            };
            scope.remove = function () {
                scope.available = scope.available.concat(scope.selected.current);
                scope.model = filterOut(scope.model, scope.selected.current);
                scope.refreshAvailable();
            };

            $q.all([dataLoading("model"), dataLoading("available")]).then(function (results) {
                scope.refreshAvailable();
            });
        }
    };
});