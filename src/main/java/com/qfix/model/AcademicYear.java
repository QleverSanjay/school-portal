package com.qfix.model;

import org.apache.log4j.Logger;

public class AcademicYear {

	private Integer id;
	private String fromDate;
	private String fromDateStr;
	private String toDate;
	private String toDateStr;
	private String isCurrentActiveYear;
	private Integer branchId;
	private String branchName;
	private Integer instituteId;
	private String instituteName;
	final static Logger logger = Logger.getLogger(AcademicYear.class);
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getFromDateStr() {
		return fromDateStr;
	}
	
	
	
	
	public String getToDateStr() {
		return toDateStr;
	}
	
	
	
	public String getIsCurrentActiveYear() {
		return isCurrentActiveYear;
	}
	public void setIsCurrentActiveYear(String isCurrentActiveYear) {
		this.isCurrentActiveYear = isCurrentActiveYear;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Integer getInstituteId() {
		return instituteId;
	}
	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	@Override
	public String toString() {
		return "AcademicYear [id=" + id + ", fromDate=" + fromDate
				+ ", fromDateStr=" + fromDateStr + ", toDate=" + toDate
				+ ", toDateStr=" + toDateStr + ", isCurrentActiveYear="
				+ isCurrentActiveYear + ", branchId=" + branchId
				+ ", branchName=" + branchName + ", instituteId=" + instituteId
				+ ", instituteName=" + instituteName + "]";
	}
}
