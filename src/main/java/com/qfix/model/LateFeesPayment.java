package com.qfix.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class LateFeesPayment {

	private List<StudentFees> studentFeesList;
	private int lateFeesCharges;
	private String discountAmount;
	private List<Long> feeScheduleIds;

	public List<Long> getFeeScheduleIds() {
		return feeScheduleIds;
	}

	public void setFeeScheduleIds(List<Long> feeScheduleIds) {
		this.feeScheduleIds = feeScheduleIds;
	}

	public String getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}

	public List<StudentFees> getStudentFeesList() {
		return studentFeesList;
	}

	public void setStudentFeesList(List<StudentFees> studentFeesList) {
		this.studentFeesList = studentFeesList;
	}

	public int getLateFeesCharges() {
		return lateFeesCharges;
	}

	public void setLateFeesCharges(int lateFeesCharges) {
		this.lateFeesCharges = lateFeesCharges;
	}

	@Override
	public String toString() {
		return "LateFeesPayment [studentFeesList=" + studentFeesList
				+ ", lateFeesCharges=" + lateFeesCharges + ", discountAmount="
				+ discountAmount + "]";
	}

}
