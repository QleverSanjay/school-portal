package com.qfix.model;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Component
public class TimetableData {
	@JsonProperty("isSkipTimetableOnHoliday")
	boolean isSkipTimetableOnHoliday;

	@JsonProperty("calendarData")
	List<TimeTableDetail> timeTableDetailList;

	@JsonProperty("nonWorkingDays")
	List<Integer> nonWorkingDays;

	private String startDate;
	private String endDate;

	private String startTime;
	private String endTime;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public boolean isSkipTimetableOnHoliday() {
		return isSkipTimetableOnHoliday;
	}

	public void setSkipTimetableOnHoliday(boolean isSkipTimetableOnHoliday) {
		this.isSkipTimetableOnHoliday = isSkipTimetableOnHoliday;
	}

	public List<TimeTableDetail> getTimeTableDetailList() {
		return timeTableDetailList;
	}

	public void setTimeTableDetailList(List<TimeTableDetail> timeTableDetailList) {
		this.timeTableDetailList = timeTableDetailList;
	}

	public List<Integer> getNonWorkingDays() {
		return nonWorkingDays;
	}

	public void setNonWorkingDays(List<Integer> nonWorkingDays) {
		this.nonWorkingDays = nonWorkingDays;
	}

}
