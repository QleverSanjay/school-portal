package com.qfix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeesReportFilter {

	private Integer instituteId;
	private Integer branchId;
	private Integer standardId;
	private Integer divisionId;
	private Integer headId;
	private Integer feesId;
	private Integer status;
	private String rollNumber;
	private String registrationCode;
	private Integer casteId;
	private String fromDate;
	private String toDate;
	private TableOptions tableOptions;
	private String isSearchClicked;
	private String qfixRefNumber;

	public String getQfixRefNumber() {
		return qfixRefNumber;
	}

	public void setQfixRefNumber(String qfixRefNumber) {
		this.qfixRefNumber = qfixRefNumber;
	}

	public String getIsSearchClicked() {
		return isSearchClicked;
	}

	public void setIsSearchClicked(String isSearchClicked) {
		this.isSearchClicked = isSearchClicked;
	}

	public TableOptions getTableOptions() {
		return tableOptions;
	}

	public void setTableOptions(TableOptions tableOptions) {
		this.tableOptions = tableOptions;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public Integer getCasteId() {
		return casteId;
	}

	public void setCasteId(Integer casteId) {
		this.casteId = casteId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public Integer getFeesId() {
		return feesId;
	}

	public void setFeesId(Integer feesId) {
		this.feesId = feesId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	@Override
	public String toString() {
		return "FeesReportFilter [instituteId=" + instituteId + ", branchId="
				+ branchId + ", standardId=" + standardId + ", divisionId="
				+ divisionId + ", headId=" + headId + ", feesId=" + feesId
				+ ", status=" + status + ", rollNumber=" + rollNumber
				+ ", registrationCode=" + registrationCode + ", casteId="
				+ casteId + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", tableOptions=" + tableOptions + "]";
	}

}
