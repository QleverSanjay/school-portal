package com.qfix.model;

import org.apache.commons.lang.RandomStringUtils;

public class PaymentAccount {

	Integer userId;

	String accountType;

	Long accountNumber;

	public PaymentAccount(Integer userId, String accountType, Long accountNumber) {
		super();
		this.userId = userId;
		this.accountType = accountType;
		this.accountNumber = accountNumber;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	// Implementation referenced from :
	// http://www.journaldev.com/1449/credit-card-check-digit-generator-java-program
	public static long generateAccountNumberWithCheckDigit() {
		String partialRandomAccountNumber = RandomStringUtils.random(15, false, true);
		int[] ints = new int[partialRandomAccountNumber.length()];
		for (int i = 0; i < partialRandomAccountNumber.length(); i++) {
			ints[i] = Integer.parseInt(partialRandomAccountNumber.substring(i, i + 1));
		}
		for (int i = ints.length - 2; i >= 0; i = i - 2) {
			int j = ints[i];
			j = j * 2;
			if (j > 9) {
				j = j % 10 + 1;
			}
			ints[i] = j;
		}
		int sum = 0;
		for (int i = 0; i < ints.length; i++) {
			sum += ints[i];
		}

		int checkDigit = 0;
		if (sum % 10 != 0) {
			checkDigit = 10 - (sum % 10);
		}
		
		return Long.valueOf(partialRandomAccountNumber + checkDigit);

	}

	public static void main(String[] args) {
		generateAccountNumberWithCheckDigit();
	}

}
