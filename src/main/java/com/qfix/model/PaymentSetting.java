package com.qfix.model;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

@Component
public class PaymentSetting {
	private Integer id;

	@NotNull()
	private String amount;

	@NotNull
	private String percentage;

	@NotNull
	private Integer instituteId;

	@NotNull
	private String gatewayId;

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	@Override
	public String toString() {
		return "PaymentSetting [id=" + id + ", amount=" + amount
				+ ", percentage=" + percentage + ", instituteId=" + instituteId
				+ ", gatewayId=" + gatewayId + "]";
	}

}
