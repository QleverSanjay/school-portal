package com.qfix.model.wrapper;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

import com.qfix.model.AcademicYear;
import com.qfix.model.Holiday;
import com.qfix.model.WorkingDay;

@Data
@NoArgsConstructor
public class AcademicYearWrapper {
	
	private Integer branchId;

	private AcademicYear academicYear;
	
	private WorkingDay workingDay;
	
	private List<Holiday> holidays;
	
}
