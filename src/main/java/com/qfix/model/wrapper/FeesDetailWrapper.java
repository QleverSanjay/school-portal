package com.qfix.model.wrapper;

import java.util.List;

import lombok.Data;

import com.qfix.model.DisplayHead;
import com.qfix.model.FeesCode;
import com.qfix.model.Head;
import com.qfix.model.LateFeesDetail;

@Data
public class FeesDetailWrapper {
	
	private List<Head> deleteHeads;
	
	private List<DisplayHead> deleteDisplayHeads;
	
	private List<FeesCode> deleteFeesCodes;
	
	private List<LateFeesDetail> deleteLateFeesDeatil;
 
	private List<Head> heads;

	private List<DisplayHead> displayHeads;

	private List<FeesCode> feesCodes;

	private List<LateFeesDetail> lateFeesDetails;

}
