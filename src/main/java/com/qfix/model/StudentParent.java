package com.qfix.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentParent {
	final static Logger logger = Logger.getLogger(StudentParent.class);

	private Integer id;
	@NotNull()
	@Pattern(regexp = "^[A-Z a-z]{2,45}$", message = "Must be valid first name.")
	private String firstname;

	@Pattern(regexp = "^$|^[A-Z a-z]{1,45}$", message = "Must be valid middle name.")
	private String middlename;

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z]{1,45}$", message = "Must be valid last name.")
	private String lastname;

	@Pattern(regexp = "^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])|(0[1-9]|[12]\\d|3[01])-(0?[1-9]|1[012])-(\\d{4})$", message = "Must be valid date eg (15-10-2015)")
	private String dob;

	@NotNull()
	@Size(max = 45, message = "Email is too long.")
	@Pattern(regexp = "^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid email address.")
	private String email;

	@NotNull()
	@Pattern(regexp = "^[FM]{1,1}$", message = "Must be valid gender.(e.g.: M or F)")
	private String gender;

	@NotNull()
	@Pattern(regexp = "^[0-9]{10,10}$", message = "Must be valid mobile number.")
	private String primaryContact;

	@Pattern(regexp = "^$|^[0-9]{10,10}$", message = "Must be valid secondary contact.")
	private String secondaryContact;

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9 , . () - /]{2,45}$", message = "Must be valid address.")
	private String addressLine;

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9 , . () -]{2,45}$", message = "Must be valid area.")
	private String area;

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z]{2,45}$", message = "Must be valid city.")
	private String city;

	@NotNull()
	@Pattern(regexp = "^[0-9]{6,6}$", message = "Must be valid pincode.")
	private String pincode;

	private Integer relationId;

	private Date dateOfBirthD;

	private String active;
	private Integer branchId;
	private String isDelete;

	private Integer stateId;
	private Integer districtId;
	private Integer talukaId;
	private Integer parentId;
	private Integer studentId;

	private String createLogin;

	@JsonProperty("income_id")
	private Integer incomeRangeId;

	@JsonProperty("profession_id")
	private Integer professionId;

	private String incomeRange;
	private String profession;
	private Integer userId;

	@JsonProperty("parentUserId")
	private String parentUserId;
	private Student student;
	private String username;
	private String password;
	private String uploadAction;

	public String getUploadAction() {
		return uploadAction;
	}

	public void setUploadAction(String uploadAction) {
		this.uploadAction = uploadAction;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}

	public String getIncomeRange() {
		return incomeRange;
	}

	public void setIncomeRange(String incomeRange) {
		this.incomeRange = incomeRange;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public Integer getIncomeRangeId() {
		return incomeRangeId;
	}

	public void setIncomeRangeId(Integer incomeRangeId) {
		this.incomeRangeId = incomeRangeId;
	}

	public Integer getProfessionId() {
		return professionId;
	}

	public void setProfessionId(Integer professionId) {
		this.professionId = professionId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(String secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

	public Date getDateOfBirthD() {
		return dateOfBirthD;
	}

	public void setDateOfBirthD(Date dateOfBirthD) {
		this.dateOfBirthD = dateOfBirthD;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTalukaId() {
		return talukaId;
	}

	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getCreateLogin() {
		return createLogin;
	}

	public void setCreateLogin(String createLogin) {
		this.createLogin = createLogin;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "StudentParent [id=" + id + ", firstname=" + firstname
				+ ", middlename=" + middlename + ", lastname=" + lastname
				+ ", dob=" + dob + ", email=" + email + ", gender=" + gender
				+ ", primaryContact=" + primaryContact + ", secondaryContact="
				+ secondaryContact + ", addressLine=" + addressLine + ", area="
				+ area + ", city=" + city + ", pincode=" + pincode
				+ ", relationId=" + relationId + ", dateOfBirthD="
				+ dateOfBirthD + ", active=" + active + ", branchId="
				+ branchId + ", isDelete=" + isDelete + ", stateId=" + stateId
				+ ", districtId=" + districtId + ", talukaId=" + talukaId
				+ ", parentId=" + parentId + ", studentId=" + studentId
				+ ", createLogin=" + createLogin + ", incomeRangeId="
				+ incomeRangeId + ", professionId=" + professionId
				+ ", incomeRange=" + incomeRange + ", profession=" + profession
				+ "]";
	}

}
