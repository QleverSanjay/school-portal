package com.qfix.model;

import java.util.List;

import lombok.Data;

@Data
public class ExpressOnboardSession {
	private Branch branch;
	private AcademicYear academicYear;
	private WorkingDay workingDay;
	private List<Holiday> holidays;
	private List<Standard> standards;
	private List<Division> divisions;
	private List<Subject> subjects;
	private List<Head> heads;
	private List<DisplayHead> displayHeads;
	private List<FeesCode> feeCodes;
	private List<LateFeesDetail> lateFees;
	private ExpressOnboardStandardSubjectList standardSubjectList;
}
