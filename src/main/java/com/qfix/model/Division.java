package com.qfix.model;

import lombok.Data;

@Data
public class Division {

	private Integer id;
	private String name;
	private String text;
	private String displayName;
	private String code;
	private Integer branchId;
	private Integer standardId;
	private String branchName;
	private Integer instituteId;
	private String instituteName;
	private Integer academicYearId;
	private String academicYearStr;
	private String qfixDefinedName;

	@Override
	public String toString() {
		return "Division [id=" + id + ", displayName=" + name
				+ ", code=" + code + ", branchId=" + branchId + ", standardId="
				+ standardId + ", branchName=" + branchName + ", instituteId="
				+ instituteId + ", instituteName=" + instituteName
				+ ", academicYearId=" + academicYearId + ", academicYearStr="
				+ academicYearStr + "]";
	}
}
