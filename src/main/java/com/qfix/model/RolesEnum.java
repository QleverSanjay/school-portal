package com.qfix.model;

public enum RolesEnum {
	
	ADMIN,STUDENT,PARENT,TEACHER,VENDOR;
	
	private final int value;

    private RolesEnum() {
        this.value = ordinal();
    }

}
