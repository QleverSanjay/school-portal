package com.qfix.model;

import java.sql.Date;

import lombok.Data;

@Data
public class TransactionReport {
	
	private String firstName;
	private String lastName;
	private String registrationCode;
	private String rollNumber;
	private Double amount;
	private String qfixReferenceNumber;
	private String paymentGatewayTransactionId;
	private String paymentMode;
	private Date createdAt;
	private String paymentGatewayResponse;
	private String payload;

}
