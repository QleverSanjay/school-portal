package com.qfix.model;

import java.util.List;

import lombok.Data;

@Data
public class ExpressOnboard {

	private List<Standard> standards;
	private String[] standardsToRemove;
}
