package com.qfix.model;

import lombok.Data;

@Data
public class DashboardPayment {
	Integer transactionCount;
	Double transactionSum;
	String transactionDate;
}
