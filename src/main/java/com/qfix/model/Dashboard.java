package com.qfix.model;

public class Dashboard {

	private Integer id;
	private Integer headId;
	private Integer branchId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	@Override
	public String toString() {
		return "Dashboard [id=" + id + ", headId=" + headId + ", branchId="
				+ branchId + "]";
	}
}
