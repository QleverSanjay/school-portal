package com.qfix.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResponse extends Response {

	@JsonProperty("Code")
	private String code;

	@JsonProperty("Message")
	private String message;

	@JsonProperty("sid")
	private String sid;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	@Override
	public String toString() {
		return "LoginResponse [code=" + code + ", message=" + message
				+ ", sid=" + sid + "]";
	}

}