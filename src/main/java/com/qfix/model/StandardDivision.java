package com.qfix.model;

import org.springframework.stereotype.Component;

@Component
public class StandardDivision {

	private Integer ids;
	private String standardName;
	private Integer standard;
	private String divisionName;
	private Integer division;

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getIds() {
		return ids;
	}

	public void setIds(Integer ids) {
		this.ids = ids;
	}

	public Integer getStandard() {
		return standard;
	}

	public void setStandard(Integer standard) {
		this.standard = standard;
	}

	public Integer getDivision() {
		return division;
	}

	public void setDivision(Integer division) {
		this.division = division;
	}

	@Override
	public String toString() {
		return "StandardDivisionList [ids=" + ids + ", standardName="
				+ standardName + ", standard=" + standard + ", divisionName="
				+ divisionName + ", division=" + division + "]";
	}
}
