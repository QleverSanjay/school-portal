package com.qfix.model;

import lombok.Data;

@Data
public class Course {
	private Integer id;
	private String name;
}
