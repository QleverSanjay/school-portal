package com.qfix.model;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class StudentReport {

	private Integer id;
	private String studentName;
	private String roleNumber;
	private List<Subject> subjectList;
	private String result;
	private String finalGrade;
	private String position;
	private String generalRemark;
	private String grossTotal;
	private Date createdDate;
	private Integer academicYearId;
	private String academicYearStr;
	private String title;
	
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getGrossTotal() {
		return grossTotal;
	}

	public void setGrossTotal(String grossTotal) {
		this.grossTotal = grossTotal;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getRoleNumber() {
		return roleNumber;
	}

	public void setRoleNumber(String roleNumber) {
		this.roleNumber = roleNumber;
	}

	public List<Subject> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<Subject> subjectList) {
		this.subjectList = subjectList;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getFinalGrade() {
		return finalGrade;
	}

	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getGeneralRemark() {
		return generalRemark;
	}

	public void setGeneralRemark(String generalRemark) {
		this.generalRemark = generalRemark;
	}

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearStr() {
		return academicYearStr;
	}

	public void setAcademicYearStr(String academicYearStr) {
		this.academicYearStr = academicYearStr;
	}

	@Override
	public String toString() {
		return "StudentReport [id=" + id + ", studentName=" + studentName
				+ ", roleNumber=" + roleNumber + ", subjectList=" + subjectList
				+ ", result=" + result + ", finalGrade=" + finalGrade
				+ ", position=" + position + ", generalRemark=" + generalRemark
				+ ", grossTotal=" + grossTotal + ", createdDate=" + createdDate
				+ ", academicYearId=" + academicYearId + ", academicYearStr="
				+ academicYearStr + "]";
	}
}
