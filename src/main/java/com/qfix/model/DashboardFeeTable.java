package com.qfix.model;

import lombok.Data;

@Data
public class DashboardFeeTable {
	String feeHeadType;
	Double paidAmount;
	Double unpaidAmount;
	Double paidAmountWithLateFees;
	
	public DashboardFeeTable(){
		this.paidAmount = 0.0;
		this.paidAmountWithLateFees = 0.0;
		this.unpaidAmount = 0.0;
	}
}
