package com.qfix.model;

import lombok.Data;

import org.springframework.stereotype.Component;

@Data
@Component
public class BranchConfig {

	private Integer id;
	private Integer branchId;
	private String studentDataAvailable;
	private String uniqueIdentifierName;
	private String uniqueIdentifierLable;
}
