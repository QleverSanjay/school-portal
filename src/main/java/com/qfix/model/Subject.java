package com.qfix.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

import org.springframework.stereotype.Component;

@Data
@Component
public class Subject {

	private Integer id;

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9]{2,45}$", message = "Must be valid subject name.")
	private String name;
	private String tag;
	private Integer branchId;
	private Integer categoryId;
	private String isDelete;
	private String code;
	private double theoryMarks;
	private double practicalMarks;
	private Integer instituteId;

	private String theory;
	private String practical;
	private String obtainedMarks;
	private String totalMarks;
	private String category;
	private String remarks;
	private Integer academicYearId;
	private String academicYearStr;
	private String qfixDefinedName;

	@Override
	public String toString() {
		return "Subject [id=" + id + ", name=" + name + ", branchId="
				+ branchId + ", categoryId=" + categoryId + ", isDelete="
				+ isDelete + ", code=" + code + ", theoryMarks=" + theoryMarks
				+ ", practicalMarks=" + practicalMarks + ", instituteId="
				+ instituteId + ", theory=" + theory + ", practical="
				+ practical + ", obtainedMarks=" + obtainedMarks
				+ ", totalMarks=" + totalMarks + ", category=" + category
				+ ", remarks=" + remarks + ", academicYearId=" + academicYearId
				+ ", academicYearStr=" + academicYearStr + "]";
	}

}
