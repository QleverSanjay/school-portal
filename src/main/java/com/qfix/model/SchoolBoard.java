package com.qfix.model;

import lombok.Data;

@Data
public class SchoolBoard {

	private Integer id;
	private String board;
}
