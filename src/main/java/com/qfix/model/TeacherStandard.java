package com.qfix.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class TeacherStandard {

	private String standard;
	private Integer standardId;
	private String division;
	private Integer divisionId;
	private String subjects;
	private List<Master> subjectList;

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

	public List<Master> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<Master> subjectList) {
		this.subjectList = subjectList;
	}

	@Override
	public String toString() {
		return "TeacherStandard [standard=" + standard + ", standardId="
				+ standardId + ", division=" + division + ", divisionId="
				+ divisionId + ", subjects=" + subjects + ", subjectList="
				+ subjectList + "]";
	}

}
