package com.qfix.model;

import java.util.List;

public class TableResponse {

	private int draw;
	private int recordsTotal;
	private int recordsFiltered;
	private List<FeesReport> feesReports;
	private List<Student> students;

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public List<FeesReport> getFeesReports() {
		return feesReports;
	}

	public void setFeesReports(List<FeesReport> data) {
		this.feesReports = data;
	}

}
