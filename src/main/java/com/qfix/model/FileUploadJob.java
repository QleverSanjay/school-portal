package com.qfix.model;

public class FileUploadJob {

	private Integer id;
	private String filePath;
	private String uniqueFileId;
	private String invalidDataFilePath;
	private Integer branchId;
	private Integer totalRecords;
	private Integer invalidRecords;
	private String invalidRecordData;
	private Integer validRecords;
	private String uploadedBy;
	private Integer uploadedById;
	private String uploadedAt;
	private String jobStatus;

	public String getInvalidRecordData() {
		return invalidRecordData;
	}

	public void setInvalidRecordData(String invalidRecordData) {
		this.invalidRecordData = invalidRecordData;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getUniqueFileId() {
		return uniqueFileId;
	}

	public void setUniqueFileId(String uniqueFileId) {
		this.uniqueFileId = uniqueFileId;
	}

	public String getInvalidDataFilePath() {
		return invalidDataFilePath;
	}

	public void setInvalidDataFilePath(String invalidDataFilePath) {
		this.invalidDataFilePath = invalidDataFilePath;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Integer getInvalidRecords() {
		return invalidRecords;
	}

	public void setInvalidRecords(Integer invalidRecords) {
		this.invalidRecords = invalidRecords;
	}

	public Integer getValidRecords() {
		return validRecords;
	}

	public void setValidRecords(Integer validRecords) {
		this.validRecords = validRecords;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Integer getUploadedById() {
		return uploadedById;
	}

	public void setUploadedById(Integer uploadedById) {
		this.uploadedById = uploadedById;
	}

	public String getUploadedAt() {
		return uploadedAt;
	}

	public void setUploadedAt(String uploadedAt) {
		this.uploadedAt = uploadedAt;
	}
}