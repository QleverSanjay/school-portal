package com.qfix.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ShoppingProfile {
	
	private String username;
	
	private String cookies;
	
	private Date cookieExpiresOn;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCookies() {
		return cookies;
	}

	public void setCookies(String cookie) {
		this.cookies = cookie;
	}

	public Date getCookieExpiresOn() {
		return cookieExpiresOn;
	}

	public void setCookieExpiresOn(Date cookiesExpiresOn) {
		this.cookieExpiresOn = cookiesExpiresOn;
	}
	
	

}
