package com.qfix.model;

public class ExcelContentModel {

	private boolean readOnly;
	private int columnIndex;

	private Object cellData;

	public ExcelContentModel(boolean readOnly, int columnIndex, Object cellData) {
		super();
		this.readOnly = readOnly;
		this.columnIndex = columnIndex;
		this.cellData = cellData;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public Object getCellData() {
		return cellData;
	}

	@Override
	public String toString() {
		return "ExcelContentModel [readOnly=" + readOnly + ", columnIndex="
				+ columnIndex + ", cellData=" + cellData + "]";
	}

}