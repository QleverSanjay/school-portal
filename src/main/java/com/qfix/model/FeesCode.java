package com.qfix.model;

import lombok.Data;

@Data
public class FeesCode {

	private Integer id;
	private String text;
	private String name;
	private String description;
	private Integer branchId;
	private Integer instituteId;
	private String isDelete;

	@Override
	public String toString() {
		return "FeesCode [id=" + id + ", name=" + name + ", description="
				+ description + ", branchId=" + branchId + ", instituteId="
				+ instituteId + "]";
	}

}
