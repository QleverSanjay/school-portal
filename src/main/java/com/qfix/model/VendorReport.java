package com.qfix.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType( propOrder = { "entity_id", "company_name", "amount" } )
@XmlRootElement ( name = "item" )
public class VendorReport {

	@XmlElement(name = "amount")
	private String amount;

	@XmlElement(name = "company_name")
	private String companyName;

	@XmlElement(name = "entity_id")
	private String entityId;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public String toString() {
		return "VendorReport [amount=" + amount + ", companyName="
				+ companyName + ", entityId=" + entityId + "]";
	}

}
