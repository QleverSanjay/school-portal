package com.qfix.model;

import lombok.Data;

import org.springframework.stereotype.Component;

@Data
@Component
public class RefundConfig {

	private Integer branchId;
	private Integer instituteId;
	private String allowCreditCardRefund;
	private String allowCreditCardPartialRefund;
	private String allowDebitCardRefund;
	private String allowDebitCardPartialRefund;
	private String allowNetBankingRefund;
	private String allowNetBankingPartialRefund;
	private String allowNeftRtgsRefund;
	private String allowNeftRtgsPartialRefund;
	private String allowChequeDDRefund;
	private String allowChequeDDPartialRefund;
	private String allowCashRefund;
	private String allowCashPartialRefund;

	public RefundConfig(Integer branchId, Integer instituteId, String allowCreditCardRefund,
			String allowCreditCardPartialRefund, String allowDebitCardRefund, String allowDebitCardPartialRefund,
			String allowNetBankingRefund, String allowNetBankingPartialRefund, String allowNeftRtgsRefund,
			String allowNeftRtgsPartialRefund, String allowChequeDDRefund, String allowChequeDDPartialRefund,
			String allowCashRefund, String allowCashPartialRefund) {

		super();
		this.branchId = branchId;
		this.instituteId = instituteId;
		this.allowCreditCardRefund = allowCreditCardRefund;
		this.allowCreditCardPartialRefund = allowCreditCardPartialRefund;
		this.allowDebitCardRefund = allowDebitCardRefund;
		this.allowDebitCardPartialRefund = allowDebitCardPartialRefund;
		this.allowNetBankingRefund = allowNetBankingRefund;
		this.allowNetBankingPartialRefund = allowNetBankingPartialRefund;
		this.allowNeftRtgsRefund = allowNeftRtgsRefund;
		this.allowNeftRtgsPartialRefund = allowNeftRtgsPartialRefund;
		this.allowChequeDDRefund = allowChequeDDRefund;
		this.allowChequeDDPartialRefund = allowChequeDDPartialRefund;
		this.allowCashRefund = allowCashRefund;
		this.allowCashPartialRefund = allowCashPartialRefund;
	}

	public RefundConfig() {
	}

}
