package com.qfix.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;

import com.qfix.utilities.Constants;

public class Holiday {

	private Integer id;
	private Integer currentAcademicYearId;
	private String academicYear;

	@NotNull()
	@Size(min = 2, max = 100, message = "length must between 2-100 charecters.")
	private String title;

	@Size(max = 255, message = "May not be greater than 255 charecters.")
	private String description;
	
	//TODO remove this variable and store date in java.util.Date format. This todo will help to remove fromDateD
	private String fromDate;
	private String fromDateStr;
	//TODO remove this variable and store date in java.util.Date format. This todo will help to remove toDateD
	private String toDate;
	private String toDateStr;
	private Integer branchId;
	private String branchName;
	private Integer instituteId;
	private String instituteName;
	private String isPublicHoliday;
	
	final static Logger logger = Logger.getLogger(Holiday.class);

	private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			"dd-MM-yyyy");

	public Integer getId() {
		return id;
	}
	
	

	public String getIsPublicHoliday() {
		return isPublicHoliday;
	}



	public void setIsPublicHoliday(String isPublicHoliday) {
		this.isPublicHoliday = isPublicHoliday;
	}



	public void setId(Integer id) {
		this.id = id;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public Integer getCurrentAcademicYearId() {
		return currentAcademicYearId;
	}

	public void setCurrentAcademicYearId(Integer currentAcademicYearId) {
		this.currentAcademicYearId = currentAcademicYearId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		try {
			Date date = simpleDateFormat.parse(fromDateStr);
			setFromDate(Constants.DATABASE_DATE_FORMAT.format(date));
		} catch (ParseException e) {
			logger.error("", e);
		}
		this.fromDateStr = fromDateStr;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		try {
			Date date = simpleDateFormat.parse(toDateStr);
			setToDate(Constants.DATABASE_DATE_FORMAT.format(date));
		} catch (ParseException e) {
			logger.error("", e);
		}
		this.toDateStr = toDateStr;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	
	@Override
	public String toString() {
		return "Holiday [id=" + id + ", currentAcademicYearId="
				+ currentAcademicYearId + ", title=" + title + ", description="
				+ description + ", fromDate=" + fromDate + ", fromDateStr="
				+ fromDateStr + ", toDate=" + toDate + ", toDateStr="
				+ toDateStr + ", branchId=" + branchId + ", branchName="
				+ branchName + ", instituteId=" + instituteId
				+ ", instituteName=" + instituteName + ", simpleDateFormat="
				+ simpleDateFormat + "]";
	}
}
