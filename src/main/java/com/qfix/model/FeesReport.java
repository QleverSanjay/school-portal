package com.qfix.model;

import java.sql.Date;
import java.util.Map;

import lombok.Data;

@Data
public class FeesReport {

	private Integer feesId;
	private Integer studentId;
	private String instituteName;
	private String branchName;
	private String standard;
	private String division;
	private String casteName;
	private String feeCodeName;
	private String studentName;
	private String rollNumber;
	private String status;
	private String head;
	private String paidDate;
	private String dueDate;
	private String feeAmount;
	private String paidAmount;
	private String remainingAmount;
	private String feesDescription;
	private String paymentOption;
	private String paymentMode;
	private String latePaymentCharges;
	private String totalAmount;
	private String discountAmount;
	private boolean recordDeleted;
	private int paymentCount;
	private boolean partialPaymentAllowed;
	private String qfixReferenceNumber;
	private String paymentReferenceDetails;
	private String paymentDetails;
	private Long feeScheduleId;
	private Long parentScheduleId;
	private Long latePaymentForFeesScheduleId;
	private boolean canPay;
	private String email;
	private String mobile;
	private Long paymentAuditId;
	private Map<Integer, String> displayHeads;

	private String bankName;
	private String bankBranchName;
	private String ifscCode;
	private String chequeOrDDNumber;
	private String registrationCode;

	private String currencyCssClass;
	private String currencyCode;
	private String paymentGatewayTransactionId;
	private Date settlementDate;
	private Map<Integer, String> splitSchemeCodes;
	
	String allowUserEnteredAmount;
	String allowRepeatEntries;
	
	String refundAmount;
	String refundedAt;
	String refundStatus;
	
}
