package com.qfix.model;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class FeesRecurringDetail {

	private String frequency;
	private String workWeeks;
	private Map<String, Boolean> byDays;
	private String byDate;


	public String getByDate() {
		return byDate;
	}

	public void setByDate(String byDate) {
		this.byDate = byDate;
	}

	public String getFrequency() {
		return frequency;
	}

	public Map<String, Boolean> getByDays() {
		return byDays;
	}

	public void setByDays(Map<String, Boolean> byDays) {
		this.byDays = byDays;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getWorkWeeks() {
		return workWeeks;
	}

	public void setWorkWeeks(String workWeeks) {
		this.workWeeks = workWeeks;
	}

	@Override
	public String toString() {
		return "FeesRecurringDetail [frequency=" + frequency + ", workWeeks="
				+ workWeeks + ", byDays=" + byDays + "]";
	}

}
