package com.qfix.model;

public class IndividualSearchData {

	private String standard;
	private String name;
	private String division;
	private String rollNumber;

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	@Override
	public String toString() {
		return "IndividualSearchData [standerd=" + standard + ", name=" + name
				+ ", division=" + division + ", rollNumber=" + rollNumber + "]";
	}

}
