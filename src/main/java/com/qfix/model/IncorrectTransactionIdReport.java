package com.qfix.model;

import lombok.Data;

@Data
public class IncorrectTransactionIdReport {
	private Integer id;
	private String transactionId;
	private String date;
	private Double amount;
	private String type;
	private String merchantId;
	private String bankName;
	private String qfixReferenceNumber;
	private String bankTransactionId;
	private String drawerName;
	private String pickupLocation;
	private String pickupPoint;
	private String clLocation;
	private String prodCode;
}
