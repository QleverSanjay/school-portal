package com.qfix.model;

public class ForgotPasswordModel {

	private String password;
	private String otp;
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return "PasswordDetails [password=" + password + ", otp=" + otp
				+ ", username=" + username + "]";
	}

}
