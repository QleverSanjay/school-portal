package com.qfix.model;

import java.util.List;

import lombok.Data;

@Data
public class ExpressOnboardStandardSubjectList {
	
	private List<ExpressOnboardStandardSubject> subjectsToRemove;
	private List<ExpressOnboardStandardSubject> subjectsToAdd;
 	
}
