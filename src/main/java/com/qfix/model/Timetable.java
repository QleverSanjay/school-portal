package com.qfix.model;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Component
public class Timetable {
	final static Logger logger = Logger.getLogger(Timetable.class);
	private Integer id;
	private Integer instituteId;
	private Integer branchId;
	private Integer standardId;
	private Integer academicYearId;
	private String standardName;
	private String divisionNames;
	private String data;
	private List<Integer> divisionIdList;

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public List<Integer> getDivisionIdList() {
		return divisionIdList;
	}

	public void setDivisionIdList(List<Integer> divisionIdList) {
		this.divisionIdList = divisionIdList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getDivisionNames() {
		return divisionNames;
	}

	public void setDivisionNames(String divisionNames) {
		this.divisionNames = divisionNames;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Timetable [id=" + id + ", branchId=" + branchId
				+ ", standardId=" + standardId + ", standardName="
				+ standardName + ", divisionNames=" + divisionNames + "]";
	}

}
