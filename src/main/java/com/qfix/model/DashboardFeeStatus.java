package com.qfix.model;

import lombok.Data;

@Data
public class DashboardFeeStatus {
	Integer feeScheduleId;
	Double partialAmountSum;
	Double latePaymentCharges;
	String feeHeadName;
}
