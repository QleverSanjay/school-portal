package com.qfix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserForumProfile {
	@JsonProperty("login")
	String login;

	@JsonProperty("password")
	String password;
 
	@JsonProperty("forumProfileId")
	Integer forumProfileId;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getForumProfileId() {
		return forumProfileId;
	}

	public void setForumProfileId(Integer forumProfileId) {
		this.forumProfileId = forumProfileId;
	}

	@Override
	public String toString() {
		return "UserForumProfile [login=" + login + ", password=" + password
				+ ", forumProfileId=" + forumProfileId + "]";
	}

}
