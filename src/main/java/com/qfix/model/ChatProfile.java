package com.qfix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
@JsonTypeName(value = "session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatProfile {
	@JsonProperty("login")
	String login;

	@JsonProperty("password")
	String password;
	
	@JsonProperty("chat_user_profile_id")
	Long chatUserProfileId;
	
	@JsonProperty("application_id")
	Long chatAppId;
	
	@JsonProperty("token")
	String token;
	
	

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getChatUserProfileId() {
		return chatUserProfileId;
	}

	public void setChatUserProfileId(Long chatUserProfileId) {
		this.chatUserProfileId = chatUserProfileId;
	}

	public Long getChatAppId() {
		return chatAppId;
	}

	public void setChatAppId(Long chatAppId) {
		this.chatAppId = chatAppId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	@Override
	public String toString() {
		return "ChatProfile [login=" + login + ", password="
				+ password + ", chatUserProfileId=" + chatUserProfileId + ", chatAppId=" + chatAppId + ", token="
				+ token +  "]";
	}
}
