package com.qfix.model;

import java.util.List;

import lombok.Data;

@Data
public class ExpressOnboardStandardSubject {

	private String name;
	private List<String> subjects;
	private List<String> courses;
}
