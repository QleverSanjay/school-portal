package com.qfix.model;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class TeacherStandardSubject {

	final static Logger logger = Logger.getLogger(TeacherStandardSubject.class);
	
	@NotNull()
	private String teacher;
	
	@NotNull()
	private String standard;
	
	private String division;
	
	private String subject;
	
	@NotNull
	private Integer branchId;
		
	private Integer teacherId;
	
	private Integer standardId;
	
	private Integer divisionId;
	
	private Integer subjectId;

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}


	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	@Override
	public String toString() {
		return "TeacherStandardSubject [teacher=" + teacher + ", standard="
				+ standard + ", division=" + division + ", subject=" + subject
				+ ", branchId=" + branchId + ", teacherId=" + teacherId
				+ ", standardId=" + standardId + ", divisionId=" + divisionId
				+ ", subjectId=" + subjectId + "]";
	}
	
	
	
}
