package com.qfix.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ValidationError {

	private String row;
	private String title;
	private List<String> errors;

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "ValidationError [row=" + row + ", title=" + title + ", errors="
				+ errors + "]";
	}

}
