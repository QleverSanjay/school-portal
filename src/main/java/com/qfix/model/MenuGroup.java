package com.qfix.model;

import java.util.LinkedList;

import lombok.Data;

@Data
public class MenuGroup {

	private String group;
	
	private String tag;
	
	private LinkedList<MenuGroupItem> groupItems;
}
