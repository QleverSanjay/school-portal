package com.qfix.model;

import org.springframework.stereotype.Component;

@Component
public class StudentFees {

	private int studentId;
	private int feesId;
	private long feeScheduleId;

	public long getFeeScheduleId() {
		return feeScheduleId;
	}

	public void setFeeScheduleId(long feeScheduleId) {
		this.feeScheduleId = feeScheduleId;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getFeesId() {
		return feesId;
	}

	public void setFeesId(int feesId) {
		this.feesId = feesId;
	}

	@Override
	public String toString() {
		return "StudentFees [studentId=" + studentId + ", feesId=" + feesId
				+ "]";
	}

}
