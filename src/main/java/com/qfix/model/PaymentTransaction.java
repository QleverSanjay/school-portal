package com.qfix.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentTransaction {

	@NotNull
	@JsonProperty("fees_id")
	Integer feesId;

	@NotNull
	@JsonProperty("amount")
	String amount;

	@JsonProperty("head")
	String head;

	@JsonProperty("description")
	String description;

	@NotNull
	@JsonProperty("student_id")
	Integer studentId;

	@JsonProperty("student_name")
	String studentName;

	@JsonProperty("due_date")
	String dueDate;

	@JsonProperty("paid_date")
	String paidDate;

	@NotNull
	@JsonProperty("ispaid")
	int isPaidFlag;

	@JsonProperty("can_mark_as_unpaid")
	boolean canBeMarkedAsUnpaid;

	@NotNull
	@JsonProperty("payment_id")
	Integer paymentId;

	@JsonProperty("late_payment_charges")
	float latePaymentCharges;

	@JsonProperty("is_split_payment")
	boolean isSplitPayment;

	@JsonProperty("is_partial_payment_allowed")
	boolean isPartialPaymentAllowed;

	@JsonIgnore
	float discountAmount;

	@JsonProperty("fee_schedule_id")
	long feeScheduleId;

	@JsonProperty("can_pay")
	boolean canPay;

	public PaymentTransaction() {

	}

	public PaymentTransaction(Integer feesId, String amount, String feesHead, String feesDescription,
					Integer studentId, String studentName, String dueDate,
					String paidDate,
					int isPaidFlag, boolean canBeMarkedAsUnpaid, Integer paymentId,
					float latePaymentCharges, boolean isSplitPayment, boolean isPartialPaymentAllowed,
					float discountAmount, long feeScheduleId, boolean canPay) {
		this.feesId = feesId;
		this.amount = amount;
		this.head = feesHead;
		this.description = feesDescription;
		this.studentId = studentId;
		this.studentName = studentName;
		this.dueDate = dueDate;
		this.isPaidFlag = isPaidFlag;
		this.paidDate = paidDate;
		this.canBeMarkedAsUnpaid = canBeMarkedAsUnpaid;
		this.paymentId = paymentId;
		this.latePaymentCharges = latePaymentCharges;
		this.isSplitPayment = isSplitPayment;
		this.isPartialPaymentAllowed = isPartialPaymentAllowed;
		this.discountAmount = discountAmount;
		this.canPay = canPay;
		this.feeScheduleId = feeScheduleId;
	}

	public PaymentTransaction(Integer feesId, String amount, String feesHead, String feesDescription,
					Integer studentId, String studentName, String dueDate,
					String paidDate,
					int isPaidFlag, boolean canBeMarkedAsUnpaid, Integer paymentId, boolean isSplitPayment,
					boolean isPartialPaymentAllowed, Long feeScheduleId) {
		this.feesId = feesId;
		this.amount = amount;
		this.head = feesHead;
		this.description = feesDescription;
		this.studentId = studentId;
		this.studentName = studentName;
		this.dueDate = dueDate;
		this.isPaidFlag = isPaidFlag;
		this.paidDate = paidDate;
		this.canBeMarkedAsUnpaid = canBeMarkedAsUnpaid;
		this.paymentId = paymentId;
		this.isSplitPayment = isSplitPayment;
		this.isPartialPaymentAllowed = isPartialPaymentAllowed;
		this.feeScheduleId = feeScheduleId;
	}

	public Integer getFeesId() {
		return feesId;
	}

	public long getFeeScheduleId() {
		return feeScheduleId;
	}

	public void setFeeScheduleId(long feeScheduleId) {
		this.feeScheduleId = feeScheduleId;
	}

	public boolean isCanPay() {
		return canPay;
	}

	public void setCanPay(boolean canPay) {
		this.canPay = canPay;
	}

	public void setFeesId(Integer feesId) {
		this.feesId = feesId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getPaidDate() {
		return (paidDate == null ? "" : paidDate);
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public int getIsPaidFlag() {
		return isPaidFlag;
	}

	public void setIsPaidFlag(int isPaidFlag) {
		this.isPaidFlag = isPaidFlag;
	}

	public boolean isCanBeMarkedAsUnpaid() {
		return canBeMarkedAsUnpaid;
	}

	public void setCanBeMarkedAsUnpaid(boolean canBeMarkedAsUnpaid) {
		this.canBeMarkedAsUnpaid = canBeMarkedAsUnpaid;
	}

	public Integer getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}

	public float getLatePaymentCharges() {
		return latePaymentCharges;
	}

	public void setLatePaymentCharges(float latePaymentCharges) {
		this.latePaymentCharges = latePaymentCharges;
	}

	public boolean isSplitPayment() {
		return isSplitPayment;
	}

	public void setSplitPayment(boolean isSplitPayment) {
		this.isSplitPayment = isSplitPayment;
	}

	public boolean isPartialPaymentAllowed() {
		return isPartialPaymentAllowed;
	}

	public void setPartialPaymentAllowed(boolean isPartialPaymentAllowed) {
		this.isPartialPaymentAllowed = isPartialPaymentAllowed;
	}

	public float getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(float discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Override
	public String toString() {
		return "PaymentTransaction [feesId=" + feesId + ", amount=" + amount + ", head=" + head + ", description=" + description + ", studentId=" + studentId
						+ ", studentName=" + studentName + ", dueDate=" + dueDate + ", paidDate=" + paidDate + ", isPaidFlag=" + isPaidFlag
						+ ", canBeMarkedAsUnpaid=" + canBeMarkedAsUnpaid + ", paymentId=" + paymentId + ", latePaymentCharges=" + latePaymentCharges
						+ ", isSplitPayment=" + isSplitPayment + ", isPartialPaymentAllowed=" + isPartialPaymentAllowed + ", discountAmount=" + discountAmount
						+ ", feeScheduleId=" + feeScheduleId + ", canPay=" + canPay + "]";
	}

}
