package com.qfix.model;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.hibernate.validator.constraints.NotEmpty;

@Data
public class DisplayHead {

	private Integer id;

	@NotEmpty
	private String name;
	private String text;
	private String description;
	private Double amount;
	private String active;
	private String isDelete;
	@NotNull
	private Integer branchId;
	private Integer instituteId;
	private Integer displayOrder;

	@Override
	public String toString() {
		return "DisplayLyout [id=" + id + ", feeDescription=" + name
				+ ", otherDescription=" + description + ", feeAmount=" + amount
				+ ", branchId=" + branchId + "]";
	}

}
