package com.qfix.model;

import lombok.Data;

@Data
public class AuditLog {
	private Integer id;
	private String entityType;
	private String userId;
	private String username;
	private String operationDate;
	private String operationType;
	private String ipAddress;
	private String oldData;
	private String newData;
}
