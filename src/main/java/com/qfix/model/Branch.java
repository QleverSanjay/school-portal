package com.qfix.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

import org.springframework.stereotype.Component;

@Data
@Component
public class Branch {

	private Integer id;
	@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9 , . () -]{2,100}$", message = "Must be valid branch name.")
	private String name;
	private String tag;
	
	@Size(max = 255, message = "Address is too long.")
	private String addressLine;

	@Size(max = 45, message = "Area is too long.")
	private String area;

	private Integer stateId;
	private Integer talukaId;

	@Pattern(regexp = "^$|^[A-Z a-z]{2,45}$", message = "Must be valid state.")
	private String state;

	@Pattern(regexp = "^$|^[A-Z a-z, . () -]{2,45}$", message = "Must be valid taluka.")
	private String taluka;

	@Pattern(regexp = "^$|^[A-Z a-z]{2,45}$", message = "Must be valid district.")
	private String district;

//	@NotNull()
	// @Pattern(regexp = "^[A-Z a-z]{2,45}$", message = "Must be valid city.")
	private String city;

	@NotNull()
	@Pattern(regexp = "^[0-9]{6,6}$", message = "Must be valid pin code.")
	private String pinCode;

	// @NotNull()
	@Pattern(regexp = "^$|^[0-9]{10,10}$", message = "Contact number must be of 10 digits.")
	private String contactNumber;

	@Pattern(regexp = "^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid email address.")
	private String contactEmail;

	@Size(max = 45, message = "Url is too long.")
	@Pattern(regexp = "$|^(http(s?):\\/\\/)?(www\\.)+[a-zA-Z0-9\\.\\-\\_]+(\\.[a-zA-Z]{2,3})+(\\/[a-zA-Z0-9\\_\\-\\s\\.\\/\\?\\%\\#\\&\\=]*)?$", message = "Must be valid url.")
	private String websiteUrl;

	private Integer instituteId;
	private String active;
	private String offlinePaymentsEnabled;
	private String showLogoOnParentPortal;


	private String principalName;

	@Pattern(regexp = "^$|^[0-9]{10,10}$", message = "Contact number must be of 10 digits.")
	private String principalMobile;

	@Pattern(regexp = "^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid email address.")
	private String principalEmailId;
	private String principalGender;
	private Integer principalUserId;
	private Integer boardAffiliation;
	private Integer university;
	private String instituteType;
	private String isExpressOnboard;
	private String logoUrl;

	private String isPayDirectEnabled;
	private String isOtpEnabled;
	
	private String uniqueIdentifierName;
	private String uniqueIdentifierLabel;
	
	private Integer districtId;

	private List<BranchAdmin> branchAdminList;
	private String boardName;
	private Integer boardId;

	@Override
	public String toString() {
		return "Branch [id=" + id + ", name=" + name + ", addressLine="
				+ addressLine + ", area=" + area + ", stateId=" + stateId
				+ ", districtId=" + districtId + ", talukaId=" + talukaId
				+ ", state=" + state + ", taluka=" + taluka + ", district="
				+ district + ", city=" + city + ", pinCode=" + pinCode
				+ ", contactNumber=" + contactNumber + ", contactEmail="
				+ contactEmail + ", websiteUrl=" + websiteUrl
				+ ", instituteId=" + instituteId + ", active=" + active
				+ ", branchAdminList=" + branchAdminList + ", principalName="
				+ principalName + ",principalMobile=" + principalMobile
				+ ",principalGender=" + principalGender + "]";
	}
}