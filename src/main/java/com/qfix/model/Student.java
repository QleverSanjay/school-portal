package com.qfix.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Student {
	final static Logger logger = Logger.getLogger(Student.class);
	private Integer id;

	@NotNull(message = "Student`s first name is mandatory")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	@Size(max = 45, message = "Student`s first name cannot be greater than 45 letters.")
	private String firstname;

	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	@Size(max = 45, message = "Middle name cannot be greater than 45 letters.")
	private String middleName;

	private String createStudentLogin;

	private String createFatherLogin;

	private String createMotherLogin;

	private String createParentLogin;

	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String fatherName;

	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String motherName;

	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	@NotNull(message = "Student`s surname is mandatory")
	@Size(max = 45, message = "Student`s surname cannot be greater than 45 letters.")
	private String surname;

	private String studentUserId;

	@Pattern(regexp = "$|^[FM]{1,1}$", message = "Must be valid gender.(e.g.: M or F)")
	private String gender;

	// @Pattern(regexp =
	// "$|^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])|(0[1-9]|[12]\\d|3[01])-(0?[1-9]|1[012])-(\\d{4})$",
	// message = "Must be valid date eg (15-10-2015)")
	private String dateOfBirth;

	private String relationship;
	private String branch;

	private Integer branchId;
	private List<Integer> feesCodeId;
	private String feesCodeName;
	private String feesCodeName1;
	private String feesCodeName2;
	private String feesCodeName3;
	private String feesCodeName4;
	private String feesCodeName5;
	
	

	public String getFeesCodeName1() {
		return feesCodeName1;
	}

	public void setFeesCodeName1(String feesCodeName1) {
		this.feesCodeName1 = feesCodeName1;
	}

	public String getFeesCodeName2() {
		return feesCodeName2;
	}

	public void setFeesCodeName2(String feesCodeName2) {
		this.feesCodeName2 = feesCodeName2;
	}

	public String getFeesCodeName3() {
		return feesCodeName3;
	}

	public void setFeesCodeName3(String feesCodeName3) {
		this.feesCodeName3 = feesCodeName3;
	}

	public String getFeesCodeName4() {
		return feesCodeName4;
	}

	public void setFeesCodeName4(String feesCodeName4) {
		this.feesCodeName4 = feesCodeName4;
	}

	public String getFeesCodeName5() {
		return feesCodeName5;
	}

	public void setFeesCodeName5(String feesCodeName5) {
		this.feesCodeName5 = feesCodeName5;
	}

	@Size(max = 45, message = "Student`s email is too long must not be graeter than 45 letters.")
	@Pattern(regexp = "$|^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid email address.")
	private String emailAddress;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid mobile number.")
	private String mobile;

	@Size(max = 200, message = "Must be less than 200 charecters.")
	private String addressLine1;

	private String area;

	@Size(max = 45, message = "Must be less than 45 charecters")
	// @Pattern(regexp = "$|^[A-Z a-z]{2,45}$", message = "Must be valid city.")
	private String city;

	@Pattern(regexp = "$|^[0-9]{6,6}$", message = "Must be valid pincode.")
	private String pincode;

	private String religion;

	@Pattern(regexp = "$|^[A-Za-z,  () -]{2,45}$", message = "Must be valid caste.")
	// add - this for upload of student cast
	private String caste;

	private String school;
	private String otherSchool;
	private String otp;
	private String academicYear;
	private String branchName;
	private String level;
	private Integer standardId;
	@Pattern(regexp = "$|^[A-Z a-z 0-9]{1,35}$", message = "Must be valid standard.")
	private String standardName;

	private Integer divisionId;
	private String courseTerm;
	private String courseName;
	private String coursePattern;
	private String specialisation;
	private String courseType;
	private String Year;
	private String parentProfession;
	private String parentIncome;
	private String fatherProfession;
	private String fatherIncome;
	private String motherProfession;
	private String motherIncome;

	private boolean isParentAvailable;
	private boolean isFatherAvailable;
	private boolean isMotherAvailable;

	private String fatherUserId;

	private String motherUserId;

	private String parentUserId;

	private String studentUploadAction;
	private String fatherUploadAction;
	private String motherUploadAction;
	private String guardianUploadAction;

	private Integer fatherId;
	private Integer motherId;
	private Integer guardianId;
	
	
	
	public String getStudentUploadAction() {
		return studentUploadAction;
	}

	public void setStudentUploadAction(String studentUploadAction) {
		this.studentUploadAction = studentUploadAction;
	}

	public String getFatherUploadAction() {
		return fatherUploadAction;
	}

	public void setFatherUploadAction(String fatherUploadAction) {
		this.fatherUploadAction = fatherUploadAction;
	}

	public String getMotherUploadAction() {
		return motherUploadAction;
	}

	public void setMotherUploadAction(String motherUploadAction) {
		this.motherUploadAction = motherUploadAction;
	}

	public String getGuardianUploadAction() {
		return guardianUploadAction;
	}

	public void setGuardianUploadAction(String guardianUploadAction) {
		this.guardianUploadAction = guardianUploadAction;
	}

	public Integer getFatherId() {
		return fatherId;
	}

	public void setFatherId(Integer fatherId) {
		this.fatherId = fatherId;
	}

	public Integer getMotherId() {
		return motherId;
	}

	public void setMotherId(Integer motherId) {
		this.motherId = motherId;
	}

	public Integer getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Integer guardianId) {
		this.guardianId = guardianId;
	}

	public String getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}

	public String getStudentUserId() {
		return studentUserId;
	}

	public void setStudentUserId(String studentUserId) {
		this.studentUserId = studentUserId;
	}

	public String getFatherUserId() {
		return fatherUserId;
	}

	public void setFatherUserId(String fatherUserId) {
		this.fatherUserId = fatherUserId;
	}

	public String getMotherUserId() {
		return motherUserId;
	}

	public void setMotherUserId(String motherUserId) {
		this.motherUserId = motherUserId;
	}

	public boolean isParentAvailable() {
		return isParentAvailable;
	}

	public boolean isFatherAvailable() {
		return isFatherAvailable;
	}

	public boolean isMotherAvailable() {
		return isMotherAvailable;
	}

	public String getFatherProfession() {
		return fatherProfession;
	}

	public void setFatherProfession(String fatherProfession) {
		if (!StringUtils.isEmpty(fatherProfession)) {
			isFatherAvailable = true;
		}
		this.fatherProfession = fatherProfession;
	}

	public String getFatherIncome() {
		return fatherIncome;
	}

	public void setFatherIncome(String fatherIncome) {
		if (!StringUtils.isEmpty(fatherIncome)) {
			isFatherAvailable = true;
		}
		this.fatherIncome = fatherIncome;
	}

	public String getMotherProfession() {
		return motherProfession;
	}

	public void setMotherProfession(String motherProfession) {
		if (!StringUtils.isEmpty(motherProfession)) {
			isMotherAvailable = true;
		}
		this.motherProfession = motherProfession;
	}

	public String getMotherIncome() {
		return motherIncome;
	}

	public void setMotherIncome(String motherIncome) {
		if (!StringUtils.isEmpty(motherIncome)) {
			isMotherAvailable = true;
		}
		this.motherIncome = motherIncome;
	}

	public List<Integer> getFeesCodeId() {
		return feesCodeId;
	}

	public void setFeesCodeId(List<Integer> feesCodeId) {
		this.feesCodeId = feesCodeId;
	}

	public String getFeesCodeName() {
		return feesCodeName;
	}

	public void setFeesCodeName(String feesCodeName) {
		this.feesCodeName = feesCodeName;
	}

	// @Pattern(regexp = "^[0-9]{1,45}$", message =
	// "Must be valid roll number.")
	@Size(max = 45, message = "Must not be greater than 45 charecters.")
	private String rollNumber;

	@Size(max = 45, message = "Must not be greater than 45 charecters.")
	private String studentRegistrationId;

	private Integer casteId;
	private String photo;
	private Integer instituteId;

	@Size(max = 45, message = "Must not be greater than 45 charecters.")
	// @Pattern(regexp = "^[A-Z a-z 0-9]{2,10}$", message =
	// "Must be valid registration code.")
	private String registrationCode;

	private String active;
	private String isDelete;

	private String divisionName;

	private String password;
	private boolean fileChanegd;
	private String name;

	private String username;

	private Integer stateId;
	private Integer districtId;
	private Integer talukaId;

	private String state;
	private String district;
	private String taluka;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String fatherFirstname;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String fatherMiddleName;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String fatherSurname;

	private String fatherBirthDate;

	@Size(max = 45, message = "Father's Email is too long.")
	@Pattern(regexp = "$|^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid father's email address.")
	private String fatherEmailAddress;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid father's primary mobile number.")
	private String fatherPrimaryMobileNumber;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid father's secondary mobile number.")
	private String fatherSecondaryMobileNumber;

	@Pattern(regexp = "/^[^'\"]*$/", message = "Must be valid address single quote and double quotes not allowed.")
	@Size(max = 100, message = "Must be less than 100 charecters.")
	private String fatherAddressLine1;

	@Pattern(regexp = "/^[^'\"]*$/", message = "Must be valid address single quote and double quotes not allowed.")
	private String fatherArea;

	@Size(max = 45, message = "Must be less than 45 charecters")
	// @Pattern(regexp = "$|^[A-Z a-z]{2,45}$", message =
	// "Must be valid father's city.")
	private String fatherCity;

	@Pattern(regexp = "$|^[0-9]{6,6}$", message = "Must be valid father's pincode.")
	private String fatherPincode;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String motherFirstname;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String motherMiddleName;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String motherSurname;

	private String motherBirthDate;

	@Size(max = 45, message = "mother's Email is too long.")
	@Pattern(regexp = "$|^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid mother's email address.")
	private String motherEmailAddress;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid mother's primary mobile number.")
	private String motherPrimaryMobileNumber;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid mother's secondary mobile number.")
	private String motherSecondaryMobileNumber;

	@Pattern(regexp = "/^[^'\"]*$/", message = "Must be valid address single quote and double quotes not allowed.")
	@Size(max = 100, message = "Must be less than 100 charecters.")
	private String motherAddressLine1;

	@Pattern(regexp = "/^[^'\"]*$/", message = "Must be valid address single quote and double quotes not allowed.")
	private String motherArea;

	@Size(max = 45, message = "Must be less than 45 charecters")
	// @Pattern(regexp = "$|^[A-Z a-z]{2,45}$", message =
	// "Must be valid mother's city.")
	private String motherCity;

	@Pattern(regexp = "$|^[0-9]{6,6}$", message = "Must be valid mother's pincode.")
	private String motherPincode;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String parentFirstname;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String parentMiddleName;

	@Size(max = 45, message = "Must be less than 45 charecters.")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	private String parentSurname;

	private String parentBirthDate;

	@Size(max = 45, message = "parent's Email is too long.")
	@Pattern(regexp = "$|^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid parent's email address.")
	private String parentEmailAddress;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid parent's primary mobile number.")
	private String parentPrimaryMobileNumber;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid parent's secondary mobile number.")
	private String parentSecondaryMobileNumber;

	@Pattern(regexp = "/^[^'\"]*$/", message = "Must be valid address single quote and double quotes not allowed.")
	@Size(max = 100, message = "Must be less than 100 charecters.")
	private String parentAddressLine1;

	@Pattern(regexp = "/^[^'\"]*$/", message = "Must be valid address single quote and double quotes not allowed.")
	private String parentArea;

	@Size(max = 45, message = "Must be less than 45 charecters")
	// @Pattern(regexp = "$|^[A-Z a-z]{2,45}$", message =
	// "Must be valid parent's city.")
	private String parentCity;

	@Pattern(regexp = "$|^[0-9]{6,6}$", message = "Must be valid parent's pincode.")
	private String parentPincode;

	private String parentRelationship;

	private String parentGender;

	private List<StudentParent> studentParentList;
	private Integer userId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getFatherName() {
		return fatherName;
	}

	public String getParentProfession() {
		return parentProfession;
	}

	public void setParentProfession(String parentProfession) {
		this.parentProfession = parentProfession;
	}

	public String getParentIncome() {
		return parentIncome;
	}

	public void setParentIncome(String parentIncome) {
		this.parentIncome = parentIncome;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getOtherSchool() {
		return otherSchool;
	}

	public void setOtherSchool(String otherSchool) {
		this.otherSchool = otherSchool;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getCourseTerm() {
		return courseTerm;
	}

	public void setCourseTerm(String courseTerm) {
		this.courseTerm = courseTerm;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCoursePattern() {
		return coursePattern;
	}

	public void setCoursePattern(String coursePattern) {
		this.coursePattern = coursePattern;
	}

	public String getSpecialisation() {
		return specialisation;
	}

	public void setSpecialisation(String specialisation) {
		this.specialisation = specialisation;
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getYear() {
		return Year;
	}

	public void setYear(String year) {
		Year = year;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getStudentRegistrationId() {
		return studentRegistrationId;
	}

	public void setStudentRegistrationId(String studentRegistrationId) {
		this.studentRegistrationId = studentRegistrationId;
	}

	public Integer getCasteId() {
		return casteId;
	}

	public void setCasteId(Integer casteId) {
		this.casteId = casteId;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isFileChanegd() {
		return fileChanegd;
	}

	public void setFileChanegd(boolean fileChanegd) {
		this.fileChanegd = fileChanegd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTalukaId() {
		return talukaId;
	}

	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTaluka() {
		return taluka;
	}

	public void setTaluka(String taluka) {
		this.taluka = taluka;
	}

	public List<StudentParent> getStudentParentList() {
		return studentParentList;
	}

	public void setStudentParentList(List<StudentParent> studentParentList) {
		this.studentParentList = studentParentList;
	}

	public String getFatherFirstname() {
		return fatherFirstname;
	}

	public void setFatherFirstname(String fatherFirstname) {
		if (!StringUtils.isEmpty(fatherFirstname)) {
			isFatherAvailable = true;
		}
		this.fatherFirstname = fatherFirstname;
	}

	public String getFatherMiddleName() {
		return fatherMiddleName;
	}

	public void setFatherMiddleName(String fatherMiddleName) {
		if (!StringUtils.isEmpty(fatherMiddleName)) {
			isFatherAvailable = true;
		}
		this.fatherMiddleName = fatherMiddleName;
	}

	public String getFatherSurname() {
		return fatherSurname;
	}

	public void setFatherSurname(String fatherSurname) {
		if (!StringUtils.isEmpty(fatherSurname)) {
			isFatherAvailable = true;
		}
		this.fatherSurname = fatherSurname;
	}

	public String getFatherBirthDate() {
		return fatherBirthDate;
	}

	public void setFatherBirthDate(String fatherBirthDate) {
		if (fatherBirthDate != null) {
			isFatherAvailable = true;
		}
		this.fatherBirthDate = fatherBirthDate;
	}

	public String getFatherEmailAddress() {
		return fatherEmailAddress;
	}

	public void setFatherEmailAddress(String fatherEmailAddress) {
		if (!StringUtils.isEmpty(fatherEmailAddress)) {
			isFatherAvailable = true;
		}
		this.fatherEmailAddress = fatherEmailAddress;
	}

	public String getFatherPrimaryMobileNumber() {
		return fatherPrimaryMobileNumber;
	}

	public void setFatherPrimaryMobileNumber(String fatherPrimaryMobileNumber) {
		if (!StringUtils.isEmpty(fatherPrimaryMobileNumber)) {
			isFatherAvailable = true;
		}
		this.fatherPrimaryMobileNumber = fatherPrimaryMobileNumber;
	}

	public String getFatherSecondaryMobileNumber() {
		return fatherSecondaryMobileNumber;
	}

	public void setFatherSecondaryMobileNumber(
			String fatherSecondaryMobileNumber) {
		if (!StringUtils.isEmpty(fatherSecondaryMobileNumber)) {
			isFatherAvailable = true;
		}
		this.fatherSecondaryMobileNumber = fatherSecondaryMobileNumber;
	}

	public String getFatherAddressLine1() {
		return fatherAddressLine1;
	}

	public void setFatherAddressLine1(String fatherAddressLine1) {
		if (!StringUtils.isEmpty(fatherAddressLine1)) {
			isFatherAvailable = true;
		}
		this.fatherAddressLine1 = fatherAddressLine1;
	}

	public String getFatherArea() {
		return fatherArea;
	}

	public void setFatherArea(String fatherArea) {
		if (!StringUtils.isEmpty(fatherArea)) {
			isFatherAvailable = true;
		}
		this.fatherArea = fatherArea;
	}

	public String getFatherCity() {
		return fatherCity;
	}

	public void setFatherCity(String fatherCity) {
		if (!StringUtils.isEmpty(fatherCity)) {
			isFatherAvailable = true;
		}
		this.fatherCity = fatherCity;
	}

	public String getFatherPincode() {
		return fatherPincode;
	}

	public void setFatherPincode(String fatherPincode) {
		if (!StringUtils.isEmpty(fatherPincode)) {
			isFatherAvailable = true;
		}
		this.fatherPincode = fatherPincode;
	}

	public String getMotherFirstname() {
		return motherFirstname;
	}

	public void setMotherFirstname(String motherFirstname) {
		if (!StringUtils.isEmpty(motherFirstname)) {
			isMotherAvailable = true;
		}
		this.motherFirstname = motherFirstname;
	}

	public String getMotherMiddleName() {
		return motherMiddleName;
	}

	public void setMotherMiddleName(String motherMiddleName) {
		if (!StringUtils.isEmpty(motherMiddleName)) {
			isMotherAvailable = true;
		}
		this.motherMiddleName = motherMiddleName;
	}

	public String getMotherSurname() {
		return motherSurname;
	}

	public void setMotherSurname(String motherSurname) {
		if (!StringUtils.isEmpty(motherSurname)) {
			isMotherAvailable = true;
		}
		this.motherSurname = motherSurname;
	}

	public String getMotherBirthDate() {
		return motherBirthDate;
	}

	public void setMotherBirthDate(String motherBirthDate) {
		if (motherBirthDate != null) {
			isMotherAvailable = true;
		}
		this.motherBirthDate = motherBirthDate;
	}

	public String getMotherEmailAddress() {
		return motherEmailAddress;
	}

	public void setMotherEmailAddress(String motherEmailAddress) {
		if (!StringUtils.isEmpty(motherEmailAddress)) {
			isMotherAvailable = true;
		}
		this.motherEmailAddress = motherEmailAddress;
	}

	public String getMotherPrimaryMobileNumber() {
		return motherPrimaryMobileNumber;
	}

	public void setMotherPrimaryMobileNumber(String motherPrimaryMobileNumber) {
		if (!StringUtils.isEmpty(motherPrimaryMobileNumber)) {
			isMotherAvailable = true;
		}
		this.motherPrimaryMobileNumber = motherPrimaryMobileNumber;
	}

	public String getMotherSecondaryMobileNumber() {
		return motherSecondaryMobileNumber;
	}

	public void setMotherSecondaryMobileNumber(
			String motherSecondaryMobileNumber) {
		if (!StringUtils.isEmpty(motherSecondaryMobileNumber)) {
			isMotherAvailable = true;
		}
		this.motherSecondaryMobileNumber = motherSecondaryMobileNumber;
	}

	public String getMotherAddressLine1() {
		return motherAddressLine1;
	}

	public void setMotherAddressLine1(String motherAddressLine1) {
		if (!StringUtils.isEmpty(motherAddressLine1)) {
			isMotherAvailable = true;
		}
		this.motherAddressLine1 = motherAddressLine1;
	}

	public String getMotherArea() {
		return motherArea;
	}

	public void setMotherArea(String motherArea) {
		if (!StringUtils.isEmpty(motherArea)) {
			isMotherAvailable = true;
		}
		this.motherArea = motherArea;
	}

	public String getMotherCity() {
		return motherCity;
	}

	public void setMotherCity(String motherCity) {
		if (!StringUtils.isEmpty(motherCity)) {
			isMotherAvailable = true;
		}
		this.motherCity = motherCity;
	}

	public String getMotherPincode() {
		return motherPincode;
	}

	public void setMotherPincode(String motherPincode) {
		if (!StringUtils.isEmpty(motherPincode)) {
			isMotherAvailable = true;
		}
		this.motherPincode = motherPincode;
	}

	public String getParentFirstname() {
		return parentFirstname;
	}

	public void setParentFirstname(String parentFirstname) {
		if (!StringUtils.isEmpty(parentFirstname)) {
			isParentAvailable = true;
		}
		this.parentFirstname = parentFirstname;
	}

	public String getParentMiddleName() {
		return parentMiddleName;
	}

	public void setParentMiddleName(String parentMiddleName) {
		if (!StringUtils.isEmpty(parentMiddleName)) {
			isParentAvailable = true;
		}
		this.parentMiddleName = parentMiddleName;
	}

	public String getParentSurname() {
		return parentSurname;
	}

	public void setParentSurname(String parentSurname) {
		if (!StringUtils.isEmpty(parentSurname)) {
			isParentAvailable = true;
		}
		this.parentSurname = parentSurname;
	}

	public String getParentBirthDate() {
		return parentBirthDate;
	}

	public void setParentBirthDate(String parentBirthDate) {
		if (parentBirthDate != null) {
			isParentAvailable = true;
		}
		this.parentBirthDate = parentBirthDate;
	}

	public String getParentEmailAddress() {
		return parentEmailAddress;
	}

	public void setParentEmailAddress(String parentEmailAddress) {
		if (!StringUtils.isEmpty(parentEmailAddress)) {
			isParentAvailable = true;
		}
		this.parentEmailAddress = parentEmailAddress;
	}

	public String getParentPrimaryMobileNumber() {
		return parentPrimaryMobileNumber;
	}

	public void setParentPrimaryMobileNumber(String parentPrimaryMobileNumber) {
		if (!StringUtils.isEmpty(parentPrimaryMobileNumber)) {
			isParentAvailable = true;
		}
		this.parentPrimaryMobileNumber = parentPrimaryMobileNumber;
	}

	public String getParentSecondaryMobileNumber() {
		return parentSecondaryMobileNumber;
	}

	public void setParentSecondaryMobileNumber(
			String parentSecondaryMobileNumber) {
		if (!StringUtils.isEmpty(parentSecondaryMobileNumber)) {
			isParentAvailable = true;
		}
		this.parentSecondaryMobileNumber = parentSecondaryMobileNumber;
	}

	public String getParentAddressLine1() {
		return parentAddressLine1;
	}

	public void setParentAddressLine1(String parentAddressLine1) {
		if (!StringUtils.isEmpty(parentAddressLine1)) {
			isParentAvailable = true;
		}
		this.parentAddressLine1 = parentAddressLine1;
	}

	public String getParentArea() {
		return parentArea;
	}

	public void setParentArea(String parentArea) {
		if (!StringUtils.isEmpty(parentArea)) {
			isParentAvailable = true;
		}
		this.parentArea = parentArea;
	}

	public String getParentCity() {
		return parentCity;
	}

	public void setParentCity(String parentCity) {
		if (!StringUtils.isEmpty(parentCity)) {
			isParentAvailable = true;
		}
		this.parentCity = parentCity;
	}

	public String getParentPincode() {
		return parentPincode;
	}

	public void setParentPincode(String parentPincode) {
		if (!StringUtils.isEmpty(parentPincode)) {
			isParentAvailable = true;
		}
		this.parentPincode = parentPincode;
	}

	public String getParentRelationship() {
		return parentRelationship;
	}

	public void setParentRelationship(String parentRelationship) {
		if (!StringUtils.isEmpty(parentRelationship)) {
			isParentAvailable = true;
		}
		this.parentRelationship = parentRelationship;
	}

	public String getParentGender() {
		return parentGender;
	}

	public void setParentGender(String parentGender) {
		if (!StringUtils.isEmpty(parentGender)) {
			isParentAvailable = true;
		}
		this.parentGender = parentGender;
	}

	public String getCreateFatherLogin() {
		return createFatherLogin;
	}

	public void setCreateFatherLogin(String createFatherLogin) {
		if (!StringUtils.isEmpty(createFatherLogin)) {
			isFatherAvailable = true;
		}
		this.createFatherLogin = createFatherLogin;
	}

	public String getCreateMotherLogin() {
		return createMotherLogin;
	}

	public void setCreateMotherLogin(String createMotherLogin) {
		if (!StringUtils.isEmpty(createMotherLogin)) {
			isMotherAvailable = true;
		}
		this.createMotherLogin = createMotherLogin;
	}

	public String getCreateParentLogin() {
		return createParentLogin;
	}

	public void setCreateParentLogin(String createParentLogin) {
		if (!StringUtils.isEmpty(createParentLogin)) {
			isParentAvailable = true;
		}
		this.createParentLogin = createParentLogin;
	}

	public String getCreateStudentLogin() {
		return createStudentLogin;
	}

	public void setCreateStudentLogin(String createStudentLogin) {
		this.createStudentLogin = createStudentLogin;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", firstname=" + firstname
				+ ", middleName=" + middleName + ", createStudentLogin="
				+ createStudentLogin + ", createFatherLogin="
				+ createFatherLogin + ", createMotherLogin="
				+ createMotherLogin + ", createParentLogin="
				+ createParentLogin + ", fatherName=" + fatherName
				+ ", motherName=" + motherName + ", surname=" + surname
				+ ", gender=" + gender + ", dateOfBirth=" + dateOfBirth
				+ ", relationship=" + relationship + ", branch=" + branch
				+ ", branchId=" + branchId + ", feesCodeId=" + feesCodeId
				+ ", feesCodeName=" + feesCodeName + ", emailAddress="
				+ emailAddress + ", mobile=" + mobile + ", addressLine1="
				+ addressLine1 + ", area=" + area + ", city=" + city
				+ ", pincode=" + pincode + ", religion=" + religion
				+ ", caste=" + caste + ", school=" + school + ", otherSchool="
				+ otherSchool + ", otp=" + otp + ", academicYear="
				+ academicYear + ", branchName=" + branchName + ", level="
				+ level + ", standardId=" + standardId + ", standardName="
				+ standardName + ", divisionId=" + divisionId + ", courseTerm="
				+ courseTerm + ", courseName=" + courseName
				+ ", coursePattern=" + coursePattern + ", specialisation="
				+ specialisation + ", courseType=" + courseType + ", Year="
				+ Year + ", parentProfession=" + parentProfession
				+ ", parentIncome=" + parentIncome + ", fatherProfession="
				+ fatherProfession + ", fatherIncome=" + fatherIncome
				+ ", motherProfession=" + motherProfession + ", motherIncome="
				+ motherIncome + ", rollNumber=" + rollNumber
				+ ", studentRegistrationId=" + studentRegistrationId
				+ ", casteId=" + casteId + ", photo=" + photo
				+ ", instituteId=" + instituteId + ", registrationCode="
				+ registrationCode + ", active=" + active + ", isDelete="
				+ isDelete + ", divisionName=" + divisionName + ", password="
				+ password + ", fileChanegd=" + fileChanegd + ", name=" + name
				+ ", username=" + username + ", stateId=" + stateId
				+ ", districtId=" + districtId + ", talukaId=" + talukaId
				+ ", state=" + state + ", district=" + district + ", taluka="
				+ taluka + ", fatherFirstname=" + fatherFirstname
				+ ", fatherMiddleName=" + fatherMiddleName + ", fatherSurname="
				+ fatherSurname + ", fatherBirthDate=" + fatherBirthDate
				+ ", fatherEmailAddress=" + fatherEmailAddress
				+ ", fatherPrimaryMobileNumber=" + fatherPrimaryMobileNumber
				+ ", fatherSecondaryMobileNumber="
				+ fatherSecondaryMobileNumber + ", fatherAddressLine1="
				+ fatherAddressLine1 + ", fatherArea=" + fatherArea
				+ ", fatherCity=" + fatherCity + ", fatherPincode="
				+ fatherPincode + ", motherFirstname=" + motherFirstname
				+ ", motherMiddleName=" + motherMiddleName + ", motherSurname="
				+ motherSurname + ", motherBirthDate=" + motherBirthDate
				+ ", motherEmailAddress=" + motherEmailAddress
				+ ", motherPrimaryMobileNumber=" + motherPrimaryMobileNumber
				+ ", motherSecondaryMobileNumber="
				+ motherSecondaryMobileNumber + ", motherAddressLine1="
				+ motherAddressLine1 + ", motherArea=" + motherArea
				+ ", motherCity=" + motherCity + ", motherPincode="
				+ motherPincode + ", parentFirstname=" + parentFirstname
				+ ", parentMiddleName=" + parentMiddleName + ", parentSurname="
				+ parentSurname + ", parentBirthDate=" + parentBirthDate
				+ ", parentEmailAddress=" + parentEmailAddress
				+ ", parentPrimaryMobileNumber=" + parentPrimaryMobileNumber
				+ ", parentSecondaryMobileNumber="
				+ parentSecondaryMobileNumber + ", parentAddressLine1="
				+ parentAddressLine1 + ", parentArea=" + parentArea
				+ ", parentCity=" + parentCity + ", parentPincode="
				+ parentPincode + ", parentRelationship=" + parentRelationship
				+ ", parentGender=" + parentGender + ",studentUserId="
				+ studentUserId + ",FatherUserId=" + fatherUserId
				+ ",motherUserId=" + motherUserId + ",parentUserId="
				+ parentUserId + ", studentParentList=" + studentParentList
				+ ", userId=" + userId + "]";
	}

	@Override
	public int hashCode() {
		String conactenatedString = "";
		final int prime = 31;
		int result = 1;
		conactenatedString += ((studentUserId == null) ? "" : studentUserId
				.toLowerCase().replaceAll(" ", ""));
		conactenatedString += ((emailAddress == null) ? "" : emailAddress
				.toLowerCase().replaceAll(" ", ""));
		conactenatedString += ((firstname == null) ? "" : firstname
				.toLowerCase().replaceAll(" ", ""));
		conactenatedString += ((surname == null) ? "" : surname.toLowerCase()
				.replaceAll(" ", ""));
		conactenatedString += ((mobile == null) ? "" : mobile.toLowerCase()
				.replaceAll(" ", ""));

		conactenatedString += ((standardName == null) ? "" : standardName.toLowerCase()).replaceAll(" ", "");
		conactenatedString += ((divisionName == null) ? "" : divisionName.toLowerCase()).replaceAll(" ", "");
		conactenatedString += ((registrationCode == null) ? "" : registrationCode.toLowerCase()).replaceAll(" ", "");

		if (studentParentList != null) {
			for (StudentParent parent : studentParentList) {
				conactenatedString += ((parent.getEmail() == null) ? ""
						: parent.getEmail().toLowerCase().replaceAll(" ", ""));
				conactenatedString += ((parent.getPrimaryContact() == null) ? ""
						: parent.getPrimaryContact().toLowerCase()
								.replaceAll(" ", ""));
			}
		}
		result = prime * conactenatedString.hashCode();
		return result;
	}
}
