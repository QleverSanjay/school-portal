package com.qfix.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "report")
@XmlAccessorType(XmlAccessType.FIELD)
public class Report {

	// @XmlElementWrapper
	@XmlElement(name = "item")
	private List<VendorReport> itemList;

	public List<VendorReport> getItemList() {
		return itemList;
	}

	public void setItemList(List<VendorReport> itemList) {
		this.itemList = itemList;
	}

	@Override
	public String toString() {
		return "Report [itemList=" + itemList + "]";
	}



}
