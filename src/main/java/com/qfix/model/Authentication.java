package com.qfix.model;

public class Authentication {
	
	private String name;
	private String roleInfo;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRoleInfo() {
		return roleInfo;
	}
	public void setRoleInfo(String roleInfo) {
		this.roleInfo = roleInfo;
	}
	
	@Override 
	public String toString(){
		return "authentication::[name:"+name+", roleInfo:"+roleInfo+"]";
	}

}
