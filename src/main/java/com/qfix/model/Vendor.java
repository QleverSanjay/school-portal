package com.qfix.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Vendor {
	final static Logger logger = Logger.getLogger(Vendor.class);
	private Integer id;
	private String merchantLegalName;
	private String merchantMarketingName;
	private String bankAccountNumber;
	private String meCode;
	private String tid;
	private String lgCode;
	private String lcCode;
	private String branch;
	private String addressLine1;
	private String area;
	private String city;
	private String pinCode;
	private String primaryPhone;
	private String secondaryPhone;
	private String faxNo;
	private String email;
	private String shopOwnership;
	private String otherGroup;
	private String otherGroupAddress;
	private String proprietorName;
	private String proprietorAddressLine1;
	private String proprietorAddressArea;
	private String proprietorAddressCity;
	private String proprietorAddressPincode;
	private String proprietorPrimaryPhone;
	private String proprietorSecondaryPhone;
	private String proprietorFaxNo;
	private String ownership;
	private String ownershipOther;
	private Date businessStartDate;
	private String businessStartDateStr;
	private String merchantPan;
	private String creditCardAccepted;
	private String creditCardAcceptedOther;
	private String annualTurnOver;
	private String annualTotalOnCreditCard;
	private String averagePerTransaction;
	private String creditCardBank;
	private String creditCardSinceYear;
	private String ebSavingAccount;
	private String ebCurrentAccount;
	private String ebTermDeposit;
	private String ebDemat;
	private String ebAutoLoan;
	private String ebPersonalLoan;
	private String ebTwoWheeler;
	private String ebCreditCard;
	private String ebLoanAgainstShares;
	private String ebBusinessBanking;
	private String ebInsurance;
	private String ebMutualFund;
	private String cardAcceptTypeVisa;
	private String cardAcceptTypeMaster;
	private String cardAcceptTypeDinner;
	private String physicalMpr;
	private String emailMpr;
	private String emailMprText;
	private String mprFrequency;
	private String sellingCategory;
	private Integer userId;
	private String active;
	private String isDelete;
	private List<Integer> instituteIdList;
	private List<Master> institutes;
	private boolean isVendorExist;

	public boolean isVendorExist() {
		return isVendorExist;
	}

	public void setVendorExist(boolean isVendorExist) {
		this.isVendorExist = isVendorExist;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMerchantLegalName() {
		return merchantLegalName;
	}

	public void setMerchantLegalName(String merchantLegalName) {
		this.merchantLegalName = merchantLegalName;
	}

	public String getMerchantMarketingName() {
		return merchantMarketingName;
	}

	public void setMerchantMarketingName(String merchantMarketingName) {
		this.merchantMarketingName = merchantMarketingName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getMeCode() {
		return meCode;
	}

	public void setMeCode(String meCode) {
		this.meCode = meCode;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getLgCode() {
		return lgCode;
	}

	public void setLgCode(String lgCode) {
		this.lgCode = lgCode;
	}

	public String getLcCode() {
		return lcCode;
	}

	public void setLcCode(String lcCode) {
		this.lcCode = lcCode;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getSecondaryPhone() {
		return secondaryPhone;
	}

	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getShopOwnership() {
		return shopOwnership;
	}

	public void setShopOwnership(String shopOwnership) {
		this.shopOwnership = shopOwnership;
	}

	public String getOtherGroup() {
		return otherGroup;
	}

	public void setOtherGroup(String otherGroup) {
		this.otherGroup = otherGroup;
	}

	public String getOtherGroupAddress() {
		return otherGroupAddress;
	}

	public void setOtherGroupAddress(String otherGroupAddress) {
		this.otherGroupAddress = otherGroupAddress;
	}

	public String getProprietorName() {
		return proprietorName;
	}

	public void setProprietorName(String proprietorName) {
		this.proprietorName = proprietorName;
	}

	public String getProprietorAddressLine1() {
		return proprietorAddressLine1;
	}

	public void setProprietorAddressLine1(String proprietorAddressLine1) {
		this.proprietorAddressLine1 = proprietorAddressLine1;
	}

	public String getProprietorAddressArea() {
		return proprietorAddressArea;
	}

	public void setProprietorAddressArea(String proprietorAddressArea) {
		this.proprietorAddressArea = proprietorAddressArea;
	}

	public String getProprietorAddressCity() {
		return proprietorAddressCity;
	}

	public void setProprietorAddressCity(String proprietorAddressCity) {
		this.proprietorAddressCity = proprietorAddressCity;
	}

	public String getProprietorAddressPincode() {
		return proprietorAddressPincode;
	}

	public void setProprietorAddressPincode(String proprietorAddressPincode) {
		this.proprietorAddressPincode = proprietorAddressPincode;
	}

	public String getProprietorPrimaryPhone() {
		return proprietorPrimaryPhone;
	}

	public void setProprietorPrimaryPhone(String proprietorPrimaryPhone) {
		this.proprietorPrimaryPhone = proprietorPrimaryPhone;
	}

	public String getProprietorSecondaryPhone() {
		return proprietorSecondaryPhone;
	}

	public void setProprietorSecondaryPhone(String proprietorSecondaryPhone) {
		this.proprietorSecondaryPhone = proprietorSecondaryPhone;
	}

	public String getProprietorFaxNo() {
		return proprietorFaxNo;
	}

	public void setProprietorFaxNo(String proprietorFaxNo) {
		this.proprietorFaxNo = proprietorFaxNo;
	}

	public String getOwnership() {
		return ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getOwnershipOther() {
		return ownershipOther;
	}

	public void setOwnershipOther(String ownershipOther) {
		this.ownershipOther = ownershipOther;
	}

	public Date getBusinessStartDate() {
		return businessStartDate;
	}

	public void setBusinessStartDate(Date businessStartDate) {
		this.businessStartDate = businessStartDate;
	}

	public String getBusinessStartDateStr() {
		return businessStartDateStr;
	}

	public void setBusinessStartDateStr(String businessStartDateStr) {
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		try {
			setBusinessStartDate(df.parse(businessStartDateStr));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
		this.businessStartDateStr = businessStartDateStr;
	}

	public String getMerchantPan() {
		return merchantPan;
	}

	public void setMerchantPan(String merchantPan) {
		this.merchantPan = merchantPan;
	}

	public String getCreditCardAccepted() {
		return creditCardAccepted;
	}

	public void setCreditCardAccepted(String creditCardAccepted) {
		this.creditCardAccepted = creditCardAccepted;
	}

	public String getCreditCardAcceptedOther() {
		return creditCardAcceptedOther;
	}

	public void setCreditCardAcceptedOther(String creditCardAcceptedOther) {
		this.creditCardAcceptedOther = creditCardAcceptedOther;
	}

	public String getAnnualTurnOver() {
		return annualTurnOver;
	}

	public void setAnnualTurnOver(String annualTurnOver) {
		this.annualTurnOver = annualTurnOver;
	}

	public String getAnnualTotalOnCreditCard() {
		return annualTotalOnCreditCard;
	}

	public void setAnnualTotalOnCreditCard(String annualTotalOnCreditCard) {
		this.annualTotalOnCreditCard = annualTotalOnCreditCard;
	}

	public String getAveragePerTransaction() {
		return averagePerTransaction;
	}

	public void setAveragePerTransaction(String averagePerTransaction) {
		this.averagePerTransaction = averagePerTransaction;
	}

	public String getCreditCardBank() {
		return creditCardBank;
	}

	public void setCreditCardBank(String creditCardBank) {
		this.creditCardBank = creditCardBank;
	}

	public String getCreditCardSinceYear() {
		return creditCardSinceYear;
	}

	public void setCreditCardSinceYear(String creditCardSinceYear) {
		this.creditCardSinceYear = creditCardSinceYear;
	}

	public String getEbSavingAccount() {
		return ebSavingAccount;
	}

	public void setEbSavingAccount(String ebSavingAccount) {
		this.ebSavingAccount = ebSavingAccount;
	}

	public String getEbCurrentAccount() {
		return ebCurrentAccount;
	}

	public void setEbCurrentAccount(String ebCurrentAccount) {
		this.ebCurrentAccount = ebCurrentAccount;
	}

	public String getEbTermDeposit() {
		return ebTermDeposit;
	}

	public void setEbTermDeposit(String ebTermDeposit) {
		this.ebTermDeposit = ebTermDeposit;
	}

	public String getEbDemat() {
		return ebDemat;
	}

	public void setEbDemat(String ebDemat) {
		this.ebDemat = ebDemat;
	}

	public String getEbAutoLoan() {
		return ebAutoLoan;
	}

	public void setEbAutoLoan(String ebAutoLoan) {
		this.ebAutoLoan = ebAutoLoan;
	}

	public String getEbPersonalLoan() {
		return ebPersonalLoan;
	}

	public void setEbPersonalLoan(String ebPersonalLoan) {
		this.ebPersonalLoan = ebPersonalLoan;
	}

	public String getEbTwoWheeler() {
		return ebTwoWheeler;
	}

	public void setEbTwoWheeler(String ebTwoWheeler) {
		this.ebTwoWheeler = ebTwoWheeler;
	}

	public String getEbCreditCard() {
		return ebCreditCard;
	}

	public void setEbCreditCard(String ebCreditCard) {
		this.ebCreditCard = ebCreditCard;
	}

	public String getEbLoanAgainstShares() {
		return ebLoanAgainstShares;
	}

	public void setEbLoanAgainstShares(String ebLoanAgainstShares) {
		this.ebLoanAgainstShares = ebLoanAgainstShares;
	}

	public String getEbBusinessBanking() {
		return ebBusinessBanking;
	}

	public void setEbBusinessBanking(String ebBusinessBanking) {
		this.ebBusinessBanking = ebBusinessBanking;
	}

	public String getEbInsurance() {
		return ebInsurance;
	}

	public void setEbInsurance(String ebInsurance) {
		this.ebInsurance = ebInsurance;
	}

	public String getEbMutualFund() {
		return ebMutualFund;
	}

	public void setEbMutualFund(String ebMutualFund) {
		this.ebMutualFund = ebMutualFund;
	}

	public String getCardAcceptTypeVisa() {
		return cardAcceptTypeVisa;
	}

	public void setCardAcceptTypeVisa(String cardAcceptTypeVisa) {
		this.cardAcceptTypeVisa = cardAcceptTypeVisa;
	}

	public String getCardAcceptTypeMaster() {
		return cardAcceptTypeMaster;
	}

	public void setCardAcceptTypeMaster(String cardAcceptTypeMaster) {
		this.cardAcceptTypeMaster = cardAcceptTypeMaster;
	}

	public String getCardAcceptTypeDinner() {
		return cardAcceptTypeDinner;
	}

	public void setCardAcceptTypeDinner(String cardAcceptTypeDinner) {
		this.cardAcceptTypeDinner = cardAcceptTypeDinner;
	}

	public String getPhysicalMpr() {
		return physicalMpr;
	}

	public void setPhysicalMpr(String physicalMpr) {
		this.physicalMpr = physicalMpr;
	}

	public String getEmailMpr() {
		return emailMpr;
	}

	public void setEmailMpr(String emailMpr) {
		this.emailMpr = emailMpr;
	}

	public String getEmailMprText() {
		return emailMprText;
	}

	public void setEmailMprText(String emailMprText) {
		this.emailMprText = emailMprText;
	}

	public String getMprFrequency() {
		return mprFrequency;
	}

	public void setMprFrequency(String mprFrequency) {
		this.mprFrequency = mprFrequency;
	}

	public String getSellingCategory() {
		return sellingCategory;
	}

	public void setSellingCategory(String sellingCategory) {
		this.sellingCategory = sellingCategory;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public List<Integer> getInstituteIdList() {
		return instituteIdList;
	}

	public void setInstituteIdList(List<Integer> instituteIdList) {
		this.instituteIdList = instituteIdList;
	}

	public List<Master> getInstitutes() {
		return institutes;
	}

	public void setInstitutes(List<Master> institutes) {
		this.institutes = institutes;
	}

	@Override
	public String toString() {
		return "Vendor [id=" + id + ", merchantLegalName=" + merchantLegalName
				+ ", merchantMarketingName=" + merchantMarketingName
				+ ", bankAccountNumber=" + bankAccountNumber + ", meCode="
				+ meCode + ", tid=" + tid + ", lgCode=" + lgCode + ", lcCode="
				+ lcCode + ", branch=" + branch + ", addressLine1="
				+ addressLine1 + ", area=" + area + ", city=" + city
				+ ", pinCode=" + pinCode + ", primaryPhone=" + primaryPhone
				+ ", secondaryPhone=" + secondaryPhone + ", faxNo=" + faxNo
				+ ", email=" + email + ", shopOwnership=" + shopOwnership
				+ ", otherGroup=" + otherGroup + ", otherGroupAddress="
				+ otherGroupAddress + ", proprietorName=" + proprietorName
				+ ", proprietorAddressLine1=" + proprietorAddressLine1
				+ ", proprietorAddressArea=" + proprietorAddressArea
				+ ", proprietorAddressCity=" + proprietorAddressCity
				+ ", proprietorAddressPincode=" + proprietorAddressPincode
				+ ", proprietorPrimaryPhone=" + proprietorPrimaryPhone
				+ ", proprietorSecondaryPhone=" + proprietorSecondaryPhone
				+ ", proprietorFaxNo=" + proprietorFaxNo + ", ownership="
				+ ownership + ", ownershipOther=" + ownershipOther
				+ ", businessStartDate=" + businessStartDate
				+ ", businessStartDateStr=" + businessStartDateStr
				+ ", merchantPan=" + merchantPan + ", creditCardAccepted="
				+ creditCardAccepted + ", creditCardAcceptedOther="
				+ creditCardAcceptedOther + ", annualTurnOver="
				+ annualTurnOver + ", annualTotalOnCreditCard="
				+ annualTotalOnCreditCard + ", averagePerTransaction="
				+ averagePerTransaction + ", creditCardBank=" + creditCardBank
				+ ", creditCardSinceYear=" + creditCardSinceYear
				+ ", ebSavingAccount=" + ebSavingAccount
				+ ", ebCurrentAccount=" + ebCurrentAccount + ", ebTermDeposit="
				+ ebTermDeposit + ", ebDemat=" + ebDemat + ", ebAutoLoan="
				+ ebAutoLoan + ", ebPersonalLoan=" + ebPersonalLoan
				+ ", ebTwoWheeler=" + ebTwoWheeler + ", ebCreditCard="
				+ ebCreditCard + ", ebLoanAgainstShares=" + ebLoanAgainstShares
				+ ", ebBusinessBanking=" + ebBusinessBanking + ", ebInsurance="
				+ ebInsurance + ", ebMutualFund=" + ebMutualFund
				+ ", cardAcceptTypeVisa=" + cardAcceptTypeVisa
				+ ", cardAcceptTypeMaster=" + cardAcceptTypeMaster
				+ ", cardAcceptTypeDinner=" + cardAcceptTypeDinner
				+ ", physicalMpr=" + physicalMpr + ", emailMpr=" + emailMpr
				+ ", emailMprText=" + emailMprText + ", mprFrequency="
				+ mprFrequency + ", sellingCategory=" + sellingCategory
				+ ", userId=" + userId + ", active=" + active + ", isDelete="
				+ isDelete + ", instituteIdList=" + instituteIdList
				+ ", institutes=" + institutes + "]";
	}
}
