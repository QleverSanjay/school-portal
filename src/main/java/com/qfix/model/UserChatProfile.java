package com.qfix.model;

public class UserChatProfile {
	
	String login;
	
	String password;
	
	long chatProfileId;
	
	long applicationId;
	
	String token;
	

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getChatProfileId() {
		return chatProfileId;
	}

	public void setChatProfileId(long chatProfileId) {
		this.chatProfileId = chatProfileId;
	}

	public long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	@Override
	public String toString() {
		return "UserChatProfile [login=" + login + ", chatProfileId=" + chatProfileId + ", password="
				+ password + ", applicationId=" + applicationId + ", token=" + token + "]";
	}
	

}
