package com.qfix.model;

public class ReportModel {

	private String institute;
	private Integer instituteId;
	private String branch;
	private Integer branchId;
	private String standard;
	private Integer standardId;
	private String division;
	private Integer divisionId;
	private String semester;
	private Integer academicYearId;
	private String academicYearStr;
	private String rollNumber;
	private Integer resultTypeId;
	
	

	public Integer getResultTypeId() {
		return resultTypeId;
	}

	public void setResultTypeId(Integer resultTypeId) {
		this.resultTypeId = resultTypeId;
	}

	public String getInstitute() {
		return institute;
	}

	public void setInstitute(String institute) {
		this.institute = institute;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}
	
	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearStr() {
		return academicYearStr;
	}

	public void setAcademicYearStr(String academicYearStr) {
		this.academicYearStr = academicYearStr;
	}	

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	@Override
	public String toString() {
		return "ReportModel [institute=" + institute + ", instituteId="
				+ instituteId + ", branch=" + branch + ", branchId=" + branchId
				+ ", standard=" + standard + ", standardId=" + standardId
				+ ", division=" + division + ", divisionId=" + divisionId
				+ ", semester=" + semester + ", academicYearId="
				+ academicYearId + ", academicYearStr=" + academicYearStr + "]";
	}
}
