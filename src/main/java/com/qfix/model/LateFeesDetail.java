package com.qfix.model;

import java.util.Map;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LateFeesDetail {
	private Integer instituteId;

	@NotNull
	private Integer branchId;
	private Integer id;

	@NotNull
	private String description;

	@NotNull
	private String type;
	private String text;
	private String frequency;
	private String byDays;
	private String byDate;
	private String combinationRule;
	private String isDelete;

	// @NotNull
	private Double amount = 0.0;
	private Double baseAmount = 0.0;
	private Double maximumAmount = 0.0;
	private Map<String, Boolean> weekDays;

	@Override
	public String toString() {
		return "LateFees [instituteId=" + instituteId + ", branchId="
				+ branchId + ", id=" + id + ", description=" + description
				+ ", type=" + type + ", frequency=" + frequency + ", byDays="
				+ byDays + ", amount=" + amount + ", maximumAmount="
				+ maximumAmount + "]";
	}

}
