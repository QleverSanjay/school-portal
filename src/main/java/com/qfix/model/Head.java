package com.qfix.model;

import lombok.Data;

@Data
public class Head {

	private Integer id;
	private String text;
	private String name;
	private String active;
	private Integer branchId;
	private String branchName;
	private Integer instituteId;
	private String instituteName;
	private String isDelete;
	private Integer academicYearId;
	private String academicYearStr;

	@Override
	public String toString() {
		return "Head [id=" + id + ", name=" + name + ", active=" + active
				+ ", branchId=" + branchId + ", branchName=" + branchName
				+ ", instituteId=" + instituteId + ", instituteName="
				+ instituteName + ", isDelete=" + isDelete
				+ ", academicYearId=" + academicYearId + ", academicYearStr="
				+ academicYearStr + "]";
	}
}
