package com.qfix.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Group {

	private Integer id;
	private String name;
	private String description;
	private Integer branchId;
	private Integer instituteId;
	private String chatId;
	private String status;
	private String isDelete;
	private List<Integer> individualIdList;
	private List<Individual> individuals;
	private List<StandardDivision> standardDivision;
	private List<Tag> tags;
	private List<Integer> tagIdList;
	private Integer createdBy;
	private String isPublicGroup;
	private Integer academicYearId;
	private String academicYearStr;
	private Integer ownerRoleId;

	public Integer getOwnerRoleId() {
		return ownerRoleId;
	}

	public void setOwnerRoleId(Integer ownerRoleId) {
		this.ownerRoleId = ownerRoleId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public List<Integer> getIndividualIdList() {
		return individualIdList;
	}

	public void setIndividualIdList(List<Integer> individualIdList) {
		this.individualIdList = individualIdList;
	}

	public List<Individual> getIndividuals() {
		return individuals;
	}

	public void setIndividuals(List<Individual> individuals) {
		this.individuals = individuals;
	}

	public List<StandardDivision> getStandardDivision() {
		return standardDivision;
	}

	public void setStandardDivision(List<StandardDivision> standardDivision) {
		this.standardDivision = standardDivision;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Integer> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Integer> tagIdList) {
		this.tagIdList = tagIdList;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getIsPublicGroup() {
		return isPublicGroup;
	}

	public void setIsPublicGroup(String isPublicGroup) {
		this.isPublicGroup = isPublicGroup;
	}

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearStr() {
		return academicYearStr;
	}

	public void setAcademicYearStr(String academicYearStr) {
		this.academicYearStr = academicYearStr;
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + ", description="
				+ description + ", branchId=" + branchId + ", instituteId="
				+ instituteId + ", chatId=" + chatId + ", status=" + status
				+ ", isDelete=" + isDelete + ", individualIdList="
				+ individualIdList + ", individuals=" + individuals
				+ ", standardDivision=" + standardDivision + ", tags=" + tags
				+ ", tagIdList=" + tagIdList + ", createdBy=" + createdBy
				+ ", isPublicGroup=" + isPublicGroup + ", academicYearId="
				+ academicYearId + ", academicYearStr=" + academicYearStr + "]";
	}
}
