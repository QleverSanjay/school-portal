package com.qfix.model;

import lombok.Data;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Master {

	private Integer id;
	private String name;
	private Integer code;
	private String active;
	private Integer branchId;
	private String branchName;

	@Override
	public String toString() {
		return "Master [id=" + id + ", name=" + name + ", branchId=" + branchId
				+ ", branchName=" + branchName + "]";
	}
}
