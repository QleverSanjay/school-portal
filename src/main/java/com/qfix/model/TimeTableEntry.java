package com.qfix.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * {
	"D1": [{
		"title": "Open Sesame",
		"startTime": "12:00",
		"endTime": "14:00"
	}, {
		"title": "Open Sesame",
		"startTime": "12:00",
		"endTime": "14:00"

	}],

	"D2": [{
		"title": "Open Sesame",
		"startTime": "12:00:00",
		"endTime": "14:00:00"
	}, {
		"title": "Open Sesame",
		"startTime": "12:00:00",
		"endTime": "14:00:00"

	}]
}

 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeTableEntry {

	@JsonProperty(value = "title")
	@NotNull
	String title;

	@JsonProperty(value = "start_time")
	@NotNull
	@Size(max = 5)
	String starttime;

	@JsonProperty(value = "end_time")
	@NotNull
	@Size(max = 5)
	String endtime;

	@NotNull
	@Size(max = 10)
	Date date;

	public TimeTableEntry(String title, String starttime, String endtime) {
		super();
		this.title = title;
		this.starttime = starttime;
		this.endtime = endtime;
	}

	public TimeTableEntry(String title, String starttime, String endtime, Date date) {
		super();
		this.title = title;
		this.starttime = starttime;
		this.endtime = endtime;
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	@JsonIgnoreProperties
	public Date getDate() {
		return date;
	}

	@JsonProperty(value = "date")
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "TimeTableEntry [title=" + title + ", starttime=" + starttime + ", endtime=" + endtime + ", date=" + date + "]";
	}

}
