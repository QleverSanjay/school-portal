package com.qfix.model;

public class Response {
	
	String result; 
	String message;
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Response [RESULT=" + result + ", MESSAGE=" + message + "]";
	}

}
