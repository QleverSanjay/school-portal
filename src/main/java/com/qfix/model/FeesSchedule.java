package com.qfix.model;

import java.sql.Date;

import org.springframework.stereotype.Component;

@Component
public class FeesSchedule {

	private long id;
	private Integer feesId;
	private Integer userId;
	private double baseAmount;
	private double totalAmount;
	private java.sql.Date dueDate;
	private String status;
	private java.sql.Date reminderStartDate;
	private String feeForDuration;
	private java.sql.Date feesStartDate;

	public FeesSchedule(long id, Integer feesId, Integer userId,
			double baseAmount, double totalAmount, Date dueDate, String status,
			Date reminderStartDate, String feeForDuration, Date feesStartDate) {
		super();
		this.id = id;
		this.feesId = feesId;
		this.userId = userId;
		this.baseAmount = baseAmount;
		this.totalAmount = totalAmount;
		this.dueDate = dueDate;
		this.status = status;
		this.reminderStartDate = reminderStartDate;
		this.feeForDuration = feeForDuration;
		this.feesStartDate = feesStartDate;
	}

	public FeesSchedule() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getFeesId() {
		return feesId;
	}

	public void setFeesId(Integer feesId) {
		this.feesId = feesId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public java.sql.Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(java.sql.Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public java.sql.Date getReminderStartDate() {
		return reminderStartDate;
	}

	public void setReminderStartDate(java.sql.Date reminderStartDate) {
		this.reminderStartDate = reminderStartDate;
	}

	public String getFeeForDuration() {
		return feeForDuration;
	}

	public void setFeeForDuration(String feeForDuration) {
		this.feeForDuration = feeForDuration;
	}

	public java.sql.Date getFeesStartDate() {
		return feesStartDate;
	}

	public void setFeesStartDate(java.sql.Date feesStartDate) {
		this.feesStartDate = feesStartDate;
	}

	@Override
	public String toString() {
		return "FeesSchedule [id=" + id + ", feesId=" + feesId + ", userId="
				+ userId + ", baseAmount=" + baseAmount + ", totalAmount="
				+ totalAmount + ", dueDate=" + dueDate + ", status=" + status
				+ ", reminderStartDate=" + reminderStartDate
				+ ", feeForDuration=" + feeForDuration + ", feesStartDate="
				+ feesStartDate + "]";
	}

}
