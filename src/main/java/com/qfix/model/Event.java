package com.qfix.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class Event {

	final static Logger logger = Logger.getLogger(Event.class);

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9]{2,45}$", message = "Must be valid event name.")
	private String title;
	private Integer id;

	@Size(max = 255, message = "Address is too long.")
	private String description;

	@NotNull
	@Pattern(regexp = "^[A-Z a-z 0-9 , . () - @]{2,100}$", message = "Must be valid venue")
	private String venue;

	// @Pattern(regexp =
	// "^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])|(0[1-9]|[12]\\d|3[01])-(0?[1-9]|1[012])-(\\d{4})$",
	// message = "Must be valid date eg (15-10-2015)")
	private String startDate;

	private Date startDateD;

	@Pattern(regexp = "^(?:(?:([01]?\\d|2[0-3]):)?([0-5]?\\d):)?([0-5]?\\d)$", message = "Must be valid time eg (10:10:10)")
	private String startTime;

	@Pattern(regexp = "^(?:(?:([01]?\\d|2[0-3]):)?([0-5]?\\d):)?([0-5]?\\d)$", message = "Must be valid time eg (10:10:10)")
	private String endTime;

	private String allDayEvent;
	private String reminderSet;
	private Integer branchId;
	private String imageUrl;

	// @Pattern(regexp =
	// "^$|^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])|(0[1-9]|[12]\\d|3[01])-(0?[1-9]|1[012])-(\\d{4})$",
	// message = "Must be valid date eg (15-10-2015)")
	private String endDate;

	private Date endDateD;

	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry for reccuring.(eg : Y or N)")
	private String reccuring;

	// @Pattern(regexp = "\\d{1,9}")
	private Integer reminderBefore;

	// @Pattern(regexp = "^[0-9]{1,10}$", message =
	// "Must be valid entry for reminder before.")
	private List<Integer> groupIdList;

	private List<Integer> individualIdList;

	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry in forParent.(eg : Y or N)")
	private String forParent;

	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry in forStudent.(eg : Y or N)")
	private String forStudent;

	private Integer courseId;

	// @Pattern(regexp = "^[A-Z a-z 0-9]{1,10}$", message =
	// "Must be valid standard.")
	private String standardName;

	// @Pattern(regexp = "^[A-Z a-z 0-9]{1,10}$", message =
	// "Must be valid division.")
	private String divisionName;

	private Integer standardId;
	private Integer divisionId;
	private String branchName;

	// @Pattern(regexp = "^[A-Z a-z]{2,45}$", message = "Must be valid course.")
	private String courseName;
	private String summary;

	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry in forTeacher.(eg : Y or N)")
	private String forTeacher;
	private List<Individual> individuals;

	@Pattern(regexp = "^[A-Z a-z]{5,45}$", message = "Must be valid frequency.")
	private String frequency;

	private Map<String, Boolean> eventWeekDays;

	@Pattern(regexp = "^$|^(1|2|3|4|5|6|7)(,(1|2|3|4|5|6|7){1})*$", message = "Please specify days as (1,2,3,4,5,6,7) where 1 = Monday, 2 = Tuesday ..,7 = Sunday")
	private String byDay;

	private String byDate;
	private List<Master> groupList;
	private Integer instituteId;
	private Date createdDate;
	private Date updatedDate;
	private boolean isScheduleUpdated;
	private String status;
	private boolean fileChanegd;

	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry in forVendor.(eg : Y or N)")
	private String forVendor;
	private Integer academicYearId;
	private String academicYearStr;
	private Integer sentBy;
	private String readStatus;
	private String sentByName;
	private Integer sentByRoleId;

	public Integer getSentByRoleId() {
		return sentByRoleId;
	}

	public void setSentByRoleId(Integer sentByRoleId) {
		this.sentByRoleId = sentByRoleId;
	}

	public String getSentByName() {
		return sentByName;
	}

	public void setSentByName(String sentByName) {
		this.sentByName = sentByName;
	}

	public String getForVendor() {
		return forVendor;
	}

	public String getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(String readStatus) {
		this.readStatus = readStatus;
	}

	public void setForVendor(String forVendor) {
		this.forVendor = forVendor;
	}

	public boolean isFileChanegd() {
		return fileChanegd;
	}

	public void setFileChanegd(boolean fileChanegd) {
		this.fileChanegd = fileChanegd;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean isScheduleUpdated() {
		return isScheduleUpdated;
	}

	public void setScheduleUpdated(boolean isScheduleUpdated) {
		this.isScheduleUpdated = isScheduleUpdated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public List<Master> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<Master> groupList) {
		this.groupList = groupList;
	}

	public String getByDate() {
		return byDate;
	}

	public void setByDate(String byDate) {
		this.byDate = byDate;
	}

	public String getByDay() {
		return byDay;
	}

	public void setByDay(String byDay) {
		this.byDay = byDay;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Map<String, Boolean> getEventWeekDays() {
		return eventWeekDays;
	}

	public void setEventWeekDays(Map<String, Boolean> eventWeekDays) {
		this.eventWeekDays = eventWeekDays;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<Individual> getIndividuals() {
		return individuals;
	}

	public void setIndividuals(List<Individual> individuals) {
		this.individuals = individuals;
	}

	public String getForTeacher() {
		return forTeacher;
	}

	public void setForTeacher(String forTeacher) {
		this.forTeacher = forTeacher;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getForParent() {
		return forParent;
	}

	public void setForParent(String forParent) {
		this.forParent = forParent;
	}

	public String getForStudent() {
		return forStudent;
	}

	public void setForStudent(String forStudent) {
		this.forStudent = forStudent;
	}

	public List<Integer> getGroupIdList() {
		return groupIdList;
	}

	public void setGroupIdList(List<Integer> groupIdList) {
		this.groupIdList = groupIdList;
	}

	public List<Integer> getIndividualIdList() {
		return individualIdList;
	}

	public void setIndividualIdList(List<Integer> individualIdList) {
		this.individualIdList = individualIdList;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getAllDayEvent() {
		return allDayEvent;
	}

	public void setAllDayEvent(String allDayEvent) {
		this.allDayEvent = allDayEvent;
	}

	public String getReminderSet() {
		return reminderSet;
	}

	public void setReminderSet(String reminderSet) {
		this.reminderSet = reminderSet;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = StringUtils.isEmpty(endDate) ? null : endDate;
	}

	public String getReccuring() {
		return reccuring;
	}

	public void setReccuring(String reccuring) {
		this.reccuring = reccuring;
	}

	public Integer getReminderBefore() {
		return reminderBefore;
	}

	public void setReminderBefore(Integer reminderBefore) {
		this.reminderBefore = reminderBefore;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standard) {
		this.standardName = standard;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String division) {
		this.divisionName = division;
	}

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearStr() {
		return academicYearStr;
	}

	public void setAcademicYearStr(String academicYearStr) {
		this.academicYearStr = academicYearStr;
	}

	public Date getStartDateD() {
		return startDateD;
	}

	public void setStartDateD(Date startDateD) {
		this.startDateD = startDateD;
	}

	public Date getEndDateD() {
		return endDateD;
	}

	public void setEndDateD(Date endDateD) {
		this.endDateD = endDateD;
	}

	public Integer getSentBy() {
		return sentBy;
	}

	public void setSentBy(Integer sentBy) {
		this.sentBy = sentBy;
	}

	@Override
	public String toString() {
		return "Event [title=" + title + ", id=" + id + ", description="
				+ description + ", venue=" + venue + ", startDate=" + startDate
				+ ", startDateD=" + startDateD + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", allDayEvent=" + allDayEvent
				+ ", reminderSet=" + reminderSet + ", branchId=" + branchId
				+ ", imageUrl=" + imageUrl + ", endDate=" + endDate
				+ ", endDateD=" + endDateD + ", reccuring=" + reccuring
				+ ", reminderBefore=" + reminderBefore + ", groupIdList="
				+ groupIdList + ", individualIdList=" + individualIdList
				+ ", forParent=" + forParent + ", forStudent=" + forStudent
				+ ", courseId=" + courseId + ", standardName=" + standardName
				+ ", divisionName=" + divisionName + ", standardId="
				+ standardId + ", divisionId=" + divisionId + ", branchName="
				+ branchName + ", courseName=" + courseName + ", summary="
				+ summary + ", forTeacher=" + forTeacher + ", individuals="
				+ individuals + ", frequency=" + frequency + ", eventWeekDays="
				+ eventWeekDays + ", byDay=" + byDay + ", byDate=" + byDate
				+ ", groupList=" + groupList + ", instituteId=" + instituteId
				+ ", createdDate=" + createdDate + ", updatedDate="
				+ updatedDate + ", isScheduleUpdated=" + isScheduleUpdated
				+ ", status=" + status + ", fileChanegd=" + fileChanegd
				+ ", forVendor=" + forVendor + ", academicYearId="
				+ academicYearId + ", academicYearStr=" + academicYearStr
				+ ", sentBy=" + sentBy + "]";
	}
}
