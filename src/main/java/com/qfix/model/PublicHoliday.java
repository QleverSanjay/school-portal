package com.qfix.model;

import lombok.Data;

@Data
public class PublicHoliday {
	private Integer id;
	private String title;
	private String description;
	private String toDate;
	private String fromDate;
	private String isPublicHoliday;
}
