package com.qfix.model;

import org.springframework.stereotype.Component;

@Component
public class SMSSetting {

	private Integer id;
	private String vendorUrl;
	private String username;
	private String password;
	private String from;
	private String active;

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVendorUrl() {
		return vendorUrl;
	}

	public void setVendorUrl(String vendorUrl) {
		this.vendorUrl = vendorUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@Override
	public String toString() {
		return "SmsSetting [id=" + id + ", vendorUrl=" + vendorUrl
				+ ", username=" + username + ", password=" + password
				+ ", from=" + from + "]";
	}

	public static void main(String a[]){
		
	}
}
