package com.qfix.model;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Notice {

	private Integer id;
	private String title;
	private String description;
	private String imageUrl;
	private String imageName;
	private String category;
	private List<Integer> groupIdList;
	private List<Integer> individualIdList;
	private List<Individual> individuals;
	private String priority;
	private Date date;
	private Integer branchId;
	private Integer instituteId;
	private String instituteName;
	private String branchName;
	private List<Master> groupList;
	private Integer sentBy;
	private String sentByName;
	private boolean fileChanegd;
	private Integer academicYearId;
	private String academicYearStr;
	private String readStatus;

	public String getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(String readStatus) {
		this.readStatus = readStatus;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public boolean isFileChanegd() {
		return fileChanegd;
	}

	public void setFileChanegd(boolean fileChanegd) {
		this.fileChanegd = fileChanegd;
	}

	public List<Master> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<Master> groupList) {
		this.groupList = groupList;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Integer> getGroupIdList() {
		return groupIdList;
	}

	public void setGroupIdList(List<Integer> groupIdList) {
		this.groupIdList = groupIdList;
	}

	public List<Integer> getIndividualIdList() {
		return individualIdList;
	}

	public void setIndividualIdList(List<Integer> individualIdList) {
		this.individualIdList = individualIdList;
	}

	public List<Individual> getIndividuals() {
		return individuals;
	}

	public void setIndividuals(List<Individual> individuals) {
		this.individuals = individuals;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Integer getSentBy() {
		return sentBy;
	}

	public void setSentBy(Integer sentBy) {
		this.sentBy = sentBy;
	}

	public String getSentByName() {
		return sentByName;
	}

	public void setSentByName(String sentByName) {
		this.sentByName = sentByName;
	}

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearStr() {
		return academicYearStr;
	}

	public void setAcademicYearStr(String academicYearStr) {
		this.academicYearStr = academicYearStr;
	}

	@Override
	public String toString() {
		return "Notice [id=" + id + ", title=" + title + ", description="
				+ description + ", imageUrl=" + imageUrl + ", imageName="
				+ imageName + ", category=" + category + ", groupIdList="
				+ groupIdList + ", individualIdList=" + individualIdList
				+ ", individuals=" + individuals + ", priority=" + priority
				+ ", date=" + date + ", branchId=" + branchId
				+ ", instituteId=" + instituteId + ", instituteName="
				+ instituteName + ", branchName=" + branchName + ", groupList="
				+ groupList + ", sentBy=" + sentBy + ", sentByName="
				+ sentByName + ", fileChanegd=" + fileChanegd
				+ ", academicYearId=" + academicYearId + ", academicYearStr="
				+ academicYearStr + "]";
	}
}
