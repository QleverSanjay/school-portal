package com.qfix.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Forum {
	private Integer id;
	private String title;
	private String description;
	private Integer groupId;
	private String groupName;
	private List<Master> groupList;
	private Integer forumId;
	private String canOneWay;
	private String active;
	private Integer branchId;
	private Integer instituteId;
	private String isDelete;
	private Integer forumGroupId;
	private Integer createdBy;
	private Integer academicYearId;
	private String academicYearStr;

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getForumGroupId() {
		return forumGroupId;
	}

	public void setForumGroupId(Integer forumGroupId) {
		this.forumGroupId = forumGroupId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Master> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<Master> groupList) {
		this.groupList = groupList;
	}

	public Integer getForumId() {
		return forumId;
	}

	public void setForumId(Integer forumId) {
		this.forumId = forumId;
	}

	public String getCanOneWay() {
		return canOneWay;
	}

	public void setCanOneWay(String canOneWay) {
		this.canOneWay = canOneWay;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}
	
	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearStr() {
		return academicYearStr;
	}

	public void setAcademicYearStr(String academicYearStr) {
		this.academicYearStr = academicYearStr;
	}

	@Override
	public String toString() {
		return "Forum [id=" + id + ", title=" + title + ", description="
				+ description + ", groupId=" + groupId + ", groupName="
				+ groupName + ", groupList=" + groupList + ", forumId="
				+ forumId + ", canOneWay=" + canOneWay + ", active=" + active
				+ ", branchId=" + branchId + ", instituteId=" + instituteId
				+ ", academicYearId=" + academicYearId + ", academicYearStr=" + academicYearStr
				+ ", isDelete=" + isDelete + ", forumGroupId=" + forumGroupId
				+ ", getForumGroupId()=" + getForumGroupId() + ", getId()="
				+ getId() + ", getTitle()=" + getTitle()
				+ ", getDescription()=" + getDescription() + ", getGroupId()="
				+ getGroupId() + ", getGroupName()=" + getGroupName()
				+ ", getGroupList()=" + getGroupList() + ", getForumId()="
				+ getForumId() + ", getCanOneWay()=" + getCanOneWay()
				+ ", getActive()=" + getActive() + ", getIsDelete()="
				+ getIsDelete() + ", getBranchId()=" + getBranchId()
				+ ", getInstituteId()=" + getInstituteId() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
}
