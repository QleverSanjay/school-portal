package com.qfix.model;

import lombok.Data;

@Data
public class CollegeYear {
	private Integer id;
	private String name;
	private String tag;
}
