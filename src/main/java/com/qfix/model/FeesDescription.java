package com.qfix.model;

import org.springframework.stereotype.Component;

@Component
public class FeesDescription implements Cloneable {

	private Integer id;
	private Integer feesId;
	private Integer studentId;
	private String paidDate;
	private Integer paymentAuditId;
	private Integer createdBy;
	private String createdByName;
	private Integer amount;
	private Integer passbookId;
	private Integer newAmount;
	private boolean isPgEntry;
	private String qfixReferenceNumber;
	private String paymentMode;
	private String ofLineReferenceNo;
	private Long feeScheduleId;
	private String transactionType;
	private String modeOfPayment;
	private String modeOfPaymentChar;
	private String refundAllowed;
	private String partialRefundAllowed;
	private String refundStatus;

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getModeOfPaymentChar() {
		return modeOfPaymentChar;
	}

	public void setModeOfPaymentChar(String modeOfPaymentChar) {
		this.modeOfPaymentChar = modeOfPaymentChar;
	}

	public String getRefundAllowed() {
		return refundAllowed;
	}

	public void setRefundAllowed(String refundAllowed) {
		this.refundAllowed = refundAllowed;
	}

	public String getPartialRefundAllowed() {
		return partialRefundAllowed;
	}

	public void setPartialRefundAllowed(String partialRefundAllowed) {
		this.partialRefundAllowed = partialRefundAllowed;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Long getFeeScheduleId() {
		return feeScheduleId;
	}

	public void setFeeScheduleId(Long feeScheduleId) {
		this.feeScheduleId = feeScheduleId;
	}

	public String getOfLineReferenceNo() {
		return ofLineReferenceNo;
	}

	public void setOfLineReferenceNo(String ofLineReferenceNo) {
		this.ofLineReferenceNo = ofLineReferenceNo;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

	public boolean isPgEntry() {
		return isPgEntry;
	}

	public void setPgEntry(boolean isPgEntry) {
		this.isPgEntry = isPgEntry;
	}

	public Integer getNewAmount() {
		return newAmount;
	}

	public void setNewAmount(Integer newAmount) {
		this.newAmount = newAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFeesId() {
		return feesId;
	}

	public void setFeesId(Integer feesId) {
		this.feesId = feesId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public Integer getPaymentAuditId() {
		return paymentAuditId;
	}

	public void setPaymentAuditId(Integer paymentAuditId) {
		this.paymentAuditId = paymentAuditId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getPassbookId() {
		return passbookId;
	}

	public void setPassbookId(Integer passbookId) {
		this.passbookId = passbookId;
	}

	public FeesDescription copy() throws CloneNotSupportedException {
		return (FeesDescription) clone();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		return "FeesDescription [id=" + id + ", feesId=" + feesId + ", studentId=" + studentId + ", paidDate="
				+ paidDate + ", paymentAuditId=" + paymentAuditId + ", createdBy=" + createdBy + ", createdByName="
				+ createdByName + ", amount=" + amount + ", passbookId=" + passbookId + ", newAmount=" + newAmount
				+ ", isPgEntry=" + isPgEntry + ", qfixReferenceNumber=" + qfixReferenceNumber + ", paymentMode="
				+ paymentMode + ", ofLineReferenceNo=" + ofLineReferenceNo + "]";
	}

}
