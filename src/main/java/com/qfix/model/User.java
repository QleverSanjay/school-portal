package com.qfix.model;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	private Integer id;
	private String username;
	private String password;
	private String active;
	private Integer roleId;
	private String role;
	private Integer branchId;
	private ChatProfile chatProfile;
	private UserForumProfile forumProfile;
	private Integer instituteId;
	private String instituteName;
	private String instituteLogoUrl;
	private Integer entityId;
	private String forumProfileId;
	private String forumCookie;
	private LinkedList<MenuGroup> menus;
	private List<RoleMenuPermissions> rolePermissions;
	private ShoppingProfile shoppingProfile;
	private String token;
	private String[] roles;
	private String status;
	private String name;
	private List<Integer> branchIdList;
	private boolean isBRanchAdmin;
	private String loginCount;

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password="
				+ password + ", active=" + active + ", roleId=" + roleId
				+ ", role=" + role + ", branchId=" + branchId
				+ ", chatProfile=" + chatProfile + ", forumProfile="
				+ forumProfile + ", instituteId=" + instituteId
				+ ", instituteName=" + instituteName + ", instituteLogoUrl="
				+ instituteLogoUrl + ", entityId=" + entityId
				+ ", forumProfileId=" + forumProfileId + ", forumCookie="
				+ forumCookie + ", menus=" + menus + ", rolePermissions="
				+ rolePermissions + ", shoppingProfile=" + shoppingProfile
				+ ", token=" + token + ", roles=" + Arrays.toString(roles)
				+ ", status=" + status + ", name=" + name + ", branchIdList="
				+ branchIdList + "]";
	}

}
