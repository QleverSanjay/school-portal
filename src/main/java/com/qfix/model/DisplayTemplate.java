package com.qfix.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class DisplayTemplate {

	private Integer id;

	@NotEmpty(message = "Template name is mandatory.")
	@Size(min = 1, max = 60, message = "Template name must be less than 60 charecters.")
	private String name;

	@Size(max = 250)
	private String description;

	@NotNull
	private List<DisplayHead> displayHeadList;

	@NotNull
	private Integer branchId;

	private String active;
	private Integer instituteId;

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DisplayHead> getDisplayHeadList() {
		return displayHeadList;
	}

	public void setDisplayHeadList(List<DisplayHead> displayHeadList) {
		this.displayHeadList = displayHeadList;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "DisplayTemplate [id=" + id + ", name=" + name
				+ ", description=" + description + ", displayHeadList="
				+ displayHeadList + ", branchId=" + branchId + ", active="
				+ active + "]";
	}

}
