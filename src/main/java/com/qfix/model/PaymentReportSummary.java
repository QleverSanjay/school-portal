package com.qfix.model;

import lombok.Data;

@Data
public class PaymentReportSummary {
	private String reportDate;
	private Integer numberOfPayments;
	private Double grandTotal;
	private Double submerchantCharges;
	private Double refundAdjusted;
	private Double chargebackAdjusted;
	private Double chargebackReversal;
	private Double totalNetAmount;
	private String uploadedBy;
	private String uploadedOn;
}
