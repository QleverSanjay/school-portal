package com.qfix.model;

import lombok.Data;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@Component
@JsonIgnoreProperties(ignoreUnknown=true)
public class BranchAdmin {

	private String city;
	private String dateOfBirth;
	private String email;
	private String firstName;
	private String gender;
	private String lastName;
	private String pincode;
	private String primaryContact;
	private String status;
	private Integer id;
	private Integer userId;
	private Integer branchId;

	@Override
	public String toString() {
		return "BranchAdmin [city=" + city + ", dateOfBirth=" + dateOfBirth
				+ ", email=" + email + ", firstName=" + firstName + ", gender="
				+ gender + ", lastName=" + lastName + ", pincode=" + pincode
				+ ", primaryContact=" + primaryContact + ", status=" + status
				+ "]";
	}

}
