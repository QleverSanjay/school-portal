package com.qfix.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qfix.utilities.Constants;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Teacher {
	final static Logger logger = Logger.getLogger(Teacher.class);

	private Integer id;
	@NotNull()
	@Pattern(regexp = "^[A-Z a-z]{2,45}$", message = "Must be valid first name.")
	private String firstName;

	@Pattern(regexp = "^$|^[A-Z a-z]{1,45}$", message = "Must be valid middle name.")
	private String middleName;

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z]{1,45}$", message = "Must be valid last name.")
	private String lastName;

	// This regexp remove because of strict validation on date is applied on
	// getDate method in date util
	// @Pattern(regexp =
	// "^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])|(0[1-9]|[12]\\d|3[01])-(0?[1-9]|1[012])-(\\d{4})$",
	// message = "Must be valid date eg (15-10-2015)")
	private String dateOfBirth;

	/*@Pattern(regexp = "^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])|(0[1-9]|[12]\\d|3[01])-(0?[1-9]|1[012])-(\\d{4})$", message = "Must be valid date eg (15-10-2015)")*/
	private String dateOfBirthStr;

	@NotNull()
	@Size(max = 45, message = "Email is too long.")
	@Pattern(regexp = "^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid email address.")
	private String emailAddress;

	@NotNull()
	@Pattern(regexp = "^[FM]{1,1}$", message = "Must be valid gender.(e.g.: M or F)")
	private String gender;

	@NotNull()
	@Pattern(regexp = "^[0-9]{10,10}$", message = "Must be valid mobile number.")
	private String primaryContact;

	@Pattern(regexp = "^$|^[0-9]{10,10}$", message = "Must be valid secondary contact.")
	private String secondaryContact;

	//@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9 , . () - /]{2,45}$", message = "Must be valid address.")
	private String addressLine;

	//@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9 , . () -]{2,45}$", message = "Must be valid area.")
	private String area;

	//@NotNull()
	@Pattern(regexp = "^[A-Z a-z]{2,45}$", message = "Must be valid city.")
	private String city;

	//@NotNull()
	@Pattern(regexp = "^[0-9]{6,6}$", message = "Must be valid pincode.")
	private String pinCode;
	private Date dateOfBirthD;
	private Integer userId;
	private String active;
	private Integer roleId;
	private String role;
	private String password;
	private String photo;
	private Integer branchId;
	private Integer instituteId;
	private String isDelete;
	private String instituteName;
	private String branchName;
	private Integer personId;

	private List<TeacherStandard> teacherStandardList;

	private Integer stateId;
	private Integer districtId;
	private Integer talukaId;

	private String state;
	private String district;
	private String taluka;
	private String username;

	private SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	private boolean isTeacherExistsAsParent;

	public boolean isTeacherExistsAsParent() {
		return isTeacherExistsAsParent;
	}

	public void setTeacherExistsAsParent(boolean isTeacherExistsAsParent) {
		this.isTeacherExistsAsParent = isTeacherExistsAsParent;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<TeacherStandard> getTeacherStandardList() {
		return teacherStandardList;
	}

	public void setTeacherStandardList(List<TeacherStandard> teacherStandardList) {
		this.teacherStandardList = teacherStandardList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getDateOfBirthStr() {
		return dateOfBirthStr;
	}

	public void setDateOfBirthStr(String dateOfBirthStr) {
		try {
			Date date = df.parse(dateOfBirthStr);
			setDateOfBirth(Constants.DATABASE_DATE_FORMAT.format(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
		this.dateOfBirthStr = dateOfBirthStr;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(String secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoto() {
		if (StringUtils.isEmpty(photo)) {
			photo = null;
		}
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getBranch() {
		return branchName;
	}

	public void setBranch(String branch) {
		this.branchName = branch;
	}

	

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public Date getDateOfBirthD() {
		return dateOfBirthD;
	}

	public void setDateOfBirthD(Date dateOfBirthD) {
		this.dateOfBirthD = dateOfBirthD;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTalukaId() {
		return talukaId;
	}

	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTaluka() {
		return taluka;
	}

	public void setTaluka(String taluka) {
		this.taluka = taluka;
	}

	@Override
	public String toString() {
		return "Teacher [id=" + id + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName
				+ ", dateOfBirth=" + dateOfBirth + ", dateOfBirthStr="
				+ dateOfBirthStr + ", dateOfBirthD=" + dateOfBirthD
				+ ", emailAddress=" + emailAddress + ", gender=" + gender
				+ ", primaryContact=" + primaryContact + ", secondaryContact="
				+ secondaryContact + ", addressLine=" + addressLine + ", area="
				+ area + ", city=" + city + ", pinCode=" + pinCode
				+ ", userId=" + userId + ", active=" + active + ", roleId="
				+ roleId + ", role=" + role + ", password=" + password
				+ ", photo=" + photo + ", branchId=" + branchId
				+ ", instituteId=" + instituteId + ", isDelete=" + isDelete
				+ ", instituteName=" + instituteName + ", branchName="
				+ branchName + ", personId=" + personId
				+ ", teacherStandardList=" + teacherStandardList + "]";
	}
}
