package com.qfix.model;

import org.springframework.stereotype.Component;

@Component
public class Individual {

	private String name;
	private String course;
	private String role;
	private Integer id;
	private String standard;
	private String division;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	@Override
	public String toString() {
		return "Individual [name=" + name + ", course=" + course + ", role="
				+ role + ", id=" + id + ", standard=" + standard
				+ ", division=" + division + "]";
	}
}
