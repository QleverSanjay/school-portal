package com.qfix.model;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TimetableModel {

	private Integer id;
	@JsonProperty(value = "standard_id")
	@NotNull
	Integer standardId;

	@JsonProperty(value = "division_id")
	List<Integer> division;

	@JsonProperty(value = "payload")
	@NotNull
	String jsonPayload;

	@JsonProperty(value = "timetable_detail")
	@NotNull
	Map<String, List<TimeTableEntry>> detail;

	@JsonProperty(value = "start_date")
	@NotNull
	String startDate;

	@JsonProperty(value = "end_date")
	@NotNull
	String endDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public List<Integer> getDivision() {
		return division;
	}

	public void setDivision(List<Integer> division) {
		this.division = division;
	}

	public String getJsonPayload() {
		return jsonPayload;
	}

	public void setJsonPayload(String jsonPayload) {
		this.jsonPayload = jsonPayload;
	}

	public Map<String, List<TimeTableEntry>> getDetail() {
		return detail;
	}

	public void setDetail(Map<String, List<TimeTableEntry>> detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "Timetable [standardId=" + standardId + ", division=" + division
				+ ", jsonPayload=" + jsonPayload + ", detail=" + detail + "]";
	}

}
