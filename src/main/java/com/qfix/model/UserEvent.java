package com.qfix.model;

public class UserEvent {

	private Integer userId;
	private Integer entityId;
	private Integer forUserId;
	private Integer roleId;
	private Integer eventId;
	private Integer eventGroupId;
	private Integer id;
	private String status;
	private boolean insertRecord;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public Integer getForUserId() {
		return forUserId;
	}

	public void setForUserId(Integer forUserId) {
		this.forUserId = forUserId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getEventGroupId() {
		return eventGroupId;
	}

	public void setEventGroupId(Integer eventGroupId) {
		this.eventGroupId = eventGroupId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isInsertRecord() {
		return insertRecord;
	}

	public void isInsertRecord(boolean insertRecord) {
		this.insertRecord = insertRecord;
	}

	@Override
	public String toString() {
		return "UserEvent [userId=" + userId + ", entityId=" + entityId
				+ ", forUserId=" + forUserId + ", roleId=" + roleId
				+ ", eventId=" + eventId + ", eventGroupId=" + eventGroupId
				+ ", id=" + id + ", status=" + status + ", insertRecord="
				+ insertRecord + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
		result = prime * result
				+ ((forUserId == null) ? 0 : forUserId.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEvent other = (UserEvent) obj;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		if (eventId == null) {
			if (other.eventId != null)
				return false;
		} else if (!eventId.equals(other.eventId))
			return false;
		if (forUserId == null) {
			if (other.forUserId != null)
				return false;
		} else if (!forUserId.equals(other.forUserId))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
}
