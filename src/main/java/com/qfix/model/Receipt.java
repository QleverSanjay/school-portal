package com.qfix.model;

import org.springframework.stereotype.Component;

@Component
public class Receipt {
	
	private Integer branchId;
	private Integer InstituteId;
	private boolean useSchoolLogAsWatermarkImage;
	private boolean hideLogo;
	private String trailerRecord;


	public boolean isUseSchoolLogAsWatermarkImage() {
		return useSchoolLogAsWatermarkImage;
	}

	public void setUseSchoolLogAsWatermarkImage(boolean watermarkImage) {
		this.useSchoolLogAsWatermarkImage = watermarkImage;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public Integer getInstituteId() {
		return InstituteId;
	}
	public void setInstituteId(Integer instituteId) {
		InstituteId = instituteId;
	}
	
	public boolean isHideLogo() {
		return hideLogo;
	}
	public void setHideLogo(boolean hideLogo) {
		this.hideLogo = hideLogo;
	}
	public String getTrailerRecord() {
		return trailerRecord;
	}
	public void setTrailerRecord(String trailerRecord) {
		this.trailerRecord = trailerRecord;
	}
	@Override
	public String toString() {
		return "Receipt [branchId=" + branchId + ", InstituteId=" + InstituteId
				+ ", watermarkImage=" + useSchoolLogAsWatermarkImage + ", hideLogo="
				+ hideLogo + ", trailerRecord=" + trailerRecord + "]";
	}
	
	
	
	
	
	

}
