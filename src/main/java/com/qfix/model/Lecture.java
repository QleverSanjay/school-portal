package com.qfix.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Lecture {
	 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
	private String uuid;
	private String title;
	private Date start;
	private Date end;
	private String className;
	private int _id;
	

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}
	
	public String getLectureAsJsonString() {
		if(start != null && end != null && title != null) {
			String startDate=format.format(start).replaceAll("IST", "Z");
			String endDate =format.format(end).replaceAll("IST", "Z");
			String generatedUUID = UUID.randomUUID().toString();
			return "{\"title\":\""+title+"\",\"start\":\""+startDate+"\",\"end\":\""+endDate+"\",\"className\":[\"openSesame\"],\"uuid\":\""+generatedUUID+"\",\"_id\":0}";
		} else {
			return "";
		}
			
	}

	@Override
	public String toString() {
		return "Lecture [uuid=" + uuid + ", title=" + title + ", start="
				+ start + ", end=" + end + ", className=" + className
				+ ", _id=" + _id + "]";
	}
	
	public static void main(String[] args) {
		Lecture l = new Lecture();
		try {
			l.setStart(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse("2016-09-12T21:51:00.000-0700"));
			l.setEnd(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse("2016-09-12T22:51:00.000-0700"));
			System.out.println(l.getLectureAsJsonString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}