package com.qfix.model;

public class StudentExcelReportFilter {

	private Integer instituteId;
	private Integer branchId;
	private Integer standardId;
	private Integer divisionId;
	private Integer teacherUserId;
	private TableOptions tableOptions;

	public TableOptions getTableOptions() {
		return tableOptions;
	}

	public void setTableOptions(TableOptions tableOptions) {
		this.tableOptions = tableOptions;
	}

	public Integer getTeacherUserId() {
		return teacherUserId;
	}

	public void setTeacherUserId(Integer teacherUserId) {
		this.teacherUserId = teacherUserId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	@Override
	public String toString() {
		return "StudentExcelReportFilter [instituteId=" + instituteId
				+ ", branchId=" + branchId + ", standardId=" + standardId
				+ ", divisionId=" + divisionId + ", teacherUserId="
				+ teacherUserId + "]";
	}

}
