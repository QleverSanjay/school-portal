package com.qfix.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PaymentReportDetail {
	private String transactionID;
	private String settlementDate;
	private Double settlementAmount;
	private String qfixReferenceNumber;
	
	public PaymentReportDetail(String transactionID, String settlementDate, Double settlementAmount) {
		this.transactionID = transactionID;
		this.settlementDate = settlementDate;
		this.settlementAmount = settlementAmount;
	}
}
