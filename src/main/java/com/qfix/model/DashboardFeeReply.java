package com.qfix.model;

import lombok.Data;

@Data
public class DashboardFeeReply {
	String feeType;
	Double amount;
}
