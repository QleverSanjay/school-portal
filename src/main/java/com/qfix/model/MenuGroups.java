package com.qfix.model;

import lombok.Data;

@Data
public class MenuGroups {

	private String menuItems;
	private String stateUrls;
	private String groupName;
	private String groupTag;
}
