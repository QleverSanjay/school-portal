package com.qfix.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Institute {

	@NotNull()
	/*
	 * @Pattern(regexp = "^[A-Z a-z]{2,100}$", message =
	 * "Must be valid institute name")
	 */
	@Size(max = 100, message = "Institute name is too long.")
	private String instituteName;

	@Pattern(regexp = "^$|^[A-Z a-z 0-9 -]{4,16}$", message = "Must be valid code.")
	private String code;
	private String active;
	private Integer id;
	@NotNull()
	// @Pattern(regexp = "^[A-Za-z]{2,45}$", message =
	// "Must be valid first name")
	private String firstName;
	@NotNull()
	// @Pattern(regexp = "^[A-Za-z]{2,45}$", message =
	// "Must be valid last name")
	private String lastName;

	@NotNull()
	@Size(max = 45, message = "Email is too long.")
	@Pattern(regexp = "^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid email.")
	private String adminEmail;

	@NotNull()
	@Pattern(regexp = "^[FM]{1,1}$", message = "Must be valid gender.(e.g.: M or F)")
	private String gender;

	@NotNull()
	@Pattern(regexp = "^[0-9]{10,10}$", message = "Primary contact number must be of 10 digits.")
	private String primaryContact;

	private String password;
	private Integer userId;
	private String isSchoolAdminActive;
	// @NotNull()
	@Size(max = 45, message = "City is too long.")
	private String adminCity;

	// @NotNull()
	@Pattern(regexp = "^[0-9]{6,6}$", message = "Must be valid pin code.")
	private String adminPinCode;

	@Size(max = 255, message = "Logo url is too long.")
	private String logoUrl;
	private String isDelete;
	private String username;
	private boolean isExpressOnboard;

	public boolean isExpressOnboard() {
		return isExpressOnboard;
	}

	public void setExpressOnboard(boolean isExpressOnboard) {
		this.isExpressOnboard = isExpressOnboard;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getIsSchoolAdminActive() {
		return isSchoolAdminActive;
	}

	public void setIsSchoolAdminActive(String isSchoolAdminActive) {
		this.isSchoolAdminActive = isSchoolAdminActive;
	}

	public String getAdminCity() {
		return adminCity;
	}

	public void setAdminCity(String adminCity) {
		this.adminCity = adminCity;
	}

	public String getAdminPinCode() {
		return adminPinCode;
	}

	public void setAdminPinCode(String adminPinCode) {
		this.adminPinCode = adminPinCode;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "Institute [instituteName=" + instituteName + ", code=" + code
				+ ", active=" + active + ", id=" + id + ", firstName="
				+ firstName + ", lastName=" + lastName + ", adminEmail="
				+ adminEmail + ", gender=" + gender + ", primaryContact="
				+ primaryContact + ", password=" + password + ", userId="
				+ userId + ", isSchoolAdminActive=" + isSchoolAdminActive
				+ ", adminCity=" + adminCity + ", adminPinCode=" + adminPinCode
				+ ", logoUrl=" + logoUrl + ", isDelete=" + isDelete + "]";
	}
}
