package com.qfix.model;

public class RoleMenuPermissions {

	private Integer roleId;
	private String permissionName;
	private boolean allow;
	private Integer menuPermissionId;
	private String url;
	private Integer menuId;
	
	
	
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getMenuPermissionId() {
		return menuPermissionId;
	}
	public void setMenuPermissionId(Integer menuPermissionId) {
		this.menuPermissionId = menuPermissionId;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	public boolean isAllow() {
		return allow;
	}
	public void setAllow(boolean allow) {
		this.allow = allow;
	}
	
	
	
}
