package com.qfix.model;

import java.util.List;

import lombok.Data;

@Data
public class Standard {

	private Integer id;
	private String name;
	private String text;
	private String tag;
	private String displayName;
	private String code;
	private Integer branchId;
	private String branchName;
	private Integer instituteId;
	private String instituteName;
	private List<Integer> divisionIdList;
	private List<Division> divisionList;
	private Integer academicYearId;
	private String academicYearStr;
	private String divisions;
	private String qfixDefinedName;
	private List<Subject> subjectList;

	@Override
	public String toString() {
		return "Standard [id=" + id + ", displayName=" + displayName
				+ ", code=" + code + ", branchId=" + branchId + ", branchName="
				+ branchName + ", instituteId=" + instituteId
				+ ", instituteName=" + instituteName
				+ ", academicYearId=" + academicYearId + ", academicYearStr="
				+ academicYearStr + "]";
	}
}
