package com.qfix.model;

import lombok.Data;

@Data
public class DashboardFeeHeadData {
	String feeHeadName;
	Double sumAmount;
}
