package com.qfix.model;

import lombok.Data;

@Data
public class University {
	private Integer id;
	private String name;
}
