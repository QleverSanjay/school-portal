package com.qfix.model;

import lombok.Data;

@Data
public class WorkingDay {

	private Integer id;
	private Integer currentAcademicYearId;
	private String currentAcademicYearStr;
	private String monday;
	private String tuesday;
	private String wednesday;
	private String thursday;
	private String friday;
	private String saturday;
	private String sunday;
	private Integer branchId;
	private String branchName;
	private Integer instituteId;
	private String instituteName;
	private String skipTimetableOnHoliday;
	private Integer roundRobinTimetableDays;

	@Override
	public String toString() {
		return "WorkingDay [id=" + id + ", currentAcademicYearId="
				+ currentAcademicYearId + ", monday=" + monday + ", tuesday="
				+ tuesday + ", wednesday=" + wednesday + ", thursday="
				+ thursday + ", friday=" + friday + ", saturday=" + saturday
				+ ", sunday=" + sunday + ", branchId=" + branchId
				+ ", branchName=" + branchName + ", instituteId=" + instituteId
				+ ", instituteName=" + instituteName + "]";
	}
}
