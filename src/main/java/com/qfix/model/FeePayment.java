package com.qfix.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

@Component
public class FeePayment {

	@NotNull
	private Long feeScheduleId;

	@NotNull
	private Integer feeId;

	@NotNull
	private Integer studentId;

	@NotNull
	private double amount;

	@NotNull
	private Date paymentDate;

	private Integer createdBy;

	private String paymentDetail;

	private String paymentReferenceDetail;

	private boolean isOfflinePaymentMarkingPaid;

	private Long paymentAuditId;

	private String bankBranchName;
	private String bankName;
	private String ifscCode;
	private String checkOrDDNumber;

	public String getBankBranchName() {
		return bankBranchName;
	}

	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getCheckOrDDNumber() {
		return checkOrDDNumber;
	}

	public void setCheckOrDDNumber(String checkOrDDNumber) {
		this.checkOrDDNumber = checkOrDDNumber;
	}

	public Long getPaymentAuditId() {
		return paymentAuditId;
	}

	public void setPaymentAuditId(Long paymentAuditId) {
		this.paymentAuditId = paymentAuditId;
	}

	public boolean isOfflinePaymentMarkingPaid() {
		return isOfflinePaymentMarkingPaid;
	}

	public void setOfflinePaymentMarkingPaid(boolean isOfflinePaymentMarkingPaid) {
		this.isOfflinePaymentMarkingPaid = isOfflinePaymentMarkingPaid;
	}

	public Long getFeeScheduleId() {
		return feeScheduleId;
	}

	public void setFeeScheduleId(Long feeScheduleId) {
		this.feeScheduleId = feeScheduleId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getFeeId() {
		return feeId;
	}

	public void setFeeId(Integer feeId) {
		this.feeId = feeId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getpaymentDetail() {
		return paymentDetail;
	}

	public void setPaymentDetail(String paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

	public String getpaymentReferenceDetail() {
		return paymentReferenceDetail;
	}

	public void setPaymentReferenceDetail(String paymentReferenceDetail) {
		this.paymentReferenceDetail = paymentReferenceDetail;
	}

	@Override
	public String toString() {
		return "FeePayment [feeId=" + feeId + ", studentId=" + studentId
				+ ", amount=" + amount + ", paymentDate=" + paymentDate
				+ ", paymentDetail=" + paymentDetail
				+ ",paymentReferenceDetail=" + paymentReferenceDetail + "]";
	}

}
