package com.qfix.model;

import org.springframework.stereotype.Component;

@Component
public class EmailSetting {

	private Integer id;
	private String serverName;
	private String port;
	private String connectionType;
	private String security;
	private String authenticationMethod;
	private String username;
	private String active;
	private String password;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public String getAuthenticationMethod() {
		return authenticationMethod;
	}

	public void setAuthenticationMethod(String authenticationMethod) {
		this.authenticationMethod = authenticationMethod;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "EmailSetting [id=" + id + ", serverName=" + serverName
				+ ", port=" + port + ", connectionType=" + connectionType
				+ ", security=" + security + ", authenticationMethod="
				+ authenticationMethod + ", username=" + username + ", active="
				+ active + ", password=" + password + "]";
	}
}
