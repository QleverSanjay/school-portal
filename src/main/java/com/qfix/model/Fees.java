package com.qfix.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Fees {
	private Integer id;
	private Integer headId;

	private String head;
	@NotNull()
	@Pattern(regexp = "^(0|[1-9]\\d*)$", message = "Must be valid amount.")
	private String amount;
	private Integer branchId;
	private String branch;
	private String studentName;
	private String rollNo;
	private String registrationCode;
	private String dueDate;
	private String toDate;
	private String fromDate;

	private Date dueDateD;
	private Date toDateD;
	private Date fromDateD;

	private Integer casteId;
	private String caste;
	private Integer standardId;

	@Size(max = 255, message = "Must be less than 255 charecters.")
	private String description;
	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry for isEmail.(e.g.: Y or N)")
	private String isEmail;
	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry for isSms.(e.g.: Y or N)")
	private String isSms;
	@Pattern(regexp = "^[YN]{1,1}$", message = "Must be valid entry for isNotification.(e.g.: Y or N)")
	private String isNotification;

	private String frequency;
	private Integer instituteId;

	private String standard;
	private Integer academicYearId;
	private String academicYearStr;
	private Integer divisionId;
	private String division;
	private Integer latePaymentCharges;
	private String isSplitPayment;
	private String isPartialPaymentAllowed;
	private String schemeCode;
	private Integer schemeCodeId;

	@Pattern(regexp = "^[0-9|\\.]{1,45}$", message = "Must be valid amount.")
	private String splitAmount;
	private String seedSchemeCode;
	private Integer seedSchemeCodeId;

	@Pattern(regexp = "$|^[0-9|\\.]{1,45}$", message = "Must be valid amount.")
	private String seedSplitAmount;
	private Integer studentId;

	final static Logger logger = Logger.getLogger(Fees.class);
	private String feesCodeName;
	private Integer feesCodeId;

	private String recurring;
	private String recurringFrequency;
	private String byDays;

	private String feeType;
	private Integer lateFeeDetailId;

	// @NotNull(message = "Reminder start date is mandatory.")
	private String reminderStartDate;
	private Date reminderStartDateD;
	private FeesRecurringDetail recurringDetail;
	private String lateFeeDetailName;
	private String editDueDateAllowed;
	private String displayTemplateName;
	private Integer displayTemplateId;
	List<DisplayTemplate> displayTemplates;
	private boolean isRecurring;

	private String currency;
	private String feesAction;
	private String allowRepeatEntries;
	
	private String isAllowUserEnterAmt;
	
	

	public String getIsAllowUserEnterAmt() {
		return isAllowUserEnterAmt;
	}

	public void setIsAllowUserEnterAmt(String isAllowUserEnterAmt) {
		this.isAllowUserEnterAmt = isAllowUserEnterAmt;
	}

	public String getAllowRepeatEntries() {
		return allowRepeatEntries;
	}

	public void setAllowRepeatEntries(String allowRepeatEntries) {
		this.allowRepeatEntries = allowRepeatEntries;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public boolean isRecurring() {
		return isRecurring;
	}

	public void setIsRecurring(boolean isRecurring) {
		this.isRecurring = isRecurring;
	}

	public String getDisplayTemplateName() {
		return displayTemplateName;
	}

	public void setDisplayTemplateName(String displayTemplateName) {
		this.displayTemplateName = displayTemplateName;
	}

	public String getEditDueDateAllowed() {
		return editDueDateAllowed;
	}

	public void setEditDueDateAllowed(String editDueDateAllowed) {
		this.editDueDateAllowed = editDueDateAllowed;
	}

	public String getLateFeeDetailName() {
		return lateFeeDetailName;
	}

	public void setLateFeeDetailName(String lateFeeDetailName) {
		this.lateFeeDetailName = lateFeeDetailName;
	}

	public Integer getFeesCodeId() {
		return feesCodeId;
	}

	public void setFeesCodeId(Integer feesCodeId) {
		this.feesCodeId = feesCodeId;
	}

	public String getFeesCodeName() {
		return feesCodeName;
	}

	public void setFeesCodeName(String feesCodeName) {
		this.feesCodeName = feesCodeName;
	}

	public String getIsPartialPaymentAllowed() {
		return isPartialPaymentAllowed;
	}

	public void setIsPartialPaymentAllowed(String isPartialPaymentAllowed) {
		this.isPartialPaymentAllowed = isPartialPaymentAllowed;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getIsSplitPayment() {
		return isSplitPayment;
	}

	public void setIsSplitPayment(String isSplitAmount) {
		this.isSplitPayment = isSplitAmount;
	}

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	public Integer getSchemeCodeId() {
		return schemeCodeId;
	}

	public void setSchemeCodeId(Integer schemeCodeId) {
		this.schemeCodeId = schemeCodeId;
	}

	public Integer getSeedSchemeCodeId() {
		return seedSchemeCodeId;
	}

	public void setSeedSchemeCodeId(Integer seedSchemeCodeId) {
		this.seedSchemeCodeId = seedSchemeCodeId;
	}

	public String getSplitAmount() {
		return splitAmount;
	}

	public void setSplitAmount(String splitAmount) {
		this.splitAmount = splitAmount;
	}

	public String getSeedSchemeCode() {
		return seedSchemeCode;
	}

	public void setSeedSchemeCode(String seedSchemeCode) {
		this.seedSchemeCode = seedSchemeCode;
	}

	public String getSeedSplitAmount() {
		return seedSplitAmount;
	}

	public void setSeedSplitAmount(String seedSplitAmount) {
		this.seedSplitAmount = seedSplitAmount;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Integer getLatePaymentCharges() {
		return latePaymentCharges;
	}

	public void setLatePaymentCharges(Integer latePaymentCharges) {
		this.latePaymentCharges = latePaymentCharges;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public Integer getCasteId() {
		return casteId;
	}

	public void setCasteId(Integer casteId) {
		this.casteId = casteId;
	}

	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getIsEmail() {
		return isEmail;
	}

	public void setIsEmail(String isEmail) {
		this.isEmail = isEmail;
	}

	public String getIsSms() {
		return isSms;
	}

	public void setIsSms(String isSms) {
		this.isSms = isSms;
	}

	public String getIsNotification() {
		return isNotification;
	}

	public void setIsNotification(String isNotification) {
		this.isNotification = isNotification;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearStr() {
		return academicYearStr;
	}

	public void setAcademicYearStr(String academicYearStr) {
		this.academicYearStr = academicYearStr;
	}

	public Date getDueDateD() {
		return dueDateD;
	}

	public void setDueDateD(Date dueDateD) {
		this.dueDateD = dueDateD;
	}

	public Date getToDateD() {
		return toDateD;
	}

	public void setToDateD(Date toDateD) {
		this.toDateD = toDateD;
	}

	public Date getFromDateD() {
		return fromDateD;
	}

	public void setFromDateD(Date fromDateD) {
		this.fromDateD = fromDateD;
	}

	public String getRecurring() {
		return recurring;
	}

	public void setRecurring(String recurring) {
		isRecurring = true;
		this.recurring = recurring;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public Integer getLateFeeDetailId() {
		return lateFeeDetailId;
	}

	public void setLateFeeDetailId(Integer lateFeeDetailId) {
		this.lateFeeDetailId = lateFeeDetailId;
	}

	public Date getReminderStartDateD() {
		return reminderStartDateD;
	}

	public void setReminderStartDateD(Date reminderStartDateD) {
		this.reminderStartDateD = reminderStartDateD;
	}

	public String getReminderStartDate() {
		return reminderStartDate;
	}

	public void setReminderStartDate(String reminderStartDate) {
		this.reminderStartDate = reminderStartDate;
	}

	public FeesRecurringDetail getRecurringDetail() {
		return recurringDetail;
	}

	public void setRecurringDetail(FeesRecurringDetail recurringDetail) {
		this.recurringDetail = recurringDetail;
	}

	public Integer getDisplayTemplateId() {
		return displayTemplateId;
	}

	public void setDisplayTemplateId(Integer displayTemplateId) {
		this.displayTemplateId = displayTemplateId;
	}

	public List<DisplayTemplate> getDisplayTemplates() {
		return displayTemplates;
	}

	public void setDisplayTemplates(List<DisplayTemplate> displayTemplates) {
		this.displayTemplates = displayTemplates;
	}

	public String getRecurringFrequency() {
		return recurringFrequency;
	}

	public void setRecurringFrequency(String recurringFrequency) {
		isRecurring = true;
		this.recurringFrequency = recurringFrequency;
	}

	public String getByDays() {
		return byDays;
	}

	public void setByDays(String byDays) {
		isRecurring = true;
		this.byDays = byDays;
	}

	public String getFeesAction() {
		return feesAction;
	}

	public void setFeesAction(String feesAction) {
		this.feesAction = feesAction;
	}

	@Override
	public String toString() {
		return "Fees [id=" + id + ", headId=" + headId + ", head=" + head + ", amount=" + amount + ", branchId="
				+ branchId + ", branch=" + branch + ", studentName=" + studentName + ", rollNo=" + rollNo
				+ ", registrationCode=" + registrationCode + ", dueDate=" + dueDate + ", toDate=" + toDate
				+ ", fromDate=" + fromDate + ", dueDateD=" + dueDateD + ", toDateD=" + toDateD + ", fromDateD="
				+ fromDateD + ", casteId=" + casteId + ", caste=" + caste + ", standardId=" + standardId
				+ ", description=" + description + ", isEmail=" + isEmail + ", isSms=" + isSms + ", isNotification="
				+ isNotification + ", frequency=" + frequency + ", instituteId=" + instituteId + ", standard="
				+ standard + ", academicYearId=" + academicYearId + ", academicYearStr=" + academicYearStr
				+ ", divisionId=" + divisionId + ", division=" + division + ", latePaymentCharges=" + latePaymentCharges
				+ ", isSplitPayment=" + isSplitPayment + ", isPartialPaymentAllowed=" + isPartialPaymentAllowed
				+ ", schemeCode=" + schemeCode + ", schemeCodeId=" + schemeCodeId + ", splitAmount=" + splitAmount
				+ ", seedSchemeCode=" + seedSchemeCode + ", seedSchemeCodeId=" + seedSchemeCodeId + ", seedSplitAmount="
				+ seedSplitAmount + ", studentId=" + studentId + ", feesCodeName=" + feesCodeName + ", feesCodeId="
				+ feesCodeId + ", recurring=" + recurring + ", recurringFrequency=" + recurringFrequency + ", byDays="
				+ byDays + ", feeType=" + feeType + ", lateFeeDetailId=" + lateFeeDetailId + ", reminderStartDate="
				+ reminderStartDate + ", reminderStartDateD=" + reminderStartDateD + ", recurringDetail="
				+ recurringDetail + ", lateFeeDetailName=" + lateFeeDetailName + ", editDueDateAllowed="
				+ editDueDateAllowed + ", displayTemplateName=" + displayTemplateName + ", displayTemplateId="
				+ displayTemplateId + ", displayTemplates=" + displayTemplates + ", feesAction=" + feesAction + "]";
	}

}
