package com.qfix.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Component
public class SubjectCategory {

	private Integer id;

	@NotNull()
	@Pattern(regexp = "^[A-Z a-z 0-9]{2,45}$", message = "Must be valid subject name.")
	private String name;
	@Size(max = 255, message = "Description may not be greater than 255 charecters.")
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "SubjectCategory [id=" + id + ", name=" + name
				+ ", description=" + description + "]";
	}

}
