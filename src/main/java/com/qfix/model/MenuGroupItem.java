package com.qfix.model;

import lombok.Data;

@Data
public class MenuGroupItem {
	
	private Integer menuId;
	private String stateUrl;
	private String menuItem;
}
