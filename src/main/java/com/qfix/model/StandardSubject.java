package com.qfix.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class StandardSubject {

	private Integer id;
	private String standard;
	private Integer standardId;
	private List<Integer> subjectIdList;
	private Integer branchId;
	private Integer instituteId;
	private Integer subjectId;
	private String subject;
	private List<String> subjectList;
	private Integer previousStandardId;

	public Integer getPreviousStandardId() {
		return previousStandardId;
	}

	public void setPreviousStandardId(Integer previousStandardId) {
		this.previousStandardId = previousStandardId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public List<Integer> getSubjectIdList() {
		return subjectIdList;
	}

	public void setSubjectIdList(List<Integer> subjectIdList) {
		this.subjectIdList = subjectIdList;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<String> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<String> subjectList) {
		this.subjectList = subjectList;
	}

	@Override
	public String toString() {
		return "StandardSubject [id=" + id + ", standard=" + standard
				+ ", standardId=" + standardId + ", subjectIdList="
				+ subjectIdList + ", branchId=" + branchId + ", instituteId="
				+ instituteId + ", subjectId=" + subjectId + ", subject="
				+ subject + ", subjectList=" + subjectList
				+ ", previousStandardId=" + previousStandardId + "]";
	}

}
