package com.qfix.daoimpl;

import java.util.Date;

import org.springframework.stereotype.Repository;

import com.qfix.model.Report;
import com.qfix.utilities.Constants;

@Repository
public class VendorReportDao {

	
	public Report getReports(Integer branchId, Date fromDate, Date toDate){
		
		String toDateStr = (toDate != null ? Constants.DATABASE_DATE_FORMAT.format(toDate) : null);
		String fromDateStr = (fromDate != null ? Constants.DATABASE_DATE_FORMAT.format(fromDate) : null);
		// TODO Call xmlConnect and get records accordingly.
		Report vendorReports = new ShoppingDao().getVendorReports(branchId, fromDateStr, toDateStr);
		
		return vendorReports;
	}
}
