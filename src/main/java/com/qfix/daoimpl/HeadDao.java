package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.Statement;
import com.qfix.dao.HeadInterfaceDao;
import com.qfix.exceptions.HeadAllreadyExistException;
import com.qfix.model.Head;

@Repository
public class HeadDao extends JdbcDaoSupport implements HeadInterfaceDao {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(HeadDao.class);
	
	@Transactional
	public List<Head> getAll(Integer branchId)throws SQLException{
		String sql = "select * from head where is_delete = 'N' AND branch_id = "+branchId ;
		
		logger.debug("get all head sql --"+sql);
		
		return getJdbcTemplate().query(sql, new RowMapper<Head>() {
			public Head mapRow(ResultSet rs, int rownumber)throws SQLException {
			Head head = new Head();
			head.setId(rs.getInt("id"));
			head.setName(rs.getString("name"));
			head.setActive(rs.getString("active"));
			head.setBranchId(rs.getInt("branch_id"));
			head.setAcademicYearId(rs.getInt("academic_year_id"));
			return head;
		}});
	}
	
	@Transactional
	public Head getHeadById(Integer id){
		String sql = "select h.*, i.id as instituteId from head as h, branch as b, institute as i " +
				"where h.branch_id = b.id AND b.institute_id = i.id AND h.id = "+id;
		
		List<Head> headList =  getJdbcTemplate().query(sql, new RowMapper<Head>() {
			public Head mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				Head head = new Head();
				head.setId(resultSet.getInt("id"));
				head.setName(resultSet.getString("name"));
				head.setActive(resultSet.getString("active"));
				head.setBranchId(resultSet.getInt("branch_id"));
				head.setInstituteId(resultSet.getInt("instituteId"));
				head.setAcademicYearId(resultSet.getInt("academic_year_id"));
				return head;
			}
		});
		return headList.get(0);
	}
	
	private boolean isHeadExist(Head head) {
		String sql = "select count(*) as count from head where (name = '"+head.getName()+"')" +
				     " AND branch_id = "+head.getBranchId()+
				      (head.getId() != null ? " AND id != "+head.getId() : "") + " AND is_delete = 'N'";
		logger.debug("is head exist sql --"+sql);
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			return true;
		}
		return false;
	}

	@Override
	public boolean insertHead(Head head) throws HeadAllreadyExistException {
		boolean flag = false;
		if(!isHeadExist(head)){

			String sql = "insert into head (name, active, branch_id, academic_year_id) " +
						 "values (?, ?, ?, ?)";
			
			try {
				final PreparedStatement preparedStatement = dataSource.getConnection().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, head.getName());
				preparedStatement.setString(2, head.getActive());
				preparedStatement.setInt(3, head.getBranchId());
				preparedStatement.setInt(4, head.getAcademicYearId());
				
				KeyHolder holder = new GeneratedKeyHolder();
				getJdbcTemplate().update(new PreparedStatementCreator() {
					
					@Override
					public PreparedStatement createPreparedStatement(Connection arg0)
							throws SQLException {
						// TODO Auto-generated method stub
						return preparedStatement;
					}
				}, holder);
				
				head.setId(holder.getKey().intValue());
				
//				heads.add(head);
				flag = true;
				logger.debug("Head insert completed...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}			
		else {
			logger.error("head already exist....");
			throw new HeadAllreadyExistException("This head is already exist in this Branch.");
		}
		return flag;
	}

	@Transactional
	public boolean updateHead(Head head) throws HeadAllreadyExistException{
		boolean flag = false;
		if(!isHeadExist(head)){
			
			String sql = "update head set name = ?, active = ?, branch_id = ?, is_delete = ?, " +
						 "academic_year_id = ? where id = ?";

			Object[] params = new Object[] {
					head.getName(),
					head.getActive(),
					head.getBranchId(),
					"N",
					head.getAcademicYearId(),
					head.getId()
			};
				
			int[] types = new int[] { Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.VARCHAR,Types.INTEGER,Types.INTEGER};
				
			getJdbcTemplate().update(sql, params, types);

			
			flag = true;
			logger.debug("update head completed...");
		}
		else {
			logger.error("head already exist..");
			throw new HeadAllreadyExistException("This head is already exist in this Branch.");
		}
		
		return flag;
	}

	@Transactional
	public boolean deleteHead(Integer headId) {
		String sql = "update head set is_delete = 'Y' where id = "+headId;
		getJdbcTemplate().update(sql);
		return true;
	}		
	
	@Transactional
	public List<Head> getHeadByBranchForDashboard(Integer branchId){
			String sql = "select * from head where is_delete = 'N' AND active = 'Y' AND branch_id = "+branchId;

			return getJdbcTemplate().query(sql, new RowMapper<Head>() {
				public Head mapRow(ResultSet resultSet, int rownumber)throws SQLException {
				Head head = new Head();
				head.setId(resultSet.getInt("id"));
				head.setName(resultSet.getString("name"));
				head.setActive(resultSet.getString("active"));
				head.setBranchId(resultSet.getInt("branch_id"));
				return head;
			}});
	}

	@Override
	public String getHeadNameByFeesId(Integer feesId) {
		String query = "SELECT h.name FROM fees f JOIN head h on f.head_id = h.id where f.id = " + feesId;
		try{
		return getJdbcTemplate().queryForObject(query, String.class);
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	
}
