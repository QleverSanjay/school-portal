package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.qfix.exceptions.PaymentSettingExistException;
import com.qfix.model.PaymentSetting;

@Repository
public class PaymentSettingDao extends JdbcDaoSupport {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private Connection connection;
	private Statement statement;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet ;
	
	@Autowired
	private Connectivity connect;

	final static Logger logger = Logger.getLogger(PaymentSettingDao.class);
	
	public List<PaymentSetting> getAll(Integer instituteId){
		List<PaymentSetting> paymentSettingList = new ArrayList<PaymentSetting>();

		String sql = null;
		sql = "select * from payment_setting where institute_id = "+instituteId;
		System.out.println("SQL-----"+sql);
		connection = connect.getConnection();

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);

			while(resultSet.next()){
				PaymentSetting paymentSetting = new PaymentSetting();
				paymentSetting.setId(resultSet.getInt("id"));
				paymentSetting.setInstituteId(resultSet.getInt("institute_id"));
				paymentSetting.setPercentage(resultSet.getString("ic_percentage"));
				paymentSetting.setAmount(resultSet.getString("ic_amount"));
				paymentSetting.setGatewayId(resultSet.getString("gateway_id"));
				paymentSettingList.add(paymentSetting);
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return paymentSettingList;
	}

	public String setDefaultPaymentSettings(PaymentSetting paymentSetting){
		String result = null;
		String sql = "select id from payment_setting where institute_id is NULL";
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			boolean flag = false;

			if(resultSet.next()){
				if(resultSet.getInt("id") > 0){
					flag = true;
				}
			}

			if(flag){
				sql = "update payment_setting set gateway_id = '"+paymentSetting.getGatewayId()+
					"', ic_amount = '"+paymentSetting.getAmount()+"', ic_percentage = '"
					+paymentSetting.getPercentage()+"' where institute_id is null";
				result = "{\"status\" : \"success\", \"message\" : \"Payment Setting updated.\"}";
			}
			else {
				sql = "insert into payment_setting (gateway_id, ic_amount, ic_percentage) " +
					"values ('"+paymentSetting.getGatewayId()+"', '"+paymentSetting.getAmount()+"'," +
					" '"+paymentSetting.getPercentage()+"')";
				result = "{\"status\" : \"success\", \"message\" : \"Payment Setting inserted.\"}";
			}
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			result = "{\"status\" : \"fail\", \"message\" : \"Problem while updating payment setting.\"}";
			logger.error("", e);
			e.printStackTrace();
		}
		
		return result;
	}
	
	private boolean paymentSettingExist(PaymentSetting paymentSetting) throws SQLException{
		Statement statement = connection.createStatement();
		String sql = "select count(*) as count from payment_setting " +
				"where institute_id = "+paymentSetting.getInstituteId()+"";

		ResultSet resultSet = statement.executeQuery(sql);
		if(resultSet.next()){
			if(resultSet.getInt("count") > 0){
				return true;
			}
		}
		resultSet.close();
		return false;
	}


	public boolean updatePaymentSetting(PaymentSetting paymentSetting) throws PaymentSettingExistException {
		connection = connect.getConnection();
		boolean flag = false;
		try {
			String sql = "update payment_setting set gateway_id = ?, ic_amount = ?, ic_percentage = ?, " +
					"institute_id = ? where id = ?";

			preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setString(1, paymentSetting.getGatewayId());
			preparedStatement.setString(2, paymentSetting.getAmount());
			preparedStatement.setString(3, paymentSetting.getPercentage());
			preparedStatement.setInt(4, paymentSetting.getInstituteId());
			preparedStatement.setInt(5, paymentSetting.getId());
			preparedStatement.executeUpdate();

			logger.debug("PaymentSetting updated...");
			flag = true;

		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}


	public boolean insertPaymentSetting(PaymentSetting paymentSetting) throws PaymentSettingExistException {
		connection = connect.getConnection();
		boolean flag= false;
		try {
			String sql = "insert into payment_setting (gateway_id, ic_amount, ic_percentage, institute_id) " +
					"values (?, ?, ?, ?)";
			if(!paymentSettingExist(paymentSetting)){
				preparedStatement = connection.prepareStatement(sql);

				preparedStatement.setString(1, paymentSetting.getGatewayId());
				preparedStatement.setString(2, paymentSetting.getAmount());
				preparedStatement.setString(3, paymentSetting.getPercentage());
				preparedStatement.setInt(4, paymentSetting.getInstituteId());

				preparedStatement.executeUpdate();

				logger.debug("PaymentSetting inserted..Institute - "+paymentSetting.getInstituteId()+", amount - "
				+paymentSetting.getAmount()+", Percentage - "+paymentSetting.getPercentage());
			}
			else {
				logger.debug("PaymentSetting structure exist..");
				throw new PaymentSettingExistException("Payment setting already exist"+(paymentSetting.getInstituteId() != null ? " for this institute." : ".") );
			}
			flag = true;
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}


	public PaymentSetting getPaymentSettingById(Integer id) {
		PaymentSetting paymentSetting = null;
		connection = connect.getConnection();
		
		try {
			statement = connection.createStatement();
			String sql = "select * from payment_setting where  id = "+id;

			resultSet = statement.executeQuery(sql);
			if(resultSet.next()){			
				paymentSetting = new PaymentSetting();
				paymentSetting.setId(resultSet.getInt("id"));
				paymentSetting.setGatewayId(resultSet.getString("gateway_id"));
				paymentSetting.setInstituteId(resultSet.getInt("institute_id"));
				paymentSetting.setAmount(resultSet.getString("ic_amount"));
				paymentSetting.setPercentage(resultSet.getString("ic_percentage"));
				
				resultSet.close();	
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return paymentSetting;	
	}

	public boolean deletePaymentSetting(Integer id) {
		boolean flag = false;
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			// delete from paymentSetting ..
			String sql = "delete from payment_setting where id = "+id;
			statement.executeUpdate(sql);
			flag = true;
			logger.debug("PaymentSetting deleted..");
		}
		catch(SQLException e){
			logger.debug("Error deling paymentSetting..");
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}

	public PaymentSetting getDefaultPaymentSetting() {
		PaymentSetting paymentSetting = null;
		connection = connect.getConnection();
		
		try {
			statement = connection.createStatement();
			String sql = "select * from payment_setting where  institute_id is null";

			resultSet = statement.executeQuery(sql);
			if(resultSet.next()){			
				paymentSetting = new PaymentSetting();
				paymentSetting.setId(resultSet.getInt("id"));
				paymentSetting.setGatewayId(resultSet.getString("gateway_id"));
				paymentSetting.setInstituteId(resultSet.getInt("institute_id"));
				paymentSetting.setAmount(resultSet.getString("ic_amount"));
				paymentSetting.setPercentage(resultSet.getString("ic_percentage"));
				
				resultSet.close();	
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return paymentSetting;	
	}
	
	
}
