package com.qfix.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.qfix.dao.CollegeDao;
import com.qfix.model.CollegeYear;
import com.qfix.model.Course;
import com.qfix.model.University;

@Repository
public class CollegeDaoImpl extends JdbcDaoSupport implements CollegeDao{
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<University> listUniversitites() {
		String query = "SELECT * FROM college_universities ORDER BY name ASC";
		return getJdbcTemplate().query(query, new BeanPropertyRowMapper<University>(University.class));
	}

	@Override
	public List<CollegeYear> listYears() {
		String query = "SELECT * FROM college_years ORDER BY id ASC";
		return getJdbcTemplate().query(query,new BeanPropertyRowMapper<CollegeYear>(CollegeYear.class));
	}

	@Override
	public List<Course> listCourses() {
		String query = "SELECT * FROM college_courses ORDER BY name ASC";
		return getJdbcTemplate().query(query,new BeanPropertyRowMapper<Course>(Course.class));
	}

}
