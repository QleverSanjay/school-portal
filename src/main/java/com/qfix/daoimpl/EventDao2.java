package com.qfix.daoimpl;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.dao.IEventDao;
import com.qfix.exceptions.ProfileImageUploadException;
import com.qfix.model.Event;
import com.qfix.model.UserEvent;
import com.qfix.service.client.notification.Notification;
import com.qfix.service.client.notification.Sender;
import com.qfix.service.file.FileFolderService;
import com.qfix.service.file.FileFolderServiceImpl;
import com.qfix.utilities.Constants;
import com.qfix.utilities.FileConstants;
import com.qfix.utilities.FtlTemplateConstants;

@Repository
public class EventDao2 extends JdbcDaoSupport implements IEventDao {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	SimpleDateFormat sdfForSqlWithTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	Sender sender;

	
	@Autowired
	private FileFolderService fileFolderService;

	@Override
	public String saveFile(MultipartFile file, Event event, boolean isUpdate) throws IOException{
		String filePath = null;
		if(file != null ){
			String parentPath = event.getInstituteId() + File.separator + event.getBranchId() + File.separator
					 + FileConstants.EVENT_DOCUMENTS_PATH;

			String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
			String fileNameWithExtension = org.apache.commons.lang3.RandomStringUtils.random(30, true, true) + fileExtension;

			if(isUpdate){
				String imageUrl = event.getImageUrl();
				fileNameWithExtension = (StringUtils.isEmpty(imageUrl)  || imageUrl.endsWith(FileConstants.DEFAULT_IMAGE_NAME) 
						? fileNameWithExtension : imageUrl.substring(imageUrl.lastIndexOf("/") + 1));
			}
			
			filePath = fileFolderService.createFileInInternalStorage
					 (parentPath, fileNameWithExtension, file.getBytes());
			logger.debug("Event file saved");
		}

		filePath = FilenameUtils.separatorsToUnix(filePath);
		return filePath;
	}

	@Transactional
	private void updatePhotoPath(Integer id, String photoPath){
		String sql = "update event set image_url = '"+photoPath+"' where id = "+id;
		getJdbcTemplate().update(sql);
	}

	@Transactional
	private void sendEventNotifications(Event event, boolean isUpdate){

		// Create notification object to send to notification-service.
		Notification notification = new Notification();
		notification.setId(event.getId());
		notification.sendEmail(true);
		notification.sendSMS(true);
		notification.sendPushMessage(true);
		notification.setType("EVENT");
		notification.setBatchEnabled(false);

		String templateCode = isUpdate ? FtlTemplateConstants.UPDATE_EVENT_CODE : FtlTemplateConstants.CREATE_EVENT_CODE;
		notification.setTemplateCode(templateCode);

		try {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println("NOTIFICATION_OBJ:::::\n"+mapper.writeValueAsString(notification));		
			sender.sendMessage(mapper.writeValueAsString(notification));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}


	@Transactional
	@Override
	public boolean insertEvent(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException {

		Integer eventId = insertIntoEvent(event);
		event.setId(eventId);
		List<UserEvent> userEvents = new ArrayList<>();
		List<Integer> studentUserIds = new ArrayList<>();
		Map<Integer, Integer> studentEventGroupMap = new HashMap<Integer, Integer>();
		
		insertIndvidualEvents(event.getId(), event.getIndividualIdList(), userEvents, studentUserIds);
		insertGroupEvents(event, userEvents, studentUserIds, studentEventGroupMap);
		// prepare parents by studentUserIds.
		if(event.getForParent().equals("Y") || event.getForStudent().equals("Y")){
			prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, "N", true);
		}

		applyConstraintsOnUserEvents(event, userEvents);
		insertOrUpdateUserEvents(userEvents);
		
		if(file != null){
			String photoPath = saveFile(file, event, false);
			if(photoPath != null){
				logger.debug("Updating event photo path --"+photoPath);
				updatePhotoPath(event.getId(), photoPath);
			}
		}

		sendEventNotifications(event, false);

		return true;
	}

	@Transactional
	private void insertOrUpdateUserEvents(List<UserEvent> userEvents) {

		if(userEvents.size() > 0){
			for(final UserEvent userEvent : userEvents){

				if(userEvent.isInsertRecord()){
					String sql = "insert into user_event" +
						"(user_id, role_id, entity_id, event_id, event_group_id, status, for_user_id)" +
						"values("+userEvent.getUserId()+", "+userEvent.getRoleId()+", "+userEvent.getEntityId()
						+", "+userEvent.getEventId()+", "+userEvent.getEventGroupId()
						+", '" +userEvent.getStatus()+ "', "+userEvent.getForUserId()+")";

					getJdbcTemplate().update(sql);
				}
				else {
					String sql = "update user_event set status = '"+userEvent.getStatus() +"'" +
						" where user_id ="+userEvent.getUserId()+ " AND event_group_id = "+userEvent.getEventGroupId() +
						" AND for_user_id = "+userEvent.getForUserId() + 
						" AND event_id = " + userEvent.getEventId();

					getJdbcTemplate().update(sql);
				}
			}
		}
	}

	@Transactional
	private void applyConstraintsOnUserEvents(Event event,
			List<UserEvent> userEvents) {

		if(userEvents.size() > 0){
			boolean forParent = event.getForParent().equals("Y");
			boolean forTeacher = event.getForTeacher().equals("Y");
			boolean forStudent = event.getForStudent().equals("Y");

			List<UserEvent> tempUserEvents = new ArrayList<>(userEvents);

			for(UserEvent userEvent : tempUserEvents){
				int roleId = userEvent.getRoleId();

				if(roleId == Constants.ROLE_ID_PARENT){
					if(!forParent && userEvent.getUserId().equals(userEvent.getForUserId())){
						userEvents.remove(userEvent);
					}
					else if(!forStudent && !userEvent.getUserId().equals(userEvent.getForUserId())){
						userEvents.remove(userEvent);
					}
				}
				else if(roleId == Constants.ROLE_ID_STUDENT && !forStudent){
					userEvents.remove(userEvent);
				}
				else if(roleId == Constants.ROLE_ID_TEACHER && !forTeacher){
					userEvents.remove(userEvent);
				}
			}
		}
	}


	@Transactional
	private void applyConstraintsOnUserEvents(Event exEvent, Event event,
			List<UserEvent> userEvents) {

		if(userEvents.size() > 0){
			boolean forParent = event.getForParent().equals("Y");
			boolean forTeacher = event.getForTeacher().equals("Y");
			boolean forStudent = event.getForStudent().equals("Y");

			boolean exForParent = exEvent.getForParent().equals("Y");
			boolean exForTeacher = exEvent.getForTeacher().equals("Y");
			boolean exForStudent = exEvent.getForStudent().equals("Y");

			List<UserEvent> tempUserEvents = new ArrayList<>(userEvents);

			for(UserEvent userEvent : tempUserEvents){
				int roleId = userEvent.getRoleId();

				if(userEvent.isInsertRecord()){
					if(roleId == Constants.ROLE_ID_PARENT){
						if(!forParent && userEvent.getUserId().equals(userEvent.getForUserId())){
							userEvents.remove(userEvent);
						}
						else if(!forStudent && !userEvent.getUserId().equals(userEvent.getForUserId())){
							userEvents.remove(userEvent);
						}
					}
					else if(roleId == Constants.ROLE_ID_STUDENT && !forStudent){
						userEvents.remove(userEvent);
					}
					else if(roleId == Constants.ROLE_ID_TEACHER && !forTeacher){
						userEvents.remove(userEvent);
					}					
				}
				else {
					if(roleId == Constants.ROLE_ID_PARENT){
						if(!forParent && !exForParent && userEvent.getUserId().equals(userEvent.getForUserId())){
							userEvents.remove(userEvent);
						}
						else if(!forStudent && !exForStudent && !userEvent.getUserId().equals(userEvent.getForUserId())){
							userEvents.remove(userEvent);
						}
					}
					else if(roleId == Constants.ROLE_ID_STUDENT && !forStudent && !exForStudent){
						userEvents.remove(userEvent);
					}
					else if(roleId == Constants.ROLE_ID_TEACHER && !forTeacher && !exForTeacher){
						userEvents.remove(userEvent);
					}	
				}
			}
		}
	}

	@Transactional
	private void insertIndvidualEvents(Integer eventId, List<Integer> individualIds, List<UserEvent> userEvents, List<Integer> studentUserIds) {

		if(individualIds != null && !individualIds.isEmpty()){
			prepareUsersByIndividuals(eventId, individualIds, userEvents, studentUserIds, "N", true);
			for(Integer userId : individualIds){
				insertOrUpdateStudentEvent(userId, eventId, "N", true);
			}
		}
	}

	@Transactional
	private void prepareUsersByIndividuals(final Integer eventId, final List<Integer> individualIdList,
		final List<UserEvent> userEvents, final List<Integer> studentUserIds, final String status, boolean isInsert) {
		
		String individuals = org.apache.commons.lang.StringUtils.join(individualIdList, ", ");

		String sql = "select ur.*, s.id as studentId, t.id as teacherId" +
				" from user_role as ur " +
				" LEFT JOIN student as s on s.user_id = ur.user_id AND s.isDelete = 'N' AND s.active = 'Y' " +
				" LEFT JOIN teacher as t on t.user_id = ur.user_id AND t.isDelete = 'N' AND t.active = 'Y' " +
				" WHERE ur.user_id in("+individuals+")";

		getJdbcTemplate().query(sql,  new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				int roleId = resultSet.getInt("role_id");
				int userId = resultSet.getInt("user_id");
				int studentId = resultSet.getInt("studentId");
				int teacherId = resultSet.getInt("teacherId");

				if(roleId == Constants.ROLE_ID_PARENT){
					// SKIP as parent can`t be in individual list.
					// Checking exceptionally for dual role.
				}
				else {
					// insert into student Event
					if(roleId == Constants.ROLE_ID_STUDENT){
						studentUserIds.add(userId);
					}

					// add into userEventSet
					UserEvent userEvent = new UserEvent();
					userEvent.setEntityId(roleId == Constants.ROLE_ID_TEACHER ? teacherId : studentId);
					userEvent.setUserId(userId);
					userEvent.setEventId(eventId);
					userEvent.setRoleId(roleId);
					userEvent.setStatus(Constants.RECORD_ADDED);
					userEvent.isInsertRecord(true);
					userEvent.setForUserId(userId);

					if(userEvents.contains(userEvent)){
						userEvents.remove(userEvent);
					}
					userEvents.add(userEvent);	
				}
				return null;
			}
		});
	}

	@Transactional
	private void insertOrUpdateStudentEvent(int userId, Integer eventId,
			String status, boolean isInsert) {

		if(isInsert){
			String sql = "insert into student_event(user_id, event_id, status)values("+userId+", "+eventId+", '"+status+"')";
			getJdbcTemplate().update(sql);
		}else {
			String sql = "update student_event set status = '"+status+"' where user_id = "+userId+" AND event_id = "+eventId;
			getJdbcTemplate().update(sql);
		}
	}

	@Transactional
	private void insertGroupEvents(Event event, List<UserEvent> userEvents, List<Integer> studentUserIds, Map<Integer, Integer> studentEventGroupMap) {
		if(event.getGroupIdList() != null && !event.getGroupIdList().isEmpty()){
			for(Integer groupId : event.getGroupIdList()){
				int eventGroupId = insertOrUpdateEventGroup(groupId, event.getId(), "N", true);
				prepareUsersByGroupId(event.getId(), groupId, eventGroupId, userEvents, studentUserIds, studentEventGroupMap, "N", true);
			}
		}
	}

	@Transactional
	private void prepareUsersByGroupId(final Integer eventId, final Integer groupId,
			final int eventGroupId, final List<UserEvent> userEvents,
			final List<Integer> studentUserIds, final Map<Integer, Integer> studentEventGroupMap, 
			final String status, final boolean isInsert) {

		String sql = "select count(*) as count, ur.*,  s.id as studentId, p.id as parentId, t.id as teacherId" +
				" from user_role as ur " +
				" LEFT JOIN student as s on s.user_id = ur.user_id AND s.isDelete = 'N' AND s.active = 'Y'" +
				" LEFT JOIN parent as p on p.user_id = ur.user_id AND p.active = 'Y'" +
				" LEFT JOIN teacher as t on t.user_id = ur.user_id AND t.isDelete = 'N' AND t.active = 'Y' " +
				" WHERE ur.user_id in " +
				" (select gu.user_id from group_user as gu" +
				" INNER JOIN `group` as g on g.id = gu.group_id " +
				" where gu.group_id = "+groupId+" AND g.active = 'Y' AND g.isDelete = 'N' ) " +
				" group by ur.user_id";

		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
				int count = resultSet.getInt("count");
				int studentId = resultSet.getInt("studentId");
				int parentId = resultSet.getInt("parentId");
				int teacherId = resultSet.getInt("teacherId");
				int roleId = resultSet.getInt("role_id");
				int userId = resultSet.getInt("user_id");

				roleId = count > 1 ? Constants.ROLE_ID_TEACHER : roleId;
				int entityId = (roleId == Constants.ROLE_ID_STUDENT ? studentId : (count > 1 ? teacherId : parentId));
				
				if(roleId == Constants.ROLE_ID_STUDENT){
					studentUserIds.add(userId);
					studentEventGroupMap.put(userId, eventGroupId);
				}

				// add into userEventSet
				UserEvent userEvent = new UserEvent();
				userEvent.setEntityId(entityId);
				userEvent.setUserId(userId);
				userEvent.setEventGroupId(eventGroupId);
				userEvent.setEventId(eventId);
				userEvent.setRoleId(roleId);
				userEvent.setStatus(Constants.RECORD_ADDED);
				userEvent.isInsertRecord(true);
				userEvent.setForUserId(userId);

				if(userEvents.contains(userEvent)){
					userEvents.remove(userEvent);
				}
				userEvents.add(userEvent);	
				return null;
			}
		});
	}

	@Transactional
	private void prepareParentsByStudents(final Event event, final List<UserEvent> userEvents,
			final List<Integer> studentUserIds, final Map<Integer, Integer> studentEventGroupMap, 
			final String status, final boolean isInsert) {

		if(!studentUserIds.isEmpty()){
			String studentUserIdsStr = org.apache.commons.lang.StringUtils.join(studentUserIds, ", " );
			String sql = "select bsp.*, p.user_id as parentUserId, s.user_id as studentUserId" +
				" from branch_student_parent as bsp " +
				" INNER JOIN parent as p on p.id = bsp.parent_id " +
				" INNER JOIN student as s on s.id = bsp.student_id " +
				" where s.user_id in ("+studentUserIdsStr+")";

			getJdbcTemplate().query(sql, new RowMapper<Object>(){
				@Override
				public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
					int parentUserId = resultSet.getInt("parentUserId");
					int parentId = resultSet.getInt("parent_id");
					int studentUserId = resultSet.getInt("studentUserId");
					Integer eventGroupId = studentEventGroupMap.get(studentUserId);

					// add into userEvent as event for parent and for_user_id will be studentUserId
					UserEvent userEvent = new UserEvent();
					userEvent.setEntityId(parentId);
					userEvent.setUserId(parentUserId);
					userEvent.setEventId(event.getId());
					userEvent.setRoleId(Constants.ROLE_ID_PARENT);
					userEvent.setStatus(Constants.RECORD_ADDED);
					userEvent.isInsertRecord(true);
					userEvent.setForUserId(studentUserId);
					userEvent.setEventGroupId(eventGroupId);

					if(userEvents.contains(userEvent)){
						userEvents.remove(userEvent);
					}
					userEvents.add(userEvent);

					// add this as event will be visible to parent and for_user_id will be parentUserId
					userEvent = new UserEvent();
					userEvent.setEntityId(parentId);
					userEvent.setEventGroupId(eventGroupId);
					userEvent.setUserId(parentUserId);
					userEvent.setEventId(event.getId());
					userEvent.setRoleId(Constants.ROLE_ID_PARENT);
					userEvent.setStatus(Constants.RECORD_ADDED);
					userEvent.isInsertRecord(true);
					userEvent.setForUserId(parentUserId);

					if(userEvents.contains(userEvent)){
						userEvents.remove(userEvent);
					}
					userEvents.add(userEvent);	
					return null;
				}
			});
		}
	}

	@Transactional
	private int insertOrUpdateEventGroup(final int groupId, final Integer eventId,
			final String status, boolean isInsert) {

		int eventGroupId = 0;

		if(isInsert){
			String sql = "select count(*) as count from event_group where event_id = "+eventId + " AND group_id = "+groupId;
			int count = getJdbcTemplate().queryForObject(sql, Integer.class);
			if(count > 0){
				isInsert = false;
			}
		}

		if(isInsert){
			KeyHolder holder = new GeneratedKeyHolder();
			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection)
						throws SQLException {
					String sql = "insert into event_group(group_id, event_id, status)values("+groupId+", "+eventId+", '"+status+"')";
					PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					return preparedStatement;
				}
			}, holder);
			eventGroupId = holder.getKey().intValue();
		}else {
			String sql = "update event_group set status = '"+status+"' where group_id = "+groupId+" AND event_id = "+eventId;
			getJdbcTemplate().update(sql);
			sql = "select ";
		}
		return eventGroupId;
	}

	@Transactional
	private Integer insertIntoEvent( final Event event) {

		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {

				String sql = "insert into event (title, description, venue, startdate, starttime, endtime, alldayevent, " +
						"reminderSet, branch_id, enddate, reccuring, course_id, " +
						" summary, forparent, forstudent, forteacher, frequency, " +
						"by_day, by_date, created_date, is_schedule_updated, status, forvendor, sent_by ,academic_year_id"
						+(event.getReminderBefore() != null  ? ",reminderbefore" : "" )+") " +
						"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? "
						+(event.getReminderBefore() != null  ? ", ?" : "" )+")";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

				preparedStatement.setString(1, event.getTitle());
				preparedStatement.setString(2, event.getDescription());
				preparedStatement.setString(3, event.getVenue());
				preparedStatement.setString(4, event.getStartDate());
				preparedStatement.setString(5, event.getStartTime());
				preparedStatement.setString(6, event.getEndTime());
				preparedStatement.setString(7, "N");
				preparedStatement.setString(8, event.getReminderSet());
				preparedStatement.setInt(9, event.getBranchId());
				preparedStatement.setString(10, (event.getEndDate() != null ) ? (event.getEndDate()) : null);

				preparedStatement.setString(11, event.getReccuring());
				preparedStatement.setInt(12, event.getCourseId());

				preparedStatement.setString(13, event.getSummary());
				preparedStatement.setString(14, event.getForParent());
				preparedStatement.setString(15, event.getForStudent());
				preparedStatement.setString(16, event.getForTeacher());
				preparedStatement.setString(17, event.getFrequency());
				preparedStatement.setString(18, event.getByDay());
				preparedStatement.setString(19, event.getByDate());
				preparedStatement.setString(20, sdfForSqlWithTime.format(new Date()));
				preparedStatement.setString(21, "N");
				preparedStatement.setString(22, "N");
				preparedStatement.setString(23, event.getForVendor());
				preparedStatement.setInt(24, event.getSentBy());
				preparedStatement.setInt(25, event.getAcademicYearId());

				if(event.getReminderBefore() != null){
					preparedStatement.setInt(26, (event.getReminderBefore() != null ) ? event.getReminderBefore(): 0);
				}
				return preparedStatement;
			}
		}, holder);
		Integer eventId = holder.getKey().intValue();

		logger.debug("insert event Completed.."+event.getTitle() +", Id is ::: "+eventId);

		return eventId;
	}

	@Transactional
	@Override
	public boolean uploadEvent(List<Event> eventList) {
		for (Event event : eventList) {
			try {
				insertEvent(event, null);
			} catch (Exception e) {
				
			}
		}
		return false;
	}

	@Transactional
	private void updateEventGroup(Event existingEvent, Event event, List<UserEvent> userEvents){
		List<Integer> newGroupIds = new ArrayList<>();
		List<Integer> modifiedGroupIds = new ArrayList<>();
		List<Integer> deletedGroupIds = new ArrayList<>();

		for(Integer groupId : event.getGroupIdList()){
			if(existingEvent.getGroupIdList().contains(groupId)){
				modifiedGroupIds.add(groupId);
			}
			else {
				newGroupIds.add(groupId);
			}
		}

		for(Integer groupId : existingEvent.getGroupIdList()){
			if(event.getGroupIdList().contains(groupId)){
				modifiedGroupIds.add(groupId);
			}
			else {
				deletedGroupIds.add(groupId);
			}
		}
		logger.debug("New eventGroups to update --"+newGroupIds);
		logger.debug("Modified eventGroups --"+modifiedGroupIds);
		logger.debug("Deleted eventGroups --"+deletedGroupIds);
		
		List<Integer> studentUserIds = new ArrayList<>();
		Map<Integer, Integer> studentEventGroupMap = new HashMap<Integer, Integer>();

		updateGroupEvents(event.getId(), newGroupIds, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_ADDED, true);
		prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_ADDED, true);
		studentUserIds.clear();

		updateGroupEvents(event.getId(), modifiedGroupIds, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_MODIFIED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_MODIFIED, false);
		studentUserIds.clear();

		updateGroupEvents(event.getId(), deletedGroupIds, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_DELETED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_DELETED, false);
		studentUserIds.clear();
	}

	@Transactional
	private void updateGroupEvents(Integer eventId, List<Integer> groupIds, List<UserEvent> userEvents, 
			List<Integer> studentUserIds, Map<Integer, Integer> studentEventGroupMap, String status, boolean isInsert) {

		if(groupIds != null && !groupIds.isEmpty()){
			for(Integer groupId : groupIds){
				int eventGroupId = insertOrUpdateEventGroup(groupId, eventId, status, isInsert);
				prepareUsersByGroupId(eventId, groupId, eventGroupId, userEvents, studentUserIds, studentEventGroupMap, status, isInsert);
			}
		}
	}

	@Transactional
	private void updateIndividualEvent(Event existingEvent, Event event, List<UserEvent> userEvents){
		List<Integer> newIndividualIds = new ArrayList<>();
		List<Integer> modifiedIndividualIds = new ArrayList<>();
		List<Integer> deletedIndividualIds = new ArrayList<>();

		for(Integer userId : event.getIndividualIdList()){
			if(existingEvent.getIndividualIdList().contains(userId)){
				modifiedIndividualIds.add(userId);
			}
			else {
				newIndividualIds.add(userId);
			}
		}

		for(Integer userId : existingEvent.getIndividualIdList()){
			if(event.getIndividualIdList().contains(userId)){
				modifiedIndividualIds.add(userId);
			}
			else {
				deletedIndividualIds.add(userId);
			}
		}

		logger.debug("new Individuals --"+newIndividualIds);
		logger.debug("modified individuals --"+modifiedIndividualIds);
		logger.debug("deleted individuals --"+deletedIndividualIds);

		List<Integer> studentUserIds = new ArrayList<>();

		insertIndvidualEvents(event.getId(), newIndividualIds, userEvents, studentUserIds);
		prepareParentsByStudents(event, userEvents, studentUserIds, null, Constants.RECORD_ADDED, true);
		studentUserIds.clear();

		prepareUsersByIndividuals(event.getId(), modifiedIndividualIds, userEvents, studentUserIds, Constants.RECORD_MODIFIED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, null, Constants.RECORD_MODIFIED, false);
		studentUserIds.clear();

		prepareUsersByIndividuals(event.getId(), deletedIndividualIds, userEvents, studentUserIds, Constants.RECORD_DELETED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, null, Constants.RECORD_DELETED, false);
		studentUserIds.clear();
	}

/*
	private void prepareUsersByIndividualsForUpdate(Event event,
		List<UserEvent> userEvents, List<Integer> individualIds, String status) {

		if(!individualIds.isEmpty()){
			if(status.equals(Constants.RECORD_DELETED) || status.equals(Constants.RECORD_MODIFIED)){

				
			}
		}
	}*/

	@Transactional
	@Override
	public boolean updateEvent(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException {

		Event existingEvent = getEventById(event.getId());
		updateEvent(event, existingEvent);

		List<UserEvent> userEvents = new ArrayList<>();
		updateIndividualEvent(existingEvent, event, userEvents);

		// firstly update into event group.
		updateEventGroup(existingEvent, event, userEvents);
		applyConstraintsOnUserEvents(existingEvent, event, userEvents);
		insertOrUpdateUserEvents(userEvents);
		return false;
	}


	/**
	 * Used only when going to update the event.
	 * Checks whether event schedule is changed. determined by the startDate, endDate, recurring, 
	 * if recurring is weekly then by weekdays else if  recurring is yearly or monthly then byDate
	 * @param existingEvent existing event object in database.
	 * @param event new Event Object to update.
	 * @return
	 */
	private boolean checkScheduleUpdated(Event existingEvent, Event event) {

		boolean isScheduleUpdated = false;

		if(!existingEvent.getStartDate().equals(event.getStartDate())){
			isScheduleUpdated = true;
		}
		else if(existingEvent.getEndDate() != null && !existingEvent.getEndDate().equals(event.getEndDate())){
			isScheduleUpdated = true;
		}
		else if(event.getEndDate() != null && !event.getEndDate().equals(existingEvent.getEndDate())){
			isScheduleUpdated = true;
		}
		else if(!event.getReccuring().equalsIgnoreCase(existingEvent.getReccuring())){
			isScheduleUpdated = true;
		}
		else if(event.getFrequency() != null && !event.getFrequency().equalsIgnoreCase(existingEvent.getFrequency())){
			isScheduleUpdated = true;
		}
		else if(event.getFrequency() != null && event.getFrequency().equalsIgnoreCase(existingEvent.getFrequency()) && event.getFrequency().equalsIgnoreCase("WEEKLY")){
			Set<String> existingDaySet = existingEvent.getEventWeekDays().keySet();
			Set<String> newDaySet = new HashSet<String> ();
			Map<String, Boolean> eventWeekDays = event.getEventWeekDays();
			
			for(String day : event.getEventWeekDays().keySet()){
				if(eventWeekDays.get(day).equals(true)){
					newDaySet.add(day);
				}
			}
			if(!newDaySet.containsAll(existingDaySet) || !existingDaySet.containsAll(newDaySet)){
				isScheduleUpdated = true;
			}			
		}
		else if(event.getFrequency() != null && event.getFrequency().equalsIgnoreCase(existingEvent.getFrequency()) && 
				(event.getFrequency().equalsIgnoreCase("MONTHLY") || event.getFrequency().equalsIgnoreCase("YEARLY"))){
			if(!event.getByDate().equals(existingEvent.getByDate())){
				isScheduleUpdated = true;
			}
		}
		logger.debug("is Event schedule updated --"+isScheduleUpdated);
		return isScheduleUpdated;
	}


	@Transactional
	private void updateEvent(final Event event, final Event existingEvent) {
		
		final String sql = "update event set title = ? , description = ? , venue = ? , startdate = ?, starttime = ?, " +
			"endtime = ?, alldayevent = ? , reminderSet = ? , branch_id = ? ,enddate = ?," +
			" reccuring = ?, course_id = ?, " +
			"summary = ?, forparent = ?, forstudent = ?," +
			" forteacher = ?, frequency = ?, by_day = ?, by_date = ?, " +
			"updated_date = ?, is_schedule_updated = ?, academic_year_id = ?, status =?, sent_by = ? "
			+(event.getReminderBefore() != null ? ", reminderbefore = ?" : "")+" where id = ?";

		final boolean isScheduleUpdated = checkScheduleUpdated(existingEvent, event);

		logger.debug("Updating event..."+event.getId());
		logger.debug("Schedule Updated..."+isScheduleUpdated);

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, event.getTitle());
				preparedStatement.setString(2, event.getDescription());
				preparedStatement.setString(3, event.getVenue());
				preparedStatement.setString(4, event.getStartDate());
				preparedStatement.setString(5, event.getStartTime());
				preparedStatement.setString(6, event.getEndTime());
				preparedStatement.setString(7, "N");
				preparedStatement.setString(8, event.getReminderSet());
				preparedStatement.setInt(9, event.getBranchId());
				preparedStatement.setString(10, (event.getEndDate() != null ) ? (event.getEndDate()) : null);
				
				preparedStatement.setString(11, event.getReccuring());
				
				preparedStatement.setInt(12, event.getCourseId());
				preparedStatement.setString(13, event.getSummary());
				preparedStatement.setString(14, event.getForParent());
				preparedStatement.setString(15, event.getForStudent());
				preparedStatement.setString(16, event.getForTeacher());
				
				preparedStatement.setString(17, event.getFrequency());
				preparedStatement.setString(18, event.getByDay());
				preparedStatement.setString(19, event.getByDate());
				preparedStatement.setString(20, sdfForSqlWithTime.format(new Date()));
				preparedStatement.setString(21, (isScheduleUpdated ? "Y" : "N"));
				preparedStatement.setInt(22, event.getAcademicYearId());
				preparedStatement.setString(23, "M");
				preparedStatement.setInt(24, event.getSentBy());
				
				if(event.getReminderBefore() != null){
					preparedStatement.setInt(25, event.getReminderBefore());
					preparedStatement.setInt(26, event.getId());
				}
				else {
					preparedStatement.setInt(25, event.getId());
				}
				return preparedStatement;
			}
		});
	}

	@Transactional
	@Override
	public List<Event> getEventsByBranchId(Integer branchId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public List<Event> getAll(Integer instituteId) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public Event getEventById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public boolean deleteEvent(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Transactional
	@Override
	public List<Event> getTeacherEvents(Integer teacherId, Integer branchId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public List<Event> getSentEventByTeacher(Integer branchId, boolean b,
			Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Event getEventByIdToView(Integer id, Integer teacherUserId) {
		// TODO Auto-generated method stub
		return null;
	}
}