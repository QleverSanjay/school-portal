package com.qfix.daoimpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.qfix.dao.IAdmissionDao;
import com.qfix.model.Admission;
import com.qfix.service.client.notification.Notification;
import com.qfix.service.client.notification.Sender;
import com.qfix.utilities.FtlTemplateConstants;

@Repository
public class AdmissionDao extends JdbcDaoSupport implements IAdmissionDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	private Sender sender;

	@Override
	public boolean save(Admission admission ) {
		// TODO Write save code here.
		sendNotification(admission);
		return false;
	}

	private void sendNotification(Admission admission) {
		Notification notification = new Notification();
		notification.setTemplateCode(FtlTemplateConstants.ADMISSION_SUCCESS_CODE);
		Map<String, Object> subjectContent = new HashMap<String, Object>();
		subjectContent.put("institute_name", "Bharati Vidyapeeth Institute of Technology");
		subjectContent.put("address_line", "Malad(W)");
		subjectContent.put("city", "Mumbai");

		Map<String, Object> content = new HashMap<String, Object>();
		content.put("first_name", "Dilip");
		content.put("middle_name", "Digambar");
		content.put("sur_name", "Hajare");
		content.put("user_id", "AP16-17/1258951");
		content.put("password", "P0adkN");
		notification.setContent(content);
		notification.setType("EMAIL");
		notification.setTo("diliphajare10@gmail.com");
		List<String> attachments = new ArrayList<>();
		attachments.add("D:/tmp/1258951.pdf");
		notification.setAttachments(attachments); 
		notification.setSubjectContent(subjectContent);
		String json = new Gson().toJson(notification);
		sender.sendMessage(json);
	}

}
