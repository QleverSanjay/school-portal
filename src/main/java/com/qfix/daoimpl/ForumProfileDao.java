
package com.qfix.daoimpl;

import java.io.IOException;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.service.user.client.forum.RestClient;
import com.qfix.service.user.client.forum.UrlConfiguration;
import com.qfix.service.user.client.forum.request.addForum.ForumRequest;
import com.qfix.service.user.client.forum.request.addUser.UserRequest;
import com.qfix.service.user.client.forum.request.category.CategoryRequest;
import com.qfix.service.user.client.forum.request.delete.DeleteCategoryRequest;
import com.qfix.service.user.client.forum.request.delete.DeleteForumRequest;
import com.qfix.service.user.client.forum.request.delete.DeleteUserRequest;
import com.qfix.service.user.client.forum.request.linkUserForum.LinkUserForumRequest;
import com.qfix.service.user.client.forum.request.session.SessionRequest;
import com.qfix.service.user.client.forum.response.MessageResponse;
import com.qfix.service.user.client.forum.response.addUser.CreateUserResponse;
import com.qfix.service.user.client.forum.response.category.CreateCategoryResponse;
import com.qfix.service.user.client.forum.response.session.ForumResponse;
import com.qfix.service.user.client.forum.response.session.SessionResponse;

@Repository
public class ForumProfileDao   {
	final static Logger logger = Logger.getLogger(ForumProfileDao.class);
	public ForumResponse createForum(String name, Integer parent_id, String forum_desc, 
			String type, String one_way) {
		
		ForumResponse forumResponse = null;
		
		RestClient restClient = new RestClient();
		ForumRequest forumRequest = new ForumRequest();
		forumRequest.setName(name);
		forumRequest.setParent_id(parent_id);
		forumRequest.setForum_desc(forum_desc);
		forumRequest.setType(type);
		forumRequest.setSchool_id(parent_id);
		forumRequest.setOne_way(one_way);
		logger.debug("Creating forum....URL --"+UrlConfiguration.CREATE_FORUM_CONFIG);
		logger.debug("REQUEST DATA --"+forumRequest);
		try {
			ResponseEntity<String> createSessionResponse = restClient.executeRequest(forumRequest,
							UrlConfiguration.CREATE_FORUM_CONFIG);
			
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				logger.debug("Create forum response --"+createSessionResponse.getBody());
				forumResponse = mapper.readValue(createSessionResponse.getBody(), ForumResponse.class);
				logger.debug("Create forum response --"+forumResponse);
			} 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
		return forumResponse;	
	}

	public CreateCategoryResponse createCategoryAccount(String name,String forum_desc, String type) {

	CreateCategoryResponse createCategoryResponse = null;
		
		RestClient restClient = new RestClient();
		CategoryRequest categoryRequest = new CategoryRequest();
		categoryRequest.setName(name);
		categoryRequest.setForum_desc(forum_desc);
		categoryRequest.setType(type);
		logger.debug("Create Category URL --"+UrlConfiguration.CREATE_CATEGORY_CONFIG);
		logger.debug("REQUEST DATA --"+categoryRequest);
		try {
			
			//ResponseEntity<String> dummyCallForCookie = restClient
			//		.executeRequest(UrlConfiguration.DUMMY_CALL_CONFIG);
			
			ResponseEntity<String> createSessionResponse = restClient.executeRequest(categoryRequest,
					UrlConfiguration.CREATE_CATEGORY_CONFIG, "");
			logger.debug("Create Catgory RESPONSE --"+createSessionResponse);
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				
				createCategoryResponse = mapper.readValue(
						createSessionResponse.getBody(), CreateCategoryResponse.class);
				
			}
			else {
				logger.error(createSessionResponse.toString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
		return createCategoryResponse;	
	}
	
	public SessionResponse getForumSessionDetails(String username, String user_password) {
		SessionResponse sessionResponse = null;
		try {
			
			SessionRequest sessionRequest = new SessionRequest();
			sessionRequest.setUsername(username);
			sessionRequest.setUser_password(user_password);
			sessionResponse = loginForForumService(sessionRequest);
		} catch(Exception e) {
			logger.error("", e);
		}
		return sessionResponse;
	}
	
	private SessionResponse loginForForumService(SessionRequest sessionRequest)
			throws IOException {
		RestClient restClient = new RestClient();
		SessionResponse sessionResponse = null;

			ResponseEntity<String> createSessionResponse = restClient
					.executeRequest(sessionRequest,UrlConfiguration.LOGIN_FORUM_CONFIG);
			logger.debug("Login forum service URL--"+UrlConfiguration.LOGIN_FORUM_CONFIG);
			logger.debug("REQUEST DATA --"+sessionRequest);
			
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
					
				ObjectMapper mapper = new ObjectMapper();
				sessionResponse = mapper.readValue(createSessionResponse.getBody(), SessionResponse.class);
				sessionResponse.setCookie(getCookieString(createSessionResponse.getHeaders().toString()));
				logger.debug("Forum Login RESPONSE --"+sessionResponse);
			} 
		return sessionResponse;
	}

	public CreateUserResponse createUserAccount(String username, String user_password, 
			String email, Integer school_id, Integer user_gid, boolean allowAttachment, boolean isAdmin) {

		CreateUserResponse createUserResponse = null;

		RestClient restClient = new RestClient();
		UserRequest userRequest = new UserRequest();
		userRequest.setUsername(username);
		userRequest.setUser_password(user_password);
		userRequest.setEmail(RandomStringUtils.random(10, true, true) + "@eduqfix.com");
		userRequest.setSchool_id(school_id);
		userRequest.setUser_gid(user_gid);
		userRequest.setAllow_attachment((allowAttachment ? "Y" : "N"));
		userRequest.setIs_admin(isAdmin ? "Y" : "N");

		logger.debug("Create Froum user account URL --"+UrlConfiguration.CREATE_USER_CONFIG);
		logger.debug("REQUEST DATA --"+userRequest);
		try {
			ResponseEntity<String> createSessionResponse = restClient
					.executeRequest(userRequest,UrlConfiguration.CREATE_USER_CONFIG);
			logger.debug("Create Forum user RESPONSE --"+createSessionResponse.getBody());
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				createSessionResponse.getStatusCode();
				
				createUserResponse = mapper.readValue(createSessionResponse.getBody(), CreateUserResponse.class);
				logger.debug("Create Forum user RESPONSE --"+createUserResponse);
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		return createUserResponse;
	}
	
	private String getCookieString(String responseHeader) {
		String firstStr = responseHeader.substring(responseHeader.indexOf("Set-Cookie=[")+12);
		String cookieString = firstStr.substring(0, firstStr.indexOf(']'));
		logger.debug("Cookie in forum response --"+cookieString);
		//String s = cookieString.substring(0, cookieString.indexOf(";")+1);
		//s = s + " "+ cookieString.substring(cookieString, cookieString.indexOf(";")+1);
		return cookieString;
	}

	public MessageResponse linkUserToForum(long forumGroupId, Set<Integer> userIdList) {

		MessageResponse messageResponse = null;

		RestClient restClient = new RestClient();
		LinkUserForumRequest request = new LinkUserForumRequest();
		request.setGroupId(forumGroupId);
		request.setUserIdList(userIdList);
		logger.debug("Link user with forum URL --"+UrlConfiguration.LINK_USER_TO_FORUM_CONFIG);
		logger.debug("REQUEST DATA --"+request);
		try {
			ResponseEntity<String> createSessionResponse = restClient
					.executeRequest(request,UrlConfiguration.LINK_USER_TO_FORUM_CONFIG);
			
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				createSessionResponse.getStatusCode();
				
				messageResponse = mapper.readValue(createSessionResponse.getBody(),MessageResponse.class);
				logger.debug("Link USer with Forum RESPONSE --"+messageResponse);
			}
		} catch (IOException e) {
			logger.error("", e);
		}
		return messageResponse;
	}
	
	
	public MessageResponse deLinkUserToForum(long forumGroupId, Set<Integer> userIdList) {

		MessageResponse messageResponse = null;

		RestClient restClient = new RestClient();
		LinkUserForumRequest request = new LinkUserForumRequest();
		request.setGroupId(forumGroupId);
		request.setUserIdList(userIdList);
		logger.debug("Deling user from forum URL --"+UrlConfiguration.DELINK_USER_TO_FORUM_CONFIG);
		logger.debug("REQUEST DATA --"+request);
		try {
			ResponseEntity<String> createSessionResponse = restClient
					.executeRequest(request,UrlConfiguration.DELINK_USER_TO_FORUM_CONFIG);
			
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				createSessionResponse.getStatusCode();
				
				messageResponse = mapper.readValue(createSessionResponse.getBody(),MessageResponse.class);
				logger.debug("Deling user from forum RESPONSE --"+messageResponse);
			}
		} catch (IOException e) {
			logger.error("", e);
		}
		return messageResponse;
	}

	public MessageResponse deleteForum(Integer forumId) {
		MessageResponse messageResponse = null;

		RestClient restClient = new RestClient();
		DeleteForumRequest request = new DeleteForumRequest();
		request.setForumId(forumId);
		logger.debug("DELETE FORUM REQUEST ----"+request);
		try {
			ResponseEntity<String> createSessionResponse = restClient.executeRequest(
					request,UrlConfiguration.DELETE_FORUM_CONFIG);
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				if(createSessionResponse.getBody() != null){
					messageResponse = mapper.readValue(createSessionResponse.getBody(),MessageResponse.class);	
				}

				logger.debug("RESPONSE--"+messageResponse);
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		return messageResponse;		
	}

	public MessageResponse deleteCategory(Integer categoryId) {
		MessageResponse messageResponse = null;

		RestClient restClient = new RestClient();
		DeleteCategoryRequest request = new DeleteCategoryRequest();
		request.setCategoryId(categoryId);
		logger.debug("DELETE CATGEORY REQUEST ----"+request);
		try {
			ResponseEntity<String> createSessionResponse = restClient.executeRequest(
					request,UrlConfiguration.DELETE_CATEGORY_CONFIG);
			logger.debug("RESPONSE---"+createSessionResponse);
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				/*ObjectMapper mapper = new ObjectMapper();
				
				if(createSessionResponse.getBody() != null){
					messageResponse = mapper.readValue(createSessionResponse.getBody(),MessageResponse.class);	
				}*/

				logger.debug("RESPONSE--"+createSessionResponse.getBody());
			}
			else {
				logger.debug("Error while deleting branch from forum...");
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new RuntimeException();
		}
		return messageResponse;		
	}


	public MessageResponse deleteUsers(Set<Integer> userIds) {
		MessageResponse messageResponse = null;

		RestClient restClient = new RestClient();
		DeleteUserRequest request = new DeleteUserRequest();
		request.setUserIds(userIds);
		logger.debug("DELETE USER REQUEST ----"+request);
		try {
			ResponseEntity<String> createSessionResponse = restClient.executeRequest(
					request,UrlConfiguration.DELETE_USER_CONFIG);
			
			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				logger.debug("RESPONSE---"+createSessionResponse);
				if(createSessionResponse.getBody() != null){
					messageResponse = mapper.readValue(createSessionResponse.getBody(),MessageResponse.class);	
				}
				logger.debug("RESPONSE--"+messageResponse);
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		return messageResponse;		
	}
}
