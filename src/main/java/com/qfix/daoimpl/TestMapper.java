package com.qfix.daoimpl;

import java.util.Properties;
import java.util.Date;

import javax.mail.*;

import javax.mail.internet.*;

import com.sun.mail.smtp.*;

public class TestMapper {

	public static void main(String args[]) throws Exception {
	    Properties props = System.getProperties();
	    props.put("mail.smtp.host","50.56.21.178");
	    props.put("mail.smtp.auth","true");
	    props.put("mail.smtp.starttls.enable", "true");
	    Session session = Session.getInstance(props, null);
	    Message msg = new MimeMessage(session);
	    msg.setFrom(new InternetAddress("<eduqfix.com>"));
	    msg.setRecipients(Message.RecipientType.TO,
	    InternetAddress.parse("shashank.saraf@eduprojects4u.com", false));
	    msg.setSubject("Hello");
	    msg.setText("Testing some Mailgun awesomness");
	    msg.setSentDate(new Date());
	    SMTPTransport t =
	        (SMTPTransport)session.getTransport("smtp");
	    t.connect("smtp.mailgun.com", "postmaster@www.eduqfix.com", "d78de44006f84544f9c6a14f3066d34e");
	    t.sendMessage(msg, msg.getAllRecipients());
	    System.out.println("Response: " + t.getLastServerResponse());
	    t.close();
	}
}
