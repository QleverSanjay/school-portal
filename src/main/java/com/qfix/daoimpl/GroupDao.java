package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.qfix.dao.IChatDao;
import com.qfix.dao.IGroupDao;
import com.qfix.dao.IIndividualDao;
import com.qfix.model.Group;
import com.qfix.model.Individual;
import com.qfix.model.StandardDivision;
import com.qfix.model.Tag;
import com.qfix.service.user.client.chat.response.group.UpdateGroupResponse;
import com.qfix.utilities.Constants;

@Repository
public class GroupDao extends JdbcDaoSupport implements IGroupDao {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	String ownerUserName = null;
	String ownerPassword = null;
	final static Logger logger = Logger.getLogger(GroupDao.class);

	@Autowired
	IIndividualDao individualDao;

	@Autowired
	IChatDao chatDao;

	@Transactional
	public List<Group> getAll(Integer instituteId){
		List<Group> groupList = new ArrayList<Group>();

			String sql = null;
			if (instituteId != null && instituteId != 0) {
				sql = "select * from `group` where  branch_id = (select id from branch where institute_id = "
						+ instituteId + ")  AND isDelete = 'N'";
			} else {
				sql = "select * from `group` where isDelete = 'N'";
			}
			logger.debug("get all groups SQL --"+sql);
			groupList = getJdbcTemplate().query(sql, new RowMapper<Group>(){
				public Group mapRow(ResultSet rs, int rownumber)throws SQLException {
				Group group = new Group();
				group.setId(rs.getInt("id"));
				group.setName(rs.getString("name"));
				group.setDescription(rs.getString("description"));
				group.setBranchId(rs.getInt("branch_id"));
				group.setChatId(rs.getString("chat_dialogue_id"));
				group.setStatus(rs.getString("active"));
				group.setIsDelete(rs.getString("isDelete"));
				group.setIsDelete(rs.getString("is_public_group"));
				return group;
			}});
		return groupList;
	}

	@Transactional
	public boolean insertGroup(final Group group) {
		boolean flag = false;

			Set<Integer> userIds = new HashSet<Integer>();
			// insert record into group table
			logger.debug("Insert group started..."+group.getName());
			final String sql = "insert into `group` (name, description, branch_id, active, isDelete,created_by, " +
					     "is_public_group, academic_year_id) values (?, ?, ?, ?, ?, ?, ?, ?)";
			KeyHolder holder = new GeneratedKeyHolder();
			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection)
						throws SQLException {
					PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					preparedStatement.setString(1, group.getName());
					preparedStatement.setString(2, group.getDescription());
					preparedStatement.setInt(3, group.getBranchId());
					preparedStatement.setString(4, group.getStatus());
					preparedStatement.setString(5, "N");
					preparedStatement.setInt(6, group.getCreatedBy());
					preparedStatement.setString(7, group.getIsPublicGroup());
					preparedStatement.setInt(8, group.getAcademicYearId());
					return preparedStatement;
				}
			}, holder);

			Integer groupId = holder.getKey().intValue();
			group.setId(groupId);

			// Add group owner to group_user and chat
			if(!group.getOwnerRoleId().equals(Constants.ROLE_ID_ADMIN)){
				System.out.println("Adding created by ----- "+group.getCreatedBy());
				userIds.add(group.getCreatedBy());
			}

			List<Integer> schoolAdminIdList = getSchoolAdminsByBranchId(group.getBranchId());
			System.out.println("Adding school admin Id :::: "+schoolAdminIdList);
			userIds.addAll(schoolAdminIdList);

			insertByStandardDivision(group, userIds);
			insertGroupTags(group);
			insertByGroupIndividuals(group, userIds);
			insertIntoGroupUser(groupId, userIds);
			try {
				createChatGroups(group, userIds);
			}catch(Exception e){
				e.printStackTrace();
			}

			logger.debug("group insert complete...");
			flag = true;
		
		return flag;
	}

	private List<Integer> getSchoolAdminsByBranchId(Integer branchId) {
		String sql = "select user_id from branch_admin where branch_id = "+branchId;

		List<Integer> schoolAdminUserId = getJdbcTemplate().queryForList(sql, Integer.class);

		return schoolAdminUserId;
	}

	@Transactional
	private void insertByGroupIndividuals(final Group group,final Set<Integer> userIds) {

		if (group.getIndividualIdList() != null) {
			// Now insert the individuals and groups mapping into database
			for (Integer userId : group.getIndividualIdList()) {
				Integer roleId = getRoleIdBasedOnUserIdExceptParent(userId);

				String sql = "insert into group_individual (group_id, user_id, role_id) values("+group.getId()+", "+userId+", "+roleId+")";	
				getJdbcTemplate().update(sql);

				if(roleId.equals(Constants.ROLE_ID_STUDENT)){
					sql = "select s.user_id as studentUserId, p.user_id as parentUserId from student as s " +
						" LEFT JOIN branch_student_parent as bsp on bsp.student_id = s.id " +
						" LEFT JOIN parent as p on p.id = bsp.parent_id " +
					    " where s.user_id = "+ userId;

					getJdbcTemplate().query(sql , new RowMapper<String>(){
						@Override
						public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
							Integer studentUserId = resultSet.getInt("studentUserId");
							Integer parentUserId = resultSet.getInt("parentUserId");

							userIds.add(studentUserId);
							if(parentUserId != 0){
								userIds.add(parentUserId);	
							}
							return null;
						}
					});
				}
				else{
					userIds.add(userId);
				}
			}
			logger.debug("group individuals inserted...");
		}
	}

	@Transactional
	private Integer getRoleIdBasedOnUserIdExceptParent(Integer userId) {
		String sql = "select role_id from user_role where role_id != "+Constants.ROLE_ID_PARENT+" AND user_id = " + userId;

		return getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getInt("role_id");
			}
		}).get(0);
	}

	@Transactional
	private void insertGroupTags(final Group group) {
		if (group.getTags() != null) {
			// Now insert the groups and tag mapping into database
			for (final Tag tag : group.getTags()) {
				getJdbcTemplate().update(new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						String sql = "insert into group_tags (group_id, tag) values(?, ?)";
						PreparedStatement preparedStatement = connection.prepareStatement(sql);
						preparedStatement.setInt(1, group.getId());
						preparedStatement.setString(2, tag.getText().trim());
						return preparedStatement;
					}
				});
			}
			logger.debug("group tags inserted...");
		}
	}

	@Transactional
	private void insertByStandardDivision(final Group group, final Set<Integer> userIds) {
		if (group.getStandardDivision() != null) {
			// Now insert the groups and standards mapping into database
			for (final StandardDivision standardDivision : group.getStandardDivision()) {
				// insert into group_standard.
				insertIntoGroupStandard(group, standardDivision);

				// get studentUserId and parentUserId based on student`s standard and division.
				String sql = "select s.user_id as studentUserId, p.user_id as parentUserId from student as s " +
						" LEFT JOIN branch_student_parent as bsp on bsp.student_id = s.id " +
						" LEFT JOIN parent as p on p.id = bsp.parent_id " +
						" where s.branch_id = "+group.getBranchId()+" " +
						" AND s.standard_id = "+ standardDivision.getStandard() +" " +
						" AND s.division_id = "+standardDivision.getDivision();
				getJdbcTemplate().query(sql, new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
						Integer studentUserId = resultSet.getInt("studentUserId");
						Integer parentUserId = resultSet.getInt("parentUserId");

						userIds.add(studentUserId);
						if(parentUserId != null && parentUserId != 0){
							userIds.add(parentUserId);	
						}
						return null;
					}
				});
				
				// get Teachers by standard 
				sql = "select t.user_id from teacher as t " +
						" INNER JOIN teacher_standard as ts on ts.teacher_id = t.id " +
						" AND ts.standard_id = "+standardDivision.getStandard();

				getJdbcTemplate().query(sql, new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
						userIds.add(resultSet.getInt("user_id"));
						return null;
					}
				});				
			}
			logger.debug("Group standard records inserted...");
		}
	}

	@Transactional
	private void insertIntoGroupStandard(final Group group, final StandardDivision standardDivision) {

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {
				/*String sql = "insert into group_standard (group_id, standard_id, division_id) values(?, ?, ?)";*/
				String sql = "insert into group_standard (group_id, standard_id, division_id) values(?, ?, ?)";
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setInt(1, group.getId());
				preparedStatement.setInt(2, standardDivision.getStandard());
				if(standardDivision.getDivision()!=null && !standardDivision.getDivision().equals(0)){
					preparedStatement.setInt(3, standardDivision.getDivision());
				}
				else {
					preparedStatement.setNull(3, Types.NULL);
				}
				return preparedStatement;
			}
		});
	}

	@Transactional
	private void insertIntoGroupUser(Integer groupId, Set<Integer> userIds) {
		for(Integer userId : userIds){
			insertIntoGroupUser(groupId, userId);
		}
	}

	@Transactional
	private void insertIntoGroupUser(Integer groupId, Integer userId){
		if (!isUserExistInGroup(groupId, userId)) {
			String sql = "insert into group_user (group_id ,user_id) values("+groupId+", "+userId+")";
			getJdbcTemplate().update(sql);
		}
	}

	@Transactional
	private boolean isUserExistInGroup(Integer groupId, Integer userId) {
		String sql = "select count(*) as count from group_user where group_id = "
				+ groupId + " AND user_id = " + userId;
		int count = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1)throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		return count > 0;
	}

	@Transactional
	private void createChatGroups(final Group group, final Set<Integer> userIds){

		String sql = "select ucp.user_id as chatUserId, " +
				  "ucp.login as userName,ucp.password as password from `user` as u, " +
				  "user_chat_profile as ucp where u.id = ucp.user_id AND " +
				  "ucp.user_id =" +group.getCreatedBy();
		final List<Integer> userIdList = new ArrayList<>();
		userIdList.addAll(userIds);
		userIdList.remove(group.getCreatedBy());

		getJdbcTemplate().query(sql , new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				String ownerUserName = resultSet.getString("userName");
				String ownerPassword = resultSet.getString("password");

				String chatDialogId = chatDao.createChatPublicGroups(ownerUserName,ownerPassword, group.getName(), null);
				String sql = "update `group` set chat_dialogue_id = '"+chatDialogId+"' where id = "+group.getId();
				getJdbcTemplate().update(sql);
				logger.debug("chat dialog id updated for group...groupChatId --"+chatDialogId+", groupId --"+group.getId());

				String chatIds = getChatIdsByUserIds(userIdList, group.getId(), true);

				if (!StringUtils.isEmpty(chatDialogId) && !StringUtils.isEmpty(chatIds)) {
					UpdateGroupResponse updateGroupResponse = chatDao.updateChatPublicGroups( chatDialogId, chatIds, "");
					logger.debug("Update chat groups response -- "+updateGroupResponse);
				}

				return null;
			}
		});
	}

	@Transactional
	private String getChatIdsByUserIds(List<Integer> userIds, Integer groupId, boolean isAttach){
		List<String> chatIdList = null;
		String chatIds = null;
		if(userIds != null){

			String userIdsStr = StringUtils.collectionToCommaDelimitedString(userIds);

			if (!StringUtils.isEmpty(userIdsStr)) {
				String sql = "select GROUP_CONCAT(chat_user_profile_id SEPARATOR ',') as chat_profile_id " +
						" from user_chat_profile " +
						" where user_id in ("+ userIdsStr + ")";

				logger.debug("get chat ids by id SQL --"+sql);

				chatIdList = getJdbcTemplate().query(sql, new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
						return resultSet.getString("chat_profile_id");
					}
				});

				System.out.println("Chat Id List --- "+chatIdList);
				chatIds = chatIdList.get(0);

				System.out.println("Final chat Ids----"+chatIds);
			}
		}
		return chatIds;
	}

	@Transactional
	private UpdateGroupResponse updateChatGroups(Integer groupId, List<Integer> newUserIds, List<Integer> deletedUserIds){

		UpdateGroupResponse updateGroupResponse = null;	

		logger.debug("Updating chat groups...");
		String deletedChatUsers = getChatIdsByUserIds(deletedUserIds,groupId, false);
		String addedChatUsers = getChatIdsByUserIds(newUserIds, groupId, true);

		logger.debug("Newly added chat group ids --- "+addedChatUsers);
		logger.debug("Deleted chat group ids --- "+deletedChatUsers);

		String sql = "select chat_dialogue_id from `group` where id = " + groupId;

		String groupChatId = getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getString("chat_dialogue_id");
			}
		}).get(0);

		updateGroupResponse = chatDao.updateChatPublicGroups(groupChatId, addedChatUsers, deletedChatUsers);
		logger.debug("update chat group RESPONSE --"+updateGroupResponse);
		return updateGroupResponse;
	}

	@Transactional
	private boolean isGroupForumProfileExist(Integer groupId) {
		String sql = "select count(*) as count from group_forum " +
				"where group_id = "+groupId;
		int count = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		return count > 0;
	}
	
	@Transactional
	public boolean updateGroup(final Group group){
		Set<Integer> previousGroupUserSet = null;
		Set<Integer> currentGroupUserSet = new HashSet<>();

		previousGroupUserSet = getPreviousGroupUserSet(group.getId(), currentGroupUserSet);
		previousGroupUserSet = (previousGroupUserSet == null) ? new HashSet<Integer>() : previousGroupUserSet;
		System.out.println("previousGroupUserSet :::: "+previousGroupUserSet);
		logger.debug("Updating group...");

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {
				String sql = "update `group` set name = ? , description = ? , branch_id = ? , active = ?, isDelete = ?, " +
						"is_public_group = ?, academic_year_id = ? where id = ?";
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, group.getName());
				preparedStatement.setString(2, group.getDescription());
				preparedStatement.setInt(3, group.getBranchId());
				preparedStatement.setString(4, group.getStatus());
				preparedStatement.setString(5, "N");
				preparedStatement.setString(6, group.getIsPublicGroup());
				preparedStatement.setInt(7, group.getAcademicYearId());
				preparedStatement.setInt(8, group.getId());
				return preparedStatement;
			}
		});

		// delete all group users from db first.
		deleteGroupData(group);

		// Add group owner to group_user and chat
		if(!group.getOwnerRoleId().equals(Constants.ROLE_ID_ADMIN)){
			System.out.println("Adding created by ::: "+group.getCreatedBy());
			currentGroupUserSet.add(group.getCreatedBy());
		}

		List<Integer> schoolAdminIdList = getSchoolAdminsByBranchId(group.getBranchId());
		System.out.println("Adding school admin Id :::: "+schoolAdminIdList);
		currentGroupUserSet.addAll(schoolAdminIdList);

		// insert into group-standard.
		insertByStandardDivision(group, currentGroupUserSet);

		// insert into group tags.
		insertGroupTags(group);

		// insert into group-individual.
		insertByGroupIndividuals(group, currentGroupUserSet);
		insertIntoGroupUser(group.getId(), currentGroupUserSet);
		
		addDeleteUsers(currentGroupUserSet, previousGroupUserSet, group.getId());

		return true;
	}

	@Transactional
	private void deleteGroupData(final Group group) {
		// delete from group standard.
		String sql = "delete from group_standard where group_id = " + group.getId();
		getJdbcTemplate().update(sql);

		// delete from group-individual.
		sql = "delete from group_individual where group_id = "+group.getId()+" AND user_id in " +
				"(select user_id from group_user where group_id = "+group.getId()+" " +
				"AND is_suggsted_group = 'N')";
		getJdbcTemplate().update(sql);

		//delete from group-tags.
		sql = "delete from group_tags where group_id = " + group.getId();
		getJdbcTemplate().update(sql);

		// delete from groupUser.
		sql = "delete from group_user where group_id = " + group.getId()+" AND is_suggsted_group = 'N'";
		getJdbcTemplate().update(sql);
	}

	@Transactional
	private void addDeleteUsers(Set<Integer> currentGroupUserSet, 
			Set<Integer> previousGroupUserSet, Integer groupId) {

		if(!currentGroupUserSet.containsAll(previousGroupUserSet) 
				|| !previousGroupUserSet.containsAll(currentGroupUserSet)){

			List<Integer> newUserIdList = getNewGroupUserList(currentGroupUserSet, previousGroupUserSet);
			List<Integer> deletedUserIdList = getDeletedGroupUserList(currentGroupUserSet, previousGroupUserSet);
			
			try {
				updateChatGroups(groupId, newUserIdList, deletedUserIdList);
			}catch(Exception e){
				e.printStackTrace();
			}
			boolean isGroupExistInAnyForum = isGroupForumProfileExist(groupId);
			if(isGroupExistInAnyForum){
				linkOrDeLinkUsers(newUserIdList, deletedUserIdList, groupId);
			}
		}
	}

	@Transactional
	private List<Integer> getNewGroupUserList(Set<Integer> currentGroupUserSet, Set<Integer> previousGroupUserSet){
		List<Integer> newUserIdList = new ArrayList<>();
		
		if(previousGroupUserSet == null || previousGroupUserSet.size() <= 0){
			newUserIdList.addAll(currentGroupUserSet);
		}
		else {
			for(Integer userId : currentGroupUserSet){
				if(!previousGroupUserSet.contains(userId)){
					newUserIdList.add(userId);
				}
			}
		}

		return newUserIdList;
	}

	@Transactional
	private List<Integer> getDeletedGroupUserList(Set<Integer> currentGroupUserSet, Set<Integer> previousGroupUserSet){
		List<Integer> deletedUserIdList = new ArrayList<>();
		
		if(currentGroupUserSet == null || currentGroupUserSet.size() <= 0){
			deletedUserIdList.addAll(previousGroupUserSet);
		}

		else {
			for(Integer userId : previousGroupUserSet){
				if(!currentGroupUserSet.contains(userId)){
					deletedUserIdList.add(userId);
				}
			}
		}

		return deletedUserIdList;
	}

	@Transactional
	private void linkOrDeLinkUsers(List<Integer> newUserIdList, 
			List<Integer> deletedUserIdList, Integer groupId) {

		List<Integer> groupForumIdList = getGroupForumIdByGroupId(groupId);
		System.out.println("groupForumIdList to update users :::::"+groupForumIdList);
		if(groupForumIdList != null && groupForumIdList.size() > 0){
			if(newUserIdList.size() > 0){
				linkUserToForum(newUserIdList, groupForumIdList);
			}
			if(deletedUserIdList.size() > 0){
				deLinkUserToForum(deletedUserIdList, groupForumIdList);
			}	
		}
	}

	@Transactional
	private List<Integer> getGroupForumIdByGroupId(Integer groupId) {
		String sql = "select forum_group_id from group_forum where group_id = "+groupId;
		List<Integer> forumGroupIdList = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getInt("forum_group_id");
			}			
		});
		logger.debug("forum_group_ids to change as group changed --"+forumGroupIdList);
		return forumGroupIdList;
	}

	@Transactional
	private void deLinkUserToForum(List<Integer> deletedUserIdList, List<Integer> groupForumIdList) {
		Set<Integer> userForumProfileIdList = getUserForumProfileIdList(deletedUserIdList);
		if(userForumProfileIdList != null && userForumProfileIdList.size() > 0){
			ForumProfileDao forumProfileDao = new ForumProfileDao();
			for(Integer groupForumId : groupForumIdList){
				forumProfileDao.deLinkUserToForum(groupForumId, userForumProfileIdList);
			}	
		}
	}

	@Transactional
	private Set<Integer> getUserForumProfileIdList(List<Integer> userIdList) {
		final Set<Integer> userForumProfileIdSet = new HashSet<>();

		for(Integer userId : userIdList)
		{
			String sql = "select ufp.forum_profile_id, ur.role_id  " +
					"from user_forum_profile as ufp INNER JOIN user_role as ur on ufp.user_id = ur.user_id AND ufp.user_id = "+userId;

			final String parentSql = "select usf.forum_profile_id, p.user_id from parent as p, user_forum_profile as usf " +
					"where usf.user_id = p.user_id AND p.id = (select parent_id from student where user_id = "+userId+")";

			getJdbcTemplate().query(sql, new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					if(resultSet.getInt("role_id") == Constants.ROLE_ID_STUDENT){
						userForumProfileIdSet.add(resultSet.getInt("forum_profile_id"));

						getJdbcTemplate().query(parentSql, new RowMapper<String>(){
							@Override
							public String mapRow(ResultSet parentResultSet, int arg1) throws SQLException {
								userForumProfileIdSet.add(parentResultSet.getInt("forum_profile_id"));	
								return null;
							}
						});
					}
					return null;
				}
			});
		}
		logger.debug("userIdList to get forum_profile_ids --"+userIdList);
		logger.debug("Forum_profile_id list by userIdList --"+userForumProfileIdSet);
		return userForumProfileIdSet;
	}

	@Transactional
	private void linkUserToForum(List<Integer> newUserIdList, List<Integer> groupForumIdList){
		Set<Integer> userForumProfileIdList = getUserForumProfileIdList(newUserIdList);
		if(userForumProfileIdList != null && userForumProfileIdList.size() > 0){
			ForumProfileDao forumProfileDao = new ForumProfileDao();
			for(Integer groupForumId : groupForumIdList){
				forumProfileDao.linkUserToForum(groupForumId, userForumProfileIdList);
			}	
		}
	}


	@Transactional
	private Set<Integer> getPreviousGroupUserSet(Integer groupId, final Set<Integer> currentGroupUserSet) {
		String sql = "select user_id, is_suggsted_group from group_user where group_id = "+groupId;
		Set<Integer> previousGroupUserSet = new HashSet<>();

		List<Integer> userList = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				int userId = resultSet.getInt("user_id");

				if(resultSet.getString("is_suggsted_group").equals("Y")){
					currentGroupUserSet.add(userId);
				}
				return userId;
			}
		});
		previousGroupUserSet.addAll (userList);

		return previousGroupUserSet;
	}

	@Transactional
	public Group getGroupById(Integer id) {
		String sql = "select g.*, i.id as instituteId, ur.role_id " +
			" from `group` as g " +
			" INNER JOIN branch as b on b.id = g.branch_id " +
			" INNER JOIN institute as i on i.id = b.institute_id " +
			" INNER JOIN user_role as ur on ur.user_id = g.created_by " +
			" WHERE g.id = "+id +" AND ur.role_id != "+Constants.ROLE_ID_PARENT;

		Group group = getJdbcTemplate().query(sql, new RowMapper<Group>(){
			@Override
			public Group mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Group group = new Group();
				group.setId(resultSet.getInt("id"));
				group.setName(resultSet.getString("name"));
				group.setDescription(resultSet.getString("description"));
				group.setBranchId(resultSet.getInt("branch_id"));
				group.setStatus(resultSet.getString("active"));
				group.setIsDelete(resultSet.getString("isDelete"));
				group.setInstituteId(resultSet.getInt("instituteId"));
				group.setCreatedBy(resultSet.getInt("created_by"));
				group.setIsPublicGroup(resultSet.getString("is_public_group"));
				group.setAcademicYearId(resultSet.getInt("academic_year_id"));
				group.setOwnerRoleId(resultSet.getInt("role_id"));
				group.setStandardDivision(getStandardDivisionByGroupId(group.getId()));
				group.setTags(getTagsByGroupId(group.getId()));
				getIndividualsByGroupId(group);
				return group;
			}
		}).get(0);
		return group;
	}

	@Transactional
	protected List<Tag> getTagsByGroupId(Integer groupId) {
		String sql = "select gt.*, g.id from `group` as g, group_tags as gt where " +
			  "gt.group_id = g.id AND gt.group_id = "+groupId;

		List<Tag> tagList = getJdbcTemplate().query(sql, new RowMapper<Tag>(){
			@Override
			public Tag mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Tag tag = new Tag();
				tag.setText(resultSet.getString("tag"));				
				return tag;
			}
		});

		return tagList;
	}

	@Transactional
	protected void getIndividualsByGroupId(final Group group) {
		final List<Individual> individualList = new ArrayList<Individual>();
		final List<Integer> individualIdList = new ArrayList<Integer>();
		String sql = "select user_id, role_id from group_individual where group_id = " + group.getId();

		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Integer role = resultSet.getInt("role_id");
				int userId = resultSet.getInt("user_id");

				if (role.equals(Constants.ROLE_ID_STUDENT)) {
					individualList.add(individualDao.getIndividualFromStudent(userId));
					individualIdList.add(userId);
				}
				/*else if (role.equals(Constants.ROLE_ID_PARENT)) {
					individualList.add(individualDao.getIndividualFromParent(userId));
					individualIdList.add(userId);
				}*/
				else if (role.equals(Constants.ROLE_ID_TEACHER)) {
					individualList.add(individualDao.getIndividualFromTeacher(userId));
					individualIdList.add(userId);
				}
				else if(role.equals(Constants.ROLE_ID_VENDOR)){
					individualList.add(individualDao.getIndividualFromVendor(userId));
					individualIdList.add(userId);
				}
				group.setIndividualIdList(individualIdList);
				group.setIndividuals(individualList);
				return null;
			}
		});
	}

	@Transactional
	private List<StandardDivision> getStandardDivisionByGroupId(Integer groupId) {
		String sql = "select g.*, s.displayName as standardName, d.displayName as divisionName from " +
				"group_standard as g, standard as s, division as d where g.standard_id = s.id " +
				"AND g.division_id = d.id AND g.group_id = "+ groupId;

		List<StandardDivision> standardDivisionList = getJdbcTemplate().query(sql, new RowMapper<StandardDivision>(){
			@Override
			public StandardDivision mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				StandardDivision stdDiv = new StandardDivision();
				stdDiv.setIds(resultSet.getInt("id"));
				stdDiv.setStandard(resultSet.getInt("standard_id"));
				stdDiv.setDivision(resultSet.getInt("division_id"));
				stdDiv.setDivisionName(resultSet.getString("divisionName"));
				stdDiv.setStandardName(resultSet.getString("standardName"));
				return stdDiv;
			}
		});
		return standardDivisionList;
	}

	@Transactional
	public boolean deleteGroup(Integer id) {
		// TODO delete from chat also...

		String sql = "update `group` set isDelete = 'Y' where id = " + id;
		getJdbcTemplate().update(sql);

		logger.debug("Group deleted successfully..."+id);

		return true;
	}

	@Transactional
	public List<Group> getGroupsByBranchId(Integer branchId) {
		String sql = "select * from `group` where isDelete = 'N' AND branch_id = "+branchId+ " AND " +
						 "academic_year_id = (select id from academic_year where " +
						 "is_current_active_year = 'Y' and branch_id = "+branchId+")";

		List<Group> groupList = getJdbcTemplate().query(sql, new RowMapper<Group>(){
			@Override
			public Group mapRow(ResultSet resultSet, int arg1)throws SQLException {
				Group group = new Group();
				group.setId(resultSet.getInt("id"));
				group.setName(resultSet.getString("name"));
				group.setDescription(resultSet.getString("description"));
				group.setBranchId(resultSet.getInt("branch_id"));
				group.setChatId(resultSet.getString("chat_dialogue_id"));
				group.setStatus(resultSet.getString("active"));
				group.setIsDelete(resultSet.getString("isDelete"));
				group.setIsPublicGroup(resultSet.getString("is_public_group"));
				group.setAcademicYearId(resultSet.getInt("academic_year_id"));
				return group;
			}
		});
		return groupList;
	}


	@Override
	public List<Group> getGroupsByTeacherUserId(Integer teacherUserId) {
		String sql = "select * from `group` where isDelete = 'N' AND created_by = "+teacherUserId;

		List<Group> groupList = getJdbcTemplate().query(sql, new RowMapper<Group>(){
			@Override
			public Group mapRow(ResultSet resultSet, int arg1)throws SQLException {
				Group group = new Group();
				group.setId(resultSet.getInt("id"));
				group.setName(resultSet.getString("name"));
				group.setDescription(resultSet.getString("description"));
				group.setBranchId(resultSet.getInt("branch_id"));
				group.setChatId(resultSet.getString("chat_dialogue_id"));
				group.setStatus(resultSet.getString("active"));
				group.setIsDelete(resultSet.getString("isDelete"));
				group.setIsPublicGroup(resultSet.getString("is_public_group"));
				group.setAcademicYearId(resultSet.getInt("academic_year_id"));
				return group;
			}
		});
		return groupList;
	}
}
