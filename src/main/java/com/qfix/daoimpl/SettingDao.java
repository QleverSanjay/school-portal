package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.qfix.dao.ISettingsDao;
import com.qfix.model.EmailSetting;
import com.qfix.model.SMSSetting;

@Repository
public class SettingDao extends JdbcDaoSupport implements ISettingsDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(SettingDao.class);

	@Transactional
	public void sendEmail(String emailOfReciever, String subject, String messageBody){
		
		String sql = "select * from email_config";
		
		EmailSetting emailSetting = getJdbcTemplate().queryForObject(sql, new RowMapper<EmailSetting>(){
			@Override
			public EmailSetting mapRow(ResultSet resultSet, int rowNum)
					throws SQLException {
				EmailSetting emailSetting = new EmailSetting();
				emailSetting.setUsername(resultSet.getString("username"));
				emailSetting.setActive(resultSet.getString("active"));
				emailSetting.setAuthenticationMethod(resultSet.getString("authenticationMethod"));
				emailSetting.setConnectionType(resultSet.getString("connectionType"));
				emailSetting.setId(resultSet.getInt("id"));
				emailSetting.setPort(resultSet.getString("port"));
				emailSetting.setSecurity(resultSet.getString("security"));
				emailSetting.setServerName(resultSet.getString("serverName"));
				return emailSetting;
			}
			
		});
		
		sendEmail(emailSetting, emailOfReciever, subject, messageBody);
	}
	
	private void sendEmail(EmailSetting emailSetting, String emailOfReciever, String subject, String messageBody) {

	// Do G-mail Security Setting from here -- https://www.google.com/settings/security/lesssecureapps  --turn it on
		
		final String username = emailSetting.getUsername();
		final String password = "qfix@123";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailOfReciever));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailOfReciever));
			message.setSubject("From : [" + username + "] : " + subject);
			message.setText(messageBody);

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	/*public void sendSMS(String contactNumber, String message){
		try {
			connection = connect.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from email_config");
			if(resultSet.next()){
				SMSSetting smsSetting = new SMSSetting();
				smsSetting.setUsername(resultSet.getString("username"));
				smsSetting.setActive(resultSet.getString("active"));
				smsSetting.setPassword(resultSet.getString("password"));
				smsSetting.setVendorUrl(resultSet.getString("vendorUrl"));
				smsSetting.setFrom(resultSet.getString("from"));
				sendSMS(smsSetting, contactNumber, message);
				logger.debug("SMS sent to --"+contactNumber +", message --"+message);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
	}*/

	/*private String sendSMS(SMSSetting smsSetting, String contactNumber, String msg) {
		
		String rsp = "";
		try {
			// Construct The Post Data
			String data = URLEncoder.encode("user", "UTF-8") + "="
					+ URLEncoder.encode(smsSetting.getUsername(), "UTF-8");
			data += "&" + URLEncoder.encode("password", "UTF-8") + "="
					+ URLEncoder.encode(smsSetting.getPassword(), "UTF-8");
			data += "&" + URLEncoder.encode("msisdn", "UTF-8") + "="
					+ URLEncoder.encode(contactNumber, "UTF-8");
			data += "&" + URLEncoder.encode("msg", "UTF-8") + "="
					+ URLEncoder.encode(msg, "UTF-8");
			data += "&" + URLEncoder.encode("sid", "UTF-8") + "="
					+ URLEncoder.encode(smsSetting.getSid(), "UTF-8");
			data += "&" + URLEncoder.encode("fl", "UTF-8") + "="
					+ URLEncoder.encode((smsSetting.getFlashMessage() != null && smsSetting.getFlashMessage().equals("Y") ? "1" : "0"), "UTF-8");
			// Push the HTTP Request
			URL url = new URL(smsSetting.getVendorUrl());
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			wr.write(data);
			
			wr.flush();
			// Read The Response
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				// Process line...
				rsp += line;
			}
			wr.close();
			rd.close();
			

		} catch (Exception e) {
			rsp = e.toString();
		}
		
		
		return rsp;
	}*/
	
	@Transactional
	public boolean updateSmsSetting(SMSSetting smsSetting){
		boolean flag = false;
		String sql = "update sms_config set vendorUrl = ?, username = ?, password = ?, from = ?, active = ?";
		Object[] params = {
				smsSetting.getVendorUrl(),
				smsSetting.getUsername(),
				smsSetting.getPassword(),
				smsSetting.getFrom(),
				smsSetting.getActive()
		};
		int[] types = {
			Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR
		};
		
		getJdbcTemplate().update(sql, params, types);
		flag = true;
		logger.debug("SMS settings updated...");
		return flag;
	}
	
	@Transactional
	public boolean updateEmailSetting(EmailSetting emailSetting){
		boolean flag = false;
		String sql = "update email_config set serverName = ?, port = ?, connectionType = ?, security = ?, " +
				"authenticationMethod = ?, username = ?, active = ?, password = ?";
		
		Object[] params = {
				emailSetting.getServerName(),
				emailSetting.getPort(),
				emailSetting.getConnectionType(),
				emailSetting.getSecurity(),
				emailSetting.getAuthenticationMethod(),
				emailSetting.getUsername(),
				emailSetting.getActive(),
				emailSetting.getPassword()
		};
		int[] types = {
			Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR
		};
		
		getJdbcTemplate().update(sql, params, types);
		
		flag = true;
		logger.debug("Email settings updated...");
		return flag;
	}

	@Transactional
	public EmailSetting getEmailSetting() {
		String sql = "select * from email_config";
		return getJdbcTemplate().queryForObject(sql, new RowMapper<EmailSetting>(){
			@Override
			public EmailSetting mapRow(ResultSet resultSet, int rowNum)
					throws SQLException {
				EmailSetting emailSetting = new EmailSetting();
				emailSetting.setActive(resultSet.getString("active"));
				emailSetting.setId(resultSet.getInt("id"));
				emailSetting.setAuthenticationMethod(resultSet.getString("authenticationMethod"));
				emailSetting.setConnectionType(resultSet.getString("connectionType"));
				emailSetting.setPort(resultSet.getString("port"));
				emailSetting.setSecurity(resultSet.getString("security"));
				emailSetting.setServerName(resultSet.getString("serverName"));
				emailSetting.setUsername(resultSet.getString("username"));
				emailSetting.setPassword(resultSet.getString("password"));
				return emailSetting;
			}
		});
	}

	public SMSSetting getSmsSetting() {
		String sql = "select * from sms_config";
		
		return getJdbcTemplate().queryForObject(sql, new RowMapper<SMSSetting>(){
			@Override
			public SMSSetting mapRow(ResultSet resultSet, int rowNum)
					throws SQLException {
				SMSSetting smsSetting = new SMSSetting();
				smsSetting.setActive(resultSet.getString("active"));
				smsSetting.setId(resultSet.getInt("id"));
				smsSetting.setFrom(resultSet.getString("from"));
				smsSetting.setVendorUrl(resultSet.getString("vendorUrl"));
				smsSetting.setPassword(resultSet.getString("password"));
				smsSetting.setUsername(resultSet.getString("username"));
				return smsSetting;
			}
		});
	}
}
