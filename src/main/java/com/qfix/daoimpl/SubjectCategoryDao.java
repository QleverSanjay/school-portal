package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.qfix.dao.ISubjectCategoryDao;
import com.qfix.model.SubjectCategory;

@Repository
public class SubjectCategoryDao extends JdbcDaoSupport implements ISubjectCategoryDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean uploadSubjectCategory(List<SubjectCategory> subjectCategoryList, Integer branchId) {
		for(SubjectCategory subjectCategory : subjectCategoryList){
			insertSubjectCategory(subjectCategory);
		}
		return true;
	}

	@Override
	public List<SubjectCategory> getSubjectCategorysByBranchId(Integer branchId) {
		String sql = "select * from subjectCategory where branch_id = "+branchId;

		List<SubjectCategory> subjectCategoryList = getJdbcTemplate().query(sql, new RowMapper<SubjectCategory>(){
			@Override
			public SubjectCategory mapRow(ResultSet resultSet, int arg1) throws SQLException {
				SubjectCategory subjectCategory = new SubjectCategory();
				subjectCategory.setId(resultSet.getInt("id"));
				subjectCategory.setName(resultSet.getString("name"));
				return subjectCategory;
			}
		} );
		return subjectCategoryList;
	}


	@Override
	public boolean updateSubjectCategory(SubjectCategory subjectCategory) {
		/*String sql = "update subjectCategory set branch_id = "+subjectCategory.getBranchId()+" " +
			"set name = '"+subjectCategory.getName()+"' where id = "+subjectCategory.getId();
		getJdbcTemplate().update(sql);
*/
		return true;
	}

	@Override
	public boolean insertSubjectCategory(SubjectCategory subjectCategory) {
		/*String sql = "insert into subject_category (name, is_delete) " +
				"values (" 
				+"'"+subjectCategory.getName()+"', "+subjectCategory.getBranchId()+", 'N')";
		getJdbcTemplate().update(sql);
*/
		return false;
	}

	@Override
	public SubjectCategory getSubjectCategoryById(Integer id) {
		String sql = "select * from subject_category where id = "+id;
		SubjectCategory subjectCategory = getJdbcTemplate().queryForObject(sql, SubjectCategory.class);
		return subjectCategory;
	}

	@Override
	public boolean deleteSubjectCategory(Integer id) {
		String sql = "update subject_category set is_delete = 'Y' where id = "+id;
		getJdbcTemplate().update(sql);
		return false;
	}
	
}
