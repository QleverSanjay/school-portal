package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qfix.model.Currency;
import com.qfix.model.Division;
import com.qfix.model.Master;
import com.qfix.model.Standard;

@Service
public class MasterDao extends JdbcDaoSupport{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	private Connectivity connect;

	final static Logger logger = Logger.getLogger(MasterDao.class);

	public List<Master> getAllHead(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from head where active = 'Y'";

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	public List<Master> getAllBoard(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from board";
			             
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getAllFeesCode(Integer branchId){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try{
			statement  = connection.createStatement();
			String sql = " select * from fees_code_configuration " +
					     " where is_delete = 'N' and branch_id = " + branchId;
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}
		catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllSubjects(Integer branchId){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from subject where is_delete = 'N' AND branch_id = "+branchId;

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getSubjectsByStandard(Integer standardId){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from subject where is_delete = 'N' AND id in " +
					"(select subject_id from standard_subject where standard_id = "+standardId+")";

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getFeesByHead(Integer headId){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();

			String sql = "select * from fees where head_id = "+headId;

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("description"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getAllBranch(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from branch where active = 'Y'";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getAllInstitute(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from institute where active = 'Y' AND isDelete = 'N'";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getAllCaste(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from caste where active = 'Y'";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getRelations(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from relation where active = 'Y'";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllReligion(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from religion where active = 'Y'";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getAllProfessions(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master>  masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try{
			statement = connection.createStatement();

			String sql = "select * from profession";

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}
		catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllIncomes(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master>  masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try{
			statement = connection.createStatement();
			String sql = "select * from income_range";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}
		catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllState(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from state where active = 'Y' order by name";
						
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllDistrict(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from district where active = 'Y'";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getDistrictByState(int stateId){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from district where active = 'Y' AND state_id = "+stateId+" order by name";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllTaluka(){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from taluka where active = 'Y'";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getTalukasByDistrict(int districtId){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from taluka where active = 'Y' and district_id = "+districtId+" order by name";

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	@Transactional
	public List<Master> getTalukaByState(Integer stateId){
		String query = "SELECT t.* FROM taluka t JOIN district d on t.district_id=d.id JOIN state s ON s.id = d.state_id WHERE s.id=? ORDER BY t.name ASC";
		
		Object[] params = new Object[]{stateId};
		
		return getJdbcTemplate().query(query, params,new BeanPropertyRowMapper(Master.class));
	}
	
	public List<Master> getFilterdBranch(Integer id) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select count(*) as count from institute where id = "+id+" AND active = 'Y'";
			resultSet = statement.executeQuery(sql);
			if(resultSet.next()){
				if(resultSet.getInt("count") > 0){
					resultSet.close();
					sql = "select * from branch where active = 'Y' AND is_delete = 'N' and institute_id = "+id;
					resultSet = statement.executeQuery(sql);
					while(resultSet.next()){
						Master master = new Master();
						master.setId(resultSet.getInt("id"));
						master.setName(resultSet.getString("name"));
						masterList.add(master);
					}
				}
			}		
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getBranchByInstitute(Integer instituteId) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from branch where active = 'Y' "
					+(instituteId != null ? "and institute_id = "+instituteId :"");
			
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getAllGroup() {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from `group` where active = 'Y' AND isDelete = 'N'";
			
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllResultTypes() {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select * from student_result_type";
			
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	// TODO Never used....
	public List<Master> getGroupsByBranch(Integer id) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select * from `group` where active = 'Y' AND isDelete = 'N' AND branch_id = "+id;
			
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	/**
	 * get groupList by name and branchId by ignoring already selected groups ids
	 * @param branchId 
	 * @param name
	 * @param allreadtSelectedIds
	 * @return
	 */
	public List<Master> getGroupsByBranchAndName(Integer branchId, String name, List<Integer> allreadtSelectedIds) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		allreadtSelectedIds = (allreadtSelectedIds == null ? new ArrayList<Integer>() : allreadtSelectedIds);
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select * from `group` where active = 'Y' AND isDelete = 'N' AND name like '%"+name+"%' AND branch_id = "+branchId;

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				if(!allreadtSelectedIds.contains(resultSet.getInt("id"))){
					Master master = new Master();
					master.setId(resultSet.getInt("id"));
					master.setName(resultSet.getString("name"));
					masterList.add(master);	
				}
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	
	public List<Master> getAllSearchInstitute(String name,List<Integer> allreadySelectedIds){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			
			String sql = "select i.id as instituteId, i.name as instituteName, b.id as branchId, b.name as branchName " +
					"from institute as i, branch as b where (i.name like '%"+name+"%') AND " +
							"i.id = b.institute_id";
			
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				if(!allreadySelectedIds.contains(resultSet.getInt("branchId"))){
					Master master = new Master();
					master.setId(resultSet.getInt("instituteId"));
					master.setName(resultSet.getString("instituteName"));
					master.setBranchId(resultSet.getInt("branchId"));
					master.setBranchName(resultSet.getString("branchName"));
					masterList.add(master);
				}
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Master> getInstituteFromVendor(Integer id) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select iv.*,i.name as instituteName, b.name as branchName,i.id as instituteId from institute as i,branch as b, branch_vendor as iv where i.id = b.institute_id AND  b.id = iv.branch_id AND user_id = "+id+" group by b.id";
			ResultSet resultSet1 = statement.executeQuery(sql);
			while(resultSet1.next()){
				Master master = new Master();
				master.setId(resultSet1.getInt("instituteId"));
				master.setName(resultSet1.getString("instituteName"));
				master.setBranchId(resultSet1.getInt("branch_id"));
				master.setBranchName(resultSet1.getString("branchName"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Standard> getFilterdStandards(Integer branchId) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Standard> masterList = new ArrayList<Standard>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select * from `standard` where active = 'Y' AND branch_id = "+branchId +" " +
					" AND academic_year_id = (select id from academic_year where is_current_active_year = 'Y' AND branch_id = "+branchId+" )";
			resultSet = statement.executeQuery(sql);
			PreparedStatement divisionPreparedStatement = connection.prepareStatement("select * from `division` where active = 'Y' AND id in " +
					"(select division_id from standard_division where standard_id = ?)" +
					" AND academic_year_id = (select id from academic_year where is_current_active_year = 'Y' AND branch_id = "+branchId+" )");

			while(resultSet.next()){
				Standard standard = new Standard();
				standard.setId(resultSet.getInt("id"));
				standard.setDisplayName(resultSet.getString("displayName"));
				
				divisionPreparedStatement.setInt(1, standard.getId());
				ResultSet divisionResultSet = divisionPreparedStatement.executeQuery();
				List<Division> divisionList = new ArrayList<>();

				while(divisionResultSet.next()){
					Division division = new Division();
					division.setId(divisionResultSet.getInt("id"));
					division.setDisplayName(divisionResultSet.getString("displayName"));
					division.setStandardId(standard.getId());
					divisionList.add(division);
				}
				standard.setDivisionList(divisionList);
				masterList.add(standard);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	

	public List<Standard> getFilterdStandardsByTeacher(Integer teacherUserId,Integer branchId) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Standard> masterList = new ArrayList<Standard>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = " select ts.*, s.displayName standardName, d.displayName as divisionName from teacher_standard as ts"+
					" INNER JOIN standard as s on s.id = ts.standard_id AND s.active = 'Y'"+
					" INNER JOIN division as d on d.id = ts.division_id AND d.active = 'Y'"+
					" INNER JOIN teacher as t on t.id = ts.teacher_id"+
					" INNER JOIN user as u on t.user_id = u.id AND u.id = "+teacherUserId+
					" WHERE ts.is_delete = 'N' group by ts.standard_id, ts.division_id order by ts.standard_id ";
			resultSet = statement.executeQuery(sql);
			List<Division> divisionList = new ArrayList<>();
			Standard standard = new Standard();
			standard.setId(0);
			while(resultSet.next()){
				boolean isNew = false;
				if(standard.getId() != resultSet.getInt("standard_id")){
					standard = new Standard();
					standard.setId(0);
					divisionList = new ArrayList<>();
					isNew = true;
				}

				if(standard.getId() == 0){
					standard.setId(resultSet.getInt("standard_id"));
					standard.setDisplayName(resultSet.getString("standardName"));	
				}

				Division division = new Division();
				division.setId(resultSet.getInt("division_id"));
				division.setDisplayName(resultSet.getString("divisionName"));
				division.setStandardId(standard.getId());
				divisionList.add(division);
				standard.setDivisionList(divisionList);

				if(isNew){
					masterList.add(standard);	
				}
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	/*public List<Master> getFilterdDivisions(Integer standardId) {
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select * from `division` where active = 'Y' AND id in " +
					"(select division_id from standard_division where standard_id = "+standardId+")";
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("displayName"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}*/
	
	public List<List<String>> getAddressSelectionData() {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<List<String>> masterList = new ArrayList<>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select s.name as state, d.name as district, t.name as taluka from state s left join  district d on d.state_id = s.id left join taluka t on t.district_id = d.id where s.active = 'Y' and d.active ='Y' and t.active = 'Y' order by state, district, taluka";
			resultSet = statement.executeQuery(sql);

			List<String> list = new ArrayList<String>();
			while (resultSet.next()) {
				list = new ArrayList<>();
				list.add(resultSet.getString("state"));
				list.add(resultSet.getString("district"));
				list.add(resultSet.getString("taluka"));

				masterList.add(list);
			}
			
		}catch(SQLException e){
			logger.error("", e);
			e.printStackTrace();
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		
		return masterList;
	}

	public List<List<String>> getStandardSelectionData(int branchId) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<List<String>> masterList = new ArrayList<>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select s.displayName as standard, d.displayName as division " +
					" from standard s LEFT JOIN standard_division sd on sd.standard_id = s.id" +
					" LEFT JOIN division as d on sd.division_id = d.id and d.active ='Y' AND d.branch_id = "+ branchId + 
					" where s.active = 'Y' " +
					" and s.academic_year_id = (select id from academic_year where is_current_active_year = 'Y' and branch_id = "+branchId+")"+
					" order by standard, division";

			System.out.println("getStandardSelectionData :::::::\n "+sql);
			resultSet = statement.executeQuery(sql);

			List<String> list = new ArrayList<String>();
			while (resultSet.next()) {
				list = new ArrayList<>();
				list.add(resultSet.getString("standard"));
				if(resultSet.getString("division") != null){
					list.add(resultSet.getString("division"));
				}

				masterList.add(list);
			}
			
		}catch(SQLException e){
			logger.error("", e);
			e.printStackTrace();
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		
		return masterList;
	}

	public List<List<String>> getStandardBasedSubjectSelectionData(int branchId) {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<List<String>> masterList = new ArrayList<>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select s.displayName as standard, su.name as subject " +
					" from standard s " +
					" INNER JOIN standard_subject ssu on s.id = ssu.standard_id " +
					" INNER JOIN subject su  on su.id = ssu.subject_id " +
					" where s.branch_id ="+branchId+" and s.active = 'Y' " +
					" and su.is_delete ='N'  and ssu.is_delete = 'N' order by s.displayName , su.name";

			resultSet = statement.executeQuery(sql);

			List<String> list = new ArrayList<String>();
			while (resultSet.next()) {
				list = new ArrayList<>();
				list.add(resultSet.getString("standard"));
				list.add(resultSet.getString("subject"));

				masterList.add(list);
			}
			
		}catch(SQLException e){
			logger.error("", e);
			e.printStackTrace();
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		
		return masterList;
	}

	public List<Master> getUserBasedBranchList(Integer userId){
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select b.id, b.name from branch as b " +
					" INNER JOIN branch_admin as ba on ba.branch_id = b.id AND ba.is_delete = 'N' " +
					" WHERE ba.user_id = "+userId +" AND b.active = 'Y' AND b.is_delete = 'N' ";

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}
	public List<Master> getAllBoards() {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Master> masterList = new ArrayList<Master>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select * from board";

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));
				masterList.add(master);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return masterList;
	}

	public List<Currency> getAllCurrencies() {
		Connection connection  = null;
		Statement statement  = null;
		ResultSet resultSet  = null;
		List<Currency> currencyList = new ArrayList<Currency>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "select * from currency";

			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Currency currency = new Currency();
				currency.setCode(resultSet.getString("code"));
				currency.setDescription(resultSet.getString("description"));
				currencyList.add(currency);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return currencyList;
	}
}
