package com.qfix.daoimpl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.exceptions.InvalidCredintialsException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.ForgotPasswordModel;
import com.qfix.model.MenuGroupItem;
import com.qfix.model.MenuGroup;
import com.qfix.model.MenuGroups;
import com.qfix.model.RoleMenuPermissions;
import com.qfix.model.ShoppingProfile;
import com.qfix.model.User;
import com.qfix.service.client.notification.Sender;
import com.qfix.service.user.UrlConfiguration;
import com.qfix.service.user.client.shopping.response.session.ShoppingSessionResponse;
import com.qfix.utilities.Constants;
import com.qfix.utilities.SecurityUtil;

@Repository
public class UserDao extends JdbcDaoSupport {//implements UserDetailsService {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	Sender sender;

	final static Logger logger = Logger.getLogger(UserDao.class);
	private Statement statement;
	private ResultSet resultSet ;

	@Autowired
	private Connectivity connect;

	@Autowired
	private ShoppingDao shoppingDao;
	private Connection connection;
	private PreparedStatement preparedStatement;


	public ShoppingProfile getShoppingProfile(User user){
		ShoppingSessionResponse sessionResponse = new ShoppingSessionResponse();
		sessionResponse = shoppingDao.getShoppingSessionDetails(user.getUsername(), user.getPassword());

		if(sessionResponse != null && sessionResponse.getSuccessResponse() != null) {
		
		}
		return null;
	}

	//TODO CHANGE PASSWORD FOR ENCRYPTION
	private void setUserInstitute(final User user) throws UnauthorizedException{
		String query = "SELECT id,institute_id " +
			" from user where username = '"+user.getUsername()+"'  " +
			" AND active = 'Y' AND isDelete = 'N'";

		try{
			getJdbcTemplate().queryForObject(query, new RowMapper<User>() {
				public User mapRow(ResultSet rs, int rownumber) throws SQLException {
					User userObj = user;
					userObj.setId(rs.getInt("id"));
					userObj.setInstituteId(rs.getInt("institute_id"));
					return userObj;
				}});
		}catch(EmptyResultDataAccessException e){
			throw  new UnauthorizedException("User is not active or deleted.");
		}
	}

	private void setUserRoles(User user) throws UnauthorizedException{
		String query = "select role.name as roleName from role, user_role where role.id = user_role.role_id " +
				"AND user_role.user_id = "+user.getId();
		List<String> data = getJdbcTemplate().query(query, new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString(1);
            }
		});
		if(data != null){
			Integer roleId = null;
			if(data.contains(Constants.ROLE_ADMIN)){
				roleId = Constants.ROLE_ID_ADMIN;
			}
			else if(data.contains(Constants.ROLE_SCHOOL_ADMIN)){
				roleId = Constants.ROLE_ID_SCHOOL_ADMIN;
			}
			else if(data.contains(Constants.ROLE_TEACHER)){
				roleId = Constants.ROLE_ID_TEACHER;
			}
			else if(data.contains(Constants.ROLE_STAFF)){
				roleId = Constants.ROLE_ID_STAFF;
			}else if(data.contains(Constants.ROLE_BANK_PARTNER)){
				roleId = Constants.ROLE_ID_BANK_PARTNER;
			}else {
				throw new UnauthorizedException("You are unauthorized to access this page !");
			}
			user.setRoleId(roleId);
			String [] roles = data.toArray(new String[data.size()]);
			user.setRoles(roles);
			logger.debug("User roles -- "+data);
        }
	}

	private void setUserInstituteInfo(final User user){
		if(user.getInstituteId() != null){
			String query = null;

			if(user.getBranchIdList().size() > 0){
				query = "select * from institute where id = (select institute_id from branch where id = " + user.getBranchIdList().get(0)+")";
			}
			else {
				query = "select * from institute where id = " + user.getInstituteId();
			}

			getJdbcTemplate().queryForObject(query, new RowMapper<User>() {
				public User mapRow(ResultSet rs, int rownumber) throws SQLException {
					User userObj = user;
					userObj.setInstituteLogoUrl(rs.getString("logo_url"));
					userObj.setInstituteName(rs.getString("name"));
					return userObj;
				}
			});
			
			query = "select count(*) as count from user where id = "+user.getId() + " AND institute_id is null";
			Integer count = getJdbcTemplate().queryForObject(query, Integer.class);
			if(count > 0){
				user.setBRanchAdmin(true);
			}
		}
	}
	
	private void setUserTeacherInfo(final User user){
		if(user.getId() != null){
			String query = "select * from teacher where user_id = "+user.getId();
			
			getJdbcTemplate().queryForObject(query, new RowMapper<User>() {
				public User mapRow(ResultSet rs, int rownumber)
						throws SQLException {
					User userObj = user;
					userObj.setBranchId(rs.getInt("branch_id"));
					userObj.setEntityId(rs.getInt("id"));
					return userObj;
				}});
			
			if(user.getBranchId() != null){
				query = "select * from institute where id = (select institute_id from branch where id = "+user.getBranchId() +')';
				getJdbcTemplate().queryForObject(query, new RowMapper<User>() {
					public User mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						User userObj = user;
						userObj.setInstituteLogoUrl(rs.getString("logo_url"));
						userObj.setInstituteName(rs.getString("name"));
						return userObj;
					}});
			}
		}
	}

	private void setUserVendorInfo(final User user){
		if(user.getId() != null){
			String query = "select id from vendor where user_id = "+user.getId();
			getJdbcTemplate().queryForObject(query, new RowMapper<User>() {
				public User mapRow(ResultSet rs, int rownumber)
						throws SQLException {
					User userObj = user;
					userObj.setEntityId(rs.getInt("id"));
					return userObj;
				}});
		}
	}

	private void setCurrentUserName(User user){
		String sql = null;
		if(user.getRoleId().equals(Constants.ROLE_ID_SCHOOL_ADMIN)){
			sql = "SELECT concat(if (firstName is not null, firstName, ''), ' ',  if (lastName is not null, lastName, '')) as name FROM `school_admin` where user_id = "+user.getId();
		}
		else if(user.getRoleId().equals(Constants.ROLE_ID_TEACHER)){
			sql = "SELECT concat(if (firstName is not null, firstName, ''), ' ',  if (lastName is not null, lastName, '')) as name FROM `teacher` where user_id = "+user.getId();
		}
		if(sql != null){
			String nameOfUser = getJdbcTemplate().query(sql,  new RowMapper<String>(){
				@Override
				public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
					return resultSet.getString("name");
				}
			}).get(0);
			user.setName(nameOfUser);
		}
		else {
			user.setName(user.getUsername());
		}
	}


	public User getUserDetails(String username, String password, boolean loadChatProfile) throws SQLException, UnauthorizedException {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		setUserInstitute(user);

		setUserRoles(user);
		setCurrentUserName(user);
		getUserServiceDetails(user);
		List<Integer> branchIdList = getUserBranches(user.getId());
		user.setBranchIdList(branchIdList);

		if(user.getRoleId().equals(Constants.ROLE_ID_SCHOOL_ADMIN)){
			setUserInstituteInfo(user);
		}
		else if(user.getRoleId().equals(Constants.ROLE_ID_TEACHER)){
			setUserTeacherInfo(user);
		}
		else if(user.getRoleId().equals(Constants.ROLE_ID_VENDOR)){
			setUserVendorInfo(user);
		}

		user.setRolePermissions(getMenuPermissions(user.getRoleId(), user.isBRanchAdmin()));
		user.setMenus(getMenuItems(user.getRoleId(), user.isBRanchAdmin()));

		user.setPassword(null);
		return user;
	}

	private void getUserServiceDetails(User user) throws UnauthorizedException {
		final HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    List<MediaType> acceptableMediaTypes = new ArrayList<>();
	    acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
	    headers.setAccept(acceptableMediaTypes);
	    
	    String body = "{\"login\":\""+user.getUsername()+"\",\"password\":\""+user.getPassword()+"\"}";
	    
	    
	    HttpEntity<String> httpEntity = new HttpEntity<String>(body, headers);
	    RestTemplate restTemplate = new RestTemplate();
	    
	    final ResponseEntity<Object> responseEntity = restTemplate.exchange(
	    		UrlConfiguration.GET_USER_SERVICE_DETAILS_URL, HttpMethod.POST, httpEntity,Object.class);
	    
	    
		 ObjectMapper mapper = new ObjectMapper();
		 try {
			 @SuppressWarnings("unchecked")
			 LinkedHashMap<String , Object> responseData  = (LinkedHashMap<String, Object>) responseEntity.getBody();
			 @SuppressWarnings("unchecked")
			 HashMap<String, Object> data  = (HashMap<String, Object>)responseData.get("authentication");
			 
			 String userJson = mapper.writeValueAsString(data);
		
			 User userObj = mapper.readValue(userJson, User.class);
			
			 if(user.getRoleId() == (Constants.ROLE_ID_TEACHER)){
				 headers.add("token", userObj.getToken());
				 body = null;
				 httpEntity = new HttpEntity<String>(body, headers);
				 restTemplate.exchange( UrlConfiguration.GET_TEACHER_SERVICE_DETAILS_URL, 
						 HttpMethod.PUT, httpEntity,Object.class);
			 }
			 user.setToken(userObj.getToken());
			 user.setChatProfile(userObj.getChatProfile());
			 user.setForumProfile(userObj.getForumProfile());
			 user.setShoppingProfile(userObj.getShoppingProfile());
			 user.setLoginCount(userObj.getLoginCount());
			 //user.setRoles(userObj.getRoles());
			 logger.debug("Login response from user service - "+userJson);
		 } catch (IOException e) {
			 logger.error("", e);
			 throw new UnauthorizedException("Username or password is incorrect!");
		 }
	}
	
	
	private LinkedList<MenuGroup> getMenuItems(Integer roleId, boolean isBranchAdmin){

		String query = "SELECT GROUP_CONCAT(menu_name order by group_item_position ASC) AS menuItems, "
				+ " GROUP_CONCAT(state_url order by group_item_position ASC) AS stateUrls, group_name AS groupName, "
				+ " group_tag AS groupTag FROM menu_items mi "
				+ " JOIN role_menu rm ON (mi.menu_id = rm.menu_id) WHERE role_id = " +roleId
				+ " AND group_name IS NOT NULL AND group_tag IS NOT NULL "
				+ " GROUP BY group_tag ORDER BY menu_position ASC; ";
		
		List<MenuGroups> groups = getJdbcTemplate().query(query, new BeanPropertyRowMapper<MenuGroups>(MenuGroups.class));
		LinkedList<MenuGroup> groupItems = new LinkedList<MenuGroup>();
		
		for(MenuGroups menuGroup:groups){
			List<String> menuItems = Arrays.asList(menuGroup.getMenuItems().split(","));
			List<String> stateUrls = Arrays.asList(menuGroup.getStateUrls().split(","));
			LinkedList<MenuGroupItem> menuGroupItems = new LinkedList<MenuGroupItem>();
			
			for(int i=0;i<menuItems.size();i++){
				String item = menuItems.get(i);
				String stateUrl = stateUrls.get(i);
				
				MenuGroupItem groupItem = new MenuGroupItem();
				groupItem.setMenuItem(item);
				groupItem.setStateUrl(stateUrl);
				menuGroupItems.add(groupItem);
			}
			MenuGroup group = new MenuGroup();
			group.setGroup(menuGroup.getGroupName());
			group.setTag(menuGroup.getGroupTag());
			group.setGroupItems(menuGroupItems);
			groupItems.add(group);
		}
		
		return groupItems;
		
	}
	//TODO CHANGE FOR PASSWORD ENCRYPTION
//	@Override
//    public UserDetails loadUserByUsername(String username)
//            throws UsernameNotFoundException, DataAccessException{	
//    	User user = new User();
//    	List<Integer> roleIdList = new ArrayList<>();	
//		try {
//			String query = "SELECT * from user where username = '"+username+"'";
//			user = getJdbcTemplate().queryForObject(query, new RowMapper<User>() {
//				public User mapRow(ResultSet rs, int rownumber)
//						throws SQLException {
//					User user = new User();
//					user.setId(rs.getInt("id"));
//					user.setUsername(rs.getString("username"));
//					user.setPassword(rs.getString("temp_password"));
////					user.setRoleId(rs.getInt("role_id"));
//					
//					return user;
//				}});
//			query = "select role_id from user_role where user_id = "+user.getId();
//			roleIdList = getJdbcTemplate().query(query, new RowMapper<Integer>() {
//				public Integer mapRow(ResultSet rs, int rownumber)
//						throws SQLException {
//					return rs.getInt("role_id");
//				}});
//			//roleIdList.add(user.getRoleId());			
//			
//		}catch(Exception e){
//			logger.error("", e);
//		}
//        org.springframework.security.core.userdetails.User userObj = 
//        	new org.springframework.security.core.userdetails.User(
//        	user.getUsername(),user.getPassword(),getGrantedAuthorities(roleIdList));
//        
//        return userObj; 
//    }

//	public static Collection<? extends GrantedAuthority> getGrantedAuthorities(List<Integer> roles) {
//		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//		if(!roles.isEmpty()){
//			for (Integer role : roles) {
//				authorities.add(new SimpleGrantedAuthority(role.toString()));
//			}	
//		}
//		
//		return authorities;
//	}
	
/*	public List<Role> getRolesByUsername(String username) throws SQLException{
		Connection connection = dataSource.getConnection();
		List<Role> roleList = new ArrayList<>();
		try {
			statement = connection.createStatement();
			String query = "SELECT * furom user where username = '"+username+"'";
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				Integer id = resultSet.getInt("role_id");
				RolesEnum roleInstance = RolesEnum.values()[id - 1];
				Role roles = new Role();
				roles.setName(roleInstance.name());
				roles.setId(id);
				roleList.add(roles);
				resultSet.close();
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(resultSet, statement, null, connection);
		}
		return roleList;
	}*/
	
	public List<RoleMenuPermissions> getMenuPermissions(Integer roleId, boolean isBranchAdmin) throws SQLException{
		List<RoleMenuPermissions> roleMenuPermissionList = new ArrayList<>();
		try {
			String query = "select mp.menu_id,mp.menu_permission_id,role_id,permission_name,mp.url " +
					"from menu_permission mp left join role_menu_permission rmp on " +
					"(mp.menu_permission_id = rmp.menu_permission_id and rmp.role_id = "+ roleId+")";
			if(isBranchAdmin){
				query = query + " AND mp.menu_id != (select menu_id from menu_items where menu_name = 'Branch')";
			}

			roleMenuPermissionList = getJdbcTemplate().query(query, new RowMapper<RoleMenuPermissions>() {
				public RoleMenuPermissions mapRow(ResultSet rs, int rownumber)
						throws SQLException {
					RoleMenuPermissions roleMenuPermissions = new RoleMenuPermissions();
					roleMenuPermissions.setPermissionName(rs.getString("permission_name"));
					roleMenuPermissions.setUrl(rs.getString("url"));
					roleMenuPermissions.setMenuId(rs.getInt("menu_id"));
					String roleStr = rs.getString("role_id");
					Integer role = roleStr != null ? Integer.parseInt(roleStr) : null;
					Boolean allow = roleStr == null ? false : true;
					roleMenuPermissions.setRoleId(role);
					roleMenuPermissions.setMenuPermissionId(rs.getInt("menu_permission_id"));
					roleMenuPermissions.setAllow(allow);
					return roleMenuPermissions;
				}});
		}catch(Exception e){
			logger.error("", e);
		}
		return roleMenuPermissionList;
	}

	// TODO Unused
	public String sendForgotPasswordEmail(String username) throws Exception{
		String otp = null;
		try {
			Connection connection = connect.getConnection();
			statement = connection.createStatement();
			String sql = "select ur.role_id from user_role as ur, user as u where ur.user_id = u.id " +
					"AND u.username = '"+username+"' AND u.active = 'Y' AND u.isDelete = 'N'";

			resultSet = statement.executeQuery(sql);
			if(resultSet.next()){
				Integer roleId = resultSet.getInt("role_id");
				resultSet.close();
				sql = null;
				// emailAddress column assumed same in each table.
				if(roleId == Constants.ROLE_ID_SCHOOL_ADMIN){
					sql = "select u.id, sc.firstName as name, sc.emailAddress, sc.primaryContact as contactNumber " +
							"from user as u, school_admin as sc where sc.user_id = u.id " +
							" AND username = '"+username+"'";
				}
				else if(roleId == Constants.ROLE_ID_TEACHER){
					sql = "select u.id, t.firstName as name, t.emailAddress,t.primaryContact as contactNumber " +
							"from user as u, teacher as t where t.user_id = u.id AND" +
							" username = '"+username+"'";
				}
				// vendor not done for email send functionality
				else if(roleId == Constants.ROLE_ID_VENDOR){
					sql = "select u.id, v.merchant_legal_name as name, v.emailAddress, v.primary_phone as contactNumber " +
							"from user as u, vendor as v where v.user_id = u.id " +
							" AND username = '"+username+"'";
				}
				else if(roleId == Constants.ROLE_ID_ADMIN){
					sql = "select u.id, u.username as name, u.username as emailAddress from user as u " +
						" where username = '"+username+"'";
				}

				if(sql != null){
					resultSet = statement.executeQuery(sql);
					if(resultSet.next()){
//						String firstName = resultSet.getString("name");
//						String email = resultSet.getString("emailAddress");
						otp = RandomStringUtils.random(6, true, true);
						//String subject = "QFix one time password for change password.";
					//	String messageBody = "Your Otp for username "+username+ " is :- "+otp;
						logger.debug("Sending Forgot password otp - "+otp);
						// sendMessageBody(firstName, email, otp);
						// sendSmsBody(contactNumber, otp);
					/*	SettingDao settingDao = new SettingDao();
						settingDao.sendEmail(email, subject, messageBody);*/
					}
				}
			}
			else {
				logger.error("Username not exist for forgot password..");
				throw new InvalidCredintialsException("Username does not exist.");
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		catch(Exception e){
			logger.error("", e);
			if(e instanceof InvalidCredintialsException){
				throw e;
			}			
		}
		return otp;
	}

	
	public boolean changePassword(ForgotPasswordModel forgotPasswordModel) {
		boolean flag = false;
		try {
			byte[] randomSalt = SecurityUtil.generateSalt();
			byte[] passwordHash = SecurityUtil.hashPassword(forgotPasswordModel.getPassword(), randomSalt);
			
			String sql = "update `user` set password = ?, password_salt = ?, temp_password = ? " +
				  "where username = ?";
			
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setBytes(1, passwordHash);
			preparedStatement.setBytes(2, randomSalt);
			preparedStatement.setString(3, forgotPasswordModel.getPassword());
			preparedStatement.setString(4, forgotPasswordModel.getUsername());
			preparedStatement.executeUpdate();
			
			flag = true;
			logger.debug("password changed for username - "+forgotPasswordModel.getUsername());
		}catch(Exception e){
			logger.error("", e);
		}
		return flag;
	}

	public List<Integer> getUserBranches(Integer userId) {

		List<Integer> branchIdList = new ArrayList<>();	
		String sql = "select role_id from user_role where user_id = " + userId + " AND role_id != "+Constants.ROLE_ID_PARENT;
		Integer roleId = getJdbcTemplate().queryForObject(sql, Integer.class);
		
		String query = "select b.id as branch_id from branch b " +
				" INNER JOIN branch_admin as ba on ba.branch_id = b.id " +
				" WHERE ba.user_id = "+userId+" and ba.is_delete = 'N' and b.is_delete = 'N' ";

		if(roleId.equals(Constants.ROLE_ID_TEACHER)){
			query = "select branch_id from teacher where user_id = "+userId;
		}

		branchIdList = getJdbcTemplate().query(query, new RowMapper<Integer>() {
			public Integer mapRow(ResultSet rs, int rownumber) throws SQLException {
				return rs.getInt("branch_id");
		}});
		return branchIdList;

	}
}
