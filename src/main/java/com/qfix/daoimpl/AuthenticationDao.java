package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.qfix.model.Authentication;

@Repository
public class AuthenticationDao extends JdbcDaoSupport{
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	public List<Authentication> fetchUrlRole() throws SQLException{
		List<Authentication> authenticationList = new ArrayList<>();
		try{
			String sql = "select rest_url, GROUP_CONCAT(rmp.role_id) permissions from menu_permission " +
					"mp join role_menu_permission rmp on(mp.menu_permission_id = rmp.menu_permission_id) " +
					"where rest_url is not null group by rest_url";

			logger.debug("FetchUrlRole SQL --"+sql);

			authenticationList = getJdbcTemplate().query(sql, new RowMapper<Authentication>() {
				public Authentication mapRow(ResultSet rs, int rownumber)
						throws SQLException {
					Authentication authentication = new Authentication();
					authentication.setName(rs.getString("rest_url"));
					authentication.setRoleInfo(rs.getString("permissions"));
					return authentication;
				}});
		}catch(Exception e){
			
		}
		return authenticationList;
	}
}
