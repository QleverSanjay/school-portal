package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.qfix.dao.StandardInterfaceDao;
import com.qfix.exceptions.StandardAllreadyExistException;
import com.qfix.model.Course;
import com.qfix.model.Division;
import com.qfix.model.Standard;

@Slf4j
@Repository
public class StandardDao  extends JdbcDaoSupport implements StandardInterfaceDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(StandardDao.class);

	@Transactional
	public List<Standard> getAll(Integer branchId) throws SQLException{
	
			String sql = "select GROUP_CONCAT(d.displayName order by d.displayName ASC SEPARATOR ', ') as divisions, s.*  from standard as s "
				+ " LEFT JOIN standard_division as sd on sd.standard_id = s.id  AND s.branch_id = sd.branch_id " 
				+ " LEFT JOIN division as d on d.id = sd.division_id  AND d.academic_year_id = s.academic_year_id "
				+ " where s.active = 'Y' AND s.branch_id = "+ branchId + " AND  s.academic_year_id = "
				+ " (select id from academic_year where  is_current_active_year = 'Y' and branch_id = "+branchId+") "
				+ " group by s.id";

			logger.debug("get All standard SQL --"+sql);

			return getJdbcTemplate().query(sql, new RowMapper<Standard>() {
				public Standard mapRow(ResultSet rs, int rownumber)throws SQLException {
				Standard standard = new Standard();
				standard.setId(rs.getInt("id"));
				standard.setCode(rs.getString("code"));
				standard.setDisplayName(rs.getString("displayName"));
				standard.setBranchId(rs.getInt("branch_id"));
				standard.setAcademicYearId(rs.getInt("academic_year_id"));
				standard.setDivisions(rs.getString("divisions"));
				return standard;
			}});
	
	}
	
	

	@Override
	public List<Standard> listStandards() {
		String query = "SELECT * FROM school_standards ORDER BY id ASC"; 
		return getJdbcTemplate().query(query, new BeanPropertyRowMapper<Standard>(Standard.class));
	}


	@Transactional
	public Standard getStandardById(Integer id){
		String sql = "select s.*, i.id as instituteId from standard as s, branch as b, institute as i " +
				"where s.branch_id = b.id AND b.institute_id = i.id AND s.id = "+id;
		
		List<Standard> standardList = getJdbcTemplate().query(sql, new RowMapper<Standard>() {
			public Standard mapRow(ResultSet resultSet, int rownumber)throws SQLException {
				Standard standard = new Standard();
				standard.setId(resultSet.getInt("id"));
				standard.setCode(resultSet.getString("code"));
				standard.setDisplayName(resultSet.getString("displayName"));
				standard.setBranchId(resultSet.getInt("branch_id"));
				standard.setInstituteId(resultSet.getInt("instituteId"));
				standard.setAcademicYearId(resultSet.getInt("academic_year_id"));
				
				String divList = "select division_id from standard_division where standard_id = "+standard.getId();
				
				List<Integer> divisionIDList = getJdbcTemplate().query(divList, new RowMapper<Integer>(){
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						// TODO Auto-generated method stub
						return resultSet.getInt(1);
					}
				});
				
				standard.setDivisionIdList(divisionIDList);
				
				return standard;
		}});
		
		return standardList.get(0);
	}
	
	@Override
	public Standard  getStandardByName(String name,Integer branchId, Integer academicYearId){
		String query = "SELECT std.* FROM standard std "
				+ " LEFT JOIN branch b on std.branch_id=b.id "
				+ " LEFT JOIN academic_year ay on std.academic_year_id=ay.id "
				+ " WHERE std.displayName =? "
				+ " AND std.active = 'Y' "
				+ " AND std.academic_year_id = ?"
				+ " AND ay.is_current_active_year = 'Y'"
				+ " AND ay.is_delete = 'N'"
				+ " AND std.branch_id=?";
		
		Object[] objects = new Object[]{name,academicYearId,branchId};
		
		try{
			return getJdbcTemplate().queryForObject(query,objects, new BeanPropertyRowMapper<Standard>(Standard.class));
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Transactional
	public boolean insertStandard(Standard standard) throws StandardAllreadyExistException{
		boolean flag = false;
			if(!isStandardExist(standard)){
				String sql = "insert into standard (code, displayName, branch_id, active, academic_year_id) " +
							 "values (?, ?, ?, ?, ?)";
				
				Object[] params = new Object[] {
						standard.getCode(),
						standard.getDisplayName(),
						standard.getBranchId(),
						"Y",
						standard.getAcademicYearId()
				};
					
				int[] types = new int[] { Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.VARCHAR,Types.INTEGER};
					
				getJdbcTemplate().update(sql, params, types);
				
				String idStr = "select max(id) as id from standard where active = 'Y'";
				
				List<Integer> idList = getJdbcTemplate().query(idStr, new RowMapper<Integer>(){

					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						// TODO Auto-generated method stub
						return resultSet.getInt(1);
					}
					
				});
				
				if(!idList.isEmpty()){
					standard.setId(idList.get(0));
				}
				if(standard.getDivisionIdList() != null ){
					insertIntoStandardDivistion(standard);	
				}

				flag = true;
				logger.debug("standard inserted...branchId --"+standard.getBranchId()
						+", standard --"+standard.getDisplayName());
			}			
			else {
				throw new StandardAllreadyExistException("This standard is already exist in this Branch.");
			}
		return flag;
	}

	private void insertIntoStandardDivistion(Standard standard) {
		String sql = "insert into standard_division (branch_id, standard_id, division_id) values (?, ?, ?)";
		for(Integer divisionId : standard.getDivisionIdList()){
			
			Object[] params = new Object[] {standard.getBranchId(),standard.getId(),divisionId};
				
			int[] types = new int[] {Types.INTEGER,Types.INTEGER,Types.INTEGER};
				
			getJdbcTemplate().update(sql, params, types);
		}
		logger.debug("insert into standard division done...");
	}

	private boolean isStandardExist(Standard standard) {
		String sql = "select count(*) as count from standard where active = 'Y' AND displayName = '"+standard.getDisplayName()+"'" +
				" AND academic_year_id = "+standard.getAcademicYearId() +
				" AND branch_id = "+standard.getBranchId();
		System.out.println("Checking standard " + sql);
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			return true;
		}
		return false;
	}

	@Transactional
	public boolean updateStandard(Standard standard) throws StandardAllreadyExistException{
		boolean flag = false;
		if(!isStandardExist(standard)){
			String sql = "update standard set code = ?, displayName = ?, branch_id = ?, academic_year_id = ? " +
							 "where id = ?";
			Object[] params = new Object[] {
					standard.getCode(),
					standard.getDisplayName(),
					standard.getBranchId(),
					standard.getAcademicYearId(),
					standard.getId()
			};
				
			int[] types = new int[] { Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER};
				
			getJdbcTemplate().update(sql, params, types);
			
			String delSql = "delete from standard_division where standard_id = "+standard.getId();
			
			getJdbcTemplate().update(delSql);
			
			insertIntoStandardDivistion(standard);
			flag = true;
			logger.debug("standard updated...branchId --"+standard.getBranchId()
					+", standard --"+standard.getDisplayName());
		}
		else {
			throw new StandardAllreadyExistException("This standard is already exist in this Branch.");
		}
		return flag;
	}

	@Transactional
	public boolean deleteStandard(Integer standardId) {
		String sql = "update standard set active = 'N' where id = "+standardId;
		getJdbcTemplate().update(sql);
		return true;
	}
	
	

	@Override
	public void deleteBranchStandards(Integer branchId) {
		String sql = "delete from standard_division where branch_id = "+branchId;
		getJdbcTemplate().update(sql);
		sql = "delete from standard where branch_id = "+branchId;
		getJdbcTemplate().update(sql);
	}

	@Override
	public Standard insertStandards(final Standard standard,List<Division> divisions) {
		final String sql = "insert into standard (code, displayName, branch_id, active, qfix_defined_name, academic_year_id) " +
				 "values (?, ?, ?, ?, ?, ?)";
			boolean isStandardExist = isStandardExist(standard);
			
			if(!isStandardExist){
				KeyHolder holder = new GeneratedKeyHolder();
				getJdbcTemplate().update(new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
						ps.setString(1, null);
						ps.setString(2, standard.getName());
						ps.setInt(3, standard.getBranchId());
						ps.setString(4, "Y");
						ps.setString(5, standard.getQfixDefinedName());
						ps.setInt(6, standard.getAcademicYearId());
						return ps;
					}
				}, holder);
				standard.setId(holder.getKey().intValue());
			}
			insertStandardDivisions(standard, divisions);
		
		return standard;
	}

	private void insertStandardDivisions(Standard standard, List<Division> divisions) {

		if(divisions != null && divisions.size() > 0){
			for(Division division : divisions){
				Integer branchId = standard.getBranchId();
				Integer standardId = standard.getId();
				Integer divisionId = division.getId();
				boolean isExist = isStandardDivisionExist(branchId, divisionId, standardId);
				
				if(!isExist){					
					String sql = "insert into standard_division (branch_id, standard_id, division_id) values("+branchId 
							+ ", "+standardId+", "+divisionId+" )";
					getJdbcTemplate().update(sql);
				}
			}
		}
	}

	private boolean isStandardDivisionExist(Integer branchId, Integer divisionId,Integer standardId){
		String query = "SELECT count(*) FROM standard_division WHERE branch_id=? AND standard_id=? AND division_id=?";
		Object[] params = new Object[]{branchId,standardId,divisionId};
		
		Integer count = getJdbcTemplate().queryForObject(query, params,Integer.class);
		return count > 0;
	}

	@Override
	public void deleteStandards(Integer branchId,String[] names) {
		
		StringBuilder builder = new StringBuilder();
		int count = 1;
		for(String name:names){
			if(count < names.length){				
				builder.append("'").append(name).append("',");
			}else{
				builder.append("'").append(name).append("'");
			}
			++count;
		}
		
		String in = builder.toString();
		
		String query = "DELETE FROM standard where displayName in (" + in + ") AND branch_id = " +branchId;
		
		System.out.println(query);
		
		getJdbcTemplate().update(query);
	}

	
	public List<Course> getCoursesByStandardId(Integer branchId, Integer standardId, Integer academicYearId){
		String sql = "select * from college_courses cc "
				+ "where cc.id in (select distinct division_id from "
				+ "year_course_subject ycs where ycs.standard_id = "+standardId
				+" and ycs.branch_id = "+branchId+" and academic_year_id = "+academicYearId+")";
		log.info(sql);
		List<Course> courseList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Course>(Course.class));
		return courseList;
	}




	@Override
	public List<Standard> getStandardDivision(Integer branchId, Integer ayId) {
		String query = "SELECT s.*, s.displayName as text, s.displayName as name "
				+ " FROM standard s WHERE s.branch_id = ? "
				+ " AND s.academic_year_id = ? "
				+ " AND s.active = 'Y'";
		Object[] params = new Object[]{branchId,ayId};
		
		List<Standard> standards = getJdbcTemplate().query(query, params,new BeanPropertyRowMapper<Standard>(Standard.class));
		
		for(Standard standard:standards){
			List<Division> divisions = getStandardDivisions(standard.getId(), branchId, ayId);
			standard.setDivisionList(divisions);
		}
		
		return standards;
	}
	
	private List<Division> getStandardDivisions(Integer standardId,Integer branchId,Integer ayId){
		String query = "SELECT d.*, d.displayName as name, d.displayName as text "
				+ " FROM division d"
				+ " JOIN standard_division sd on sd.division_id=d.id "
				+ " WHERE d.branch_id = ? "
				+ " AND sd.standard_id = ? "
				+ " AND d.academic_year_id = ? "
				+ " AND d.active = 'Y'";
		
		Object[] params = new Object[]{branchId,standardId,ayId};
		return getJdbcTemplate().query(query, params, new BeanPropertyRowMapper<Division>(Division.class));
	} 

}
