package com.qfix.daoimpl;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.dao.IEventDao;
import com.qfix.dao.IIndividualDao;
import com.qfix.exceptions.ProfileImageUploadException;
import com.qfix.model.Event;
import com.qfix.model.Individual;
import com.qfix.model.Master;
import com.qfix.model.UserEvent;
import com.qfix.service.client.notification.Notification;
import com.qfix.service.client.notification.Sender;
import com.qfix.service.file.FileFolderService;
import com.qfix.service.file.FileFolderServiceImpl;
import com.qfix.utilities.Constants;
import com.qfix.utilities.FileConstants;
import com.qfix.utilities.FtlTemplateConstants;

@Repository
public class EventDao extends JdbcDaoSupport implements IEventDao {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(EventDao.class);

	SimpleDateFormat sdfForSqlWithTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	IIndividualDao individualDao;
	
	@Autowired
	private FileFolderService fileFolderService;

	@Autowired
	Sender sender;

	public List<Event> getAll(Integer instituteId)throws SQLException{
		List<Event> eventList = new ArrayList<Event>();

		String sql = null;
		if(instituteId != null && instituteId != 0){
		sql = "select e.*, c.name as courseName, b.name as branchName from " +
					"event as e, branch as b, course as c where e.branch_id = b.id " +
					"AND e.course_id = c.id AND b.institute_id = "+instituteId+" AND e.status != '"+Constants.RECORD_DELETED+"'";
		}else{
			sql = "select e.*, c.name as courseName, b.name as branchName from " +
					"event as e, branch as b, course as c where e.branch_id = b.id AND e.course_id = c.id"+
					" AND e.status != '"+Constants.RECORD_DELETED+"'";
		}
		logger.debug("get All event SQL --"+sql);
		eventList = getJdbcTemplate().query(sql, new RowMapper<Event>(){
			public Event mapRow(ResultSet rs, int rownumber)throws SQLException {
				Event event = new Event();
				event.setId(rs.getInt("id"));
				event.setTitle(rs.getString("title"));
				event.setVenue(rs.getString("venue"));
				event.setStartDate(rs.getString("startdate"));
				event.setEndDate(rs.getString("enddate"));
				event.setStartTime(rs.getString("starttime"));
				event.setEndTime(rs.getString("endTime"));
				/*event.setStandardId(rs.getInt("standard_id"));
				event.setDivisionId(rs.getInt("division_id"));*/
				event.setBranchName(rs.getString("branchName"));
				event.setCourseName(rs.getString("courseName"));
				event.setBranchId(rs.getInt("branch_id"));
				event.setCourseId(rs.getInt("course_id"));
				event.setSentBy(rs.getInt("sent_by"));
				event.setSentByName(getSentBy(event.getSentBy()));
				return event;
			}
		});
		return eventList;
	}


	/*public boolean uploadEvent(List<Event> eventList){
		boolean flag = false;
		connection = connect.getConnection();
		logger.debug("Upload event list --"+eventList);
		
		try {
			
			insertIntoEvent(event);

				System.out.println("Done Here ........."+event);
				sql = "select max(id) as id from event";
				statement = connection.createStatement();
				resultSet = statement.executeQuery(sql);
				Integer eventId = null;
				if(resultSet.next()){
					eventId = resultSet.getInt("id");
				}
				event.setId(eventId);
				sql = "select gs.group_id from group_standard as gs, `group` as g where standard_id = '"+event.getStandardId()+"'" +
						" AND division_id ='"+event.getDivisionId()+"' AND g.branch_id = "+event.getBranchId() 
						+" AND g.isDelete = 'N' AND g.active = 'Y' AND g.id = gs.group_id";

				statement = connection.createStatement();
				resultSet = statement.executeQuery(sql);

				logger.debug("inserting into event_group started...Groups --"+event.getGroupIdList());
				sql = "insert into event_group (group_id, event_id, status) values(?, ?, ?)";
				PreparedStatement preparedStatement1 = connection.prepareStatement(sql);
				Integer groupId = null;
				boolean flag1 = false;
				while(resultSet.next()){
					groupId = resultSet.getInt("group_id");
					preparedStatement1.setInt(1, groupId);
					preparedStatement1.setInt(2, eventId);
					preparedStatement1.setString(3, "N");
					preparedStatement1.executeUpdate();
					flag1= true;
					
					insertGroupUserEvent(event);
				}

				if(flag1 == false){	
					sql = "select user_id from student where standard_id = "+event.getStandardId()+" " +
							"AND division_id = "+event.getDivisionId()+" AND branch_id = "+event.getBranchId();

					statement = connection.createStatement();
					resultSet = statement.executeQuery(sql);

					logger.debug("Inserting into student_event started... users --"+event.getIndividualIdList());

					sql = "insert into student_event (user_id, event_id, status) values(?, ?, ?)";
					Integer userId = null;
					while(resultSet.next()){
						userId = resultSet.getInt("user_id");
						PreparedStatement preparedStatement2 = connection.prepareStatement(sql);
						preparedStatement2.setInt(1, userId);
						preparedStatement2.setInt(2, eventId);
						preparedStatement2.setString(3, "N");
						preparedStatement2.executeUpdate();
					}
					insertIndividualEvent(event);
				}
				sendEventNotifications(event, false);
				logger.debug("Event inserted...");
			flag = true;
		}catch(SQLException e){
			logger.error("", e);
			e.printStackTrace();
		}
		finally{
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}*/

	
	public String saveFile(MultipartFile file, Event event, boolean isUpdate) throws IOException{
		String filePath = null;
		if(file != null ){
			String parentPath = event.getInstituteId() + File.separator + event.getBranchId() + File.separator
					 + FileConstants.EVENT_DOCUMENTS_PATH;

			String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
			String fileNameWithExtension = org.apache.commons.lang3.RandomStringUtils.random(30, true, true) + fileExtension;

			if(isUpdate){
				String imageUrl = event.getImageUrl();
				fileNameWithExtension = (StringUtils.isEmpty(imageUrl)  || imageUrl.endsWith(FileConstants.DEFAULT_IMAGE_NAME) 
						? fileNameWithExtension : imageUrl.substring(imageUrl.lastIndexOf("/") + 1));
			}
			
			filePath = fileFolderService.createFileInInternalStorage
					 (parentPath, fileNameWithExtension, file.getBytes());
			logger.debug("Event file saved");
		}

		filePath = FilenameUtils.separatorsToUnix(filePath);
		return filePath;
	}

	@Transactional
	private void updatePhotoPath(Integer id, String photoPath){
		String sql = "update event set image_url = '"+photoPath+"' where id = "+id;
		getJdbcTemplate().update(sql);
	}

	@Transactional
	private void sendEventNotifications(Event event, boolean isUpdate){

		// Create notification object to send to notification-service.
		Notification notification = new Notification();
		notification.setId(event.getId());
		notification.sendEmail(true);
		notification.sendSMS(true);
		notification.sendPushMessage(true);
		notification.setType("EVENT");
		notification.setBatchEnabled(false);

		String templateCode = isUpdate ? FtlTemplateConstants.UPDATE_EVENT_CODE : FtlTemplateConstants.CREATE_EVENT_CODE;
		notification.setTemplateCode(templateCode);

		try {
			ObjectMapper mapper = new ObjectMapper();
			logger.debug("NOTIFICATION_OBJ:::::\n"+mapper.writeValueAsString(notification));		
			sender.sendMessage(mapper.writeValueAsString(notification));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}


	@Override
	public boolean insertEvent(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException {
		
		insertEventInDB(event, file);
		sendEventNotifications(event, false);

		return true;
	}
	
	@Transactional
	public boolean insertEventInDB(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException {

		Integer eventId = insertIntoEvent(event);
		event.setId(eventId);
		List<UserEvent> userEvents = new ArrayList<>();
		List<Integer> studentUserIds = new ArrayList<>();
		Map<Integer, Integer> studentEventGroupMap = new HashMap<Integer, Integer>();

		insertOrUpdateIndvidualEvents(event.getId(), event.getIndividualIdList(), userEvents, studentUserIds, Constants.RECORD_ADDED, true);
		insertGroupEvents(event, userEvents, studentUserIds, studentEventGroupMap);
		// prepare parents by studentUserIds.
		if(event.getForParent().equals("Y") || event.getForStudent().equals("Y")){
			prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_ADDED, true);
		}

		applyConstraintsOnUserEvents(event, userEvents);
		insertOrUpdateUserEvents(userEvents, true);
		
		if(file != null){
			String photoPath = saveFile(file, event, false);
			if(photoPath != null){
				logger.debug("Updating event photo path --"+photoPath);
				updatePhotoPath(event.getId(), photoPath);
			}
		}

//		sendEventNotifications(event, false);

		return true;
	}

	@Transactional
	private void insertOrUpdateUserEvents(List<UserEvent> userEvents, boolean isEventInsert) {
		if(userEvents.size() > 0){
			for(final UserEvent userEvent : userEvents){

				if(userEvent.isInsertRecord()){
					String sql = "select count(*) as count from user_event where event_id = "+userEvent.getEventId()+" AND user_id = "+userEvent.getUserId() +" AND for_user_id = "+userEvent.getForUserId();
					int count = getJdbcTemplate().queryForObject(sql, Integer.class);
					if(count == 0){
						sql = "insert into user_event" +
							"(user_id, role_id, entity_id, event_id, event_group_id, status, for_user_id)" +
							"values("+userEvent.getUserId()+", "+userEvent.getRoleId()+", "+userEvent.getEntityId()
							+", "+userEvent.getEventId()+", "+userEvent.getEventGroupId()
							+", '" +userEvent.getStatus()+ "', "+userEvent.getForUserId()+")";						
					}
					else {
						sql = "update user_event set status = '"+userEvent.getStatus() +"'" +
							" where user_id ="+userEvent.getUserId()+ 
							" AND for_user_id = "+userEvent.getForUserId() + 
							" AND event_id = " + userEvent.getEventId();
					}
					getJdbcTemplate().update(sql);
				}
				else {
					String sql = "update user_event set status = '"+userEvent.getStatus() +"'" +
						" where user_id ="+userEvent.getUserId()+ 
						" AND for_user_id = "+userEvent.getForUserId() + 
						" AND event_id = " + userEvent.getEventId();

					getJdbcTemplate().update(sql);
				}
			}
		}
	}

	@Transactional
	private void applyConstraintsOnUserEvents(Event event,
			List<UserEvent> userEvents) {

		if(userEvents.size() > 0){
			boolean forParent = event.getForParent().equals("Y");
			boolean forTeacher = event.getForTeacher().equals("Y");
			boolean forStudent = event.getForStudent().equals("Y");

			List<UserEvent> tempUserEvents = new ArrayList<>(userEvents);

			for(UserEvent userEvent : tempUserEvents){
				int roleId = userEvent.getRoleId();

				if(roleId == Constants.ROLE_ID_PARENT){
					if(!forParent && userEvent.getUserId().equals(userEvent.getForUserId())){
						userEvents.remove(userEvent);
					}
					else if(!forStudent && !userEvent.getUserId().equals(userEvent.getForUserId())){
						userEvents.remove(userEvent);
					}
				}
				else if(roleId == Constants.ROLE_ID_STUDENT && !forStudent){
					userEvents.remove(userEvent);
				}
				else if(roleId == Constants.ROLE_ID_TEACHER && !forTeacher){
					userEvents.remove(userEvent);
				}
			}
		}
	}


	@Transactional
	private void applyConstraintsOnUserEvents(Event exEvent, Event event,
			List<UserEvent> userEvents) {

		if(userEvents.size() > 0){
			boolean forParent = event.getForParent().equals("Y");
			boolean forTeacher = event.getForTeacher().equals("Y");
			boolean forStudent = event.getForStudent().equals("Y");

			boolean exForParent = exEvent.getForParent().equals("Y");
			boolean exForTeacher = exEvent.getForTeacher().equals("Y");
			boolean exForStudent = exEvent.getForStudent().equals("Y");

			String parentStatus = (!forParent && exForParent ? Constants.RECORD_DELETED : (forParent && !exForParent ? Constants.RECORD_ADDED : Constants.RECORD_MODIFIED));
			String studentStatus = (!forStudent && exForStudent ? Constants.RECORD_DELETED : (forStudent && !exForStudent ? Constants.RECORD_ADDED : Constants.RECORD_MODIFIED));
			String teacherStatus = (!forTeacher && exForTeacher ? Constants.RECORD_DELETED : (forTeacher && !exForTeacher ? Constants.RECORD_ADDED : Constants.RECORD_MODIFIED));

			boolean parentInsert = forParent && !exForParent;
			boolean studentInsert = forStudent && !exForStudent;
			boolean teacherInsert = forTeacher && !exForTeacher;
			
			System.out.println("forTeacher ::: "+forTeacher);
			System.out.println("forParent ::: "+forParent);
			System.out.println("forStudent ::: "+forStudent);

			System.out.println("exForParent ::: "+exForParent);
			System.out.println("exForStudent ::: "+exForStudent);
			System.out.println("exForTeacher ::: "+exForTeacher);

			System.out.println("parentStatus ::: "+parentStatus);
			System.out.println("studentStatus ::: "+studentStatus);
			System.out.println("teacherStatus ::: "+teacherStatus);
			
			
			List<UserEvent> tempUserEvents = new ArrayList<>(userEvents);

			for(UserEvent userEvent : tempUserEvents){
				int roleId = userEvent.getRoleId();
				UserEvent userEventObj = getUserEventFromListById(userEvents, userEvent);
				System.out.println("userEventObj  BEFORE ::: \n"+userEventObj);

				if(userEvent.isInsertRecord()){
					if(roleId == Constants.ROLE_ID_PARENT){
						if(!forParent && userEvent.getUserId().equals(userEvent.getForUserId())){
							userEvents.remove(userEvent);
						}
						else if(!forStudent && !userEvent.getUserId().equals(userEvent.getForUserId())){
							userEvents.remove(userEvent);
						}
					}
					else if(roleId == Constants.ROLE_ID_STUDENT && !forStudent){
						userEvents.remove(userEvent);
					}
					else if(roleId == Constants.ROLE_ID_TEACHER && !forTeacher){
						userEvents.remove(userEvent);
					}
				}
				else if(!userEventObj.getStatus().equals(Constants.RECORD_DELETED)){
					if(roleId == Constants.ROLE_ID_PARENT){
						if(userEvent.getUserId().equals(userEvent.getForUserId())){
							if(!forParent && !exForParent){
								userEvents.remove(userEvent);
							}
							else {
								userEventObj.setStatus(parentStatus);
								userEventObj.isInsertRecord(parentInsert);
							}
						}
						else {
							if(!forStudent && !exForStudent){
								userEvents.remove(userEvent);
							}
							else {
								userEventObj.setStatus(studentStatus);
								userEventObj.isInsertRecord(studentInsert);
							}
						}
					}
					else if(roleId == Constants.ROLE_ID_STUDENT){
						if(!forStudent && !exForStudent){
							userEvents.remove(userEvent);
						}
						else {
							userEventObj.setStatus(studentStatus);
							userEventObj.isInsertRecord(studentInsert);
						}
					}
					else if(roleId == Constants.ROLE_ID_TEACHER && !forTeacher && !exForTeacher){
						userEvents.remove(userEvent);
					}
					else {
						userEventObj.setStatus(teacherStatus);
						userEventObj.isInsertRecord(teacherInsert);
					}
				}
				System.out.println("userEventObj  AFTER ::: \n"+userEventObj);
			}
		}
	}

	private UserEvent getUserEventFromListById(List<UserEvent> userEvents, UserEvent userEvent) {
		if(userEvents.size() > 0){
			for(UserEvent  event : userEvents){
				if(event.equals(userEvent)){
					return event;
				}
			}
		}
		return null;
	}

	@Transactional
	private void insertOrUpdateIndvidualEvents(Integer eventId, List<Integer> individualIds, List<UserEvent> userEvents, 
			List<Integer> studentUserIds, String status, boolean isInsert) {

		if(individualIds != null && !individualIds.isEmpty()){
			prepareUsersByIndividuals(eventId, individualIds, userEvents, studentUserIds, status, isInsert);
			for(Integer userId : individualIds){
				insertOrUpdateStudentEvent(userId, eventId, status, isInsert);
			}
		}
	}

	@Transactional
	private void prepareUsersByIndividuals(final Integer eventId, final List<Integer> individualIdList,
		final List<UserEvent> userEvents, final List<Integer> studentUserIds, final String status, 
		final boolean isInsert) {

		if(individualIdList != null && !individualIdList.isEmpty()){
			String individuals = org.apache.commons.lang.StringUtils.join(individualIdList, ", ");

			String sql = "select ur.*, s.id as studentId, t.id as teacherId" +
					" from user_role as ur " +
					" LEFT JOIN student as s on s.user_id = ur.user_id AND s.isDelete = 'N' AND s.active = 'Y' " +
					" LEFT JOIN teacher as t on t.user_id = ur.user_id AND t.isDelete = 'N' AND t.active = 'Y' " +
					" WHERE ur.user_id in("+individuals+")";

			getJdbcTemplate().query(sql,  new RowMapper<String>(){
				@Override
				public String mapRow(ResultSet resultSet, int arg1)
						throws SQLException {
					int roleId = resultSet.getInt("role_id");
					int userId = resultSet.getInt("user_id");
					int studentId = resultSet.getInt("studentId");
					int teacherId = resultSet.getInt("teacherId");

					if(roleId == Constants.ROLE_ID_PARENT){
						// SKIP as parent can`t be in individual list.
						// Checking exceptionally for dual role.
					}
					else {
						// insert into student Event
						if(roleId == Constants.ROLE_ID_STUDENT){
							studentUserIds.add(userId);
						}

						// add into userEventSet
						UserEvent userEvent = new UserEvent();
						userEvent.setEntityId(roleId == Constants.ROLE_ID_TEACHER ? teacherId : studentId);
						userEvent.setUserId(userId);
						userEvent.setEventId(eventId);
						userEvent.setRoleId(roleId);
						userEvent.setStatus(status);
						userEvent.isInsertRecord(isInsert);
						userEvent.setForUserId(userId);

						if(userEvents.contains(userEvent)){
							userEvents.remove(userEvent);
						}
						userEvents.add(userEvent);	
					}
					return null;
				}
			});
		}
	}

	@Transactional
	private void insertOrUpdateStudentEvent(int userId, Integer eventId,
			String status, boolean isInsert) {

		if(isInsert){
			String sql = "insert into student_event(user_id, event_id, status)values("+userId+", "+eventId+", '"+status+"')";
			getJdbcTemplate().update(sql);
		}else {
			String sql = "update student_event set status = '"+status+"' where user_id = "+userId+" AND event_id = "+eventId;
			getJdbcTemplate().update(sql);
		}
	}

	@Transactional
	private void insertGroupEvents(Event event, List<UserEvent> userEvents, List<Integer> studentUserIds, Map<Integer, Integer> studentEventGroupMap) {
		if(event.getGroupIdList() != null && !event.getGroupIdList().isEmpty()){
			for(Integer groupId : event.getGroupIdList()){
				Integer eventGroupId = insertIntoEventGroup(groupId, event.getId());
				eventGroupId = eventGroupId == 0 ? null : eventGroupId;
				prepareUsersByGroupId(event.getId(), groupId, eventGroupId, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_ADDED, true);
			}
		}
	}

	@Transactional
	private void prepareUsersByGroupId(final Integer eventId, final Integer groupId,
			final Integer eventGroupId, final List<UserEvent> userEvents,
			final List<Integer> studentUserIds, final Map<Integer, Integer> studentEventGroupMap, 
			final String status, final boolean isInsert) {

		String sql = "select count(*) as count, ur.*,  s.id as studentId, p.id as parentId, t.id as teacherId" +
				" from user_role as ur " +
				" LEFT JOIN student as s on s.user_id = ur.user_id AND s.isDelete = 'N' AND s.active = 'Y'" +
				" LEFT JOIN parent as p on p.user_id = ur.user_id AND p.active = 'Y'" +
				" LEFT JOIN teacher as t on t.user_id = ur.user_id AND t.isDelete = 'N' AND t.active = 'Y' " +
				" WHERE ur.user_id in " +
				" (select gu.user_id from group_user as gu" +
				" INNER JOIN `group` as g on g.id = gu.group_id " +
				" where gu.group_id = "+groupId+" AND g.active = 'Y' AND g.isDelete = 'N' ) " +
				" group by ur.user_id";

		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
				int count = resultSet.getInt("count");
				int studentId = resultSet.getInt("studentId");
				int parentId = resultSet.getInt("parentId");
				int teacherId = resultSet.getInt("teacherId");
				int roleId = resultSet.getInt("role_id");
				int userId = resultSet.getInt("user_id");

				roleId = count > 1 ? Constants.ROLE_ID_TEACHER : roleId;
				int entityId = (roleId == Constants.ROLE_ID_STUDENT ? studentId : (count > 1 ? teacherId : parentId));
				
				if(roleId == Constants.ROLE_ID_STUDENT){
					studentUserIds.add(userId);
					studentEventGroupMap.put(userId, eventGroupId);
				}

				// add into userEventSet
				UserEvent userEvent = new UserEvent();
				userEvent.setEntityId(entityId);
				userEvent.setUserId(userId);
				userEvent.setEventGroupId(eventGroupId);
				userEvent.setEventId(eventId);
				userEvent.setRoleId(roleId);
				userEvent.setStatus(status);
				userEvent.isInsertRecord(isInsert);
				userEvent.setForUserId(userId);

				if(userEvents.contains(userEvent)){
					userEvents.remove(userEvent);
				}
				userEvents.add(userEvent);	
				return null;
			}
		});
	}

	@Transactional
	private void prepareParentsByStudents(final Event event, final List<UserEvent> userEvents,
			final List<Integer> studentUserIds, final Map<Integer, Integer> studentEventGroupMap, 
			final String status, final boolean isInsert) {
		
		System.out.println("StudentEventGroup MAP :::::: \n"+studentEventGroupMap);

		if(!studentUserIds.isEmpty()){
			String studentUserIdsStr = org.apache.commons.lang.StringUtils.join(studentUserIds, ", " );
			String sql = "select bsp.*, p.user_id as parentUserId, s.user_id as studentUserId" +
				" from branch_student_parent as bsp " +
				" INNER JOIN parent as p on p.id = bsp.parent_id " +
				" INNER JOIN student as s on s.id = bsp.student_id " +
				" where  bsp.isDelete = 'N' AND s.isDelete = 'N' AND s.user_id in ("+studentUserIdsStr+")";

			getJdbcTemplate().query(sql, new RowMapper<Object>(){
				@Override
				public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
					int parentUserId = resultSet.getInt("parentUserId");
					int parentId = resultSet.getInt("parent_id");
					int studentUserId = resultSet.getInt("studentUserId");
					Integer eventGroupId = studentEventGroupMap != null ? studentEventGroupMap.get(studentUserId) : null;

					// add into userEvent as event for parent and for_user_id will be studentUserId
					UserEvent userEvent = new UserEvent();
					userEvent.setEntityId(parentId);
					userEvent.setUserId(parentUserId);
					userEvent.setEventId(event.getId());
					userEvent.setRoleId(Constants.ROLE_ID_PARENT);
					userEvent.setStatus(status);
					userEvent.isInsertRecord(isInsert);
					userEvent.setForUserId(studentUserId);
					userEvent.setEventGroupId(eventGroupId);

					if(userEvents.contains(userEvent)){
						userEvents.remove(userEvent);
					}
					userEvents.add(userEvent);

					// add this as event will be visible to parent and for_user_id will be parentUserId
					userEvent = new UserEvent();
					userEvent.setEntityId(parentId);
					userEvent.setEventGroupId(eventGroupId);
					userEvent.setUserId(parentUserId);
					userEvent.setEventId(event.getId());
					userEvent.setRoleId(Constants.ROLE_ID_PARENT);
					userEvent.setStatus(status);
					userEvent.isInsertRecord(isInsert);
					userEvent.setForUserId(parentUserId);

					if(userEvents.contains(userEvent)){
						userEvents.remove(userEvent);
					}
					userEvents.add(userEvent);	
					return null;
				}
			});
		}
	}

	@Transactional
	private Integer insertOrUpdateEventGroup(final int groupId, final Integer eventId,
			final String status, boolean isInsert) {

		String sql = "select count(*) as count, id from event_group where event_id = "+eventId + " AND group_id = "+groupId;
		Integer eventGroupId = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1)throws SQLException {
				Integer eventGroupId = resultSet.getInt("id");
				if(resultSet.getInt("count") == 0){
					eventGroupId = null;
				}
				return eventGroupId;
			}
		}).get(0);

		if(eventGroupId == null && isInsert){
			eventGroupId = insertIntoEventGroup(groupId, eventId);
		}else {
			sql = "update event_group set status = '"+status+"' where group_id = "+groupId+" AND event_id = "+eventId;
			getJdbcTemplate().update(sql);
		}
		return eventGroupId;
	}

	@Transactional
	private Integer insertIntoEventGroup(final Integer groupId, final Integer eventId) {
		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {
				String sql = "insert into event_group(group_id, event_id, status)values("+groupId+", "+eventId+", '"+Constants.RECORD_ADDED+"')";
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				return preparedStatement;
			}
		}, holder);
		int eventGroupId = holder.getKey().intValue();
		return eventGroupId;
	}

	@Transactional
	private Integer insertIntoEvent( final Event event) {

		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {

				String sql = "insert into event (title, description, venue, startdate, starttime, endtime, alldayevent, " +
						"reminderSet, branch_id, enddate, reccuring, course_id, " +
						" summary, forparent, forstudent, forteacher, frequency, " +
						"by_day, by_date, created_date, is_schedule_updated, status, forvendor, sent_by ,academic_year_id"
						+(event.getReminderBefore() != null  ? ",reminderbefore" : "" )+") " +
						"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? "
						+(event.getReminderBefore() != null  ? ", ?" : "" )+")";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

				preparedStatement.setString(1, event.getTitle());
				preparedStatement.setString(2, event.getDescription());
				preparedStatement.setString(3, event.getVenue());
				preparedStatement.setString(4, event.getStartDate());
				preparedStatement.setString(5, event.getStartTime());
				preparedStatement.setString(6, event.getEndTime());
				preparedStatement.setString(7, "N");
				preparedStatement.setString(8, event.getReminderSet());
				preparedStatement.setInt(9, event.getBranchId());
				preparedStatement.setString(10, (event.getEndDate() != null ) ? (event.getEndDate()) : null);

				preparedStatement.setString(11, event.getReccuring());
				preparedStatement.setInt(12, event.getCourseId());

				preparedStatement.setString(13, event.getSummary());
				preparedStatement.setString(14, event.getForParent());
				preparedStatement.setString(15, event.getForStudent());
				preparedStatement.setString(16, event.getForTeacher());
				preparedStatement.setString(17, event.getFrequency());
				preparedStatement.setString(18, event.getByDay());
				preparedStatement.setString(19, event.getByDate());
				preparedStatement.setString(20, sdfForSqlWithTime.format(new Date()));
				preparedStatement.setString(21, "N");
				preparedStatement.setString(22, "N");
				preparedStatement.setString(23, event.getForVendor());
				preparedStatement.setInt(24, event.getSentBy());
				preparedStatement.setInt(25, event.getAcademicYearId());

				if(event.getReminderBefore() != null){
					preparedStatement.setInt(26, (event.getReminderBefore() != null ) ? event.getReminderBefore(): 0);
				}
				return preparedStatement;
			}
		}, holder);

		Integer eventId = holder.getKey().intValue();
		logger.debug("insert event Completed.."+event.getTitle() +", Id is ::: "+eventId);
		return eventId;
	}

	@Transactional
	@Override
	public boolean uploadEvent(List<Event> eventList) {
		System.out.println("Uploading event............");
		for (Event event : eventList) {
			try {
				List<Integer> groupIds = getGroupIdsByStandardDivision(event.getStandardId(), event.getDivisionId());
				event.setGroupIdList(groupIds);
				event.setCourseId(3);
				if(event.getReminderBefore() == null){
					event.setReminderSet("N");
				}
				else {
					event.setReminderSet("Y");
				}
				insertEvent(event, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	

	private List<Integer> getGroupIdsByStandardDivision(Integer standardId,
			Integer divisionId) {
		
		String sql = "select group_id from group_standard where standard_id = "+standardId + " AND division_id = "+divisionId;
		List<Integer> groupIds = getJdbcTemplate().queryForList(sql, Integer.class);
		return groupIds;
	}

	@Transactional
	private void updateEventGroup(Event existingEvent, Event event, List<UserEvent> userEvents){
		List<Integer> newGroupIds = new ArrayList<>();
		List<Integer> modifiedGroupIds = new ArrayList<>();
		List<Integer> deletedGroupIds = new ArrayList<>();

		for(Integer groupId : event.getGroupIdList()){
			if(existingEvent.getGroupIdList().contains(groupId)){
				modifiedGroupIds.add(groupId);
			}
			else {
				newGroupIds.add(groupId);
			}
		}

		for(Integer groupId : existingEvent.getGroupIdList()){
			if(event.getGroupIdList().contains(groupId)){
				if(!modifiedGroupIds.contains(groupId)){
					modifiedGroupIds.add(groupId);	
				}
				
			}
			else {
				deletedGroupIds.add(groupId);
			}
		}

		logger.debug("New eventGroups to update --"+newGroupIds);
		logger.debug("Modified eventGroups --"+modifiedGroupIds);
		logger.debug("Deleted eventGroups --"+deletedGroupIds);

		List<Integer> studentUserIds = new ArrayList<>();
		Map<Integer, Integer> studentEventGroupMap = new HashMap<Integer, Integer>();

		updateGroupEvents(event.getId(), newGroupIds, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_ADDED, true);
		prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_ADDED, true);
		studentUserIds.clear();

		updateGroupEvents(event.getId(), modifiedGroupIds, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_MODIFIED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_MODIFIED, false);
		studentUserIds.clear();

		updateGroupEvents(event.getId(), deletedGroupIds, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_DELETED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, studentEventGroupMap, Constants.RECORD_DELETED, false);
		studentUserIds.clear();
	}

	@Transactional
	private void updateGroupEvents(Integer eventId, List<Integer> groupIds, List<UserEvent> userEvents, 
			List<Integer> studentUserIds, Map<Integer, Integer> studentEventGroupMap, String status, boolean isInsert) {

		if(groupIds != null && !groupIds.isEmpty()){
			for(Integer groupId : groupIds){
				Integer eventGroupId = insertOrUpdateEventGroup(groupId, eventId, status, isInsert);
				eventGroupId = eventGroupId == 0 ? null : eventGroupId;
				System.out.println("Event Group Id :::: "+eventGroupId);
				prepareUsersByGroupId(eventId, groupId, eventGroupId, userEvents, studentUserIds, studentEventGroupMap, status, isInsert);
			}
		}
	}

	@Transactional
	private void updateIndividualEvent(Event existingEvent, Event event, List<UserEvent> userEvents){
		List<Integer> newIndividualIds = new ArrayList<>();
		List<Integer> modifiedIndividualIds = new ArrayList<>();
		List<Integer> deletedIndividualIds = new ArrayList<>();

		for(Integer userId : event.getIndividualIdList()){
			if(existingEvent.getIndividualIdList().contains(userId)){
				modifiedIndividualIds.add(userId);
			}
			else {
				newIndividualIds.add(userId);
			}
		}

		for(Integer userId : existingEvent.getIndividualIdList()){
			if(event.getIndividualIdList().contains(userId)){
				modifiedIndividualIds.add(userId);
			}
			else {
				deletedIndividualIds.add(userId);
			}
		}

		logger.debug("new Individuals --"+newIndividualIds);
		logger.debug("modified individuals --"+modifiedIndividualIds);
		logger.debug("deleted individuals --"+deletedIndividualIds);

		List<Integer> studentUserIds = new ArrayList<>();

		insertOrUpdateIndvidualEvents(event.getId(), newIndividualIds, userEvents, studentUserIds, Constants.RECORD_ADDED, true);
		prepareParentsByStudents(event, userEvents, studentUserIds, null, Constants.RECORD_ADDED, true);
		studentUserIds.clear();

		insertOrUpdateIndvidualEvents(event.getId(), modifiedIndividualIds, userEvents, studentUserIds, Constants.RECORD_MODIFIED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, null, Constants.RECORD_MODIFIED, false);
		studentUserIds.clear();

		insertOrUpdateIndvidualEvents(event.getId(), deletedIndividualIds, userEvents, studentUserIds, Constants.RECORD_DELETED, false);
		prepareParentsByStudents(event, userEvents, studentUserIds, null, Constants.RECORD_DELETED, false);
		studentUserIds.clear();
	}

	@Override
	public boolean updateEvent(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException {

		updateEventInDB(event, file);
		sendEventNotifications(event, true);
		return true;
	}

	@Transactional
	public boolean updateEventInDB(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException {

		Event existingEvent = getEventById(event.getId());
		updateEvent(event, existingEvent);

		List<UserEvent> userEvents = new ArrayList<>();
		updateIndividualEvent(existingEvent, event, userEvents);

		// firstly update into event group.
		updateEventGroup(existingEvent, event, userEvents);
		applyConstraintsOnUserEvents(existingEvent, event, userEvents);
		insertOrUpdateUserEvents(userEvents, false);
//		sendEventNotifications(event, true);
		return true;
	}


	/**
	 * Used only when going to update the event.
	 * Checks whether event schedule is changed. determined by the startDate, endDate, recurring, 
	 * if recurring is weekly then by weekdays else if  recurring is yearly or monthly then byDate
	 * @param existingEvent existing event object in database.
	 * @param event new Event Object to update.
	 * @return
	 */
	private boolean checkScheduleUpdated(Event existingEvent, Event event) {

		boolean isScheduleUpdated = false;

		if(!existingEvent.getStartDate().equals(event.getStartDate())){
			isScheduleUpdated = true;
		}
		else if(existingEvent.getEndDate() != null && !existingEvent.getEndDate().equals(event.getEndDate())){
			isScheduleUpdated = true;
		}
		else if(event.getEndDate() != null && !event.getEndDate().equals(existingEvent.getEndDate())){
			isScheduleUpdated = true;
		}
		else if(!event.getReccuring().equalsIgnoreCase(existingEvent.getReccuring())){
			isScheduleUpdated = true;
		}
		else if(event.getFrequency() != null && !event.getFrequency().equalsIgnoreCase(existingEvent.getFrequency())){
			isScheduleUpdated = true;
		}
		else if(event.getFrequency() != null && event.getFrequency().equalsIgnoreCase(existingEvent.getFrequency()) && event.getFrequency().equalsIgnoreCase("WEEKLY")){
			Set<String> existingDaySet = existingEvent.getEventWeekDays().keySet();
			Set<String> newDaySet = new HashSet<String> ();
			Map<String, Boolean> eventWeekDays = event.getEventWeekDays();
			
			for(String day : event.getEventWeekDays().keySet()){
				if(eventWeekDays.get(day).equals(true)){
					newDaySet.add(day);
				}
			}
			if(!newDaySet.containsAll(existingDaySet) || !existingDaySet.containsAll(newDaySet)){
				isScheduleUpdated = true;
			}			
		}
		else if(event.getFrequency() != null && event.getFrequency().equalsIgnoreCase(existingEvent.getFrequency()) && 
				(event.getFrequency().equalsIgnoreCase("MONTHLY") || event.getFrequency().equalsIgnoreCase("YEARLY"))){
			if(!event.getByDate().equals(existingEvent.getByDate())){
				isScheduleUpdated = true;
			}
		}
		logger.debug("is Event schedule updated --"+isScheduleUpdated);
		return isScheduleUpdated;
	}


	@Transactional
	private void updateEvent(final Event event, final Event existingEvent) {

		final String sql = "update event set title = ? , description = ? , venue = ? , startdate = ?, starttime = ?, " +
			"endtime = ?, alldayevent = ? , reminderSet = ? , branch_id = ? ,enddate = ?," +
			" reccuring = ?, course_id = ?, " +
			"summary = ?, forparent = ?, forstudent = ?," +
			" forteacher = ?, forvendor = ?, frequency = ?, by_day = ?, by_date = ?, " +
			"updated_date = ?, is_schedule_updated = ?, academic_year_id = ?, status =?, sent_by = ? "
			+(event.getReminderBefore() != null ? ", reminderbefore = ?" : "")+" where id = ?";

		final boolean isScheduleUpdated = checkScheduleUpdated(existingEvent, event);

		logger.debug("Updating event..."+event.getId());
		logger.debug("Schedule Updated..."+isScheduleUpdated);

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, event.getTitle());
				preparedStatement.setString(2, event.getDescription());
				preparedStatement.setString(3, event.getVenue());
				preparedStatement.setString(4, event.getStartDate());
				preparedStatement.setString(5, event.getStartTime());
				preparedStatement.setString(6, event.getEndTime());
				preparedStatement.setString(7, "N");
				preparedStatement.setString(8, event.getReminderSet());
				preparedStatement.setInt(9, event.getBranchId());
				preparedStatement.setString(10, (event.getEndDate() != null ) ? (event.getEndDate()) : null);
				
				preparedStatement.setString(11, event.getReccuring());
				
				preparedStatement.setInt(12, event.getCourseId());
				preparedStatement.setString(13, event.getSummary());
				preparedStatement.setString(14, event.getForParent());
				preparedStatement.setString(15, event.getForStudent());
				preparedStatement.setString(16, event.getForTeacher());
				preparedStatement.setString(17, event.getForVendor());
				preparedStatement.setString(18, event.getFrequency());
				preparedStatement.setString(19, event.getByDay());
				preparedStatement.setString(20, event.getByDate());
				preparedStatement.setString(21, sdfForSqlWithTime.format(new Date()));
				preparedStatement.setString(22, (isScheduleUpdated ? "Y" : "N"));
				preparedStatement.setInt(23, event.getAcademicYearId());
				preparedStatement.setString(24, "M");
				preparedStatement.setInt(25, event.getSentBy());

				if(event.getReminderBefore() != null){
					preparedStatement.setInt(26, event.getReminderBefore());
					preparedStatement.setInt(27, event.getId());
				}
				else {
					preparedStatement.setInt(26, event.getId());
				}
				return preparedStatement;
			}
		});
	}

	public Event getEventByIdToView(Integer id, Integer teacherUserId){
		logger.debug("Get event by id --"+id);
		String sql = "select e.*, (if(uers.event_id is null, 'N' , 'Y')) as readStatus, " +
			" i.id as instituteId " +
			" from event as e " +
			" INNER JOIN branch as b on b.id = e.branch_id " +
			" INNER JOIN institute as i on b.institute_id = i.id "+
			" INNER JOIN user_event as ue on ue.event_id = e.id " +
			" AND ue.user_id = " + teacherUserId +" AND ue.for_user_id = " + teacherUserId +
			" LEFT JOIN user_event_read_status as uers on uers.user_id = ue.user_id " +
			" AND uers.for_user_id = ue.for_user_id AND uers.event_id = ue.id " +
			" where e.status != '"+Constants.RECORD_DELETED+"' AND e.id = "+id;		
		System.out.println("SQL::::::::::::::\n"+sql);

		List<Event> eventList = getJdbcTemplate().query(sql, new RowMapper<Event>(){
			@Override
			public Event mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Event event = new Event();
				event.setId(resultSet.getInt("id"));
				event.setAllDayEvent(resultSet.getString("alldayevent"));
				event.setTitle(resultSet.getString("title"));
				event.setStartDate((resultSet.getString("startdate") != null ) ? (resultSet.getString("startdate"))  : null);
				event.setStartTime(resultSet.getString("starttime"));
				event.setEndDate((resultSet.getString("enddate") != null ) ? (resultSet.getString("enddate")) : null);
				event.setEndTime(resultSet.getString("endtime"));
				event.setInstituteId(resultSet.getInt("instituteId"));
				event.setReccuring(resultSet.getString("reccuring"));
				event.setVenue(resultSet.getString("venue"));
				event.setSummary(resultSet.getString("summary"));
				event.setDescription(resultSet.getString("description"));
				event.setReminderSet(resultSet.getString("reminderSet"));
				event.setReminderBefore((resultSet.getInt("reminderbefore") != 0 ? resultSet.getInt("reminderbefore") : null));
				event.setForParent(resultSet.getString("forparent"));
				event.setForStudent(resultSet.getString("forstudent"));
				event.setForTeacher(resultSet.getString("forteacher"));
				event.setForVendor(resultSet.getString("forvendor"));
				event.setImageUrl(resultSet.getString("image_url"));
				event.setFrequency(resultSet.getString("frequency"));
				event.setBranchId(resultSet.getInt("branch_id"));
				event.setAcademicYearId(resultSet.getInt("academic_year_id"));
				event.setByDate(resultSet.getString("by_date"));
				event.setSentBy(resultSet.getInt("sent_by"));
				String byDay = resultSet.getString("by_day");
				event.setReadStatus(resultSet.getString("readStatus"));

				if(byDay != null && byDay.length() > 0){
					String[] byDayArray = byDay.split(",");
					Map<String, Boolean> byDayMap = new HashMap<>();
					for(String day : byDayArray){
						byDayMap.put(day, true);
					}
					event.setEventWeekDays(byDayMap);
				}
				setEventGroups(event);
				setEventIndividuals(event);
				return event;
			}
		});
		Event event = eventList.isEmpty() ? null : eventList.get(0);
		return event;	
	}

	public Event getEventById(Integer id){
		String sql = "select e.*, i.id as instituteId from event as e, institute as i, branch as b " +
			"where  i.id = b.institute_id AND b.id = e.branch_id AND e.status != '"+Constants.RECORD_DELETED+"' AND e.id = "+id;		
		List<Event> eventList = getJdbcTemplate().query(sql, new RowMapper<Event>(){
			@Override
			public Event mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Event event = new Event();
				event.setId(resultSet.getInt("id"));
				event.setAllDayEvent(resultSet.getString("alldayevent"));
				event.setTitle(resultSet.getString("title"));
				event.setStartDate((resultSet.getString("startdate") != null ) ? (resultSet.getString("startdate"))  : null);
				event.setStartTime(resultSet.getString("starttime"));
				event.setEndDate((resultSet.getString("enddate") != null ) ? (resultSet.getString("enddate")) : null);
				event.setEndTime(resultSet.getString("endtime"));
				event.setInstituteId(resultSet.getInt("instituteId"));
				event.setReccuring(resultSet.getString("reccuring"));
				event.setVenue(resultSet.getString("venue"));
				event.setSummary(resultSet.getString("summary"));
				event.setDescription(resultSet.getString("description"));
				event.setReminderSet(resultSet.getString("reminderSet"));
				event.setReminderBefore((resultSet.getInt("reminderbefore") != 0 ? resultSet.getInt("reminderbefore") : null));
				event.setForParent(resultSet.getString("forparent"));
				event.setForStudent(resultSet.getString("forstudent"));
				event.setForTeacher(resultSet.getString("forteacher"));
				event.setForVendor(resultSet.getString("forvendor"));
				event.setImageUrl(resultSet.getString("image_url"));
				event.setFrequency(resultSet.getString("frequency"));
				event.setBranchId(resultSet.getInt("branch_id"));
				event.setAcademicYearId(resultSet.getInt("academic_year_id"));
				event.setByDate(resultSet.getString("by_date"));
				event.setSentBy(resultSet.getInt("sent_by"));
				String byDay = resultSet.getString("by_day");

				if(byDay != null && byDay.length() > 0){
					String[] byDayArray = byDay.split(",");
					Map<String, Boolean> byDayMap = new HashMap<>();
					for(String day : byDayArray){
						byDayMap.put(day, true);
					}
					event.setEventWeekDays(byDayMap);
				}
				setEventGroups(event);
				setEventIndividuals(event);
				return event;
			}
		});

		Event event = eventList.isEmpty() ? null : eventList.get(0);
		logger.debug("Returning event --"+(event != null ? event.getTitle() : "Problem"));
		return event;	
	}

	protected void setEventIndividuals(Event event) {
		final List<Individual> individualList = new ArrayList<Individual>();
		final List<Integer> individualIdList = new ArrayList<Integer>();

		String sql = "select ur.*, r.name as role, c.firstname, c.lastname " +
				" from contact as c " +
				" INNER JOIN user_role as ur on ur.user_id = c.user_id " +
				" INNER JOIN role as r on r.id = ur.role_id "+
				" INNER JOIN student_event as se on c.user_id = se.user_id "+
				" WHERE se.event_id = "+event.getId()+" AND ur.role_id != "+Constants.ROLE_ID_PARENT;

		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
				int userId = resultSet.getInt("user_id");
				individualIdList.add(userId);
				Individual individual = new Individual();
				individual.setId(userId);
				String firstName = resultSet.getString("firstname");
				String lastName = resultSet.getString("lastname");
				String name = (StringUtils.isEmpty(firstName) ? "" : firstName +" ") +(StringUtils.isEmpty(lastName) ? "" : lastName);
				individual.setName(name);
				individual.setRole(resultSet.getString("role"));
				individualList.add(individual);
				return null;
			}
		});
		event.setIndividualIdList(individualIdList);
		event.setIndividuals(individualList);
	}

	@Transactional
	protected void setEventGroups(Event event) {
		String sql = "select eg.*, g.id as groupId, g.name as groupName from " +
				"event_group as eg, `group` as g where event_id = "+event.getId()+"" +
				"  AND g.isDelete = 'N' AND g.active = 'Y' and eg.group_id = g.id " +
				" AND eg.status != '"+Constants.RECORD_DELETED+"'";

		final List<Integer> groupIdList = new ArrayList<Integer>();
		final List<Master> groupList = new ArrayList<Master>();
		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
				groupIdList.add(resultSet.getInt("group_id"));
				Master group = new Master();
				group.setName(resultSet.getString("groupName"));
				group.setId(resultSet.getInt("groupId"));
				groupList.add(group);
				return null;
			}
		});
		event.setGroupList(groupList);
		event.setGroupIdList(groupIdList);
	}

	public boolean deleteEvent(Integer id) {
		// delete from even_group first ..
		String sql = "update user_event set status = '"+Constants.RECORD_DELETED+"' where event_id = "+id;
		getJdbcTemplate().update(sql);
					
		// delete from even_group first ..
		sql = "update event_group set status = '"+Constants.RECORD_DELETED+"' where event_id = "+id;
		getJdbcTemplate().update(sql);
		
		// delete from student_event
		sql = "update student_event set status = '"+Constants.RECORD_DELETED+"' where event_id = "+id;
		getJdbcTemplate().update(sql);
		
		// last delete from event
		sql = "update event set status = '"+Constants.RECORD_DELETED+"' where id = "+id;
		getJdbcTemplate().update(sql);
		logger.debug("event deleted.."+id);
		return true;
	}


	public List<Event> getEventsByBranchId(Integer branchId) {
		String sql = "select e.*, c.name as courseName, b.name as branchName from " +
					"event as e, branch as b, course as c where e.branch_id = b.id " +
					"AND e.course_id = c.id AND e.branch_id = "+branchId+
					" AND academic_year_id = (select id from academic_year where " +
					"is_current_active_year = 'Y' and branch_id = "+branchId+")" +
					" AND e.status != '"+Constants.RECORD_DELETED+"' order by startdate, starttime desc";

		sql = "select e.*, c.name as courseName, b.name as branchName, " +
			" concat(ifnull(contact. firstname, ''), ' ',ifnull(contact.lastname, '')) as name " +
			" from event as e INNER JOIN contact as contact on contact.user_id = e.sent_by " +
			" INNER JOIN branch as b on b.id = e.branch_id " +
			" INNER JOIN course as c on c.id = e.course_id " +
			" WHERE academic_year_id = (select id from academic_year where " +
			" is_current_active_year = 'Y' and branch_id = "+branchId+")" +
			" AND e.status != '"+Constants.RECORD_DELETED+"' order by startdate, starttime desc";

		logger.debug("get events by branch sql --"+sql);
		
		List<Event> eventList = getJdbcTemplate().query(sql, new RowMapper<Event>(){
			@Override
			public Event mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Event event = new Event();
				event.setId(resultSet.getInt("id"));
				event.setTitle(resultSet.getString("title"));
				event.setDescription(resultSet.getString("description"));
				event.setVenue(resultSet.getString("venue"));
				event.setStartDate(resultSet.getString("startdate"));
				event.setEndDate(resultSet.getString("enddate"));
				event.setStartTime(resultSet.getString("starttime"));
				event.setEndTime(resultSet.getString("endTime"));
				event.setBranchName(resultSet.getString("branchName"));
				event.setCourseName(resultSet.getString("courseName"));
				event.setReccuring(resultSet.getString("reccuring"));
				event.setCreatedDate(resultSet.getTimestamp("created_date"));
				event.setForParent(resultSet.getString("forparent"));
				event.setForTeacher(resultSet.getString("forteacher"));
				event.setForVendor(resultSet.getString("forvendor"));
				event.setForStudent(resultSet.getString("forstudent"));
				event.setReminderSet(resultSet.getString("reminderSet"));
				event.setReminderBefore(resultSet.getInt("reminderbefore"));
				event.setFrequency(resultSet.getString("frequency"));
				event.setSentBy(resultSet.getInt("sent_by"));
				event.setSentByName(resultSet.getString("name"));
				return event;
			}
		});
		return eventList;
	}

	@Transactional
	protected String getSentBy(final Integer sentBy) {
		String sql = "select concat(ifnull(firstname, ''), ' ',ifnull(lastname, '')) as name from contact where user_id = " + sentBy;

		List<String> nameList = getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getString("name");
			}
		});

		String name = "";
		if(nameList != null && nameList.size() > 0){
			name = nameList.get(0);
		}
		System.out.println("Sent by name is ----"+name);
		return name;
	}

	@Transactional
	public List<Event> getTeacherEvents(Integer teacherUserId, Integer branchId){
		String sql = "select e.*, c.name as courseName, b.name as branchName, ue.*  " +
				" from event as e " +
				" INNER JOIN branch as b on b.id = e.branch_id AND b.id = "+branchId +
				" INNER JOIN institute as i on i.id = b.institute_id" +
				" INNER JOIN course as c on e.course_id = c.id " +
				" INNER JOIN academic_year as ay on ay.id = e.academic_year_id AND ay.is_current_active_year = 'Y' " +
				" INNER JOIN user_event as ue on ue.event_id = e.id " +
				" AND ue.user_id = "+teacherUserId+" AND ue.for_user_id = "+teacherUserId+
				" WHERE e.status != '"+Constants.RECORD_DELETED+"' " +
				" AND ue.status != '"+Constants.RECORD_DELETED+"' AND e.sent_by != ue.user_id";

		logger.debug("get teacher event SQL --"+sql);

		List<Event> eventList = getJdbcTemplate().query(sql, new RowMapper<Event>(){
			@Override
			public Event mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Event event = new Event();
				event.setId(resultSet.getInt("id"));
				event.setTitle(resultSet.getString("title"));
				event.setDescription(resultSet.getString("description"));
				event.setVenue(resultSet.getString("venue"));
				event.setStartDate(resultSet.getString("startdate"));
				event.setEndDate(resultSet.getString("enddate"));
				event.setStartTime(resultSet.getString("starttime"));
				event.setEndTime(resultSet.getString("endTime"));
				event.setBranchName(resultSet.getString("branchName"));
				event.setCourseName(resultSet.getString("courseName"));
				event.setReccuring(resultSet.getString("reccuring"));
				event.setCreatedDate(resultSet.getTimestamp("created_date"));
				event.setForParent(resultSet.getString("forparent"));
				event.setForParent(resultSet.getString("forteacher"));
				event.setForParent(resultSet.getString("forvendor"));
				event.setForParent(resultSet.getString("forstudent"));
				event.setReminderSet(resultSet.getString("reminderSet"));
				event.setReminderBefore(resultSet.getInt("reminderbefore"));
				event.setFrequency(resultSet.getString("frequency"));
				event.setSentBy(resultSet.getInt("sent_by"));
				event.setSentByName(getSentBy(event.getSentBy()));
				return event;
			}
		});
		return eventList;
	}

	
	@Transactional
	public List<Event> getSentEventByTeacher(Integer branchId, boolean isTeacher, Integer teacherUserId){
		List<Event> eventList = new ArrayList<Event>();
		
		String sql = null;
		if (isTeacher) {
			sql = "select * from event where sent_by = " + teacherUserId + " AND " +
					  "academic_year_id = (select id from academic_year where " +
					  "is_current_active_year = 'Y' and branch_id = "+branchId+") and status != '" + Constants.RECORD_DELETED + "' order by id desc";
		} else {
			sql = "select * from event where branch_id = " + branchId + " AND " +
				  "academic_year_id = (select id from academic_year where " +
				  "is_current_active_year = 'Y' and branch_id = "+branchId+") and status != '" + Constants.RECORD_DELETED + "' order by id desc";
		}

		logger.debug("get all event SQL --"+sql);
		eventList = getJdbcTemplate().query(sql , new RowMapper<Event>(){
			@Override
			public Event mapRow(ResultSet rs, int rownumber)throws SQLException {
				Event event = new Event();
				event.setId(rs.getInt("id"));
				event.setTitle(rs.getString("title"));
				event.setDescription(rs.getString("description"));
				event.setVenue(rs.getString("venue"));
				event.setStartDate(rs.getString("startdate"));
				event.setEndDate(rs.getString("enddate"));
				event.setStartTime(rs.getString("starttime"));
				event.setEndTime(rs.getString("endTime"));
				event.setReccuring(rs.getString("reccuring"));
				event.setReminderSet(rs.getString("reminderSet"));
				event.setReminderBefore(rs.getInt("reminderbefore"));
				event.setForStudent(rs.getString("forstudent"));
				event.setForParent(rs.getString("forparent"));
				event.setForTeacher(rs.getString("forteacher"));
				event.setForVendor(rs.getString("forvendor"));
				event.setFrequency(rs.getString("frequency"));
				event.setBranchId(rs.getInt("branch_id"));
				event.setCourseId(rs.getInt("course_id"));
				event.setSentBy(rs.getInt("sent_by"));
				event.setSentByName(getSentBy(event.getSentBy()));
				return event;
			}
		});
		return eventList;
	}
}