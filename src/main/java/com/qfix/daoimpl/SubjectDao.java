package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.qfix.dao.IStandardSubjectDao;
import com.qfix.dao.ISubjectDao;
import com.qfix.model.Course;
import com.qfix.model.Standard;
import com.qfix.model.Subject;

@Slf4j
@Repository
public class SubjectDao extends JdbcDaoSupport implements ISubjectDao{
	
	@Autowired
	private IStandardSubjectDao standardSubjectDao;

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	@Transactional
	public boolean uploadSubject(List<Subject> subjectList, Integer branchId) {
		for(Subject subject : subjectList){
			insertSubject(subject);
		}
		return true;
	}

	@Override
	@Transactional
	public List<Subject> getSubjectsByBranchId(Integer branchId) {
		String sql = "select * from subject where branch_id = "+branchId+" AND is_delete = 'N'";

		List<Subject> subjectList = getJdbcTemplate().query(sql, new RowMapper<Subject>(){
			@Override
			public Subject mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Subject subject = new Subject();
				subject.setId(resultSet.getInt("id"));
				subject.setName(resultSet.getString("name"));
				subject.setCode(resultSet.getString("code"));
				subject.setCategoryId(resultSet.getInt("category_id"));
				subject.setTheoryMarks(resultSet.getDouble("theory"));
				subject.setPracticalMarks(resultSet.getDouble("practical"));
				subject.setBranchId(resultSet.getInt("branch_id"));
				return subject;
			}
		} );
		return subjectList;
	}

	@Override
	@Transactional
	public List<Subject> getSubjectsByStandardId(Integer branchId, Integer standardId) {
		String sql = "select * from subject where branch_id = "
				+ branchId
				+ " and is_delete = 'N' AND id in (select subject_id from standard_subject where standard_id = "
				+ standardId + " and is_delete = 'N');";

		List<Subject> subjectList = getJdbcTemplate().query(sql, new RowMapper<Subject>(){
			@Override
			public Subject mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Subject subject = new Subject();
				subject.setId(resultSet.getInt("id"));
				subject.setName(resultSet.getString("name"));
				subject.setCode(resultSet.getString("code"));
				subject.setCategoryId(resultSet.getInt("category_id"));
				subject.setTheoryMarks(resultSet.getDouble("theory"));
				subject.setPracticalMarks(resultSet.getDouble("practical"));
				subject.setBranchId(resultSet.getInt("branch_id"));
				return subject;
			}
		} );
		return subjectList;
	}

	@Override
	@Transactional
	public boolean updateSubject(final Subject subject) {
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "update subject set name = ?, code = ?, " +
						"branch_id = ?, theory = ?, practical = ?, category_id = ? where id = ?";
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setString(1, subject.getName());
				ps.setString(2, subject.getCode());
				ps.setInt(3, subject.getBranchId());
				ps.setDouble(4, subject.getTheoryMarks());
				ps.setDouble(5, subject.getPracticalMarks());
				ps.setInt(6, subject.getCategoryId());
				ps.setInt(7, subject.getId());
				return ps;
			}
		});

		return true;
	}


	@Override
	@Transactional
	public boolean insertSubject(final Subject subject) {
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into subject (name, code , branch_id, " +
						"theory, practical, category_id, is_delete) values (?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setString(1, subject.getName());
				ps.setString(2, subject.getCode());
				ps.setInt(3, subject.getBranchId());
				ps.setDouble(4, subject.getTheoryMarks());
				ps.setDouble(5, subject.getPracticalMarks());
				ps.setInt(6, subject.getCategoryId());
				ps.setString(7, "N");
				return ps;
			}
		});

		return true;
	}
	
	public boolean insertSubjectExpress(final Subject subject) {
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into subject (name , branch_id, " +
						"is_delete) values (?, ?, ?)";
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setString(1, subject.getName());
				ps.setInt(2, subject.getBranchId());
				ps.setString(3, "N");
				return ps;
			}
		});

		return true;
	}


	@Override
	@Transactional
	public Subject getSubjectById(Integer id) {
		String sql = "select s.*, i.id as instituteId from subject as s " +
				"INNER JOIN branch as b on b.id = s.branch_id " +
				"INNER JOIN institute as i on i.id = b.institute_id where s.id = "+id;

		Subject subject = getJdbcTemplate().query(sql, new RowMapper<Subject>(){
			@Override
			public Subject mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Subject subject = new Subject();
				subject.setId(resultSet.getInt("id"));
				subject.setName(resultSet.getString("name"));
				subject.setCode(resultSet.getString("code"));
				subject.setTheoryMarks(resultSet.getDouble("theory"));
				subject.setPracticalMarks(resultSet.getDouble("practical"));
				subject.setBranchId(resultSet.getInt("branch_id"));
				subject.setInstituteId(resultSet.getInt("instituteId"));
				return subject;
			}
		}).get(0);
		return subject;
	}

	@Override
	@Transactional
	public boolean deleteSubject(Integer id) {
		String sql = "update subject set is_delete = 'Y' where id = "+id;
		getJdbcTemplate().update(sql);

		sql = "update standard_subject set is_delete = 'Y' where subject_id = "+id;
		getJdbcTemplate().update(sql);
		return true;
	}

	@Override
	@Transactional
	public Map<String, Integer> insertSubjects(final Integer branchId,List<Subject> subjects) {
		Map<String, Integer> subjectMap = new HashMap<String, Integer>();
		final String sql = "insert into subject (branch_id, name, code,tag) " +
				 "values (?, ?, ?,?)";

		for(final Subject subject : subjects){
			KeyHolder holder = new GeneratedKeyHolder();
			final String tag = subject.getTag();
			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					ps.setInt(1, branchId);
					ps.setString(2, subject.getName());
					ps.setString(3, null);
					ps.setString(4, tag);
					return ps;
				}
			}, holder);
			subject.setId(holder.getKey().intValue());
			subjectMap.put(subject.getName(), subject.getId());
		}
		return subjectMap;
	}
	
	@Transactional
	@Override
	public List<Subject> listSubjects(String type){
		String query = null;
		
		if(type.equals("COLLEGE")){
			query = "SELECT * FROM college_subject";
		}else if(type.equals("SCHOOL")){
			query = "SELECT * FROM school_subject";
		}else{
			query = null;
		}
		
		if(query != null){
			return getJdbcTemplate().query(query, new BeanPropertyRowMapper(Subject.class));
		}else{
			return null;
		}
	}
	
	
	
	public Standard getStandardByName(String name, Integer branchId, Integer academicYearId){
		String sql = "select id, displayName, branch_id as branchId from standard where active = 'Y' "
				+ "and academic_year_id = "+academicYearId+" and displayName = '" + name +"' and branch_id = " + branchId;
		log.info(sql);
		List<Standard> standard = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Standard>(Standard.class));
		if(standard.size() == 0)
			return null;
		
		return standard.get(0);
	}
	
	public Subject getStandardSubjectByName(String name, Integer standardId, Integer branchId){
		String sql = "select s.id, branch_id as branchId,  name branchId from subject s "
				+ "join standard_subject ss on ss.subject_id = s.id "
				+ "where name = '"+name+"' and branch_id = "+branchId+" and s.is_delete = 'N' "
				+ "and ss.standard_id = " + standardId;
		
		List<Subject> subjects = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Subject>(Subject.class));
		return subjects.get(0);
	}
	
	public Subject getSubjectByName(String name, Integer branchId){
		String sql = "select name, id from subject where name ='"+name+"' and branch_id = "+branchId+" and is_delete = 'N' ";
		log.info(sql);
		List<Subject> subject = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Subject>(Subject.class));
		if(subject.size() == 0)
			return null;
		return subject.get(0);
	}
	
	public Course getCourseByName(String courseName){
		String sql = "select * from college_courses where name = '" + courseName + "'";
		log.info(sql);
		List<Course> course = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Course>(Course.class));
		if(course.size() == 0)
			return null;
		return course.get(0);
	}
	
	/*public void addSubjectForBranch(String name, Integer branchId, List<Subject> subjects){
		
		 * For a standard iterate through whole subjects list.
		 * If the subject is present in the subject table for a branch 
		 * then remove it from the list. Add the rest of the list in the 
		 * database
		 * 
		 * 
		Iterator<Subject> itr = subjects.iterator();
		while(itr.hasNext()){
			if(getSubjectByName(itr.next().getName(), branchId) != null)
				itr.remove();
		}
		List<Subject> subjectList = (List<Subject>)subjects;
		insertSubjects(branchId, subjectList);
	}*/
	
	public void insertSubjects(String standardName, Integer branchId, List<String> list, Integer academicYearId){
		
		/*
		 * 1. Get Standard by name
		 * 2. Iterate through the subject list
		 * 3. Check if the subject is present in the subject table based on the branch id. If it isn't present then
		 * 		store it, else don't store it.
		 * 4. 
		 * */
		
		Standard standard = getStandardByName(standardName, branchId, academicYearId);
		if(standard == null){
			log.info("No standard present with name " + standardName);
			return;
		}
			
		for(String subjectName : list){
			Subject s =  getSubjectByName(subjectName, branchId);
			if(s == null){
				Subject subject = new Subject();
				subject.setName(subjectName);
				subject.setBranchId(branchId);
				insertSubjectExpress(subject);
				s = getSubjectByName(subject.getName(), branchId);
			} 
			standardSubjectDao.insertStandardSubject(standard.getId(), s.getId());
		}
		
		
	}
	
	public void removeSubjects(String standardName, Integer branchId, List<String> subjectsList, Integer academicYearId){
		if(subjectsList.size() <= 0)
			return;
		
		Standard standard = getStandardByName(standardName, branchId, academicYearId);
		if(standard == null){
			log.info("No standard present with name " + standardName);
			return;
		}
		for(String subjectName : subjectsList){
			Subject s =  getSubjectByName(subjectName, branchId);
			if(s == null){
				log.info("No subject with name " + subjectName);
				continue;
			}
			log.info("Deleting standard : "+standardName + " subject : " + s.getName());
			standardSubjectDao.deleteStandardSubject(standard.getId(), s.getId());
		}
		
	}

	@Override
	public void insertYearCourseSubject(String standardName, Integer branchId,
			Integer academicYearId, List<String> courseList,
			List<String> subjectList) {
		Standard standard = getStandardByName(standardName, branchId, academicYearId);
		if(standard == null){
			log.info("No standard present with name " + standardName);
			return;
		}
		
		for(String courseName : courseList){
			Course course = getCourseByName(courseName);
			for(String subjectName : subjectList){
				Subject s =  getSubjectByName(subjectName, branchId);
				if(s == null){
					Subject subject = new Subject();
					subject.setName(subjectName);
					subject.setBranchId(branchId);
					insertSubjectExpress(subject);
					s = getSubjectByName(subject.getName(), branchId);
				} 
				standardSubjectDao.insertYearCourseSubject(standard.getId(), branchId, s.getId(), course.getId(), academicYearId);
			}
		}
		
	}

	@Override
	public void removeYearCourseSubject(String standardName, Integer branchId,
			Integer academicYearId, List<String> courseList,
			List<String> subjectList) {
		
		Standard standard = getStandardByName(standardName, branchId, academicYearId);
		if(standard == null){
			log.info("No standard present with name " + standardName);
			return;
		}
		
		for(String courseName : courseList){
			Course course = getCourseByName(courseName);
			for(String subjectName : subjectList){
				Subject s =  getSubjectByName(subjectName, branchId);
				if(s == null){
					log.info("No subject with name : " + subjectName);
					continue;
				} 
				standardSubjectDao.deleteYearCourseSubject(s.getId(), branchId, s.getId(), course.getId(), academicYearId);
			}
		}
	}
	
	@Override
	public List<Subject> getSubjectsByStandardId(Integer branchId, Integer standardId, Integer academicYearId){
		String sql = "select * from subject where id in (select distinct subject_id from year_course_subject where standard_id = "+standardId+" and branch_id = "+standardId+" and  academic_year_id = "+academicYearId+")";
		log.info(sql);
		List<Subject> subjectList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Subject>(Subject.class));
		return subjectList;
	}
	
	

		
}