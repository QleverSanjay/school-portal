package com.qfix.daoimpl;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.config.AppProperties;
import com.qfix.dao.InstituteInterfaceDao;
import com.qfix.exceptions.InstituteAllreadyExistException;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Institute;
import com.qfix.service.file.FileFolderService;
import com.qfix.service.user.client.chat.response.session.UserResponse;
import com.qfix.utilities.Constants;
import com.qfix.utilities.FileConstants;
import com.qfix.utilities.SecurityUtil;

@Repository
public class InstituteDao extends JdbcDaoSupport implements
		InstituteInterfaceDao {

	@Autowired
	private DriverManagerDataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private List<Institute> allreadyExistInstituteAdminList;
	private List<Institute> allreadyExistInstituteList;

	@Autowired
	private AppProperties appProperties;

	final static Logger logger = Logger.getLogger(InstituteDao.class);
	
	@Autowired
	private FileFolderService fileFolderService;

	@Transactional
	public List<Institute> getAll() {
		String sql = "select i.*, sa.*, u.id as userId from institute as i, school_admin as sa, user as u "
				+ "where i.isDelete = 'N' AND sa.isDelete = 'N' AND u.institute_id = i.id "
				+ "AND sa.user_id = u.id order by i.code, i.name";

		return getJdbcTemplate().query(sql, new RowMapper<Institute>() {
			@Override
			public Institute mapRow(ResultSet resultSet, int rowNum)
					throws SQLException {
				Institute institute = new Institute();
				institute.setId(resultSet.getInt("i.id"));
				institute.setInstituteName(resultSet.getString("name"));
				institute.setCode(resultSet.getString("code"));
				institute.setFirstName(resultSet.getString("firstName"));
				institute.setLastName(resultSet.getString("lastName"));
				institute.setAdminCity(resultSet.getString("city"));
				institute.setAdminPinCode(resultSet.getString("pincode"));
				institute.setPrimaryContact(resultSet
						.getString("primaryContact"));
				institute.setAdminEmail(resultSet.getString("emailAddress"));
				institute.setUserId(resultSet.getInt("userId"));
				institute.setGender(resultSet.getString("gender"));
				institute.setActive(resultSet.getString("active"));
				return institute;
			}
		});
	}

	@Transactional
	public boolean uploadInstitute(List<Institute> instituteList)
			throws UserAllreadyExistException, InstituteAllreadyExistException,
			IOException, Exception {
		boolean flag = true;
		for (Institute institute : instituteList) {
			logger.debug("Inserting institute...");
			institute.setActive("Y");
			insertInstitute(institute, null);
			logger.debug("institute inserted.." + institute.getInstituteName());

		}

		if (allreadyExistInstituteAdminList != null
				&& allreadyExistInstituteAdminList.size() > 0) {
			throw new UserAllreadyExistException("Following institute`s admin"
					+ (allreadyExistInstituteAdminList.size() > 0 ? "s are"
							: " is")
					+ " already registered or email id is used.");
		}
		if (allreadyExistInstituteList != null
				&& allreadyExistInstituteList.size() > 0) {
			throw new InstituteAllreadyExistException(
					"Following Institutes already exist");
		}
		return flag;
	}

	public List<Institute> getAllreadyExistInstituteAdminList() {
		return allreadyExistInstituteAdminList;
	}

	public List<Institute> getAllreadyExistInstituteList() {
		return allreadyExistInstituteList;
	}

	private void createChatProfile(String email, String firstName,
			String lastName, Integer userId) throws SQLException {

		ChatDao chatDao = new ChatDao();
		UserResponse userResponse = chatDao.createChatAccount(email, firstName,
				lastName, "");
		String sql = "insert into user_chat_profile (login, password, email, chat_user_profile_id, user_id) values (?,?,?,?,?)";

		Object[] params = { userResponse.getLogin(),
				userResponse.getPassword(), userResponse.getEmail(),
				userResponse.getChatUserProfileId(), userId };

		int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.DECIMAL, Types.INTEGER };

		getJdbcTemplate().update(sql, params, types);

		logger.debug("Chat profile created for..." + email);
	}

	private String saveFile(MultipartFile file, Institute institute,
			boolean isUpdate) throws Exception {
		String filePath = null;
		// && student.isFileChanegd()
		if (file != null) {
			String parentPath = institute.getId() + File.separator;

			String fileExtension = file.getOriginalFilename().substring(
					file.getOriginalFilename().lastIndexOf("."));
			String fileNameWithExtension = org.apache.commons.lang3.RandomStringUtils
					.random(30, true, true) + fileExtension;

			if (isUpdate) {
				String logoUrl = institute.getLogoUrl();
				fileNameWithExtension = (StringUtils.isEmpty(logoUrl)
						|| logoUrl.endsWith(FileConstants.DEFAULT_IMAGE_NAME) ? fileNameWithExtension
						: logoUrl.substring(logoUrl.lastIndexOf("/") + 1));
			} else {
				fileFolderService.createExternalStorage(parentPath);
			}

			filePath = fileFolderService.createFileInExternalStorage(
					parentPath, fileNameWithExtension, file.getBytes());
		} else if (!isUpdate) {
			filePath = appProperties.getFileProps().getExternalDocumentsPath()
					+ FileConstants.DEFAULT_IMAGE_NAME;

		}
		filePath = FilenameUtils.separatorsToUnix(filePath);
		logger.debug("institute logo saved ..." + filePath);
		return filePath;
	}

	@Transactional
	public boolean insertInstitute(Institute institute, MultipartFile file)
			throws Exception {

		boolean flag = true;

		try {
			logger.debug("Insert institute started..."
					+ institute.getInstituteName());
			saveInstitute(institute);
			Integer instituteId = getInstituteIdBasedOnInstituteName(institute
					.getInstituteName());
			String username = RandomStringUtils.random(10, true, true) + "@"
					+ Constants.DOMAIN_NAME;
			institute.setUsername(username);

			saveUser(institute, instituteId);
			Integer userId = getUserIdBasedOnUsername(institute.getAdminEmail());
			saveUserRole(userId);
			saveSchoolAdmin(institute, userId);

			insertIntoContact(institute, userId);
			institute.setId(instituteId);
			String photoPath = saveFile(file, institute, false);
			updatePhotoPath(institute.getId(), photoPath);

			// createChatProfile(institute.getUsername(),
			// institute.getFirstName() , institute.getLastName(), userId);
			logger.debug("insert institute completed..");

		} catch (SQLException e) {
			flag = false;
			logger.error("", e);
		}
		return flag;
	}

	private void insertIntoContact(Institute institute, Integer userId) {
		String sql = "insert into contact (email, phone, user_id, photo, firstname, lastname, city) "
				+ "values (?, ?, ?, ?, ?, ?, ?)";

		Object[] params = new Object[] { institute.getAdminEmail(),
				institute.getPrimaryContact(), userId, null,
				institute.getFirstName(), institute.getLastName(),
				institute.getAdminCity() };

		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

		getJdbcTemplate().update(sql, params, types);
		System.out.println("INSERT INTO CONTACT DONE ::::::::::::::::::::::");
	}

	private void saveUserRole(Integer userId) {
		String userRoleSql = "insert into user_role (user_id, role_id) values (?, ?)";
		Object[] params = new Object[] { userId, Constants.ROLE_ID_SCHOOL_ADMIN };
		int[] types = new int[] { Types.INTEGER, Types.INTEGER };
		getJdbcTemplate().update(userRoleSql, params, types);
	}

	private Integer getInstituteIdBasedOnInstituteName(String instituteName) {
		String sqlQuery = "select id from institute where name = '"
				+ instituteName + "'";
		return getJdbcTemplate().queryForObject(sqlQuery, Integer.class);
	}

	private Integer getUserIdBasedOnUsername(String username) {
		String sqlQuery = "select id from user where username = '" + username
				+ "' ";
		return getJdbcTemplate().queryForObject(sqlQuery, Integer.class);
	}

	private void saveUser(Institute institute, Integer instituteId)
			throws Exception {
		String userSql = "insert into user (username, password, password_salt, temp_password, active, "
				+ "institute_id, isDelete) values (?, ?, ?, ?, ?, ?, ?)";

		String password = (institute.getFirstName().replaceAll(
				"([\\() \\.\\-_])", "") + "123").toUpperCase();
		byte[] randomSalt = SecurityUtil.generateSalt();
		byte[] passwordHash = SecurityUtil.hashPassword(password, randomSalt);

		Object[] params = new Object[] { institute.getAdminEmail(),
				passwordHash, randomSalt, password, institute.getActive(),
				instituteId, "N" };
		int[] types = new int[] { Types.VARCHAR, Types.BINARY, Types.BINARY,
				Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR };
		getJdbcTemplate().update(userSql, params, types);

	}

	private void updateUser(Institute institute) {
		String userSql = "update user set username= ?, active = ? where institute_id = ?";
		Object[] params = new Object[] { institute.getAdminEmail(),
				institute.getActive(), institute.getId() };
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER };
		getJdbcTemplate().update(userSql, params, types);
	}

	private void saveInstitute(Institute institute) {
		String instituteSql = "insert into institute (name, code, active, logo_url, isDelete) values(?, ?, ?, ?, ?)";
		Object[] params = new Object[] { institute.getInstituteName(),
				institute.getCode(), institute.getActive(),
				institute.getLogoUrl(), "N" };
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR };
		getJdbcTemplate().update(instituteSql, params, types);
	}

	private void updateInstituteDetails(Institute institute) {
		String instituteSql = "update institute set code = ?, name = ?, active = ?, isDelete = ? where id = ?";
		Object[] params = new Object[] { institute.getCode(),
				institute.getInstituteName(), institute.getActive(), "N",
				institute.getId() };
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.INTEGER };
		getJdbcTemplate().update(instituteSql, params, types);
	}

	private void saveSchoolAdmin(Institute institute, Integer userId) {
		String schoolAdminSql = "insert into school_admin (firstName, lastName, emailAddress, gender, primaryContact,user_id, active, city, pincode, isDelete) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		Object[] params = new Object[] { institute.getFirstName(),
				institute.getLastName(), institute.getAdminEmail(),
				institute.getGender(), institute.getPrimaryContact(), userId,
				institute.getActive(), institute.getAdminCity(),
				institute.getAdminPinCode(), "N" };
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		getJdbcTemplate().update(schoolAdminSql, params, types);
	}

	private void updatePhotoPath(Integer id, String photoPath) {
		String sql = "update institute set logo_url = '" + photoPath
				+ "' where id = " + id;
		getJdbcTemplate().update(sql);
	}

	@Transactional
	public Institute getInstituteById(Integer instituteId) {
		String sql = "select i.id as instituteId, i.*, sa.*, u.id as userId from institute as i, school_admin as sa, user as u "
				+ "where u.institute_id = i.id AND sa.user_id = u.id AND i.id = "
				+ instituteId;

		List<Institute> instituteList = getJdbcTemplate().query(sql,
				new RowMapper<Institute>() {
					@Override
					public Institute mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
						Institute institute = new Institute();
						institute.setId(resultSet.getInt("instituteId"));
						institute.setInstituteName(resultSet.getString("name"));
						institute.setCode(resultSet.getString("code"));
						institute.setLogoUrl(resultSet.getString("logo_url")
								+ "?"
								+ Calendar.getInstance().getTimeInMillis());
						institute.setFirstName(resultSet.getString("firstName"));
						institute.setLastName(resultSet.getString("lastName"));
						institute.setAdminEmail(resultSet
								.getString("emailAddress"));
						institute.setGender(resultSet.getString("gender"));
						institute.setPrimaryContact(resultSet
								.getString("primaryContact"));
						institute.setAdminCity(resultSet.getString("city"));
						institute.setAdminPinCode(resultSet
								.getString("pinCode"));
						institute.setActive(resultSet.getString("active"));
						institute.setIsDelete(resultSet.getString("isDelete"));
						return institute;
					}
				});

		logger.debug("get institute by id SQL --" + sql);
		return instituteList.get(0);
	}

	@Transactional
	@Override
	public boolean updateInstitute(Institute institute, MultipartFile file) {
		boolean flag = false;
		logger.debug("Update institute started.."
				+ institute.getInstituteName());

		updateInstituteStatus(institute);
		updateInstituteDetails(institute);
		updateUser(institute);
		Integer userId = getUserIdBasedOnUsername(institute.getAdminEmail());
		updateSchoolAdmin(institute, userId);
		flag = true;
		institute.setLogoUrl(getLogoUrlBasedOnInstituteId(institute.getId()));
		String photoPath = null;
		try {
			photoPath = saveFile(file, institute, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (photoPath != null) {
			updatePhotoPath(institute.getId(), photoPath);
		}

		logger.debug("Institute updated..." + institute.getInstituteName());
		return flag;
	}

	@Transactional
	private void updateInstituteStatus(final Institute institute) {
		String sql = "select active from institute where id = "
				+ institute.getId();

		getJdbcTemplate().query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet resultSet) throws SQLException {

				if (!institute.getActive()
						.equals(resultSet.getString("active"))) {
					if (institute.getActive().equals("N")) {
						deactivateInstitute(institute.getId());
						logger.debug("Institute De-Activated :: name - "
								+ institute.getInstituteName());
					} else if (institute.getActive().equals("Y")) {
						activateInstitute(institute.getId());
						logger.debug("Institute Activated :: name - "
								+ institute.getInstituteName());
					}
				}
			}
		});
	}

	@Transactional
	private void activateInstitute(int instituteId) {
		activateInstituteRelatedEntities(instituteId);

		String branchSelectSql = "select id from branch where institute_id = "
				+ instituteId;

		getJdbcTemplate().query(branchSelectSql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				activateBranchData(rs.getInt("id"));
			}
		});
	}

	@Transactional
	private void activateBranchData(int branchId) {
		String teacherUserSelectSql = "select user_id from teacher where branch_id = "
				+ branchId;
		String studentUserSql = "update user set active = 'Y' where id in (select user_id from student where branch_id = "
				+ branchId + ")";
		/*
		 * String eventSql =
		 * "update event set active = 'Y' where branch_id = "+branchId; String
		 * noticeSql =
		 * "update notice set active = 'Y' where branch_id = "+branchId; String
		 * feesSql = "update fees set active = 'Y' where branch_id = "+branchId;
		 */

		String studentSql = "update student set active = 'Y' where branch_id = "
				+ branchId;
		String groupSql = "update `group` set active = 'Y' where branch_id = "
				+ branchId;

		getJdbcTemplate().query(teacherUserSelectSql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet resultSet) throws SQLException {
				String teacherUserSql = "insert into user_role values("
						+ resultSet.getInt("user_id") + ", "
						+ Constants.ROLE_ID_TEACHER + ")";
				getJdbcTemplate().update(teacherUserSql);
			}
		});
		getJdbcTemplate().update(studentUserSql);
		getJdbcTemplate().update(groupSql);
		// getJdbcTemplate().update(eventSql);
		/*
		 * getJdbcTemplate().update(noticeSql);
		 * getJdbcTemplate().update(feesSql);
		 */

		getJdbcTemplate().update(studentSql);
	}

	@Transactional
	private void deactivateInstitute(Integer id) {
		deactivateInstituteRelatedEntities(id);

		String branchSelectSql = "select id from branch where institute_id = "
				+ id;

		getJdbcTemplate().query(branchSelectSql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				deactivateBranchData(rs.getInt("id"));
			}
		});
	}

	@Transactional
	private void deactivateBranchData(int branchId) {
		String teacherUserSql = "delete from user_role where user_id in "
				+ "(select user_id from teacher where branch_id = " + branchId
				+ ") AND role_id = " + Constants.ROLE_ID_TEACHER;
		String studentUserSql = "update user set active = 'N' where id in (select user_id from student where branch_id = "
				+ branchId + ")";
		// String eventSql =
		// "update event set active = 'N' where branch_id = "+branchId;
		// String noticeSql =
		// "update notice set active = 'N' where branch_id = "+branchId;
		// String feesSql =
		// "update fees set active = 'N' where branch_id = "+branchId;
		String studentSql = "update student set active = 'N' where branch_id = "
				+ branchId;
		String groupSql = "update `group` set active = 'N' where branch_id = "
				+ branchId;

		getJdbcTemplate().update(groupSql);
		getJdbcTemplate().update(teacherUserSql);
		getJdbcTemplate().update(studentUserSql);

		// getJdbcTemplate().update(eventSql);
		// getJdbcTemplate().update(noticeSql);
		// getJdbcTemplate().update(feesSql);
		getJdbcTemplate().update(studentSql);
	}

	@Transactional
	private void activateInstituteRelatedEntities(Integer instituteId) {
		// deactivate institute, user, school_admin and branches related to
		// institute.
		String userSql = "update user set active = 'Y' where institute_id = "
				+ instituteId;
		getJdbcTemplate().update(userSql);

		String schoolAdminSql = "update school_admin set active = 'Y' where user_id = "
				+ "(select id from user where institute_id = "
				+ instituteId
				+ ")";
		getJdbcTemplate().update(schoolAdminSql);

		String branchSql = "update branch set active = 'Y' where institute_id = "
				+ instituteId;
		getJdbcTemplate().update(branchSql);

		String instituteSql = "update institute set active = 'Y' where id = "
				+ instituteId;
		getJdbcTemplate().update(instituteSql);
	}

	@Transactional
	private void deactivateInstituteRelatedEntities(Integer instituteId) {
		// deactivate institute, user, school_admin and branches related to
		// institute.
		String userSql = "update user set active = 'N' where institute_id = "
				+ instituteId;
		getJdbcTemplate().update(userSql);

		String schoolAdminSql = "update school_admin set active = 'N' where user_id = "
				+ "(select id from user where institute_id = "
				+ instituteId
				+ ")";
		getJdbcTemplate().update(schoolAdminSql);

		String branchSql = "update branch set active = 'N' where institute_id = "
				+ instituteId;
		getJdbcTemplate().update(branchSql);

		String instituteSql = "update institute set active = 'N' where id = "
				+ instituteId;
		getJdbcTemplate().update(instituteSql);
	}

	@Transactional
	private String getLogoUrlBasedOnInstituteId(Integer instituteID) {
		String sql = "select logo_url from institute where id = " + instituteID;
		return getJdbcTemplate().queryForObject(sql, String.class);
	}

	@Transactional
	private void updateSchoolAdmin(Institute institute, Integer userId) {
		String schoolAdminSql = "update school_admin set firstName = ?, lastName = ?, emailAddress = ?, "
				+ "gender = ?, primaryContact = ?, active = ?, city = ?, pincode = ?, isDelete = ? where user_id = ?";
		Object[] params = new Object[] { institute.getFirstName(),
				institute.getLastName(), institute.getAdminEmail(),
				institute.getGender(), institute.getPrimaryContact(),
				institute.getActive(), institute.getAdminCity(),
				institute.getAdminPinCode(), "N", userId };
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.INTEGER };
		getJdbcTemplate().update(schoolAdminSql, params, types);
		
		String contactSql = "update contact set firstname = ?, lastname = ?, email = ?, " +
				" phone = ?, city = ? where user_id = ?";
		
		params = new Object[]{institute.getFirstName(), institute.getLastName(), institute.getAdminEmail(),
							  institute.getPrimaryContact(), institute.getAdminCity(), userId};
		types = new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, 
				  Types.VARCHAR,Types.VARCHAR,Types.INTEGER};
		getJdbcTemplate().update(contactSql, params, types);
	}

	@Transactional
	public boolean deleteInstitute(Integer instituteId) {
		boolean flag = false;
		deleteInstituteRelatedEntities(instituteId);

		List<Integer> branchIdList = getBranchIdBasedOnInstituteId(instituteId);
		deleteBranchRelatedEntities(branchIdList);
		// deleteForumUser(instituteId);
		flag = true;
		logger.debug("delete institute done..." + instituteId);
		return flag;
	}

	@Transactional
	private void deleteInstituteRelatedEntities(Integer instituteId) {
		// deactivate institute, user, school_admin and branches related to
		// institute.
		String randomUserName = RandomStringUtils.random(10, true, true);

		String userSql = "update user set username = '"+randomUserName+"', isDelete = 'Y' where institute_id = " + instituteId;
		getJdbcTemplate().update(userSql);

		String contactSql = "update contact set email = '"+randomUserName+"' where user_id = (select id from user where institute_id = "
				+ instituteId+")";

		getJdbcTemplate().update(contactSql);

		String schoolAdminSql = "update school_admin set isDelete = 'Y' where user_id = "
				+ "(select id from user where institute_id = " + instituteId + ")";
		getJdbcTemplate().update(schoolAdminSql);

		String branchSql = "update branch set is_delete = 'Y' where institute_id = " + instituteId;
		getJdbcTemplate().update(branchSql);

		String instituteSql = "update institute set isDelete = 'Y' where id = " + instituteId;
		getJdbcTemplate().update(instituteSql);
	}

	private void deleteForumUser(Integer instituteId) {
		String sql = "select forum_profile_id from user_forum_profile where user_id = "
				+ "(select id from user where institute_id = "
				+ instituteId
				+ ")";
		List<Integer> forumProfileIdList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return rs.getInt("forum_profile_id");
					}
				});
		/*
		 * sql =
		 * "select forum_profile_id from user_forum_profile where user_id in " +
		 * "(select user_id from student where branch_id in " +
		 * "(select id from branch where institute_id = "+instituteId+")) ";
		 * 
		 * List<Integer> studentForumProfileIdList =
		 * getJdbcTemplate().query(sql, new RowMapper<Integer>(){
		 * 
		 * @Override public Integer mapRow(ResultSet rs, int rowNum) throws
		 * SQLException { return rs.getInt("forum_profile_id"); } });
		 * 
		 * forumProfileIdList.addAll(studentForumProfileIdList);
		 */
		new ForumProfileDao().deleteUsers(new HashSet<Integer>(
				forumProfileIdList));
		logger.debug("delete institute forum user done..." + instituteId);
	}

	/*
	 * private void deleteForumCategory(Integer branchId) {
	 * 
	 * String sql =
	 * "select forum_category_id from branch_forum_detail where branch_id = "
	 * +branchId; List<Integer> forumCategoryList = getJdbcTemplate().query(sql,
	 * new RowMapper<Integer>(){
	 * 
	 * @Override public Integer mapRow(ResultSet rs, int rowNum) throws
	 * SQLException { return rs.getInt("forum_category_id"); } }); new
	 * ForumProfileDao().deleteCategory(forumCategoryList.get(0));
	 * logger.debug("delete forum category done branchId --"+branchId); }
	 */

	private List<Integer> getBranchIdBasedOnInstituteId(Integer instituteId) {

		String branchStr = "select id from branch where institute_id = "
				+ instituteId;

		return getJdbcTemplate().query(branchStr, new RowMapper<Integer>() {
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("id");
			}
		});

	}

	private void deleteBranchRelatedEntities(List<Integer> branchIdList) {

		String studentSql = "update student set isDelete = 'Y' where branch_id = ?";
		String studentUploadSql = "update student_upload set isDelete = 'Y' where branch_id = ?";
		String groupSql = "update `group` set active = 'N' where branch_id = ?";
		String teacherSql = "delete from user_role where user_id in "
				+ "(select user_id from teacher where branch_id = ?) AND role_id = "
				+ Constants.ROLE_ID_TEACHER;

		// Deactivate student,teacher student_upload and groups of related
		// branches.
		if (!branchIdList.isEmpty()) {
			for (Integer branchId : branchIdList) {

				Object[] params = { branchId };
				int[] types = { Types.INTEGER };

				getJdbcTemplate().update(studentSql, params, types);
				getJdbcTemplate().update(teacherSql, params, types);
				getJdbcTemplate().update(studentUploadSql, params, types);
				getJdbcTemplate().update(groupSql, params, types);

				// deleteForumCategory(branchId);
			}
		}
	}

	@Override
	public Integer getInstituteIdByName(String instituteName) {
		String sql = "select id from institute where name = '"+instituteName+"'";
		return getJdbcTemplate().queryForObject(sql, Integer.class);
	}
	
	public Integer saveInstituteExpressSession(final Integer id, final Integer userId){
		final String sql = "insert into express_session(stage, user_id, saved_id) values(?, ?, ?)";
		KeyHolder holder = new GeneratedKeyHolder();
		
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatementUser = connection.prepareStatement(
						sql, Statement.RETURN_GENERATED_KEYS);
				preparedStatementUser.setString(1, "INSTITUTE");
				preparedStatementUser.setInt(2, userId);
				preparedStatementUser.setInt(3, id);
				return preparedStatementUser;
			}
		}, holder);
		Integer sessionId = holder.getKey().intValue();
		return sessionId;
	}
	
	public List<Institute> getSessionInstitutes(Integer userId){
		String sql = "select i.id as id, i.name as instituteName from institute i";
		List<Institute> institutes = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Institute>(Institute.class));
		return institutes;
	}
}