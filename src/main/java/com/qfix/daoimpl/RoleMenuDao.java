package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qfix.model.Role;
import com.qfix.model.RoleMenu;
import com.qfix.model.RoleMenuMasterList;
import com.qfix.model.RoleMenuPermissions;

@Repository
public class RoleMenuDao {
	private Connection connection;
	private Statement statement;

	@Autowired
	private Connectivity connect;

	final static Logger logger = Logger.getLogger(RoleMenuDao.class);

	public RoleMenuMasterList getRole(){
		connection = connect.getConnection();
		
		List<Role> roleList = new ArrayList<>();
		try {
			statement = connection.createStatement();
			String query = "SELECT * from role";
			ResultSet resultSet = statement.executeQuery(query);
			while(resultSet.next()){
				Integer id = resultSet.getInt("id");
				String roleName = resultSet.getString("name");
				Role roles = new Role();
				roles.setName(roleName);
				roles.setId(id);
				roleList.add(roles);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(null, statement, null, connection);
		}
		RoleMenuMasterList roleMenuMasterList = new RoleMenuMasterList();
		roleMenuMasterList.setRoleList(roleList);
		
		logger.debug("Role menu master list --"+roleMenuMasterList);
		return roleMenuMasterList;
	}

	public List<RoleMenu> getRoleMenu(Integer roleId){
		connection = connect.getConnection();
		List<RoleMenu> roleMenuList = new ArrayList<>();
		try {
			statement = connection.createStatement();
			String query = "select mi.menu_id,menu_name,role_id from menu_items mi " +
					"left join role_menu on(mi.menu_id = role_menu.menu_id and role_id = " + roleId +")";
			ResultSet resultSet = statement.executeQuery(query);
			while(resultSet.next()){
				Integer id = resultSet.getInt("menu_id");
				String menuName = resultSet.getString("menu_name");
				Boolean allow = resultSet.getString("role_id") == null ? false : true;
				RoleMenu roleMenu = new RoleMenu();
				roleMenu.setMenuId(id);
				roleMenu.setMenuName(menuName);
				roleMenu.setAllow(allow);
				roleMenuList.add(roleMenu);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(null, statement, null, connection);
		}
		logger.debug("role Menu List... branchId --"+roleId+", Role menu List --"+roleMenuList);
		return roleMenuList;
	}
	
	
	public List<RoleMenuPermissions> getMenuPermissions(Integer menuId,Integer roleId){
		connection = connect.getConnection();
		List<RoleMenuPermissions> roleMenuPermissionList = new ArrayList<>();
		try {
			statement = connection.createStatement();
			String query = "select mp.menu_permission_id,role_id,permission_name,mp.url " +
					"from menu_permission mp left join role_menu_permission rmp on" +
					"(mp.menu_permission_id = rmp.menu_permission_id and rmp.role_id = "+ roleId+") " +
					"where  menu_id = " +menuId+ " and permission_name is not null";
			
			logger.debug("Role menu Permission SQL --"+query);

			ResultSet resultSet = statement.executeQuery(query);
			while(resultSet.next()){
				Integer id = resultSet.getInt("menu_permission_id");
				String permissionName = resultSet.getString("permission_name");
				Boolean allow = resultSet.getString("role_id") == null ? false : true;
				String url = resultSet.getString("url");
				RoleMenuPermissions roleMenuPermissions = new RoleMenuPermissions();
				roleMenuPermissions.setPermissionName(permissionName);
				roleMenuPermissions.setUrl(url);
				roleMenuPermissions.setMenuPermissionId(id);
				roleMenuPermissions.setAllow(allow);
				roleMenuPermissionList.add(roleMenuPermissions);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(null, statement, null, connection);
		}
		return roleMenuPermissionList;
	}
	
	
	public void save(List<RoleMenu> roleMenuList){
		for(RoleMenu roleMenu : roleMenuList){
			if(roleMenu.isAllow()){
				insertRoleMenu(roleMenu);
			}else{
				deleteRoleMenu(roleMenu);
			}
		}
	}

	private void insertRoleMenu(RoleMenu roleMenu){
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String query = "insert into role_menu (role_id,menu_id) values ("+roleMenu.getRoleId()+","+roleMenu.getMenuId() +")";
			statement.execute(query);
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(null, statement, null, connection);
		}
	}

	private void deleteRoleMenu(RoleMenu roleMenu){
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String query = "delete from role_menu where role_id = " +roleMenu.getRoleId() 
					+ " and menu_id = " +roleMenu.getMenuId();
			statement.execute(query);
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(null, statement, null, connection);
		}
	}
	
	public void savePermissions(List<RoleMenuPermissions> roleMenuPermissionList){
		
		for(RoleMenuPermissions roleMenuPermission : roleMenuPermissionList){
			if(roleMenuPermission.isAllow()){
				insertRoleMenuPermission(roleMenuPermission);
			}else{
				deleteRoleMenuPermission(roleMenuPermission);
			}
		}
	}


	private void insertRoleMenuPermission(RoleMenuPermissions roleMenuPermission){
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String query = "insert into role_menu_permission (role_id,menu_permission_id) " +
					"values ("+roleMenuPermission.getRoleId()+","+roleMenuPermission.getMenuPermissionId() +")";
			statement.execute(query);
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(null, statement, null, connection);
		}
	}
	
	private void deleteRoleMenuPermission(RoleMenuPermissions roleMenu){
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String query = "delete from role_menu_permission where role_id = " +roleMenu.getRoleId() 
					+ " and menu_permission_id = " +roleMenu.getMenuPermissionId();
			statement.execute(query);
		}catch(SQLException e){
			logger.error("", e);
		}
		finally{
			connect.closeDatabaseTransaction(null, statement, null, connection);
		}
	}
}
