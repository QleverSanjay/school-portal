
package com.qfix.daoimpl;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.config.AppProperties;
import com.qfix.dao.IBranchDao;
import com.qfix.model.Branch;
import com.qfix.model.BranchAdmin;
import com.qfix.model.DisplayHead;
import com.qfix.model.RefundConfig;
import com.qfix.service.file.FileFolderService;
import com.qfix.service.user.client.chat.response.session.UserResponse;
import com.qfix.service.user.client.forum.response.MessageResponse;
import com.qfix.service.user.client.forum.response.addUser.CreateUserResponse;
import com.qfix.service.user.client.forum.response.category.CreateCategoryResponse;
import com.qfix.utilities.Constants;
import com.qfix.utilities.FileConstants;
import com.qfix.utilities.SecurityUtil;

@Slf4j
@Repository
public class BranchDao extends JdbcDaoSupport implements IBranchDao {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	final static Logger logger = Logger.getLogger(BranchDao.class);
	
	@Autowired
	private FileFolderService fileFolderService;
	
	@Autowired
	private AppProperties appProperties;

	@Transactional
	public List<Branch> getAll(Integer instituteId) {
		List<Branch> branchList = new ArrayList<>();
		String sql = " select b.*, s.name as state, t.name as taluka"
				+ " from"
				+ " branch  as b INNER JOIN state as s on s.id = b.state_id AND s.active = 'Y'"
				+ " INNER JOIN taluka as t on t.id = b.taluka_id AND t.active = 'Y'"
				+ " where"
				+ " b.institute_id = "
				+ instituteId
				+ " AND b.is_delete = 'N'";

		logger.debug("get all branch SQL --" + sql);

		branchList = getJdbcTemplate().query(sql, new RowMapper<Branch>() {
			public Branch mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				Branch branch = new Branch();
				branch.setId(resultSet.getInt("id"));
				branch.setName(resultSet.getString("name"));
				branch.setActive(resultSet.getString("active"));
				branch.setArea(resultSet.getString("area"));
				branch.setAddressLine(resultSet.getString("address_line"));
				branch.setPinCode(resultSet.getString("pincode"));
				branch.setActive(resultSet.getString("active"));
				branch.setContactEmail(resultSet.getString("contactEmail"));
				branch.setContactNumber(resultSet.getString("contactNumber"));
//				branch.setDistrict(resultSet.getString("district"));
				branch.setState(resultSet.getString("state"));
				branch.setTaluka(resultSet.getString("taluka"));
				branch.setCity(resultSet.getString("city"));
				branch.setWebsiteUrl(resultSet.getString("websiteUrl"));
				return branch;
			}
		});
		return branchList;
	}

	@Transactional
	public Branch getBranchById(Integer id) {
		Branch branch = null;
		String sql = "select branch.*, bc.* from branch " +
				" LEFT JOIN branch_configuration as bc on bc.branch_id = branch.id " +
				" where branch.id = " + id;
		
		System.out.println("Getting branch " + sql);
		
		branch = getJdbcTemplate().query(sql, new RowMapper<Branch>() {
			@Override
			public Branch mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				System.out.println(resultSet.getString("is_paydirect_enabled"));
				Branch branch = new Branch();
				branch.setId(resultSet.getInt("id"));
				branch.setName(resultSet.getString("name"));
				branch.setActive(resultSet.getString("active"));
				branch.setAddressLine(resultSet.getString("address_line"));
				branch.setArea(resultSet.getString("area"));
				branch.setContactEmail(resultSet.getString("contactEmail"));
				branch.setContactNumber(resultSet.getString("contactNumber"));
				branch.setInstituteId(resultSet.getInt("institute_id"));
				branch.setDistrictId(resultSet.getInt("district_id"));
				branch.setStateId(resultSet.getInt("state_id"));
				branch.setTalukaId(resultSet.getInt("taluka_id"));
				branch.setPinCode(resultSet.getString("pincode"));
				branch.setCity(resultSet.getString("city"));
				branch.setInstituteType(resultSet.getString("institute_type"));
				branch.setBoardAffiliation(resultSet.getInt("board_affiliation"));
				branch.setUniversity(resultSet.getInt("university"));
				branch.setWebsiteUrl(resultSet.getString("websiteUrl"));
				branch.setOfflinePaymentsEnabled(resultSet.getString("offline_payments_enabled"));
				branch.setLogoUrl(resultSet.getString("logo_url"));
				branch.setBranchAdminList(getBranchAdminList(branch.getId()));
				branch.setIsPayDirectEnabled(resultSet.getString("is_paydirect_enabled"));
				branch.setShowLogoOnParentPortal(resultSet.getString("show_logo_on_parent_portal"));
				branch.setIsOtpEnabled(resultSet.getString("is_otp_enabled"));
				branch.setUniqueIdentifierLabel(resultSet.getString("unique_identifier_lable"));
				branch.setUniqueIdentifierName(resultSet.getString("unique_identifier_name"));
				setPrincipal(branch);
				
				return branch;
			}

		}).get(0);
		return branch;
	}

	private void setPrincipal(final Branch branch) {
		String sql = "select p.* from principal as p INNER JOIN branch_admin as ba on ba.user_id = p.user_id where ba.branch_id = "+ branch.getId();
		getJdbcTemplate().query(sql, new RowMapper<Object> (){
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				branch.setPrincipalName(rs.getString("principal_name"));
				branch.setPrincipalEmailId(rs.getString("email_id"));
				branch.setPrincipalMobile(rs.getString("mobile_no"));
				branch.setPrincipalGender(rs.getString("gender"));
				branch.setPrincipalUserId(rs.getInt("user_id"));
				return null;
			}
		});
	}

	private List<BranchAdmin> getBranchAdminList(final Integer id) {
		String sql = "select sc.* from school_admin as sc "
				+ " INNER JOIN branch_admin as ba on ba.user_id = sc.user_id AND ba.is_delete = 'N' "
				+ " INNER JOIN user as u on u.id = sc.user_id AND u.isDelete = 'N' AND u.active = 'Y' "
				+ " WHERE ba.branch_id = " + id
				+ " AND sc.isDelete = 'N' AND u.institute_id is not null";

		return getJdbcTemplate().query(sql, new RowMapper<BranchAdmin>() {
			@Override
			public BranchAdmin mapRow(ResultSet rs, int arg1)
					throws SQLException {
				BranchAdmin admin = new BranchAdmin();
				admin.setId(rs.getInt("id"));
				admin.setFirstName(rs.getString("firstName"));
				admin.setLastName(rs.getString("lastName"));
				admin.setDateOfBirth(rs.getString("dateOfBirth"));
				admin.setEmail(rs.getString("emailAddress"));
				admin.setGender(rs.getString("gender"));
				admin.setPrimaryContact(rs.getString("primaryContact"));
				admin.setCity(rs.getString("city"));
				admin.setPincode(rs.getString("pinCode"));
				admin.setUserId(rs.getInt("user_id"));
				admin.setStatus(rs.getString("active"));
				return admin;
			}
		});
	}

	@Transactional
	public boolean insertBranch(final Branch branch, MultipartFile file) throws Exception {
		boolean flag = false;

		String sql = "insert into branch (name, address_line, area, city, pincode, contactNumber, "
				+ "contactEmail, websiteUrl, institute_id, taluka_id, district_id, state_id, active, board_affiliation,university, institute_type)"
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		Object[] params = { branch.getName(), branch.getAddressLine(),
				branch.getArea(), branch.getCity(), branch.getPinCode(),
				branch.getContactNumber(), branch.getContactEmail(),
				branch.getWebsiteUrl(), branch.getInstituteId(),
				branch.getTalukaId(), branch.getDistrictId(),
				branch.getStateId(), "Y" , branch.getBoardAffiliation(), branch.getUniversity(),branch.getInstituteType()};

//		int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
//				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
//				Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER,
//				Types.INTEGER, Types.VARCHAR , Types.INTEGER, Types.VARCHAR};

		getJdbcTemplate().update(sql, params);

		Branch branchByName = getBranchIdByNameAndInstitute(branch);
		branchByName.setIsOtpEnabled(branch.getIsOtpEnabled());
		branchByName.setIsPayDirectEnabled(branch.getIsPayDirectEnabled());
		insertIntoBranchConfiguration(branchByName);

		insertBranchAdmins(branch);

		createProfiles(branch);
		try {
			createFolderStructure(branchByName.getId(),
					branchByName.getInstituteId());
		} catch (IOException e) {
			logger.error(
					"Folder Structure not created for ::: " + branch.getName(),
					e);
		}
		branch.setId(branchByName.getId());
		if(file != null){
			String photoPath = saveFile(file, branch, false);
			updatePhotoPath(branch.getId(), photoPath);
		}

		if(!StringUtils.isEmpty(branch.getPrincipalName())){
			insertIntoPrincipal(branch);
		}

		flag = true;
		return flag;
	}
	
	private void updatePhotoPath(Integer id, String photoPath) {
		String sql = "update branch set logo_url = '" + photoPath
				+ "' where id = " + id;
		getJdbcTemplate().update(sql);
	}

	private void insertBranchAdmins(Branch branch) throws Exception {
		List<BranchAdmin> branchAdmins = branch.getBranchAdminList();
		assignThisBranchToInstituteAdmin(branch);
		if (branchAdmins != null && !branchAdmins.isEmpty()) {
			for (BranchAdmin admin : branchAdmins) {
				admin.setBranchId(branch.getId());
				System.out.println("Admin ::::: " + admin);
				insertBranchAdmin(admin);
			}

		}
	}


	private void insertIntoPrincipal(Branch branch) throws Exception {
		if(!StringUtils.isEmpty(branch.getPrincipalName())){
			Integer userId = insertIntoUser(branch.getPrincipalName(), branch.getPrincipalEmailId(), "Y");

			String sql = "insert into principal (principal_name, mobile_no, email_id, gender, user_id) values (?, ?, ?, ?, ?)";

			Object[] params = new Object[] { branch.getPrincipalName(), branch.getPrincipalMobile(), branch.getPrincipalEmailId(),
					branch.getPrincipalGender(), userId};

			int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.INTEGER};

			getJdbcTemplate().update(sql, params, types);

			insertIntoContact(branch.getPrincipalName(), "", branch.getPrincipalEmailId(), branch.getPrincipalMobile(), null, userId);
			insertIntoBranchAdmin(branch.getId(), userId);
		}
	}


	private void insertBranchAdmin(BranchAdmin admin) throws Exception {
		Integer userId = insertIntoUser(admin.getFirstName(), admin.getEmail(), admin.getStatus());
		admin.setUserId(userId);

		insertIntoSchoolAdmin(admin);
		insertIntoContact(admin.getFirstName(), admin.getLastName(), admin.getEmail(), admin.getPrimaryContact(), admin.getCity(), admin.getUserId());
		insertIntoBranchAdmin(admin.getBranchId(), admin.getUserId());
	}

	private void assignThisBranchToInstituteAdmin(Branch branch) {
		String sql = "select id from user where institute_id = (select institute_id from branch where id = "
				+ branch.getId() + ")";
		Integer instituteUserId = getJdbcTemplate().queryForObject(sql,
				Integer.class);
		insertIntoBranchAdmin(branch.getId(), instituteUserId);
	}

	private void insertIntoContact(String firstName, String lastName, String email, String mobile, String city , Integer userId) {
		String sql = "insert into contact (email, phone, user_id, photo, firstname, lastname, city) "
				+ "values (?, ?, ?, ?, ?, ?, ?)";

		Object[] params = new Object[] { email,
				mobile, userId, null,
				firstName, lastName, city };

		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

		getJdbcTemplate().update(sql, params, types);
		System.out.println("INSERT INTO CONTACT DONE ::::::::::::::::::::::");
	}

	private void insertIntoSchoolAdmin(BranchAdmin admin) {
		String schoolAdminSql = "insert into school_admin (firstName, lastName, dateOfBirth, emailAddress, gender, "
				+ " primaryContact,user_id, active, city, pincode, isDelete) "
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		Object[] params = new Object[] { admin.getFirstName(),
				admin.getLastName(), admin.getDateOfBirth(), admin.getEmail(),
				admin.getGender(), admin.getPrimaryContact(),
				admin.getUserId(), admin.getStatus(), admin.getCity(),
				admin.getPincode(), "N" };

		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

		getJdbcTemplate().update(schoolAdminSql, params, types);
	}

	private void insertIntoBranchAdmin(Integer branchId, Integer userId) {
		String sql = "insert into branch_admin (branch_id, user_id) values ("
				+ branchId + ", " + userId + ") "
				+ "ON DUPLICATE KEY UPDATE branch_id = branch_id";
		getJdbcTemplate().update(sql);
	}

	private int insertIntoUser(final String firstName, final String email, final String status) throws Exception {
		final String userSql = "insert into user (username, password, password_salt, temp_password, active, "
				+ "isDelete) values (?, ?, ?, ?, ?, ?)";

		final String password = (firstName.replaceAll(
				"([\\() \\.\\-_])", "") + "123").toUpperCase();
		final byte[] randomSalt = SecurityUtil.generateSalt();
		final byte[] passwordHash = SecurityUtil.hashPassword(password,
				randomSalt);

		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatementUser = connection.prepareStatement(
						userSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatementUser.setString(1, email);
				preparedStatementUser.setBytes(2, passwordHash);
				preparedStatementUser.setBytes(3, randomSalt);
				preparedStatementUser.setString(4, password);
				preparedStatementUser.setString(5, status);
				preparedStatementUser.setString(6, "N");
				return preparedStatementUser;
			}
		}, holder);
		Integer userId = holder.getKey().intValue();

		String sql = "insert into user_role (user_id, role_id) values ("
				+ userId + ", " + Constants.ROLE_ID_SCHOOL_ADMIN + ")";
		getJdbcTemplate().update(sql);

		return userId;
	}

	private void insertIntoBranchConfiguration(Branch branch) {
		String offlinePaymentEnabled = branch.getOfflinePaymentsEnabled();
		String showLogoOnParentPortal = branch.getShowLogoOnParentPortal();
		String isPayDirectEnabled = branch.getIsPayDirectEnabled();
		String isOtpEnabled = branch.getIsOtpEnabled();
		if (StringUtils.isEmpty(offlinePaymentEnabled)) {
			offlinePaymentEnabled = "N";
		}
		
		if(StringUtils.isEmpty(showLogoOnParentPortal)){
			showLogoOnParentPortal = "N";
		}
		
		if(StringUtils.isEmpty(isPayDirectEnabled)){
			isPayDirectEnabled = "N";
		}
		
		if(StringUtils.isEmpty(isOtpEnabled)){
			isOtpEnabled = "N";
		}
		
		String sql = "insert into branch_configuration(branch_id, offline_payments_enabled,is_paydirect_enabled, "
				+ "show_logo_on_parent_portal,is_otp_enabled,unique_identifier_lable,unique_identifier_name) "
				+ "values(?,?,?,?,?,?,?)";

		Object[] data = {branch.getId(),offlinePaymentEnabled,isPayDirectEnabled,showLogoOnParentPortal,isOtpEnabled,
				branch.getUniqueIdentifierLabel(),branch.getUniqueIdentifierName()};
		
		getJdbcTemplate().update(sql,data);
		logger.debug("Entry added to branch_configuration....");
	}

	private void createFolderStructure(Integer id, Integer instituteId)
			throws IOException {
		String parentPath = instituteId + File.separator;
		String folderName = id + File.separator;
		logger.debug("Branch Folder PATH --BASE_PATH+" + parentPath
				+ folderName);
		try {
			fileFolderService.createExternalStorage(parentPath,
					folderName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Transactional
	public boolean uploadBranch(List<Branch> branchList, Integer instituteId)
			throws Exception {
		for (Branch branch : branchList) {
			insertBranch(branch, null);
		}
		return true;
	}

	@Transactional
	public boolean updateBranch(final Branch branch, MultipartFile file) throws Exception {
		final String sql = "update branch set name = ?, address_line = ?, area = ?, city = ?, pincode = ?, "
				+ "contactNumber = ?, contactEmail = ?, websiteUrl = ?, institute_id = ?, taluka_id = ?,"
				+ "state_id = ?, active = ?,board_affiliation=?,university=?, institute_type=? where id = ?";
		
		Branch preBranch = getBranchById(branch.getId());
			
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection
						.prepareStatement(sql);
				preparedStatement.setString(1, branch.getName());
				preparedStatement.setString(2, branch.getAddressLine());
				preparedStatement.setString(3, branch.getArea());
				preparedStatement.setString(4, branch.getCity());
				preparedStatement.setString(5, branch.getPinCode());
				preparedStatement.setString(6, branch.getContactNumber());
				preparedStatement.setString(7, branch.getContactEmail());
				preparedStatement.setString(8, branch.getWebsiteUrl());
				preparedStatement.setInt(9, branch.getInstituteId());
				preparedStatement.setInt(10, branch.getTalukaId());
				preparedStatement.setInt(11, branch.getStateId());
				preparedStatement.setString(12, branch.getActive());
				if(branch.getBoardAffiliation() == null){
					preparedStatement.setNull(13, Types.INTEGER);
				}else{					
					preparedStatement.setInt(13, branch.getBoardAffiliation());
				}
				
				if(branch.getUniversity() == null){
					preparedStatement.setNull(14, Types.INTEGER);
				}else{					
					preparedStatement.setInt(14, branch.getUniversity());
				}
				
				preparedStatement.setString(15, branch.getInstituteType());
				preparedStatement.setInt(16,branch.getId());
				return preparedStatement;
			}
		});

		updateBranchAdmin(branch, preBranch);

		if(preBranch.getPrincipalUserId() == null && !StringUtils.isEmpty(branch.getPrincipalName())){
			insertIntoPrincipal(branch);
		}
		else if(branch.getPrincipalUserId() != null){
			updatePrincipal(branch);
		}

		updateBranchConfig(branch);
		branch.setLogoUrl(getLogoUrlBasedOnBranchId(branch.getId()));
		if(file != null){
			String photoPath = saveFile(file, branch, true);
			updatePhotoPath(branch.getId(), photoPath);
		}
		return true;
	}

	private void updatePrincipal (Branch branch){
		String sql = "update principal set principal_name = ?, mobile_no = ?, email_id = ?, gender = ? where user_id = ?";

		Object[] params = new Object[] { branch.getPrincipalName(), branch.getPrincipalMobile(), branch.getPrincipalEmailId(),
				branch.getPrincipalGender(), branch.getPrincipalUserId()};

		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.INTEGER};

		getJdbcTemplate().update(sql, params, types);
	}

	private void updateBranchConfig(Branch branch) {
		String offlinePaymentEnabled = branch.getOfflinePaymentsEnabled();
		String showLogoOnParentPortal = branch.getShowLogoOnParentPortal();
		String isPayDirectEnabled = branch.getIsPayDirectEnabled();
		String isOtpEnabled = branch.getIsOtpEnabled();
		if (StringUtils.isEmpty(offlinePaymentEnabled)) {
			offlinePaymentEnabled = "N";
		}
		
		if(StringUtils.isEmpty(showLogoOnParentPortal)){
			showLogoOnParentPortal = "N";
		}
		
		if(StringUtils.isEmpty(isPayDirectEnabled)){
			isPayDirectEnabled = "N";
		}
		
		if(StringUtils.isEmpty(isOtpEnabled)){
			isOtpEnabled = "N";
		}
		
		String sql = "UPDATE branch_configuration SET offline_payments_enabled = ?, is_paydirect_enabled = ?, "
				+ "show_logo_on_parent_portal = ?, is_otp_enabled = ?, unique_identifier_name = ?, unique_identifier_lable = ? WHERE branch_id = ?";
		
		Object[] args = new Object[]{offlinePaymentEnabled,isPayDirectEnabled,showLogoOnParentPortal,isOtpEnabled
				,branch.getUniqueIdentifierName(),branch.getUniqueIdentifierLabel(),branch.getId(),};
		getJdbcTemplate().update(sql,args);
	}

	private void updateBranchAdmin(Branch branch, Branch preBranch)
			throws Exception {
		List<BranchAdmin> branchAdmins = branch.getBranchAdminList();
		List<BranchAdmin> preBranchAdmins = preBranch.getBranchAdminList();
		List<Integer> newBranchAdmins = new ArrayList<>();
		if (branchAdmins != null && !branchAdmins.isEmpty()) {

			for (BranchAdmin admin : branchAdmins) {
				admin.setBranchId(branch.getId());
				if (admin.getId() != null
						&& (preBranchAdmins != null && !preBranchAdmins
								.isEmpty())) {
					newBranchAdmins.add(admin.getId());
					for (BranchAdmin preAdmin : preBranchAdmins) {
						if (preAdmin.getId().equals(admin.getId())) {
							updateBranchAdmin(admin);
						}
					}
				} else {
					insertBranchAdmin(admin);
					try {
						createChatProfile(admin.getFirstName(),
								admin.getLastName(), admin.getUserId());
					}catch(Exception e){
						e.printStackTrace();
					}
					createForumProfileWhenBranchUpdate(admin);
				}
			}
		}
		if (preBranchAdmins != null && !preBranchAdmins.isEmpty()) {
			for (BranchAdmin preAdmin : preBranchAdmins) {
				if (!newBranchAdmins.contains(preAdmin.getId())) {
					deleteBranchAdmin(preAdmin.getId());
				}
			}
		}
	}

	private void createForumProfileWhenBranchUpdate(final BranchAdmin admin) {
		String sql = "select * from branch_forum_detail where branch_id = "
				+ admin.getBranchId();
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				String password = RandomStringUtils.random(8, true, true);
				String email = RandomStringUtils.random(8, true, true)
						+ ".branchAdmin@qfixinfo.com";
				createForumProfile(password, email, admin.getUserId(),
						rs.getInt("forum_category_id"),
						rs.getInt("read_write_group_id"));
				return null;
			}
		});
	}

	private void deleteBranchAdmin(Integer id) {
		String email = RandomStringUtils.random(30, true, true);
		String phone = RandomStringUtils.random(7, false, true);

		String sql = "update school_admin set emailAddress = '" + email
				+ "', primaryContact = '" + phone
				+ "', isDelete = 'Y' where id = " + id;
		getJdbcTemplate().update(sql);

		sql = "delete from branch_admin where user_id = (select user_id from school_admin where id = "
				+ id + ")";
		getJdbcTemplate().update(sql);

		sql = "update contact set email = '"
				+ email
				+ "', phone = '"
				+ phone
				+ "' where user_id = (select user_id from school_admin where id = "
				+ id + ")";
		getJdbcTemplate().update(sql);

		sql = "update user set username = '"
				+ email
				+ "', isDelete = 'Y' where id = (select user_id from school_admin where id = "
				+ id + ")";
		getJdbcTemplate().update(sql);
	}

	private void updateBranchAdmin(BranchAdmin admin) {
		String schoolAdminSql = "update school_admin set firstName = ?, lastName = ?, dateOfBirth = ?, emailAddress = ?, "
				+ "gender = ?, primaryContact = ?, active = ?, city = ?, pincode = ?, isDelete = ? where user_id = ?";

		Object[] params = new Object[] { admin.getFirstName(),
				admin.getLastName(), admin.getDateOfBirth(), admin.getEmail(),
				admin.getGender(), admin.getPrimaryContact(),
				admin.getStatus(), admin.getCity(), admin.getPincode(), "N",
				admin.getUserId() };

		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER };

		getJdbcTemplate().update(schoolAdminSql, params, types);
		updateUser(admin);
	}

	private void updateUser(BranchAdmin admin) {
		String userSql = "update user set username= ?, active = ? where id = ?";
		Object[] params = new Object[] { admin.getEmail(), admin.getStatus(),
				admin.getUserId() };
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER };
		getJdbcTemplate().update(userSql, params, types);
	}

	@Transactional
	public boolean deleteBranch(Integer branchId) {
		boolean flag = false;

		deleteBranchAdmins(branchId);
		getJdbcTemplate().update(
				"update branch set is_delete = 'Y' where id = " + branchId);

		String studentSql = "update student set isDelete = 'Y' where branch_id = "
				+ branchId;
		String studentUploadSql = "update student_upload set isDelete = 'Y' where branch_id = "
				+ branchId;
		String teacherSql = "update teacher set isDelete = 'Y' where branch_id = "
				+ branchId;
		String groupSql = "update `group` set isDelete = 'Y' where branch_id = "
				+ branchId;

		getJdbcTemplate().update(studentSql);
		getJdbcTemplate().update(studentUploadSql);
		getJdbcTemplate().update(teacherSql);
		getJdbcTemplate().update(groupSql);

		logger.debug("branch deleted now deleting from forum...");
		// delete from forum phpbb service.

		String selectForumCategorySql = "select forum_category_id from branch_forum_detail where branch_id = "
				+ branchId;
		getJdbcTemplate().query(selectForumCategorySql,
				new RowMapper<String>() {
					@Override
					public String mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						new ForumProfileDao().deleteCategory(resultSet
								.getInt("forum_category_id"));
						return null;
					}
				});
		flag = true;
		return flag;
	}

	private void deleteBranchAdmins(final Integer branchId) {
		String sql = "select user_id from branch_admin where branch_id = "
				+ branchId;
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				deleteBranchAdmin(rs.getInt("user_id"));
				return null;
			}
		});
	}

	@Transactional
	private Branch getBranchIdByNameAndInstitute(final Branch branch) {
		String sql = "select id from branch " + "where name = '"
				+ branch.getName() + "' AND institute_id = "
				+ branch.getInstituteId();
		getJdbcTemplate().query(sql, new RowMapper<Branch>() {
			@Override
			public Branch mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				branch.setId(resultSet.getInt("id"));
				return branch;
			}
		}).get(0);
		return branch;
	}

	@Transactional
	private void createInstituteAdminForumProfile(final Integer branchId,
			final Integer schoolId, final Integer rwgId) {

		// get inserted teacher id from database for user profile forum
		String sql = "select u.username as userName, u.temp_password as `password`, u.id as userId,"
				+ " sa.emailAddress as email from school_admin as sa, user as u "
				+ " where sa.user_id = u.id AND u.institute_id = "
				+ "(select institute_id from branch where id = "
				+ branchId
				+ ") group by u.id";
		logger.debug("create user profile sql --" + sql);

		getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {

				final String password = resultSet.getString("password");
				final String email = RandomStringUtils.random(8, true, true)
						+ ".schoolAdmin@qfixinfo.com";
				final Integer userId = resultSet.getInt("userId");
				String userSql = "select count(*) as count from "
						+ "user_forum_profile where user_id = " + userId;
				getJdbcTemplate().query(userSql, new RowMapper<String>() {
					@Override
					public String mapRow(ResultSet rs, int arg1)
							throws SQLException {
						if (rs.getInt("count") > 0) {
							logger.debug("linking user with forum.");
							linkUserToForum(rwgId, userId);
						} else {
							createForumProfile(password, email, userId,
									schoolId, rwgId);
						}
						return null;
					}
				});
				return null;
			}
		});
	}

	@Transactional
	private void createForumProfile(final String password, final String email,
			final Integer userId, final Integer schoolId, final Integer rwgId) {

		String username = RandomStringUtils.random(10, true, true) + "@"
				+ Constants.DOMAIN_NAME;
		final CreateUserResponse userResponse = new ForumProfileDao()
				.createUserAccount(username, password, email, schoolId, rwgId,
						true, true);

		logger.debug("User forum profile created...REPONSE --" + userResponse);

		if (userResponse != null) {
			String sql = "insert into user_forum_profile (login, password, forum_profile_id, is_active, "
					+ "is_delete ,user_id) values ('"
					+ username
					+ "', '"
					+ password
					+ "', '"
					+ userResponse.getForum_profile_id()
					+ "', 'Y', 'N', " + userId + ")";

			logger.debug("USER_FORUM_PROFILE INSERT SQL -----\n" + sql);
			getJdbcTemplate().update(sql);
		}
		/*
		 * else { throw Exception("Forum User not Created..."); }
		 */

	}

	@Transactional
	private void linkUserToForum(final Integer rwgId, final Integer userId)
			throws SQLException {
		String sql = "select forum_profile_id from user_forum_profile where user_id = "
				+ userId;

		getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				Integer forumProfileId = resultSet.getInt("forum_profile_id");
				if (forumProfileId != null && forumProfileId != 0) {
					Set<Integer> userForumProfileIdList = new HashSet<>();
					userForumProfileIdList.add(forumProfileId);
					ForumProfileDao forumProfileDao = new ForumProfileDao();
					MessageResponse response = forumProfileDao.linkUserToForum(
							rwgId, userForumProfileIdList);
					logger.debug("Link user forum response --" + response);
				}
				return null;
			}
		});
	}

	@Transactional
	private void createProfiles(Branch branch) {
		try {
			createForumCategoryAndProfiles(branch);
			List<BranchAdmin> branchAdmins = branch.getBranchAdminList();
			if (branchAdmins != null && !branchAdmins.isEmpty()) {
				
					for (BranchAdmin admin : branchAdmins) {
						createChatProfile(admin.getFirstName(), admin.getLastName(),
								admin.getUserId());
					}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void createChatProfile(String firstName, String lastName,
			Integer userId) {

		ChatDao chatDao = new ChatDao();
		UserResponse userResponse = chatDao.createChatAccount(
				RandomStringUtils.random(12, true, true) + "@qfixinfo.com",
				firstName, lastName, "");
		String sql = "insert into user_chat_profile (login, password, email, chat_user_profile_id, user_id) values (?,?,?,?,?)";

		Object[] params = { userResponse.getLogin(),
				userResponse.getPassword(), userResponse.getEmail(),
				userResponse.getChatUserProfileId(), userId };

		int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.DECIMAL, Types.INTEGER };

		getJdbcTemplate().update(sql, params, types);

		logger.debug("Chat profile created for..." + firstName + " " + lastName);
	}

	private void createForumCategoryAndProfiles(final Branch branch) {
		ForumProfileDao forumProfileDao = new ForumProfileDao();

		final CreateCategoryResponse categoryResponse = forumProfileDao
				.createCategoryAccount(branch.getName(), branch.getName(), "C");
		logger.debug("Category created to forum -- " + categoryResponse);

		final String sql = "insert into branch_forum_detail (forum_category_id, read_write_group_id, "
				+ "read_only_group_id, branch_id) values (?,?,?,?)";

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setString(1, categoryResponse.getSchool_id());
				ps.setString(2, categoryResponse.getRwgid());
				ps.setString(3, categoryResponse.getRogid());
				ps.setInt(4, branch.getId());
				return ps;
			}
		});

		createInstituteAdminForumProfile(branch.getId(),
				Integer.parseInt(categoryResponse.getSchool_id()),
				Integer.parseInt(categoryResponse.getRwgid()));

		createBranchAdminForumProfile(branch,
				Integer.parseInt(categoryResponse.getSchool_id()),
				Integer.parseInt(categoryResponse.getRwgid()));
	}

	private void createBranchAdminForumProfile(Branch branch, int schoolId,
			int rwgId) {
		List<BranchAdmin> branchAdmins = branch.getBranchAdminList();
		if (branchAdmins != null && !branchAdmins.isEmpty()) {
			for (BranchAdmin admin : branchAdmins) {
				String email = RandomStringUtils.random(8, true, true)
						+ ".branchAdmin@qfixinfo.com";
				createForumProfile(RandomStringUtils.random(8, true, true),
						email, admin.getUserId(), schoolId, rwgId);
			}
		}
	}

	@Override
	@Transactional
	public void updateFeesDisplayLayout(DisplayHead layout) {
		String sql = "update branch_display_layout set fees_description = ?, "
				+ " fees_amount = ?, other_description = ?, branch_id = ? where id = ?";

		Object[] args = new Object[] { layout.getName(),
				layout.getAmount(), layout.getDescription(),
				layout.getBranchId(), layout.getId() };

		int[] argTypes = new int[] { Types.VARCHAR, Types.DOUBLE,
				Types.VARCHAR, Types.INTEGER, Types.INTEGER };

		getJdbcTemplate().update(sql, args, argTypes);
	}

	@Override
	@Transactional
	public void insertFeesDisplayLayout(DisplayHead layout) {
		String sql = "insert into branch_display_layout(fees_description, fees_amount, other_description, branch_id) values (?, ?, ?, ?)";
		System.out.println("LAYOUT :::: \n" + layout);

		Object[] args = new Object[] { layout.getName(),
				layout.getAmount(), layout.getDescription(),
				layout.getBranchId() };

		int[] argTypes = new int[] { Types.VARCHAR, Types.DOUBLE,
				Types.VARCHAR, Types.INTEGER };

		System.out
				.println(Arrays.asList(args) + "\n" + Arrays.asList(argTypes));
		getJdbcTemplate().update(sql, args, argTypes);
	}

	@Override
	public List<DisplayHead> getDisplayLyouts(Integer branchId) {
		List<DisplayHead> displayLyouts = new ArrayList<>();
		String sql = "select * from branch_display_layout where branch_id = "
				+ branchId + " AND is_delete = 'N' ";
		displayLyouts = getJdbcTemplate().query(sql,
				new RowMapper<DisplayHead>() {
					@Override
					public DisplayHead mapRow(ResultSet rs, int arg1)
							throws SQLException {
						DisplayHead layout = new DisplayHead();
						layout.setAmount(rs.getDouble("fees_amount"));
						layout.setName(rs
								.getString("fees_description"));
						layout.setDescription(rs
								.getString("other_description"));
						layout.setId(rs.getInt("id"));
						return layout;
					}
				});
		return displayLyouts;
	}

	@Override
	public boolean deleteDisplayLayout(Integer displayLayoutId) {
		String sql = "update branch_display_layout set is_delete = 'Y' where id = "
				+ displayLayoutId;
		getJdbcTemplate().update(sql);
		return true;
	}

	@Override
	public DisplayHead getDisplayLyout(Integer displayLayoutId) {
		String sql = "select bdl.*, b.institute_id from branch_display_layout as bdl "
				+ " INNER JOIN branch as b on bdl.branch_id = b.id "
				+ " where bdl.id = " + displayLayoutId
				+ " AND bdl.is_delete = 'N' ";

		List<DisplayHead> displayLyouts = getJdbcTemplate().query(sql,
				new RowMapper<DisplayHead>() {
					@Override
					public DisplayHead mapRow(ResultSet rs, int arg1)
							throws SQLException {
						DisplayHead layout = new DisplayHead();
						layout.setAmount(rs.getDouble("fees_amount"));
						layout.setName(rs
								.getString("fees_description"));
						layout.setBranchId(rs.getInt("branch_id"));
						layout.setInstituteId(rs.getInt("institute_id"));
						layout.setDescription(rs
								.getString("other_description"));
						layout.setId(rs.getInt("id"));
						return layout;
					}
				});
		if (!displayLyouts.isEmpty()) {
			return displayLyouts.get(0);
		}
		return null;
	}
	
	private String saveFile(MultipartFile file, Branch branch,
			boolean isUpdate) throws Exception {
		String filePath = null;
		// && student.isFileChanegd()
		if (file != null) {
			String parentPath = branch.getInstituteId() + File.separator + branch.getId() + File.separator;

			String fileExtension = file.getOriginalFilename().substring(
					file.getOriginalFilename().lastIndexOf("."));
			String fileNameWithExtension = org.apache.commons.lang3.RandomStringUtils
					.random(30, true, true) + fileExtension;

			if (isUpdate) {
				String logoUrl = branch.getLogoUrl();
				fileNameWithExtension = (StringUtils.isEmpty(logoUrl)
						|| logoUrl.endsWith(FileConstants.DEFAULT_IMAGE_NAME) ? fileNameWithExtension
						: logoUrl.substring(logoUrl.lastIndexOf("/") + 1));
			} else {
				fileFolderService.createExternalStorage(parentPath);
			}

			filePath = fileFolderService.createFileInExternalStorage(
					parentPath, fileNameWithExtension, file.getBytes());
		} else if (!isUpdate) {
			filePath = appProperties.getFileProps().getExternalDocumentsPath()
					+ FileConstants.DEFAULT_IMAGE_NAME;

		}
		filePath = FilenameUtils.separatorsToUnix(filePath);
		logger.debug("institute logo saved ..." + filePath);
		return filePath;
	}
	
	@Transactional
	private String getLogoUrlBasedOnBranchId(Integer branchID) {
		String sql = "select logo_url from branch where id = " + branchID;
		return getJdbcTemplate().queryForObject(sql, String.class);
	}

	@Override
	public Integer getBranchIdByName(String name, Integer instituteId) {
		String sql = "select id from branch where name = '"+name+"' and institute_id = " + instituteId;
		return getJdbcTemplate().queryForObject(sql, Integer.class);
	}

	@Override
	public RefundConfig getRefundConfigurationByBranchId(Integer branchId) {
		String sql = "select bc.*, b.institute_id from branch_configuration as bc "
				+ " inner join branch as b on bc.branch_id = b.id "
				+ " where branch_id = " + branchId;
		List<RefundConfig> refundConfigs = getJdbcTemplate().query(sql, new RowMapper<RefundConfig> (){
			@Override
			public RefundConfig mapRow(ResultSet rs, int arg1) throws SQLException {
				RefundConfig config = new RefundConfig(rs.getInt("branch_id"), 
						rs.getInt("institute_id"),
						rs.getString("allow_credit_card_refund"),
						rs.getString("allow_credit_card_partial_refund"),
						rs.getString("allow_debit_card_refund"),
						rs.getString("allow_debit_card_partial_refund"),
						rs.getString("allow_net_banking_refund"),
						rs.getString("allow_net_banking_partial_refund"),
						rs.getString("allow_neft_rtgs_refund"),
						rs.getString("allow_neft_rtgs_partial_refund"),
						rs.getString("allow_cheque_dd_refund"),
						rs.getString("allow_cheque_dd_partial_refund"),
						rs.getString("allow_cash_refund"),
						rs.getString("allow_cash_partial_refund")
						);
				return config;
			}});
		return refundConfigs.get(0);
	}

	@Override
	public void saveRefundConfiguration(RefundConfig config) {
		String sql = "select count(*) from branch_configuration where branch_id = " + config.getBranchId();
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0) {
			sql = "update branch_configuration set allow_credit_card_refund = ?, allow_credit_card_partial_refund = ?, "
					+ " allow_debit_card_refund = ? , allow_debit_card_partial_refund = ? , allow_net_banking_refund = ? , "
					+ " allow_net_banking_partial_refund = ?, allow_neft_rtgs_refund = ? , allow_neft_rtgs_partial_refund = ?, "
					+ " allow_cheque_dd_refund = ?, allow_cheque_dd_partial_refund = ? , allow_cash_refund = ?, "
					+ " allow_cash_partial_refund = ? where branch_id = ?";
		}else {
			sql = "insert into branch_configuration (allow_credit_card_refund, allow_credit_card_partial_refund, "
					+ " allow_debit_card_refund, allow_debit_card_partial_refund, allow_net_banking_refund, allow_net_banking_partial_refund, "
					+ " allow_neft_rtgs_refund, allow_neft_rtgs_partial_refund, allow_cheque_dd_refund, allow_cheque_dd_partial_refund, "
					+ " allow_cash_refund, allow_cash_partial_refund, branch_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		}

		Object[] args = new Object[]{
				config.getAllowCreditCardRefund(), config.getAllowCreditCardPartialRefund(),
				config.getAllowDebitCardRefund(), config.getAllowDebitCardPartialRefund(),
				config.getAllowNetBankingRefund(), config.getAllowNetBankingPartialRefund(),
				config.getAllowNeftRtgsRefund(), config.getAllowNeftRtgsPartialRefund(),
				config.getAllowChequeDDRefund(), config.getAllowChequeDDPartialRefund(),
				config.getAllowCashRefund(), config.getAllowCashPartialRefund(), config.getBranchId()
		};

		int[] types = new int[]{
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, 
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, 
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, 
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, 
				Types.INTEGER
		};
		
		getJdbcTemplate().update(sql, args, types);
	}
	
	public void saveExpressBranchSession(final Integer branchId, final Integer instituteId, final Integer userId){
		final String sql = "insert into express_session(stage, user_id, saved_id, parent_id) values(?, ?, ?, ?)";
		KeyHolder holder = new GeneratedKeyHolder();
		
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatementUser = connection.prepareStatement(
						sql, Statement.RETURN_GENERATED_KEYS);
				preparedStatementUser.setString(1, "BRANCH");
				preparedStatementUser.setInt(2, userId);
				preparedStatementUser.setInt(3, branchId);
				preparedStatementUser.setInt(4, instituteId);
				return preparedStatementUser;
			}
		}, holder);
		Integer sessionId = holder.getKey().intValue();
		log.info("Save Express Onboard branch with id " + sessionId);
		String sessionSql = "update express_session set express_session_id = " + sessionId + " where save_id = " + branchId;
		log.info("branch session sql -> " + sessionSql);
		try{
			getJdbcTemplate().update(sessionSql);
		}catch(Exception e){
			log.info("Failed in saving express branch session");
		}
	}
	
	public List<Branch> getExpressSessionBranches(Integer instituteId, Integer userId){
		String sql = "select b.id as id, b.name as name from branch b where b.institute_id = "+ instituteId;
		log.info("Fetching expression branches -> " + sql);
		List<Branch> branchList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<Branch>(Branch.class));
		return branchList; 
	}
	
	public void deleteExpressSessionData(Integer branchId){
		String sql = "delete from express_session where stage = 'BRANCH' and save_id = " + branchId;
		getJdbcTemplate().update(sql);
	}
	
	@Transactional
	public Branch getBranchDataById(Integer id) {
		Branch branch = null;
		String sql = "select branch.*, bc.*, bo.name as boardName, st.name as state, t.name as taluka from branch join board bo on bo.id = branch.board_id  JOIN branch_configuration as bc on bc.branch_id = branch.id join state st on st.id = branch.state_id join taluka t on t.id = branch.taluka_id where branch.id =" + id;
		
		System.out.println("Getting branch " + sql);
		
		branch = getJdbcTemplate().query(sql, new RowMapper<Branch>() {
			@Override
			public Branch mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				System.out.println(resultSet.getString("is_paydirect_enabled"));
				Branch branch = new Branch();
				branch.setId(resultSet.getInt("id"));
				branch.setName(resultSet.getString("name"));
				branch.setActive(resultSet.getString("active"));
				branch.setAddressLine(resultSet.getString("address_line"));
				branch.setArea(resultSet.getString("area"));
				branch.setContactEmail(resultSet.getString("contactEmail"));
				branch.setContactNumber(resultSet.getString("contactNumber"));
				branch.setInstituteId(resultSet.getInt("institute_id"));
				branch.setDistrictId(resultSet.getInt("district_id"));
				branch.setStateId(resultSet.getInt("state_id"));
				branch.setTalukaId(resultSet.getInt("taluka_id"));
				branch.setPinCode(resultSet.getString("pincode"));
				branch.setCity(resultSet.getString("city"));
				branch.setWebsiteUrl(resultSet.getString("websiteUrl"));
				branch.setOfflinePaymentsEnabled(resultSet.getString("offline_payments_enabled"));
				branch.setLogoUrl(resultSet.getString("logo_url"));
				branch.setBranchAdminList(getBranchAdminList(branch.getId()));
				branch.setIsPayDirectEnabled(resultSet.getString("is_paydirect_enabled"));
				branch.setShowLogoOnParentPortal(resultSet.getString("show_logo_on_parent_portal"));
				branch.setIsOtpEnabled(resultSet.getString("is_otp_enabled"));
				branch.setUniqueIdentifierLabel(resultSet.getString("unique_identifier_lable"));
				branch.setUniqueIdentifierName(resultSet.getString("unique_identifier_name"));
				branch.setState(resultSet.getString("state"));
				branch.setTaluka(resultSet.getString("taluka"));
				branch.setBoardName(resultSet.getString("boardName"));
				branch.setInstituteType(resultSet.getString("institute_type"));
				setPrincipal(branch);
				
				return branch;
			}

		}).get(0);
		
		branch.setBranchAdminList(getBranchAdminDataByBranchId(id));
		return branch;
	}
	
	public List<BranchAdmin> getBranchAdminDataByBranchId(Integer branchId){
		String sql = "select  c.firstname as firstName, c.lastname as lastName, c.email, c.phone as primaryContact from branch_admin ba "
				+ "join contact c on c.user_id = ba.user_id "
				+ "join user u on u.id = ba.user_id  "
				+ "where ba.branch_id = "+ branchId +" "
				+ "and u.institute_id is null and u.isDelete = 'N' and u.active = 'Y'";
		
		List<BranchAdmin> ba = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<BranchAdmin>(BranchAdmin.class));
		if(ba != null && ba.size() > 0)
			return ba;
		
		sql = "select sa.firstName as firstName, sa.lastName as lastName, sa.gender as gender, sa.primaryContact as primaryContact, sa.emailAddress as email from school_admin sa join branch_admin ba on sa.user_id = ba.user_id where ba.branch_id = " + branchId;
		ba = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<BranchAdmin>(BranchAdmin.class));
		return ba;
	}
}