package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.qfix.dao.VendorInterfaceDao;
import com.qfix.exceptions.BranchForumNotExistException;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Vendor;
import com.qfix.service.user.client.forum.response.addUser.CreateUserResponse;

@Repository
public class VendorDao extends JdbcDaoSupport implements VendorInterfaceDao  {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private Connection connection;
	private Statement statement;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;

	@Autowired
	private Connectivity connect;

//	private List<Vendor> errorVendorList;
	final static Logger logger = Logger.getLogger(VendorDao.class);
	
	public List<Vendor> getAll()throws SQLException {
		List<Vendor> vendorList = new ArrayList<Vendor>();
		
			String sql = "select * from vendor where active = 'Y'";

			vendorList = getJdbcTemplate().query(sql, new RowMapper<Vendor>(){
				public Vendor mapRow(ResultSet rs, int rownumber)throws SQLException {
					
				Vendor vendor = new Vendor();
				vendor.setId(rs.getInt("id"));
				vendor.setMerchantLegalName(rs.getString("merchant_legal_name"));
				vendor.setMerchantMarketingName(rs.getString("marketing_name"));
				/*vendor.setBankAccountNumber(rs.getString("bank_account_number"));
				vendor.setMeCode(rs.getString("me_Code"));
				vendor.setTid(rs.getString("t_id"));
				vendor.setLgCode(rs.getString("lg_code"));
				vendor.setLcCode(rs.getString("lc_code"));
				vendor.setBranch(rs.getString("branch"));
				vendor.setAddressLine1(rs.getString("address_line1"));
				vendor.setArea(rs.getString("area"));
				vendor.setCity(rs.getString("city"));
				vendor.setPinCode(rs.getString("pinCode"));
				vendor.setPrimaryPhone(rs.getString("primary_phone"));
				vendor.setSecondaryPhone(rs.getString("secondary_phone"));
				vendor.setFaxNo(rs.getString("fax_no"));
				vendor.setEmail(rs.getString("email"));
				vendor.setShopOwnership(rs.getString("shop_ownership"));
				vendor.setOtherGroup(rs.getString("other_group"));
				vendor.setOtherGroupAddress(rs.getString("other_group_address"));
				vendor.setProprietorName(rs.getString("proprietor_name"));
				vendor.setProprietorAddressLine1(rs.getString("proprietor_address_line1"));
				vendor.setProprietorAddressArea(rs.getString("proprietor_address_area"));
				vendor.setProprietorAddressCity(rs.getString("proprietor_address_city"));
				vendor.setProprietorAddressPincode(rs.getString("proprietor_address_pincode"));
				vendor.setProprietorPrimaryPhone(rs.getString("proprietor_primary_phone"));
				vendor.setProprietorSecondaryPhone(rs.getString("proprietor_secondary_phone"));
				vendor.setProprietorFaxNo(rs.getString("proprietor_fax_no"));
				vendor.setOwnership(rs.getString("ownership"));
				vendor.setOwnershipOther(rs.getString("ownership_other"));
				vendor.setBusinessStartDate(new java.util.Date(rs.getDate("business_start_date").getTime()));
				vendor.setMerchantPan(rs.getString("merchant_pan"));
				vendor.setCreditCardAccepted(rs.getString("credit_card_accepted"));
				vendor.setCreditCardAcceptedOther(rs.getString("credit_card_accepted_other"));
				vendor.setAnnualTurnOver(rs.getString("annual_turn_over"));
				vendor.setAnnualTotalOnCreditCard(rs.getString("annual_total_on_credit_card"));
				vendor.setAveragePerTransaction(rs.getString("average_per_transaction"));
				vendor.setCreditCardBank(rs.getString("credit_card_bank"));
				vendor.setCreditCardSinceYear(rs.getString("credit_card_since_year"));
				vendor.setEbSavingAccount(rs.getString("eb_saving_account"));
				vendor.setEbCurrentAccount(rs.getString("eb_current_account"));
				vendor.setEbTermDeposit(rs.getString("eb_term_deposit"));
				vendor.setEbDemat(rs.getString("eb_demat"));
				vendor.setEbAutoLoan(rs.getString("eb_auto_loan"));
				vendor.setEbPersonalLoan(rs.getString("eb_personal_loan"));
				vendor.setEbTwoWheeler(rs.getString("eb_two_wheeler"));
				vendor.setEbCreditCard(rs.getString("eb_credit_card"));
				vendor.setEbLoanAgainstShares(rs.getString("eb_loan_against_shares"));
				vendor.setEbBusinessBanking(rs.getString("eb_business_banking"));
				vendor.setEbInsurance(rs.getString("eb_insurance"));
				vendor.setEbMutualFund(rs.getString("eb_mutual_fund"));
				vendor.setCardAcceptTypeVisa(rs.getString("card_accept_type_visa"));
				vendor.setCardAcceptTypeMaster(rs.getString("card_accept_type_master"));
				vendor.setCardAcceptTypeDinner(rs.getString("card_accept_type_dinner"));
				vendor.setPhysicalMpr(rs.getString("physical_mpr"));
				vendor.setEmailMpr(rs.getString("email_mpr"));
				vendor.setEmailMprText(rs.getString("email_mpr_text"));
				vendor.setMprFrequency(rs.getString("mpr_frequency"));
				vendor.setSellingCategory(rs.getString("selling_category"));
				vendor.setUserId(rs.getInt("user_id"));
			    vendor.setActive(rs.getString("active"));
				vendor.setIsDelete(rs.getString("is_delete"));*/
				return vendor;
			}});
		return vendorList;
	}

		public boolean insertVendor(Vendor vendor)throws UserAllreadyExistException {
		boolean flag = false;
		connection = connect.getConnection();
		Random random = new Random();
		Integer min = 0;
		Integer max = 999999;
		boolean isVendorExist = isUserNameExist(vendor);
		try {
			if (!isVendorExist) {
				// insert the user into user Table
				String sql = "insert into user (username, password, active, role_id, isDelete) values(?, ?, ?, ?, ?)";

				preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, vendor.getEmail());
				preparedStatement.setString(2, random.nextInt(max - min) + min + "");
				preparedStatement.setString(3, "Y");
				preparedStatement.setInt(4, 5);
				preparedStatement.setString(5, "N");
				preparedStatement.executeUpdate();

				// get inserted user id from database
				sql = "select max(id) as id from user";
				statement = connection.createStatement();
				resultSet = statement.executeQuery(sql);
				Integer userId = null;
				if (resultSet.next()) {
					userId = resultSet.getInt("id");
				}
				
				if(userId != null){
					// Now insert the vendor into database
					sql = "insert into vendor (merchant_legal_name, merchant_marketing_name, bank_account_number, me_code, t_id, " +
						  "lg_code, lc_code, branch, address_line1, area, city, pincode, primary_phone, secondary_phone, fax_no, email, " +
						  "shop_ownership, other_group, other_group_address, proprietor_name, proprietor_address_line1, " +
						  "proprietor_address_area, proprietor_address_city, proprietor_address_pincode, proprietor_primary_phone, " +
						  "proprietor_secondary_phone, proprietor_fax_no, ownership, ownership_other, business_start_date, merchant_pan, " +
						  "credit_card_accepted, credit_card_accepted_other, annual_turn_over, annual_total_on_credit_card, " +
						  "average_per_transaction, credit_card_bank, credit_card_since_year, eb_saving_account, eb_current_account, " +
						  "eb_term_deposit, eb_demat, eb_auto_loan, eb_personal_loan, eb_two_wheeler, eb_credit_card, eb_loan_against_shares, " +
						  "eb_business_banking, eb_insurance, eb_mutual_fund, card_accept_type_visa, card_accept_type_master, " +
						  "card_accept_type_dinner, physical_mpr, email_mpr, email_mpr_text, mpr_frequency, selling_category, " +
						  "user_id, active, is_delete) "
							+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
							" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
					
					preparedStatement = connection.prepareStatement(sql);
					preparedStatement.setString(1, vendor.getMerchantLegalName());
					preparedStatement.setString(2, vendor.getMerchantMarketingName());
					preparedStatement.setString(3, vendor.getBankAccountNumber());
					preparedStatement.setString(4, vendor.getMeCode());
					preparedStatement.setString(5, vendor.getTid());
					preparedStatement.setString(6, vendor.getLgCode());
					preparedStatement.setString(7, vendor.getLcCode());
					preparedStatement.setString(8, vendor.getBranch());
					preparedStatement.setString(9, vendor.getAddressLine1());
					preparedStatement.setString(10, vendor.getArea());
					preparedStatement.setString(11, vendor.getCity());
					preparedStatement.setString(12, vendor.getPinCode());
					preparedStatement.setString(13, vendor.getPrimaryPhone());
					preparedStatement.setString(14, vendor.getSecondaryPhone());
					preparedStatement.setString(15, vendor.getFaxNo());
					preparedStatement.setString(16, vendor.getEmail());
					preparedStatement.setString(17, vendor.getShopOwnership());
					preparedStatement.setString(18, vendor.getOtherGroup());
					preparedStatement.setString(19, vendor.getOtherGroupAddress());
					preparedStatement.setString(20, vendor.getProprietorName());
					preparedStatement.setString(21, vendor.getProprietorAddressLine1());
					preparedStatement.setString(22, vendor.getProprietorAddressArea());
					preparedStatement.setString(23, vendor.getProprietorAddressCity());
					preparedStatement.setString(24, vendor.getProprietorAddressPincode());
					preparedStatement.setString(25, vendor.getProprietorPrimaryPhone());
					preparedStatement.setString(26, vendor.getProprietorSecondaryPhone());
					preparedStatement.setString(27, vendor.getProprietorFaxNo());
					preparedStatement.setString(28, vendor.getOwnership());
					preparedStatement.setString(29, vendor.getOwnershipOther());
					preparedStatement.setDate(30, new java.sql.Date(vendor
							.getBusinessStartDate().getTime()));
					preparedStatement.setString(31, vendor.getMerchantPan());
					preparedStatement.setString(32, vendor.getCreditCardAccepted());
					preparedStatement.setString(33, vendor.getCreditCardAcceptedOther());
					preparedStatement.setString(34, vendor.getAnnualTurnOver());
					preparedStatement.setString(35, vendor.getAnnualTotalOnCreditCard());
					preparedStatement.setString(36, vendor.getAveragePerTransaction());
					preparedStatement.setString(37, vendor.getCreditCardBank());
					preparedStatement.setString(38, vendor.getCreditCardSinceYear());
					preparedStatement.setString(39, vendor.getEbSavingAccount());
					preparedStatement.setString(40, vendor.getEbCurrentAccount());
					preparedStatement.setString(41, vendor.getEbTermDeposit());
					preparedStatement.setString(42, vendor.getEbDemat());
					preparedStatement.setString(43, vendor.getEbAutoLoan());
					preparedStatement.setString(44, vendor.getEbPersonalLoan());
					preparedStatement.setString(45, vendor.getEbTwoWheeler());
					preparedStatement.setString(46, vendor.getEbCreditCard());
					preparedStatement.setString(47, vendor.getEbLoanAgainstShares());
					preparedStatement.setString(48, vendor.getEbBusinessBanking());
					preparedStatement.setString(49, vendor.getEbInsurance());
					preparedStatement.setString(50, vendor.getEbMutualFund());
					preparedStatement.setString(51, vendor.getCardAcceptTypeVisa());
					preparedStatement.setString(52, vendor.getCardAcceptTypeMaster());
					preparedStatement.setString(53, vendor.getCardAcceptTypeDinner());
					preparedStatement.setString(54, vendor.getPhysicalMpr());
					preparedStatement.setString(55, vendor.getEmailMpr());
					preparedStatement.setString(56, vendor.getEmailMprText());
					preparedStatement.setString(57, vendor.getMprFrequency());
					preparedStatement.setString(58, vendor.getSellingCategory());
					preparedStatement.setInt(59, userId);
					preparedStatement.setString(60, vendor.getActive());
					preparedStatement.setString(61, "N");
					preparedStatement.executeUpdate();
					if (vendor.getInstituteIdList() != null
							&& vendor.getInstituteIdList().size() > 0) {
						String instituteSql = "insert into branch_vendor (user_id, branch_id) values(?, ?)";
						PreparedStatement preparedStatement1 = connection
								.prepareStatement(instituteSql);
						for (Integer branchId : vendor.getInstituteIdList()) {
							preparedStatement1.setInt(1, userId);
							preparedStatement1.setInt(2, branchId);
							preparedStatement1.executeUpdate();
						}
					}
					flag = true;
				}
			} else {
				throw new UserAllreadyExistException(
						"Vendor already exist or emailid is used.");
			}
		} catch (UserAllreadyExistException e) {
			throw e;
		} catch (SQLException e) {
			logger.error("", e);
		} finally {
			connect.closeDatabaseTransaction(resultSet, statement,
					preparedStatement, connection);
		}
		return flag;
	}

	public Vendor getVendorById(Integer id) {
		Vendor vendor = null;
		connection = connect.getConnection();
		try {
			String sql = "select * from vendor where id = " + id;
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {

				vendor = new Vendor();
				vendor.setId(resultSet.getInt("id"));
				vendor.setMerchantLegalName(resultSet.getString("merchant_legal_name"));
				vendor.setMerchantMarketingName(resultSet.getString("merchant_marketing_name"));
				vendor.setBankAccountNumber(resultSet.getString("bank_account_number"));
				vendor.setMeCode(resultSet.getString("me_Code"));
				vendor.setTid(resultSet.getString("t_id"));
				vendor.setLgCode(resultSet.getString("lg_code"));
				vendor.setLcCode(resultSet.getString("lc_code"));
				vendor.setBranch(resultSet.getString("branch"));
				vendor.setAddressLine1(resultSet.getString("address_line1"));
				vendor.setArea(resultSet.getString("area"));
				vendor.setCity(resultSet.getString("city"));
				vendor.setPinCode(resultSet.getString("pinCode"));
				vendor.setPrimaryPhone(resultSet.getString("primary_phone"));
				vendor.setSecondaryPhone(resultSet.getString("secondary_phone"));
				vendor.setFaxNo(resultSet.getString("fax_no"));
				vendor.setEmail(resultSet.getString("email"));
				vendor.setShopOwnership(resultSet.getString("shop_ownership"));
				vendor.setOtherGroup(resultSet.getString("other_group"));
				vendor.setOtherGroupAddress(resultSet.getString("other_group_address"));
				vendor.setProprietorName(resultSet.getString("proprietor_name"));
				vendor.setProprietorAddressLine1(resultSet.getString("proprietor_address_line1"));
				vendor.setProprietorAddressArea(resultSet.getString("proprietor_address_area"));
				vendor.setProprietorAddressCity(resultSet.getString("proprietor_address_city"));
				vendor.setProprietorAddressPincode(resultSet.getString("proprietor_address_pincode"));
				vendor.setProprietorPrimaryPhone(resultSet.getString("proprietor_primary_phone"));
				vendor.setProprietorSecondaryPhone(resultSet.getString("proprietor_secondary_phone"));
				vendor.setProprietorFaxNo(resultSet.getString("proprietor_fax_no"));
				vendor.setOwnership(resultSet.getString("ownership"));
				vendor.setOwnershipOther(resultSet.getString("ownership_other"));
				vendor.setBusinessStartDate(new java.util.Date(resultSet.getDate("business_start_date").getTime()));
				vendor.setMerchantPan(resultSet.getString("merchant_pan"));
				vendor.setCreditCardAccepted(resultSet.getString("credit_card_accepted"));
				vendor.setCreditCardAcceptedOther(resultSet.getString("credit_card_accepted_other"));
				vendor.setAnnualTurnOver(resultSet.getString("annual_turn_over"));
				vendor.setAnnualTotalOnCreditCard(resultSet.getString("annual_total_on_credit_card"));
				vendor.setAveragePerTransaction(resultSet.getString("average_per_transaction"));
				vendor.setCreditCardBank(resultSet.getString("credit_card_bank"));
				vendor.setCreditCardSinceYear(resultSet.getString("credit_card_since_year"));
				vendor.setEbSavingAccount(resultSet.getString("eb_saving_account"));
				vendor.setEbCurrentAccount(resultSet.getString("eb_current_account"));
				vendor.setEbTermDeposit(resultSet.getString("eb_term_deposit"));
				vendor.setEbDemat(resultSet.getString("eb_demat"));
				vendor.setEbAutoLoan(resultSet.getString("eb_auto_loan"));
				vendor.setEbPersonalLoan(resultSet.getString("eb_personal_loan"));
				vendor.setEbTwoWheeler(resultSet.getString("eb_two_wheeler"));
				vendor.setEbCreditCard(resultSet.getString("eb_credit_card"));
				vendor.setEbLoanAgainstShares(resultSet.getString("eb_loan_against_shares"));
				vendor.setEbBusinessBanking(resultSet.getString("eb_business_banking"));
				vendor.setEbInsurance(resultSet.getString("eb_insurance"));
				vendor.setEbMutualFund(resultSet.getString("eb_mutual_fund"));
				vendor.setCardAcceptTypeVisa(resultSet.getString("card_accept_type_visa"));
				vendor.setCardAcceptTypeMaster(resultSet.getString("card_accept_type_master"));
				vendor.setCardAcceptTypeDinner(resultSet.getString("card_accept_type_dinner"));
				vendor.setPhysicalMpr(resultSet.getString("physical_mpr"));
				vendor.setEmailMpr(resultSet.getString("email_mpr"));
				vendor.setEmailMprText(resultSet.getString("email_mpr_text"));
				vendor.setMprFrequency(resultSet.getString("mpr_frequency"));
				vendor.setSellingCategory(resultSet.getString("selling_category"));
				vendor.setUserId(resultSet.getInt("user_id"));
			    vendor.setActive(resultSet.getString("active"));
				vendor.setIsDelete(resultSet.getString("is_delete"));
				
				resultSet = statement
						.executeQuery("select * from user where id ="
								+ vendor.getUserId());
				if (resultSet.next()) {
					vendor.setUserId(resultSet.getInt("id"));
				/*	vendor.setRoleId(resultSet.getInt("role_id"));
					vendor.setPassword(resultSet.getString("password"));*/
				}

				/*List<Master> instituteList = new ArrayList<Master>();
				List<Integer> instituteIdList = new ArrayList<Integer>();
				resultSet = statement
						.executeQuery("select * from branch_vendor where user_id ="
								+ vendor.getUserId());

				MasterDao masterDao = new MasterDao();
				Integer userId = null;
				while (resultSet.next()) {
					userId = resultSet.getInt("user_id");
					int branchId = resultSet.getInt("branch_id");
					instituteIdList.add(branchId);
				}
				instituteList = masterDao.getInstituteFromVendor(userId);
				vendor.setInstituteIdList(instituteIdList);
				vendor.setInstitutes(instituteList);*/
			}
		} catch (SQLException e) {
			logger.error("", e);
		} finally {
			connect.closeDatabaseTransaction(resultSet, statement,
					preparedStatement, connection);
		}
		return vendor;
	}
	
	public boolean updateVendor(Vendor vendor)
			throws UserAllreadyExistException {
		boolean flag = false;
		connection = connect.getConnection();
		boolean isVendorExist = isUserNameExist(vendor);
		try {
			if (!isVendorExist) {
				String sql = "update user set username = ?, active = ?, role_id = ?, isDelete = ? where id = ?";

				preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, vendor.getEmail());
				preparedStatement.setString(2, vendor.getActive());
				preparedStatement.setInt(3, 5);
				preparedStatement.setString(4, "N");
				preparedStatement.setInt(5, vendor.getUserId());
				preparedStatement.executeUpdate();

				sql = "update vendor set merchant_legal_name = ? , merchant_marketing_name = ? , bank_account_number = ? , me_code = ?, t_id = ?,"
						+ " lg_code = ?, lc_code = ? , branch = ? , address_line1 = ? , area = ?, city = ?, pincode = ?, primary_phone = ?,"
						+ " secondary_phone = ?, fax_no = ?, email = ?, shop_ownership = ?, other_group = ?, other_group_address = ?,"
						+ " proprietor_name = ?, proprietor_address_line1 = ?, proprietor_address_area = ?, proprietor_address_city = ?,"
						+ " proprietor_address_pincode = ?, proprietor_primary_phone = ?, proprietor_secondary_phone = ?, proprietor_fax_no = ?,"
						+ " ownership = ?, ownership_other = ?, business_start_date = ?, merchant_pan = ?, credit_card_accepted = ?,"
						+ " credit_card_accepted_other = ?, annual_turn_over = ?, annual_total_on_credit_card = ?, average_per_transaction = ?,"
						+ " credit_card_bank = ?, credit_card_since_year = ?, eb_saving_account = ?, eb_current_account = ?, eb_term_deposit = ?,"
						+ " eb_demat = ?, eb_auto_loan = ?, eb_personal_loan = ?, eb_two_wheeler = ?, eb_credit_card = ?,"
						+ " eb_loan_against_shares = ?, eb_business_banking = ?, eb_insurance = ?, eb_mutual_fund = ?, card_accept_type_visa = ?,"
						+ " card_accept_type_master = ?, card_accept_type_dinner = ?, physical_mpr = ?, email_mpr = ?, email_mpr_text = ?,"
						+ " mpr_frequency = ?, selling_category = ?, user_id = ?, active = ?, is_delete = ?"
						+ " where id = ?";

				preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, vendor.getMerchantLegalName());
				preparedStatement.setString(2, vendor.getMerchantMarketingName());
				preparedStatement.setString(3, vendor.getBankAccountNumber());
				preparedStatement.setString(4, vendor.getMeCode());
				preparedStatement.setString(5, vendor.getTid());
				preparedStatement.setString(6, vendor.getLgCode());
				preparedStatement.setString(7, vendor.getLcCode());
				preparedStatement.setString(8, vendor.getBranch());
				preparedStatement.setString(9, vendor.getAddressLine1());
				preparedStatement.setString(10, vendor.getArea());
				preparedStatement.setString(11, vendor.getCity());
				preparedStatement.setString(12, vendor.getPinCode());
				preparedStatement.setString(13, vendor.getPrimaryPhone());
				preparedStatement.setString(14, vendor.getSecondaryPhone());
				preparedStatement.setString(15, vendor.getFaxNo());
				preparedStatement.setString(16, vendor.getEmail());
				preparedStatement.setString(17, vendor.getShopOwnership());
				preparedStatement.setString(18, vendor.getOtherGroup());
				preparedStatement.setString(19, vendor.getOtherGroupAddress());
				preparedStatement.setString(20, vendor.getProprietorName());
				preparedStatement.setString(21, vendor.getProprietorAddressLine1());
				preparedStatement.setString(22, vendor.getProprietorAddressArea());
				preparedStatement.setString(23, vendor.getProprietorAddressCity());
				preparedStatement.setString(24, vendor.getProprietorAddressPincode());
				preparedStatement.setString(25, vendor.getProprietorPrimaryPhone());
				preparedStatement.setString(26, vendor.getProprietorSecondaryPhone());
				preparedStatement.setString(27, vendor.getProprietorFaxNo());
				preparedStatement.setString(28, vendor.getOwnership());
				preparedStatement.setString(29, vendor.getOwnershipOther());
				preparedStatement.setDate(30, new java.sql.Date(vendor
						.getBusinessStartDate().getTime()));
				preparedStatement.setString(31, vendor.getMerchantPan());
				preparedStatement.setString(32, vendor.getCreditCardAccepted());
				preparedStatement.setString(33, vendor.getCreditCardAcceptedOther());
				preparedStatement.setString(34, vendor.getAnnualTurnOver());
				preparedStatement.setString(35, vendor.getAnnualTotalOnCreditCard());
				preparedStatement.setString(36, vendor.getAveragePerTransaction());
				preparedStatement.setString(37, vendor.getCreditCardBank());
				preparedStatement.setString(38, vendor.getCreditCardSinceYear());
				preparedStatement.setString(39, vendor.getEbSavingAccount());
				preparedStatement.setString(40, vendor.getEbCurrentAccount());
				preparedStatement.setString(41, vendor.getEbTermDeposit());
				preparedStatement.setString(42, vendor.getEbDemat());
				preparedStatement.setString(43, vendor.getEbAutoLoan());
				preparedStatement.setString(44, vendor.getEbPersonalLoan());
				preparedStatement.setString(45, vendor.getEbTwoWheeler());
				preparedStatement.setString(46, vendor.getEbCreditCard());
				preparedStatement.setString(47, vendor.getEbLoanAgainstShares());
				preparedStatement.setString(48, vendor.getEbBusinessBanking());
				preparedStatement.setString(49, vendor.getEbInsurance());
				preparedStatement.setString(50, vendor.getEbMutualFund());
				preparedStatement.setString(51, vendor.getCardAcceptTypeVisa());
				preparedStatement.setString(52, vendor.getCardAcceptTypeMaster());
				preparedStatement.setString(53, vendor.getCardAcceptTypeDinner());
				preparedStatement.setString(54, vendor.getPhysicalMpr());
				preparedStatement.setString(55, vendor.getEmailMpr());
				preparedStatement.setString(56, vendor.getEmailMprText());
				preparedStatement.setString(57, vendor.getMprFrequency());
				preparedStatement.setString(58, vendor.getSellingCategory());
				preparedStatement.setInt(59, vendor.getUserId());
				preparedStatement.setString(60, vendor.getActive());
				preparedStatement.setString(61, "N");
				preparedStatement.setInt(62, vendor.getId());
				preparedStatement.executeUpdate();

				/*sql = "delete from branch_vendor where user_id = "
						+ vendor.getUserId();
				statement = connection.createStatement();
				statement.executeUpdate(sql);

				// Now insert the institute and user mapping into database
				sql = "insert into branch_vendor (user_id, branch_id) values(?, ?)";
				preparedStatement = connection.prepareStatement(sql);
				if (vendor.getInstituteIdList() != null) {
					for (Integer branchId : vendor.getInstituteIdList()) {
						preparedStatement.setInt(1, vendor.getUserId());
						preparedStatement.setInt(2, branchId);
						preparedStatement.executeUpdate();
					}
				}*/
				flag = true;
			} else {
				throw new UserAllreadyExistException(
						"Vendor already exist or email id is used.");
			}
		} catch (SQLException e) {
			logger.error("", e);
		} finally {
			connect.closeDatabaseTransaction(resultSet, statement,
					preparedStatement, connection);
		}
		return flag;
	}

	public boolean deleteVendor(Integer id) {
		boolean flag = false;
		connection = connect.getConnection();
		try {

			statement = connection.createStatement();
			String sql = "select user_id from vendor where id = " + id;
			resultSet = statement.executeQuery(sql);
			int userId = 0;
			if (resultSet.next()) {
				userId = resultSet.getInt("user_id");
			}
			// delete from vendor first ..
			sql = "update vendor set is_delete = 'Y' where id = " + id;
			statement.executeUpdate(sql);

			// delete from user
			sql = "update user set isDelete = 'Y' where id = " + userId;
			statement.executeUpdate(sql);
			flag = true;
			logger.debug("vendor deleted..."+id);
		} catch (SQLException e) {
			logger.error("", e);
		} finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}
/*
	public List<Vendor> getUploadingFailedRecords() {
		return errorVendorList;
	}
*/
	private boolean isUserNameExist(Vendor vendor) {
		boolean flag = false;
		try {
			statement = connection.createStatement();
			String sql = "select count(*) as count from vendor where email = '" + vendor.getEmail()+ "'"
					+ (vendor.getId() != null ? " AND id != " + vendor.getId() : "");
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				if (resultSet.getInt("count") > 0) {
					flag = true;
				}
			}
		} catch (SQLException e) {
			logger.error("", e);
		}
		return flag;
	}

	
	private boolean isVendorExist(Integer userId, Integer branchId) {
		boolean flag = false;
		try {
			statement = connection.createStatement();
			String sql = "select count(*) as count from branch_vendor where user_id = '"
					+ userId + "' AND branch_id = '" + branchId + "'";
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				if (resultSet.getInt("count") > 0) {
					flag = true;
				}
			}
		} catch (SQLException e) {
			logger.error("", e);
		}
		return flag;
	}
	
	public boolean attachVendorToBranch (Integer userId, Integer branchId)
			throws UserAllreadyExistException, BranchForumNotExistException {
		
		boolean flag = false;
		connection = connect.getConnection();
		try {
			if (!isVendorExist(userId, branchId)) {
				logger.debug("Attaching vendor to branch.. branchId --"+branchId+", vendorId --"+userId);
				
				String sql = "select shopping_profile_id from user_mall_profile where user_id = "+userId;
				statement = connection.createStatement();
				resultSet = statement.executeQuery(sql);
				Integer vendorProfileId = null;
				if(resultSet.next()){
					vendorProfileId = resultSet.getInt("shopping_profile_id");
				}
				if(vendorProfileId != null && vendorProfileId != 0){
					sql = "insert into branch_vendor (user_id, branch_id) values(?, ?)";
					preparedStatement = connection.prepareStatement(sql);
					preparedStatement.setInt(1, userId);
					preparedStatement.setInt(2, branchId);
					preparedStatement.executeUpdate();

					if(new ShoppingDao().attachVendorAndBranch(vendorProfileId, branchId)){
						resultSet = statement.executeQuery("select forum_category_id from branch_forum_detail " +
								"where branch_id = "+branchId);
						Integer forumCategoryId = null;
						if(resultSet.next()){
							forumCategoryId = resultSet.getInt("forum_category_id");
							if(forumCategoryId != null){
								
								createUserForumProfileByUserId(forumCategoryId, userId);
								flag = true;
							}
						}
						else {
							throw new BranchForumNotExistException("This branch is not registered with forum.");
						}
					}
				}
			} else {
				throw new UserAllreadyExistException("Vendor already attached.");
			}
		} catch (UserAllreadyExistException e) {
			throw e;
		} catch (SQLException e) {
			logger.error("", e);
		} finally {
			connect.closeDatabaseTransaction(resultSet, statement,
					preparedStatement, connection);
		}
		return flag;
	}

	

	private void createUserForumProfileByUserId(Integer forumCategoryId, Integer userId) throws SQLException {
		Statement statement = connection.createStatement();
		String sql = "select u.*,v.email, ufp.forum_profile_id from user as u LEFT JOIN user_forum_profile as ufp on ufp.user_id = u.id INNER JOIN vendor as v on v.user_id = u.id where u.id = "+userId;
		ResultSet resultSet = statement.executeQuery(sql);
		String username = null;
		String password = null;
		String email = null;
		int forumProfileId = 0;
		
		if(resultSet.next()){
			username = resultSet.getString("username");
			password = resultSet.getString("password");
			email = resultSet.getString("email");
			forumProfileId = resultSet.getInt("forum_profile_id");
		}
		
		if(forumProfileId == 0){
			forumProfileId = createUserForumProfile(username, password, email, forumCategoryId, userId);
		}
		
		if(forumProfileId != 0){
			linkUserToForum(forumCategoryId, forumProfileId);	
		}
		resultSet.close();
		statement.close();
	}
	
	

	private Integer createUserForumProfile(String username, String password, String email, Integer forumCategoryId, Integer userId) throws SQLException {
		CreateUserResponse userResponse = new ForumProfileDao().createUserAccount(username,password,email,forumCategoryId, null, false, false);
		

		String sql = "insert into user_forum_profile (login, password, forum_profile_id, is_active, is_delete ,user_id) values (?,?,?,?,?,?)";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, username);
		ps.setString(2, password);
		ps.setInt(3, userResponse.getForum_profile_id());
		ps.setString(4, "Y");
		ps.setString(5, "N");
		ps.setInt(6, userId);
		ps.executeUpdate();
		ps.close();
		return userResponse.getForum_profile_id();
	}

	private void linkUserToForum(Integer forumCategoryId, Integer forumProfileId) throws SQLException {
		if(forumProfileId != null && forumProfileId != 0){
			Set<Integer> userForumProfileIdList = new HashSet<>();
			userForumProfileIdList.add(forumProfileId);
			
			ForumProfileDao forumProfileDao = new ForumProfileDao();
			forumProfileDao.linkUserToForum(forumCategoryId, userForumProfileIdList);
		}	
	}

	public boolean deAttachVendorFromBranch(Integer userId, Integer branchId) {
		boolean flag = false;
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = "delete from branch_vendor where user_id ="
					+ userId + " AND branch_id= " + branchId;
			statement.executeUpdate(sql);
			deattchVendorFromShopping(userId, branchId);
			deattchVendorFromForum(userId, branchId);
			flag = true;
			logger.debug("vendor deattched to branch.. branchId --"+branchId+", vendorId --"+userId);
		} catch (SQLException e) {
			logger.error("", e);
		} finally {
			connect.closeDatabaseTransaction(resultSet, statement,
					preparedStatement, connection);
		}
		return flag;
	}


	private void deattchVendorFromShopping(Integer userId, Integer branchId)throws SQLException {
		Statement statement = connection.createStatement();
		String sql = "select shopping_profile_id from user_mall_profile where user_id = "+userId;
		ResultSet resultSet = statement.executeQuery(sql);
		Integer vendorProfileId = null;
		if(resultSet.next()){
			vendorProfileId = resultSet.getInt("shopping_profile_id");
		}
		new ShoppingDao().deAttachVendorAndBranch(vendorProfileId, branchId);
	}

	private void deattchVendorFromForum(Integer userId, Integer branchId) throws SQLException {
		Statement statement = connection.createStatement();
		String sql = "select forum_profile_id from user_forum_profile where user_id = "+userId;
		ResultSet resultSet = statement.executeQuery(sql);
		Integer forumProfileId = null;
		if(resultSet.next()){
			forumProfileId = resultSet.getInt("forum_profile_id");
		}
		sql = "select forum_category_id from branch_forum_detail where branch_id = "+branchId;
		resultSet = statement.executeQuery(sql);
		Integer forumCategoryId = null;
		if(resultSet.next()){
			forumCategoryId = resultSet.getInt("forum_category_id");
		}
		if(forumCategoryId != null && forumProfileId != null){
			Set<Integer> deletedUsers = new HashSet<>();
			deletedUsers.add(forumProfileId);
			deLinkUserToForum(deletedUsers, forumCategoryId);
		}
	}

	private void deLinkUserToForum(Set<Integer> deletedUserForumIdList, Integer forumGroupId) throws SQLException {
		ForumProfileDao forumProfileDao = new ForumProfileDao();
		forumProfileDao.deLinkUserToForum(forumGroupId, deletedUserForumIdList);
	}

	public List<Vendor> getVendorByBranchId(Integer branchId) {
		List<Vendor> vendorList = new ArrayList<Vendor>();
		try {
			connection = connect.getConnection();
			statement = connection.createStatement();
			String sql = null;
			sql = "select v.*,b.id as branchId, b.name as branch from vendor as v, "
					+ "branch as b where v.active = 'Y' AND b.id = " + branchId + " order by b.id";

			resultSet = statement.executeQuery(sql);
			sql = "select count(*) as count from branch_vendor where user_id = ? AND branch_id = ?";
			preparedStatement = connection.prepareStatement(sql);
			while (resultSet.next()) {
				Vendor vendor = new Vendor();
				vendor.setId(resultSet.getInt("id"));
				vendor.setMerchantLegalName(resultSet.getString("merchant_legal_name"));
				vendor.setMerchantMarketingName(resultSet.getString("marketing_name"));
				vendor.setUserId(resultSet.getInt("user_id"));
				
				preparedStatement.setInt(1, vendor.getUserId());
				preparedStatement.setInt(2, branchId);
				ResultSet vendorResultSet = preparedStatement.executeQuery();
				if (vendorResultSet.next()) {
					if (vendorResultSet.getInt("count") > 0) {
						vendor.setVendorExist(true);
					} else {
						vendor.setVendorExist(false);
					}
				}
				vendorList.add(vendor);
			}
		} catch (SQLException e) {
			logger.error("", e);
		} finally {
			connect.closeDatabaseTransaction(resultSet, statement,
					preparedStatement, connection);
		}
		return vendorList;
	}
}
