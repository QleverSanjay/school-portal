package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.qfix.dao.DivisionInterfaceDao;
import com.qfix.exceptions.DivisionAllreadyExistException;
import com.qfix.model.Division;

@Repository
public class DivisionDao extends JdbcDaoSupport implements DivisionInterfaceDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	final static Logger logger = Logger.getLogger(DivisionDao.class);
	
	@Transactional
	public List<Division> getAll(Integer branchId){

			String sql = "select * from division where active = 'Y' AND branch_id = "+branchId+ " AND " +
					 "academic_year_id = (select id from academic_year where " +
					 "is_current_active_year = 'Y' and branch_id = "+branchId+")";
			
			
			return getJdbcTemplate().query(sql, new RowMapper<Division>() {
				public Division mapRow(ResultSet rs, int rownumber)throws SQLException {
					
				Division division = new Division();
				division.setId(rs.getInt("id"));
				division.setCode(rs.getString("code"));
				division.setDisplayName(rs.getString("displayName"));
				division.setBranchId(rs.getInt("branch_id"));
				division.setAcademicYearId(rs.getInt("academic_year_id"));
				return division;
			}});
	}
	
	@Transactional
	public Division getDivisionById(Integer id){
			String sql = "select d.*, i.id as instituteId from division as d, branch as b, institute as i " +
					"where d.branch_id = b.id AND b.institute_id = i.id AND d.id = "+id;
			
			List<Division> divisionList =  getJdbcTemplate().query(sql, new RowMapper<Division>() {
				public Division mapRow(ResultSet resultSet, int rownumber)
						throws SQLException {
					Division division = new Division();
					division.setId(resultSet.getInt("id"));
					division.setCode(resultSet.getString("code"));
					division.setDisplayName(resultSet.getString("displayName"));
					division.setBranchId(resultSet.getInt("branch_id"));
					division.setInstituteId(resultSet.getInt("instituteId"));
					division.setAcademicYearId(resultSet.getInt("academic_year_id"));
					return division;
				}
			});
		return divisionList.get(0);
			
	}
	
	@Transactional
	public boolean insertDivision(Division division) throws DivisionAllreadyExistException{
		boolean flag = false;
			if(!isDivisionExist(division)){
				String sql = "insert into division (code, displayName, branch_id, active, academic_year_id) " +
							 "values (?, ?, ?, ?, ?)";
				
				Object[] params = new Object[] {
						division.getCode(),
						division.getDisplayName(),
						division.getBranchId(),
						"Y",
						division.getAcademicYearId()
				};
					
				int[] types = new int[] { Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.VARCHAR,Types.INTEGER};
					
				getJdbcTemplate().update(sql, params, types);

				flag = true;
			}			
			else {
				throw new DivisionAllreadyExistException("This division is already exist in this Branch.");
			}
		return flag;
	}

	private boolean isDivisionExist(Division division){
		String sql = "select count(*) as count from division where active = 'Y' AND displayName = '"+division.getDisplayName()+
				"' AND academic_year_id = "+division.getAcademicYearId()+" AND branch_id = "+division.getBranchId();
		
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			return true;
		}
		return false;
	}

	@Transactional
	public boolean updateDivision(Division division) throws DivisionAllreadyExistException{
		boolean flag = false;
		if(!isDivisionExist(division)){
			String sql = "update division set code = ?, displayName = ?, branch_id = ?, academic_year_id = ? " +
						 "where id = ?";
			Object[] params = new Object[] {
					division.getCode(),
					division.getDisplayName(),
					division.getBranchId(),
					division.getAcademicYearId(),
					division.getId()
			};
				
			int[] types = new int[] { Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.INTEGER};
				
			getJdbcTemplate().update(sql, params, types);
			flag = true;
		}else {
			throw new DivisionAllreadyExistException("This division is already exist in this Branch.");
		}
			
		return flag;
	}

	@Transactional
	public boolean deleteDivision(Integer divisionId) {
		String sql = "update division set active = 'N' where id = " + divisionId;
		getJdbcTemplate().update(sql);
		return true;
	}
	
	

	@Override
	public void deleteBranchDivisions(Integer branchId) {
		String sql = "delete from division where branch_id = " + branchId;
		getJdbcTemplate().update(sql);
	}

	@Override
	public List<Division> getAllByStandardTimetable(Integer standardId) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM division d "+
					" inner join standard_division sd on sd.division_id = d.id where d.active = 'Y' AND sd.standard_id = " + standardId
				    + " and d.id not in (select td.division_id from timetable_division td inner join timetable t on t.id = td.timetable_id " +
								"where t.standard_id = "+ standardId +" AND t.is_delete = 'N' AND td.is_delete = 'N')";

		
		return getJdbcTemplate().query(sql, new RowMapper<Division>() {
			public Division mapRow(ResultSet rs, int rownumber)throws SQLException {
				
			Division division = new Division();
			division.setId(rs.getInt("id"));
			division.setCode(rs.getString("code"));
			division.setDisplayName(rs.getString("displayName"));
			division.setBranchId(rs.getInt("branch_id"));
			division.setAcademicYearId(rs.getInt("academic_year_id"));
			return division;
		}});
	}


	@Override
	public List<Division> getUnusedDivisionsByStandardTimetable(Integer timetableId) {
		
		List<Division> unUsedDivisionList = new ArrayList<Division>();

		//List unUsedDivisionList = getAllByStandardTimetable(standardId);
		String sql = "select t.standard_id from timetable t where t.is_delete = 'N' and t.id = "+timetableId;

		Integer standardId = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(standardId > 0){
			
			unUsedDivisionList = getAllByStandardTimetable(standardId);
			
			String divList = "select d.* from division d, timetable_division td where d.id = td.division_id and td.is_delete = 'N' and td.timetable_id = "+timetableId;
			
			List<Division> divisionList = getJdbcTemplate().query(divList, new RowMapper<Division>(){
				@Override
				public Division mapRow(ResultSet rs, int arg1)
						throws SQLException {
					Division division = new Division();
					division.setId(rs.getInt("id"));
					division.setCode(rs.getString("code"));
					division.setDisplayName(rs.getString("displayName"));
					division.setBranchId(rs.getInt("branch_id"));
					division.setAcademicYearId(rs.getInt("academic_year_id"));
					return division;
				}
			});			
			//also add existing divisionids to this list
			unUsedDivisionList.addAll(divisionList);
		}
		return unUsedDivisionList;
		
	}


	@Override
	public Map<String, Integer> insertDivisions(final Integer branchId,final Integer academicYearId,List<Division> divisions) {
		
		final String sql = "insert into division (code, displayName, branch_id, active, qfix_defined_name, academic_year_id) " +
				 "values (?, ?, ?, ?, ?, ?)";
		final Map<String, Integer> divisionMap = new HashMap<String, Integer>();

		for(final Division division : divisions){
			
			String name = division.getName();
			
			if(StringUtils.isEmpty(name)){
				name = division.getText();
			}
			
			division.setDisplayName(name);
			division.setAcademicYearId(academicYearId);
			division.setBranchId(branchId);
			boolean isDivisionExist = isDivisionExist(division);
			
			if(!isDivisionExist){				
				KeyHolder holder = new GeneratedKeyHolder();
				getJdbcTemplate().update(new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
						ps.setString(1, null);
						ps.setString(2, division.getDisplayName());
						ps.setInt(3, branchId);
						ps.setString(4, "Y");
						ps.setString(5, division.getQfixDefinedName());
						ps.setInt(6, academicYearId);
						return ps;
					}
				}, holder);
				division.setId(holder.getKey().intValue());
				divisionMap.put(division.getDisplayName(), division.getId());
			}else{
				Integer id = getDivisionIdByName(division.getDisplayName(), branchId, academicYearId);
				if(id > 0){					
					division.setId(id);
				}
			}
		}
		return divisionMap;
	}
	
	public Integer getDivisionIdByName(String name,Integer branchId,Integer ayId){
		String query = "SELECT id FROM division where displayName=? AND branch_id=? AND academic_year_id=?";
		
		Object[] objects = new Object[]{name,branchId,ayId};
		
		try{
		return getJdbcTemplate().queryForObject(query, objects,Integer.class);
		}catch(EmptyResultDataAccessException e){
			return 0;
		}
	}
}