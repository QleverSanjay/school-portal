package com.qfix.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.qfix.dao.SchoolDao;
import com.qfix.model.SchoolBoard;

@Repository
public class SchoolDaoImpl extends JdbcDaoSupport implements SchoolDao {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public List<SchoolBoard> listBoards() {
		String query = "SELECT * FROM school_board_affiliation ORDER BY board ASC";
		return getJdbcTemplate().query(query, new BeanPropertyRowMapper<SchoolBoard>(SchoolBoard.class));
	}

}
