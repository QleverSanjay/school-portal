package com.qfix.daoimpl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.dao.IDisplayTemplateDao;
import com.qfix.exceptions.ApplicationException;
import com.qfix.model.DisplayHead;
import com.qfix.model.DisplayTemplate;
import com.qfix.utilities.ReadWriteExcelUtil;

@Repository
public class DisplayTemplateDao extends JdbcDaoSupport implements
		IDisplayTemplateDao {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(DisplayTemplateDao.class);

	@Transactional
	@Override
	public void insertDisplayHead(final DisplayHead head) throws ApplicationException {
		checkDisplayHeadExistByName(head);
		final String sql = "insert into display_head(name, description, branch_id, active) values (?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection conn)
					throws SQLException {
				PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, head.getName());
				stmt.setString(2, head.getDescription());
				stmt.setInt(3, head.getBranchId());
				stmt.setString(4, head.getActive());
				return stmt;
			}
		}, holder);
		head.setId(holder.getKey().intValue());
	}
	
	private void checkDisplayHeadExistByName(DisplayHead head) throws ApplicationException {
		String sql = "select count(*) from display_head where is_delete = 'N' AND name = '"+head.getName()+"' AND branch_id = " + head.getBranchId();
		if(head.getId() != null){
			sql += " AND id != " + head.getId();
		}
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count> 0){
			throw new ApplicationException("Display Head already exist by this name.");
		}
	}

	@Transactional
	@Override
	public void updateDisplayHead(DisplayHead head) throws ApplicationException {
		checkDisplayHeadExistByName(head);
		String sql = "update display_head set name = ?, description = ?, branch_id = ?, active = ? where id = ?";

		Object[] args = new Object[] { head.getName(), head.getDescription(),
				head.getBranchId(), head.getActive(), head.getId() };

		int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
				Types.INTEGER, Types.VARCHAR, Types.INTEGER };
		getJdbcTemplate().update(sql, args, argTypes);
	}

	@Override
	public List<DisplayHead> getDisplayHeads(Integer branchId) {
		String sql = "select * from display_head where is_delete = 'N' AND branch_id = "
				+ branchId;
		List<DisplayHead> displayHeads = getJdbcTemplate().query(sql,
				new RowMapper<DisplayHead>() {
					@Override
					public DisplayHead mapRow(ResultSet rs, int arg1)
							throws SQLException {
						DisplayHead head = new DisplayHead();
						head.setId(rs.getInt("id"));
						head.setName(rs.getString("name"));
						head.setDescription(rs.getString("description"));
						head.setActive(rs.getString("active"));
						head.setBranchId(rs.getInt("branch_id"));
						return head;
					}
				});
		return displayHeads;
	}

	@Override
	public DisplayHead getDisplayHead(Integer displayHeadId) {
		String sql = "select dh.*, institute_id from display_head as dh " +
				" INNER JOIN branch as b on b.id = dh.branch_id " +
				" where dh.is_delete = 'N' AND dh.id = "
				+ displayHeadId;

		return getDisplayHead(sql);
	}

	private DisplayHead getDisplayHead(String sql) {
		List<DisplayHead> displayHeads = getJdbcTemplate().query(sql,
				new RowMapper<DisplayHead>() {
					@Override
					public DisplayHead mapRow(ResultSet rs, int arg1)
							throws SQLException {
						DisplayHead head = new DisplayHead();
						head.setId(rs.getInt("id"));
						head.setName(rs.getString("name"));
						head.setDescription(rs.getString("description"));
						head.setActive(rs.getString("active"));
						head.setBranchId(rs.getInt("branch_id"));
						head.setInstituteId(rs.getInt("institute_id"));
						return head;
					}
				});
		if(displayHeads != null && displayHeads.size() > 0){
			return displayHeads.get(0);
		}
		return null;
	}

	@Transactional
	@Override
	public void deleteDisplayHead(Integer displayHeadId) {
		String sql = "update display_head set is_delete = 'Y' where id = "
				+ displayHeadId;
		getJdbcTemplate().update(sql);
	}

	@Transactional
	@Override
	public void updateDisplayTemplate(DisplayTemplate template) throws ApplicationException {
		String sql = "select count(*) from fees where display_template_id = " + template.getId();
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			throw new ApplicationException("Cannot update this template as it is being used!!");
		}

		sql = "update display_template set name = ?, description = ?, branch_id = ?, active = ? where id = ?";
		Object[] args = new Object[] { template.getName(), template.getDescription(),
				template.getBranchId(), template.getActive(), template.getId() };

		int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
				Types.INTEGER, Types.VARCHAR, Types.INTEGER };
		getJdbcTemplate().update(sql, args, argTypes);

		deleteDisplayTemplateHeads(template.getId());
		insertDisplayTemplateHeds(template);
	}

	private void deleteDisplayTemplateHeads(Integer displayTemplateId) {
		String sql = "delete from display_template_head  where display_template_id = " + displayTemplateId;
		getJdbcTemplate().update(sql);
	}

	@Transactional
	@Override
	public void insertDisplayTemplate(final DisplayTemplate template) throws ApplicationException {
		final String sql = "insert into display_template(name, description, branch_id, active) values (?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection conn)
					throws SQLException {
				PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, template.getName());
				stmt.setString(2, template.getDescription());
				stmt.setInt(3, template.getBranchId());
				stmt.setString(4, template.getActive());
				return stmt;
			}
		}, holder);
		template.setId(holder.getKey().intValue());
		insertDisplayTemplateHeds(template);
	}

	private void insertDisplayTemplateHeds(DisplayTemplate template) {
		if(template.getDisplayHeadList() != null){
			final List<DisplayHead> displayHeads = template.getDisplayHeadList();
			final Integer templateId = template.getId();

			String sql = "insert into display_template_head values (?, ?, ?, ?)";
			getJdbcTemplate().batchUpdate(sql,
					new BatchPreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement ps, int index)
								throws SQLException {
							DisplayHead head = displayHeads.get(index);
							ps.setInt(1, head.getId());
							ps.setInt(2, templateId);
							ps.setDouble(3, head.getAmount());
							ps.setInt(4, head.getDisplayOrder() == null ? index : head.getDisplayOrder());
						}

						@Override
						public int getBatchSize() {
							return displayHeads.size();
						}
					});
		}
	}

	@Override
	public List<DisplayTemplate> getDisplayTemplates(Integer branchId) {
		String sql = "select * from display_template where branch_id = " 
				+ branchId +" AND is_delete = 'N' ";

		List<DisplayTemplate> displayTemplates = getJdbcTemplate().query(sql,
					new RowMapper<DisplayTemplate>() {
						@Override
						public DisplayTemplate mapRow(ResultSet rs, int arg1)
								throws SQLException {
							DisplayTemplate template = new DisplayTemplate();
							template.setId(rs.getInt("id"));
							template.setName(rs.getString("name"));
							template.setDescription(rs.getString("description"));
							template.setActive(rs.getString("active"));
							template.setBranchId(rs.getInt("branch_id"));
							return template;
						}
					});
			return displayTemplates;
	}

	@Override
	public DisplayTemplate getDisplayTemplate(Integer displayTemplateId) {
		String sql = "select dt.*, institute_id from display_template as dt " +
				" INNER JOIN branch as b on b.id = dt.branch_id " +
				" where dt.id = " + displayTemplateId +" AND dt.is_delete = 'N' ";

		List<DisplayTemplate> displayTemplates = getJdbcTemplate().query(sql,
					new RowMapper<DisplayTemplate>() {
						@Override
						public DisplayTemplate mapRow(ResultSet rs, int arg1)
								throws SQLException {
							DisplayTemplate template = new DisplayTemplate();
							template.setId(rs.getInt("id"));
							template.setName(rs.getString("name"));
							template.setDescription(rs.getString("description"));
							template.setActive(rs.getString("active"));
							template.setBranchId(rs.getInt("branch_id"));
							template.setInstituteId(rs.getInt("institute_id"));
							template.setDisplayHeadList(getDisplayHeadsByTemplateId(template.getId()));
							return template;
						}
					});
			return displayTemplates.get(0);
	}

	private List<DisplayHead> getDisplayHeadsByTemplateId(Integer displayTemplateId) {
		String sql = "select dh.*, dth.amount from display_head as dh " +
				" INNER JOIN display_template_head as dth on dth.display_head_id = dh.id " +
				" where is_delete = 'N' AND dth.display_template_id = " + displayTemplateId +
				" order by display_order ";

		List<DisplayHead> displayHeads = getJdbcTemplate().query(sql,
				new RowMapper<DisplayHead>() {
					@Override
					public DisplayHead mapRow(ResultSet rs, int arg1)
							throws SQLException {
						DisplayHead head = new DisplayHead();
						head.setId(rs.getInt("id"));
						head.setName(rs.getString("name"));
						head.setDescription(rs.getString("description"));
						head.setActive(rs.getString("active"));
						head.setBranchId(rs.getInt("branch_id"));
						head.setAmount(rs.getDouble("amount"));
						return head;
					}
				});
		return displayHeads;
	}

	@Transactional
	@Override
	public void deleteDisplayTemplate(Integer displayTemplateId) throws ApplicationException{
		String sql = "select count(*) from fees where display_template_id = " + displayTemplateId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			throw new ApplicationException("Cannot delete this template as it is being used!!");
		}

		sql = "update display_template set is_delete = 'Y' where id = "+ displayTemplateId;
		getJdbcTemplate().update(sql);
		deleteDisplayTemplateHeads(displayTemplateId);
	}


	@Override
	public List<DisplayHead> searchDisplayHeads(String searchText,
			String excludeIds, Integer branchId) {
		String sql = "select * from display_head where name like '%"+searchText+"%' AND branch_id = " 
			+ branchId +" AND is_delete = 'N' AND active = 'Y' ";
		
		if(!StringUtils.isEmpty(excludeIds)){
			sql += "AND id not in ("+excludeIds+")";
		}
		List<DisplayHead> displayHeads = getJdbcTemplate().query(sql,
				new RowMapper<DisplayHead>() {
					@Override
					public DisplayHead mapRow(ResultSet rs, int arg1)
							throws SQLException {
						DisplayHead head = new DisplayHead();
						head.setId(rs.getInt("id"));
						head.setName(rs.getString("name"));
						head.setDescription(rs.getString("description"));
						head.setActive(rs.getString("active"));
						head.setBranchId(rs.getInt("branch_id"));
						return head;
					}
				});
		return displayHeads;
	}

	@Override
	public List<DisplayTemplate> searchDisplayTemplates(String searchText,
			String excludeIds, Integer branchId) {
		String sql = "select * from display_template where name like '%"+searchText+"%' AND branch_id = " 
				+ branchId +" AND is_delete = 'N' AND active = 'Y' ";

			if(!StringUtils.isEmpty(excludeIds)){
				sql += "AND id not in ("+excludeIds+")";
			}
			List<DisplayTemplate> displayTemplates = getJdbcTemplate().query(sql,
					new RowMapper<DisplayTemplate>() {
						@Override
						public DisplayTemplate mapRow(ResultSet rs, int arg1)
								throws SQLException {
							DisplayTemplate template = new DisplayTemplate();
							template.setId(rs.getInt("id"));
							template.setName(rs.getString("name"));
							template.setDescription(rs.getString("description"));
							template.setActive(rs.getString("active"));
							template.setBranchId(rs.getInt("branch_id"));
							return template;
						}
					});
			return displayTemplates;
	}

	private final static String TEMPLATE_NAME = "Template Name*:";
	private final static String TEMPLATE_DESCRIPTION = "Template Description:";
	private final static String DISPLAY_HEAD = "Display Head *";
	private final static String AMOUNT = "Amount *";
	private final static String DISPLAY_ORDER = "Display Order *";

	public ByteArrayOutputStream downloadDisplayTemplate(Integer branchId) throws Exception {

		DataValidation dataValidation = null;
		DataValidationConstraint constraint = null;
		DataValidationHelper validationHelper = null;

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = (XSSFSheet) wb.createSheet("sheet1");

		XSSFRow row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue(TEMPLATE_NAME);

		row = sheet.createRow(1);
		cell = row.createCell(0);
		cell.setCellValue(TEMPLATE_DESCRIPTION);

		row = sheet.createRow(3);
		cell = row.createCell(0);
		cell.setCellValue(DISPLAY_HEAD);

		cell = row.createCell(1);
		cell.setCellValue(AMOUNT);

		cell = row.createCell(2);
		cell.setCellValue(DISPLAY_ORDER);

		List<DisplayHead> displayHeadList = getDisplayHeads(branchId);
		ByteArrayOutputStream bo =  new ByteArrayOutputStream();

		if(displayHeadList!= null && displayHeadList.size() > 0){
			String[] displaHeads = getDisplayHeadArr(displayHeadList);
		    validationHelper=new XSSFDataValidationHelper(sheet);
		    CellRangeAddressList addressList = new  CellRangeAddressList(4,110,0,0);
		    constraint =validationHelper.createExplicitListConstraint(displaHeads);
		    dataValidation = validationHelper.createValidation(constraint, addressList);
		    dataValidation.setSuppressDropDownArrow(true);      
		    sheet.addValidationData(dataValidation);
		    wb.write(bo);
		}
		else{
			throw new ApplicationException("Display Heads not Present.");
		}
	    return bo;
	}

	private String[] getDisplayHeadArr(List<DisplayHead> displayHeadList) {
		String[] displayHeads = new String[displayHeadList.size()];
		int i =0;
		for(DisplayHead head : displayHeadList){
			displayHeads[i] = head.getName();
			i++;
		}
		return displayHeads;
	}

	@Override
	public DisplayTemplate parseExcelFile(MultipartFile file, Integer branchId) throws Exception{
		if(file != null){
			InputStream fileInputStream = file.getInputStream(); 
			try {
				Sheet sheet = new ReadWriteExcelUtil().getSheetByReadingExcelFile(
						fileInputStream, file.getOriginalFilename().endsWith(".xls"));
				if(sheet != null){
					Iterator<Row> rowIterator = sheet.iterator();
					DisplayTemplate template = new DisplayTemplate();
					Map<Integer, String> headerIndexMap = new HashMap<Integer, String>();
					List<DisplayHead> heads = new ArrayList<>();

					int displayHeadIndex = -1;

					while (rowIterator.hasNext()) {
						Row nextRow = rowIterator.next();
						if(headerIndexMap.size() < 3){
							readHeadersAndPrepareIndexes(nextRow, template, headerIndexMap);
							continue;
						}
						if(headerIndexMap.size() == 3){
							readDisplayHeads(nextRow, heads, displayHeadIndex, headerIndexMap, branchId);
							displayHeadIndex ++;
						}
					}
					template.setDisplayHeadList(heads);
					return template;
				}
			}finally{
				if(fileInputStream != null){
					try {
						fileInputStream.close();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}


	private void readDisplayHeads(Row row, List<DisplayHead> heads, int displayHeadIndex, Map<Integer, String> headerIndexMap, Integer branchId) {

		Iterator<Cell> cellIterator = row.iterator();
		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			String cellData = ReadWriteExcelUtil.getCellData(cell);
			int columnIndex = cell.getColumnIndex();
			String columnHeader = headerIndexMap.get(columnIndex);
			DisplayHead head = (displayHeadIndex != -1 && heads.size() > displayHeadIndex) ?  heads.get(displayHeadIndex) : null;

			switch(columnHeader){
				case DISPLAY_HEAD :
					if(!StringUtils.isEmpty(cellData)){
						head = getDisplayHeadByName(cellData, branchId);
					}
					head = (head == null) ? new DisplayHead() : head;
					heads.add(head);
					displayHeadIndex ++;
				break;
				case AMOUNT :
					if(head == null){
						head = (head == null) ? new DisplayHead() : head;
						heads.add(head);
						displayHeadIndex ++;
					}
					try {
						double amount = (StringUtils.isEmpty(cellData)) ? 0 : Double.parseDouble(cellData);
						head.setAmount(amount);
					}catch(NumberFormatException e){}

				break;
				case DISPLAY_ORDER : 
					if(head == null){
						head = (head == null) ? new DisplayHead() : head;
						heads.add(head);
						displayHeadIndex ++;
					}
					try {
						Double orderDbl = (StringUtils.isEmpty(cellData)) ? 0 : Double.parseDouble(cellData);
						Integer order = orderDbl != null ? orderDbl.intValue() : null;
						head.setDisplayOrder(order);
					}catch(NumberFormatException e){}
				break;
			}
		}
	}

	private void readHeadersAndPrepareIndexes(Row row, DisplayTemplate template, Map<Integer, String> headerIndexMap) throws Exception {
		Iterator<Cell> cellIterator = row.iterator();
		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			String cellData = ReadWriteExcelUtil.getCellData(cell);
			boolean breakWhile = false;
			if(cellData != null){
				cellData = cellData.trim();
				System.out.println(cellData);
				switch(cellData){
					case TEMPLATE_NAME:
						if(cellIterator.hasNext()){
							Cell nextCell = cellIterator.next();
							String templateName = ReadWriteExcelUtil.getCellData(nextCell);
							template.setName(templateName);
						}
						breakWhile = true;
			            break;
					case TEMPLATE_DESCRIPTION :
						if(cellIterator.hasNext()){
							Cell nextCell = cellIterator.next();
							String templateDesc = ReadWriteExcelUtil.getCellData(nextCell);
							template.setDescription(templateDesc);
						}
						breakWhile = true;
						break;
					case DISPLAY_HEAD :
						headerIndexMap.put(cell.getColumnIndex(), DISPLAY_HEAD);
						break;
					case AMOUNT :
						headerIndexMap.put(cell.getColumnIndex(), AMOUNT);
						break;
					case DISPLAY_ORDER :
						headerIndexMap.put(cell.getColumnIndex(), DISPLAY_ORDER);
						breakWhile = true;
						break;
					default :
						break;
				}

				if(breakWhile){
					break;
				}
			}
		}		
	}

	private DisplayHead getDisplayHeadByName(String headName, Integer branchId) {
		String sql = "select dh.*, institute_id from display_head as dh " +
				" INNER JOIN branch as b on b.id = dh.branch_id " +
				" where dh.is_delete = 'N' AND dh.name = '"+headName+"' and branch_id = "+ branchId;
		return getDisplayHead(sql);
	}
}



