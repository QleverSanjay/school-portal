package com.qfix.daoimpl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.Connection;
import com.qfix.dao.IHolidayDao;
import com.qfix.exceptions.HolidayAllreadyExistException;
import com.qfix.model.Holiday;
import com.qfix.model.PublicHoliday;
import com.qfix.utilities.Constants;

@Repository
public class HolidayDao extends JdbcDaoSupport implements IHolidayDao {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(HolidayDao.class);

	@Transactional
	public List<Holiday> getAll(Integer branchId){
		String sql = null;
		sql = " select h.*, a.from_date as academicFromDate, a.to_date academicToDate"
				+" from"
				+" holidays as h INNER JOIN academic_year as a on h.current_academic_year = a.id" 
				+" AND a.is_current_active_year = 'Y'"
				+" INNER JOIN branch as b on b.id = h.branch_id "
				+" WHERE "
				+" h.branch_id = "+branchId+" order by h.from_date";
		logger.debug("get all holidays SQL --"+sql);

		return getJdbcTemplate().query(sql, new RowMapper<Holiday>() {
			public Holiday mapRow(ResultSet resultSet, int rownumber)throws SQLException {
				Date fromDate = resultSet.getDate("academicFromDate");
				Date toDate = resultSet.getDate("academicToDate");
				String academicYear = Constants.USER_DATE_FORMAT.format(fromDate) + " - " + Constants.USER_DATE_FORMAT.format(toDate);
				Holiday holiday = new Holiday();
				holiday.setId(resultSet.getInt("id"));
				holiday.setAcademicYear(academicYear);
				holiday.setTitle(resultSet.getString("title"));
				holiday.setDescription(resultSet.getString("description"));
				holiday.setFromDate(resultSet.getString("from_date"));
				holiday.setToDate(resultSet.getString("to_date"));
				holiday.setCurrentAcademicYearId(resultSet.getInt("current_academic_year"));
				holiday.setIsPublicHoliday(resultSet.getString("is_public_holiday"));
				holiday.setBranchId(resultSet.getInt("branch_id"));
				return holiday;
		}});
			
}
	
	private boolean holidayAlreadyExist(Holiday holiday){
		String sql = "select count(*) as count from holidays where " +
		     "((from_date <= '"+holiday.getFromDate()+
		     "' AND to_date >= '"+holiday.getFromDate()+"' ) OR (from_date <= '"+holiday.getToDate()+
	     	 "' AND to_date >= '"+holiday.getToDate()+"') OR title = '"+holiday.getTitle()+"')" +
		     " AND current_academic_year = "+holiday.getCurrentAcademicYearId()+ " AND " +
		     "branch_id = "+holiday.getBranchId()+
		     (holiday.getId() != null ? " AND id != "+holiday.getId() : "");
		
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			return true;
		}
		return false;
	}

	@Transactional
	public boolean updateHoliday(Holiday holiday) throws HolidayAllreadyExistException{ 
		boolean flag = false;
			if(!holidayAlreadyExist(holiday)){
				
				String sql = "update holidays set current_academic_year = ?, from_date = ?, " +
						"to_date = ?,title = ?, description = ?, " +
						"branch_id = ? where id = ?";
				
				Object[] params = new Object[] {
						holiday.getCurrentAcademicYearId(),
						holiday.getFromDate(),
						holiday.getToDate(),
						holiday.getTitle(),
						holiday.getDescription(),
						holiday.getBranchId(),
						holiday.getId()
				};
					
				int[] types = new int[] { Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER,Types.INTEGER};
					
				getJdbcTemplate().update(sql, params, types);
				
				flag = true;
				logger.debug("holiday updated..."+holiday.getTitle());
			}
			else {
				logger.error("update holiday failed.."+holiday.getTitle());
				throw new HolidayAllreadyExistException("Holiday for this duration is already exist in this Branch.");
			}

		return flag;
	}

	@Transactional
	public boolean insertHoliday(Holiday holiday){
		boolean flag= false;
		String sql = "insert into holidays (current_academic_year, from_date, to_date, title, " +
				"description, branch_id) values (?, ?, ?, ?, ?, ?)";
		
			Object[] params = new Object[] {
					holiday.getCurrentAcademicYearId(),
					holiday.getFromDate(),
					holiday.getToDate(),
					holiday.getTitle(),
					holiday.getDescription(),
					holiday.getBranchId()
			};

			int[] types = new int[] { Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER};
				
			getJdbcTemplate().update(sql, params, types);
			

			flag = true;
			logger.debug("holiday inserted..."+holiday.getTitle());
		return flag;
	}
	
	/*@Override
	public void insertHoliday(Holiday holiday, List<Integer> holidays)
			throws HolidayAllreadyExistException {
		boolean flag= false;
		String sql = "insert into holidays (current_academic_year, from_date, to_date, title, " +
				"description, branch_id) values (?, ?, ?, ?, ?, ?)";
		
		Object[] params = new Object[] {
				holiday.getCurrentAcademicYearId(),
				holiday.getFromDate(),
				holiday.getToDate(),
				holiday.getTitle(),
				holiday.getDescription(),
				holiday.getBranchId()
		};
		
		int[] types = new int[] { Types.INTEGER,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.INTEGER};*/

       /* KeyHolder holder = new GeneratedKeyHolder();
        getJdbcTemplate().update(new Holiday() {
        @Override
        public Holiday createHoliday(Connection connection)
        throws SQLException {
        	Holiday h = connection.holiday(sql.toString(), Holiday.RETURN_GENERATED_KEYS);
        	Object[] params = new Object[] {
    				holiday.getCurrentAcademicYearId(),
    				holiday.getFromDate(),
    				holiday.getToDate(),
    				holiday.getTitle(),
    				holiday.getDescription(),
    				holiday.getBranchId()
    				return holiday;
    		}
        },holder);

Long newHolidayId = holder.getKey().longValue();
        	
		
		
		*/
		
		
		
//}

	//TODO use insertHoliday() method instead of this. Once TODOs expected in com.qfix.model.Holiday.java 
	/*@Transactional
	public boolean insertHolidayUpload(Holiday holiday){
		boolean flag= false;
		String sql = "insert into holidays (current_academic_year, from_date, to_date, title, " +
				"description, branch_id) values (?, ?, ?, ?, ?, ?)";
		
			Object[] params = new Object[] {
					holiday.getCurrentAcademicYearId(),
					holiday.getFromDateD(),
					holiday.getToDateD(),
					holiday.getTitle(),
					holiday.getDescription(),
					holiday.getBranchId()
			};

			int[] types = new int[] { Types.INTEGER,Types.DATE,Types.DATE,Types.VARCHAR,Types.VARCHAR,Types.INTEGER};
				
			getJdbcTemplate().update(sql, params, types);

			flag = true;
			logger.debug("holiday inserted..."+holiday.getTitle());
		return flag;
	}
*/
	@Transactional
	public Holiday getHolidayById(Integer id){
		String sql = "select h.*, i.id as instituteId from holidays as h, branch as b, institute as i " +
				"where h.branch_id = b.id AND b.institute_id = i.id AND h.id = "+id;

		List<Holiday> holidayList =  getJdbcTemplate().query(sql, new RowMapper<Holiday>() {
			public Holiday mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				Holiday holiday = new Holiday();
				holiday.setId(resultSet.getInt("id"));
				holiday.setCurrentAcademicYearId(resultSet.getInt("current_academic_year"));
				holiday.setFromDate(resultSet.getString("from_date"));
				holiday.setToDate(resultSet.getString("to_date"));
				holiday.setTitle(resultSet.getString("title"));
				holiday.setDescription(resultSet.getString("description"));
				holiday.setBranchId(resultSet.getInt("branch_id"));
				holiday.setInstituteId(resultSet.getInt("instituteId"));

				return holiday;
			}
		});
		return holidayList.get(0);
	}

	public boolean uploadHoliday(List<Holiday> holidayList){
		for(Holiday holiday : holidayList){
			//insertHolidayUpload(holiday);
			insertHoliday(holiday);
		}
		return true;
	}
	
	@Transactional
	public boolean deleteHoliday(Integer id) {
		// delete from Holiday ..
		String sql = "delete from holidays where id = "+id;
		getJdbcTemplate().update(sql);
		logger.debug("holiday deleted ..."+id);
		return true;
	}

	@Override
	public void deleteHolidays(Integer branchId) {
		// delete from Holiday ..
		String sql = "delete from holidays where branch_id = "+branchId;
		getJdbcTemplate().update(sql);
		logger.debug("holidays deleted for branch ..."+branchId);
	}

	@Override
	public List<PublicHoliday> getPublicHoliday() {
		String sql = "select id, title, description, is_public_holiday as isPublicHoliday, DATE_FORMAT(to_date, '%d-%m-%Y') as toDate, DATE_FORMAT(from_date, '%d-%m-%Y')  as fromDate from public_holiday";
		List<PublicHoliday> list = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<PublicHoliday>(PublicHoliday.class));
		return list;
	}
	
	
}
