package com.qfix.daoimpl;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.qfix.exceptions.ShoppingAccountNotCreatedException;
import com.qfix.model.Report;
import com.qfix.model.Student;
import com.qfix.model.Teacher;
import com.qfix.service.user.client.shopping.RestClient;
import com.qfix.service.user.client.shopping.ServiceConfiguration;
import com.qfix.service.user.client.shopping.UrlConfiguration;
import com.qfix.service.user.client.shopping.response.ShoppingMessageResponse;
import com.qfix.service.user.client.shopping.response.customer.CustomerResponse;
import com.qfix.service.user.client.shopping.response.session.ShoppingSessionResponse;

@Repository
public class ShoppingDao extends NamedParameterJdbcDaoSupport   {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(ShoppingDao.class);
	
	public ShoppingSessionResponse getShoppingSessionDetails(String shoppingAccountUsername, String shoppingAccountPassword) {
		ShoppingSessionResponse sessionResponse = null;
		try {
			MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<String, String>();
			
			valueMap.add("username", shoppingAccountUsername);
			valueMap.add("password", shoppingAccountPassword);
			
			RestClient restClient = new RestClient();
			/*ResponseEntity<String> configurationResponse = restClient
					.executeRequest(sessionRequest,
							UrlConfiguration.GET_SERVER_CONFIG);

			if (configurationResponse.getStatusCode() == HttpStatus.OK) {
				
			} else {
				
			}*/

			ResponseEntity<String> createSessionResponse = restClient.executeRequest(valueMap,
					UrlConfiguration.CREATE_SESSION_CONFIG, false);

			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				
				
				/* XmlMapper xmlMapper = new XmlMapper();
				 sessionResponse = xmlMapper.readValue(createSessionResponse.getBody(),SessionResponse.class);
				 */  
				String cookieString = createSessionResponse.getHeaders().toString();
				
				sessionResponse = new ShoppingSessionResponse();
				sessionResponse.setCookies(RestClient.getFrontendCookieText(cookieString));
				sessionResponse.setCookieExpiresOn(RestClient.getCookieExpireTime(cookieString));
				
				/*sessionResponse = mapper.readValue(createSessionResponse.getBody(),
						SessionResponse.class);*/

			} 
			else {
			 			throw new Exception(createSessionResponse.getBody());

			}
		} catch(Exception e) {
			//throw new ApplicationException("Error in getting chat session details from quickblox.", e);
			logger.error("",e);
		}
		return sessionResponse;
	}
	
	public Report getVendorReports(Integer branchId, String fromDate, String toDate) {
		Report vendorReports = null;
		try {
			
			MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<String, String>();
			
			valueMap.add("from_date", fromDate.toString());
			valueMap.add("to_date", toDate.toString());

			RestClient restClient = new RestClient();

			ServiceConfiguration serviceConfiguration = new ServiceConfiguration(
					UrlConfiguration.GET_VENDOR_REPORTS_CONFIG_URL + "12",HttpMethod.POST);

			ResponseEntity<String> createSessionResponse = restClient.executeRequest(valueMap, serviceConfiguration, false);

			logger.debug("Vendor Reports RESPONSE --"+createSessionResponse.getBody());

			JAXBContext jaxbContext = JAXBContext.newInstance(Report.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StreamSource streamSource = new StreamSource(new StringReader(createSessionResponse.getBody()));

			JAXBElement<Report> je = unmarshaller.unmarshal(streamSource, Report.class);
			vendorReports = (Report)je.getValue();
			logger.debug("Parsed XML RESPONSE --"+vendorReports);
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("",e);
		}
		return vendorReports;
	}

	public boolean attachVendorAndBranch(Integer vendorProfileId, Integer branchId) {
		ShoppingMessageResponse vendorResponse = null;
		try {

			MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<String, String>();
			List<Integer> vendorList = new ArrayList<>();
			vendorList.add(vendorProfileId);

			valueMap.add("vendor_id[]", vendorProfileId+"");
			valueMap.add("branch_id", branchId + "");

			RestClient restClient = new RestClient();

			ResponseEntity<String> createSessionResponse = restClient.executeRequest(valueMap, UrlConfiguration.ATTCH_VENDOR_CONFIG_URL, true);

			logger.debug("Vendor Reports RESPONSE --"+createSessionResponse.getBody());
			JAXBContext jaxbContext = JAXBContext.newInstance(ShoppingMessageResponse.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StreamSource streamSource = new StreamSource(new StringReader(createSessionResponse.getBody()));

			JAXBElement<ShoppingMessageResponse> je = unmarshaller.unmarshal(streamSource, ShoppingMessageResponse.class);
			vendorResponse = (ShoppingMessageResponse)je.getValue();
			logger.debug("Parsed XML RESPONSE --"+vendorResponse);
			return vendorResponse.getStatus().equalsIgnoreCase("success");
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("",e);
		}
		return false;
	}


	public boolean deAttachVendorAndBranch(Integer vendorProfileId, Integer branchId) {
		ShoppingMessageResponse vendorResponse = null;
		try {

			MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<String, String>();
			List<Integer> vendorList = new ArrayList<>();
			vendorList.add(vendorProfileId);

			valueMap.add("vendor_id[]", vendorProfileId+"");
			valueMap.add("branch_id", branchId + "");

			RestClient restClient = new RestClient();

			ResponseEntity<String> createSessionResponse = restClient.executeRequest(valueMap, UrlConfiguration.DE_ATTCH_VENDOR_CONFIG_URL, true);

			logger.debug("Vendor Reports RESPONSE --"+createSessionResponse.getBody());
			JAXBContext jaxbContext = JAXBContext.newInstance(ShoppingMessageResponse.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StreamSource streamSource = new StreamSource(new StringReader(createSessionResponse.getBody()));

			JAXBElement<ShoppingMessageResponse> je = unmarshaller.unmarshal(streamSource, ShoppingMessageResponse.class);
			vendorResponse = (ShoppingMessageResponse)je.getValue();
			logger.debug("Parsed XML RESPONSE --"+vendorResponse);
			return vendorResponse.getStatus().equalsIgnoreCase("success");
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("",e);
		}
		return false;
	}
	
	/**
	 * Creates new shopping profile and returns shopping profile customer id.
	 * @param user object to create shopping profile.
	 * @return shopping profile customer id 
	 * @throws ShoppingAccountNotCreatedException 
	 */
	public int createShoppingAccount(Object user) throws ShoppingAccountNotCreatedException {
		try {

			MultiValueMap<String, String> valueMap = null;
			if(user instanceof Student){
				valueMap = getvalueMapForStudent((Student)user);
			}
			else if(user instanceof Teacher){
				valueMap = getvalueMapForTeacher((Teacher)user);	
			}
			System.out.println("Sending Data for Shopping account creation ----"+valueMap);
			RestClient restClient = new RestClient();
			ResponseEntity<String> createSessionResponse = restClient.executeRequest(valueMap,
						UrlConfiguration.CREATE_CUSTOMER_CONFIG, true);

			logger.debug("Shopping account created for --"+createSessionResponse.getBody());

			JAXBContext jaxbContext = JAXBContext.newInstance(CustomerResponse.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StreamSource streamSource = new StreamSource(new StringReader(createSessionResponse.getBody()));

			JAXBElement<CustomerResponse> je = unmarshaller.unmarshal(streamSource, CustomerResponse.class);

			CustomerResponse customerResponse = (CustomerResponse)je.getValue();
			System.out.println("customerResponse AFTER PARSED --- "+customerResponse);
			if(customerResponse.getStatus().equalsIgnoreCase("success")){
				return customerResponse.getCustomerId();	
			}
			else {
				throw new ShoppingAccountNotCreatedException(customerResponse.getText());
			}
		} catch(Exception e) {
			if(e instanceof ShoppingAccountNotCreatedException){
				throw (ShoppingAccountNotCreatedException) e;
			}
			logger.error("",e);
		}
		return 0;
	}

	private MultiValueMap<String, String> getvalueMapForStudent(Student student) {
		MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
		valueMap.add("firstname", RandomStringUtils.random(10, true, false));
		valueMap.add("lastname", RandomStringUtils.random(10, true, false));
		valueMap.add("email", student.getUsername());
		valueMap.add("password", student.getPassword());
		valueMap.add("confirmation", student.getPassword());
		valueMap.add("is_subscribed", "1");
		valueMap.add("birthdate", student.getDateOfBirth());
		valueMap.add("mobile", student.getMobile());
		valueMap.add("branch_id", ""+student.getBranchId());
		return valueMap;
	}


	private MultiValueMap<String, String> getvalueMapForTeacher(Teacher teacher) {
		MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
		valueMap.add("firstname", RandomStringUtils.random(10, true, false));
		valueMap.add("lastname", RandomStringUtils.random(10, true, false));
		valueMap.add("email", teacher.getEmailAddress());
		valueMap.add("password", teacher.getPassword());
		valueMap.add("confirmation", teacher.getPassword());
		valueMap.add("is_subscribed", "1");
		valueMap.add("birthdate", teacher.getDateOfBirth());
		valueMap.add("mobile", teacher.getPrimaryContact());
		valueMap.add("branch_id", ""+teacher.getBranchId());
		return valueMap;
	}
	
	

}
