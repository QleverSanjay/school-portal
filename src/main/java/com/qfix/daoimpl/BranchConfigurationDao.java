package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.qfix.dao.IBranchConfigurationDao;
import com.qfix.model.BranchConfig;
import com.qfix.model.Receipt;

@Repository
public class BranchConfigurationDao extends JdbcDaoSupport implements
		IBranchConfigurationDao {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(BranchConfigurationDao.class);
	String flag;

	@Override
	public String saveReceiptConfiguration(Receipt receipt)throws Exception {
		String url = "";
		
		
			if (receipt.isUseSchoolLogAsWatermarkImage()) {
				String sql = "select logo_url from institute where id =(select institute_id from branch where id= "
						+ receipt.getBranchId()+")";
				url = getJdbcTemplate().queryForObject(sql, String.class);
				System.out.println(url);

			}
			String watermarkImage = receipt.isUseSchoolLogAsWatermarkImage() ? url
					.contains("no-images.png") ? null : url : null;
			String sql = "select count(*) as count from  payment_receipt_configuration where branch_id = "
					+ receipt.getBranchId();

			Integer count = getJdbcTemplate()
					.queryForObject(sql, Integer.class);

			if (count > 0) {

				sql = "update payment_receipt_configuration set hide_qfix_logo=?,watermark_image_url=?,trailer_record=? where branch_id=?";
				System.out.println("count*********" + count);
				Object[] args = { receipt.isHideLogo() ? "Y" : "N",
						watermarkImage, receipt.getTrailerRecord(),
						receipt.getBranchId() };

				int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.INTEGER };
				getJdbcTemplate().update(sql, args, argTypes);
				flag="update";
			} else {
				sql = "insert into  payment_receipt_configuration(branch_id,hide_qfix_logo, watermark_image_url, trailer_record )values(?, ?, ?,?)";

				Object[] args = { receipt.getBranchId(),
						receipt.isHideLogo() ? "Y" : "N", watermarkImage,
						receipt.getTrailerRecord() };

				int[] argTypes = new int[] { Types.INTEGER, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR };
				getJdbcTemplate().update(sql, args, argTypes);
				flag = "insert";
			}
			logger.debug("update" + sql);
			return flag;

		
	}

	@Override
	public Receipt getReceiptConfiguration(Integer branchId) {
		logger.debug("inside getReceiptConfiguration >>>>> ");
		String sql = "select * from payment_receipt_configuration where branch_id = "
				+ branchId;

		List<Receipt> receiptList = getJdbcTemplate().query(sql,
				new RowMapper<Receipt>() {
					@Override
					public Receipt mapRow(ResultSet rs, int arg1)
							throws SQLException {
						Receipt receipt = new Receipt();
						receipt.setBranchId(rs.getInt("branch_id"));
						receipt.setHideLogo(("Y".equals(rs
								.getString("hide_qfix_logo"))));
						String watermarkImage = rs
								.getString("watermark_image_url");
						receipt.setUseSchoolLogAsWatermarkImage(watermarkImage == null ? false
								: true);
						receipt.setTrailerRecord(rs.getString("trailer_record"));
						return receipt;
					}
				});

		System.out.println("******************************" + receiptList);
		// TRY NOW....
		if (receiptList.size() > 0) {
			return receiptList.get(0);
		}
		return null;
	}

	@Override
	public void saveStudentBranchConfig(final BranchConfig branchConfig) throws SQLException {
		String sql = "update branch_configuration set student_data_available = ?, unique_identifier_lable = ?, unique_identifier_name = ? where branch_id = ?";
		
		Object[] args = {
				branchConfig.getStudentDataAvailable(),
				branchConfig.getUniqueIdentifierLable(),
				branchConfig.getUniqueIdentifierName(),
				branchConfig.getBranchId()
		};
		
		int[] types = new int[]{
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER
		};
		getJdbcTemplate().update(sql, args, types);
	}
}
