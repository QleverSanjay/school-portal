package com.qfix.daoimpl;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.dao.IIndividualDao;
import com.qfix.dao.INoticeDao;
import com.qfix.model.Individual;
import com.qfix.model.Master;
import com.qfix.model.Notice;
import com.qfix.service.client.notification.Notification;
import com.qfix.service.client.notification.Sender;
import com.qfix.service.file.FileFolderService;
import com.qfix.service.file.FileFolderServiceImpl;
import com.qfix.utilities.Constants;
import com.qfix.utilities.FileConstants;
import com.qfix.utilities.FtlTemplateConstants;

@Repository
public class NoticeDao extends JdbcDaoSupport implements INoticeDao{
	

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	

	final static Logger logger = Logger.getLogger(NoticeDao.class);

	@Autowired
	IIndividualDao individualDao;
	
	@Autowired
	Sender sender;
	
	@Autowired
	private FileFolderService fileFolderService;

	@Transactional
	public List<Notice> getAll(Integer branchId, boolean isTeacher, Integer teacherUserId){
		List<Notice> noticeList = new ArrayList<Notice>();

		String sql = null;
		if (isTeacher) {
			sql = "select concat(ifnull(firstname, ''), ' ',ifnull(lastname, '')) as name, n.* " +
				" from notice as n " +
				" INNER JOIN contact on contact.user_id = sent_by " +
				" where sent_by = " + teacherUserId + " AND " +
				" academic_year_id = (select id from academic_year where " +
				" is_current_active_year = 'Y' and branch_id = "+branchId+") order by id desc";
		} else {
			sql = "select concat(ifnull(firstname, ''), ' ',ifnull(lastname, '')) as name, n.* " +
				" from notice as n " +
				" INNER JOIN contact on contact.user_id = sent_by " +
				" where branch_id = " + branchId + " AND " +
				" academic_year_id = (select id from academic_year where " +
				" is_current_active_year = 'Y' and branch_id = "+branchId+")and n.category !='assignment' order by id desc";
		}

		logger.debug("get all notice SQL --"+sql);
		noticeList = getJdbcTemplate().query(sql , new RowMapper<Notice>(){
			@Override
			public Notice mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Notice notice = new Notice();
				notice.setId(resultSet.getInt("id"));
				notice.setTitle(resultSet.getString("title"));
				notice.setCategory(resultSet.getString("category"));
				notice.setDescription(resultSet.getString("description"));
				notice.setPriority(resultSet.getString("priority"));
				notice.setDate(resultSet.getDate("date"));
				notice.setBranchId(resultSet.getInt("branch_id"));
				notice.setSentBy(resultSet.getInt("sent_by"));
				notice.setSentByName(resultSet.getString("name"));
				return notice;
			}
		});
		return noticeList;
	}
	
	@Transactional
	public List<Notice> getAllAssignment(Integer branchId, boolean isTeacher, Integer teacherUserId){
		List<Notice> assignmentList = new ArrayList<Notice>();

		String sql = null;
		if (isTeacher) {
			sql = "select concat(ifnull(firstname, ''), ' ',ifnull(lastname, '')) as name, n.* " +
				" from notice as n " +
				" INNER JOIN contact on contact.user_id = sent_by " +
				" where sent_by = " + teacherUserId + " AND " +
				" academic_year_id = (select id from academic_year where " +
				" is_current_active_year = 'Y' and branch_id = "+branchId+") order by id desc";
		} else {
			sql = "select concat(ifnull(firstname, ''), ' ',ifnull(lastname, '')) as name, n.* " +
				" from notice as n " +
				" INNER JOIN contact on contact.user_id = sent_by " +
				" where branch_id = " + branchId + " AND " +
				" academic_year_id = (select id from academic_year where " +
				" is_current_active_year = 'Y' and branch_id = "+branchId+") and n.category='assignment'order by id desc";
		}

		logger.debug("get all notice SQL --"+sql);
		assignmentList = getJdbcTemplate().query(sql , new RowMapper<Notice>(){
			@Override
			public Notice mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Notice notice = new Notice();
				notice.setId(resultSet.getInt("id"));
				notice.setTitle(resultSet.getString("title"));
				notice.setCategory(resultSet.getString("category"));
				notice.setDescription(resultSet.getString("description"));
				notice.setPriority(resultSet.getString("priority"));
				notice.setDate(resultSet.getDate("date"));
				notice.setBranchId(resultSet.getInt("branch_id"));
				notice.setSentBy(resultSet.getInt("sent_by"));
				notice.setSentByName(resultSet.getString("name"));
				return notice;
			}
		});
		return assignmentList;
	}

	@Transactional
	public List<Notice> getTeacherNotice(Integer teacherId,Integer branchId){
		String sql = "select concat(ifnull(firstname, ''), ' ',ifnull(lastname, '')) as name, n.* " +
				" from notice as n " +
				" INNER JOIN contact on contact.user_id = sent_by " +
				" where n.id in (select notice_id from notice_individual where for_user_id = "
				 + teacherId + ") AND academic_year_id = (select id from academic_year where "
				 +"is_current_active_year = 'Y' and branch_id = "+branchId+") order by n.date desc";

		logger.debug("get teacher notice SQL --"+sql);

		List<Notice> noticeList = getJdbcTemplate().query(sql, new RowMapper<Notice>(){
			@Override
			public Notice mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Notice notice = new Notice();
				notice.setId(resultSet.getInt("id"));
				notice.setTitle(resultSet.getString("title"));
				notice.setCategory(resultSet.getString("category"));
				notice.setDescription(resultSet.getString("description"));
				notice.setPriority(resultSet.getString("priority"));
				notice.setDate(resultSet.getDate("date"));
				notice.setBranchId(resultSet.getInt("branch_id"));
				notice.setSentBy(resultSet.getInt("sent_by"));
				notice.setSentByName(resultSet.getString("name"));
				return notice;
			}
		});
		return noticeList;
	}

	public boolean insertNotice(final Notice notice, MultipartFile file){
		logger.debug("insert notice started --"+notice.getTitle());
		insertNoticeInDb(notice, file);
		// send notice by email and sms to users
		prepareEmailAndSMS(notice);
		return true;
	}

	@Transactional
	public boolean insertNoticeInDb(final Notice notice, MultipartFile file){
		logger.debug("insert notice started --"+notice.getTitle());
		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into notice (title, description, priority, category, branch_id, date, image_url," +
						 "sent_by, academic_year_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, notice.getTitle());
				preparedStatement.setString(2, notice.getDescription());
				preparedStatement.setString(3, notice.getPriority());
				preparedStatement.setString(4, notice.getCategory());
				preparedStatement.setInt(5, notice.getBranchId());
				preparedStatement.setDate(6, new java.sql.Date(new Date().getTime()));
				preparedStatement.setString(7, notice.getImageUrl());
				preparedStatement.setInt(8, notice.getSentBy());
				preparedStatement.setInt(9, notice.getAcademicYearId());
				return preparedStatement;
			}
		}, holder);

		Integer noticeId = holder.getKey().intValue();
		notice.setId(noticeId);
		// insert group notice.
		insertGroupUserNotice(notice);

		// insert individual notice.
		insertIntoNoticeIndividual(new HashSet<Integer>(notice.getIndividualIdList()), noticeId);

		// save uploaded file and update image path in db.
		updatePhotoPath(notice, file);

		// prepare data for push message and send push message
		// prepareAndSendPushMessage(notice);
		logger.debug("insert notice done..."+notice.getTitle());
		
		return true;
	}


	@Transactional
	private void prepareEmailAndSMS(final Notice notice) {

		/*String sql = "select distinct ur.user_id, ur.role_id, r.name as role, " +
				" b.name as branchName, i.name as instituteName " +
				" from user_role as ur " +
				" INNER JOIN role as r on ur.role_id = r.id "+
				" INNER JOIN notice_individual as ni on ur.user_id = ni.user_id " +
				" INNER JOIN notice as n on ni.notice_id = n.id and n.id = "+notice.getId() + 
				" INNER JOIN branch as b on b.id = n.branch_id " +
				" INNER JOIN institute as i on i.id = b.institute_id" +
				" WHERE ur.role_id != "+Constants.ROLE_ID_PARENT;

		System.out.println("SQL::: "+sql);
		final Map<String, Integer> userIds = new HashMap<>();
		final Map<String, Object> content = new HashMap<String, Object>();

		getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Integer userId = resultSet.getInt("user_id");
				Integer roleId = resultSet.getInt("role_id");

				userIds.put(resultSet.getString("role").toUpperCase()+"-"+userId, userId);

				if(content.size() == 0){
					content.put(TemplateParameterConstants.INSTITUTE_NAME, resultSet.getString("instituteName"));
					content.put(TemplateParameterConstants.BRANCH_NAME, resultSet.getString("branchName"));
				}

				if(roleId.equals(Constants.ROLE_ID_STUDENT)){
					String sql = "select user_id  from parent where id = (select parent_id from student where user_id = "+userId+")";

					getJdbcTemplate().query(sql, new RowMapper<String>(){
						@Override
						public String mapRow(ResultSet rs, int arg1) throws SQLException {
							userIds.put(Constants.ROLE_PARENT+"-"+rs.getInt("user_id"), rs.getInt("user_id"));
							return null;
						}
					});
				}
				return null;
			}
		});

		// Set push message details..

		content.put(TemplateParameterConstants.NOTICE_TYPE, notice.getCategory());
		content.put("push-title", notice.getCategory());
		content.put("push-message", notice.getTitle());
		String additionalData = "{" 
				+ "\"type\":\""+(notice.getCategory().equalsIgnoreCase("Assignment") 
				? notice.getCategory().toUpperCase() : "NOTICE")+"\","
				+ "\"id\":" + notice.getId() + "}";
		content.put("push-detailJson", additionalData);*/

		// Create notification object to send to notification-service.
		Notification notification = new Notification();
//		notification.setContent(content);
//		notification.setUserIds(userIds);
		notification.setType("NOTICE");
		notification.sendEmail(true);
		notification.sendSMS(true);
		notification.setId(notice.getId());
		notification.sendPushMessage(true);
		notification.setTemplateCode(FtlTemplateConstants.CREATE_NOTICE_CODE);
		notification.setBatchEnabled(false);

		try {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println("NOTIFICATION_OBJ:::::\n"+mapper.writeValueAsString(notification));		
			sender.sendMessage(mapper.writeValueAsString(notification));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void setParentDataBasedOnChild(final List<String> emailList,
			final Set<Map<String, String>> emailContentSet, final Set<String> mobileSet, Integer userId) {

		String sql = "select p.email as emailAddress, p.primaryContact as contactNumber, p.firstname as name, " +
				" b.name as branchName, i.name as instituteName " +
				" from parent as p " +
				" INNER JOIN student as s on p.id = s.parent_id " +
				" INNER JOIN branch as b on b.id = s.branch_id " +
				" INNER JOIN institute as i on i.id = b.institute_id " +
				" WHERE s.user_id = "+userId;
		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				String mobileNumber = rs.getString("contactNumber");
				String email = rs.getString("emailAddress");

				if(!emailList.contains(email)){
					emailList.add(email);
					Map<String, String> emailContentMap = new HashMap<>();
					emailContentMap.put("firstName", rs.getString("name"));
					emailContentMap.put("mobileNumber", mobileNumber);
					emailContentMap.put("email", email);
					emailContentMap.put("instituteName", rs.getString("instituteName"));
					emailContentMap.put("branchName", rs.getString("branchName"));
					emailContentMap.put("roleId", Constants.ROLE_ID_PARENT + "");
					emailContentSet.add(emailContentMap);
				}
				mobileSet.add(mobileNumber);
				return null;
			}
		});
	}


/*	@Transactional
	private void insertIntoIndividualUserNotice(Notice notice) {
		if (notice.getIndividualIdList() != null
				&& notice.getIndividualIdList().size() > 0) {

			for (Integer individualId : notice.getIndividualIdList()) {
				insertGroupUserNotice(individualId, notice.getId());
			}
		}
	}*/


	@Transactional
	private List<Integer> getParentIdsByStudentIds( List<Integer> individualIdList) {
		final List<Integer> parentIds = new ArrayList<Integer>();
		for (Integer studentId : individualIdList) {
			String sql = "select user_id from parent where id = ("
					+ "select parent_id from student where user_id = "+ studentId + ")";

			getJdbcTemplate().query(sql , new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					parentIds.add(resultSet.getInt("user_id"));
					return null;
				}
			});
		}
		return parentIds;
	}

	@Transactional
	private List<String> getUserRegistrationIdsByUserIds(Set<Integer> userIdSet) {
		List<String> gcmRegistrationIdList = new ArrayList<String>();
		if (userIdSet.size() > 0) {
			for (Integer userId : userIdSet) {
				String sql = "select gcm_id from user " +
						"where active = 'Y' AND gcm_id is not null AND id = "+userId;

				List<String> gcmIds = getJdbcTemplate().query(sql , new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
						return resultSet.getString("gcm_id");
					}
				});
				if(gcmIds .size() > 0){
					String gcmRegId = gcmIds.get(0);
					gcmRegistrationIdList.add(gcmRegId);
				}
			}
		}

		return gcmRegistrationIdList;
	}

	@Transactional
	private List<Integer> getUserIdsByGroups( List<Integer> groupIdList) {
		final List<Integer> userIdList = new ArrayList<Integer>();

		if (groupIdList.size() > 0) {
			for (Integer groupId : groupIdList) {
				String userSql = "select * from user_role where "
						+ "user_id in (select user_id from group_user where group_id = "+groupId+")";

				getJdbcTemplate().query(userSql, new RowMapper<Integer>(){
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
						/*if (resultSet.getInt("role_id") == Constants.ROLE_ID_TEACHER) {
							 if role is teacher then get user id 
							
						} else */
						userIdList.add(resultSet.getInt("user_id"));
						if (resultSet.getInt("role_id") == Constants.ROLE_ID_STUDENT) {
							/* If role is student then get userId of his parent */
							
							String parentSql = "select user_id from parent where id = ("
									+ "select parent_id from student where user_id = "+resultSet.getInt("user_id")+")";

							List<Integer> parentUserIdList = getJdbcTemplate().query(parentSql, new RowMapper<Integer>(){
								@Override
								public Integer mapRow(ResultSet resultSet1, int arg1) throws SQLException {
									return resultSet1.getInt("user_id");
								}
							});
							if(parentUserIdList != null && parentUserIdList.size() > 0){
								userIdList.add(parentUserIdList.get(0));
							}
						}
						return null;
					}
				});
			}
		}
		return userIdList;
	}

	@Transactional
	public boolean deleteNotice(Integer id) {
		String sql = "delete from notice_group where notice_id = " + id;
		getJdbcTemplate().update(sql);
		
		sql = "delete from notice_individual where notice_id = " + id;
		getJdbcTemplate().update(sql);

		sql = "delete from notice_individual_read_status where notice_id = " + id;
		getJdbcTemplate().update(sql);

		sql = "delete from notice where id = " + id;
		getJdbcTemplate().update(sql);
		
		logger.debug("Notice deleted.."+id);
		return true;
		
		
	}

	@Transactional
	private void insertGroupUserNotice(Integer userId, Integer noticeId, Integer forUserId){
		if(!isUserIdexist(noticeId, userId, forUserId)){
			String sql = "insert into notice_individual (notice_id, user_id, for_user_id) " +
					" values("+noticeId+", "+userId+", "+forUserId+")";

			getJdbcTemplate().update(sql);
		}
	}


	@Transactional
	private void insertIntoNoticeIndividual(Set<Integer> userIdSet, final Integer noticeId){
		if(userIdSet != null && userIdSet.size() > 0){
			String userIds = org.apache.commons.lang.StringUtils.join(userIdSet, ",");
			String sql = "select ur.*, p.user_id as parentUserId, s.user_id as studentUserId " +
					" from user_role as ur " +
					" LEFT JOIN student as s on s.user_id = ur.user_id " +
					" LEFT JOIN branch_student_parent as bsp on bsp.student_id = s.id AND bsp.isDelete = 'N'" +
					" LEFT JOIN parent as p on p.id = bsp.parent_id " +
					" WHERE ur.user_id in ("+userIds+") AND ur.role_id != "+Constants.ROLE_ID_PARENT;

			logger.debug("insertIntoNoticeIndividual SQL :::: \n"+sql);
			final Set<Integer> insertedStudents =  new HashSet<>();

			getJdbcTemplate().query(sql, new RowMapper<Object> (){
				@Override
				public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
					Integer userId = resultSet.getInt("user_id");
					Integer roleId = resultSet.getInt("role_id");

					if(roleId == Constants.ROLE_ID_STUDENT){
						if(!insertedStudents.contains(userId)){
							insertGroupUserNotice(userId, noticeId, userId);
							insertedStudents.add(userId);
						}

						if(resultSet.getInt("parentUserId") != 0){
							insertGroupUserNotice(resultSet.getInt("parentUserId"), noticeId, resultSet.getInt("studentuserId"));	
						}
					}
					else {
						insertGroupUserNotice(userId, noticeId, userId);
					}
					return null;
				}
			});
		}
	}

	@Transactional
	private void insertGroupUserNotice(Notice notice){		

		if (notice.getGroupIdList() != null && notice.getGroupIdList().size() > 0) {
			Set<Integer> userIdSet = new HashSet<>();

			for (Integer groupId : notice.getGroupIdList()) {
				String sql = "insert into notice_group (notice_id, group_id) values ("+ notice.getId()+", "+groupId+")";
				getJdbcTemplate().update(sql);
			}
			String groupIds = org.apache.commons.lang.StringUtils.join(notice.getGroupIdList(), ", ");
			String sql = ("select ur.user_id from user_role as ur " +
					" INNER JOIN `group_user` as gu on gu.user_id = ur.user_id " +
					" WHERE gu.group_id in ("+groupIds+") AND ur.role_id != "+Constants.ROLE_ID_PARENT);

			List<Integer> userIdList = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1)throws SQLException {
					Integer userId = resultSet.getInt("user_id");
					return userId;
				}
			});

			if(userIdList != null && userIdList.size() > 0){
				userIdSet.addAll(userIdList);
				insertIntoNoticeIndividual(userIdSet, notice.getId());
			}
		}
	}

	@Transactional
	private boolean isUserIdexist(Integer noticeId,Integer userId, Integer forUserId) {
		String sql = "select count(*) as count from notice_individual where notice_id = "
				+ noticeId + " AND user_id = " + userId +" AND for_user_id = "+forUserId;
		int count = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		return count > 0;
	}

	@Transactional
	public Notice getNoticeById(Integer id, final Integer teacherUserId) {
		Notice notice = null;
		String sql = "select n.*, i.id as instituteId, i.name as instituteName, b.name as branchName " +
				" from notice as n " +
				" INNER JOIN  branch as b on b.id = n.branch_id " +
				" INNER JOIN institute as i on b.institute_id = i.id " +
				" where n.id = " + id;

		if(teacherUserId != null){
			sql = "select n.*, (if(nirs.user_id is null,'N', 'Y')) as readStatus, i.id as instituteId, " +
					" i.name as instituteName, b.name as branchName " +
					" from notice as n " +
					" INNER JOIN  branch as b on b.id = n.branch_id " +
					" INNER JOIN institute as i on b.institute_id = i.id " +
					" INNER JOIN teacher as t on t.user_id = "+teacherUserId +
					" LEFT JOIN notice_individual_read_status as nirs on " +
					" nirs.notice_id = n.id AND nirs.user_id = t.user_id " +
					" AND nirs.for_user_id = t.user_id" +
					" where n.id = " + id;
		}

		List<Notice> notices = getJdbcTemplate().query(sql, new RowMapper<Notice>(){
			@Override
			public Notice mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Notice notice = new Notice();
				notice.setId(resultSet.getInt("id"));
				notice.setInstituteId(resultSet.getInt("instituteId"));
				notice.setBranchId(resultSet.getInt("branch_id"));
				notice.setInstituteName(resultSet.getString("instituteName"));
				notice.setBranchName(resultSet.getString("branchname"));
				notice.setTitle(resultSet.getString("title"));
				notice.setDescription(resultSet.getString("description"));
				notice.setCategory(resultSet.getString("category"));
				notice.setPriority(resultSet.getString("priority"));
				notice.setAcademicYearId(resultSet.getInt("academic_year_id"));
				notice.setImageName(resultSet.getString("image_url"));
				String imagePath = resultSet.getString("image_url");
				if(teacherUserId != null){
					notice.setReadStatus(resultSet.getString("readStatus"));
				}

				if(imagePath != null && imagePath.length() > 0){
					notice.setImageUrl(resultSet.getString("title") 
						+ imagePath.substring(imagePath.lastIndexOf(".") , imagePath.length()));	
				}

				List<Integer> groupUserIdList = setNoticeGroups(notice);
				
				setNoticeIndividuals(notice, groupUserIdList);
				return notice;
			}
		});
		if(!notices.isEmpty()){
			notice = notices.get(0);
		}
		return notice;
	}

	@Transactional
	protected void setNoticeIndividuals(final Notice notice, final List<Integer> groupUserIdList) {
		final List<Individual> individualList = new ArrayList<Individual>();
		final List<Integer> individualIdList = new ArrayList<Integer>();

		String sql = "select * from user_role where user_id in " +
				"(select user_id from notice_individual where notice_id = " + notice.getId() + ")";

		getJdbcTemplate().query(sql , new RowMapper<Individual>(){
			@Override
			public Individual mapRow(ResultSet resultSet, int arg1) throws SQLException {

				int userId = resultSet.getInt("user_id");
				if (!groupUserIdList.contains(userId)) {
					Integer role = resultSet.getInt("role_id");
					if (role.equals(Constants.ROLE_ID_STUDENT)) {
						individualList.add(individualDao.getIndividualFromStudent(userId));
					}
					/*else if(role.equals(Constants.ROLE_ID_PARENT)){
						individualList.add(individualDao.getIndividualFromParent(userId));
					}*/
					else if (role.equals(Constants.ROLE_ID_TEACHER)) {
						individualList.add(individualDao.getIndividualFromTeacher(userId));
					}
					else if(role.equals(Constants.ROLE_ID_VENDOR)){
						individualList.add(individualDao.getIndividualFromVendor(userId));
					}
					individualIdList.add(userId);
				}

				return null;
			}
		});
		
		notice.setIndividualIdList(individualIdList);
		notice.setIndividuals(individualList);
	}

	@Transactional
	protected List<Integer> setNoticeGroups(final Notice notice) {
		String sql = "select id, name from `group` where id in (select group_id from notice_group where notice_id = " + notice.getId() + ")";
		final List<Integer> groupUserIdList = new ArrayList<>();

		List<Master> groupList = getJdbcTemplate().query(sql, new RowMapper<Master>(){
			@Override
			public Master mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Master master = new Master();
				master.setId(resultSet.getInt("id"));
				master.setName(resultSet.getString("name"));

				String sql = "select user_id from group_user where group_id = "+master.getId();
				getJdbcTemplate().query(sql, new RowMapper<Integer> (){
					@Override
					public Integer mapRow(ResultSet resultSet1, int arg1) throws SQLException {
						groupUserIdList.add(resultSet1.getInt("user_id"));		
						return null;
					}
				});
				return master;
			}
		});
		notice.setGroupList(groupList);
		return groupUserIdList;
	}

	@Transactional
	private void updatePhotoPath(final Notice notice, final MultipartFile file){
		if( file != null){
			// get institute id of current branch for storing image.
			String sql = "select i.id as instituteId " +
					"from notice as n, institute as i, branch as b " +
					"where i.id = b.institute_id AND b.id = n.branch_id AND n.id = "+notice.getId();
			
			getJdbcTemplate().query(sql , new RowMapper<Integer>(){

				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					notice.setInstituteId(resultSet.getInt("instituteId"));

					String photoPath = saveFile(file, notice, false);
					notice.setImageUrl(Constants.APPLICATION_URL + photoPath);
					getJdbcTemplate().update("update notice set image_url = '"+photoPath+"' where id = "+notice.getId());

					return null;
				}
			});
		}
	}
	
	@Transactional
	private String saveFile(MultipartFile file, Notice notice, boolean isUpdate){
		String filePath = null;
		try {

			if(file != null ){
				String parentPath = notice.getInstituteId() + File.separator + notice.getBranchId() + File.separator
						 + FileConstants.NOTICE_DOCUMENTS_PATH;

				String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
				String fileNameWithExtension = org.apache.commons.lang3.RandomStringUtils.random(30, true, true) + fileExtension;

				if(isUpdate){
					String imageUrl = notice.getImageUrl();
					fileNameWithExtension = (StringUtils.isEmpty(imageUrl)  || imageUrl.endsWith(FileConstants.DEFAULT_IMAGE_NAME) 
							? fileNameWithExtension : imageUrl.substring(imageUrl.lastIndexOf("/") + 1));
				}
				
				filePath = fileFolderService.createFileInExternalStorage
						 (parentPath, fileNameWithExtension, file.getBytes());
			}
			
			filePath = FilenameUtils.separatorsToUnix(filePath);
			logger.debug("Notice file saved --"+filePath);
		}catch(Exception e){
			throw new RuntimeException("Notice image not uploaded ..", e);
		}
		return filePath;
	}
}
