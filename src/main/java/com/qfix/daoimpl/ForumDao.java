package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qfix.exceptions.BranchForumNotExistException;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Forum;
import com.qfix.service.user.client.forum.response.MessageResponse;
import com.qfix.service.user.client.forum.response.session.ForumResponse;
import com.qfix.utilities.Constants;

@Repository
public class ForumDao {
	final static Logger logger = Logger.getLogger(ForumDao.class);
	private Connection connection;
	private Statement statement;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet ;
	@Autowired
	private Connectivity connect;
	
	public List<Forum> getAll(Integer branchId){
		List<Forum> forumList = new ArrayList<Forum>();
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			String sql = null;
			if(branchId != null && branchId != 0){
				sql = "select gf.*,b.id as branchId, b.name as branch, g.id as groupId, g.name as groupName, g.branch_id as groupBranch " +
						"from group_forum as gf,branch as b, `group` as g where gf.is_delete = 'N' AND gf.is_active = 'Y' AND " +
						"gf.branch_id = b.id AND g.id = gf.group_id AND b.id = "+branchId;
			}else{
				sql = "select gf.*,b.id as branchId, b.name as branch, g.id as groupId, g.name as groupName, g.branch_id as groupBranch " +
						"from group_forum as gf,branch as b, `group` as g where gf.is_delete = 'N' AND gf.is_active = 'Y'" +
						" AND gf.branch_id = b.id AND g.id = gf.group_id";
			}

			logger.debug("get All forum sql --"+sql);
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Forum forum = new Forum();
				forum.setId(resultSet.getInt("id"));
				forum.setTitle(resultSet.getString("title"));
				forum.setDescription(resultSet.getString("description"));
				forum.setGroupId(resultSet.getInt("group_id"));
				forum.setGroupName(resultSet.getString("groupName"));
				forum.setForumId(resultSet.getInt("forum_id"));
				forum.setBranchId(resultSet.getInt("branch_id"));
				forum.setCanOneWay(resultSet.getString("read_only"));
				forum.setActive(resultSet.getString("is_active"));
				forum.setIsDelete(resultSet.getString("is_delete"));
				forum.setAcademicYearId(resultSet.getInt("academic_year_id"));
				forumList.add(forum);
			}	
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return forumList;
	}

	public boolean insertForum(Forum forum) throws UserAllreadyExistException, BranchForumNotExistException{
		boolean flag = false;
		connection = connect.getConnection();
		boolean isForumExist = isForumExist(forum);
		try {
			if(!isForumExist){
				statement = connection.createStatement();
				resultSet = statement.executeQuery("select forum_category_id from branch_forum_detail " +
						"where branch_id = "+forum.getBranchId());
				if(resultSet.next()){
					logger.debug("insert forum started.."+forum.getTitle());
					int forumCategoryId = resultSet.getInt("forum_category_id");
					if(forumCategoryId != 0){
						ForumResponse response = createForumProfile(forum, forumCategoryId);
						if(response != null && response.getForum_id() != 0 ){

							String sql = "insert into group_forum (title, description, group_id, branch_id, " +
									"read_only, forum_id, is_active, is_delete, forum_group_id, created_by, " +
									"academic_year_id) " +
									"values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

							preparedStatement = connection.prepareStatement(sql);
							preparedStatement.setString(1, forum.getTitle());
							preparedStatement.setString(2, forum.getDescription());
							preparedStatement.setInt(3, forum.getGroupId());
							preparedStatement.setInt(4, forum.getBranchId());
							preparedStatement.setString(5, forum.getCanOneWay());
							preparedStatement.setLong(6, response.getForum_id());
							preparedStatement.setString(7, forum.getActive());
							preparedStatement.setString(8, "N");
							preparedStatement.setLong(9, response.getForum_gid());
							preparedStatement.setInt(10, forum.getCreatedBy());
							preparedStatement.setInt(11, forum.getAcademicYearId());
							preparedStatement.executeUpdate();
							linkGroupUsersWithForum(response.getForum_gid(), forum.getGroupId());
							flag = true;
						}
					}					
				}
				else {
					logger.error("Branch is not registerd with forum...");
					throw new BranchForumNotExistException("This branch is not registered with forum.");
				}
			}
			else {
				throw new UserAllreadyExistException("Forum already exist by this name.");
			}
		}
		catch(UserAllreadyExistException e){
			throw e;
		}
		catch(BranchForumNotExistException e){
			throw e;
		}
		catch(Exception e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}
	
	private void linkGroupUsersWithForum(long forum_gid, Integer groupId) throws SQLException {
		
		Set<Integer> userList = getForumProfileIdListByGroup(groupId);
		if(userList.size() > 0) {
			ForumProfileDao forumProfileDao = new ForumProfileDao();
			logger.debug("Linking users with forum... forum_gid --"+forum_gid+", userList --"+userList);
			MessageResponse forumResponse = forumProfileDao.linkUserToForum(forum_gid, userList);
			logger.debug("Link users with forum RESPONSE --"+forumResponse);
		}
	}

	private Set<Integer> getForumProfileIdListByGroup(Integer  groupId) throws SQLException{
		Statement statement = connection.createStatement();
		
		String parentSql = "select forum_profile_id from user_forum_profile where user_id = ?";
		PreparedStatement parentStatement = connection.prepareStatement(parentSql);
		
		String sql = "SELECT forum_profile_id,u.id as userId,ur.role_id"
				+" FROM `user_forum_profile` as usf INNER JOIN `user` as u on u.id = usf.user_id"
				+" LEFT JOIN user_role ur on ur.user_id = u.id"
				+" INNER JOIN group_user as gu on usf.user_id = gu.user_id and gu.group_id = "+groupId+"";
		
		logger.debug("get forum profile Id list by group SQL --"+sql);
		
		ResultSet resultSet = statement.executeQuery(sql);
		
		Set<Integer> userSet = new HashSet<>();

		while(resultSet.next()){
			if(resultSet.getInt("role_id") == Constants.ROLE_ID_STUDENT){
				userSet.add(resultSet.getInt("forum_profile_id"));
//				parentStatement.setInt(1, resultSet.getInt("parentUserId"));
//				ResultSet parentResultSet = parentStatement.executeQuery();
//				if(parentResultSet.next()){
//					
//					userSet.add(parentResultSet.getInt("forum_profile_id"));
//				}
//				parentResultSet.close();
			}
			else if(resultSet.getInt("role_id") == Constants.ROLE_ID_PARENT){
				userSet.add(resultSet.getInt("forum_profile_id"));
			}
			else if(resultSet.getInt("role_id") == Constants.ROLE_ID_VENDOR){
				userSet.add(resultSet.getInt("forum_profile_id"));
			}
		}
		parentStatement.close();
		resultSet.close();
		statement.close();

		return userSet;
	}


	public List<Forum> getForumByBranchId(Integer userId, Integer roleId, Integer branchId) {
		List<Forum> forumList = new ArrayList<Forum>();
		try {
			connection = connect.getConnection();
			statement = connection.createStatement();
			String sql = null;
			
			if(userId != null && roleId == Constants.ROLE_ID_TEACHER){
				sql = "select gf.*,b.id as branchId, b.name as branch, g.id as groupId, " +
						"g.name as groupName, g.branch_id as groupBranch " +
						"from group_forum as gf,branch as b, `group` as g where gf.is_delete = 'N' AND gf.is_active = 'Y' AND " +
						"gf.branch_id = b.id AND g.id = gf.group_id "
						+" AND gf.created_by = "+userId;
			}
			else {
				sql = "select gf.*,b.id as branchId, b.name as branch, g.id as groupId, " +
						"g.name as groupName, g.branch_id as groupBranch " +
						"from group_forum as gf,branch as b, `group` as g where gf.is_delete = 'N' AND gf.is_active = 'Y' AND " +
						"gf.branch_id = b.id AND g.id = gf.group_id AND b.id = "+branchId;
			}
			logger.debug("get forum list by branch SQL --"+sql);
			
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()){
				Forum forum = new Forum();
				forum.setId(resultSet.getInt("id"));
				forum.setTitle(resultSet.getString("title"));
				forum.setDescription(resultSet.getString("description"));
				forum.setGroupId(resultSet.getInt("group_id"));
				forum.setGroupName(resultSet.getString("groupName"));
				forum.setForumId(resultSet.getInt("forum_id"));
				forum.setBranchId(resultSet.getInt("branch_id"));
				forum.setCanOneWay(resultSet.getString("read_only"));
				forum.setActive(resultSet.getString("is_active"));
				forum.setIsDelete(resultSet.getString("is_delete"));
				forum.setAcademicYearId(resultSet.getInt("academic_year_id"));
				forumList.add(forum);
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return forumList;
	}
	
	
	public Forum getForumById(Integer id){
		Forum forum = null;
		connection = connect.getConnection();
		try {
			String sql = "select gf.*, i.id as instituteId from group_forum as gf, institute as i, " +
					"branch as b " +
					"where  i.id = b.institute_id AND b.id = gf.branch_id AND gf.id = "+id;		
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			if(resultSet.next()){
				forum = new Forum();
				forum.setId(resultSet.getInt("id"));
				forum.setTitle(resultSet.getString("title"));
				forum.setDescription(resultSet.getString("description"));
				forum.setGroupId(resultSet.getInt("group_id"));
				forum.setForumId(resultSet.getInt("forum_id"));
				forum.setBranchId(resultSet.getInt("branch_id"));
				forum.setCanOneWay(resultSet.getString("read_only"));
				forum.setInstituteId(resultSet.getInt("instituteId"));
				forum.setActive(resultSet.getString("is_active"));
				forum.setIsDelete(resultSet.getString("is_delete"));
				forum.setForumGroupId(resultSet.getInt("forum_group_id"));
				forum.setAcademicYearId(resultSet.getInt("academic_year_id"));
				resultSet.close();
				}
			}
		catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return forum;	
	}
	
	
	
	public boolean updateForum(Forum forum) throws UserAllreadyExistException {
		boolean flag = false;
		connection = connect.getConnection();
		boolean isForumExist = isForumExist(forum);
		try {
			if(!isForumExist){
				Forum previousForum = getForumById(forum.getId());
				logger.debug("Updating forum...."+forum.getTitle());
				connection = connect.getConnection();
				String sql = "update group_forum set title = ?, description = ?, group_id = ?, branch_id = ?, " +
						"read_only = ?, is_active = ?, is_delete = ?, academic_year_id = ? where id = ?";
				
					preparedStatement = connection.prepareStatement(sql);
					preparedStatement.setString(1, forum.getTitle());
					preparedStatement.setString(2, forum.getDescription());
					preparedStatement.setInt(3, forum.getGroupId());
					preparedStatement.setInt(4, forum.getBranchId());
					preparedStatement.setString(5, forum.getCanOneWay());
					preparedStatement.setString(6, forum.getActive());
					preparedStatement.setString(7, "N");
					preparedStatement.setInt(8, forum.getAcademicYearId());
					preparedStatement.setInt(9, forum.getId());
					preparedStatement.executeUpdate();
					flag = true;
					
					if(!previousForum.getGroupId().equals(forum.getGroupId())){
						logger.debug("Group changed");
						linkOrDelinkForumUsers(previousForum.getGroupId(), forum.getGroupId(), previousForum.getForumGroupId());
					}
				}
			else {
				throw new UserAllreadyExistException("This forum is already added .");
			}
		}	
		catch(SQLException e){
			logger.error("forum update failed..."+forum.getTitle());
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}

	
	private void linkOrDelinkForumUsers(Integer previousGroupId, Integer newGroupId, Integer forumGroupId) throws SQLException {
		Set<Integer> newUserIdSet = new HashSet<>();
		Set<Integer> deletedUserIdSet = new HashSet<>();
		
		Set<Integer> previousGroupsUserForumProfileIds = getForumProfileIdListByGroup(previousGroupId);
		Set<Integer> currentGroupsUserForumProfileIds = getForumProfileIdListByGroup(newGroupId);
	
		if(currentGroupsUserForumProfileIds == null || currentGroupsUserForumProfileIds.size() <= 0){
			deletedUserIdSet.addAll(previousGroupsUserForumProfileIds);
		}
		else if(previousGroupsUserForumProfileIds == null || previousGroupsUserForumProfileIds.size() <= 0){
			newUserIdSet.addAll(currentGroupsUserForumProfileIds);
		}
		else {
			for(Integer forumProfileId : currentGroupsUserForumProfileIds){
				if(!previousGroupsUserForumProfileIds.contains(forumProfileId)){
					newUserIdSet.add(forumProfileId);
				}
			}
			
			for(Integer forumProfileId : previousGroupsUserForumProfileIds){
				if(!currentGroupsUserForumProfileIds.contains(forumProfileId)){
					deletedUserIdSet.add(forumProfileId);
				}
			}
		}

		if(newUserIdSet.size() > 0){
			linkUserToForum(newUserIdSet, forumGroupId);
		}
		if(deletedUserIdSet.size() > 0){
			deLinkUserToForum(deletedUserIdSet, forumGroupId);
		}
	}
	
	
	private void deLinkUserToForum(Set<Integer> deletedUserForumIdList, Integer forumGroupId) throws SQLException {
		ForumProfileDao forumProfileDao = new ForumProfileDao();
		forumProfileDao.deLinkUserToForum(forumGroupId, deletedUserForumIdList);
	}


	private void linkUserToForum(Set<Integer> newUserForumIdList, Integer forumGroupId) throws SQLException {
		ForumProfileDao forumProfileDao = new ForumProfileDao();
		forumProfileDao.linkUserToForum(forumGroupId, newUserForumIdList);
	}

	public boolean deleteForum(Integer id) {
		boolean flag = false;
		connection = connect.getConnection();
		try {
			statement = connection.createStatement();
			// delete from forum ..
			String sql = "update group_forum set is_Delete = 'Y' where id = "+id;
			statement.executeUpdate(sql);
			
			// Delink all users from forum..
			// deLinkAllUsersWithForum(id);
			// delete from forum phpbb service.
			logger.debug("Forum deleted Now deleting from forum-service...");
			
			resultSet = statement.executeQuery("select forum_id from group_forum where id = "+id);
			if(resultSet.next()){
				new ForumProfileDao().deleteForum(resultSet.getInt("forum_id"));
			}
			
			flag = true;
		}
		catch(SQLException e){
			logger.error("", e);
		}
		finally {
			connect.closeDatabaseTransaction(resultSet, statement, preparedStatement, connection);
		}
		return flag;
	}
	

	/*private void deLinkAllUsersWithForum(Integer forumId) throws SQLException{
		resultSet = statement.executeQuery("select forum_profile_id from user_forum_profile where user_id in " +
				"(select user_id from group_user where group_id = " +
				"(select group_id from group_forum where id = "+forumId+"))");
		Set<Integer> userIds = new HashSet<>();

		while(resultSet.next()){
			userIds.add(resultSet.getInt("forum_profile_id"));
		}
		Integer forumGroupId = null;
		resultSet = statement.executeQuery("select forum_group_id from group_forum where id = "+forumId);
		if(resultSet.next()){
			forumGroupId = resultSet.getInt("forum_group_id");
		}

		if(forumGroupId != null && userIds.size() > 0){
			deLinkUserToForum(userIds, forumGroupId);	
		}
		
	}*/
	
	
	private boolean isForumExist(Forum forum){
		boolean flag = false;
		try {
			statement = connection.createStatement();
			String sql = "select count(*) as count from group_forum where title = '"+forum.getTitle()+"' AND branch_id = " +forum.getBranchId()+
					(forum.getId() != null ? " AND id != "+forum.getId() : "")+
					" AND group_id = "+forum.getGroupId();
			logger.debug("Is forum exist SQL--"+sql);
			resultSet = statement.executeQuery(sql);
			if(resultSet.next()){
				if(resultSet.getInt("count") > 0){
					flag = true;
				}
			}
		}catch(SQLException e){
			logger.error("", e);
		}
		return flag;
	}

	private ForumResponse createForumProfile(Forum forum, Integer forum_category_id) throws SQLException {
		
		ForumProfileDao forumProfileDao = new ForumProfileDao();
		ForumResponse forumResponse = forumProfileDao.createForum(forum.getTitle(), forum_category_id, forum.getDescription(), "F", forum.getCanOneWay());
		
		return forumResponse;	
	}
}
