package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.qfix.model.Dashboard;
import com.qfix.model.DashboardFeeHeadData;
import com.qfix.model.DashboardFeeReply;
import com.qfix.model.DashboardFeeStatus;
import com.qfix.model.DashboardFeeTable;
import com.qfix.model.DashboardPayment;
import com.qfix.model.Head;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class DashboardDao  extends JdbcDaoSupport{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(DashboardDao.class);
	
	public List<Dashboard> getAll(Integer branchId){
		String sql = "select * from division where active = 'Y' AND branch_id = "+branchId;
			
		return getJdbcTemplate().query(sql, new RowMapper<Dashboard>() {
			public Dashboard mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				Dashboard dashboard = new Dashboard();
				dashboard.setId(resultSet.getInt("id"));
			//	dashboard.setHeadId(resultSet.getInt("headId"));
				dashboard.setBranchId(resultSet.getInt("branch_id"));
				return dashboard;
			}
		});
	}


	public List<Head> getHead(Integer branchId){
		
		String sql = "select * from head where is_delete = 'N' AND branch_id = "+branchId;
		
		return getJdbcTemplate().query(sql, new RowMapper<Head>() {
			public Head mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				Head head = new Head();
				head.setId(resultSet.getInt("id"));
				head.setName(resultSet.getString("name"));
				head.setActive(resultSet.getString("active"));
				head.setBranchId(resultSet.getInt("branch_id"));
				return head;
			}
		});			
	}
	
	public List<DashboardPayment> getDashboardPayment(Integer branchId, Integer days){
		String monthBegin = getDatefromDays(days);
		log.info(monthBegin);
		String sql = "select count(pd.id) as transactionCount, sum(pd.amount) as transactionSum, STR_TO_DATE(pd.created_at, '%Y-%m-%d') as transactionDate"
				+ " from payment.payment_detail pd "
				+ "join qfix.payment_audit pa on pa.payment_detail_id = pd.id "
				+ "join student s on s.user_id = pa.user_id "
				+ "where pd.created_at between STR_TO_DATE('"+ monthBegin +"', '%Y-%m-%d')  "
				+ "and  STR_TO_DATE(NOW(), '%Y-%m-%d')  and  s.branch_id = " + branchId
				+ " group by STR_TO_DATE(created_at, '%Y-%m-%d')";
		
		log.info(sql);
		
		List<DashboardPayment> paymentList = getJdbcTemplate().query(sql, 
				new BeanPropertyRowMapper<DashboardPayment>(DashboardPayment.class));
		return paymentList;
	}
	
	public List<DashboardFeeReply> getDashboardFeeStatus(Integer branchId){
		/*LocalDate monthBegin = new LocalDate().withDayOfMonth(1);
		LocalDate monthEnd = new LocalDate().plusMonths(1).withDayOfMonth(1).minusDays(1);
		log.info(monthBegin + " " + monthEnd);*/
		/*
		String monthBegin = unixTimeToDate(startDate);
		String monthEnd = unixTimeToDate(endDate);
		log.info(monthBegin + " " + monthEnd);
		
		String sql = "select count(fs.id) as studentCount, status as feeStatus from fees_schedule fs "
				+ "join student s on s.user_id = fs.user_id "
				+ "where STR_TO_DATE(fs.due_date, '%Y-%m-%d')  between STR_TO_DATE('"+ monthBegin +"', '%Y-%m-%d') "
				+ "and STR_TO_DATE('"+ monthEnd +"', '%Y-%m-%d') and branch_id = " + branchId +" group by fs.status ";
		
		log.info(sql);
		
		List<DashboardFeeStatus> feeStatusList = getJdbcTemplate().query(sql, 
				new BeanPropertyRowMapper<DashboardFeeStatus>(DashboardFeeStatus.class));
		return feeStatusList;*/
		List<DashboardFeeReply> list = new LinkedList<DashboardFeeReply>();
		Map<String, Double> map = new HashMap<String, Double>();
		String monthBegin = getAcademicYearStartDate(branchId);
		//String monthEnd = unixTimeToDate(endDate);
		log.info(monthBegin);
		
		/* Query to get paid amount with late payment charges */
		String sql = "select sum(total_amount) as totalAmount from fees_schedule fs "
				+ "join student s on s.user_id = fs.user_id where STR_TO_DATE(fs.due_date, '%Y-%m-%d') "
				+ " between STR_TO_DATE('" + monthBegin + "', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d') "
				+ "and s.branch_id = " + branchId + " and fs.status = 'PAID'  and fs.is_delete = 'N' and late_payment_charges > 0;";
		log.info(sql);
		Double amount = getJdbcTemplate().queryForObject(sql, Double.class);
		log.info("Paid_With_Late_Fees -> " + amount );
		if(amount != null)
			map.put("Paid_With_Late_Fees", amount);
		else
			map.put("Paid_With_Late_Fees", 0.0);
		/* Query to get paid amount with late payment charges */
		
		/* Query to get paid amount without late payment charges */
		
		sql = "select sum(base_amount) as totalAmount from fees_schedule fs "
				+ "join student s on s.user_id = fs.user_id where STR_TO_DATE(fs.due_date, '%Y-%m-%d')  "
				+ "between STR_TO_DATE('" + monthBegin + "', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d') "
				+ "and s.branch_id = " + branchId + " and fs.status = 'PAID'  and fs.is_delete = 'N' and late_payment_charges <= 0";
		log.info(sql);
		amount = getJdbcTemplate().queryForObject(sql, Double.class);
		log.info("Fees_Paid -> " + amount );
		if(amount != null)
			map.put("Fees_Paid", amount);
		else
			map.put("Fees_Paid", 0.0);
		/* Query to get paid amount without late payment charges */
		
		/* Query to get unpaid amount */
		sql = "select sum(total_amount) as totalAmount from fees_schedule fs "
				+ "join student s on s.user_id = fs.user_id where STR_TO_DATE(fs.due_date, '%Y-%m-%d')  "
				+ "between STR_TO_DATE('" + monthBegin + "', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d') "
				+ "and s.branch_id = " + branchId + " and fs.status = 'UNPAID'  and fs.is_delete = 'N'";
		log.info(sql);
		amount = getJdbcTemplate().queryForObject(sql, Double.class);
		log.info("Fees_Unpaid -> " + amount );
		if(amount != null)
			map.put("Fees_Unpaid", amount);
		else
			map.put("Fees_Unpaid", 0.0);
		/* Query to get unpaid amount */
		
		/*Query to get partial paid amount*/
		/*sql = "select base_amount as baseAmount, late_payment_charges as latePaymentCharges, "
			+ "total_amount as totalAmount, partial_paid_amount as partialPaidAmount from fees_schedule fs "
			+ "join student s on s.user_id = fs.user_id where STR_TO_DATE(fs.due_date, '%Y-%m-%d') "
			+ " between STR_TO_DATE('" + monthBegin + "', '%Y-%m-%d') and STR_TO_DATE('" + monthEnd + "', '%Y-%m-%d') "
			+ "and s.branch_id = " + branchId + " and fs.status = 'PARTIAL_PAID'";*/
		sql = "select fs.parent_fees_schedule_id as feeScheduleId, "
			+ "(select fsh.late_payment_charges from fees_schedule fsh where fsh.id = fs.parent_fees_schedule_id) as latePaymentCharges,"
			+ " sum(fs.partial_paid_amount) as partialAmountSum, null from fees_schedule fs "
			+ "join student s on s.user_id = fs.user_id where STR_TO_DATE(fs.due_date, '%Y-%m-%d')  "
			+ "between STR_TO_DATE('" + monthBegin + "', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d') "
			+ "and s.branch_id = " + branchId + "  and fs.is_delete = 'N' and  fs.status = 'PARTIAL_PAID' "
			+ "and fs.parent_fees_schedule_id is not null  and fs.is_delete = 'N' group by fs.parent_fees_schedule_id";
		log.info(sql);
		List<DashboardFeeStatus> d = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<DashboardFeeStatus>(DashboardFeeStatus.class));
		calculateAmoutOnFeeStatus(map, d);
		/*Query to get partial paid amount*/
		
		for(String s : map.keySet()){
			DashboardFeeReply da = new DashboardFeeReply();	
			if(map.get(s) != null){
				da.setFeeType(s);
				da.setAmount(map.get(s));
				list.add(da);
			}
		}
		log.info(list.toString());
		return list;
	}
	
	private String getAcademicYearStartDate(Integer branchId){
		String sql = "select from_date from academic_year where is_current_active_year = 'Y' and branch_id = " + branchId;
		String fromDate = getJdbcTemplate().queryForObject(sql, String.class);
		return fromDate;
	}
	
	private void calculateAmoutOnFeeStatus(Map<String, Double> map, List<DashboardFeeStatus> list){
		for(DashboardFeeStatus d : list){
			Double partialPaidAmount =  d.getPartialAmountSum() + getParentPartialAmount(d.getFeeScheduleId());
			Double totalAmount = getFeeScheduleTotalAmount(d.getFeeScheduleId());
			
			if(d.getLatePaymentCharges() > 0){
				Double paidAmount = map.get("Paid_With_Late_Fees");
				paidAmount += partialPaidAmount;
				map.put("Paid_With_Late_Fees", paidAmount);
			}else if(d.getLatePaymentCharges() <= 0){
				Double paidAmount = map.get("Fees_Paid");
				paidAmount += partialPaidAmount;
				map.put("Fees_Paid", paidAmount);	
			}
			
			Double unpaidAmount = map.get("Fees_Unpaid");
			unpaidAmount += (totalAmount - partialPaidAmount);
			map.put("Fees_Unpaid", unpaidAmount);
		}
	}
	
	private Double getFeeScheduleTotalAmount(Integer id){
		String sql = "select total_amount from fees_schedule where id = " + id;
		Double totalAmount = getJdbcTemplate().queryForObject(sql, Double.class);
		return totalAmount;
	}
	
	private Double getParentPartialAmount(Integer id){
		String sql = "select partial_paid_amount from fees_schedule where id = " + id;
		Double parentPartialAmount = getJdbcTemplate().queryForObject(sql, Double.class);
		return parentPartialAmount;
	}
	
	private String unixTimeToDate(Long unixTime){
		Date date = new Date(unixTime*1000L); // *1000 is to convert seconds to milliseconds
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
		return sdf.format(date);
	}
	
	private String getDatefromDays(Integer days){
		days = days - 1;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - days);
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
		return sdf.format(date);
	}
	
	public List<DashboardFeeTable> getDashboardFeeTableData(Integer branchId){
		Map<String, DashboardFeeTable> map = new HashMap<String,DashboardFeeTable>();
		String monthBegin = getAcademicYearStartDate(branchId);
		String sql = "select sum(total_amount) as sumAmount, h.name as feeHeadName from fees_schedule fs "
				+ "join fees f on f.id = fs.fees_id join head h on h.id = f.head_id where status = 'UNPAID' and fs.is_delete = 'N'"
				+ "and from_unixtime( UNIX_TIMESTAMP(fs.created_at), '%Y-%m-%d') "
				+ "between STR_TO_DATE('" + monthBegin + "', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d')  "
				+ "and f.branch_id = " + branchId + " group by h.name ";
		log.info(sql);
		List<DashboardFeeHeadData> data = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<DashboardFeeHeadData>(DashboardFeeHeadData.class));
		updateFeeHeadMap(map, data, "UNPAID");
		
		sql = "select sum(total_amount) as sumAmount, h.name as feeHeadName "
				+ "from fees_schedule fs join fees f on f.id = fs.fees_id "
				+ "join head h on h.id = f.head_id where status = 'PAID' and from_unixtime( UNIX_TIMESTAMP(fs.created_at), '%Y-%m-%d') "
				+ "between STR_TO_DATE('"+monthBegin+"', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d') and fs.late_payment_charges <= 0 "
				+ "and fs.is_delete = 'N' and f.branch_id = "+branchId+" group by h.name";
		log.info(sql);
		data = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<DashboardFeeHeadData>(DashboardFeeHeadData.class));
		log.info(data.toString());
		updateFeeHeadMap(map, data, "PAID");
		
		sql = "select sum(total_amount) as sumAmount, h.name as feeHeadName "
				+ "from fees_schedule fs join fees f on f.id = fs.fees_id "
				+ "join head h on h.id = f.head_id where status = 'PAID'  and fs.is_delete = 'N' and from_unixtime( UNIX_TIMESTAMP(fs.created_at), '%Y-%m-%d') "
				+ "between STR_TO_DATE('"+monthBegin+"', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d') and fs.late_payment_charges > 0 "
				+ "and f.branch_id = "+branchId+" group by h.name";
		log.info(sql);
		data = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<DashboardFeeHeadData>(DashboardFeeHeadData.class));
		log.info(data.toString());
		updateFeeHeadMap(map, data, "PAID_WITH_LATE_FEES");
		
		sql = "select fs.parent_fees_schedule_id as feeScheduleId, "
				+ "(select fsh.late_payment_charges from fees_schedule fsh where fsh.id = fs.parent_fees_schedule_id) "
				+ "as latePaymentCharges, sum(fs.partial_paid_amount) as partialAmountSum, h.name as feeHeadName "
				+ "from fees_schedule fs join fees f on f.id = fs.fees_id "
				+ "join head h on h.id = f.head_id where STR_TO_DATE(fs.due_date, '%Y-%m-%d')  "
				+ "between STR_TO_DATE('"+ monthBegin +"', '%Y-%m-%d') and STR_TO_DATE(NOW(), '%Y-%m-%d') "
				+ "and f.branch_id = "+branchId+" and fs.status = 'PARTIAL_PAID'  and fs.is_delete = 'N' and fs.parent_fees_schedule_id is not null  "
				+ "group by h.name, fs.parent_fees_schedule_id;";
		log.info(sql);
		List<DashboardFeeStatus> dfs = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<DashboardFeeStatus>(DashboardFeeStatus.class));
		calculatePartialPaidData(map, dfs);
		List<DashboardFeeTable> list = new LinkedList<DashboardFeeTable>(map.values());
		return list;
	}
	
	private void updateFeeHeadMap(Map<String, DashboardFeeTable> map, List<DashboardFeeHeadData> data, String status){
		for(DashboardFeeHeadData d : data){
			String feeheadName = d.getFeeHeadName();
			DashboardFeeTable dft = getDashboardFeeTable(map, d.getFeeHeadName());
			
			if(status.equals("UNPAID")){
				if(dft.getFeeHeadType() == null)
					dft.setFeeHeadType(feeheadName);
				dft.setUnpaidAmount(d.getSumAmount());
				
			}else if(status.equals("PAID")){
				if(dft.getFeeHeadType() == null)
					dft.setFeeHeadType(feeheadName);
				dft.setPaidAmount(d.getSumAmount());	
			}else if(status.equals("PAID_WITH_LATE_FEES")){
				if(dft.getFeeHeadType() == null)
					dft.setFeeHeadType(feeheadName);
				dft.setPaidAmountWithLateFees(d.getSumAmount());
			}
			map.put(feeheadName, dft);
		}
	}
	
	private void calculatePartialPaidData(Map<String, DashboardFeeTable> map, List<DashboardFeeStatus> data){
		for(DashboardFeeStatus d : data){
			Double partialPaidAmount =  d.getPartialAmountSum() + getParentPartialAmount(d.getFeeScheduleId());
			Double totalAmount = getFeeScheduleTotalAmount(d.getFeeScheduleId());
			DashboardFeeTable dft = getDashboardFeeTable(map, d.getFeeHeadName());
			
			if(d.getLatePaymentCharges() > 0){
				dft.setPaidAmountWithLateFees(dft.getPaidAmount() + partialPaidAmount);
				dft.setUnpaidAmount(dft.getUnpaidAmount() + (totalAmount - partialPaidAmount)); 
			}else if(d.getLatePaymentCharges() <= 0){
				dft.setPaidAmount(dft.getPaidAmount() + partialPaidAmount);
				dft.setUnpaidAmount(dft.getUnpaidAmount() + (totalAmount - partialPaidAmount));
			}
			map.put(d.getFeeHeadName(), dft);
		}
	}
	
	private DashboardFeeTable getDashboardFeeTable(Map<String, DashboardFeeTable> map, String feeHeadName){
		DashboardFeeTable dft = null;
		if(map.containsKey(feeHeadName))
			dft = map.get(feeHeadName);
		else
			dft = new DashboardFeeTable();
		return dft; 
	}
}
