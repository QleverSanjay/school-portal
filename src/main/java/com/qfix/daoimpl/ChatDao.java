package com.qfix.daoimpl;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.dao.IChatDao;
import com.qfix.model.ChatProfile;
import com.qfix.model.Student;
import com.qfix.service.user.client.chat.RestClient;
import com.qfix.service.user.client.chat.UrlConfiguration;
import com.qfix.service.user.client.chat.request.group.GroupRequest;
import com.qfix.service.user.client.chat.request.group.UpdateGroupRequest;
import com.qfix.service.user.client.chat.request.session.SessionRequest;
import com.qfix.service.user.client.chat.request.user.UserRequest;
import com.qfix.service.user.client.chat.response.group.CreateGroupResponse;
import com.qfix.service.user.client.chat.response.group.UpdateGroupResponse;
import com.qfix.service.user.client.chat.response.session.UserResponse;

@Repository
public class ChatDao extends JdbcDaoSupport implements IChatDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(ChatDao.class);
	public void assignStudentChatGroups(Student students) {
		
		//List<String> chatDialogIdsByBranch = getGroupChatDialogIdsForStudentByBranch(students.getEducationDetails().getBranchId(), students.getEducationDetails().getStandard(), students.getPersonalDetails().getId());	
		//List<String> chatDialogIdsByTags = getGroupChatDialogIdsForStudentByTags(students.getOtherDetails().getHobbies());	
		
	}

	public UserResponse createChatAccount(String email, String firstname, String surname, String tags) {
		
		UserResponse userResponse = null;
		RestClient restClient = new RestClient();
		UserRequest userRequest = new UserRequest();
		userRequest.setEmail(email);
		userRequest.setFirstname(firstname);
		userRequest.setSurname(surname);
		userRequest.setTags(tags);

		try {
			ResponseEntity<String> createSessionResponse = restClient.executeRequest(
					userRequest, UrlConfiguration.CREATE_USER_CONFIG);

			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				userResponse = mapper.readValue(createSessionResponse.getBody(), UserResponse.class);
				logger.debug("Chat account created RESPONSE --"+userResponse);
			}
			else {
				logger.error("Chat Account not created for user "+firstname+" "+surname+".");
				throw new RuntimeException("Chat Account not created for user "+firstname+" "+surname+".");
			}
		} catch (Exception e) {
			logger.error("Chat Account not created for user "+firstname+" "+surname+".", e);
			throw new RuntimeException("Chat Account not created for user "+firstname+" "+surname+".", e);
		}
		return userResponse;
	}

	public String createChatPublicGroups(String ownerUserName,String ownerPassword,String groupName, String photo) {
		String groupChatId = null;
		try {
			GroupRequest groupRequest = new GroupRequest();
			groupRequest.setGroupOwnerUsername(ownerUserName);
			groupRequest.setGroupOwnerPassword(ownerPassword);
			groupRequest.setGroupName(groupName);
			groupRequest.setPhotoId(photo);
			RestClient restClient = new RestClient();

			ResponseEntity<String> createSessionResponse = restClient.executeRequest(groupRequest,
				UrlConfiguration.CREATE_GROUP_CONFIG);

			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {

				ObjectMapper mapper = new ObjectMapper();
				CreateGroupResponse response = mapper.readValue(createSessionResponse.getBody(),
						CreateGroupResponse.class);
				groupChatId = response.getGroupChatId();
				logger.debug("Chat Group Created...");
			}
			else {
				logger.error("chat group not created for groupId --"+groupName);
			}
		}catch(Exception e){
			logger.error("Failed to create chat group::", e);
			throw new RuntimeException("Failed to create chat group::", e);
		}

		return groupChatId;
	}
	
	
	public UpdateGroupResponse updateChatPublicGroups(String groupId, String occupantsIdsAdd, String occupantsIdsRemove) {
		UpdateGroupResponse groupResponse = new UpdateGroupResponse();
		try {
			RestClient restClient = new RestClient();
			UpdateGroupRequest groupRequest = new UpdateGroupRequest();

			// Calling service seperatly for adding and removing as if we pass both id chat service not responding.
			groupRequest.setGroupId(groupId);

			if (occupantsIdsAdd != null || occupantsIdsRemove != null) {
				groupRequest.setOccupantsIdsAdd(occupantsIdsAdd);
				groupRequest.setOccupantsIdsRemove(occupantsIdsRemove);
				ResponseEntity<String> createSessionResponse = restClient.executeRequest(groupRequest,
					UrlConfiguration.UPDATE_GROUP_CONFIG);

				if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
					ObjectMapper mapper = new ObjectMapper();
					groupResponse = mapper.readValue(createSessionResponse.getBody(),
							UpdateGroupResponse.class);
					logger.debug("chat group updated RESPONSE --"+groupResponse);
				}
				else {
					logger.error("chat group not updated.. GroupId --"+groupId);
				}
			}
		}catch(Exception e){
			logger.error("Failed to update chat group ids for group - "+groupId +" :: ", e);
			throw new RuntimeException("Failed to update chat group ids for group - "+groupId +" :: ", e);
		}
		return groupResponse;
	}


	public ChatProfile getChatSessionDetails(String chatAccountUsername, String chatAccountPassword, long chatAccountProfileId) {
		ChatProfile chatProfile = null;
		try {
			SessionRequest sessionRequest = new SessionRequest();
			sessionRequest.setLogin(chatAccountUsername);
			sessionRequest.setPassword(chatAccountPassword);
			
			chatProfile = loginForChatService(sessionRequest);
			chatProfile.setPassword(chatAccountPassword);
			chatProfile.setChatUserProfileId(chatAccountProfileId);
			logger.debug("Chat session details loaded..");
		} catch(Exception e) {
			logger.error("", e);
			throw new RuntimeException("Failed to get chat session :: ", e);
		}
		return chatProfile;
	}
	
	
	//Re-factor this method later using rules-engine and NoSQL solution.
	/*private List<String> getGroupChatDialogIdsForStudentByBranch(Integer branchId, Integer standard,Integer studentId) {	
		
		String query = "select chat_dialog_id from group where branch_id=:branchId " +
				" and standard=:standard" +
				" and id NOT IN (select group_id from student_groups where student_id = :studentId)";		
		//List<Long> groupIds = retrieveGroupList(query, new Object[]{student.getEducationDetails().getBranchId()});
		
		Map<String,Integer> values = new HashMap<String,Integer>();
		values.put("branchId", branchId);
		values.put("standard", standard);
		values.put("studentId", studentId);
		
		return retrieveChatDialogIds(query, values);	
	}*/

	//Re-factor this method later using rules-engine and NoSQL solution.
	/*private List<String> getGroupChatDialogIdsForStudentByTags(List<String> tags) {	
		
		List<String> chatDialogIds = new ArrayList<String>();
		
		if(tags != null && tags.size() > 0) {
			
			String finalTagsList = "";
			
			for(String tag :tags) {
				finalTagsList += tag;
				finalTagsList = finalTagsList +",";				
			}
			
			finalTagsList = finalTagsList.endsWith(",") ? finalTagsList.substring(0, finalTagsList.length() -1) : finalTagsList;

			String query = "select chat_dialog_id from group where" +
					" id in (select group_id from group_tags where tag like (:tags)) " +
					" and (branch_id IS NULL or branch_id IS EMPTY)" +
					" and chat_dialog_id IS NOT NULL";
			
			Map<String,String> values = new HashMap<String,String>();
			values.put("tags", finalTagsList);
			
			chatDialogIds = retrieveChatDialogIds(query, values);
		} 		
		
		return chatDialogIds;		
	}	*/
	

	/*private List<String> retrieveChatDialogIds(String query, Map<String,?> values) {
		
		SqlParameterSource namedParameters = new MapSqlParameterSource(values);		
	
		return (List<String>) getJdbcTemplate().query(query, namedParameters, new RowMapper() {
			public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {				

				return new String(resultSet.getString("chat_dialog_id"));
			}
		});
	}*/
	
	
	private ChatProfile loginForChatService(SessionRequest sessionRequest)
			throws IOException {
		RestClient restClient = new RestClient();

		ResponseEntity<String> createSessionResponse = restClient.executeRequest(sessionRequest,
						UrlConfiguration.CREATE_SESSION_CONFIG);

		ChatProfile chatProfile = null;
		if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
			
			ObjectMapper mapper = new ObjectMapper();
			chatProfile = mapper.readValue(createSessionResponse.getBody(),
					ChatProfile.class);
			logger.debug("Chat profile login RESPONSE --"+chatProfile);
		}
		else {
			logger.error("Chat profile login failed RESPONSE --"+createSessionResponse);
		}
		/*else {
		 			throw new ApplicationException(createSessionResponse.getBody());

		}*/

		return chatProfile;
	}
	

}
