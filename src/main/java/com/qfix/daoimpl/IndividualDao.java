package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.qfix.dao.IIndividualDao;
import com.qfix.model.Individual;
import com.qfix.utilities.Constants;

@Repository
public class IndividualDao extends JdbcDaoSupport implements IIndividualDao {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	final static Logger logger = Logger.getLogger(IndividualDao.class);

	public List<Individual> getIndividualsByName(String name, Integer branchId, List<Integer> allreadySelectedIds) {
		
		List<Individual> individuals = new ArrayList<>();
		getIndividualsByNameFromStudent(name, branchId, allreadySelectedIds, individuals, null);
		
		getIndividualsByNameFromTeacher(name, branchId, allreadySelectedIds, individuals, null);

		getIndividualsByNameFromVendor(name, branchId, allreadySelectedIds, individuals, null);

		return individuals;
	}

	// Used for group individual selection..
	public List<Individual> getIndividualsByNameExceptGroup(String name, Integer groupId, Integer branchId, List<Integer> allreadySelectedIds) {

		List<Individual> individuals = new ArrayList<>();
		getIndividualsByNameFromStudent(name, branchId, allreadySelectedIds, individuals, groupId);
		
		getIndividualsByNameFromTeacher(name, branchId, allreadySelectedIds, individuals, groupId);

		getIndividualsByNameFromVendor(name, branchId, allreadySelectedIds, individuals, groupId);

		return individuals;
	}

	private void getIndividualsByNameFromVendor(String name, Integer branchId,
			final List<Integer> allreadySelectedIds, final List<Individual> individuals, Integer groupId) {
		
		String sql =  null;
		if(groupId == null){
			sql = "select u.id as userId, v.merchant_legal_name from vendor as v, user as u, " +
					" branch_vendor as iv where (merchant_legal_name like '%"+name+"%'" +
					" OR marketing_name like '%"+name+"%') AND v.user_id = u.id " +
					" AND iv.user_id = v.user_id AND iv.branch_id = "+branchId+
					" AND u.active = 'Y' AND v.active = 'Y'";	
		}
		else {
			sql = "select u.id as userId, v.merchant_legal_name from vendor as v, user as u, " +
					" branch_vendor as iv where (merchant_legal_name like '%"+name+"%'" +
					" OR marketing_name like '%"+name+"%') AND v.user_id = u.id " +
					" AND iv.user_id = v.user_id AND iv.branch_id = "+branchId+
					" AND u.active = 'Y' AND v.active = 'Y' " +
					" AND u.id not in (select user_id from group_user where group_id = "+groupId+")";
		}

		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName(resultSet.getString("merchant_legal_name"));
					//individual.setCourse(resultSet.getString("standard_id"));
					individual.setRole("Vendor");
					individuals.add(individual);
				}				
				return null;
			}
		});
	}

	private void getIndividualsByNameFromTeacher(final String name, final Integer branchId,
			final List<Integer> allreadySelectedIds, final List<Individual> individuals, Integer groupId) {
		String sql = null;
		if(groupId == null){
			 sql = "select u.id as userId, t.firstname, t.lastname from teacher as t, user as u " +
						" where (firstname like '%"+name+"%' OR lastname like '%"+name+"%'" +
						" OR middlename like '%"+name+"%') AND t.user_id = u.id AND t.branch_id = "+branchId+" " +
						" AND u.active = 'Y' AND t.isDelete =  'N' AND t.active = 'Y'";
		}
		else {
			 sql = "select u.id as userId, t.firstname, t.lastname from teacher as t, user as u " +
						" where (firstname like '%"+name+"%' OR lastname like '%"+name+"%'" +
						" OR middlename like '%"+name+"%') AND t.user_id = u.id AND t.branch_id = "+branchId+" " +
						" AND u.active = 'Y' AND t.isDelete =  'N' AND t.active = 'Y'"+
						" AND u.id not in (select user_id from group_user where group_id = "+groupId+")";
		}
		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName((resultSet.getString("firstname") != null ? resultSet.getString("firstname")+ " " : "")
							+ (resultSet.getString("lastname") != null ? resultSet.getString("lastname") : ""));
					//individual.setCourse(resultSet.getString("standard_id"));
					individual.setRole("Teacher");
					individuals.add(individual);
				}
				return null;
			}
		});
		
	}

	private void getIndividualsByNameFromStudent(final String name, final Integer branchId,
			final List<Integer> allreadySelectedIds, final List<Individual> individuals, Integer groupId) {

		String sql = null;

		if(groupId == null){
			sql = "select u.id as userId, s.*,st.displayName as standardName,d.displayName as divisionName " +
					" from student as s, user as u,standard as st, division as d where " +
					" (first_name like '%"+name+"%' OR last_name like '%"+name+"%'" +
					" OR father_name like '%"+name+"%'  OR mother_name like '%"+name+"%') " +
					" AND st.id = s.standard_id AND d.id = s.division_id AND s.user_id = u.id  AND s.branch_id = "+branchId+" AND " +
					" s.active = 'Y' AND s.isDelete = 'N' AND u.isDelete = 'N' AND u.active = 'Y'";
		}
		else {
			sql = "select u.id as userId, s.*,st.displayName as standardName,d.displayName as divisionName " +
					" from student as s, user as u,standard as st, division as d where " +
					" (first_name like '%"+name+"%' OR last_name like '%"+name+"%'" +
					" OR father_name like '%"+name+"%'  OR mother_name like '%"+name+"%') " +
					" AND st.id = s.standard_id AND d.id = s.division_id AND s.user_id = u.id  AND s.branch_id = "+branchId+" AND " +
					" s.active = 'Y' AND s.isDelete = 'N' AND u.isDelete = 'N' AND u.active = 'Y'"+
					" AND u.id not in (select user_id from group_user where group_id = "+groupId+")";
		}
		logger.debug("get individuals by name SQL --"+sql);

		getJdbcTemplate().query(sql, new RowMapper<Individual>(){
			@Override
			public Individual mapRow(ResultSet resultSet, int arg1)throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName((resultSet.getString("first_name") != null ? resultSet.getString("first_name")+ " " : "")
							+ (resultSet.getString("last_name") != null ? resultSet.getString("last_name") : ""));
					individual.setStandard(resultSet.getString("standardName"));
					individual.setDivision(resultSet.getString("divisionName"));
					individual.setRole("Student");
					individuals.add(individual);
				}
				return null;
			}
		});
	}


	public List<Individual> getStudentsForNoticeSearch(String name,Integer branchId, List<Integer> allreadySelectedIds) {
		List<Individual> individuals = new ArrayList<>();

		getIndividualsAsStudentForNotice(name, branchId, allreadySelectedIds, individuals);
		
		getIndividualsAsTeacherForNotice(name, branchId, allreadySelectedIds, individuals);

		getIndividualsAsVendorForNotice(name, branchId, allreadySelectedIds, individuals);
		return individuals;
	}

	private void getIndividualsAsVendorForNotice(String name, Integer branchId, 
			final List<Integer> allreadySelectedIds, final List<Individual> individuals) {

		String sql = "select u.id as userId, v.merchant_legal_name from vendor as v, user as u, " +
				" branch_vendor as iv where (merchant_legal_name like '%"+name+"%'" +
				" OR marketing_name like '%"+name+"%') AND v.user_id = u.id AND iv.user_id = v.user_id " +
				"AND iv.branch_id = "+branchId+" AND u.active = 'Y' AND v.active = 'Y'";

		logger.debug("get students for notice search SQL --"+sql);
		
		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName(resultSet.getString("merchant_legal_name"));
					//individual.setCourse(resultSet.getString("standard_id"));
					individual.setRole("Vendor");
					individuals.add(individual);
				}
				return null;
			}
		});
	}
	
	private void getIndividualsAsStudentForNotice(String name, Integer branchId, 
			final List<Integer> allreadySelectedIds, final List<Individual> individuals) {

		String sql = "select u.id as userId, s.* from student as s, user as u where " +
				"(first_name like '%"+name+"%' OR last_name like '%"+name+"%' " +
				"OR father_name like '%"+name+"%'  OR mother_name like '%"+name+"%') " +
				"AND s.user_id = u.id AND s.active = 'Y' AND s.isDelete = 'N' " +
				"AND u.active = 'Y' AND s.branch_id = "+branchId;

		logger.debug("get students for notice search SQL --"+sql);
		
		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName((resultSet.getString("first_name") != null ? resultSet.getString("first_name")+ " " : "")
							+ (resultSet.getString("last_name") != null ? resultSet.getString("last_name") : ""));
					individual.setStandard(resultSet.getString("standard_id"));
					individual.setDivision(resultSet.getString("division_id"));
					individual.setRole("Student");
					individuals.add(individual);	
				}
				return null;
			}
		});
	}

	private void getIndividualsAsTeacherForNotice(String name, Integer branchId, 
			final List<Integer> allreadySelectedIds, final List<Individual> individuals) {

		String sql = "select u.id as userId, t.firstname, t.lastname from teacher as t, user as u " +
				" where (firstname like '%"+name+"%' OR lastname like '%"+name+"%'" +
				" OR middlename like '%"+name+"%') AND t.user_id = u.id  AND t.branch_id = "+branchId+" " +
				" AND t.active= 'Y' AND t.isDelete = 'N' AND u.active = 'Y'";

		logger.debug("get students for notice search SQL --"+sql);

		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName((resultSet.getString("firstname") != null ? resultSet.getString("firstname")+ " " : "")
							+ (resultSet.getString("lastname") != null ? resultSet.getString("lastname") : ""));
					//individual.setCourse(resultSet.getString("standard_id"));
					individual.setRole("Teacher");
					individuals.add(individual);
				}
				return null;
			}
		});
	}

	
	public List<Individual> getStudentsForEdiarySearch(String name,Integer branchId, final List<Integer> allreadySelectedIds) {
		final List<Individual> individuals = new ArrayList<>();
		String sql = "select u.id as userId, s.* from student as s, user as u where " +
			"(first_name like '%"+name+"%' OR last_name like '%"+name+"%' " +
			"OR father_name like '%"+name+"%'  OR mother_name like '%"+name+"%') " +
			"AND s.user_id = u.id AND s.active = 'Y' AND s.isDelete = 'N' AND u.active = 'Y'" +(
			branchId != null && branchId != 0 ? "AND s.branch_id = "+branchId+"" : "");

		logger.debug("get students for ediary search sql --"+sql);
		
		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName((resultSet.getString("first_name") != null ? resultSet.getString("first_name")+ " " : "")
							+ (resultSet.getString("last_name") != null ? resultSet.getString("last_name") : ""));
					individual.setStandard(resultSet.getString("standard_id"));
					individual.setDivision(resultSet.getString("division_id"));
					individual.setRole("Student");
					individuals.add(individual);	
				}
				return null;
			}
		});
		return individuals;
	}
	
	
	public Individual getIndividualFromStudent(Integer id) {
		String sql = "select st.displayName as standardName,d.displayName as divisionName, user_id," +
					 "s.first_name, s.last_name from student as s LEFT JOIN standard as st on st.id = s.standard_id " +
					 "LEFT JOIN division as d on d.id = s.division_id where s.active = 'Y' AND s.isDelete = 'N' AND user_id = "+id;

		Individual individual = getJdbcTemplate().query(sql , new RowMapper<Individual>(){
			@Override
			public Individual mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Individual individual = new Individual();
				individual.setId(resultSet.getInt("user_id"));
				individual.setName((resultSet.getString("first_name") != null ? resultSet.getString("first_name")+ " " : "")
						+ (resultSet.getString("last_name") != null ? resultSet.getString("last_name") : ""));
				individual.setStandard(resultSet.getString("standardName"));
				individual.setDivision(resultSet.getString("divisionName"));
				individual.setRole("Student");
				return individual;
			}
		}).get(0);
		return individual;
	}


	public Individual getIndividualFromTeacher(Integer id) {
		String sql = "select user_id, firstname, lastname from teacher where active = 'Y' AND isDelete = 'N' AND user_id = "+id;

		List<Individual> individualList = getJdbcTemplate().query(sql , new RowMapper<Individual>() {
			@Override
			public Individual mapRow(ResultSet resultSet, int arg1)throws SQLException {
				Individual individual = new Individual();
				individual.setId(resultSet.getInt("user_id"));
				individual.setName((resultSet.getString("firstname") != null ? resultSet.getString("firstname")+ " " : "")
						+ (resultSet.getString("lastname") != null ? resultSet.getString("lastname") : ""));
				//individual.setCourse(resultSet.getString("standard_id"));
				individual.setRole("Teacher");

				return individual;
			}
		});
		Individual individual = null;
		if(individualList != null && individualList.size() > 0){
			individual = individualList.get(0);	
		}
		return individual;
	}

	
	public Individual getIndividualFromVendor(Integer id) {
		String sql = "select user_id, merchant_legal_name from vendor where active = 'Y' AND user_id = "+id;

		Individual individual = getJdbcTemplate().query(sql, new RowMapper<Individual>(){
			@Override
			public Individual mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Individual individual = new Individual();
				individual.setId(resultSet.getInt("user_id"));
				individual.setName(resultSet.getString("merchant_legal_name"));
				//individual.setCourse(resultSet.getString("standard_id"));
				individual.setRole("Vendor");
				return individual;
			}
		}).get(0);

		return individual;
	}
	
	public List<Individual> getIndividualsGroupByName(String name,Integer instituteId, List<Integer> allreadySelectedIds) {
		List<Individual> individuals = new ArrayList<>();
		
		getIndividualsAsStudentForGroup(name, instituteId, allreadySelectedIds, individuals);
		
		getIndividualsAsTeacherForGroup(name, instituteId, allreadySelectedIds, individuals);

		return individuals;
	}


	private void getIndividualsAsTeacherForGroup(String name, Integer instituteId, 
			final List<Integer> allreadySelectedIds, final List<Individual> individuals) {
		String sql = "select u.id as userId, t.firstname, t.lastname from teacher as t, user as u " +
				" where (firstname like '%"+name+"%' OR lastname like '%"+name+"%'" +
				" OR middlename like '%"+name+"%') AND t.user_id = u.id AND u.active = 'Y' " +
				" AND t.isDelete = 'N' AND t.active = 'Y'";

		logger.debug("individuals for group by name SQL --"+sql);
		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName((resultSet.getString("firstname") != null ? resultSet.getString("firstname")+ " " : "")
							+ (resultSet.getString("lastname") != null ? resultSet.getString("lastname") : ""));
					//individual.setCourse(resultSet.getString("standard_id"));
					individual.setRole("Teacher");
					individuals.add(individual);
				}
				return null;
			}
		});
	}

	private void getIndividualsAsStudentForGroup(String name, Integer instituteId, 
			final List<Integer> allreadySelectedIds, final List<Individual> individuals) {
		String sql = "select u.id as userId, s.first_name, s.last_name from student as s, " +
				"user as u where (first_name like '%"+name+"%' OR last_name like '%"+name+"%'" +
				" OR father_name like '%"+name+"%'  OR mother_name like '%"+name+"%') AND s.user_id = u.id" 
				+( instituteId != null && instituteId != 0 ? "AND s.branch_id = " +
				"(select id from branch where institute_id = "+instituteId+")": "")
				+" AND u.active = 'Y' AND s.active = 'Y'";

		logger.debug("individuals for group by name SQL --"+sql);
		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
					Individual individual = new Individual();
					individual.setId(resultSet.getInt("userId"));
					individual.setName((resultSet.getString("first_name") != null ? resultSet.getString("first_name")+ " " : "")
							+ (resultSet.getString("last_name") != null ? resultSet.getString("last_name") : ""));
					individual.setStandard(resultSet.getString("standard_id"));
					individual.setDivision(resultSet.getString("division_id"));
					individual.setRole("Student");
					individuals.add(individual);
				}
				return null;
			}
			
		});
	}

	/*@Override
	public List<Individual> getIndividualsByNameExceptGroupWhenTeacher(String name, 
			Integer groupId, Integer teacherUserId, final List<Integer> allreadySelectedIds) {

		final List<Individual> individuals = new ArrayList<>();

		String sql = "select standard_id from teacher_standard where teacher_id = (select id from teacher where user_id = "+teacherUserId+")";

		sql = "select c.*, st.displayName as standardName, d.displayName as divisionName, " +
				" r.name as role from standard as st  " +
				" LEFT JOIN teacher_standard as ts on ts.standard_id = st.id AND ts.is_delete = 'N' "+
				" LEFT JOIN student as s on s.standard_id = st.id AND s.isDelete = 'N' AND s.active = 'Y' "+
				" LEFT JOIN division as d on d.id = s.division_id AND d.active = 'Y' "+
				" INNER JOIN contact as c on (c.user_id = (select user_id from teacher where id = ts.teacher_id) OR c.user_id = s.user_id) "+
				" INNER JOIN user_role as ur on ur.user_id = c.user_id" +
				" INNER JOIN role as r on r.id = ur.role_id" +
				" where s.id in (select standard_id from teacher_standard " +
				" where teacher_id = (select id from teacher where user_id = "+teacherUserId+"))" +
				" AND s.active = 'Y' "+
				" AND c.user_id not in (select user_id from group_user where group_id = "+groupId+")";
		
		List<Integer> standardIdList = new ArrayList<>();
		
		getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getInt("standard_id");
			}
		});
		if(standardIdList != null && standardIdList.size() > 0)	{
			String standardIds = org.apache.commons.lang.StringUtils.join(standardIdList, ", ");

			String teacherSql = "select c.* " +
					" from contact as c " +
					" INNER JOIN teacher as t on t.user_id = c.user_id "+
					" INNER JOIN teacher_standard as ts on ts.teacher_id = t.id AND ts.standard_id in ("+standardIds+")" +
					" where c.firstname like '%"+name+"%' OR c.lastname like '%"+name+"%'";
		

			System.out.println("GETTING INDIVIDUALS FOR TEACHER LOGIN ::::: \n SQL ::"+teacherSql);

			getJdbcTemplate().query(teacherSql, new RowMapper<Individual>(){
				@Override
				public Individual mapRow(ResultSet resultSet, int arg1)throws SQLException {
					if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
						Individual individual = new Individual();
						individual.setId(resultSet.getInt("userId"));

						String firstName = resultSet.getString("firstname") ;
						String lastName = resultSet.getString("lastname") ;
						String name = (StringUtils.isEmpty(firstName) ? "" : firstName + " ") + (StringUtils.isEmpty(lastName) ? "" : " "+lastName);

						individual.setName(name);
						individual.setStandard(resultSet.getString("standardName"));
						individual.setDivision(resultSet.getString("divisionName"));
						individual.setRole(Constants.ROLE_TEACHER);

						individuals.add(individual);
					}
					return null;
				}
			});

			String studentSql = "select c.* " +
					" from contact as c " +
					" INNER JOIN student as s on s.user_id = c.user_id "+
					" where s.standard_id in ("+standardIds+") " +
					" AND c.firstname like '%"+name+"%' OR c.lastname like '%"+name+"%'";

			System.out.println("GETTING INDIVIDUALS FOR TEACHER LOGIN ::::: \n SQL ::"+studentSql);

			getJdbcTemplate().query(studentSql, new RowMapper<Individual>(){
				@Override
				public Individual mapRow(ResultSet resultSet, int arg1)throws SQLException {
					if(!allreadySelectedIds.contains(resultSet.getInt("userId"))){
						Individual individual = new Individual();
						individual.setId(resultSet.getInt("userId"));

						String firstName = resultSet.getString("firstname") ;
						String lastName = resultSet.getString("lastname") ;
						String name = (StringUtils.isEmpty(firstName) ? "" : firstName + " ") + (StringUtils.isEmpty(lastName) ? "" : " "+lastName);

						individual.setName(name);
						individual.setStandard(resultSet.getString("standardName"));
						individual.setDivision(resultSet.getString("divisionName"));
						individual.setRole(Constants.ROLE_STUDENT);

						individuals.add(individual);
					}
					return null;
				}
			});
		}
		return null;
	}*/
}
