package com.qfix.daoimpl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.dao.IWorkingDayDao;
import com.qfix.dao.TimetableInterfaceDao;
import com.qfix.model.Lecture;
import com.qfix.model.TimeTableDetail;
import com.qfix.model.TimeTableEntry;
import com.qfix.model.Timetable;
import com.qfix.model.TimetableData;
import com.qfix.model.TimetableModel;
import com.qfix.model.ValidationError;
import com.qfix.model.WorkingDay;
import com.qfix.service.user.UrlConfiguration;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.TimetableValidator;

@Repository
public class TimetableDao extends JdbcDaoSupport implements TimetableInterfaceDao{
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(TimetableDao.class);

	private List<ValidationError> validationErrors;

	@Autowired
	TimetableValidator timetableValidator;
	
	@Autowired
	IWorkingDayDao workingDayDao;
	

	public List<ValidationError> getValidationErrors() {
		return validationErrors;
	}

	private void setValidationErrors(List<ValidationError> validationErrors) {
		this.validationErrors = validationErrors;
	}

	@Override
	public List<Timetable> getAll(Integer branchId, Integer standardId, Integer divisionId)
			throws SQLException {
		// TODO Auto-generated method stub
		String timetableSearchSql = "select t.id as id, st.displayName as standard, GROUP_CONCAT(d.displayName SEPARATOR ', ')  as divisions "+
			 " FROM timetable t "+ 
			 " INNER JOIN standard as st on st.id = t.standard_id "+
			 " INNER JOIN branch as b on b.id = st.branch_id "+
			" LEFT OUTER JOIN timetable_division as td on td.timetable_id = t.id "+
			" LEFT OUTER JOIN division as d on d.id = td.division_id "+
			" where t.is_delete = 'N' and td.is_delete = 'N' and b.id = "+branchId +
			" AND st.academic_year_id = (select id from academic_year where branch_id = b.id AND is_current_active_year = 'Y') ";
			if(standardId != null && standardId > 0) {
				timetableSearchSql = timetableSearchSql + " and st.id = "+standardId;
			}
			if(divisionId != null && divisionId > 0){
				timetableSearchSql = timetableSearchSql + " and td.division_id = "+divisionId;
			}
			timetableSearchSql = timetableSearchSql + " GROUP BY  t.standard_id, t.id order by st.displayName";
			
			List<Timetable> timetables = getJdbcTemplate().query(timetableSearchSql, new RowMapper<Timetable>(){
				@Override
				public Timetable mapRow(ResultSet resultSet, int arg1)
						throws SQLException {
					
					Timetable timetable = new Timetable();
					timetable.setId(resultSet.getInt("id"));
					timetable.setStandardName(resultSet.getString("standard"));
					timetable.setDivisionNames(resultSet.getString("divisions"));
					return timetable;
					
				}
			});
			return timetables;
	}

	@Override
	public Timetable getTimetableById(final Integer id) {
		String sql = "select t.*, i.id as instituteId, b.id as branchId, s.academic_year_id as academic_year_id from timetable as t, standard s, branch as b, institute as i " +
				"where t.is_delete = 'N'  and t.standard_id = s.id and s.branch_id = b.id AND b.institute_id = i.id AND t.id = "+id;
		
		List<Timetable> timetableList = getJdbcTemplate().query(sql, new RowMapper<Timetable>() {
			public Timetable mapRow(ResultSet resultSet, int rownumber)throws SQLException {
				
				Timetable timetable = new Timetable();
				timetable.setId(resultSet.getInt("id"));
				timetable.setInstituteId(resultSet.getInt("instituteId"));
				timetable.setBranchId(resultSet.getInt("branchId"));
				timetable.setStandardId(resultSet.getInt("standard_id"));
				timetable.setData(resultSet.getString("json"));
				timetable.setAcademicYearId(resultSet.getInt("academic_year_id"));

				String divList = "select division_id from timetable_division where is_delete='N' and timetable_id = "+id;
				
				List<Integer> divisionIDList = getJdbcTemplate().query(divList, new RowMapper<Integer>(){
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						// TODO Auto-generated method stub
						return resultSet.getInt(1);
					}
				});
				
				timetable.setDivisionIdList(divisionIDList);
				
				return timetable;
		}});
		if(timetableList.size() > 0) {
			return timetableList.get(0);
		} else {
			return null;
		}
		
	}

	@Override
	public boolean deleteTimeTable(Integer id, String token) throws Exception {
		Integer branchId = getBranchIdByTimetableId(id);
		boolean flag = false;
		if(branchId != null && branchId != 0 ){
			updateTimeTableToService("", id, getBranchIdByTimetableId(id), token, HttpMethod.DELETE);
			flag = true;
		}
		else {
			flag = false;
		}
		return flag;
	}

	private Integer getBranchIdByTimetableId(Integer id) {
		String sql = "select branch_id from standard where id = (select standard_id from timetable where id = "+id+")";
		Integer branchId = getJdbcTemplate().queryForObject(sql, Integer.class);
		return branchId;
	}


	@Override
	@Transactional
	public boolean insertTimetable(final Timetable timetable, String token) throws Exception {
		prepareTimeTableData(timetable, token, HttpMethod.POST);
		return true;
	}


	private void prepareTimeTableData(Timetable timetable, String token, HttpMethod method) throws Exception{
		
		ObjectMapper mapper = new ObjectMapper();
		System.out.println("::::::::::::::::\n"+timetable);
		System.out.println("::::::::::::\n"+timetable.getData());
		
		
		TimetableData timeTableData = mapper.readValue(timetable.getData(), TimetableData.class);

		Map<String, List<TimeTableEntry>> detailMap = new HashMap<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		System.out.println("Time table data :::: \n"+mapper.writeValueAsString(timeTableData));

		for(TimeTableDetail timeTableDetail : timeTableData.getTimeTableDetailList()){
			Calendar calendar = Calendar.getInstance();

			String start = timeTableDetail.getStart();
			start = start.substring(0, start.lastIndexOf("."));
			start = start.replaceAll("T", " ");
			// Now start has value in format of yyyy-MM-dd HH:mm:ss then  create date object to get timetable date.
			Date date = dateFormat.parse(start);

			start = start.substring(start.indexOf(" ") + 1, start.length());
			start = start.substring(0, start.lastIndexOf(":"));

			String end = timeTableDetail.getEnd();
			end = end.substring(0, end.lastIndexOf("."));
			end = end.replaceAll("T", " ");
			end = end.substring(end.indexOf(" ") + 1, end.length());
			end = end.substring(0, end.lastIndexOf(":"));

			calendar.setTime(date);

			int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
			String key = "D"+(weekDay == 1 ? 7 : weekDay-1);

			TimeTableEntry tableEntry = new TimeTableEntry(timeTableDetail.getTitle(), start, end);

			List<TimeTableEntry> tableEntries = (detailMap.containsKey(key) ? detailMap.get(key) : new ArrayList<TimeTableEntry>());
			tableEntries.add(tableEntry);

			detailMap.put(key, tableEntries);
		}

		TimetableModel model = new TimetableModel();
		model.setDivision(timetable.getDivisionIdList());
		model.setJsonPayload(timetable.getData());
		model.setStandardId(timetable.getStandardId());
		model.setDetail(detailMap);
		model.setId(timetable.getId());

		model.setStartDate(timeTableData.getStartDate());
		model.setEndDate(timeTableData.getEndDate());

		String json = mapper.writeValueAsString(model);
		updateTimeTableToService(json, timetable.getId(), timetable.getBranchId(), token, method);
	}


	private void updateTimeTableToService(String body, Integer timeTableId, Integer branchId, String token, HttpMethod method) throws Exception {

		String url = UrlConfiguration.UPDATE_TIMETABLE_SERVICE_DETAILS_URL+ branchId + "/timetable";

		url = (method == HttpMethod.PUT || method == HttpMethod.DELETE ) ? url + "/"+ timeTableId : url;

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    List<MediaType> acceptableMediaTypes = new ArrayList<>();
	    acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
	    headers.setAccept(acceptableMediaTypes);
	    headers.add("token", token);

	    HttpEntity<String> httpEntity = new HttpEntity<String>(body, headers);
	    RestTemplate restTemplate = new RestTemplate();

	    ResponseEntity<Object> responseEntity = restTemplate.exchange(url, method, httpEntity,Object.class);

	    logger.debug("SUBMIT TIME-TABLE RESPONSE ::: "+responseEntity);
	    if(responseEntity.getStatusCode().equals(HttpStatus.CREATED)){
	    	logger.debug("Time table submitted to user-service....");
	    }
	}


	@Transactional
	public void insertTimetableDivision(final int timetableId, List<Integer> divisionList) throws Exception {

		for(final Integer divisionId : divisionList) {
			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection)
						throws SQLException {
					String sql = "insert into timetable_division (timetable_id, division_id)"
									+" values(?, ?)";

					PreparedStatement preparedStatement = connection.prepareStatement(sql);
					preparedStatement.setInt(1, timetableId);
					preparedStatement.setInt(2, divisionId);
					return preparedStatement;
				}
			});
		}
	}


	@Override
	public boolean updateTimetable(Timetable timetable, String token) throws Exception {
		prepareTimeTableData(timetable, token, HttpMethod.PUT);
		return true;
	}
	
	
	@Transactional
	@Override
	public void uploadTimetable(MultipartFile file, Timetable timetable,
			String token) throws Exception {

		String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		if(fileExtension .equalsIgnoreCase(".xls") || fileExtension .equalsIgnoreCase(".xlsx")){

			int day = 11;
			int year = 2016;
			int month = 9;
			String startDateFormat = null ;
			String endDateFormat = null;
			String createStartTime = "";
			String createEndTime = "";
			Date startTime;
			Date endTime;
			String lectureDate = year+"-0"+month+"-"+day; 
			Date startDate = null;
			Date endDate = null;
			XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
			XSSFSheet Sheet = workbook.getSheetAt(0);
			XSSFRow row;
			Iterator <Row> rowIterator = Sheet.iterator();
			List <Lecture> lectureList = new ArrayList<Lecture>();
			List<Integer> workingDaysList = getWorkingDays(timetable.getBranchId());
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

			nextRow : while(rowIterator.hasNext())						//nextRow is for avoid blank cell
			{

				int first = 1, second = 2, third = 3, fourth = 4, fifth = 5, increment = 5;
				row = (XSSFRow) rowIterator.next();
				if (row.getRowNum() > 5) {
					lectureDate = year + "-" + month + "-" + (++day);
					if (workingDaysList.contains(row.getRowNum() - 5)) // do note apply date for 0,1,2,3,4 row
					{

						Iterator<Cell> cellIterator = row.iterator();
						Lecture lecture = new Lecture();
						while (cellIterator.hasNext()) {

							Cell cell = cellIterator.next();
							if (cell.getColumnIndex() == first)
								lecture = new Lecture();

							switch (cell.getCellType()) {

							case Cell.CELL_TYPE_BLANK:
								continue nextRow;

							case Cell.CELL_TYPE_STRING:
								if (cell.getColumnIndex() == first) {
									lectureDate += " "
											+ cell.getStringCellValue();
									first = first + increment;
								} else if (cell.getColumnIndex() == second) {
									lectureDate += ":"
											+ cell.getStringCellValue() + ":00";
									Date date = new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss")
											.parse(lectureDate); // seeting date
																	// format
									lecture.setStart(date);
									System.out.println("Str :: " + lectureDate);
									lectureDate = year + "-" + month + "-"
											+ day;
									second += increment;
									System.out.println(date);
								} else if (cell.getColumnIndex() == third) {
									lectureDate += " "
											+ cell.getStringCellValue();
									third += increment;
								} else if (cell.getColumnIndex() == fourth) {
									lectureDate += ":"
											+ cell.getStringCellValue()
											+ ":00.0";
									java.util.Date date = new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss.S")
											.parse(lectureDate);
									lecture.setEnd(date);
									lectureDate = year + "-" + month + "-"
											+ day;
									fourth += increment;
								}

								else if (cell.getColumnIndex() == fifth
										&& cell.getRowIndex() != 1) {
									lecture.setTitle(cell.getStringCellValue());
									fifth += increment;
								}
								break;

							case Cell.CELL_TYPE_NUMERIC:

								if (cell.getColumnIndex() == first) {
									Double cellvalue = cell
											.getNumericCellValue();
									lectureDate += " "
											+ (cellvalue.intValue() < 10 ? "0"
													+ cellvalue.intValue()
													: cellvalue.intValue());
									first = first + increment;
								} else if (cell.getColumnIndex() == second) {
									Double cellvalue = cell
											.getNumericCellValue();
									lectureDate += ":"
											+ (cellvalue.intValue() < 10 ? "0"
													+ cellvalue.intValue()
													: cellvalue.intValue())
											+ ":00";
									Date date = new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss")
											.parse(lectureDate); // seeting date
																	// format
									lecture.setStart(date);
									System.out.println("Str :: " + lectureDate);
									lectureDate = year + "-" + month + "-"
											+ day;
									second += increment;
								} else if (cell.getColumnIndex() == third) {
									Double cellvalue = cell
											.getNumericCellValue();
									lectureDate += " " + cellvalue.intValue();
									third += increment;
								} else if (cell.getColumnIndex() == fourth) {
									Double cellvalue = cell
											.getNumericCellValue();
									lectureDate += ":" + cellvalue.intValue()
											+ ":00.0";
									java.util.Date date = new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss.S")
											.parse(lectureDate);
									lecture.setEnd(date);
									lectureDate = year + "-" + month + "-"
											+ day;
									fourth += increment;
								}
								break;

							case Cell.CELL_TYPE_ERROR:
								break;
							}
							if (lecture.getTitle() != null) // avoid other cells
							{
								lectureList.add(lecture);
							}
						}
					}
				}
				Iterator<Cell> cellIterator = row.iterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_BLANK:
						continue nextRow;

					case Cell.CELL_TYPE_NUMERIC:
						break;
					case Cell.CELL_TYPE_STRING:
						System.out.println("This is cell value - "
								+ cell.getStringCellValue()
								+ "On this cell row ->" + cell.getRowIndex()
								+ "on this column--->" + cell.getColumnIndex());
						if (cell.getRowIndex() == second
								&& cell.getColumnIndex() == first) {
							// Double cellvalue = cell.getStringCellValue();
							// startDateFormat.setTime(cellvalue.intValue());
							createStartTime = cell.getStringCellValue();
							System.out
									.println("The start Time in hour is ---->"
											+ createStartTime);
						} else if (cell.getRowIndex() == third
								&& cell.getColumnIndex() == first) {
							// Double cellvalue = cell.getNumericCellValue();
							createEndTime += cell.getStringCellValue();
							System.out
									.println("This is endTime in hour ---------->"
											+ createEndTime);
						}
						if (cell.getRowIndex() == 0
								&& cell.getColumnIndex() == first) {
							startDateFormat = cell.getStringCellValue();
						} else if (cell.getRowIndex() == first
								&& cell.getColumnIndex() == first) {
							endDateFormat = cell.getStringCellValue();
						}
						break;
					}
				}
			}

			startTime = timeFormat.parse(createStartTime);
			endTime = timeFormat.parse(createEndTime);
			startDate = Constants.USER_DATE_FORMAT.parse(startDateFormat);
			endDate = Constants.USER_DATE_FORMAT.parse(endDateFormat);

			if(lectureList != null && lectureList.size() > 0){
				StringBuffer tempBuffer = new StringBuffer("{\"calendarData\":[");
				for(Lecture lecture: lectureList) {
					String temp = lecture.getLectureAsJsonString();
					tempBuffer.append(temp).append(StringUtils.isNotBlank(temp) ? ",":"");
				}
				String str = tempBuffer.substring(0, (tempBuffer.length() -1))+"],";

				List<ValidationError> validationErrors = validateLectures(timetable, lectureList, token, startDate, endDate, startTime, endTime);
				if(validationErrors.size() > 0){
					setValidationErrors(validationErrors);
					throw new IOException("Some the data is incorrect!");
				}

				StringBuffer displayText = new StringBuffer("");
				displayText.append(str);
				getNonWorkingDay(displayText,timetable);
				System.out.println(displayText);
				appendTimetableDuration(displayText, createStartTime, createEndTime, dateFormat.format(startDate), dateFormat.format(endDate));
				System.out.println("Display Text:::::::::"+displayText);
				timetable.setData(displayText.toString());
				logger.debug("uploaded Student reports --"+lectureList);
				insertTimetable(timetable, token);	
			}
			else {
				logger.error("Empty excel without student reports Ignoring...");
				throw new IOException("Please provide valid student reports.");
			}
		}
		else {
			logger.error("File is not excel...");
			throw new IOException("Please provide only excel sheet.");
		}
	}
	
	private void appendTimetableDuration(StringBuffer data, String createStartTime, String createEndTime, String startDateFormat, String endDateFormat ) {
		data.append("\"startDate\": \""+startDateFormat+"\",");
		data.append("\"endDate\": \""+endDateFormat+"\",");
		data.append("\"startTime\": \""+createStartTime+"\",");
		data.append("\"endTime\": \""+createEndTime+"\"}");
		
	}

	//Method to create list of NonWorking Days
	private void getNonWorkingDay(StringBuffer displayText, Timetable timetable)
	{
		
		StringBuffer nonWorkingDays = new StringBuffer("\"nonWorkingDays\":[");
		StringBuffer dayNamesForSelectBox = new StringBuffer("\"dayNamesForSelectBox\":[");
		String noFlag = "N";
		WorkingDay workingDay = workingDayDao.getCurrentWorkingDayForBranch(timetable.getBranchId());
		boolean skipOnHolidayText = false;
		if(workingDay.getSkipTimetableOnHoliday() != null && workingDay.getSkipTimetableOnHoliday().equals("Y")) {
			skipOnHolidayText = true;
		}
		
		if(workingDay.getMonday().equals("N")){
			nonWorkingDays.append("1,");
		}
		else{
			dayNamesForSelectBox.append("{\"value\":\"2016-09-12\","+"\"text\":\""+ (skipOnHolidayText ? "Day1" : "Mon")+"\"},");
		}
		if(workingDay.getTuesday().equals(noFlag)){
			nonWorkingDays.append("2,");
		}
		else{
			dayNamesForSelectBox.append("{\"value\":\"2016-09-13\","+"\"text\":\""+ (skipOnHolidayText ? "Day2" : "Tue")+"\"},");
		}
		if(workingDay.getWednesday().equals(noFlag)){
			nonWorkingDays.append("3,");
		}
		else{
			dayNamesForSelectBox.append("{\"value\":\"2016-09-14\","+"\"text\":\""+ (skipOnHolidayText ? "Day3" : "Wed")+"\"},");
		}
		if(workingDay.getThursday().equals(noFlag)){
			nonWorkingDays.append("4,");
		}
		else{
			dayNamesForSelectBox.append("{\"value\":\"2016-09-15\","+"\"text\":\""+ (skipOnHolidayText ? "Day4" : "Thu")+"\"},");
		}
		if(workingDay.getFriday().equals(noFlag)){
			nonWorkingDays.append("5,");
		}
		else{
			dayNamesForSelectBox.append("{\"value\":\"2016-09-16\","+"\"text\":\""+ (skipOnHolidayText ? "Day5" : "Fri")+"\"},");
		}
		if(workingDay.getSaturday().equals(noFlag)){
			nonWorkingDays.append("6,");
		}
		else{
			dayNamesForSelectBox.append("{\"value\":\"2016-09-17\","+"\"text\":\""+ (skipOnHolidayText ? "Day6" : "Sat")+"\"},");
		}
		if(workingDay.getSunday().equals(noFlag)){
			nonWorkingDays.append("0,");
		}
		else{

			dayNamesForSelectBox.append("{\"value\":\"2016-09-18\","+"\"text\":\""+ (skipOnHolidayText ? "Day7" : "Sun")+"\"},");
		}
		displayText.append(nonWorkingDays.substring(0, (nonWorkingDays.length() -1)));
		displayText.append("],");
		displayText.append(dayNamesForSelectBox.substring(0, (dayNamesForSelectBox.length() -1)));
		displayText.append("],");
		displayText.append("\"isSkipTimetableOnHoliday\":"+skipOnHolidayText+",");
	}
	/*	private void insertUploadedTimetable() {
		
	}
	*/
	private List<ValidationError> validateLectures(Timetable timetable, List<Lecture>  lectures, String token, Date startDate, Date endDate, Date startTime, Date endTime) {
		int row = 1;
		timetableValidator.initData(timetable, lectures, startDate, endDate, startTime, endTime);
		//timetableValidator.checkStartandEndDateTime(startDate, endDate, lectures);
		List<ValidationError> validationList = new ArrayList<>();
		
		for(Lecture lecture : lectures){
			ValidationError validationError = ValidatorUtil.validateForAllErrors(lecture, timetableValidator);
			if(validationError != null){
				validationError.setRow("RECORD : "+row);
				validationError.setTitle("Subject - "+lecture.getTitle());
				validationList.add(validationError);	
			}
			row ++;
		}
		return validationList;
	}
	
	private List<Integer> getWorkingDays(Integer branchId) {
		List<Integer> workingDayList = new ArrayList<Integer>();
		String workginFlag = "Y";
		WorkingDay workingDay = workingDayDao.getCurrentWorkingDayForBranch(branchId);
		try {
			if(workingDay.getMonday().equalsIgnoreCase(workginFlag)) {
				workingDayList.add(1);
			}
			
			if(workingDay.getTuesday().equalsIgnoreCase(workginFlag)) {
				workingDayList.add(2);
			}
			if(workingDay.getWednesday().equalsIgnoreCase(workginFlag)) {
				workingDayList.add(3);
			}
			if(workingDay.getThursday().equalsIgnoreCase(workginFlag)) {
				workingDayList.add(4);
			}
			if(workingDay.getFriday().equalsIgnoreCase(workginFlag)) {
				workingDayList.add(5);
			}
			if(workingDay.getSaturday().equalsIgnoreCase(workginFlag)) {
				workingDayList.add(6);
			}
			if(workingDay.getSunday().equalsIgnoreCase(workginFlag)) {
				workingDayList.add(7);
			}
		}catch(Exception e){
			logger.error("Working days Not found...", e);
		}
		
		return workingDayList;
	}
}
