package com.qfix.daoimpl;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;
import com.qfix.dao.FeesInterfaceDao;
import com.qfix.dao.HeadInterfaceDao;
import com.qfix.dao.IBranchDao;
import com.qfix.exceptions.ApplicationException;
import com.qfix.exceptions.FeeStructureAlreadyExistException;
import com.qfix.model.AuditLog;
import com.qfix.model.DisplayTemplate;
import com.qfix.model.FeePayment;
import com.qfix.model.Fees;
import com.qfix.model.FeesCode;
import com.qfix.model.FeesDescription;
import com.qfix.model.FeesRecurringDetail;
import com.qfix.model.FeesReport;
import com.qfix.model.FeesReportFilter;
import com.qfix.model.IncorrectTransactionIdReport;
import com.qfix.model.LateFeesDetail;
import com.qfix.model.LateFeesPayment;
import com.qfix.model.PaymentTransaction;
import com.qfix.model.RefundConfig;
import com.qfix.model.SchemeCode;
import com.qfix.model.Student;
import com.qfix.model.StudentParent;
import com.qfix.model.TableOptions;
import com.qfix.model.TableResponse;
import com.qfix.model.TransactionReport;
import com.qfix.service.user.UrlConfiguration;
import com.qfix.utilities.Constants;
import com.qfix.utilities.Query;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Repository
public class FeesDao extends JdbcDaoSupport implements FeesInterfaceDao {

	@Autowired
	private DriverManagerDataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	HeadInterfaceDao headDao;
	
	@Autowired
	IBranchDao branchDao;
	
	@Autowired
	private Gson gson;

	final static Logger logger = Logger.getLogger(FeesDao.class);

	private static String GET_STUDENT_APPLICABLE_FOR_FEES = " select s.user_id as studentId from fees as f "
			+ " INNER JOIN student as s on s.branch_id = f.branch_id "
			+ " LEFT JOIN feescode_student as fs on fs.student_id = s.id " 
			+ " where s.isDelete = 'N' AND f.is_delete = 'N' "
			+ " AND  (f.caste_id = s.caste_id or (f.caste_id is NULL)) "
			+ " AND (fs.fees_code_configuration_id in (f.fees_code_configuration_id)  OR f.fees_code_configuration_id is NULL) "
			+ " AND ((f.standard_id = s.standard_id OR f.standard_id is null) AND (f.division_id IS NULL or f.division_id=s.division_id) AND (f.student_id = s.id OR f.student_id is null)) ";

	@Override
	@Transactional
	public List<Fees> getAll(Integer instituteId) {
		String sql = "select f.*, s.displayName as standardName, concat(ifnull(stud.firstname, ''), ' ' ,ifnull(stud.surname, '')) as studentName, "
				+ " c.name as caste, c.id as casteId, h.id as headId, h.name as head,b.id as branchId, "
				+ " b.name as branch from fees as f, caste as c, head as h, standard as s, student as stud "
				+ " branch as b where f.standard_id = s.id AND f.branch_id = b.id AND f.caste_id = c.id "
				+ " AND f.head_id = h.id "
				+ " AND (f.student_id is NULL OR f.student_id = stud.id) ";

		if (instituteId != null && instituteId != 0) {
			sql = sql + " AND b.institute_id = " + instituteId;
		}

		logger.debug("Get all fees SQL --" + sql);
		return getJdbcTemplate().query(sql, new RowMapper<Fees>() {
			public Fees mapRow(ResultSet rs, int rownumber) throws SQLException {

				Fees fees = new Fees();
				fees.setId(rs.getInt("id"));
				fees.setBranch(rs.getString("branch"));
				fees.setCaste(rs.getString("caste"));
				fees.setHead(rs.getString("head"));
				fees.setBranchId(rs.getInt("branch_id"));
				fees.setHeadId(rs.getInt("headId"));
				fees.setCasteId(rs.getInt("casteId"));
				fees.setAmount(rs.getString("amount"));
				fees.setFromDate(rs.getString("fromdate"));
				fees.setToDate(rs.getString("todate"));
				fees.setDueDate(rs.getString("duedate"));
				fees.setStandardId(rs.getInt("standard_id"));
				fees.setStandard(rs.getString("standardName"));
				fees.setLatePaymentCharges(rs.getInt("late_payment_charges"));
				fees.setStudentName(rs.getString("studentName"));
				return fees;
			}
		});
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public boolean uploadFees(List<Fees> feesList, Integer currentLoggedInUserId)
			throws FeeStructureAlreadyExistException {
		boolean flag = true;

		for (Fees fees : feesList) {
			insertFeesOneByOne(fees, currentLoggedInUserId);
		}
		return flag;
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public boolean uploadUpdateFees(Map<String, List<Fees>> feesMap, Integer currentLoggedInUserId)
			throws FeeStructureAlreadyExistException {
		boolean flag = true;
		List<Fees> feesListToAdd = feesMap.get("Add");
		List<Fees> feesListToUpdate = feesMap.get("Update");
		List<Fees> feesListToDelete = feesMap.get("Delete");

		for (Fees fees : feesListToAdd) {
			insertFeesOneByOne(fees, currentLoggedInUserId);
		}

		for (Fees fees : feesListToUpdate) {
			updateFeesOneByOne(fees, currentLoggedInUserId);
		}

		for (Fees fees : feesListToDelete) {
			deleteFeeOneByOne(fees.getId());
		}

		return flag;
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public boolean updateFees(Fees fees, Integer currentLoggedInUserId) {
		return updateFeesOneByOne(fees, currentLoggedInUserId);
	}

	private boolean updateFeesOneByOne(Fees fees, Integer currentLoggedInUserId) {
		boolean flag = false;
		String sql = "update fees set head_id = ?, description = ?, amount = ?, branch_id = ?, duedate = ?, "
				+ "fromdate = ?, todate = ?, caste_id = ?, standard_id = ?, isemail = ?,issms = ?, "
				+ "isnotification = ?, frequency = ?, academic_year_id = ?, "
				+ " division_id = ?, student_id = ?, is_split_payment = ?, is_partial_payment_allowed = ?, "
				+ " fees_code_configuration_id = ?, "
				+ " recurring = ?, recurring_frequency = ?, by_days = ?, late_fee_detail_id = ?, "
				+ " reminder_start_date = ?, fee_type = ?, by_date = ?, updated_at = ?, updated_by = ?, display_template_id = ?, "
				+ " currency=?, allow_user_entered_amount = ?, allow_repeat_entries = ? where id = ?";

		Fees previousFees = getFeeById(fees.getId());
		Date currentDate = new Date();

		FeesRecurringDetail recurringDetail = fees.getRecurringDetail();
		Object[] params = {
				fees.getHeadId(),
				fees.getDescription(),
				fees.getAmount(),
				fees.getBranchId(),
				fees.getDueDate(),
				fees.getFromDate(),
				fees.getToDate(),
				(fees.getCasteId() == null || fees.getCasteId() == 0 ? null : fees.getCasteId()),
				fees.getStandardId(),
				(StringUtils.isEmpty(fees.getIsEmail()) ? "N" : fees.getIsEmail()),
				(StringUtils.isEmpty(fees.getIsSms()) ? "N" : fees.getIsSms()),
				(StringUtils.isEmpty(fees.getIsNotification()) ? "N" : fees.getIsNotification()),
				(StringUtils.isEmpty(fees.getFrequency()) ? null : fees.getFrequency()),
				fees.getAcademicYearId(),
				(fees.getDivisionId() == null || fees.getDivisionId() == 0 ? null : fees.getDivisionId()),
				(fees.getStudentId() == null || fees.getStudentId() == 0 ? null : fees.getStudentId()),
				fees.getIsSplitPayment(), fees.getIsPartialPaymentAllowed(), 
				(fees.getFeesCodeId() == null || fees.getFeesCodeId() == 0 ? null : fees.getFeesCodeId()),
				fees.getRecurring(),
				recurringDetail != null ? fees.getRecurringDetail().getFrequency() : Types.NULL,
				recurringDetail != null ? fees.getRecurringDetail().getWorkWeeks() : Types.NULL,
				fees.getLateFeeDetailId(),
				(StringUtils.isEmpty(fees.getReminderStartDate()) ? null : fees.getReminderStartDate()),
				fees.getFeeType(),
				recurringDetail != null ? fees.getRecurringDetail().getByDate() : Types.NULL,
				(Constants.DATABASE_DATE_FORMAT.format(currentDate) + " " + Constants.DATABASE_TIME_FORMAT.format(currentDate)),
				currentLoggedInUserId, (fees.getDisplayTemplateId() == null || fees.getDisplayTemplateId() == 0 ? null:fees.getDisplayTemplateId()), 
				fees.getCurrency(), (Double.parseDouble(fees.getAmount()) == 0 ? "Y" : "N"), 
				fees.getAllowRepeatEntries(),
				fees.getId() };

		int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
				Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
				Types.INTEGER, Types.CHAR, Types.CHAR, Types.INTEGER,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
				StringUtils.isEmpty(fees.getReminderStartDate()) ? Types.NULL : Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.DATE, Types.INTEGER,
				Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER };

		getJdbcTemplate().update(sql, params, types);
		System.out.println("Fees updated...");
		updateFeesWhenSplitPayment(fees);

//		updateFeesDisplayLayouts(fees, previousFees);

		udpateFeesRepeatSchedule(fees);
		updateFeesSchedule(previousFees, fees);
		flag = true;
		return flag;
	}

	private void udpateFeesRepeatSchedule(final Fees fees) {
		String sql= "delete from fees_repeat_schedule where fees_id ="+fees.getId()+" AND repeat_date > now()";
		getJdbcTemplate().update(sql);

		sql = "select count(*) from fees_repeat_schedule where fees_id = " + fees.getId();
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);

		if(count > 0 && "Y".equals(fees.getRecurring())){
			sql = "select max(repeat_date) as repeat_date, max(due_date) as due_date," +
					" max(reminder_start_date) as reminder_start_date " +
					" from fees_repeat_schedule where fees_id = " + fees.getId();
			getJdbcTemplate().query(sql, new RowMapper<Object>(){
				@Override
				public Object mapRow(ResultSet rs, int arg1)
						throws SQLException {
					prepareFeesRepeatSchedule(fees, rs.getDate("repeat_date"), fees.getToDateD(), rs.getDate("due_date"), rs.getDate("reminder_start_date"));
					return null;
				}
			});
		}
		else if("Y".equals(fees.getRecurring())){
			prepareFeesRepeatSchedule(fees, fees.getFromDateD(), fees.getToDateD(),
					fees.getDueDateD(), fees.getReminderStartDateD());
		}
	}

	private void updateFeesSchedule(Fees previousFees, Fees fees) {
		// check whether users changed.
		boolean isUserChanged = isUsersChanged(previousFees, fees);
		if (!previousFees.getAmount().equals(fees.getAmount())) {
			// amount changed do update in fees_schedule
			updateFeesScheduleWhenAmountChanged(fees.getId(),
					previousFees.getAmount(), fees.getAmount());

		}
		if (isUserChanged || !previousFees.getToDate().equals(fees.getToDate())){
			// students changed Delete fees which are unpaid and mark deleted
			// for partial paid.
			List<Integer> studentUserIds = getStudentsApllicableForFees(fees
					.getId());
			deleteUnpaidFromFeesSchedule(fees.getId(), studentUserIds);
			markDeletePartialPaidFromFeesSchedule(fees.getId(), studentUserIds);
			insertFeesSchedule(fees, true);
		}

		if(!"Y".equals(fees.getRecurring())){
			if(previousFees.getReminderStartDate() == null && fees.getReminderStartDate() != null){
				String sql = "update fees_schedule set reminder_start_date = '"+fees.getReminderStartDate()+"' where fees_id = " + fees.getId();
				getJdbcTemplate().update(sql);
			}
			else if(previousFees.getReminderStartDate() != null && fees.getReminderStartDate() == null){
				String sql = "update fees_schedule set reminder_start_date = null where fees_id = " + fees.getId();
				getJdbcTemplate().update(sql);
			}
		}

		// change late payment charges in case user has changed the due-date after previous due date.(if fee is not repeated).

		// Check fees is repeated.
		String sql = "select count(*) as count from fees_repeat_schedule where repeat_date < now() AND fees_id = " + fees.getId();
		Integer count  = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count == 0){
			// set late payment charges as 0 where status is UNPAID.
 			sql = "update fees_schedule set late_payment_charges = 0, due_date = '"+fees.getDueDate()+"' where fees_id = " + fees.getId() + 
					" AND status = '"+Constants.PAYMENT_STATUS.UNPAID.get()+"'";
			getJdbcTemplate().update(sql);

			// Delete from fees_late_payment_schedule which is unpaid entry
			sql = "delete from fees_late_payment_schedule where parent_fees_schedule_id in (select id from fees_schedule  where fees_id = " + fees.getId() + 
					" AND status = '"+Constants.PAYMENT_STATUS.UNPAID.get()+"')";
			getJdbcTemplate().update(sql);
		}
	}

	private void updateFeesScheduleWhenAmountChanged(Integer id,
			String previousAmount, final String newAmount) {
		String sql = "select max(id) as id, total_amount from fees_schedule "
				+ " where fees_id = " + id + " " + "AND status != '"
				+ Constants.PAYMENT_STATUS.PAID.get() + "' GROUP BY user_id ";

		final double ammountDiff = Double.parseDouble(newAmount)
				- Double.parseDouble(previousAmount);

		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {

				double totalAmount = rs.getDouble("total_amount");
				long id = rs.getLong("id");
				String sql = "update fees_schedule set total_amount = "
						+ (totalAmount + ammountDiff) + ", base_amount = "
						+ newAmount + " WHERE id = " + id;
				getJdbcTemplate().update(sql);

				return null;
			}
		});
	}

	private void markDeletePartialPaidFromFeesSchedule(Integer feeId,
			List<Integer> excludeUserIds) {
		String userIds = null;
		if (excludeUserIds != null && !excludeUserIds.isEmpty()) {
			userIds = StringUtils.join(excludeUserIds, ", ");
		}

		String sql = "update fees_schedule set is_delete = 'Y' where status = '"
				+ Constants.PAYMENT_STATUS.PARTIAL_PAID.get()
				+ "' AND fees_id = " + feeId;

		if (!StringUtils.isEmpty(userIds)) {
			sql = sql + " AND user_id not in (" + userIds + ")";
		}
		getJdbcTemplate().update(sql);

		logger.debug("student fees UNPAID mapping deleted ::::: " + feeId);
	}

	private void deleteUnpaidFromFeesSchedule(Integer feeId, List<Integer> excludeUserIds) {

		String userIds = null;
		if (excludeUserIds != null && !excludeUserIds.isEmpty()) {
			userIds = StringUtils.join(excludeUserIds, ", ");
		}
		String sql = "update fees_schedule set is_delete = 'Y' where status = '"
				+ Constants.PAYMENT_STATUS.UNPAID.get()
				+ "' AND parent_fees_schedule_id is null AND fees_id = "
				+ feeId;
		if (!StringUtils.isEmpty(userIds)) {
			sql = sql + " AND user_id not in (" + userIds + ") ";
		}
		getJdbcTemplate().update(sql);

		logger.debug("student fees UNPAID mapping deleted ::::: " + feeId);
	}

	private boolean isUsersChanged(Fees previousFees, Fees fees) {
		int preStandard = previousFees.getStandardId() != null ? previousFees
				.getStandardId() : 0;
		int standard = fees.getStandardId() != null ? fees.getStandardId() : 0;

		int preDivision = previousFees.getDivisionId() != null ? previousFees
				.getDivisionId() : 0;
		int division = fees.getDivisionId() != null ? fees.getDivisionId() : 0;

		int preCaste = previousFees.getCasteId() != null ? previousFees
				.getCasteId() : 0;
		int caste = fees.getCasteId() != null ? fees.getCasteId() : 0;

		int preFeesCode = previousFees.getFeesCodeId() != null ? previousFees
				.getFeesCodeId() : 0;

		int feesCode = fees.getFeesCodeId() != null ? fees.getFeesCodeId() : 0;

		int preStudent = previousFees.getStudentId() != null ? previousFees
				.getStudentId() : 0;
		int student = fees.getStudentId() != null ? fees.getStudentId() : 0;

		logger.debug("standard =" + standard + ", preStandard=" + preStandard
				+ ", division=" + division + ", preDivision=" + preDivision
				+ "," + " caste=" + caste + ", preCaste=" + preCaste
				+ ", feesCode=" + feesCode + ", preFeesCode=" + preFeesCode
				+ ", student=" + student + ", preStudent=" + preStudent);
		if (preStandard != standard || preDivision != division
				|| caste != preCaste || preFeesCode != feesCode
				|| preStudent != student) {

			logger.debug("User changed :::::: ");
			return true;
		}
		return false;
	}
/*
	private void updateFeesDisplayLayouts(Fees fees, Fees previousFees) {
		Set<Integer> templateIds = fees.getDisplayTemplateIds();
		Set<Integer> previousTemplateIds = previousFees.getDisplayTemplateIds();
		List<Integer> templateIdsToDelete = new ArrayList<>();

		if (previousTemplateIds != null && !previousTemplateIds.isEmpty()) {
			if (templateIds == null
					|| templateIds.isEmpty()
					|| Constants.FEE_TYPE.SINGLE_FEE.get().equals(
							fees.getFeeType())) {

				templateIdsToDelete.addAll(previousTemplateIds);
			} else {
				for (Integer previousLayoutId : previousTemplateIds) {
					if (!templateIds.contains(previousLayoutId)) {
						templateIdsToDelete.add(previousLayoutId);
					}
				}
			}
		}

		// Delete from display Layouts
		if (!templateIdsToDelete.isEmpty()) {
			String sql = "delete from fees_display_template where fees_id = "
					+ fees.getId() + " AND display_template_id in ("
					+ StringUtils.join(templateIdsToDelete, ", ") + ")";

			getJdbcTemplate().update(sql);
		}
		insertDisplayTemplates(fees);
	}
*/

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public boolean insertFees(final Fees fees, Integer currentLoggedInUserId) {
		return insertFeesOneByOne(fees, currentLoggedInUserId);
	}

	private boolean insertFeesOneByOne(final Fees fees, Integer currentLoggedInUserId) {

		boolean flag = false;

		String sql = "insert into fees (head_id, description, amount, duedate, fromdate, todate, "
				+ " standard_id, caste_id, branch_id, isemail, issms, isnotification, frequency, "
				+ " academic_year_id, division_id, student_id, is_split_payment, "
				+ " fees_code_configuration_id, is_partial_payment_allowed, is_delete, recurring, "
				+ " recurring_frequency, by_days, late_fee_detail_id, reminder_start_date, fee_type, by_date, created_by, display_template_id, "
				+ " currency, allow_repeat_entries, allow_user_entered_amount) "
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		FeesRecurringDetail recurringDetail = fees.getRecurringDetail();
		recurringDetail = recurringDetail == null ? new FeesRecurringDetail() : recurringDetail;

		Object[] args = { fees.getHeadId(), fees.getDescription(),
				fees.getAmount(), fees.getDueDate(), fees.getFromDate(),
				fees.getToDate(), fees.getStandardId(), fees.getCasteId(),
				fees.getBranchId(), 
				(StringUtils.isEmpty(fees.getIsEmail()) ? "N" : fees.getIsEmail()),
				(StringUtils.isEmpty(fees.getIsSms()) ? "N" : fees.getIsSms()),
				(StringUtils.isEmpty(fees.getIsNotification()) ? "N" : fees.getIsNotification()),
				(StringUtils.isEmpty(fees.getFrequency()) ? null : fees.getFrequency()),
				fees.getAcademicYearId(), fees.getDivisionId(),
				fees.getStudentId(), fees.getIsSplitPayment(),
				fees.getFeesCodeId(), fees.getIsPartialPaymentAllowed(), 'N',
				fees.getRecurring(), recurringDetail.getFrequency(),
				recurringDetail.getWorkWeeks(), fees.getLateFeeDetailId(),
				(StringUtils.isEmpty(fees.getReminderStartDate()) ? null : fees.getReminderStartDate()),
				fees.getFeeType(),
				recurringDetail.getByDate(), currentLoggedInUserId, 
				fees.getDisplayTemplateId(), fees.getCurrency(), fees.getAllowRepeatEntries(),
				fees.getIsAllowUserEnterAmt()
				
			};

		int[] argTypes = new int[] { Types.INTEGER, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
				Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
				Types.INTEGER, Types.INTEGER , Types.INTEGER, Types.VARCHAR, 
				Types.VARCHAR, Types.VARCHAR
			};

		getJdbcTemplate().update(sql, args, argTypes);

		sql = "select max(id) as id from fees";
		Integer feesId = getJdbcTemplate().queryForObject(sql, Integer.class);
		fees.setId(feesId);

		logger.debug("Inserting split payment.......");
		insertFeesWhenSplitPayment(fees);

//		insertDisplayTemplates(fees);
		if ("Y".equals(fees.getRecurring())) {
			prepareFeesRepeatSchedule(fees, fees.getFromDateD(), fees.getToDateD(),
					fees.getDueDateD(), fees.getReminderStartDateD());
		}
		insertFeesSchedule(fees, false);
		flag = true;
		return flag;
	}


	private void prepareFeesRepeatSchedule(Fees fees, Date fromDate, Date feeEndDate, Date feeDueDate, Date feeReminderStartDate) {
		
		boolean isDaily = Constants.FREQUENCY.DAILY.get().equals(fees.getRecurringDetail().getFrequency());

		java.sql.Date feeStartDate = getNextDateForFees(fees, new java.sql.Date(fromDate.getTime()), false, true);
		java.sql.Date dueDate = getNextDateForFees(fees, new java.sql.Date(feeDueDate.getTime()), true, true);
		java.sql.Date reminderStartDate = (feeReminderStartDate != null ? getNextDateForFees(fees, new java.sql.Date(feeReminderStartDate.getTime()), false, true) : null );
		java.sql.Date endDate = new java.sql.Date(feeEndDate.getTime());
		java.sql.Date tempEndDate = isDaily ? feeStartDate  : getNextDateForFees(fees, feeStartDate, false, false);

		System.out.println("Due Date ::>>>>>"+dueDate);
		while (dueDate != null && !feeStartDate.after(endDate) && !tempEndDate.after(endDate)) {

			String feeForDuration = simpleDateFormat.format(feeStartDate)
					+ " - " + simpleDateFormat.format(tempEndDate);

			if(dueDate.after(feeStartDate)){
				insertFeeRepeatSchedule(fees.getId(), feeStartDate, dueDate, reminderStartDate, feeForDuration);
			}
			/*else {
				dueDate = getNextDateForFees(fees, dueDate, true, true);
				if(dueDate.after(tempEndDate)){
					dueDate = tempEndDate;
				}
				insertFeeRepeatSchedule(fees.getId(), feeStartDate, dueDate, reminderStartDate, feeForDuration);
			}*/

			feeStartDate = getNextDateForFees(fees, feeStartDate, false, true);
			dueDate = getNextDateForFees(fees, dueDate, true, true);
			reminderStartDate = reminderStartDate != null ? getNextDateForFees(fees, reminderStartDate, false, true) : null;
			tempEndDate = isDaily ? feeStartDate  : getNextDateForFees(fees, feeStartDate, false, false);
			System.out.println("Due Date ::>>>>>"+dueDate);
		}
	}

	private void insertFeeRepeatSchedule(Integer feesId, java.sql.Date feeStartDate,
			java.sql.Date dueDate, java.sql.Date reminderStartDate, String feeForDuration) {
		
		String sql = "insert into fees_repeat_schedule " +
				"(fees_id, repeat_date, due_date, fee_for_duration "+(reminderStartDate != null ? ", reminder_start_date" : "") + ")" +
				"values(?, ?, ?, ? "+(reminderStartDate != null ? ", ?" : "")+")";

		if(reminderStartDate == null ){
			Object[] args = {feesId, feeStartDate, dueDate, feeForDuration};
			int [] types = new int[] {Types.INTEGER, Types.DATE, Types.DATE, Types.VARCHAR};
			getJdbcTemplate().update(sql, args, types);
		}
		else {
			Object[] args = {feesId, feeStartDate, dueDate, feeForDuration, reminderStartDate};
			int [] types = new int[] {Types.INTEGER, Types.DATE, Types.DATE, Types.VARCHAR, Types.DATE};
			getJdbcTemplate().update(sql, args, types);
		}
	}

	private java.sql.Date getNextDateForFees(Fees fees, java.sql.Date fromDate,
			boolean isDueDate, boolean isStartDate){

		Calendar calendar = Calendar.getInstance();
		fromDate = fromDate == null ? new java.sql.Date(
				calendar.getTimeInMillis()) : fromDate;

		java.sql.Date nextDate = null;
		if ("Y".equals(fees.getRecurring())) {

			FeesRecurringDetail recurringDetail = fees.getRecurringDetail();
			String frequency = recurringDetail.getFrequency();

			if (Constants.FREQUENCY.DAILY.get().equals(frequency)) {
				String[] byDayArr = recurringDetail.getWorkWeeks()
						.replaceAll(" ", "").split(",");
				calendar.setTime(fromDate);
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
				dayOfWeek = (dayOfWeek == 1 ? 7 : dayOfWeek - 1);
				List<String> byDays = Arrays.asList(byDayArr);
				nextDate = getDueDateWhenDaily(byDays, dayOfWeek, fromDate);
			} else if (Constants.FREQUENCY.WEEKLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils.addDays(fromDate, 6 +(isStartDate ? 1 : 0))
						.getTime());
			} else if (Constants.FREQUENCY.MONTHLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils.addDays(fromDate, 29 +(isStartDate ? 1 : 0)).getTime());

				/*if (isDueDate) {
					nextDate = new java.sql.Date(DateUtils.setDays(nextDate,
							Integer.parseInt(recurringDetail.getByDate())).getTime());
				}*/
			} else if (Constants.FREQUENCY.QUARTERLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils.addDays(fromDate,
						(30 * 3) -1 + (isStartDate ? 1 : 0)).getTime());
			} else if (Constants.FREQUENCY.HALF_YEARLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils.addDays(fromDate,
						(30 * 6) -1 + (isStartDate ? 1 : 0)).getTime());
			}
		}
		return nextDate;
	}

	public static void main(String a[]){
		String sql = GET_STUDENT_APPLICABLE_FOR_FEES + " AND f.id = 28438";

		sql += " group by s.user_id ";
		System.out.println(sql);

	}

	private List<Integer> getStudentsApllicableForFees(Integer feeId) {
		String sql = GET_STUDENT_APPLICABLE_FOR_FEES + " AND f.id = " + feeId;

		sql += " group by s.user_id ";
		List<Integer> studentUserIds = getJdbcTemplate().queryForList(sql,
				Integer.class);
		logger.debug("Fees applicable users :::: " + studentUserIds);
		return studentUserIds;
	}

	private void insertFeesSchedule(Fees fees, boolean isUpdate) {
		String sql = null;

		List<Integer> studentUserIds = getStudentsApllicableForFees(fees
				.getId());

		if (studentUserIds != null && !studentUserIds.isEmpty()) {
			sql = "insert into fees_schedule (fees_id, user_id, base_amount, "
					+ " late_payment_charges, total_amount, due_date, status, can_pay, is_delete, reminder_start_date, fee_for_duration, fee_start_date) "
					+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			String feeForDuration = getFeeDuration(fees);

			for (Integer studentUserId : studentUserIds) {
				boolean isPresent = false;
				if (isUpdate) {
					isPresent = checkFeesScheduledForUser(fees.getId(), studentUserId);
				}

				logger.debug("IS ::: PRESENT :::: " + isPresent);
				logger.debug("StudentUserId =" + studentUserId + ", Fees Id =" + fees.getId());
				if (!isPresent) {
					Double amount = Double.parseDouble(fees.getAmount());
					Object[] args = { fees.getId(), studentUserId, amount, 0.0,
							amount, fees.getDueDate(),
							Constants.PAYMENT_STATUS.UNPAID, "Y", "N",
							StringUtils.isEmpty(fees.getReminderStartDate()) ? null : fees.getReminderStartDate(), feeForDuration,
							fees.getFromDate() };

					int[] argTypes = new int[] { Types.INTEGER, Types.INTEGER,
							Types.DOUBLE, Types.DOUBLE, Types.DOUBLE,
							Types.DATE, Types.VARCHAR, Types.VARCHAR,
							Types.VARCHAR, StringUtils.isEmpty(fees.getReminderStartDate()) ? Types.NULL : Types.DATE, Types.VARCHAR,
							Types.DATE };

					getJdbcTemplate().update(sql, args, argTypes);
				} else {
					String query = "select max(id) as feesScheduleId from fees_schedule where fees_id = "
							+ fees.getId() + " AND user_id = " + studentUserId;
					Long feeScheduleId = getJdbcTemplate().queryForObject(
							query, Long.class);
					query = "update fees_schedule set reminder_start_date = ?, is_delete = 'N'  where (id = ? OR "
							+ " late_payment_for_fees_schedule = ? OR parent_fees_schedule_id = ?) AND status != ?";

					Object[] args = { StringUtils.isEmpty(fees.getReminderStartDate()) ? null:fees.getReminderStartDate(),
							feeScheduleId, feeScheduleId, feeScheduleId, Constants.PAYMENT_STATUS.PAID.get() };
					logger.debug("args :::: " + args);
					int[] argTypes = new int[] { StringUtils.isEmpty(fees.getReminderStartDate()) ? Types.NULL : Types.DATE , Types.BIGINT,
							Types.BIGINT, Types.BIGINT,
							Types.VARCHAR };
					logger.debug("args :::: " + argTypes);

					getJdbcTemplate().update(query, args, argTypes);
				}
			}
		}
		logger.debug("Fees schedule inserted....");
	}

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			"dd MMM, yyyy");

	private String getFeeDuration(Fees fees) {
		String feeForDuration = null;
		if ("Y".equals(fees.getRecurring())) {
			Date fromDate = fees.getFromDateD();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fees.getFromDateD());

			FeesRecurringDetail recurringDetail = fees.getRecurringDetail();
			Date nextDate = null;
			String frequency = recurringDetail.getFrequency();
			int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			dayOfWeek = (dayOfWeek == 1 ? 7 : dayOfWeek - 1);

			if (Constants.FREQUENCY.DAILY.get().equals(frequency)) {
				String[] byDayArr = recurringDetail.getWorkWeeks()
						.replaceAll(" ", "").split(",");

				
				List<String> byDays = Arrays.asList(byDayArr);
				nextDate = getDueDateWhenDaily(byDays, dayOfWeek, null);
			} else if (Constants.FREQUENCY.WEEKLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils.addDays(fromDate, 6)
						.getTime());
			} else if (Constants.FREQUENCY.MONTHLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils.addDays(fromDate, 29)
						.getTime());
			} else if (Constants.FREQUENCY.QUARTERLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils
						.addDays(fromDate, 30 * 3 -1).getTime());
			} else if (Constants.FREQUENCY.HALF_YEARLY.get().equals(frequency)) {
				nextDate = new java.sql.Date(DateUtils
						.addDays(fromDate, 30 * 6 -1).getTime());
			}
			if (nextDate != null) {
				if (nextDate.after(fees.getToDateD())) {
					nextDate = fees.getToDateD();
				}
				feeForDuration = simpleDateFormat.format(fromDate) + " - "
						+ simpleDateFormat.format(nextDate);
			}
		} else {
			feeForDuration = simpleDateFormat.format(fees.getFromDateD())
					+ " - " + simpleDateFormat.format(fees.getToDateD());
		}
		return feeForDuration;
	}

	private java.sql.Date getDueDateWhenDaily(List<String> byDays, int dayOfWeek, Date fromDate) {
		java.sql.Date nextDueDate = null;
		Date date = fromDate == null ? new Date() : fromDate;
		for (String dayStr : byDays) {
			int day = Integer.parseInt(dayStr);
			if (day > dayOfWeek) {
				nextDueDate = new java.sql.Date(DateUtils.addDays(date, day - dayOfWeek).getTime());
				break;
			}
		}

		if (nextDueDate == null) {
			int day = 7 - dayOfWeek;
			day = day + (Integer.parseInt(byDays.get(0)));
			nextDueDate = new java.sql.Date(DateUtils.addDays(date, day)
					.getTime());
		}
		return nextDueDate;
	}

	private boolean checkFeesScheduledForUser(Integer feesId,
			Integer studentUserId) {
		String sql = "select count(*) as count from fees_schedule "
				+ "where user_id = " + studentUserId + " AND fees_id = "
				+ feesId ;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);

		return (count > 0);
	}

/*	private void insertDisplayTemplates(Fees fees) {
		if (Constants.FEE_TYPE.GROUP_FEE.get().equals(fees.getFeeType())) {
			String sql = "insert into fees_display_template (fees_id, display_template_id) values (?, ?) ON DUPLICATE KEY UPDATE fees_id = fees_id";
			for (Integer displayLayoutId : fees.getDisplayTemplateIds()) {
				Object[] args = { fees.getId(), displayLayoutId };

				int[] argTypes = new int[] { Types.INTEGER, Types.INTEGER };

				getJdbcTemplate().update(sql, args, argTypes);
			}
		}
	}*/

	@Override
	@Transactional
	public Fees getFeeById(Integer id) {
		String sql = "select f.*,fcc.name as feeCodeName,count(frs.repeat_date) as feeRepeatCount, i.id as instituteId from fees as f " +
				" INNER JOIN branch as b on b.id = f.branch_id " +
				" INNER JOIN institute as i on i.id = b.institute_id "
				+" LEFT JOIN fees_repeat_schedule as frs on frs.fees_id = f.id AND DATEDIFF(now(), frs.repeat_date ) > 0 "+
				"LEFT JOIN fees_code_configuration as fcc on fcc.id=f.fees_code_configuration_id "+
				" where i.id = b.institute_id AND b.id = f.branch_id AND f.id ="+id;

		List<Fees> feesList = getJdbcTemplate().query(sql,
				new RowMapper<Fees>() {
					@Override
					public Fees mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						Fees fees = new Fees();
						fees.setId(rs.getInt("id"));
						fees.setDescription(rs.getString("description"));
						fees.setBranchId(rs.getInt("branch_id"));
						fees.setInstituteId(rs.getInt("instituteId"));
						fees.setHeadId(rs.getInt("head_id"));
						fees.setCasteId(rs.getInt("caste_id"));
						fees.setAmount(rs.getString("amount"));
						fees.setFromDate(rs.getString("fromdate"));
						fees.setToDate(rs.getString("todate"));
						fees.setDueDate(rs.getString("duedate"));
						fees.setStandardId(rs.getInt("standard_id"));
						fees.setDivisionId(rs.getInt("division_id"));
						fees.setIsEmail(rs.getString("isemail"));
						fees.setIsSms(rs.getString("issms"));

						fees.setIsNotification(rs.getString("isnotification"));
						fees.setFrequency(rs.getString("frequency"));
						fees.setAcademicYearId(rs.getInt("academic_year_id"));
						fees.setStudentId(rs.getInt("student_id"));
						fees.setIsSplitPayment(rs.getString("is_split_payment"));
						fees.setIsPartialPaymentAllowed(rs.getString("is_partial_payment_allowed"));
						fees.setFeesCodeId(rs.getInt("fees_code_configuration_id"));
						fees.setCurrency(rs.getString("currency"));
						fees.setAllowRepeatEntries(rs.getString("allow_repeat_entries"));

						fees.setEditDueDateAllowed(rs.getInt("feeRepeatCount") == 0 ? "Y" : "N");
						fees.setRecurring(rs.getString("recurring"));
						fees.setLateFeeDetailId(rs.getInt("late_fee_detail_id"));
						fees.setFeesCodeName(rs.getString("feeCodeName"));
						FeesRecurringDetail recurringDetail = new FeesRecurringDetail();
						recurringDetail.setFrequency(rs.getString("recurring_frequency"));
						recurringDetail.setWorkWeeks(rs.getString("by_days"));
						recurringDetail.setByDate(rs.getString("by_date"));
						if (!StringUtils.isEmpty(recurringDetail.getWorkWeeks())) {
							String[] byDayArr = recurringDetail.getWorkWeeks().split(",");
							Map<String, Boolean> byDays = new HashMap<String, Boolean>();
							for (String day : byDayArr) {
								byDays.put(day, true);
							}
							recurringDetail.setByDays(byDays);
						}

						fees.setRecurringDetail(recurringDetail);
						fees.setReminderStartDate(rs
								.getString("reminder_start_date"));
						fees.setFeeType(rs.getString("fee_type"));
						fees.setDisplayTemplateId(rs.getInt("display_template_id"));
						getSplitPaymentDetails(fees);
						getDisplayTemplates(fees);
						return fees;
					}
				});
		return feesList.get(0);
	}

	private void getDisplayTemplates(Fees fees) {
		String sql = "select dt.* from display_template as dt  "
				+ " WHERE dt.id = " + fees.getDisplayTemplateId() +" AND dt.is_delete = 'N' " ;

		final List<DisplayTemplate> displayTemplates = new ArrayList<>();

		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				DisplayTemplate template = new DisplayTemplate();
				template.setBranchId(rs.getInt("branch_id"));
				template.setId(rs.getInt("id"));
				template.setName(rs.getString("name"));
				template.setDescription(rs.getString("description"));
				displayTemplates.add(template);
				return null;
			}
		});
		fees.setDisplayTemplates(displayTemplates);
	}

	void getSplitPaymentDetails(final Fees fees) {
		String sql = "select * from fees_split_configuration where fees_id = "
				+ fees.getId();

		logger.debug("FEEES SPLIT PAYMENT GETTING:::::::::\n" + sql);
		getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
				if ("N".equals(rs.getString("is_seed_account"))) {
					fees.setSeedSchemeCodeId(rs.getInt("scheme_code_id"));
					fees.setSeedSplitAmount(rs.getString("amount"));
				} else {
					fees.setSchemeCodeId(rs.getInt("scheme_code_id"));
					fees.setSplitAmount(rs.getString("amount"));
				}
				return null;
			}
		});
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public boolean deleteFee(Integer id) {
		return deleteFeeOneByOne(id);
	}

	private  boolean deleteFeeOneByOne(Integer id) {
		String sql = "select is_split_payment from  fees where id = " + id;
		boolean isSplitPayment = getJdbcTemplate().query(sql,
				new RowMapper<Boolean>() {
					@Override
					public Boolean mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return "Y".equals(resultSet
								.getString("is_split_payment"));
					}
				}).get(0);

		sql = "update fees set is_delete = 'Y' where id = " + id;
		getJdbcTemplate().update(sql);

		if (isSplitPayment) {
			deleteSplitPayment(id);
		}

		// delete from display templates.
		/*sql = "delete from fees_display_template where fees_id = " + id;
		getJdbcTemplate().update(sql);*/

		sql = "update fees_repeat_schedule set is_delete = 'Y' where fees_id = " + id;
		getJdbcTemplate().update(sql);

		// delete from  fees_schedule
		sql = "update fees_schedule set is_delete = 'Y' where fees_id = " + id;
		getJdbcTemplate().update(sql);

		logger.debug("Fees deleted..");
		return true;
	}

	@Override
	@Transactional
	public List<Fees> getFeesByBranchId(Integer branchId) {
		String sql = "select f.*, ifnull(st.displayName, 'All') as standardName, ifnull(c.name, 'All') as caste,c.id as casteId,"
				+ " h.id as headId, h.name as head,b.id as branchId, ifnull(fcc.name, 'All') as feesCodeName, "
				+ " ifnull(d.displayName, 'All') as divisionName, concat("
				+ " if (s.first_name is not null, s.first_name, ''), ' ', if (s.last_name is not null, s.last_name, '')) "
				+ " as studentName, b.name as branch, lfpd.description as late_fee_detail_name from fees f "
				+ " inner join head h on f.head_id = h.id "
				+ " inner join branch b on b.id = f.branch_id "
				+ " left join caste c on c.id = f.caste_id "
				+ " left join standard st on st.id = f.standard_id "
				+ " left join division d on d.id = f.division_id "
				+ " left join fees_code_configuration fcc on f.fees_code_configuration_id = fcc.id"
				+ " left join student as s on s.id = f.student_id "
				+ " left join late_fees_payment_detail as lfpd on lfpd.id = f.late_fee_detail_id "
				+ " where (f.standard_id = st.id OR f.standard_id is null) AND f.branch_id = b.id "
				+ " AND f.is_delete = 'N' AND f.head_id = h.id AND "
				+ " f.branch_id ="
				+ branchId
				+ " AND f.academic_year_id = "
				+ "(select id from academic_year where is_current_active_year = 'Y' "
				+ " AND branch_id = "
				+ branchId
				+ " ) group by f.id order by f.duedate desc, f.fromdate desc, f.todate desc";

		System.out.println(sql);
		
		logger.debug("get fees by branch sql --" + sql);
		return getJdbcTemplate().query(sql, new RowMapper<Fees>() {
			@Override
			public Fees mapRow(ResultSet rs, int rowNum) throws SQLException {
				Fees fees = new Fees();
				fees.setFeesCodeName(rs.getString("feesCodeName"));
				fees.setId(rs.getInt("id"));
				fees.setBranch(rs.getString("branch"));
				fees.setCaste(rs.getString("caste"));
				fees.setHead(rs.getString("head"));
				fees.setDescription(rs.getString("description"));
				fees.setBranchId(rs.getInt("branch_id"));
				fees.setHeadId(rs.getInt("head_id"));
				fees.setCasteId(rs.getInt("caste_id"));
				fees.setAmount(rs.getString("amount"));
				fees.setFromDate(rs.getString("fromdate"));
				fees.setToDate(rs.getString("todate"));
				fees.setDueDate(rs.getString("duedate"));
				fees.setStandardId(rs.getInt("standard_id"));
				fees.setDivisionId(rs.getInt("division_id"));
				fees.setIsEmail(rs.getString("isemail"));
				fees.setIsSms(rs.getString("issms"));
				fees.setIsNotification(rs.getString("isnotification"));
				fees.setFrequency(rs.getString("frequency"));
				fees.setStandard(rs.getString("standardName"));
				fees.setDivision(rs.getString("divisionName"));
				fees.setAcademicYearId(rs.getInt("academic_year_id"));
				fees.setIsSplitPayment(rs.getString("is_split_payment"));
				fees.setIsPartialPaymentAllowed(rs.getString("is_partial_payment_allowed"));
				fees.setStudentName(rs.getString("studentName"));
				fees.setLateFeeDetailName(rs.getString("late_fee_detail_name"));

				// new changes
				fees.setRecurring(rs.getString("recurring"));
				FeesRecurringDetail recurringDetail = new FeesRecurringDetail();
				recurringDetail.setFrequency(rs.getString("recurring_frequency"));
				recurringDetail.setWorkWeeks(rs.getString("by_days"));
				fees.setRecurringDetail(recurringDetail);
				fees.setReminderStartDate(rs.getString("reminder_start_date"));
				fees.setFeeType(rs.getString("fee_type"));

				return fees;
			}
		});
	}

	@Override
	public List<FeesReport> getFeesReport(final FeesReportFilter filter) {

		String sql = " select fs.id as fees_schedule_id, fs.parent_fees_schedule_id, fs.late_payment_for_fees_schedule, "
				+ " s.id as studentId,s.registration_code as registrationCode, f.student_id as feesStudentId, slfp.discount_amount as discountAmount, "
				+ "  (select IFNULL(sum(late_payment_charges), 0.0) from fees_schedule where late_payment_for_fees_schedule = fs.id) as totalLatePaymentCharges, "
				+ " concat( if (s.first_name is not null, s.first_name, ''), ' ', if (s.last_name is not null, s.last_name, '')) "
				+ " as studentName,  s.roll_number, f.description,"
				+ " fs.due_date, f.amount as feeamount, slfp.is_student_specific, slfp.late_payment_charges as studentLatePaymentCharges ,"
				+ " f.id as feesId, p.paid_date, h.name as head, "
				+ " ppd.payment_gateway_transaction_id AS paymentGatewayTransactionId, "
				+ " b.name as branchName, i.name as instituteName, "
				+ " st.displayname as standard, p.payment_audit_id, p.amount as paidamount, "
				+ " d.displayname as division, c.name as caste, currency.icon_css_class, currency.code as currencyCode, "
				+ " pa.option_name as paymentOption, pa.mode_of_payment as paymentMode, p.payment_details as paymentDetails,p.paymentreference_details as paymentReferenceDetails,"
				+ " u.id as createdUserId, r.id as createdUserRoleId, fcc.name as feeCodeName, pa.qfix_reference_number, "
				+ " p.bank_name, p.bank_branch_name, p.ifsc_code, p.cheque_or_dd_number, "
				+ " prd.amount as refundAmount, prd.created_at as refundedAt, prd.status as refundStatus "
				+ " from  student s "
				+ " inner join fees_schedule fs on fs.user_id = s.user_id "
				+ " inner join branch b on b.id = s.branch_id "
				+ " inner join institute i on i.id = b.institute_id "
				+ " inner join fees f on f.id = fs.fees_id and f.branch_id = s.branch_id "
				+ " INNER JOIN currency on currency.code= f.currency " 
				+ " inner join head h on f.head_id = h.id"
				+ " left join standard st on st.id = s.standard_id "
				+ " left join division d on d.id = s.division_id"
				+ " left join caste c on c.id = s.caste_id"
				+ " left join fees_code_configuration fcc on fcc.id = s.fees_code_configuration_id and f.fees_code_configuration_id = s.fees_code_configuration_id "
				+ " left join payments p on p.id = fs.pyment_id "
				+ " left join user u on u.id = p.created_by"
				+ " left join user_role ur on ur.user_id = u.id "
				+ " left join role r on r.id = ur.role_id "
				+ " left join student_late_fee_payment slfp on slfp.fees_schedule_id = fs.id "
				+ " left join payment_audit pa on pa.id = p.payment_audit_id "
				+ " LEFT JOIN payment_refund_detail as prd on prd.payment_id = p.id "
				+ " LEFT JOIN payment.payment_detail ppd on ppd.qfix_reference_number = pa.qfix_reference_number "
				+ " where  s.branch_id = " + filter.getBranchId()
				+ " AND (fs.is_delete = 'N' OR (fs.is_delete = 'Y' AND fs.status != 'UNPAID')) "
				+ " AND fs.late_payment_for_fees_schedule is null "
				+ (filter.getInstituteId() != null && filter.getInstituteId() != 0 ? " and i.id = " + filter.getInstituteId() : "")
				+ (filter.getCasteId() != null ? " and c.id = " + filter.getCasteId() : " ")
				+ (filter.getStandardId() != null ? " and st.id = " + filter.getStandardId() : " ")
				+ (filter.getDivisionId() != null ? " and d.id = " + filter.getDivisionId() : " ")
				+ (!StringUtils.isEmpty(filter.getRollNumber()) ? " and s.roll_number like '%" + filter.getRollNumber() + "%'" : " ")
				+ (filter.getFeesId() != null ? " and f.id = " + filter.getFeesId() : "")
				+ (filter.getHeadId() != null ? " and h.id = " + filter.getHeadId() : "")
				+ (!StringUtils.isEmpty(filter.getRegistrationCode()) ? " and s.registration_code='" + filter.getRegistrationCode() + "'" : " ")
				+ (filter.getStatus() != null ? (" AND fs.status " + (filter.getStatus() == 0 ? " != 'PAID'" : " = 'PAID'")) : "")
				+ (!StringUtils.isEmpty(filter.getFromDate())
						&& !StringUtils.isEmpty(filter.getToDate()) ? " AND p.paid_date is not null AND p.paid_date BETWEEN '"
						+ filter.getFromDate() + " 00:00:00' AND '" + filter.getToDate() + " 23:59:00' " : "")
				+ " group by fs.id, p.id, prd.id order by s.first_name, s.last_name, f.id, p.id, prd.id ";

		System.out.println("GET DETAIL REPORT SQL ::::>>>\n\n" + sql);

		List<FeesReport> feesReportList = getJdbcTemplate().query(sql,
				new RowMapper<FeesReport>() {
					@Override
					public FeesReport mapRow(ResultSet rs, int arg1)
							throws SQLException {
						FeesReport report = null;
						report = new FeesReport();
						java.sql.Date paidDate = rs.getDate("paid_date");
						java.sql.Date dueDate = rs.getDate("due_date");

						report.setFeesId(rs.getInt("feesId"));
						report.setInstituteName(rs.getString("instituteName"));
						report.setBranchName(rs.getString("branchName"));
						report.setStandard(rs.getString("standard"));
						report.setDivision(rs.getString("division"));
						report.setCasteName(rs.getString("caste"));
						report.setFeeCodeName(rs.getString("feeCodeName"));
						report.setStudentName(rs.getString("studentName"));
						report.setRollNumber(rs.getString("roll_number"));
						report.setRegistrationCode(rs.getString("registrationCode"));
						report.setDiscountAmount(rs.getString("discountAmount"));
						report.setQfixReferenceNumber(rs.getString("qfix_reference_number"));
						report.setPaymentGatewayTransactionId(rs.getString("paymentGatewayTransactionId"));

						String paymentAuditId = rs.getString("payment_audit_id");
						int createdUserRoleId = rs.getInt("createdUserRoleId");
						if (paymentAuditId == null && paidDate != null) {
							if (createdUserRoleId == 1 || createdUserRoleId == 6) {
								report.setPaymentMode("SCHOOL");
							} else {
								report.setPaymentMode("SELF");
							}
						} else if (paidDate != null) {
							report.setPaymentMode(rs.getString("paymentMode"));
						} else {
							report.setPaymentMode("N/A");
						}

						java.sql.Date refundedAt = rs.getDate("refundedAt");

						if(refundedAt != null){
							report.setRefundStatus(rs.getString("refundStatus"));
							report.setRefundedAt(rs.getString("refundedAt"));
							report.setRefundAmount(rs.getString("refundAmount"));
						}else {
							report.setRefundStatus("NA");
							report.setRefundedAt("NA");
							report.setRefundAmount("NA");
						}

						report.setPaymentOption(rs.getString("paymentOption"));
						report.setHead(rs.getString("head"));
						report.setPaidDate((paidDate != null ? Constants.USER_DATE_FORMAT.format(paidDate) : ""));
						report.setDueDate((dueDate != null ? Constants.USER_DATE_FORMAT.format(dueDate) : ""));
						report.setFeeAmount(rs.getString("feeamount"));
						report.setPaidAmount(rs.getString("paidamount"));
						report.setCurrencyCode(rs.getString("currencyCode"));
						report.setCurrencyCssClass(rs.getString("icon_css_class"));

						if(rs.getInt("parent_fees_schedule_id") != 0 || rs.getInt("late_payment_for_fees_schedule") != 0){
							report.setLatePaymentCharges("");
						}
						else {
							Double latePaymentCharges = rs.getDouble("studentLatePaymentCharges");
							if (!"Y".equals(rs.getString("is_student_specific"))) {
								latePaymentCharges = rs.getDouble("totalLatePaymentCharges");
							}

							latePaymentCharges  = latePaymentCharges == null ? 0 : latePaymentCharges ;
							report.setLatePaymentCharges(latePaymentCharges+"");							
						}
						report.setFeesDescription(rs.getString("description"));
						report.setPaymentReferenceDetails(rs.getString("paymentReferenceDetails"));
						report.setPaymentDetails(rs.getString("paymentDetails"));
						report.setBankName(rs.getString("bank_name"));
						report.setBankBranchName(rs.getString("bank_branch_name"));
						report.setIfscCode(rs.getString("ifsc_code"));
						report.setChequeOrDDNumber(rs.getString("cheque_or_dd_number"));
						return report;
					}
				});
		System.out.println("Records Fetched :::::>>>>" + feesReportList.size());
		return feesReportList;
	}

	private String getFeesSummaryReportSqlClause(FeesReportFilter filter) {

		String sql = " from student s   "
				+ " inner join fees_schedule fs on fs.user_id = s.user_id   "
				+ " inner join fees f on f.id = fs.fees_id and f.branch_id = s.branch_id "
				+ " INNER JOIN currency on currency.code= f.currency " 
				+ " inner join branch b on b.id = s.branch_id "
				+ " inner join institute i on i.id = b.institute_id "
				+ " inner join head h on f.head_id = h.id  "
				+ " left join standard st on st.id = s.standard_id   "
				+ " left join division d on d.id = s.division_id  "
				+ " left join caste c on c.id = s.caste_id  "
				+ " left join payments as p on p.fees_schedule_id = fs.id "
				+ " left join student_late_fee_payment slfp on slfp.fees_schedule_id = fs.id   "
				+ " where  s.branch_id = "
				+ filter.getBranchId()
				+ " AND fs.late_payment_for_fees_schedule is null "
				+ " AND fs.parent_fees_schedule_id is null "
				+ " AND (fs.is_delete = 'N' OR (fs.is_delete = 'Y' AND fs.status != 'UNPAID')) "
				+ (filter.getInstituteId() != null && filter.getInstituteId() != 0 ? " and i.id = " + filter.getInstituteId() : "")
				+ (filter.getCasteId() != null ? " and c.id = " + filter.getCasteId() : " ")
				+ (filter.getStandardId() != null ? " and st.id = " + filter.getStandardId() : " ")
				+ (filter.getDivisionId() != null ? " and d.id = " + filter.getDivisionId() : " ")
				+ (!StringUtils.isEmpty(filter.getRollNumber()) ? " and s.roll_number like '%" + filter.getRollNumber() + "%'" : " ")
				+ (filter.getFeesId() != null ? " and f.id = " + filter.getFeesId() : "")
				+ (filter.getHeadId() != null ? " and h.id = " + filter.getHeadId() : "")
				+ (!StringUtils.isEmpty(filter.getRegistrationCode()) ? " and s.registration_code='" + filter.getRegistrationCode() + "'" : " ")
				+ (filter.getStatus() != null ? (" AND fs.status " + (filter.getStatus() == 0 ? " != 'PAID'" : " = 'PAID'")) : "")
				+ (!StringUtils.isEmpty(filter.getFromDate())
						&& !StringUtils.isEmpty(filter.getToDate()) ? " AND p.paid_date is not null AND p.paid_date BETWEEN '"
						+ filter.getFromDate() + " 00:00:00' AND '" + filter.getToDate() + " 23:59:59' " : "");
		return sql;
	}

	public Integer getFeesSummaryReportTotalCount(FeesReportFilter filter) {
		String sql = getFeesSummaryReportSqlClause(filter);
		List<Integer> countList = getJdbcTemplate().queryForList(
				"select count(*) as count " + sql + "  group by fs.id",
				Integer.class);
		Integer totalCount = countList.size();
		System.out.println(totalCount + "=--------->>");
		return totalCount;
	}

	@Override
	public TableResponse searchFeeReport(FeesReportFilter filter, boolean getDisplayHeads) {
		TableOptions tableOptions = filter.getTableOptions();
		int start = tableOptions == null ? 0 : tableOptions.getStart();
		int length = tableOptions == null ? 0 : tableOptions.getLength();
		String searchTxt = (String) (tableOptions == null ? "" : tableOptions .getSearch().get("value"));

		String select = " select "
				+ " (select IFNULL(sum(amount), 0.0) from payments where fees_schedule_id = fs.id) as totalPaidAmount,  "
				+ " (select IFNULL(sum(prd.amount), 0.0) from payment_refund_detail as prd inner join payments as p on p.id = prd.payment_id where p.fees_schedule_id = fs.id and status = 'SUCCESS') as refundedAmount, "
				+ " (select IFNULL(sum(late_payment_charges), 0.0) from fees_schedule where late_payment_for_fees_schedule = fs.id) as totalLatePaymentCharges,  "
				+ " fs.id as fee_schedule_id, fs.partial_paid_amount as partial_paid_amount,  f.is_delete as fees_deleted, fs.is_delete as fee_schedule_deleted,"
				+ " fs.can_pay as can_pay, fs.status as schedule_status,  s.id as studentId, f.student_id as feesStudentId,  s.isDelete as student_deleted, "
				+ " s.email_address, s.mobile, s.registration_code as registrationCode,  f.is_partial_payment_allowed as partialPaymentAllowed, "
				+ " slfp.late_payment_charges as student_late_payment_charges,  slfp.is_student_specific, slfp.discount_amount as discount_amount, "
				+ " concat( if (s.first_name is not null, s.first_name, ''), ' ',  if (s.last_name is not null, s.last_name, ''))  as studentName,  "
				+ " s.roll_number, f.description, fs.due_date as duedate, f.amount as feeamount, f.id as feesId, h.name as head,    "
				+ " st.displayname as standard,  d.displayname as division, IFNULL(c.name, '') as caste,  fs.total_amount as total_amount,"
				+ " fs.late_payment_charges as late_payment_charges, currency.icon_css_class, currency.code as currencyCode, allow_user_entered_amount, allow_repeat_entries, "
				+ " i.name as instituteName, b.name as branchName, p.paid_date, p.bank_name, p.bank_branch_name, p.ifsc_code, p.cheque_or_dd_number  ";

		String sql = getFeesSummaryReportSqlClause(filter)
				+ (StringUtils.isEmpty(searchTxt) ? ""
						: " AND (s.first_name like '%" + searchTxt
								+ "%' OR s.last_name like '%" + searchTxt
								+ "%')") 
				+ " group by fs.id order by s.first_name, s.last_name, f.id ";

		TableResponse tableResponse = new TableResponse();

		tableResponse.setDraw(tableOptions != null ? tableOptions.getDraw() : 0);
		if (length > 0) {
			sql += " LIMIT " + start + ", " + length;
		}
		select += sql;
		final List<FeesReport> feeReports = getFeeSummaryReport(select, false);
		tableResponse.setFeesReports(feeReports);
		return tableResponse;
	}


	@Override
	public List<FeesReport> getFeeSummaryReport(FeesReportFilter filter) {
		String select = " select "
				+ " (select IFNULL(sum(amount), 0.0) from payments where fees_schedule_id = fs.id) as totalPaidAmount,  "
				+ " (select IFNULL(sum(late_payment_charges), 0.0) from fees_schedule where late_payment_for_fees_schedule = fs.id) as totalLatePaymentCharges,  "
				+ " (select IFNULL(sum(amount), 0.0) from payment_refund_detail where payment_id = fs.pyment_id and status = 'SUCCESS') as refundedAmount, "
				+ " fs.id as fee_schedule_id, fs.partial_paid_amount as partial_paid_amount,  f.is_delete as fees_deleted, fs.is_delete as fee_schedule_deleted,"
				+ " fs.can_pay as can_pay, fs.status as schedule_status,  s.id as studentId, f.student_id as feesStudentId,  s.isDelete as student_deleted, "
				+ " s.email_address, s.mobile, s.registration_code as registrationCode,  f.is_partial_payment_allowed as partialPaymentAllowed, "
				+ " slfp.late_payment_charges as student_late_payment_charges,  slfp.is_student_specific, slfp.discount_amount as discount_amount, "
				+ " concat( if (s.first_name is not null, s.first_name, ''), ' ',  if (s.last_name is not null, s.last_name, ''))  as studentName,  "
				+ " s.roll_number, f.description, fs.due_date as duedate, f.amount as feeamount, f.id as feesId, h.name as head,    "
				+ " st.displayname as standard,  d.displayname as division, IFNULL(c.name, '') as caste,  fs.total_amount as total_amount,"
				+ " fs.late_payment_charges as late_payment_charges, currency.icon_css_class, currency.code as currencyCode, "
				+ " i.name as instituteName, b.name as branchName, p.paid_date, allow_user_entered_amount, allow_repeat_entries, "
				+ "  GROUP_CONCAT(dth.amount SEPARATOR ',') as displayHeadAmounts,  GROUP_CONCAT(dth.display_head_id SEPARATOR ',') as displayHeadIds, "
				+ "  GROUP_CONCAT(fsc.amount SEPARATOR ',') as schemeSplitAmounts,  GROUP_CONCAT(fsc.scheme_code_id SEPARATOR ',') as schemeSplitIds, "
				+ " p.bank_name, p.bank_branch_name, p.ifsc_code, p.cheque_or_dd_number "
				+ " from student s "
				+ " inner join fees_schedule fs on fs.user_id = s.user_id   "
				+ " inner join fees f on f.id = fs.fees_id and f.branch_id = s.branch_id "
				+ " INNER JOIN currency on currency.code= f.currency " 
				+ " inner join branch b on b.id = s.branch_id "
				+ " inner join institute i on i.id = b.institute_id "
				+ " inner join head h on f.head_id = h.id  "
				+ " inner join fees_split_configuration as fsc on fsc.fees_id = f.id " 
				+ " left join standard st on st.id = s.standard_id   "
				+ " left join division d on d.id = s.division_id  "
				+ " left join caste c on c.id = s.caste_id  "
				+ " left join payments as p on p.fees_schedule_id = fs.id "
				+ " left join student_late_fee_payment slfp on slfp.fees_schedule_id = fs.id "
				+ " LEFT JOIN display_template_head as dth on dth.display_template_id = f.display_template_id " 
				+ " LEFT JOIN display_head as dh on dh.id = dth.display_head_id "
				+ " where  s.branch_id = " + filter.getBranchId()
				+ " AND fs.late_payment_for_fees_schedule is null "
				+ " AND fs.parent_fees_schedule_id is null "
				+ " AND (fs.is_delete = 'N' OR (fs.is_delete = 'Y' AND fs.status != 'UNPAID')) "
				+ (filter.getInstituteId() != null && filter.getInstituteId() != 0 ? " and i.id = " + filter.getInstituteId() : "")
				+ (filter.getCasteId() != null ? " and c.id = " + filter.getCasteId() : " ")
				+ (filter.getStandardId() != null ? " and st.id = " + filter.getStandardId() : " ")
				+ (filter.getDivisionId() != null ? " and d.id = " + filter.getDivisionId() : " ")
				+ (!StringUtils.isEmpty(filter.getRollNumber()) ? " and s.roll_number like '%" + filter.getRollNumber() + "%'" : " ")
				+ (filter.getFeesId() != null ? " and f.id = " + filter.getFeesId() : "")
				+ (filter.getHeadId() != null ? " and h.id = " + filter.getHeadId() : "")
				+ (!StringUtils.isEmpty(filter.getRegistrationCode()) ? " and s.registration_code='" + filter.getRegistrationCode() + "'" : " ")
				+ (filter.getStatus() != null ? (" AND fs.status " + (filter.getStatus() == 0 ? " != 'PAID'" : " = 'PAID'")) : "")
				+ (!StringUtils.isEmpty(filter.getFromDate())
						&& !StringUtils.isEmpty(filter.getToDate()) ? " AND p.paid_date is not null AND p.paid_date BETWEEN '"
						+ filter.getFromDate() + " 00:00:00' AND '" + filter.getToDate() + " 23:59:59' " : "") 
				+ " group by fs.id order by s.first_name, s.last_name, f.id ";

		final List<FeesReport> feeReports = getFeeSummaryReport(select, true);
		return feeReports;
	}


	private List<FeesReport> getFeeSummaryReport(String select, final boolean getDisplayHeads) {
		final List<FeesReport> feeReports = new ArrayList<>();

		System.out.println("getFeesReport SQL -- " + (select ));
		getJdbcTemplate().query((select), new RowMapper<FeesReport>() {
			@Override
			public FeesReport mapRow(ResultSet rs, int arg1)
					throws SQLException {
				FeesReport report = null;
				Long feeScheduleId = rs.getLong("fee_schedule_id");

				double totalAmount = rs.getDouble("total_amount");
				Double latePaymentCharges = rs.getDouble("student_late_payment_charges");
				double feeAmount = rs.getDouble("feeamount");
				double discountAmount = rs.getDouble("discount_amount");

				if (!"Y".equals(rs.getString("is_student_specific"))) {
					latePaymentCharges = rs.getDouble("totalLatePaymentCharges");
				}
				latePaymentCharges = latePaymentCharges == null ? 0 : latePaymentCharges;

				double refundedAmount = rs.getDouble("refundedAmount");

				totalAmount = feeAmount + latePaymentCharges - discountAmount;

				double paidAmount = rs.getDouble("totalPaidAmount") - refundedAmount;
				double remainingAmount = totalAmount - paidAmount;
				String status = rs.getString("schedule_status");

				if (remainingAmount == totalAmount) {
					status = Constants.PAYMENT_STATUS.UNPAID.get();
				} else if (remainingAmount == 0) {
					status = Constants.PAYMENT_STATUS.PAID.get();
				}
				else if(remainingAmount < 0){
					status = Constants.PAYMENT_STATUS.OVER_PAID.get();
				}
				else {
					status = Constants.PAYMENT_STATUS.PARTIAL_PAID.get();
				}

				if(refundedAmount > 0 && remainingAmount > 0){
					status = Constants.PAYMENT_STATUS.PARTIAL_REFUND.get();
				}
				else if (refundedAmount> 0 && refundedAmount == paidAmount){
					status = Constants.PAYMENT_STATUS.REFUND.get();
				}

				report = new FeesReport();
				boolean recordDeleted = ("Y".equals(rs.getString("fees_deleted"))
					|| "Y".equals(rs.getString("student_deleted")) || "Y".equals(rs.getString("fee_schedule_deleted")));

				report.setRecordDeleted(recordDeleted);
				report.setCanPay("Y".equals(rs.getString("can_pay")));
				report.setFeeScheduleId(feeScheduleId);
				java.sql.Date dueDate = rs.getDate("duedate");

				report.setPartialPaymentAllowed(rs.getString("partialPaymentAllowed").equals("Y"));
				report.setFeesId(rs.getInt("feesId"));
				report.setStudentId(rs.getInt("studentId"));
				report.setInstituteName(rs.getString("instituteName"));
				report.setBranchName(rs.getString("branchName"));
				report.setStandard(rs.getString("standard"));
				report.setDivision(rs.getString("division"));
				report.setStudentName(rs.getString("studentName"));
				report.setRollNumber(rs.getString("roll_number"));
				report.setHead(rs.getString("head"));
				report.setDueDate((dueDate != null ? Constants.USER_DATE_FORMAT .format(dueDate) : ""));
				report.setFeeAmount(String.valueOf(feeAmount));
				report.setPaidAmount(String.valueOf(paidAmount));
				report.setRemainingAmount(String.valueOf(remainingAmount));
				report.setFeesDescription(rs.getString("description"));
				report.setLatePaymentCharges(String.valueOf(latePaymentCharges));
				report.setTotalAmount(String.valueOf(totalAmount));
				report.setDiscountAmount(discountAmount + "");
				report.setRegistrationCode(rs.getString("registrationCode"));
				report.setEmail(rs.getString("email_address"));
				report.setMobile(rs.getString("mobile"));
				report.setBankName(rs.getString("bank_name"));
				report.setBankBranchName(rs.getString("bank_branch_name"));
				report.setIfscCode(rs.getString("ifsc_code"));
				report.setChequeOrDDNumber(rs.getString("cheque_or_dd_number"));
				report.setCurrencyCode(rs.getString("currencyCode"));
				report.setCurrencyCssClass(rs.getString("icon_css_class"));
				report.setAllowUserEnteredAmount(rs.getString("allow_user_entered_amount"));

				if(rs.getDate("paid_date") != null){
					report.setPaidDate(Constants.USER_DATE_FORMAT.format(rs.getDate("paid_date")));
				}
				report.setStatus(status);
				report.setCasteName(rs.getString("caste"));

				if(getDisplayHeads){
					Map<Integer, String> displayHeads = getDisplayHeads(rs.getString("displayHeadAmounts"), rs.getString("displayHeadIds"));
					report.setDisplayHeads(displayHeads);
					
					Map<Integer, String > splitSchemeCodes = getSchemeCodes(rs.getString("schemeSplitAmounts"), rs.getString("schemeSplitIds"));
					report.setSplitSchemeCodes(splitSchemeCodes);
				}
				feeReports.add(report);
				return report;
			}
		});
		return feeReports;
	}
	
	protected Map<Integer, String> getSchemeCodes(String schemeSplitAmounts, String schemeSplitIds) {
		if(!StringUtils.isEmpty(schemeSplitIds)){
			String[] schemeSplitAmountArr = schemeSplitAmounts.split(",");
			String[] schemeSplitIdArr = schemeSplitIds.split(",");
			if(schemeSplitIdArr != null && schemeSplitIdArr.length > 0){
				Map<Integer, String> dataMap = new LinkedHashMap<Integer, String>();
				for(int i =0; i < schemeSplitAmountArr.length; i ++){
					String headName = schemeSplitAmountArr[i];
					dataMap.put(Integer.parseInt(schemeSplitIdArr[i]), headName );
				}
				return dataMap;
			}
		}
		return null;
	}


	protected Map<Integer, String> getDisplayHeads(String displayHeadNames, String displayHeadIds) {
		if(!StringUtils.isEmpty(displayHeadIds)){
			String[] displayHeadNameArr = displayHeadNames.split(",");
			String[] displayHeadIdArr = displayHeadIds.split(",");
			if(displayHeadIdArr != null && displayHeadIdArr.length > 0){
				Map<Integer, String> dataMap = new LinkedHashMap<Integer, String>();
				for(int i =0; i < displayHeadNameArr.length; i ++){
					String headName = displayHeadNameArr[i];
					dataMap.put(Integer.parseInt(displayHeadIdArr[i]), headName );
				}
				return dataMap;
			}
		}
		return null;
	}

	protected double getTotalPaidAmount(Long feeScheduleId) {
		Double paidAmount = 0.0;
		String sql = "select sum(fs.partial_paid_amount) as totalPaidAmountfrom from fees_schedule as fs "
				+ " WHERE fs.parent_fees_schedule_id = "
				+ feeScheduleId
				+ " OR fs.late_payment_for_fees_schedule = "
				+ feeScheduleId
				+ " OR id = " + feeScheduleId;
		paidAmount = getJdbcTemplate().queryForObject(sql, Double.class);
		paidAmount = paidAmount == null ? 0 : paidAmount;
		return paidAmount;
	}

	protected int getIndexOfFeesReport(List<FeesReport> feeReports,
			Long feeScheduleId) {
		int index = -1;
		for (int i = 0; i < feeReports.size(); i++) {
			FeesReport report = feeReports.get(i);
			if (feeScheduleId.equals(report.getFeeScheduleId())) {
				index = i;
				break;
			}
		}
		return index;
	}

	private Double getTotalLatePaymentCharges(Long feeScheduleId) {
		// changes made by pitabas IfNULL check
		String sql = "select IFNULL(sum(late_payment_charges),0.0) as totalLateFees from fees_schedule where late_payment_for_fees_schedule = "
				+ feeScheduleId;

		return getJdbcTemplate().queryForObject(sql, Double.class);
	}

	@Transactional
	@Override
	public List<FeesDescription> getFeesPaymentDescription(Long feeScheduleId,
			Integer roleId, Integer createdUserId) {

		String sql = "select fs.id as feeScheduleId, p.*, pa.qfix_reference_number, prd.amount as refundedAmount, prd.created_at as refundedAt,"
				+ " prd.status as refundStatus, pa.mode_of_payment, pa.payment_mode as paymentModeChar, "
				+ " CONCAT (IFNULL (c.firstname, ''), ' ', IFNULL(c.lastname, '')) as paidBy, "
				+ " CONCAT (IFNULL (c1.firstname, ''), ' ', IFNULL(c1.lastname, '')) as refundedBy, f.is_partial_payment_allowed "
				+ " from payments as p "
				+ " INNER JOIN contact as c on p.created_by = c.user_id "
				+ " INNER JOIN fees_schedule as fs on p.fees_schedule_id = fs.id "
				+ " INNER JOIN fees as f on f.id = fs.fees_id "
				+ " LEFT JOIN payment_audit as pa on pa.id = p.payment_audit_id "
				+ " LEFT JOIN payment_refund_detail as prd on prd.payment_id = p.id "
				+ " LEFT JOIN contact as c1 on prd.created_by = c1.user_id "
				+ " WHERE ( fs.parent_fees_schedule_id = " + feeScheduleId + " OR fs.id = " + feeScheduleId + " ) "
				+ " AND late_payment_for_fees_schedule is null";

		if (!roleId.equals(Constants.ROLE_ID_ADMIN)
				&& !roleId.equals(Constants.ROLE_ID_SCHOOL_ADMIN)) {
			sql += " AND created_by = " + createdUserId;
		}

		final RefundConfig config = getRefundConfigurationByFeesScheduleId(feeScheduleId);
		
		final List<Integer> paymentIds = new ArrayList<Integer>();

		final List<FeesDescription> feesDescriptions = new ArrayList<FeesDescription> ();
		getJdbcTemplate().query(sql, new RowMapper<FeesDescription>() {
					@Override
					public FeesDescription mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						FeesDescription feesDescription = new FeesDescription();
						feesDescription.setStudentId(resultSet.getInt("student_id"));
						feesDescription.setFeesId(resultSet.getInt("fees_id"));
						feesDescription.setId(resultSet.getInt("id"));
						feesDescription.setPgEntry(resultSet.getInt("payment_audit_id") > 0);
						feesDescription.setQfixReferenceNumber(resultSet.getString("qfix_reference_number"));
						feesDescription.setOfLineReferenceNo(resultSet.getString("paymentreference_details"));
						feesDescription.setPaymentMode(resultSet.getString("payment_details"));
						feesDescription.setFeeScheduleId(resultSet.getLong("feeScheduleId"));
						feesDescription.setModeOfPayment(resultSet.getString("mode_of_payment"));
						feesDescription.setModeOfPaymentChar(resultSet.getString("paymentModeChar"));
						java.sql.Date refundedAt = resultSet.getDate("refundedAt");

						feesDescription.setAmount(resultSet.getInt("amount"));
						feesDescription.setPaidDate(Constants.USER_DATE_FORMAT.format(resultSet.getDate("paid_date")));
						feesDescription.setCreatedByName(resultSet.getString("paidBy"));
						feesDescription.setTransactionType("PAID");

						FeesDescription refundDesc = null;

						if(refundedAt != null){
							try {
								refundDesc = feesDescription.copy();
								refundDesc.setAmount(resultSet.getInt("refundedAmount"));
								refundDesc.setPaidDate(Constants.USER_DATE_FORMAT.format(refundedAt));
								refundDesc.setCreatedByName(resultSet.getString("refundedBy"));
								refundDesc.setTransactionType("REFUND");
								refundDesc.setPaymentMode("N/A");
								refundDesc.setRefundStatus(resultSet.getString("refundStatus"));
							} catch (CloneNotSupportedException e) {
								e.printStackTrace();
							}
						}
						if(!paymentIds.contains(feesDescription.getId())){
							feesDescriptions.add(feesDescription);
							paymentIds.add(feesDescription.getId());
						}
						if(refundDesc != null){
							feesDescriptions.add(refundDesc);
						}

						// Set Refund and partial refund is allowed..
						if(feesDescription.isPgEntry()){
							setRefundAllowed(feesDescription, config);	
						}else {
							if(!"REFUND".equals(feesDescription.getTransactionType())){
								// Manual marked as paid
								feesDescription.setRefundAllowed("Y");
								feesDescription.setPartialRefundAllowed("Y".equals("is_partial_payment_allowed") ? "Y" : "N");	
							}
						}
						return feesDescription;
					}
				});
		System.out.println("FeesDEscription" + feesDescriptions);
		return feesDescriptions;
	}

	protected void setRefundAllowed(FeesDescription feesDescription, RefundConfig config) {
		boolean refundAllowed = false;
		boolean partialRefundAllowed = false;

		if(config != null && !"REFUND".equals(feesDescription.getTransactionType())){
			if("O".equals(feesDescription.getModeOfPaymentChar())){
				if("DEBIT CARD".equalsIgnoreCase(feesDescription.getModeOfPayment())){
					refundAllowed = "Y".equals(config.getAllowDebitCardRefund());
					partialRefundAllowed = "Y".equals(config.getAllowDebitCardPartialRefund());
				}

				if("CREDIT CARD".equalsIgnoreCase(feesDescription.getModeOfPayment())){
					refundAllowed = "Y".equals(config.getAllowCreditCardRefund());
					partialRefundAllowed = "Y".equals(config.getAllowCreditCardPartialRefund());
				}

				if("BANK".equalsIgnoreCase(feesDescription.getModeOfPayment())){
					refundAllowed = "Y".equals(config.getAllowNetBankingRefund());
					partialRefundAllowed = "Y".equals(config.getAllowNetBankingPartialRefund());
				}
			}else {
				if("H".equalsIgnoreCase(feesDescription.getModeOfPaymentChar()) || "D".equalsIgnoreCase(feesDescription.getModeOfPaymentChar())){
					refundAllowed = "Y".equals(config.getAllowChequeDDRefund());
					partialRefundAllowed = "Y".equals(config.getAllowChequeDDPartialRefund());
				}else if("N".equalsIgnoreCase(feesDescription.getModeOfPaymentChar())){
					refundAllowed = "Y".equals(config.getAllowNeftRtgsRefund());
					partialRefundAllowed = "Y".equals(config.getAllowNeftRtgsPartialRefund());
				}else if("C".equalsIgnoreCase(feesDescription.getModeOfPaymentChar())){
					refundAllowed = "Y".equals(config.getAllowChequeDDRefund());
					partialRefundAllowed = "Y".equals(config.getAllowChequeDDPartialRefund());
				}
			}
		}
		feesDescription.setRefundAllowed(refundAllowed ? "Y" :"N");
		feesDescription.setPartialRefundAllowed(partialRefundAllowed ? "Y" :"N");
	}


	private RefundConfig getRefundConfigurationByFeesScheduleId(Long feeScheduleId) {
		String sql = "select branch_id from fees where id = (select fees_id from fees_schedule where id = " + feeScheduleId + ")";
		Integer branchId = getJdbcTemplate().queryForObject(sql, Integer.class);
		return branchDao.getRefundConfigurationByBranchId(branchId);
	}


	@Transactional
	@Override
	public void deletePaidFees(List<Long> feesScheduleIdList) {
		for (final Long feesScheduleId : feesScheduleIdList) {
			String sql = "select IFNULL(pyment_id, 0) as paymentId, parent_fees_schedule_id, if(parent_fees_schedule_id is null, 'Y', 'N') as isBaseRecord from fees_schedule where id = "
					+ feesScheduleId;
			getJdbcTemplate().queryForObject(sql, new RowMapper<Object>() {
				@Override
				public Object mapRow(ResultSet rs, int arg1)
						throws SQLException {
					Integer paymentId = rs.getInt("paymentId");
					boolean isBaseRecord = "Y".equals(rs
							.getString("isBaseRecord"));

					if (!isBaseRecord) {
						String sql = "update fees_schedule set status = '"
								+ Constants.PAYMENT_STATUS.PARTIAL_PAID
								+ "' where id = "
								+ rs.getInt("parent_fees_schedule_id");
						getJdbcTemplate().update(sql);
					}

					String sql = "update fees_schedule set pyment_id=null, partial_paid_amount=null, "
							+ (isBaseRecord ? "" : "is_delete='Y', ")
							+ " parent_fees_schedule_id=null, status='UNPAID' where id ="
							+ feesScheduleId + " OR late_payment_for_fees_schedule ="+ feesScheduleId ;
					getJdbcTemplate().update(sql);

					if (paymentId != 0) {
						// delete from payment
						sql = "delete from payments where id = " + paymentId;
						getJdbcTemplate().update(sql);

						// delete from passbook
						sql = "delete from passbook where id = (select passbook_id from payments where id = "
								+ paymentId + ")";
						getJdbcTemplate().update(sql);
					}
					return null;
				}

			});
		}
	}

	@Transactional
	@Override
	public void updatePaidFees(List<FeesDescription> feesDescriptions) {

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(new Date());

		List<Long> paymentIdListToDelete = new ArrayList<Long>();

		for (FeesDescription feeDescription : feesDescriptions) {
			if (feeDescription.getNewAmount() != null
					&& feeDescription.getNewAmount().intValue() == 0) {
				paymentIdListToDelete.add(feeDescription.getFeeScheduleId());
			} else if (feeDescription.getNewAmount() != null) {
				String sql = "update passbook set amount = '"
						+ feeDescription.getNewAmount()
						+ "', updated_date = '"
						+ date
						+ "'"
						+ " WHERE id = (select passbook_id from payments where id = "
						+ feeDescription.getId() + ")";
				getJdbcTemplate().update(sql);

				// update in fees_schedule
				String status = "PARTIAL PAID";
				sql = "update fees_schedule set partial_paid_amount = '"
						+ feeDescription.getNewAmount() + "' , " + "status= '"
						+ status + "' " + " WHERE pyment_id = "
						+ feeDescription.getId();

				getJdbcTemplate().update(sql);

				// update in payments.
				sql = "update payments set amount = '"
						+ feeDescription.getNewAmount() + "' " + " WHERE id = "
						+ feeDescription.getId();

				getJdbcTemplate().update(sql);
			}
		}
		if (!paymentIdListToDelete.isEmpty()) {
			deletePaidFees(paymentIdListToDelete);
		}
	}

	@Transactional
	@Override
	public void markFeePaid(final FeePayment feePayment) throws Exception {

		isValidFeeOrStudentToChnageFeeCharge(feePayment.getFeeId(),
				feePayment.getStudentId());
		double amount = checkFeeAmount(feePayment.getFeeScheduleId(),
				feePayment.getAmount());
		feePayment.setAmount(amount);
		
		final Long auditPaymentId = feePayment.getPaymentAuditId();
		
		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				String sql = "insert into payments (student_id, amount, paid_date, fees_id, created_by,payment_details, "
						+ " paymentreference_details, fees_schedule_id, bank_name, bank_branch_name, ifsc_code, cheque_or_dd_number, payment_audit_id) "
						+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, feePayment.getStudentId());
				ps.setDouble(2, feePayment.getAmount());
				ps.setDate(3, new java.sql.Date(feePayment.getPaymentDate().getTime()));
				ps.setInt(4, feePayment.getFeeId());
				ps.setInt(5, feePayment.getCreatedBy());
				ps.setString(6, feePayment.getpaymentDetail());
				ps.setString(7, feePayment.getpaymentReferenceDetail());
				ps.setLong(8, feePayment.getFeeScheduleId());
				ps.setString(9, feePayment.getBankName());
				ps.setString(10, feePayment.getBankBranchName());
				ps.setString(11, feePayment.getIfscCode());
				ps.setString(12, feePayment.getCheckOrDDNumber());
				if(feePayment.getPaymentAuditId() != null){
					ps.setLong(13, feePayment.getPaymentAuditId());
				}else {
					ps.setString(13, null);
				}
				return ps;
			}
		}, holder);

		if(auditPaymentId != null){
			String sql = "update payment_audit set status = 'SUCCESS', updated_at = now(), updated_by = "+feePayment.getCreatedBy() +
					", updated_by_process = 'MANUAL' where id = " + feePayment.getPaymentAuditId();
			getJdbcTemplate().update(sql);
		}

		Integer paymentId = holder.getKey().intValue();

		// String sql = "select user_id from student where id = "
		// + feePayment.getStudentId();
		// Integer userId = getJdbcTemplate().queryForObject(sql,
		// Integer.class);
		// System.out.println("userId************" + userId);

		/*
		 * sql = "select MIN(id) from fees_schedule where fees_id = " +
		 * feePayment.getFeeId() + " AND user_id = " +
		 * userId+" group by fees_id,user_id "; Long feeScheduleId =
		 * getJdbcTemplate().queryForObject (sql, Long.class);
		 */
		System.out.println("feeScheduleId::::::::::::::::"
				+ feePayment.getFeeScheduleId());
		updateFeesSchedule(feePayment.getFeeScheduleId(), paymentId, amount
				+ "");

		// Check if total amount of fees is paid including late payment charges.
		/*
		 * sql =
		 * "select (f.amount + IFNULL(slfp.late_payment_charges, 0) -  IFNULL(sum(p.amount), 0) "
		 * + " - IFNULL(slfp.discount_amount, 0)) as remainingamount " +
		 * " from payments as p " + " INNER JOIN fees as f on p.fees_id = f.id"
		 * + " INNER JOIN student_late_fee_payment slfp " +
		 * " on slfp.fees_id = f.id AND slfp.student_id = p.student_id " +
		 * " WHERE slfp.student_id = " + feePayment.getStudentId() +
		 * " AND slfp.fees_id = " + feePayment.getFeeId();
		 * 
		 * System.out.println("CHECK FEES IS FULLY PAID \nSQL :::: " + sql);
		 * Integer remainingAmount = getJdbcTemplate().queryForObject(sql,
		 * Integer.class); if (remainingAmount == 0) { // remaining amount is 0
		 * then mark fees as paid. sql =
		 * "update student_late_fee_payment set is_paid = 'Y' " +
		 * "where student_id = " + feePayment.getStudentId() + " AND fees_id = "
		 * + feePayment.getFeeId(); getJdbcTemplate().update(sql);
		 * 
		 * } else if (remainingAmount < 0) { logger.error("Over paid feesId = "
		 * + feePayment.getFeeId() + ", studentId = " +
		 * feePayment.getStudentId()); }
		 */
	}

	/**
	 * @throws ArrayIndexOutOfBoundsException
	 *             when fees not found for this feesId
	 * @throws RuntimeException
	 *             when actual fees amount is less than paying fees amount.
	 * */
	private double checkFeeAmount(final Long feesScheduleId,
			final double feeAmount) {
		String sql = "select f.is_partial_payment_allowed,  f.amount as baseFeeAmount, "
				+ " slfp.is_student_specific, IFNULL(slfp.late_payment_charges, 0) as student_late_payment_charges, "
				+ " IFNULL(slfp.discount_amount, 0)  as discount_amount "
				+ " from fees as f "
				+ " INNER JOIN fees_schedule as fs on fs.fees_id = f.id "
				+ " LEFT JOIN student_late_fee_payment as slfp on slfp.fees_schedule_id = fs.id where fs.id = "
				+ feesScheduleId;

		double amount = getJdbcTemplate().query(sql, new RowMapper<Double>() {
			@Override
			public Double mapRow(ResultSet rs, int arg1) throws SQLException {
				double amount = 0.0;
				if ("Y".equals(rs.getString("is_student_specific"))) {
					amount = rs.getDouble("baseFeeAmount")
							+ rs.getDouble("student_late_payment_charges")
							- rs.getDouble("discount_amount");
				} else {
					amount = getTotalLatePaymentCharges(feesScheduleId)
							+ rs.getDouble("baseFeeAmount") - rs.getDouble("discount_amount");
				}
				if (rs.getString("is_partial_payment_allowed").equals("Y")) {
					if (amount < feeAmount) {
						throw new RuntimeException(
								"Amount cannot be greater than actual fee amount");
					}
					amount = feeAmount;
				}
				return amount;
			}
		}).get(0);

		return amount;
	}

	private boolean isValidFeeOrStudentToChnageFeeCharge(Integer feesId,
			Integer studentId) throws Exception {
		String sql = "select count(*) as count from fees where id = " + feesId
				+ " AND is_delete = 'N'";
		int count = getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		if (count == 0) {
			throw new Exception("Invalid Fees provided to process.");
		}

		sql = "select count(*) as count from student where id = " + studentId
				+ " AND isDelete = 'N' AND active = 'Y'";
		count = getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		if (count == 0) {
			throw new Exception("Invalid Student provided to process.");
		}
		return true;
	}

	@Transactional
	@Override
	public void setLateFeesCharge(final LateFeesPayment latePayment)
			throws Exception {

		final double latePaymentAmount = latePayment.getLateFeesCharges();
		final double discountAmount = (StringUtils.isEmpty(latePayment
				.getDiscountAmount()) ? 0 : Double.parseDouble(latePayment
				.getDiscountAmount()));

		if (latePaymentAmount > 0 || discountAmount > 0) {
			for (final Long scheduleId : latePayment.getFeeScheduleIds()) {

				String sql = "select f.amount as amount from fees as f"
						+ " INNER JOIN fees_schedule as fs on fs.fees_id = f.id WHERE fs.id = "
						+ scheduleId;
				double baseFeeAmount = getJdbcTemplate().queryForObject(sql,
						Double.class);

				if (!StringUtils.isEmpty(latePayment.getDiscountAmount())
						&& baseFeeAmount < Double.parseDouble(latePayment.getDiscountAmount())) {
					throw new Exception("Discount amount must be less than actual fee amount.");
				}

				sql = "select count(*) as count, student_late_fee_payment.* from student_late_fee_payment "
						+ " where fees_schedule_id = " + scheduleId;

				getJdbcTemplate().query(sql, new RowMapper<Object>() {
					@Override
					public Object mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						int count = resultSet.getInt("count");
						if (count > 0) {
							String sql = "update student_late_fee_payment "
									+ " set is_student_specific = "+(latePaymentAmount > 0 ? "'Y'" : "'N'")+", is_paid = 'N'";
							sql += ", discount_amount = " + discountAmount;
							sql += ", late_payment_charges = " + latePaymentAmount;
							sql += " where fees_schedule_id = " + scheduleId;
							getJdbcTemplate().update(sql);
						} else {
							String sql = "insert into student_late_fee_payment "
									+ " (fees_schedule_id, late_payment_charges, "
									+ " is_student_specific, is_paid, discount_amount)"
									+ " values("
									+ scheduleId
									+ ", "
									+ latePaymentAmount
									+ ", "+(latePaymentAmount > 0 ? "'Y'" : "'N'")+", 'N', "
									+ discountAmount + ")";
							getJdbcTemplate().update(sql);
						}
						return null;
					}
				});
			}
		} else {
			String feeScheduleIds = StringUtils.join(
					latePayment.getFeeScheduleIds(), ", ");
			String sql = "delete from student_late_fee_payment where fees_schedule_id in ("
					+ feeScheduleIds + ")";
			getJdbcTemplate().update(sql);
		}
	}

	/*
	 * private void deletePaymentSetting(Integer feesId){ String sql =
	 * "delete from payment_setting where fees_id = '"+feesId+"'";
	 * getJdbcTemplate().update(sql); }
	 */

	private void deleteSplitPayment(Integer feesId) {
		String sql = "delete from fees_split_configuration where fees_id = "
				+ feesId;
		getJdbcTemplate().update(sql);
	}

	/*
	 * private void updatePaymentSetting(final Fees fees, List<Integer>
	 * paymentGatewayIds, boolean isUpdate) { if(isUpdate){
	 * deletePaymentSetting(fees.getId()); }
	 * 
	 * for(final Integer gatewayId : paymentGatewayIds){ String sql =
	 * "insert into payment_setting (gateway_id, branch_id, fees_id) " +
	 * "values( "+gatewayId+", "+fees.getBranchId()+", "+fees.getId()+")";
	 * getJdbcTemplate().update(sql); } }
	 */

	private void insertFeesWhenSplitPayment(Fees fees) {
		double splitAmount;
		if (StringUtils.isEmpty(fees.getSeedSplitAmount())) {
			splitAmount = (Double.parseDouble(fees.getAmount()) - 0);
		} else {
			splitAmount = (Double.parseDouble(fees.getAmount()) - Double
					.parseDouble(fees.getSeedSplitAmount()));
		}
		String sql = "insert into fees_split_configuration (fees_id, scheme_code_id, amount, is_seed_account) "
				+ " values(?, ?, ?, ?)";

		Object[] params = new Object[] { fees.getId(), fees.getSchemeCodeId(),
				splitAmount, "Y" };

		int[] types = new int[] { Types.INTEGER, Types.VARCHAR, Types.INTEGER,
				Types.CHAR };

		getJdbcTemplate().update(sql, params, types);

		if ("Y".equals(fees.getIsSplitPayment())) {
			params = new Object[] { fees.getId(), fees.getSeedSchemeCodeId(),
					fees.getSeedSplitAmount(), "N" };

			types = new int[] { Types.INTEGER, Types.VARCHAR, Types.INTEGER,
					Types.CHAR };

			getJdbcTemplate().update(sql, params, types);

		}
	}

	private void updateFeesWhenSplitPayment(Fees fees) {
		deleteSplitPayment(fees.getId());
		insertFeesWhenSplitPayment(fees);
	}

	public List<SchemeCode> getSchemeCodes(Integer branchId, String token) {
		// Integer paymentConfigId = getPaymentConfigIdByBranch(branchId);
		List<SchemeCode> schemeCodes = null;
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> acceptableMediaTypes = new ArrayList<>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptableMediaTypes);
		headers.add("token", token);

		HttpEntity<String> httpEntity = new HttpEntity<String>("", headers);
		RestTemplate restTemplate = new RestTemplate();

		final ResponseEntity<Object> responseEntity = restTemplate.exchange(
				UrlConfiguration.GET_SCHEME_CODES_URL + "?branch_id="
						+ branchId, HttpMethod.GET, httpEntity, Object.class);

		ObjectMapper mapper = new ObjectMapper();
		try {
			String userJson = mapper.writeValueAsString(responseEntity
					.getBody());
			schemeCodes = mapper.readValue(userJson, mapper.getTypeFactory()
					.constructCollectionType(List.class, SchemeCode.class));

			// user.setRoles(userObj.getRoles());
			logger.debug("Login response from user service - " + userJson);
		} catch (IOException e) {
			logger.error("", e);
		}
		return schemeCodes;
	}

	/*
	 * private Integer getPaymentConfigIdByBranch(Integer branchId) { String sql
	 * = "select * from payment_setting where branch_id = "+branchId; Integer
	 * paymentConfigId = null;
	 * 
	 * List<Integer> gatewayIds = getJdbcTemplate().query(sql, new
	 * RowMapper<Integer>(){
	 * 
	 * @Override public Integer mapRow(ResultSet resultSet, int arg1) throws
	 * SQLException { return resultSet.getInt("gateway_id"); }} );
	 * 
	 * if(gatewayIds != null && !gatewayIds.isEmpty()){ paymentConfigId =
	 * gatewayIds.get(0); } return paymentConfigId; }
	 */

	@Override
	public List<LateFeesDetail> getAllLateFees(Integer branchId) {
		List<LateFeesDetail> lateFeesList = new ArrayList<>();
		String sql = "select * from late_fees_payment_detail where is_delete = 'N' AND branch_id = "
				+ branchId;
		lateFeesList = getJdbcTemplate().query(sql,
				new RowMapper<LateFeesDetail>() {
					@Override
					public LateFeesDetail mapRow(ResultSet rs, int arg1)
							throws SQLException {
						LateFeesDetail lateFeesDetail = new LateFeesDetail();
						lateFeesDetail.setId(rs.getInt("id"));
						lateFeesDetail.setBranchId(rs.getInt("branch_id"));
						lateFeesDetail.setDescription(rs.getString("description"));
						lateFeesDetail.setDescription(rs.getString("description"));
						lateFeesDetail.setType(rs.getString("type"));
						lateFeesDetail.setFrequency(rs.getString("frequency"));
						lateFeesDetail.setByDays(rs.getString("by_days"));
						lateFeesDetail.setAmount(rs.getDouble("amount"));
						lateFeesDetail.setMaximumAmount(rs.getDouble("maximum_amount"));
						lateFeesDetail.setBaseAmount(rs.getDouble("base_amount"));
						return lateFeesDetail;
					}
				});
		return lateFeesList;
	}

	@Override
	public LateFeesDetail getLateFees(Integer lateFeesId) {
		String sql = "select lfpd.*, b.institute_id from late_fees_payment_detail as lfpd "
				+ " INNER JOIN branch as b on b.id = lfpd.branch_id "
				+ " where lfpd.is_delete = 'N' AND lfpd.id = " + lateFeesId;

		// Map <String,Boolean>dayMap=new HashMap();

		// Map <String,Boolean>dayMap=new HashMap();

		List<LateFeesDetail> lateFeesList = getJdbcTemplate().query(sql,
				new RowMapper<LateFeesDetail>() {
					@Override
					public LateFeesDetail mapRow(ResultSet rs, int arg1)
							throws SQLException {
						LateFeesDetail lateFeesDetail = new LateFeesDetail();
						lateFeesDetail.setId(rs.getInt("id"));
						lateFeesDetail.setBranchId(rs.getInt("branch_id"));
						lateFeesDetail.setDescription(rs.getString("description"));
						lateFeesDetail.setDescription(rs.getString("description"));
						lateFeesDetail.setType(rs.getString("type"));
						lateFeesDetail.setFrequency(rs.getString("frequency"));
						lateFeesDetail.setByDays(rs.getString("by_days"));
						lateFeesDetail.setBaseAmount(rs.getDouble("base_amount"));
						lateFeesDetail.setByDate(String.valueOf(rs.getInt("by_date")));
						lateFeesDetail.setCombinationRule(rs.getString("combination_rule"));

						String byDay = rs.getString("by_days");
						if (byDay != null && byDay.length() > 0) {
							String[] byDayArray = byDay.split(",");
							Map<String, Boolean> byDayMap = new HashMap<>();
							for (String day : byDayArray) {
								byDayMap.put(day, true);
							}
							lateFeesDetail.setWeekDays(byDayMap);
						}
						lateFeesDetail.setAmount(rs.getDouble("amount"));
						lateFeesDetail.setMaximumAmount(rs.getDouble("maximum_amount"));
						lateFeesDetail.setInstituteId(rs.getInt("institute_id"));
						return lateFeesDetail;
					}
				});
		if (lateFeesList.size() > 0) {
			return lateFeesList.get(0);
		} else {
			return null;
		}
	}

	@Override
	@Transactional
	public void updateLateFeesDetail(LateFeesDetail lateFeesDetail) {
		String sql = "update late_fees_payment_detail set  "
				+ " description = ? , type = ? , frequency = ? , by_days = ? ,by_date=?, "
				+ " amount = ? , maximum_amount = ?, base_amount = ?, combination_rule = ? where id = ?";

		System.out.println(lateFeesDetail);
		Object[] args = { lateFeesDetail.getDescription(),
				lateFeesDetail.getType(), lateFeesDetail.getFrequency(),
				lateFeesDetail.getByDays(), lateFeesDetail.getByDate(),
				lateFeesDetail.getAmount(), lateFeesDetail.getMaximumAmount(),
				lateFeesDetail.getBaseAmount(), lateFeesDetail.getCombinationRule(), lateFeesDetail.getId() };

		int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.DOUBLE,
				Types.DOUBLE, Types.DOUBLE, Types.VARCHAR, Types.INTEGER };

		getJdbcTemplate().update(sql, args, argTypes);
	}

	@Override
	@Transactional
	public void insertLateFeesDetail(LateFeesDetail lateFeesDetail) throws SQLException {
		String sql = "insert into late_fees_payment_detail "
				+ " (branch_id, description, type, frequency, by_days, by_date, amount, maximum_amount, base_amount, combination_rule) "
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		final PreparedStatement preparedStatement = dataSource.getConnection().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setInt(1, lateFeesDetail.getBranchId());
		preparedStatement.setString(2, lateFeesDetail.getDescription());
		preparedStatement.setString(3, lateFeesDetail.getType());
		preparedStatement.setString(4, lateFeesDetail.getFrequency());
		preparedStatement.setString(5, lateFeesDetail.getByDays());
		preparedStatement.setString(6, lateFeesDetail.getByDate());
		preparedStatement.setDouble(7, lateFeesDetail.getAmount());
		preparedStatement.setDouble(8, lateFeesDetail.getMaximumAmount());
		preparedStatement.setDouble(9, lateFeesDetail.getBaseAmount());
		preparedStatement.setString(10, lateFeesDetail.getCombinationRule());

		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection arg0)
					throws SQLException {
				// TODO Auto-generated method stub
				return preparedStatement;
			}
		}, holder);
		lateFeesDetail.setId(holder.getKey().intValue());
	}
	

	@Override
	@Transactional
	public void deleteLateFeesDetail(Integer lateFeesId) {
		String sql = "update late_fees_payment_detail set is_delete = 'Y' where id = "
				+ lateFeesId;
		getJdbcTemplate().update(sql);
	}

	private boolean getLateFeesApplied(Long feeScheduleId) {
		String sql = "select count(*) as count from fees_schedule where parent_fees_schedule_id = "
				+ feeScheduleId
				+ " OR late_payment_for_fees_schedule = "
				+ feeScheduleId;
		int count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		}
		return false;
	}

	private void updateFeesSchedule(final Long feeScheduleId,
			final Integer paymentId, final String amount) {
		logger.debug("UPDATE FEES SCHEDULE STARTED :::::::");

		final boolean isLateFeeApplied = getLateFeesApplied(feeScheduleId);
		String sql = "select fs.*, sum(p.amount) as paidAmount, slfp.late_payment_charges as student_late_payment_charges, "
				+ "(select IFNULL(sum(late_payment_charges), 0.0) from fees_schedule where late_payment_for_fees_schedule = fs.id) as totalLatePaymentCharges, "
				+ " slfp.is_student_specific, slfp.discount_amount as discount_amount "
				+ "	from fees_schedule as fs  "
				+ " LEFT JOIN student_late_fee_payment as slfp on slfp.fees_schedule_id = fs.id "
				+ "  LEFT JOIN payments as p on p.fees_schedule_id = "
				+ feeScheduleId;

		if (isLateFeeApplied) {
			sql += " where fs.id = (select max(id) as id from fees_schedule "
					+ "where parent_fees_schedule_id = " + feeScheduleId
					+ " OR late_payment_for_fees_schedule = " + feeScheduleId
					+ ")";
		} else {
			sql += " where fs.id =" + feeScheduleId;
		}

		logger.debug("SQL ::::: " + sql);
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {

				double remainingAmount = rs.getDouble("base_amount")
						+ rs.getDouble("totalLatePaymentCharges")
						- rs.getDouble("paidAmount") - rs.getDouble("discount_amount");

				if ("Y".equals(rs.getString("is_student_specific"))) {
					remainingAmount = rs.getDouble("base_amount")
							+ rs.getDouble("student_late_payment_charges")
							- rs.getDouble("discount_amount")
							- rs.getDouble("paidAmount");
				}

				String sql = "update fees_schedule set status = ?, pyment_id = ?, partial_paid_amount = ? "
						+ " WHERE (id = ? OR (parent_fees_schedule_id = ? OR late_payment_for_fees_schedule = ?)) AND status = ?";

				String status = remainingAmount > 0 ? Constants.PAYMENT_STATUS.PARTIAL_PAID
						.get() : Constants.PAYMENT_STATUS.PAID.get();

				Object[] params = { status, paymentId, amount, feeScheduleId,
						feeScheduleId, feeScheduleId,
						Constants.PAYMENT_STATUS.UNPAID };
				int[] types = { Types.VARCHAR, Types.INTEGER, Types.DOUBLE,
						Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.VARCHAR };

				System.out.println("update fees schedule >>>" + sql);
				System.out.println(params);

				int rowsAffcted = getJdbcTemplate().update(sql, params, types);

				logger.debug("Total Amount ::::: "
						+ rs.getDouble("total_amount") + ", PAID AMOUNT ::: "
						+ amount);
				if (remainingAmount > 0) {
					insertIntoFeesSchedule(feeScheduleId, null, rs,
							remainingAmount,
							Constants.PAYMENT_STATUS.UNPAID.get(), null);
				} else if (rowsAffcted == 0) {
					insertIntoFeesSchedule(feeScheduleId, paymentId, rs,
							remainingAmount,
							Constants.PAYMENT_STATUS.PAID.get(), amount);
				}
				if (remainingAmount == 0) {
					sql = "update fees_schedule set status = '" + status
							+ "' where id = " + feeScheduleId;
					getJdbcTemplate().update(sql);
				}
				logger.debug("UPDATE FEES SCHEDULE DONE :::::::");
				return null;
			}
		});
	}

	private void insertIntoFeesSchedule(final Long feeScheduleId,
			final Integer paymentId, ResultSet rs, double remainingAmount,
			String status, String partialPaidAmount) throws SQLException {
		String sql;
		sql = "insert into fees_schedule (fees_id, user_id, base_amount, pyment_id, "
				+ " late_payment_charges, total_amount, due_date, status, "
				+ " reminder_start_date, fee_for_duration, fee_start_date, parent_fees_schedule_id, partial_paid_amount) "
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		Object[] params1 = { rs.getInt("fees_id"), rs.getInt("user_id"),
				rs.getDouble("base_amount"), paymentId,
				rs.getDouble("late_payment_charges"), remainingAmount,
				rs.getDate("due_date"), status,
				rs.getDate("reminder_start_date"),
				rs.getString("fee_for_duration"), rs.getDate("fee_start_date"),
				feeScheduleId, partialPaidAmount };

		int[] types1 = { Types.INTEGER, Types.INTEGER, Types.DOUBLE,
				Types.INTEGER, Types.DOUBLE, Types.DOUBLE, Types.DATE,
				Types.VARCHAR, Types.DATE, Types.VARCHAR, Types.DATE,
				Types.BIGINT, Types.DOUBLE };

		getJdbcTemplate().update(sql, params1, types1);
	}

	@Override
	public TableResponse getOfflinePayments(FeesReportFilter filter) throws ApplicationException {
		String sql = "select payload, id as payment_audit_id from payment_audit where qfix_reference_number = '"+filter.getQfixRefNumber()+"'";
		TableResponse tableResponse = new TableResponse();
		try {
			Map<String, Object> dataMap= getJdbcTemplate().queryForMap(sql);
			String payload = (String) dataMap.get("payload");

			List<PaymentTransaction> transactions = Arrays.asList(new ObjectMapper().readValue(payload, PaymentTransaction[].class));
			List<Long> feesScheduleIds = new ArrayList<>();
			Map<Long, String> amountMap = new HashMap<Long, String>();

			for(PaymentTransaction transaction : transactions){
				feesScheduleIds.add(transaction.getFeeScheduleId());
				amountMap.put(transaction.getFeeScheduleId(), transaction.getAmount());
			}
			String scheduleIds = StringUtils.join(feesScheduleIds, ",");

			String select = " select pa.id as paymentAuditId, fs.id as fee_schedule_id, pa.qfix_reference_number, pa.created_at as challanGenerationDate, pa.payment_mode, pa.status as transactionStatus, "
					+ " concat( if (s.first_name is not null, s.first_name, ''), ' ',  if (s.last_name is not null, s.last_name, ''))  as studentName,  "
					+ " s.registration_code as registrationCode, s.roll_number, s.id as studentId, f.description, fs.due_date as duedate, f.amount as feeamount, f.id as feesId, h.name as head,    "
					+ " st.displayname as standard,  d.displayname as division, IFNULL(c.name, '') as caste,  fs.total_amount as total_amount, "
					+ " i.name as instituteName, b.name as branchName  from student s    inner join fees_schedule fs on fs.user_id = s.user_id " +
					" inner join fees f on f.id = fs.fees_id and f.branch_id = s.branch_id " +
					" inner join branch b on b.id = s.branch_id  inner join institute i on i.id = b.institute_id " +
					" inner join head h on f.head_id = h.id   left join standard st on st.id = s.standard_id " +
					" left join division d on d.id = s.division_id   left join caste c on c.id = s.caste_id " +
					" left join payment_audit as pa on pa.id = pa.id " + 
					" where  fs.late_payment_for_fees_schedule is null  AND fs.parent_fees_schedule_id is null " +
					" AND (fs.is_delete = 'N' OR (fs.is_delete = 'Y' AND fs.status != 'UNPAID'))";

			sql = select + " AND fs.id in ("+scheduleIds+") AND pa.id = "+dataMap.get("payment_audit_id")+" group by fs.id";

			final List<FeesReport> feesReports = new ArrayList<>();

			getJdbcTemplate().query((sql), new RowMapper<FeesReport>() {
				@Override
				public FeesReport mapRow(ResultSet rs, int arg1)
						throws SQLException {
					FeesReport report = null;
					Long feeScheduleId = rs.getLong("fee_schedule_id");
					java.sql.Date challanGenerationDate = rs.getDate("challanGenerationDate");
					String status = rs.getString("transactionStatus");
					report = new FeesReport();
					report.setFeeScheduleId(feeScheduleId);
					report.setFeesId(rs.getInt("feesId"));
					report.setStudentId(rs.getInt("studentId"));
					report.setInstituteName(rs.getString("instituteName"));
					report.setBranchName(rs.getString("branchName"));
					report.setStandard(rs.getString("standard"));
					report.setDivision(rs.getString("division"));
					report.setStudentName(rs.getString("studentName"));
					report.setRollNumber(rs.getString("roll_number"));
					report.setHead(rs.getString("head"));
					report.setRegistrationCode(rs.getString("registrationCode"));
					report.setQfixReferenceNumber(rs.getString("qfix_reference_number"));
					report.setPaymentMode(rs.getString("payment_mode"));
					report.setPaidDate( Constants.USER_DATE_FORMAT.format(challanGenerationDate));
					report.setPaymentAuditId(rs.getLong("paymentAuditId"));
					report.setStatus(status);
					feesReports.add(report);
					return report;
				}
			});

			for(FeesReport report : feesReports){
				report.setPaidAmount(amountMap.get(report.getFeeScheduleId()));
			}

			TableOptions tableOptions = filter.getTableOptions();
			tableResponse.setDraw(tableOptions != null ? tableOptions.getDraw() : 0);
			tableResponse.setRecordsTotal(feesReports.size());
			tableResponse.setRecordsFiltered(feesReports.size());
			tableResponse.setFeesReports(feesReports);
		}catch(Exception e){
			logger.error("", e);
			throw new ApplicationException("Qfix reference number not found.");
		}

		return tableResponse;
	}

	@Override
	@Transactional
	public List<Fees> downloadFeesToUploadUpdate(Integer branchId, String token) {
		String sql = "select f.*, fsc.*, dt.name as display_template_name, s.roll_number, s.registration_code, " +
				" ifnull(st.displayName, '') as standardName, ifnull(c.name, '') as caste,c.id as casteId,"
				+ " h.id as headId, h.name as head, ifnull(fcc.name, '') as feesCodeName, "
				+ " ifnull(d.displayName, '') as divisionName, concat("
				+ " if (s.first_name is not null, s.first_name, ''), ' ', if (s.last_name is not null, s.last_name, '')) "
				+ " as studentName, lfpd.description as late_fee_detail_name from fees f "
				+ " inner join head h on f.head_id = h.id "
				+ " left join caste c on c.id = f.caste_id "
				+ " left join standard st on st.id = f.standard_id "
				+ " left join division d on d.id = f.division_id "
				+ " left join fees_code_configuration fcc on f.fees_code_configuration_id = fcc.id"
				+ " left join student as s on s.id = f.student_id "
				+ " left join late_fees_payment_detail as lfpd on lfpd.id = f.late_fee_detail_id "
				+ " left join display_template as dt on dt.id = f.display_template_id "
				+ " left join fees_split_configuration as fsc on fsc.fees_id = f.id "
				+ " where (f.standard_id = st.id OR f.standard_id is null) "
				+ " AND f.is_delete = 'N' AND f.head_id = h.id AND "
				+ " f.branch_id =" + branchId + " AND f.academic_year_id = "
				+ "(select id from academic_year where is_current_active_year = 'Y' "
				+ " AND branch_id = " + branchId + " ) order by f.id desc";

		final List<SchemeCode> schemeCodes = getSchemeCodes(branchId, token);
		final List<Integer> feesIds = new ArrayList<>();

		logger.debug("get fees by branch sql --" + sql);
		final List<Fees> feesList = new ArrayList<>();
		getJdbcTemplate().query(sql, new RowMapper<Fees>() {
			@Override
			public Fees mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				if(!feesIds.contains(rs.getInt("id"))){
					Fees fees = new Fees();
					fees.setFeesCodeName(rs.getString("feesCodeName"));
					fees.setId(rs.getInt("id"));
					fees.setCaste(rs.getString("caste"));
					fees.setHead(rs.getString("head"));
					fees.setDescription(rs.getString("description"));
					fees.setBranchId(rs.getInt("branch_id"));
					fees.setHeadId(rs.getInt("head_id"));
					fees.setCasteId(rs.getInt("caste_id"));
					fees.setAmount(rs.getString("amount"));
					fees.setFromDate(Constants.USER_DATE_FORMAT.format(rs.getDate("fromdate")));
					fees.setToDate(Constants.USER_DATE_FORMAT.format(rs.getDate("todate")));
					fees.setDueDate(Constants.USER_DATE_FORMAT.format(rs.getDate("duedate")));
					fees.setStandardId(rs.getInt("standard_id"));
					fees.setDivisionId(rs.getInt("division_id"));
					fees.setIsEmail(rs.getString("isemail"));
					fees.setIsSms(rs.getString("issms"));
					fees.setIsNotification(rs.getString("isnotification"));
					fees.setFrequency(rs.getString("frequency"));
					fees.setStandard(rs.getString("standardName"));
					fees.setDivision(rs.getString("divisionName"));
					fees.setAcademicYearId(rs.getInt("academic_year_id"));
					fees.setIsSplitPayment(rs.getString("is_split_payment"));
					fees.setIsPartialPaymentAllowed(rs.getString("is_partial_payment_allowed"));
					fees.setStudentName(rs.getString("studentName"));
					fees.setLateFeeDetailName(rs.getString("late_fee_detail_name"));
					fees.setCurrency(rs.getString("currency"));
					fees.setAllowRepeatEntries(rs.getString("allow_repeat_entries"));

					fees.setRollNo(rs.getString("roll_number"));
					fees.setRegistrationCode(rs.getString("registration_code"));
					fees.setRecurring(rs.getString("recurring"));
					FeesRecurringDetail recurringDetail = new FeesRecurringDetail();
					recurringDetail.setFrequency(rs.getString("recurring_frequency"));
					recurringDetail.setWorkWeeks(rs.getString("by_days"));
					fees.setRecurringDetail(recurringDetail);
					java.sql.Date reminderStartDate = rs.getDate("reminder_start_date");
					if(reminderStartDate != null){
						fees.setReminderStartDate(Constants.USER_DATE_FORMAT.format(reminderStartDate));
					}else {
						fees.setReminderStartDate("");
					}

					fees.setFeeType(rs.getString("fee_type"));
					fees.setDisplayTemplateName(rs.getString("display_template_name"));

					setSchemeCodes(rs.getInt("scheme_code_id"), rs.getString("is_seed_account"), rs.getString("amount"), schemeCodes, fees);
					feesList.add(fees);
					feesIds.add(fees.getId());
				}
				else {
					for(Fees fees : feesList){
						if(fees.getId() .equals(rs.getInt("id"))){
							setSchemeCodes(rs.getInt("scheme_code_id"), rs.getString("is_seed_account"), rs.getString("fsc.amount"), schemeCodes, fees);
							break;
						}
					}
				}
				return null;
			}
		});
		return feesList;
	}

	private void setSchemeCodes(int schemeCodeId, String isSeedAccount, String seedAmount, List<SchemeCode> schemeCodes, Fees fees) {
		for(SchemeCode code : schemeCodes){
			if(schemeCodeId == code.getId()){
				if("Y".equals(isSeedAccount)){
					fees.setSchemeCode(code.getName());
				}
				else {
					fees.setSeedSchemeCode(code.getName());
					fees.setSeedSplitAmount(seedAmount);
				}
				break;
			}
		}
	}

/**
 * addFeesCode while adding fees
 * @author pitabas
 * @param Fees
 * @return int
 *
 */
	@Override
	public int addFeeCode(Fees fees) {
		Integer id=getFeeCode(fees);
		if(id!=null){
			
			fees.setFeesCodeId(id);
		}
		else{
			getJdbcTemplate().update(Query.INSERT_FEES_CODE,new Object[]{fees.getFeesCodeName(),fees.getFeesCodeName(),fees.getBranchId()});
			id=getFeeCode(fees);
			fees.setFeesCodeId(id);
		}
		
		return id;
	}
	
	/**
	 * getFeeCodeId
	 * @author pitabas
	 * @param Fee 
	 * @return Integer
	 *
	 */
	Integer getFeeCode(Fees fees){
		Integer id=null;
		try{
		id=getJdbcTemplate().queryForObject(Query.GET_FEES_CODE,new Object[]{fees.getFeesCodeName(),fees.getBranchId()},Integer.class);
		}catch(Exception e){
			
			e.printStackTrace();
		}
		return id ;
	}

	@Override
	public void updateFeesCode(FeesCode feesCode) throws Exception {
		checkFeesCodeExist(feesCode);
		String sql = "update fees_code_configuration set name = ?, description = ?, branch_id = ? where id = ?";
		
		Object[] args = {
				feesCode.getName(), feesCode.getDescription(), feesCode.getBranchId(), feesCode.getId()
		};

		int[] types = new int[] {
			Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER
		};
		getJdbcTemplate().update(sql, args, types);
	}

	@Override
	public void insertFeesCode(FeesCode feesCode) throws Exception {
		checkFeesCodeExist(feesCode);
		String sql = "insert into fees_code_configuration (name, description, branch_id) values (?, ?, ?)";

		Object[] args = {
				feesCode.getName(), feesCode.getDescription(), feesCode.getBranchId()
		};
		final PreparedStatement preparedStatement = dataSource.getConnection().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, feesCode.getName());
		preparedStatement.setString(2, feesCode.getDescription());
		preparedStatement.setInt(3, feesCode.getBranchId());
		
		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection arg0)
					throws SQLException {
				// TODO Auto-generated method stub
				return preparedStatement;
			}
		}, holder);
		feesCode.setId(holder.getKey().intValue());
	}

	private void checkFeesCodeExist(FeesCode code) throws Exception{
		String sql = "select count(*) from fees_code_configuration where is_delete = 'N' AND name = '"+code.getName()
				+"' and branch_id = " + code.getBranchId();
			if(code.getId() != null){
				sql += " AND id != " + code.getId();
			}
			Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
			if(count > 0){
				throw new Exception("Fees code with this name already exist.");
			}
	}

	@Override
	public FeesCode getFeesCodeById(Integer id) {
		String sql = "select fcc.*, b.institute_id as institute_id from fees_code_configuration as fcc "
				+ " inner join branch as b on fcc.branch_id = b.id where fcc.is_delete = 'N' AND fcc.id = " + id;

		List<FeesCode> feesCodes = getJdbcTemplate().query(sql, new RowMapper<FeesCode>(){
			@Override
			public FeesCode mapRow(ResultSet rs, int arg1) throws SQLException {
				FeesCode feesCode = new FeesCode();
				feesCode.setId(rs.getInt("id"));
				feesCode.setName(rs.getString("name"));
				feesCode.setDescription(rs.getString("description"));
				feesCode.setBranchId(rs.getInt("branch_id"));
				feesCode.setInstituteId(rs.getInt("institute_id"));
				return feesCode;
			}
		});

		if(feesCodes != null && feesCodes.size() > 0){
			return feesCodes.get(0);
		}

		return null;
	}

	@Override
	public List<FeesCode> getAllFeesCode(Integer branchId) {
		String sql = "select fcc.*, b.institute_id as institute_id from fees_code_configuration as fcc "
				+ " inner join branch as b on fcc.branch_id = b.id where fcc.is_delete = 'N' and b.id = " + branchId;

		List<FeesCode> feesCodes = getJdbcTemplate().query(sql, new RowMapper<FeesCode>(){
			@Override
			public FeesCode mapRow(ResultSet rs, int arg1) throws SQLException {
				FeesCode feesCode = new FeesCode();
				feesCode.setId(rs.getInt("id"));
				feesCode.setName(rs.getString("name"));
				feesCode.setDescription(rs.getString("description"));
				feesCode.setBranchId(rs.getInt("branch_id"));
				feesCode.setInstituteId(rs.getInt("institute_id"));
				return feesCode;
			}
		});
		return feesCodes;
	}

	@Override
	public void deleteFeesCode(Integer id){
		String sql = "update fees_code_configuration set is_delete = 'Y' where id = " + id;
		getJdbcTemplate().update(sql);
	}
	
	public Integer auditFeesDelete(final Integer userId, final Fees fee, final String ipAddress) throws JsonProcessingException{
		final String query = "insert into audit_log(entity_type, user_id, operation_type, ip_address, old_data) "
				+ "values(?, ?, ?, ?, ?)";
		Integer id = 0;
		KeyHolder holder = new GeneratedKeyHolder();
		final String feeJson = getJson(fee);
		getJdbcTemplate().update(new PreparedStatementCreator() {
		
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement stmt = connection.prepareStatement(
						query, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, "FEES");
				stmt.setInt(2, userId);
				stmt.setString(3, "DELETE");
				stmt.setString(4, ipAddress);
				stmt.setString(5, feeJson);
				return stmt;
			}
		}, holder);
		
		id = holder.getKey().intValue();
		return id;
	}
	
	public void auditFeesDelete(Integer userId, List<Fees> fees, String ipAddress) throws JsonProcessingException{
		for(Fees fee : fees)
			auditFeesDelete(userId, fee, ipAddress);
	}
	
	public String getJson(Object o) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(o);
		return json;
	}
	
	@Override
	public Integer auditFeesUpdate(final Integer userId, Fees oldFee, final String ipAddress) throws JsonProcessingException {
		Fees newFee = getFeeById(oldFee.getId());
		updateFeesValues(oldFee);
		updateFeesValues(newFee);
		final String oldFeesJson = getJson(oldFee);
		final String newFeesJson = getJson(newFee);
		
		final String query = "insert into audit_log(entity_type, user_id, operation_type, ip_address, old_data, new_data) "
				+ "values(?, ?, ?, ?, ?, ?)";
		Integer id = 0;
		KeyHolder holder = new GeneratedKeyHolder();
		
		getJdbcTemplate().update(new PreparedStatementCreator() {
		
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement stmt = connection.prepareStatement(
						query, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, "FEES");
				stmt.setInt(2, userId);
				stmt.setString(3, "UPDATE");
				stmt.setString(4, ipAddress);
				stmt.setString(5, oldFeesJson);
				stmt.setString(6, newFeesJson);
				return stmt;
			}
		}, holder);
		
		id = holder.getKey().intValue();
		return id;
	}
	
	public void auditFeesUpdate(final Integer userId, List<Fees> oldFeesData, final String ipAddress) throws JsonProcessingException{
		for(Fees fee : oldFeesData){
			auditFeesUpdate(userId, fee, ipAddress);
		}
	}
	
	public List<Fees> getFeesByIds(List<Fees> fees){
		List<Fees> feesList = new LinkedList<Fees>();
		for(Fees fee : fees){
			Fees f = getFeeById(fee.getId());
			feesList.add(f);
		}
		return feesList;
	}
	
	public List<AuditLog> getDeletedFeesData(){
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where entity_type='FEES' and operation_type = 'DELETE'; ";
		
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		
		return auditlogs;
	}
	
	public List<AuditLog> getUpdatedFeesData() throws JsonParseException, JsonMappingException, IOException{
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where entity_type='FEES' and operation_type = 'UPDATE'; ";
		
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		
		Iterator<AuditLog> itr = auditlogs.iterator();
		while(itr.hasNext()){
			AuditLog a = itr.next();
			Fees oldFees = getFeesFromJson(a.getOldData());
			Fees newFees = getFeesFromJson(a.getNewData());
			if(compareFees(oldFees, newFees))
				continue;
			else
				itr.remove();
		}
		
		return auditlogs;
	}
	
	private Fees getFeesFromJson(String s) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		Fees fees = mapper.readValue(s, Fees.class);
		return fees;
	}
	
	private boolean compareFees(Fees oldFees, Fees newFees){
		if((oldFees.getAmount() != null && newFees.getAmount() != null && !oldFees.getAmount().equals(newFees.getAmount())) || 
				(oldFees.getAmount() == null && newFees.getAmount() != null))
			return true;
		if((oldFees.getSchemeCode() != null && newFees.getSchemeCode() != null && !oldFees.getSchemeCode().equals(newFees.getSchemeCode())) || 
				(oldFees.getSchemeCode() == null && newFees.getSchemeCode() != null))
			return true;
		if((oldFees.getDueDate() != null && newFees.getDueDate() != null && !oldFees.getDueDate().equals(newFees.getDueDate())) || 
				(oldFees.getDueDate() == null && newFees.getDueDate() != null))
			return true;
		if((oldFees.getToDate() != null && newFees.getToDate() != null && !oldFees.getToDate().equals(newFees.getToDate())) || 
				(oldFees.getToDate() == null && newFees.getToDate() != null))
			return true;
		if((oldFees.getLatePaymentCharges() != null && newFees.getLatePaymentCharges() != null && !oldFees.getLatePaymentCharges().equals(newFees.getLatePaymentCharges())) || 
				(oldFees.getLatePaymentCharges() == null && newFees.getLatePaymentCharges() != null))
			return true;
		if((oldFees.getStandard() != null && newFees.getStandard() != null && !oldFees.getStandard().equals(newFees.getStandard())) || 
				(oldFees.getStandard() == null && newFees.getStandard() != null))
			return true;
		if((oldFees.getDivision() != null && newFees.getDivision() != null && !oldFees.getDivision().equals(newFees.getDivision())) || 
				(oldFees.getDivision() == null && newFees.getDivision() != null))
			return true;
		if((oldFees.getFeesCodeName() != null && newFees.getFeesCodeName() != null && !oldFees.getFeesCodeName().equals(newFees.getFeesCodeName())) || 
				(oldFees.getFeesCodeName() == null && newFees.getFeesCodeName() != null))
			return true;
		if((oldFees.getSchemeCode() != null && newFees.getSchemeCode() != null && !oldFees.getSchemeCode().equals(newFees.getSchemeCode())) || 
				(oldFees.getSchemeCode() == null && newFees.getSchemeCode() != null))
			return true;
		return false;
	}
	
	public List<AuditLog> getAuditLog(String operation, Integer branchId) throws Exception{
		String op = operation.toUpperCase();
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where operation_type = '"+ op +"' and entity_type = 'FEES'";
				
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		auditlogs = getFeesAuditDataByBranchId(auditlogs, branchId);
		return auditlogs;
	}
	
	public TreeMap<Integer, Object[]> generateFeesExcelData(List<AuditLog> auditLogs, String operationType){
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		int rowNum = 0;
		String headName = "";
		String branchName = "";
		String oldAmount = "";
		String newAmount = "";
		String oldShemeCode = "";
		String newShemeCode = "";
		String oldDueDate = "";
		String newDueDate = "";
		String oldEndDate = "";
		String newEndDate = "";
		Integer oldLatePaymentCharges = 0;
		Integer newLatePaymentCharges = 0;
		String oldStandard = "";
		String newStandard = "";
		String oldDivision = "";
		String newDivision = "";
		String oldFeesCodeName = "";
		String newFeesCodeName = "";
		/*String newMobile = "";
		String oldEmail = ""; 
		String newEmail = "";*/ 
		
		if(operationType.equals("DELETE")){
			Object[] headerArr = new Object[] { "Performed By", "Operation type", 
					"Operation Date",
					"IP Address",
					"Fee Head", 
					"Branch",
					"Amount"};
			excelDataMap.put(rowNum, headerArr);
			
			try {
				for (AuditLog auditLog : auditLogs) {
					if (auditLog != null) {
						if(auditLog.getOldData() != null){
							Fees f = getFeesFromJson(auditLog.getOldData());
							headName = headDao.getHeadById(f.getHeadId()).getName();
							branchName = branchDao.getBranchById(f.getBranchId()).getName();
							oldAmount = f.getAmount();
						}
						rowNum++;
						Object[] dataArr = { auditLog.getUsername(), 
											 auditLog.getOperationType(), 
											 auditLog.getOperationDate(),
											 auditLog.getIpAddress(),
											 headName,
											 branchName,
											 oldAmount};
						excelDataMap.put(rowNum, dataArr);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(operationType.equals("UPDATE")){
			Object[] headerArr = new Object[] { "Performed By", "Operation type", 
					"Operation Date",
					"IP Address",
					"Fee head", 
					"Branch",
					"Old Amount",
					"New Amount",
					"Old Scheme Code",
					"New Scheme Code",
					"Old Due Date",
					"New Due Date",
					"Old End Date",
					"New End Date",
					"Old Late Fees Charges",
					"New Late Fees Charges",
					"Old Standard",
					"New Standard",
					"Old Division",
					"New Division",
					"Old Fees Code",
					"New Fees Code"
					};
			excelDataMap.put(rowNum, headerArr);
			
			try {
				for (AuditLog auditLog : auditLogs) {
					
					if (auditLog != null) {
						if(auditLog.getOldData() != null && auditLog.getNewData() != null){
							Fees oldFees = getFeesFromJson(auditLog.getOldData());
							Fees newFees = getFeesFromJson(auditLog.getNewData());
							if(!compareFees(oldFees, newFees))
								continue;
							headName = headDao.getHeadById(oldFees.getHeadId()).getName();
							branchName = branchDao.getBranchById(oldFees.getBranchId()).getName();
							oldAmount = oldFees.getAmount();
							newAmount = newFees.getAmount();
							oldShemeCode = oldFees.getSchemeCode();   
							newShemeCode = newFees.getSchemeCode();   
							oldDueDate = oldFees.getDueDate();     
							newDueDate = newFees.getDueDate();     
							oldEndDate = oldFees.getToDate();     
							newEndDate = newFees.getToDate();     
							oldLatePaymentCharges = oldFees.getLatePaymentCharges();
							newLatePaymentCharges = newFees.getLatePaymentCharges();
							oldStandard = oldFees.getStandard();    
							newStandard = newFees.getStandard();    
							oldDivision = oldFees.getDivision();    
							newDivision = newFees.getDivision();
							oldFeesCodeName = oldFees.getFeesCodeName();
							newFeesCodeName = newFees.getFeesCodeName();
						}
						rowNum++;
						Object[] dataArr = { auditLog.getUsername(), 
											 auditLog.getOperationType(), 
											 auditLog.getOperationDate(),
											 auditLog.getIpAddress(),
											 headName,
											 branchName,
											 oldAmount,
											 newAmount,
											 oldShemeCode,
											 newShemeCode,
											 oldDueDate,
											 newDueDate,
											 oldEndDate,
											 newEndDate,
											 oldLatePaymentCharges,
											 newLatePaymentCharges,
											 oldStandard,
											 newStandard,
											 oldDivision,
											 newDivision,
											 oldFeesCodeName,
											 newFeesCodeName
											 };
						excelDataMap.put(rowNum, dataArr);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	
		return excelDataMap;
	}
	
	@Override
	public TreeMap<Integer, Object[]> generateFailedTransactionExcelData(
			List<TransactionReport> transactionReports) {
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();

		int rowNum = 0;
		Object[] headerArr = new Object[] { "First name", "Last name", "Registration code", "Roll Number",
				"Fees head","Amount", "Qfix reference number", "Transaction id","Transaction date", "Payment mode",
				"Error message"};
		excelDataMap.put(rowNum, headerArr);

		for (TransactionReport transactionReport : transactionReports) {
			if (transactionReport != null) {
				rowNum++;
				String errorMessage = getPaymentErrorMessage(transactionReport.getPaymentGatewayResponse());
				Double feesId = getFeesIdFromPayload(transactionReport.getPayload());
				String feesHead = headDao.getHeadNameByFeesId(feesId.intValue());
				Object[] dataArr = {
						transactionReport.getFirstName(),
						transactionReport.getLastName(),
						transactionReport.getRegistrationCode(),
						transactionReport.getRollNumber(),
						feesHead,
						transactionReport.getAmount(),
						transactionReport.getQfixReferenceNumber(),
						transactionReport.getPaymentGatewayTransactionId(),
						getTransactionDateTime(transactionReport.getCreatedAt()),
						transactionReport.getPaymentMode(),
						errorMessage
				};
				excelDataMap.put(rowNum, dataArr);
			}
		}
		return excelDataMap;
	}
	
	public TreeMap<Integer, Object[]> generateIncorrectTransactionIdReport(List<IncorrectTransactionIdReport> list){
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		
		int rowNum = 0;
		Object[] headerArr = new Object[] { "Transaction Id", "Settlement Date", "Amount",
											"Merchant Id", "Bank Name", "Qfix Reference Number", 
											"Bank Transaction Id", "Drawer name", "Pickup location", 
											"Pickup Point", "CL Location", "Prod Code"};
		excelDataMap.put(rowNum, headerArr);
		
		for(IncorrectTransactionIdReport i : list){
			if(i != null){
				rowNum++;
				Object[] dataArr = {
						i.getTransactionId(),
						i.getDate(),
						i.getAmount(),
						i.getMerchantId(),
						i.getBankName(),
						i.getQfixReferenceNumber(),
						i.getBankTransactionId(),
						i.getDrawerName(),
						i.getPickupLocation(),
						i.getPickupPoint(),
						i.getClLocation(),
						i.getProdCode()
				};
				excelDataMap.put(rowNum, dataArr);
			}
		}
		return excelDataMap;
	}
	
	 public void writeReport(PrintWriter writer, List<IncorrectTransactionIdReport> list)  {

	        try {

	            ColumnPositionMappingStrategy mapStrategy
	                    = new ColumnPositionMappingStrategy();

	            mapStrategy.setType(IncorrectTransactionIdReport.class);
	            mapStrategy.generateHeader();

	            String[] columns = new String[]{"transactionId", "date", "amount", "type"};
	            mapStrategy.setColumnMapping(columns);

	            StatefulBeanToCsv btcsv = new StatefulBeanToCsvBuilder(writer)
	                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
	                    .withMappingStrategy(mapStrategy)
	                    .withSeparator(',')
	                    .build();

	            btcsv.write(list);

	        } catch (CsvException ex) {

	            log.error("Failed to write incorrect transaction data");
	        }
	    }
	
	
	private String getPaymentErrorMessage(String paymentGatewayResponse){
		String[] items = paymentGatewayResponse.split("\\|");
		try{
			return items[2].split("=")[1].split(":")[0];
		}catch(ArrayIndexOutOfBoundsException e){
			return null;
		}
	}

	private Double getFeesIdFromPayload(String payload){
		
		List<HashMap<String,Object>> data = gson.fromJson(payload,new TypeToken<List<HashMap<String,Object>>>(){}.getType());
		HashMap<String,Object> hasmapData = data.get(0);
		return (Double) hasmapData.get("fees_id");
	}
	
	private String getTransactionDateTime(java.sql.Date date){
		Date createdAt = new Date(date.getTime());
		DateFormat dateFormat = new SimpleDateFormat("dd MMM YYYY - hh:mm a");
		return dateFormat.format(createdAt);
	}
	
	@Override
	public List<TransactionReport> getFailedTransaction(Integer branchId) {
		String query = "SELECT s.first_name AS firstName, s.last_name AS lastName, s.registration_code as registrationCode, ppd.amount, "
					+ " ppd.qfix_reference_number AS qfixReferenceNumber, "
					+ " ppd.payment_gateway_transaction_id AS paymentGatewayTransactionId, pta.payment_gateway_response AS paymentGatewayResponse, "
					+ " pa.mode_of_payment as paymentMode, pta.created_at as createdAt, pta.payment_gateway_response AS paymentGatewayResponse, pa.payload "
					+ " FROM payment.transaction_audit pta "
					+ " JOIN payment.payment_detail ppd ON ppd.id = pta.payment_detail_id "
					+ " JOIN payment_audit pa ON pa.qfix_reference_number = ppd.qfix_reference_number "
					+ " JOIN user u ON u.id = pa.user_id "
					+ " JOIN student s ON s.user_id = u.id WHERE pta.transaction_status_code = 'PG_ERROR' "
					+ " AND s.branch_id = " + branchId;
		
		System.out.println("Selecting all of the failed transactions \n" + query);

		return getJdbcTemplate().query(query, new BeanPropertyRowMapper(TransactionReport.class));
	}
	

	public List<AuditLog> getDeletedFeesData(Integer branchId) throws JsonParseException, JsonMappingException, IOException{
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where entity_type='FEES' and operation_type = 'DELETE'; ";
		log.info("Fees audit data -> " + sql);
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		log.info("List of logs size -> " + auditlogs.size());
		auditlogs = getFeesAuditDataByBranchId(auditlogs, branchId);
		log.info("List of logs by branch id size -> " + auditlogs.size());
		return auditlogs;
	}
	
	public List<AuditLog> getUpdatedFeesData(Integer branchId) throws JsonParseException, JsonMappingException, IOException{
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where entity_type='FEES' and operation_type = 'UPDATE'; ";
		log.info("Updated fees data query -> " + sql);
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		
		Iterator<AuditLog> itr = auditlogs.iterator();
		while(itr.hasNext()){
			AuditLog a = itr.next();
			Fees oldFees = getFeesFromJson(a.getOldData());
			Fees newFees = getFeesFromJson(a.getNewData());
			if(compareFees(oldFees, newFees))
				continue;
			else
				itr.remove();
		}
		log.info("List of logs size before editing with branch list -> " + auditlogs.size());
		auditlogs = getFeesAuditDataByBranchId(auditlogs, branchId);
		log.info("List of logs by branch id after getting branch id -> " + auditlogs.size());
		return auditlogs;
	}
	
	public List<AuditLog> getFeesAuditDataByBranchId(List<AuditLog> auditLogList, int branchId) throws JsonParseException, JsonMappingException, IOException{
		Iterator<AuditLog> itr = auditLogList.iterator();
		while(itr.hasNext()){
			AuditLog a = itr.next();
			Fees f = null;
			
			if(a.getOldData() != null){
				f = getFeesFromJson(a.getOldData());
				log.info("Fees branch id -> " + f.getBranchId() + " " + branchId);
				int feesBranchId = f.getBranchId();
				if(feesBranchId != branchId){
					log.info("Fees branch id not equal -> " + feesBranchId + " " + branchId);
					itr.remove();
				}
			}
		}
		
		return auditLogList;
	}
	
	public List<IncorrectTransactionIdReport> getIncorrectTransactionIdReport(){
		String sql = "select id, transaction_id as transactionId, date, amount,  "
				+ "merchant_id as merchantId, bank_name as bankName, "
				+ "qfix_reference_number as qfixReferenceNumber, "
				+ "bank_transaction_id as bankTransactionId, drawer_name as drawerName, "
				+ "pickup_location as pickupLocation, pickup_point as pickupPoint, "
				+ "cl_location as clLocation, prod_code as prodCode from incorrect_transaction_id";
		List<IncorrectTransactionIdReport> list = getJdbcTemplate().query(sql, new BeanPropertyRowMapper<IncorrectTransactionIdReport>(IncorrectTransactionIdReport.class));
		return list;
	}
	
	void updateFeesValues(Fees fees){
		if(fees.getDivisionId() != null && fees.getDivisionId() > 0){
			String sql = "select displayName from division where id = " + fees.getDivisionId();
			try{
				String divisionName = getJdbcTemplate().queryForObject(sql, String.class);
				log.info(sql +  " " + divisionName);
				fees.setDivision(divisionName);
			}
			catch(Exception e){
				log.info(e.getMessage());
			}
			
		}
		if(fees.getStandardId() != null && fees.getStandardId() > 0){
			String sql = "select displayName from standard where id = " + fees.getStandardId();
			try{
				String standardName = getJdbcTemplate().queryForObject(sql, String.class);
				log.info(sql +  " " + standardName);
				fees.setStandard(standardName);
			}
			catch(Exception e){
				log.info(e.getMessage());
			}
		}
		if(fees.getFeesCodeId() != null && fees.getFeesCodeId() > 0){
			String sql = "select name from qfix.fees_code_configuration where id = " + fees.getFeesCodeId();
			try{
				String name = getJdbcTemplate().queryForObject(sql, String.class);
				log.info(sql +  " " + name);
				fees.setFeesCodeName(name);
			}
			catch(Exception e){
				log.info(e.getMessage());
			}
		}
		if(fees.getSchemeCodeId() != null && fees.getSchemeCodeId() > 0){
			String sql = "select name from payment.payment_gateway_configuration_scheme where id =  " + fees.getSchemeCodeId();
			try{
				String name = getJdbcTemplate().queryForObject(sql, String.class);
				log.info(sql +  " " + name);
				fees.setSchemeCode(name);
			}
			catch(Exception e){
				log.info(e.getMessage());
			}
		}
	}
	
	public List<FeesReport> getSettlementDateNullReport(Integer branchId){
		String sql = "select i.name as instituteName, b.name as branchName, "
				+ "concat( if (s.first_name is not null, s.first_name, ''), ' ', if (s.last_name is not null, s.last_name, '')) as studentName, "
				+ "ps.paid_date, h.name as head, pd.payment_gateway_transaction_id AS paymentGatewayTransactionId, "
				+ "f.amount as feeamount from payment_audit pa "
				+ "join payment.payment_detail pd on pa.qfix_reference_number = pd.qfix_reference_number "
				+ "join qfix.payments ps on pa.id = ps.payment_audit_id "
				+ "join fees f on f.id = ps.fees_id join head h on h.id = f.head_id left "
				+ "join student s on s.user_id = ps.student_id left "
				+ "join qfix.branch b on b.id = s.branch_id  "
				+ "left join institute i on i.id = b.institute_id "
				+ "where pd.settlement_date is null  "
				+ "and STR_TO_DATE(pd.created_at, '%Y-%m-%d') >= STR_TO_DATE('2017-11-17', '%Y-%m-%d') and b.id = " + branchId;
		log.info("Null settlement date query -> " + sql);
		List<FeesReport> report = getJdbcTemplate().query(sql,new BeanPropertyRowMapper<FeesReport>(FeesReport.class));
		return report;
	}
	
	public TreeMap<Integer, Object[]> generateSettlementDateNullReport(List<FeesReport> list){
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		
		int rowNum = 0;
		Object[] headerArr = new Object[] {"Student name", "Institute Name",
				"Branch Name", "Fee Amount", "Transaction Id", "Paid Date", "Fee head", ""};
		excelDataMap.put(rowNum, headerArr);
		
		for(FeesReport i : list){
			if(i != null){
				rowNum++;
				Object[] dataArr = {
						i.getStudentName(),
						i.getInstituteName(),
						i.getBranchName(),
						i.getFeeAmount(),
						i.getPaymentGatewayTransactionId(),
						i.getPaidDate(),
						i.getHead()
				};
				excelDataMap.put(rowNum, dataArr);
			}
		}
		return excelDataMap;
	}
}
