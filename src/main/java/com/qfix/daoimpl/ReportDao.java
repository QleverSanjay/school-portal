package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.dao.IReportDao;
import com.qfix.model.ExcelContentModel;
import com.qfix.model.ReportModel;
import com.qfix.model.Student;
import com.qfix.model.StudentReport;
import com.qfix.model.Subject;
import com.qfix.model.ValidationError;
import com.qfix.service.report.ReportUploadService;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ReadWriteExcelUtil;
import com.qfix.utilities.ReportHeaderConstants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.StudentReportValidator;

@Repository
public class ReportDao extends JdbcDaoSupport implements IReportDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(ReportDao.class);

	@Autowired
	StudentReportValidator studentReportValidator;

	private List<ValidationError> validationErrors;

	public List<ValidationError> getValidationErrors() {
		return validationErrors;
	}

	private void setValidationErrors(List<ValidationError> validationErrors) {
		this.validationErrors = validationErrors;
	}


	@Transactional
	public void uploadStudntReport(MultipartFile file, ReportModel reportModel , String title) throws Exception {

		String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		if(fileExtension .equalsIgnoreCase(".xls") || fileExtension .equalsIgnoreCase(".xlsx")){

			ReportUploadService reportUploadService = new ReportUploadService();
			List<StudentReport> studentReports = reportUploadService.generateStudentReport(file, reportModel.getBranchId(), reportModel.getAcademicYearId(), 
					reportModel.getStandardId(), reportModel.getDivisionId(), reportModel.getSemester());

			// TODO validate student reports here and change code for multi threading (make studentReports object method level)
			if(studentReports != null && studentReports.size() > 0){
				List<ValidationError> validationErrors = validateReports(studentReports, reportModel.getStandardId(), 
						reportModel.getDivisionId(), reportModel.getResultTypeId(), title);

				if(validationErrors.size() > 0){
					setValidationErrors(validationErrors);
					throw new Exception("Some the data is incorrect!");
				}

				logger.debug("uploaded Student reports --"+studentReports);
				insertUploadedStudentReports(studentReports, reportModel.getBranchId(),reportModel.getAcademicYearId(), 
						reportModel.getStandardId(),reportModel.getDivisionId(), reportModel.getSemester(), reportModel.getResultTypeId());
			}
			else {
				logger.error("Empty excel without student reports Ignoring...");
				throw new Exception("Please provide valid student reports.");
			}
		}
		else {
			logger.error("File is not excel...");
			throw new Exception("Please provide only excel sheet.");
		}

	}


	private List<ValidationError> validateReports(List<StudentReport> studentReports,
			Integer standardId, Integer divisionId, Integer resultTypeId, String title) {
		int row = 1;
		studentReportValidator.initData(standardId, divisionId, resultTypeId, studentReports);
		List<ValidationError> validationList = new ArrayList<>();

		for(StudentReport studentReport : studentReports){
			studentReport.setTitle(title);

			ValidationError validationError = ValidatorUtil.validateForAllErrors(studentReport, studentReportValidator);
			if(validationError != null){
				validationError.setRow("RECORD : "+row);
				validationError.setTitle("Student - "+studentReport.getStudentName());
				validationList.add(validationError);	
			}
			row ++;
		}
		return validationList;
	}


	@Transactional
	private void insertUploadedStudentReports( List<StudentReport> studentReports, Integer branchId, Integer academicYearId,
			Integer standardId, Integer divisionId, String semester, Integer resultTypeId) throws SQLException {

		final String studentMarksSql = "insert into student_marks (theory, practical, obtained, " +
				"subject_id, student_result_id, remarks) values (?, ?, ?, ?, ?, ?)";

		for(final StudentReport studentReport : studentReports){
			final Integer studentResultId = getStudentResultIdByInsertingRecord(studentReport,academicYearId, resultTypeId);
			
			if(studentResultId > 0){
				for(final Subject subject : studentReport.getSubjectList()){
					/*final Integer subjectId = getSubjectIdBySubjectName(subject.getName(), branchId, academicYearId,
							standardId, semester, subject.getCategory(), subject.getTotalMarks());*/

					getJdbcTemplate().update (new PreparedStatementCreator() {
						@Override
						public PreparedStatement createPreparedStatement(Connection connection) 
								throws SQLException {
							PreparedStatement studentMarksPreparedStatement = 
										connection.prepareStatement(studentMarksSql);
							studentMarksPreparedStatement.setString(1, subject.getTheory());
							studentMarksPreparedStatement.setString(2, subject.getPractical());
							studentMarksPreparedStatement.setString(3, subject.getObtainedMarks());
							studentMarksPreparedStatement.setInt(4, subject.getId());
							studentMarksPreparedStatement.setInt(5, studentResultId);
							studentMarksPreparedStatement.setString(6, subject.getRemarks());

							return studentMarksPreparedStatement;
						}
					});	
				}
			}
		}
	}


	@Transactional
	private Integer getStudentResultIdByInsertingRecord(final StudentReport report, final Integer academicYearId, 
			final Integer resultTypeId) throws SQLException {
		Integer studentResultId = null;

		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)
					throws SQLException {

				String sql = "insert into student_result (student_id, result, final_grade, position, " +
						"general_remark, gross_total, created_date, academic_year_id, title, result_type_id)" +
						" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setInt(1, report.getId());
				preparedStatement.setString(2, report.getResult());
				preparedStatement.setString(3, report.getFinalGrade());
				preparedStatement.setString(4, report.getPosition());
				preparedStatement.setString(5, report.getGeneralRemark());
				preparedStatement.setString(6, report.getGrossTotal());
				preparedStatement.setDate(7, new java.sql.Date(new Date().getTime()));
				preparedStatement.setInt(8, academicYearId);
				preparedStatement.setString(9, report.getTitle());
				preparedStatement.setInt(10, resultTypeId);
				return preparedStatement;
			}
		}, holder);
		studentResultId = holder.getKey().intValue();
		return studentResultId;
	}

/*
	private Integer getSubjectIdBySubjectName(String subjectName, Integer branchId, Integer academicYearId, Integer standardId, String semester, String category,
			String totalMarks, Map<String, Integer> subjectMap) throws SQLException {
		
		Integer subjectId = null;
		if(subjectMap != null){
			if(subjectMap.containsKey(subjectName)){
				subjectId = subjectMap.get(subjectName);
			}
		}
		return subjectId;
	}*/

/*	private boolean isStudentExist(StudentReport studentReport) {
		boolean flag = false;
		if(studentList != null ){
			for(Student student : studentList){
				if(student.getId().equals(studentReport.getId())
						&& student.getName().equals(studentReport.getStudentName()) 
						&& student.getRollNumber().equals(studentReport.getRoleNumber())){
					
					flag = true;
					break;
				}
			}
		}
		return flag;
	}
*/

	@Transactional
	public Map<String, Integer> setSubjectListBasedOnSemesterAndStandard(Integer standardId, String semester) {
		String sql = "select s.* from subject as s " +
				" INNER JOIN standard_subject ss on ss.subject_id = s.id" +
				" INNER JOIN standard as st on st.id = ss.standard_id" +
				" WHERE s.is_delete = 'N' AND ss.is_delete = 'N' AND st.active = 'Y'" +
				" AND st.id = "+standardId;

		final Map<String, Integer> subjectMap = new HashMap<String, Integer>();
		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
				subjectMap.put(resultSet.getString("name"), resultSet.getInt("id"));
				return null;
			}
		});
		
		return subjectMap;
	}


	@Transactional
	public List<Student> getStudentListBasedOnStandardAndDivision( Integer standardId, Integer divisionId) {
		List<Student> studentList = new ArrayList<>();
		String sql = "SELECT id, first_name , father_name, last_name , " +
				"roll_number, branch_id, standard_id, division_id " +
				"FROM `student` where active = 'Y' AND isDelete = 'N' " +
				"AND standard_id = "+standardId+" AND division_id = "+divisionId;

		System.out.println(sql);
		studentList = getJdbcTemplate().query(sql, new RowMapper<Student>(){
			@Override
			public Student mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				Student student = new Student();
				student.setId(resultSet.getInt("id"));
				String firstName = resultSet.getString("first_name");
				String fatherName = resultSet.getString("father_name");
				String lastName = resultSet.getString("last_name");
				String name = (firstName != null ? firstName+" " : "" )+ (fatherName != null ? fatherName+" " : "")+(lastName != null ? lastName : "");
				student.setName(name);
				student.setRollNumber(resultSet.getString("roll_number"));
				student.setBranchId(resultSet.getInt("branch_id"));
				student.setStandardId(resultSet.getInt("standard_id"));
				student.setDivisionId(resultSet.getInt("division_id"));
				System.out.println("STUDENT:::::::::::::::\n"+student);
				return student;
			}
		});

		return studentList;
	}

	List<CellRangeAddress> cellRangeAddresseList;
	

	@Transactional
	public Workbook generateReportTemplate(ReportModel reportModel){
		List<Student> studentList = getStudentListBasedOnStandardAndDivision(
			reportModel.getStandardId(), reportModel.getDivisionId());

		reportModel = getReportModelByIds(reportModel);
		TreeMap<Integer , List<ExcelContentModel>> dataMap = new TreeMap<>();
		Workbook workbook = null;
		List<ExcelContentModel> data1 = new ArrayList<ExcelContentModel>();		
		data1.add(new ExcelContentModel(true, 1, "School"));
		data1.add(new ExcelContentModel(true, 2, reportModel.getInstitute()));

		List<ExcelContentModel> data2 = new ArrayList<ExcelContentModel>();
		data2.add(new ExcelContentModel(true, 1, "Branch"));
		data2.add(new ExcelContentModel(true, 2, reportModel.getBranch()));

		List<ExcelContentModel> data3 = new ArrayList<ExcelContentModel>();
		data3.add(new ExcelContentModel(true, 1, "AcademicYear"));
		data3.add(new ExcelContentModel(true, 2, reportModel.getAcademicYearStr()));

		List<ExcelContentModel> data4 = new ArrayList<ExcelContentModel>();
		data4.add(new ExcelContentModel(true, 1, "Standard"));
		data4.add(new ExcelContentModel(true, 2, reportModel.getStandard()));

		List<ExcelContentModel> data5 = new ArrayList<ExcelContentModel>();
		data5.add(new ExcelContentModel(true, 1, "Division"));
		data5.add(new ExcelContentModel(true, 2, reportModel.getDivision()));

		/*List<ExcelContentModel> data6 = new ArrayList<ExcelContentModel>();
		data6.add(new ExcelContentModel(true, 1, "Semester"));
		data6.add(new ExcelContentModel(true, 2, reportModel.getSemester()));*/

		dataMap.put(1, data1);
		dataMap.put(2, data2);
		dataMap.put(3, data3);
		dataMap.put(4, data4);
		dataMap.put(5, data5);
//		dataMap.put(6, data6);

		cellRangeAddresseList = new ArrayList<>();
		List<Subject> subjectList = getSubjectList(reportModel);

		dataMap = prepareFirstHeadersAndSubjects(subjectList, dataMap);
		dataMap = prepareSubjectEntities(subjectList, dataMap, reportModel.getResultTypeId());
		dataMap = prepareStudentList(dataMap, studentList);
		logger.debug("Student Report Data to write in excel --"+dataMap);
		workbook = ReadWriteExcelUtil.writeToExcel(dataMap, cellRangeAddresseList, 1, "ReportTemplate.xls");
		return workbook;
	}


	@Transactional
	private List<Subject> getSubjectList(ReportModel reportModel) {
		String sql = "select s.* from subject as s " +
				" INNER JOIN standard_subject ss on ss.subject_id = s.id" +
				" INNER JOIN standard as st on st.id = ss.standard_id" +
				" WHERE s.is_delete = 'N' AND ss.is_delete = 'N' AND st.active = 'Y'" +
				" AND st.id = "+reportModel.getStandardId();
		
		System.out.println("GET SUBJECT LIST ---- "+sql);

		List<Subject> subjectList = getJdbcTemplate().query(sql,  new RowMapper<Subject>(){
			@Override
			public Subject mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Subject subject = new Subject();
				subject.setId(resultSet.getInt("id"));
				subject.setName(resultSet.getString("name"));
				subject.setTheory(resultSet.getString("theory"));
				subject.setPractical(resultSet.getString("practical"));
				return subject;
			}
		});
		System.out.println(new com.google.gson.Gson().toJson(subjectList));
		return subjectList;
	}


	@Transactional
	private TreeMap<Integer, List<ExcelContentModel>> prepareStudentList(
			TreeMap<Integer, List<ExcelContentModel>> dataMap, List<Student> studentList) {

		int rowIndex = dataMap.size()+1;

		if(studentList != null){
			for(Student student : studentList){
				int columnIndex = 0;
				List<ExcelContentModel> studentData = new ArrayList<ExcelContentModel>();
				studentData.add(new ExcelContentModel(true, columnIndex++, student.getId()));
				studentData.add(new ExcelContentModel(true, columnIndex++, student.getName()));
				studentData.add(new ExcelContentModel(true, columnIndex++, student.getRollNumber()));
				dataMap.put(rowIndex, studentData);
				rowIndex++;
			}
		}
		return dataMap;
	}


	private TreeMap<Integer, List<ExcelContentModel>> prepareSubjectEntities(
			List<Subject> subjectList, TreeMap<Integer, List<ExcelContentModel>> dataMap, Integer resultTypeId) {
		int rowIndex = dataMap.size() + 1;
		int columnIndex = 2;
		List<ExcelContentModel> headData1 = new ArrayList<ExcelContentModel>();

		for(int i = 0; i < subjectList.size(); i++ ){
			columnIndex ++;
			headData1.add(new ExcelContentModel(true, columnIndex , ReportHeaderConstants.THEROY_MARKS_TEXT));
			
			columnIndex ++;
			headData1.add(new ExcelContentModel(true, columnIndex , ReportHeaderConstants.PRACTICAL_MARKS_TEXT));

			columnIndex ++;
			headData1.add(new ExcelContentModel(true, columnIndex , ReportHeaderConstants.OBTAINED_MARKS_TEXT));

			/*columnIndex ++;
			headData1.add(new ExcelContentModel(true, columnIndex , ReportHeaderConstants.TOTAL_MARKS_TEXT));*/

			columnIndex ++;
			headData1.add(new ExcelContentModel(true, columnIndex , ReportHeaderConstants.CATEGORY_TEXT));

			columnIndex ++;
			headData1.add(new ExcelContentModel(true, columnIndex , ReportHeaderConstants.REMARKS_TEXT));
		}
		columnIndex ++;
		if(resultTypeId.equals(Constants.RESULT_TYPE_MARKS_ID)){
			headData1.add(new ExcelContentModel(true, columnIndex ++, ReportHeaderConstants.GROSS_TOTAL_TEXT));	
		}
		else if(resultTypeId.equals(Constants.RESULT_TYPE_GRADE_ID)){
			headData1.add(new ExcelContentModel(true, columnIndex ++, ReportHeaderConstants.FINAL_GRADE_TEXT));	
		}
		headData1.add(new ExcelContentModel(true, columnIndex ++, ReportHeaderConstants.RESULT_TEXT));
		headData1.add(new ExcelContentModel(true, columnIndex ++, ReportHeaderConstants.POSITION_TEXT));
		headData1.add(new ExcelContentModel(true, columnIndex ++, ReportHeaderConstants.GENERAL_REMARK_TEXT));
		dataMap.put(rowIndex, headData1);
		return dataMap;
	}


	@Transactional
	private TreeMap<Integer , List<ExcelContentModel>> prepareFirstHeadersAndSubjects(
			List<Subject> subjectList, TreeMap<Integer , List<ExcelContentModel>> dataMap) {
		dataMap.put(dataMap.size() + 1, null);

		int rowIndex = dataMap.size() + 1;

		List<ExcelContentModel> headData1 = new ArrayList<ExcelContentModel>();

		headData1.add(new ExcelContentModel(true, 0, ReportHeaderConstants.STUDENT_ID_TEXT));
		cellRangeAddresseList.add(new CellRangeAddress(rowIndex, (rowIndex + 1), 0, 0));

		headData1.add(new ExcelContentModel(true, 1, ReportHeaderConstants.STUDENT_NAME_TEXT));
		cellRangeAddresseList.add(new CellRangeAddress(rowIndex, (rowIndex + 1), 1, 1));

		headData1.add(new ExcelContentModel(true, 2, ReportHeaderConstants.ROLE_NUMBER_TEXT));
		cellRangeAddresseList.add(new CellRangeAddress(rowIndex, (rowIndex + 1), 2, 2));

		int columnIndex = 3;
		int nextColumnIndex = 0;
		for(Subject subject : subjectList){
			nextColumnIndex = columnIndex + 4;

			headData1.add(new ExcelContentModel(false, columnIndex, subject.getName()));

			cellRangeAddresseList.add(new CellRangeAddress(rowIndex, rowIndex, columnIndex, nextColumnIndex));
			columnIndex = nextColumnIndex + 1;
		}
		dataMap.put(rowIndex , headData1);
		
		return dataMap;
	}

	@Transactional
	private ReportModel getReportModelByIds(final ReportModel reportModel) {
		String sql = "select i.name as institute, b.name as branch," +
			     	 " CONCAT(DATE_FORMAT(ay.from_date,'%d/%m/%Y') ,' - ', DATE_FORMAT(ay.to_date,'%d/%m/%Y')) as academicYear, s.displayName as standard, " +
			     	 " d.displayName as division " +
				     " from institute as i, branch as b,academic_year as ay, standard as s, division as d " +
				     " where i.id = b.institute_id AND b.id = "+reportModel.getBranchId()+" " +
				     " AND ay.id = "+reportModel.getAcademicYearId()+" AND s.id = "+reportModel.getStandardId()+" " +
				     " AND d.id = "+reportModel.getDivisionId();

		logger.debug("get report models SQL --"+sql);

		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
				reportModel.setBranch(resultSet.getString("branch"));
				reportModel.setInstitute(resultSet.getString("institute"));
				reportModel.setAcademicYearStr(resultSet.getString("academicYear"));
				reportModel.setStandard(resultSet.getString("standard"));
				reportModel.setDivision(resultSet.getString("division"));
				return null;
			}
		});

		return reportModel;
	}


	@Transactional
	public List<StudentReport> getReports(ReportModel reportModel) {
		List<StudentReport>  studentReports = new ArrayList<>();
		String sql = "select sr.id, s.first_name, s.father_name, s.last_name,"+
			" s.roll_number as roll_number, sr.result, sr.final_grade, sr.position, sr.general_remark, sr.created_date, " +
			" sr.gross_total,sr.academic_year_id, sr.title as title from student as s, student_result as sr where sr.student_id = s.id " +
			" AND s.branch_id = "+reportModel.getBranchId()+" " +
			" AND s.active = 'Y' AND s.isDelete = 'N' " +
			" AND s.standard_id = "+reportModel.getStandardId()+
			" AND s.division_id = "+reportModel.getDivisionId()+
			" AND sr.academic_year_id = (select id from academic_year where is_current_active_year = 'Y' AND branch_id = s.branch_id )"+
			" AND sr.result_type_id = "+reportModel.getResultTypeId()+
			(reportModel.getRollNumber() != null ? " and s.roll_number like '%"+reportModel.getRollNumber()+"%'" : " " )+
			" order by sr.created_date , sr.id";

		logger.debug("get reports SQL --"+sql);
		
		
		studentReports = getJdbcTemplate().query(sql, new RowMapper<StudentReport>(){
			@Override
			public StudentReport mapRow(ResultSet resultSet, int arg1) throws SQLException {
				StudentReport report = new StudentReport();
				report.setId(resultSet.getInt("id"));
				String firstName =  resultSet.getString("first_name");
				String fatherName =  resultSet.getString("father_name");
				String lastName =  resultSet.getString("last_name");
				String name = (firstName != null ? firstName+" " : "") + (fatherName != null ? fatherName+" " : "") + (lastName != null ? lastName+" " : "");
				report.setStudentName(name);				
				report.setResult(resultSet.getString("result"));
				report.setFinalGrade(resultSet.getString("final_grade"));
				report.setPosition(resultSet.getString("position"));
				report.setGeneralRemark(resultSet.getString("general_remark"));
				report.setGrossTotal(resultSet.getString("gross_total"));
				report.setCreatedDate(resultSet.getDate("created_date"));
				report.setRoleNumber(resultSet.getString("roll_number"));
				report.setAcademicYearId(resultSet.getInt("academic_year_id"));
				report.setTitle(resultSet.getString("title"));
				return report;
			}
		});
		return studentReports;
	}

	@Transactional
	@Override
	public boolean deleteReport(List<Integer> idList) {
		String ids = StringUtils.join(idList, ", ");
		String sql = "delete from student_marks where student_result_id in ("+ids+")";
		getJdbcTemplate().update(sql);

		sql = "delete from student_result where id in ("+ids+")";
		getJdbcTemplate().update(sql);
		return true;
	}
}