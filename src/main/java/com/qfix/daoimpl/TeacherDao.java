package com.qfix.daoimpl;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.config.AppProperties;
import com.qfix.dao.IChatDao;
import com.qfix.dao.TeacherInterfaceDao;
import com.qfix.exceptions.ApplicationException;
import com.qfix.exceptions.ProfileImageUploadException;
import com.qfix.exceptions.ShoppingAccountNotCreatedException;
import com.qfix.model.Master;
import com.qfix.model.PaymentAccount;
import com.qfix.model.StudentExcelReportFilter;
import com.qfix.model.Teacher;
import com.qfix.model.TeacherStandard;
import com.qfix.model.TeacherStandardSubject;
import com.qfix.service.client.notification.Notification;
import com.qfix.service.client.notification.Sender;
import com.qfix.service.file.FileFolderService;
import com.qfix.service.file.FileFolderServiceImpl;
import com.qfix.utilities.Constants;
import com.qfix.utilities.FileConstants;
import com.qfix.utilities.FtlTemplateConstants;
import com.qfix.utilities.SecurityUtil;
import com.qfix.utilities.TemplateParameterConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class TeacherDao.
 */
@Repository
public class TeacherDao extends JdbcDaoSupport implements TeacherInterfaceDao {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	private FileFolderService fileFolderService;
	
	/** The Constant logger. */
	final static Logger logger = Logger.getLogger(TeacherDao.class);
	
	/** The allready exist teacher list. */
	private List<Teacher> allreadyExistTeacherList;
	
	/** The error teacher list. */
	private List<Teacher> errorTeacherList;

	/** The chat dao. */
	@Autowired
	IChatDao chatDao;

	Sender sender;

	public void setSender(Sender sender) {
		this.sender = sender;
	}
	
	@Autowired
	private AppProperties appProperties;

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#getAll(java.lang.Integer)
	 */
	@Transactional
	public List<Teacher> getAll(Integer instituteId)throws SQLException {

		String sql = null;
		if (instituteId != null && instituteId != 0) {
			sql = "select t.*,b.id as branchId,b.name as branch from teacher as t,branch as b " +
					"where t.isDelete = 'N' AND t.branch_id = b.id AND b.institute_id = " + instituteId;
		} else {
			sql = "select t.*,b.id as branchId,b.name as branch from teacher as t,branch as b " +
					"where t.isDelete = 'N' AND t.branch_id = b.id";
		}
		logger.debug("get All teacher SQL --"+sql);

		return getJdbcTemplate().query(sql, new RowMapper<Teacher>() {
			public Teacher mapRow(ResultSet rs, int rownumber)throws SQLException {
		
			Teacher teacher = new Teacher();
			teacher.setId(rs.getInt("id"));
			teacher.setFirstName(rs.getString("firstName"));
			teacher.setMiddleName(rs.getString("middleName"));
			teacher.setLastName(rs.getString("lastName"));
			teacher.setDateOfBirth(rs.getString("dateOfBirth"));
			teacher.setEmailAddress(rs.getString("emailAddress"));
			teacher.setGender(rs.getString("gender"));
			teacher.setPrimaryContact(rs.getString("primaryContact"));
			teacher.setSecondaryContact(rs.getString("secondaryContact"));
			teacher.setAddressLine(rs.getString("addressLine"));
			teacher.setArea(rs.getString("area"));
			teacher.setCity(rs.getString("city"));
			teacher.setPinCode(rs.getString("pinCode"));
			teacher.setUserId(rs.getInt("user_id"));
			teacher.setActive(rs.getString("active"));

			return teacher;
		}});
			
	}

	/**
	 * Update photo path.
	 *
	 * @param id the id
	 * @param photoPath the photo path
	 */
	@Transactional
	public void updatePhotoPath(Integer id, String photoPath) {
			String sql = "update teacher set photo = '"+photoPath+"' where id = "+id;
			getJdbcTemplate().update(sql);
	}

	/**
	 * Save file.
	 *
	 * @param file the file
	 * @param teacher the teacher
	 * @param isUpdate the is update
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Transactional
	@Override
	public String saveFile(MultipartFile file, Teacher teacher, boolean isUpdate) throws IOException{
		String filePath = null;
		//&& student.isFileChanegd()
		if(file != null ){
			String parentPath = teacher.getInstituteId() + File.separator + teacher.getBranchId() + File.separator
					 + FileConstants.PROFILE_DOCUMENTS_PATH + FileConstants.TEACHER_PROFILE_PATH;

			String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
			String fileNameWithExtension = org.apache.commons.lang3.RandomStringUtils.random(30, true, true)
					+ fileExtension;

			if(isUpdate){
				String photo = teacher.getPhoto();
				fileNameWithExtension = (StringUtils.isEmpty(photo)  || 
					photo.endsWith(FileConstants.DEFAULT_PROFILE_IMAGE_NAME) 
					? fileNameWithExtension : photo.substring(photo.lastIndexOf("/") + 1));
			}

			filePath = fileFolderService.createFileInExternalStorage
					 (parentPath, fileNameWithExtension, file.getBytes());
		}
		else if(!isUpdate){
			filePath = appProperties.getFileProps()
					.getExternalDocumentsPath()
					+ FileConstants.PROFILE_DOCUMENTS_PATH
					+ FileConstants.DEFAULT_PROFILE_IMAGE_NAME;
		}
		if(filePath != null){
			filePath = FilenameUtils.separatorsToUnix(filePath);
		}
		logger.debug("teacher profile image path --"+filePath);
		return filePath;
	}
 
	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#uploadTeacher(java.util.List, java.lang.Integer)
	 */
	@Transactional
	public boolean uploadTeacher(List<Teacher> teacherList, final Integer branchId)
			throws Exception {

		errorTeacherList = new ArrayList<Teacher>();
		boolean flag = true;

		logger.debug("Upload teacher started...");

		for (final Teacher teacher : teacherList) {
			teacher.setBranchId(branchId);
			logger.debug("inserting teacher..."+teacher.getEmailAddress());

			try {
				insertTeacher(teacher, null);
			}
			catch (Exception e) {
				errorTeacherList.add(teacher);	
				logger.error("", e);
			}
		}
		if (errorTeacherList.size() > 0) {
			flag = false;
		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#uploadTeacher(java.util.List, java.lang.Integer)
	 */
	@Transactional
	public boolean uploadTeacherStandardSubject(List<TeacherStandardSubject> teacherStandardSubjectList, Integer branchId) throws Exception {

		boolean flag = true;

		logger.debug("Upload teacher started...");

		final String teacherSql = "insert into teacher_standard (teacher_id, standard_id, division_id, subject_id, is_delete) " +
				" values(?, ?, ?, ?, ?)";
		final String teacherWithOutDivisionSql = "insert into teacher_standard (teacher_id, standard_id,subject_id, is_delete) " +
				" values(?, ?, ?, ?)";

		// insert into user table
		for (final TeacherStandardSubject teacherStandardSubject : teacherStandardSubjectList) {

			try {
				final Integer teacherId = teacherStandardSubject.getTeacherId();
				if(teacherId != null ){
					if(teacherStandardSubject.getDivisionId() != null) {
						getJdbcTemplate().update(new PreparedStatementCreator() {
							@Override
							public PreparedStatement createPreparedStatement(Connection connection)
									throws SQLException {
								PreparedStatement preparedStatement = connection.prepareStatement(teacherSql, Statement.RETURN_GENERATED_KEYS);
								preparedStatement.setInt(1, teacherStandardSubject.getTeacherId());
								preparedStatement.setInt(2, teacherStandardSubject.getStandardId());
								preparedStatement.setInt(3, teacherStandardSubject.getDivisionId());

								if(teacherStandardSubject.getSubjectId() != null && teacherStandardSubject.getSubjectId() != 0){
									preparedStatement.setInt(4, teacherStandardSubject.getSubjectId());
								}
								else {
									preparedStatement.setNull(4, Types.NULL);
								}
								preparedStatement.setString(5,"N");
								
								return preparedStatement;
							}
						});
					} else {
						getJdbcTemplate().update(new PreparedStatementCreator() {
							@Override
							public PreparedStatement createPreparedStatement(Connection connection)
									throws SQLException {
								PreparedStatement preparedStatement = connection.prepareStatement(teacherWithOutDivisionSql, Statement.RETURN_GENERATED_KEYS);
								preparedStatement.setInt(1, teacherStandardSubject.getTeacherId());
								preparedStatement.setInt(2, teacherStandardSubject.getStandardId());

								if(teacherStandardSubject.getSubjectId() != null && teacherStandardSubject.getSubjectId() != 0){
									preparedStatement.setInt(3, teacherStandardSubject.getSubjectId());
								}
								else {
									preparedStatement.setNull(3, Types.NULL);
								}

								preparedStatement.setString(4,"N");
								
								return preparedStatement;
							}
						});
					}
					logger.debug("Subjects are assigned to teachers successfully.."+teacherStandardSubject.getTeacher());
				}
			}
			catch (Exception e) {
				logger.error("", e);
				flag = false;
			}
		}
		return flag;
	}
	private void createProfiles(final Teacher teacher) throws Exception{
		/*createShoppingProfile(teacher);
		createUserProfile(teacher.getUserId());
		createChatProfile(teacher);*/

		createPaymentAccount(teacher.getUserId());
		if(!teacher.isTeacherExistsAsParent() ){
			createParent(teacher);
		}
		sendNotification(teacher, false);
	}

	private void sendNotification(final Teacher teacher, boolean isUpdate) {
		String sql = "select t.user_id, i.name as instituteName, b.name as branchName " +
		    "from institute as i, branch as b, teacher as t where i.id = b.institute_id " +
		    "AND t.branch_id = b.id AND t.user_id = "+teacher.getUserId();

		final Map<String, Integer> userIds = new HashMap<>();
		final Map<String, Object> content = new HashMap<String, Object>();
		getJdbcTemplate().query(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Integer userId = resultSet.getInt("user_id");

				userIds.put(Constants.ROLE_TEACHER+"-"+userId, userId);
				content.put(TemplateParameterConstants.INSTITUTE_NAME, resultSet.getString("instituteName"));
				content.put(TemplateParameterConstants.BRANCH_NAME, resultSet.getString("branchName"));

				return null;
			}
		});

		content.put(TemplateParameterConstants.USERNAME, teacher.getEmailAddress());
		content.put(TemplateParameterConstants.PASSWORD, teacher.getPassword());
		// Create notification object to send to notification-service.
		Notification notification = new Notification();
		notification.setContent(content);
		notification.setUserIds(userIds);
		notification.sendEmail(true);
		notification.sendSMS(true);

		notification.setTemplateCode(isUpdate ? FtlTemplateConstants.PROFILE_UPDATE_CODE : 
			FtlTemplateConstants.CREATE_USER_CODE);

		notification.setBatchEnabled(false);

		try {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println("NOTIFICATION_OBJ:::::\n"+mapper.writeValueAsString(notification));		
/*			sender.sendMessage(mapper.writeValueAsString(notification));*/
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Insert into user.
	 *
	 * @param teacher the teacher
	 * @return the integer
	 * @throws NoSuchAlgorithmException the no such algorithm exception
	 * @throws ApplicationException the application exception
	 */
	@Transactional
	private Integer insertIntoUser(final Teacher teacher) throws NoSuchAlgorithmException, ApplicationException {
		Integer userId = null;
		if(!teacher.isTeacherExistsAsParent()){
			final String userSql = "insert into user (username, password, password_salt, temp_password, active, isDelete) " +
					 "values(?, ?, ?, ?, ?, ?)";

			final String password = RandomStringUtils.random(10, true, true).toUpperCase();
			final byte[] randomSalt = SecurityUtil.generateSalt();
			final byte[] passwordHash = SecurityUtil.hashPassword(password, randomSalt);

			final String email = teacher.getEmailAddress();
			KeyHolder holder = new GeneratedKeyHolder();

			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
	            public PreparedStatement createPreparedStatement(Connection connection)
	                    throws SQLException {
	                PreparedStatement ps = connection.prepareStatement(userSql, Statement.RETURN_GENERATED_KEYS);
	                ps.setString(1, email);
	                ps.setBytes(2, passwordHash);
	                ps.setBytes(3, randomSalt);
	                ps.setString(4, password);
	                ps.setString(5, teacher.getActive());
	                ps.setString(6, "N");
	                return ps;
	            }
	        }, holder);

			userId = holder.getKey().intValue();
			teacher.setPassword(password);
		}
		else {
			String sql = "select user_id from parent " +
				"where email = '"+teacher.getEmailAddress()+"' OR primaryContact = '"+teacher.getPrimaryContact()+"'";
			userId = getJdbcTemplate().queryForObject(sql, Integer.class);
		}
		insertIntoUserRole(userId, Constants.ROLE_ID_TEACHER);
		return userId;
	}

	/**
	 * Insert into user role.
	 *
	 * @param userId the user id
	 * @param roleId the role id
	 */
	@Transactional
	private void insertIntoUserRole(Integer userId, Integer roleId) {
		String sql = "insert into user_role values ("+userId+", "+roleId+")";
		getJdbcTemplate().update(sql);
	}

	/**
	 * Creates the parent.
	 *
	 * @param teacher the teacher
	 * @param userId the user id
	 * @throws SQLException the sQL exception
	 */
	@Transactional
	private void createParent(final Teacher teacher) throws SQLException {

		int parentInserted = getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into parent (firstname, middlename, lastname, dob, email, " +
					"gender, primaryContact, secondaryContact, addressLine, area, " +
					"city, pincode, user_id, active, photo) " +
					"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preStatement = connection.prepareStatement(sql);
				preStatement.setString(1, teacher.getFirstName());
				preStatement.setString(2, teacher.getMiddleName());
				preStatement.setString(3, teacher.getLastName());
				preStatement.setString(4, teacher.getDateOfBirth());
				preStatement.setString(5, teacher.getEmailAddress());
				preStatement.setString(6, teacher.getGender());
				preStatement.setString(7, teacher.getPrimaryContact());
				preStatement.setString(8, teacher.getSecondaryContact());
				preStatement.setString(9, teacher.getAddressLine());
				preStatement.setString(10, teacher.getArea());
				preStatement.setString(11, teacher.getCity());
				preStatement.setString(12, teacher.getPinCode());
				preStatement.setInt(13, teacher.getUserId());
				preStatement.setString(14, "Y");
				preStatement.setString(15, teacher.getPhoto());
				return preStatement;
			}
		});
		if(parentInserted > 0){
			insertIntoUserRole(teacher.getUserId(), Constants.ROLE_ID_PARENT);
		}
	}

	/**
	 * Creates the shopping profile.
	 *
	 * @param teacher the teacher
	 * @param userId the user id
	 * @throws SQLException the sQL exception
	 * @throws ShoppingAccountNotCreatedException the shopping account not created exception
	 */
/*	@Transactional
	private void createShoppingProfile(Teacher teacher) throws SQLException, ShoppingAccountNotCreatedException{
		ShoppingDao shoppingDao = new ShoppingDao();
		int shoppingprofileId = shoppingDao.createShoppingAccount(teacher);
		insertShoppingProfile(teacher, shoppingprofileId);
		logger.debug("shopping profile created for teacher.."+teacher.getUserId());
	}*/

	/**
	 * Insert shopping profile.
	 *
	 * @param teacher the teacher
	 * @param userId the user id
	 * @param shoppingprofileId the shoppingprofile id
	 * @throws SQLException the sQL exception
	 */
	@Transactional
	private void insertShoppingProfile(final Teacher teacher, final int shoppingprofileId)
			throws SQLException {

		getJdbcTemplate().update(new  PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into user_mall_profile " +
					"(user_id, shopping_profile_id,username, password) values (?, ?, ?, ?)";
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1, teacher.getUserId());
				ps.setInt(2, shoppingprofileId);
				ps.setString(3, teacher.getEmailAddress());
				ps.setString(4, teacher.getPassword());
				return ps;
			}
		});
	}

	/**
	 * Creates the chat profile.
	 *
	 * @param teacher the teacher
	 * @param userId the user id
	 * @throws SQLException the sQL exception
	 */
	/*@Transactional
	private void createChatProfile(final Teacher teacher) {
		final String username = RandomStringUtils.random(10, true, true) + "@"+Constants.DOMAIN_NAME;
		final UserResponse userResponse = chatDao.createChatAccount(username, teacher.getFirstName(), teacher.getLastName(), "");
		if(userResponse != null){
			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection)throws SQLException {
					String sql = "insert into user_chat_profile (login, password, email, chat_user_profile_id, user_id) values (?,?,?,?,?)";
					PreparedStatement ps = connection.prepareStatement(sql);
					ps.setString(1, userResponse.getLogin());
					ps.setString(2, userResponse.getPassword());
					ps.setString(3, userResponse.getEmail());
					ps.setLong(4, userResponse.getChatUserProfileId());
					ps.setInt(5, teacher.getUserId());
					return ps;
				}
			});
			// attach chat groups by standard.
			Map<Integer, String> groupChatIds = getGroupChatIdsByStandard(teacher);
			if(groupChatIds != null){
				// TODO insert GROUP related data.
				//insertGroupRelatedData(groupChatIds.keySet(), teacher.getUserId());
				attachOrDeAttachChatGroupsByStandard(groupChatIds,  userResponse.getChatUserProfileId(), teacher.getUserId(), teacher.getId(), true);
			}	
		}
		else {
			logger.error("Failed to create chat user profile for teacher::::::"+teacher.getEmailAddress());
		}
		logger.debug("Chat profile created for user as teacher --"+teacher.getUserId());
	}*/


	/**
	 * Gets the group chat ids by standard.
	 *
	 * @param teacher the teacher
	 * @return the group chat ids by standard
	 */
	@Transactional
	private Map<Integer , String> getGroupChatIdsByStandard(final Teacher teacher){
		final Map<Integer, String> groupChatIds = new HashMap<>();

		if(teacher.getTeacherStandardList() != null  && teacher.getTeacherStandardList().size() > 0){
			// retrive all group chat ids by standard.
			for(TeacherStandard teacherStandard : teacher.getTeacherStandardList()){
				String sql = "select g.chat_dialogue_id as groupChatId, g.id as groupId FROM `group` as g " +
					" INNER JOIN group_standard as gs on g.id = gs.group_id " +
					" LEFT JOIN group_user as gu on g.id = gu.group_id " +
					" WHERE g.isDelete = 'N' AND g.active = 'Y' " +
					" AND gs.standard_id = "+teacherStandard.getStandardId()+
					" group by gs.group_id, standard_id";

				System.out.println("getGroupChatIdsByStandard SQL :::::::::::\n"+sql);

				getJdbcTemplate().query(sql, new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
						groupChatIds.put(resultSet.getInt("groupId"), resultSet.getString("groupChatId"));
						return null;
					}
				});
			}
		}
		return groupChatIds;
	}


	/**
	 * Attach or deAttach chat groups by standard.
	 * Attaches or deAttaches teacher to chat groups based on standards which are assigned to teacher or deAssigned in case of update.
	 *
	 * @param groupChatIds the group chat ids
	 * @param chatUserProfileId the chat user profile id
	 * @param isAttach the is attach
	 */
	@Transactional
	private void attachOrDeAttachChatGroupsByStandard(Map<Integer, String> groupChatIds, long chatUserProfileId, Integer userId, Integer teacherId, boolean isAttach) {
		System.out.println("Gorup Chat Ids to attach or deattach ::::::::: isAttach ::: "+isAttach+"\n"+groupChatIds);

		if(groupChatIds != null){
			Set<Integer> groupIdSet = groupChatIds.keySet();
			if(groupIdSet != null && groupIdSet.size() > 0){
				for(Integer groupId : groupIdSet){

					String groupChatId = groupChatIds.get(groupId);
					if(isAttach){
						chatDao.updateChatPublicGroups(groupChatId, chatUserProfileId+"", "");
						String sql = "insert into group_user (group_id, user_id) values ("+groupId+", "+userId+")";
						getJdbcTemplate().update(sql);
					}
					else {
						chatDao.updateChatPublicGroups(groupChatId, "", chatUserProfileId+"");
						String sql = "delete from group_user where group_id = "+groupId+" AND user_id = "+userId+"";
						getJdbcTemplate().update(sql);
					}
				}
				updateGroupRelatedData(groupIdSet, userId, teacherId, isAttach);
			}
		}
	}


	@Transactional
	private void updateGroupRelatedData(Set<Integer> groupIdSet, final Integer userId, final Integer studentId, boolean isAttach) {

		updateForumData(groupIdSet, userId, isAttach);
		if(!isAttach){
			String groupIds = org.apache.commons.lang.StringUtils.join(groupIdSet, ", ");
			String sql = "update user_event set status = 'D' where user_id = "+userId+" " +
				" AND event_group_id in (select id from event_group where group_id in("+groupIds+"))";
			getJdbcTemplate().update(sql);
			
			// Delete from notice_individual
			sql = "delete from notice_individual where notice_id in" +
				"(select notice_id from notice_group where group_id in ("+groupIds+")) " +
				" AND user_id = "+userId + " AND for_user_id = "+ userId;
			getJdbcTemplate().update(sql);
		}
		else {
			// update notice Data.
			insertNoticeData(groupIdSet, userId);

			insertEventData(groupIdSet, userId, studentId);
		}
	}


	@Transactional
	private void updateForumData(Set<Integer> groupIdSet, Integer userId, boolean isAttach) {
		Set<Long> forumGroupIds = getForumGrouIds(groupIdSet);
		if(forumGroupIds != null && forumGroupIds .size() > 0){
			String sql = "select forum_profile_id from user_forum_profile where user_id = "+userId;
			Integer userForumId = getJdbcTemplate().queryForObject(sql, Integer.class);
			if(userForumId != 0){
				for(Long forumGroupId : forumGroupIds){
					linkOrDelinkUserToForum(forumGroupId, userForumId, isAttach);
				}	
			}
		}
	}

	@Transactional
	private void linkOrDelinkUserToForum(long groupForumId, int userForumId, boolean isAttach){
		ForumProfileDao forumProfileDao = new ForumProfileDao();
		Set<Integer> userForumProfileIdList = new HashSet<>();
		userForumProfileIdList.add(userForumId);

		if(isAttach){
			forumProfileDao.linkUserToForum(groupForumId, userForumProfileIdList);			
		}else {
			forumProfileDao.deLinkUserToForum(groupForumId, userForumProfileIdList);	
		}
	}

	@Transactional
	private Set<Long> getForumGrouIds(Set<Integer> groupIdSet) {
		Set<Long> forumGroupIds = null;
		String groupIds = org.apache.commons.lang.StringUtils.join(groupIdSet, ", ");
		String sql = "select forum_group_id from group_forum where group_id in("+groupIds+")";
		List<Long> forumGroupIdList = getJdbcTemplate().query(sql, new RowMapper<Long>(){
			@Override
			public Long mapRow(ResultSet resultSet, int arg1) throws SQLException {
				
				return resultSet.getLong("forum_group_id");
			}
		});
		if(forumGroupIdList.size() > 0){
			forumGroupIds = new HashSet<>(forumGroupIdList);
		}
		return forumGroupIds;
	}

	@Transactional
	private void insertNoticeData(Set<Integer> groupIdSet, final Integer userId) {

		String groupIds = org.apache.commons.lang.StringUtils.join(groupIdSet, ", ");
		String sql = "select * from notice_group where group_id in ("+groupIds+")";
		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Integer noticeId = resultSet.getInt("notice_id");
				String sql = "select count(*) as count from notice_individual " +
					"where notice_id = "+noticeId+" AND user_id = "+userId + " AND for_user_id = "+userId;
				Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
				if(count == 0){
					sql = "insert into notice_individual (notice_id, user_id, for_user_id) values ("+noticeId+", "+userId+", "+userId+")";
					getJdbcTemplate().update(sql);
				}
				return null;
			}
		});
	}


	@Transactional
	private void insertEventData(Set<Integer> groupIdSet, final Integer userId, final Integer teacherId) {
		 for(Integer groupId : groupIdSet){
			// update event data.
		 	String sql = "select * from event_group where group_id = "+groupId ;
			getJdbcTemplate().query(sql, new RowMapper<String>(){
				@Override
				public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
					Integer eventGroupId = resultSet.getInt("id");
					String sql = "select count(*) as count from user_event where user_id = "+userId+" AND event_group_id = "+eventGroupId;
					Integer count = getJdbcTemplate().queryForObject(sql , Integer.class);
					if(count > 0){
						sql = "update user_event set status = 'N' where user_id = "+userId+" AND event_group_id = "+eventGroupId;
						getJdbcTemplate().update(sql);
					}
					else {
						sql = "insert into user_event (user_id, role_id, entity_id, event_id, event_group_id, status, for_user_id) " +
							" values ("+userId+", "+Constants.ROLE_ID_TEACHER+", "+teacherId+
							", "+resultSet.getInt("event_id")+", "+eventGroupId+", 'N', "+userId+")";
						getJdbcTemplate().update(sql);
					}
					return null;
				}
			});
		}
		
	}

	/**
	 * Gets the group chat ids by standard.
	 *
	 * @param standardIds the standard ids
	 * @return the group chat ids by standard
	 */
	@Transactional
	private Map<Integer, String> getGroupChatIdsByStandard(List<Integer> standardIds, Integer userId, boolean isAdd){
		final Map<Integer, String> groupChatIds = new HashMap<>();

		if(standardIds != null  && standardIds.size() > 0){
			// retrive all group chat ids by standard.
			for(Integer standardId : standardIds){
				String sql = "select g.id as groupId, g.chat_dialogue_id as groupChatId FROM `group` as g " +
					" INNER JOIN group_standard as gs on g.id = gs.group_id " +
					" WHERE g.isDelete = 'N' AND g.active = 'Y' " +
					" AND gs.standard_id = "+standardId+
					" AND "+(isAdd ? "NOT " : "")+" EXISTS(select gu.group_id " +
					" from group_user as gu where gu.user_id = "+userId+" AND g.id = gu.group_id) "+
					" group by group_id, standard_id";

				System.out.println("getGroupChatIdsByStandard :::: "+sql);
				getJdbcTemplate().query(sql, new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
						groupChatIds.put(resultSet.getInt("groupId"), resultSet.getString("groupChatId"));
						return null;
					}
				});
			}
		}
		return groupChatIds;
	}


	/**
	 * Creates the user profile.
	 *
	 * @param userId the user id
	 * @throws SQLException the sQL exception
	 */
	/*@Transactional
	private void createUserProfile(final Integer userId) throws SQLException{
		// get inserted teacher id from database for user profile forum
		String sql = "select u.temp_password as password, " +
				"t.emailAddress as email, t.branch_id as branchId, " +
				"bfd.read_write_group_id as rwgId, " +
				"bfd.forum_category_id as schoolId " +
				"from teacher as t, user as u, branch_forum_detail as bfd " +
				"where u.id = t.user_id AND bfd.branch_id = t.branch_id AND t.user_id = "+userId;
		getJdbcTemplate().query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet resultSet) throws SQLException {
				final String password = resultSet.getString("password");
				final String email = resultSet.getString("email");
				final Integer schoolId = resultSet.getInt("schoolId");
				final Integer rwgId = resultSet.getInt("rwgId");
				final String username = RandomStringUtils.random(10, true, true) + "@"+Constants.DOMAIN_NAME;
				final CreateUserResponse userResponse = new ForumProfileDao()
						.createUserAccount(username,password,email,schoolId,rwgId, true, false);
				if(userResponse != null){
					getJdbcTemplate().update(new PreparedStatementCreator() {
						@Override
						public PreparedStatement createPreparedStatement(Connection connection)throws SQLException {
							String sql = "insert into user_forum_profile " +
								"(login, password, forum_profile_id, is_active, is_delete ,user_id) " +
								"values (?, ?, ?, ?, ?, ?)";
							PreparedStatement ps = connection.prepareStatement(sql);
							ps.setString(1, username);
							ps.setString(2, password);
							ps.setInt(3, userResponse.getForum_profile_id());
							ps.setString(4, "Y");
							ps.setString(5, "N");
							ps.setInt(6, userId);
							return ps;
						}
					});	
				}
			}
		});
	}*/

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#insertTeacher(com.qfix.model.Teacher, org.springframework.web.multipart.MultipartFile)
	 */
	@Transactional
	public boolean insertTeacher(final Teacher teacher, MultipartFile file)
			throws ShoppingAccountNotCreatedException, Exception {
		boolean flag = false;
		
		Integer userId = insertIntoUser(teacher);
		teacher.setUserId(userId);

		if(userId != null){
			// Now insert the teacher into database
			KeyHolder holder = new GeneratedKeyHolder();
			teacher.setUserId(userId);

			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection)
						throws SQLException {
					String sql = "insert into teacher (firstName, middleName, lastName, " +
						" dateOfBirth, emailAddress, gender, primaryContact, secondaryContact, " +
						" addressLine, area, city, pinCode, user_id, active, branch_id, isDelete, state_id, district_id, taluka_id)"+ 
						" values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?)";

					PreparedStatement preparedStatement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
					preparedStatement.setString(1, teacher.getFirstName());
					preparedStatement.setString(2, teacher.getMiddleName());
					preparedStatement.setString(3, teacher.getLastName());
					preparedStatement.setString(4, !StringUtils.isEmpty(teacher.getDateOfBirth())?teacher.getDateOfBirth():null);
					preparedStatement.setString(5, !StringUtils.isEmpty(teacher.getEmailAddress())?teacher.getEmailAddress():null);
					preparedStatement.setString(6, teacher.getGender());

					preparedStatement.setString(7, teacher.getPrimaryContact());
					preparedStatement.setString(8, teacher.getSecondaryContact());
					preparedStatement.setString(9, teacher.getAddressLine());
					preparedStatement.setString(10, teacher.getArea());
					preparedStatement.setString(11, teacher.getCity());
					preparedStatement.setString(12, teacher.getPinCode());
					preparedStatement.setInt(13, teacher.getUserId());
					preparedStatement.setString(14, teacher.getActive());
					preparedStatement.setInt(15, teacher.getBranchId());
					preparedStatement.setString(16, "N");

					if(teacher.getStateId() == null){
						preparedStatement.setNull(17, java.sql.Types.NULL);
					}
					else {
						preparedStatement.setInt(17, teacher.getStateId());
					}

					if(teacher.getDistrictId() == null){
						preparedStatement.setNull(18, java.sql.Types.NULL);
					}
					else {
						preparedStatement.setInt(18, teacher.getDistrictId());
					}

					if(teacher.getTalukaId() == null){
						preparedStatement.setNull(19, java.sql.Types.NULL);
					}
					else {
						preparedStatement.setInt(19, teacher.getTalukaId());
					}
					return preparedStatement;
				}
			}, holder);
			teacher.setId(holder.getKey().intValue());
			
			insertIntoContact(teacher.getFirstName(), teacher.getLastName(), teacher.getPrimaryContact(), 
					teacher.getEmailAddress(), teacher.getCity(), teacher.getPhoto(), userId);

			insertTeacherStandard(teacher.getId(), teacher.getTeacherStandardList());

			createProfiles(teacher);
			
			String photoPath = null;
			try {
				photoPath = saveFile(file, teacher, true);
				updatePhotoPath(teacher.getId(), photoPath);
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("File not uploaded..", e);
			}

			flag = true;
		}
		return flag;
	}

	/**
	 * Insert teacher standard.
	 *
	 * @param teacherId the teacher id
	 * @param teacherStandardList the teacher standard list
	 */
	@Transactional
	private void insertTeacherStandard(Integer teacherId, List<TeacherStandard> teacherStandardList) {
		if(teacherStandardList != null){
			for(TeacherStandard teacherStandard : teacherStandardList){
				if(teacherStandard.getStandardId() != null){

					boolean isDivisionAvailable = teacherStandard.getDivisionId() != null;

					// check if subjects available
					if(teacherStandard.getSubjectList() != null && teacherStandard.getSubjectList().size() > 0){
						for(Master master : teacherStandard.getSubjectList()){
							String sql = "insert into teacher_standard (teacher_id, standard_id, "
									+(isDivisionAvailable ? "division_id, " : "") +" subject_id ) " +
									"values ("+teacherId+", "+teacherStandard.getStandardId()+", "
									+(isDivisionAvailable ? teacherStandard.getDivisionId()+", " : "") +""+master.getId()+")";
							getJdbcTemplate().update(sql);
						}
					}
					else {
						String sql = "insert into teacher_standard (teacher_id, standard_id "
								+(isDivisionAvailable ? ", division_id" : "") +") " +
								" values ("+teacherId+", "+teacherStandard.getStandardId()+" "
								+(isDivisionAvailable ? ", "+teacherStandard.getDivisionId() : "") +")";
						getJdbcTemplate().update(sql);
					}
				}
			}
		}
	}

	/**
	 * Creates the payment account.
	 *
	 * @param userId the user id
	 * @throws SQLException the sQL exception
	 */
	@Transactional
	private void createPaymentAccount(Integer userId)throws SQLException {
		long accountNumber = PaymentAccount.generateAccountNumberWithCheckDigit();
		String sql = "insert into user_payment_account (account_type, user_id, account_number) " +
				"values('QFIXPAY', "+userId+", '"+accountNumber+"')";

		getJdbcTemplate().update(sql);
		logger.debug("payment account created..."+userId);
	}

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#getTeacherById(java.lang.Integer)
	 */
	@Transactional
	public Teacher getTeacherById(Integer id) {
		String sql = "select t.*, i.id as instituteId, i.name as instituteName, b.name as branchName " +
					 "from teacher as t, institute as i, branch as b "
					 + "where  i.id = b.institute_id AND b.id = t.branch_id AND t.id = " + id;

		return getJdbcTemplate().query(sql, new RowMapper<Teacher>() {
			public Teacher mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				final Teacher teacher = new Teacher();
				teacher.setId(resultSet.getInt("id"));
				teacher.setFirstName(resultSet.getString("firstName"));
				teacher.setMiddleName(resultSet.getString("middleName"));
				teacher.setLastName(resultSet.getString("lastName"));
				teacher.setDateOfBirth((resultSet.getString("dateOfBirth") != null) ? (resultSet.getString("dateOfBirth")) : null);
				teacher.setEmailAddress(resultSet.getString("emailAddress"));

				teacher.setGender(resultSet.getString("gender"));
				teacher.setPrimaryContact(resultSet.getString("primaryContact"));
				teacher.setSecondaryContact(resultSet.getString("secondaryContact"));
				teacher.setAddressLine(resultSet.getString("addressLine"));
				teacher.setArea(resultSet.getString("area"));
				teacher.setCity(resultSet.getString("city"));
				teacher.setPinCode(resultSet.getString("pinCode"));
				teacher.setUserId(resultSet.getInt("user_id"));
				teacher.setActive(resultSet.getString("active"));
				teacher.setPhoto(resultSet.getString("photo"));
				teacher.setBranchId(resultSet.getInt("branch_id"));
				teacher.setInstituteId(resultSet.getInt("instituteId"));
				teacher.setIsDelete(resultSet.getString("isDelete"));
				teacher.setInstituteName(resultSet.getString("instituteName"));
				teacher.setBranchName(resultSet.getString("branchName"));
				teacher.setRoleId(Constants.ROLE_ID_TEACHER);
				teacher.setDistrictId(resultSet.getInt("district_id"));
				teacher.setStateId(resultSet.getInt("state_id"));
				teacher.setTalukaId(resultSet.getInt("taluka_id"));

				// Get Teacher standard data.
				List<TeacherStandard> teacherStandardList = getTeacherStandardsByTeacherId(teacher.getId());
				teacher.setTeacherStandardList(teacherStandardList);
				return teacher;
			}
		}).get(0);
	}

	/**
	 * Gets the teacher standards by teacher id.
	 *
	 * @param teacherId the teacher id
	 * @return the teacher standards by teacher id
	 */
	@Transactional
	private List<TeacherStandard> getTeacherStandardsByTeacherId(final Integer teacherId){
		final List<TeacherStandard> teacherStandardList = new ArrayList<>();
		
		String sql = " SELECT ts.*, st.displayName as standardName, d.displayName as divisionName , "+
				" GROUP_CONCAT(s.name SEPARATOR ', ' ) as subjects FROM `teacher_standard` ts "+
				" INNER JOIN standard st on ts.standard_id = st.id "+
				" LEFT JOIN division as d on d.id = ts.division_id "+
				" LEFT JOIN subject s on s.id = ts.subject_id "+
				" where teacher_id = "+ teacherId +
				" group by ts.standard_id, ts.division_id ";

		
		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {

				final TeacherStandard teacherStandard = new TeacherStandard();
				teacherStandard.setStandard(resultSet.getString("standardName"));
				teacherStandard.setStandardId(resultSet.getInt("standard_id"));

				if(resultSet.getInt("division_id") != 0 && (teacherStandard.getDivisionId() == null 
						|| (teacherStandard.getDivisionId() != null 
						&& resultSet.getInt("division_id") != teacherStandard.getDivisionId()))){

					teacherStandard.setDivision(resultSet.getString("divisionName"));
					teacherStandard.setDivisionId(resultSet.getInt("division_id"));
				}

				int divisionId = (teacherStandard.getDivisionId() != null && teacherStandard.getDivisionId() != 0 ? teacherStandard.getDivisionId() : 0);
				String sql = "select su.name as subjectName, su.id as subjectId from teacher_standard as ts " +
						" LEFT JOIN subject as su on ts.subject_id = su.id " +
						" WHERE ts.standard_id = "+teacherStandard.getStandardId() +
						" AND ts.division_id "+ (divisionId != 0 ? " = "+divisionId : " is null") +
						" AND ts.teacher_id = "+teacherId;

				System.out.println("Standard - "+teacherStandard.getStandardId() +", DivsionId - "+teacherStandard.getDivisionId());	
				final List<Master> subjectList = new ArrayList<>();
				final List<String> subjects = new ArrayList<>();

				getJdbcTemplate().query(sql, new RowMapper<Object>() {
					@Override
					public Object mapRow(ResultSet rs, int arg1) throws SQLException {
						System.out.println("Standard - "+teacherStandard.getStandardId() +", DivsionId - "+teacherStandard.getDivisionId() +", SubjectId - "+ rs.getInt("subjectId"));
						if(rs.getInt("subjectId") != 0){
							Master subject = new Master();
							subject.setId(rs.getInt("subjectId"));
							subject.setName(rs.getString("subjectName"));
							subjectList.add(subject);
							subjects.add(subject.getName());
						}
						return null;
					}
				});
				
				if(subjects != null){
					teacherStandard.setSubjects(org.apache.commons.lang3.StringUtils.join(subjects, ", "));
				}
				teacherStandard.setSubjectList(subjectList);
				teacherStandardList.add(teacherStandard);
				System.out.println("::::::::::"+teacherStandard);
				return null;
			}
		});
		return teacherStandardList;
	}

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#updateTeacher(com.qfix.model.Teacher, org.springframework.web.multipart.MultipartFile)
	 */
	@Transactional
	public boolean updateTeacher(final Teacher teacher,  MultipartFile file) throws ProfileImageUploadException{
		boolean flag = false;

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "update user set active = ?, isDelete = ? where id = ?";
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, teacher.getActive());
				preparedStatement.setString(2, "N");
				preparedStatement.setInt(3, teacher.getUserId());
				return preparedStatement;
			}
		});

		System.out.println("Value of the teacher object ::::::"+teacher);

		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection)throws SQLException {
				String sql = "update teacher set firstName = ? , middleName = ? , lastName = ? , dateOfBirth = ?, emailAddress = ?, "
						+ "gender = ?, primaryContact = ? , secondaryContact = ? , addressLine = ? , area = ?, city = ?,"
						+ " pinCode = ?, user_id = ?, active = ?, branch_id = ?, isDelete = ?,  "
						+" district_id = ?, state_id = ?, taluka_id = ? "
						+ " where id = ?";

				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, teacher.getFirstName());
				preparedStatement.setString(2, teacher.getMiddleName());
				preparedStatement.setString(3, teacher.getLastName());
				preparedStatement.setString(4, teacher.getDateOfBirth());
				preparedStatement.setString(5, teacher.getEmailAddress());
				preparedStatement.setString(6, teacher.getGender());
				preparedStatement.setString(7, teacher.getPrimaryContact());
				preparedStatement.setString(8, teacher.getSecondaryContact());
				preparedStatement.setString(9, teacher.getAddressLine());
				preparedStatement.setString(10, teacher.getArea());
				preparedStatement.setString(11, teacher.getCity());
				preparedStatement.setString(12, teacher.getPinCode());
				preparedStatement.setInt(13, teacher.getUserId());
				preparedStatement.setString(14, teacher.getActive());
				preparedStatement.setInt(15, teacher.getBranchId());
				preparedStatement.setString(16, "N");
				if(teacher.getDistrictId() != null && teacher.getDistrictId() != 0){
					preparedStatement.setInt(17, teacher.getDistrictId());	
				}
				else {
					preparedStatement.setNull(17, Types.NULL);
				}
				
				if(teacher.getStateId() != null && teacher.getStateId() != 0){
					preparedStatement.setInt(18, teacher.getStateId());
				}
				else {
					preparedStatement.setNull(18, Types.NULL);
				}
				
				if(teacher.getTalukaId() != null && teacher.getTalukaId() != 0){
					preparedStatement.setInt(19, teacher.getTalukaId());
				}
				else {
					preparedStatement.setNull(19, Types.NULL);
				}
				preparedStatement.setInt(20, teacher.getId());
				return preparedStatement;
			}
		});

		updateContact(teacher.getFirstName(), teacher.getLastName(), teacher.getPrimaryContact(), teacher.getEmailAddress(), 
				teacher.getCity(), teacher.getPhoto(), teacher.getUserId());

		String sql = "select t.*, i.id as instituteId " +
				"from teacher as t, institute as i, branch as b  " +
				"where b.id = t.branch_id AND i.id = b.institute_id AND t.id = "+teacher.getId();

		getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				teacher.setInstituteId(resultSet.getInt("instituteId"));
				teacher.setBranchId(resultSet.getInt("branch_id"));
				teacher.setPhoto(resultSet.getString("photo"));
				return null;
			}
		});

		String photoPath = null;
		try {
			photoPath = saveFile(file, teacher, true);
			updatePhotoPath(teacher.getId(), photoPath);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("File not uploaded..", e);
		}

		List<Integer> previousStandards  = getPreviousStandardsForTeacherAndDelete(teacher.getId());
		System.out.println("Previous Standards :: "+previousStandards);
		insertTeacherStandard(teacher.getId(), teacher.getTeacherStandardList());
		updateTeacherStandardsAndChatGroups(teacher, previousStandards);

		logger.debug("teacher updated --"+teacher.getId());
		flag = true;
		return flag;
	}

	@Transactional
	private List<Integer> getPreviousStandardsForTeacherAndDelete(Integer id) {
		String sql = "select standard_id from teacher_standard where teacher_id = "+id +" AND is_delete = 'N' group by standard_id";
		List<Integer> standardIds = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getInt("standard_id");
			}
		});
		sql = "delete from teacher_standard where teacher_id = "+id;
		getJdbcTemplate().update(sql);
		return standardIds;
	}

	@Transactional
	private void updateTeacherStandardsAndChatGroups(final Teacher teacher, List<Integer> previousTeacherStandards) {

		final List<Integer> deletedStandards = new ArrayList<>();
		final List<Integer> addedStandards = new ArrayList<>();

		if(teacher.getTeacherStandardList() != null){
			for(TeacherStandard currentTeacherStandard : teacher.getTeacherStandardList()){
				
				// check if current standard exist in previous list of standard.
				if(!previousTeacherStandards.contains(currentTeacherStandard.getStandardId())){
					addedStandards.add(currentTeacherStandard.getStandardId());
				}
			}
 		}

		for(Integer previousStandardId : previousTeacherStandards){
			boolean isStandardAvailable = false;
			if(teacher.getTeacherStandardList() != null){
				for(TeacherStandard currentTeacherStandard : teacher.getTeacherStandardList()){

					// check if previous standard is equal to current standards.
					if(!previousStandardId.equals(currentTeacherStandard.getStandardId())){
						isStandardAvailable = true;
					}
				}	
			}

			if(!isStandardAvailable){
				deletedStandards.add(previousStandardId);
			}
		}

		System.out.println("Addded Standards :::::::::: "+addedStandards);
		System.out.println("DELETED Standards :::::::::: "+deletedStandards);
		final Map<Integer, String> addedChatGorupMap =  getGroupChatIdsByStandard(addedStandards, teacher.getUserId(), true);
		final Map<Integer, String> deletedChatGorupMap = getGroupChatIdsByStandard(deletedStandards, teacher.getUserId(), false);

		if(addedChatGorupMap.size() > 0 && deletedChatGorupMap.size() > 0){
			String sql = "select user_id , chat_user_profile_id from user_chat_profile where " +
					" user_id = (select user_id from teacher where id = "+teacher.getId()+")";

			getJdbcTemplate().query(sql, new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					teacher.setUserId(resultSet.getInt("user_id"));

					attachOrDeAttachChatGroupsByStandard(deletedChatGorupMap, resultSet.getInt("chat_user_profile_id"), teacher.getUserId(), teacher.getId(), false);
					attachOrDeAttachChatGroupsByStandard(addedChatGorupMap, resultSet.getInt("chat_user_profile_id"), teacher.getUserId(), teacher.getId(), true);

					return null;
				}
			});	
		}
	}

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#deleteTeacher(java.lang.Integer)
	 */
	@Transactional
	public boolean deleteTeacher(final Integer id) {
		String sql = "select user_id from teacher where id = " + id;
		getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {

				String username = RandomStringUtils.random(40, true, true);
				String mobile = RandomStringUtils.random(5, false, true);
				int userId = resultSet.getInt("user_id");
				String sql = "update teacher set isDelete = 'Y', emailAddress = '"+username+"', primaryContact = '"+mobile+"' where id = " + id;
				getJdbcTemplate().update(sql);
				deleteParentIfOneChild(userId, username, mobile);
				sql = "delete from user_role where user_id = "+userId+" AND role_id = "+Constants.ROLE_ID_TEACHER;
				getJdbcTemplate().update(sql);
				return 1;
			}
		});

		return true;
	}


	private void deleteParentIfOneChild(int userId, String username, String mobile) {
		String sql = "select count(*) as count from parent as p " +
			" INNER JOIN branch_student_parent as bsp on bsp.parent_id = p.id " +
			" WHERE bsp.isDelete = 'N' AND p.active = 'Y' AND p.user_id = "+userId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);

		if(count.equals(0)){
			sql = "update parent set email = '"+username+"', primaryContact = '"+mobile+"' where user_id = "+userId;
			getJdbcTemplate().update(sql);

			sql = "update user set username = '"+username+"' where id = "+userId;
			getJdbcTemplate().update(sql);

		}
	}

	/**
	 * Delete forum user.
	 *
	 * @param userId the user id
	 * @throws SQLException the sQL exception
	 */
	@Transactional
	private void deleteForumUser(int userId) throws SQLException {
		String str = "select forum_profile_id from user_forum_profile where user_id = "+userId;
		List<Integer> forumProfileIdList = getJdbcTemplate().query(str, new RowMapper<Integer>(){
			public Integer mapRow(ResultSet rs,int row) throws SQLException{
				return rs.getInt("forum_profile_id");
			}
		});

		new ForumProfileDao().deleteUsers(new HashSet<Integer>(forumProfileIdList));
	}

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#getUploadingFailedRecords()
	 */
	public List<Teacher> getUploadingFailedRecords() {
		return errorTeacherList;
	}

	/**
	 * Gets the allready exist teacher list.
	 *
	 * @return the allready exist teacher list
	 */
	public List<Teacher> getAllreadyExistTeacherList() {
		return allreadyExistTeacherList;
	}

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#getTeacherByBranchId(java.lang.Integer)
	 */
	@Transactional
	public List<Teacher> getTeacherByBranchId(Integer branchId) {
		String sql = "select t.*,b.id as branchId, b.name as branch from teacher as t, "
				+ "branch as b where t.isDelete = 'N' AND t.branch_id = b.id AND t.branch_id ="
				+ branchId + " order by t.id desc";
		

		return  getJdbcTemplate().query(sql, new RowMapper<Teacher>() {
			public Teacher mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				Teacher teacher = new Teacher();
				teacher.setId(resultSet.getInt("id"));
				teacher.setFirstName(resultSet.getString("firstName"));
				teacher.setMiddleName(resultSet.getString("middleName"));
				teacher.setLastName(resultSet.getString("lastName"));
				teacher.setDateOfBirth(resultSet.getString("dateOfBirth"));
				teacher.setEmailAddress(resultSet.getString("emailAddress"));
				teacher.setGender(resultSet.getString("gender"));
				teacher.setPrimaryContact(resultSet.getString("primaryContact"));
				teacher.setSecondaryContact(resultSet.getString("secondaryContact"));
				teacher.setAddressLine(resultSet.getString("addressLine"));
				teacher.setArea(resultSet.getString("area"));
				teacher.setCity(resultSet.getString("city"));
				teacher.setPinCode(resultSet.getString("pinCode"));
				teacher.setUserId(resultSet.getInt("user_id"));
				teacher.setActive(resultSet.getString("active"));
				teacher.setPhoto(resultSet.getString("photo"));
				teacher.setBranchId(resultSet.getInt("branch_id"));
				teacher.setIsDelete(resultSet.getString("isDelete"));
				return teacher;
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.qfix.dao.TeacherInterfaceDao#updateProfile(com.qfix.model.Teacher, org.springframework.web.multipart.MultipartFile)
	 */
	@Transactional
	public boolean updateProfile(final Teacher teacher, MultipartFile file) throws IOException, Exception {
		boolean flag = false;

		String sql = "update teacher set emailAddress = ?, primaryContact = ?, "
				+ "secondaryContact = ?, addressLine = ?, area = ?, city = ?, pinCode = ?, photo = ? where id = ?";

		Object[] teacherProfileParams = new Object[] {
				teacher.getEmailAddress(),
				teacher.getPrimaryContact(),
				teacher.getSecondaryContact(),
				teacher.getAddressLine(),
				teacher.getArea(),
				teacher.getCity(),
				teacher.getPinCode(),
				teacher.getPhoto(),
				teacher.getId()
		};
			
		int[] teacherProfileTypes = new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,
			  Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,
			  Types.VARCHAR,Types.VARCHAR,Types.INTEGER
		};
			
		getJdbcTemplate().update(sql,teacherProfileParams, teacherProfileTypes);

		if (teacher.getPassword() != null) {
			
			byte[] randomSalt = SecurityUtil.generateSalt();
			byte[] passwordHash = SecurityUtil.hashPassword(teacher.getPassword(), randomSalt);
			
			sql = "update `user` set password = ?, password_salt = ?, temp_password = ? " +
				  "where id = (select user_id from teacher where id = ?)";

			Object[] userParams = new Object[] {passwordHash,randomSalt,teacher.getPassword(),teacher.getId()};
			int[] userTypes = new int[] { Types.BLOB,Types.BLOB,Types.VARCHAR,Types.INTEGER};
			getJdbcTemplate().update(sql,userParams, userTypes);
		}
		

		// get inserted teacher institute Name and branch name 	
		String queryStr = "select t.*, i.id as instituteId, i.name as instituteName, b.name as branchName " +
			    "from institute as i, branch as b, teacher as t where i.id = b.institute_id " +
			    "AND t.branch_id = b.id AND t.id = "+teacher.getId();
		
		getJdbcTemplate().queryForObject(queryStr, new RowMapper<Teacher>(){
			public Teacher mapRow(ResultSet resultSet,int row) throws SQLException{
				teacher.setInstituteName(resultSet.getString("instituteName"));
				teacher.setBranchName(resultSet.getString("branchName"));
				teacher.setId(resultSet.getInt("id"));
				teacher.setInstituteId(resultSet.getInt("instituteId"));
				teacher.setBranchId(resultSet.getInt("branch_id"));
				teacher.setPhoto(resultSet.getString("photo"));
				teacher.setUserId(resultSet.getInt("user_id"));
				return null;
			}
		});

		String photoPath = saveFile(file, teacher, true);
		if(photoPath != null){
			updatePhotoPath(teacher.getId(), photoPath);
		}
		
		flag = true;
		sendNotification(teacher, true);

		return flag;
	}


	@Override
	public List<Teacher> getTeacherReport(StudentExcelReportFilter filter) {
		
		String sql = "select distinct(t.id), t.*, b.id as branchId, b.name as branch, sta.name as state, "
				+" dis.name as district, tal.name as taluka "
				+" from teacher as t "
				+" inner join branch as b on b.id =  t.branch_id "
				+" left outer join teacher_standard as ts on ts.teacher_id = t.id "
				+" LEFT OUTER JOIN state as sta on sta.id = t.state_id "
				+" LEFT OUTER JOIN district as dis on dis.id = t.district_id "
				+" LEFT OUTER JOIN taluka as tal on tal.id = t.taluka_id "
				+" where t.isDelete = 'N'  "
				+" and t.branch_id = '"+filter.getBranchId()+"' "
				+(filter.getStandardId() != null ? " AND ts.standard_id = '"+filter.getStandardId()+"'" : "")
				+(filter.getDivisionId() != null ? " AND ts.division_id = '"+filter.getDivisionId()+"'" : "")
				+" order by t.firstName, t.lastName  ";

		return  getJdbcTemplate().query(sql, new RowMapper<Teacher>() {
			public Teacher mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				Teacher teacher = new Teacher();
				teacher.setId(resultSet.getInt("id"));
				teacher.setFirstName(resultSet.getString("firstName"));
				teacher.setMiddleName(resultSet.getString("middleName"));
				teacher.setLastName(resultSet.getString("lastName"));
				teacher.setDateOfBirth(resultSet.getString("dateOfBirth"));
				teacher.setEmailAddress(resultSet.getString("emailAddress"));
				teacher.setGender(resultSet.getString("gender"));
				teacher.setPrimaryContact(resultSet.getString("primaryContact"));
				teacher.setSecondaryContact(resultSet.getString("secondaryContact"));
				teacher.setAddressLine(resultSet.getString("addressLine"));
				teacher.setArea(resultSet.getString("area"));
				teacher.setCity(resultSet.getString("city"));
				teacher.setPinCode(resultSet.getString("pinCode"));
				teacher.setUserId(resultSet.getInt("user_id"));
				teacher.setActive(resultSet.getString("active"));
				teacher.setPhoto(resultSet.getString("photo"));
				teacher.setBranchId(resultSet.getInt("branch_id"));
				teacher.setIsDelete(resultSet.getString("isDelete"));
				teacher.setState(resultSet.getString("state"));
				teacher.setDistrict(resultSet.getString("district"));
				teacher.setTaluka(resultSet.getString("taluka"));
				return teacher;
			}
		});
	}
	/**
	 * @param userId
	 */
	public Integer getIdByUserId(Integer userId){
		String sql="select id from teacher where user_id=?";
		Object[] input={userId};
		return getJdbcTemplate().queryForObject(sql,Integer.class,input);
		
	
	}
	/**
	 * 
	 * @param teacher
	 */
	@Transactional
	public boolean updateProfileFirstTime(Teacher teacher){
		boolean flag=false;
		String sql = "update teacher set emailAddress = ?, primaryContact = ?, firstName=?, lastName=?, dateOfBirth=?, gender=?"
				+" where id = ?";

		Object[] teacherProfileParams = new Object[] {
				teacher.getEmailAddress(),
				teacher.getPrimaryContact(),
				teacher.getFirstName(),
				teacher.getLastName(),
				teacher.getDateOfBirth(),
				teacher.getGender(),
				teacher.getId()
		};
			
		int[] teacherProfileTypes = new int[] { Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,
			  Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,
			  Types.INTEGER
		};
			
		getJdbcTemplate().update(sql,teacherProfileParams, teacherProfileTypes);
		flag=true;
		return flag;
		
	}
	 @Override
	 public List<Teacher> exportTeacherData(Integer branchId, Integer standardId,
			Integer divisionId) {

		String sql = " select t.*, b.name as branchName, u.username as username, u.temp_password as password "
				+" from "
				+" teacher as t INNER JOIN branch as b on b.id = t.branch_id "
				+" INNER JOIN user as u on u.id = t.user_id "
				+" LEFT JOIN teacher_standard  as ts on ts.teacher_id = t.id "
				+" LEFT JOIN standard as st on st.id = ts.standard_id AND st.active = 'Y'"
				+" LEFT JOIN division as d on d.id = ts.division_id AND d.active = 'Y'"
				+" where "
				+" t.isDelete = 'N' AND t.branch_id = '"+branchId+"' "
				+(standardId != null ? " AND ts.standard_id = '"+standardId+"'" : "")
				+(divisionId != null ? " AND ts.division_id = '"+divisionId+"'" : "")
				+" group by t.id order by t.firstName, t.lastName ";

		List<Teacher> teachers = getJdbcTemplate().query(sql, new RowMapper<Teacher>(){
			@Override
			public Teacher mapRow(ResultSet rs, int arg1) throws SQLException {
				Teacher teacher = new Teacher();
				teacher.setFirstName(rs.getString("firstName"));
				teacher.setLastName(rs.getString("lastName"));
				teacher.setEmailAddress(rs.getString("emailAddress"));
				teacher.setPrimaryContact(rs.getString("primaryContact"));
				teacher.setBranch(rs.getString("branchName"));
				teacher.setUsername(rs.getString("username"));
				teacher.setPassword(rs.getString("password"));
				return teacher;
			}
		});
		System.out.println(teachers);
		return teachers;
	}
		private void insertIntoContact(String firstname, String surname,
				String mobile, String emailAddress, String city, String photo, Integer userId) {
			String sql = "insert into contact (email, phone, user_id, photo, firstname, lastname, city) values (?, ?, ?, ?, ?, ?, ?)";
			
			Object[] args = {emailAddress, mobile, userId, photo, firstname, surname, city};
			
			int[] types = new int[] {
				Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR
			};
			getJdbcTemplate().update(sql, args, types);
			
			System.out.println("Insert contact done ....");
		}


		private void updateContact(String firstname, String surname,
				String mobile, String emailAddress, String city, String photo, Integer userId) {
			String sql = "update contact set email = ?, phone = ?, photo = ?, firstname = ?, lastname = ?, city = ? where user_id = ?";

			Object[] args = {emailAddress, mobile, photo, firstname, surname, city, userId};

			int[] types = new int[] {
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR, Types.INTEGER
			};
			getJdbcTemplate().update(sql, args, types);
			System.out.println("Update Contact Done.....");
		}

	}
