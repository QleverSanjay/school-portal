package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.qfix.dao.IAcademicYearDao;
import com.qfix.exceptions.YearAllreadyExistException;
import com.qfix.model.AcademicYear;

@Repository
public class AcademicYearDao extends JdbcDaoSupport implements IAcademicYearDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(AcademicYearDao.class);

	@Transactional
	public List<AcademicYear> getAll(Integer branchId){
		String sql = null;
		if(branchId != null && branchId != 0){
			sql = "select * from academic_year where is_delete = 'N' AND  branch_id = "+branchId;
		}else{
			sql = "select * from academic_year where is_delete = 'N'";
		}

		return getJdbcTemplate().query(sql, new RowMapper<AcademicYear>() {
			public AcademicYear mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				AcademicYear academicYear = new AcademicYear();
				academicYear.setId(resultSet.getInt("id"));
				academicYear.setFromDate(resultSet.getString("from_date"));
				academicYear.setToDate(resultSet.getString("to_date"));
				academicYear.setIsCurrentActiveYear(resultSet.getString("is_current_active_year"));
				academicYear.setBranchId(resultSet.getInt("branch_id"));
				return academicYear;
			}
		});
	}


	private boolean yearAllreadyExist(AcademicYear academicYear) {
		String sql = "select count(*) as count from academic_year where " +
			     " ('"+academicYear.getFromDate()+"' between from_date and to_date " +
			     " OR '"+academicYear.getToDate()+"' between from_date and to_date)" +
			     " AND branch_id = "+academicYear.getBranchId()+
			     " AND is_delete = 'N' "+
			     (academicYear.getId() != null ? " AND id != "+academicYear.getId() : "");
		
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			return true;
		}
		updateYearStatus(academicYear);
		return false;
	}

	@Transactional
	public boolean updateYearStatus(AcademicYear academicYear){
		boolean flag = false;
		if(academicYear.getIsCurrentActiveYear().equals("Y")){
			if(academicYear.getIsCurrentActiveYear().equals("Y")){
				String sql = "update academic_year set is_current_active_year = ? " +
						     "where branch_id = "+academicYear.getBranchId();
				Object[] params = new Object[] {"N"};
				int[] types = new int[] { Types.VARCHAR};
				getJdbcTemplate().update(sql, params, types);
				flag = true;
			}	
		}
		return flag;
	}

	@Transactional
	public boolean updateAcademicYear(AcademicYear academicYear) throws YearAllreadyExistException{
		boolean flag = false;
		if(!yearAllreadyExist(academicYear)){
			String sql = "update academic_year set from_date = ?, to_date = ?,is_current_active_year = ?," +
						" branch_id = ? where id = ?";
			Object[] params = new Object[] {academicYear.getFromDate(), 
				academicYear.getToDate(),
				academicYear.getIsCurrentActiveYear(),
				academicYear.getBranchId(),
				academicYear.getId()
			};
			int[] types = new int[] { Types.DATE,Types.DATE,Types.CHAR,Types.INTEGER, Types.INTEGER};
			getJdbcTemplate().update(sql, params, types);
			flag = true;
		}else {
			logger.error("acadmic year exist..");
			throw new YearAllreadyExistException("This academic year already exist in this Branch.");
		}
		return flag;
	}

	@Transactional
	public boolean insertAcademicYear(AcademicYear academicYear)throws YearAllreadyExistException {
		boolean flag= false;
		String sql = "insert into academic_year (from_date, to_date, is_current_active_year, branch_id)" +
					" values (?, ?, ?, ?)";
		if(!yearAllreadyExist(academicYear)){
				Object[] params = new Object[] {
						academicYear.getFromDate(), 
						academicYear.getToDate(),
						academicYear.getIsCurrentActiveYear(),
						academicYear.getBranchId()};
				
				int[] types = new int[] { Types.DATE,Types.DATE,Types.CHAR,Types.INTEGER};	
				
				getJdbcTemplate().update(sql, params, types);
				
				flag = true;
				logger.debug("acadmic year created..");
		}else {
				logger.error("acadmic year exist..");
				throw new YearAllreadyExistException("This academic year is already in this Branch.");
		}
		return flag;
	}

	@Transactional
	public AcademicYear getYearById(Integer id){
		String sql = "select ay.*, i.id as instituteId from academic_year as ay, branch as b, institute as i " +
					"where ay.branch_id = b.id AND b.institute_id = i.id AND ay.id = "+id;			

		List<AcademicYear> acedemicYearList =  getJdbcTemplate().query(sql, new RowMapper<AcademicYear>() {
				public AcademicYear mapRow(ResultSet resultSet, int rownumber)
						throws SQLException {
					AcademicYear academicYear = new AcademicYear();
					academicYear.setId(resultSet.getInt("id"));
					academicYear.setFromDate(resultSet.getString("from_date"));
					academicYear.setToDate(resultSet.getString("to_date"));
					academicYear.setIsCurrentActiveYear(resultSet.getString("is_current_active_year"));
					academicYear.setBranchId(resultSet.getInt("branch_id"));
					academicYear.setInstituteId(resultSet.getInt("instituteId"));
					return academicYear;
				}
			});
		return acedemicYearList.get(0);
	}

	@Transactional
	public boolean deleteAcademicYear(Integer id) throws Exception {
		//delete from AcademicYear ..
		String sql = "select count(*) as count from academic_year where id = "+id +" AND is_current_active_year = 'Y'";
		Integer count =  getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			throw new Exception("This is currently active year can`t delete.");
		}
		sql = "update academic_year set is_delete = 'Y' where id = "+id;
		getJdbcTemplate().update(sql);
		return true;
	}

	@Transactional
	public List<AcademicYear> getActiveYear(Integer branchId){
		String sql = null;
		sql = "select * from academic_year where is_current_active_year = 'Y' AND branch_id = "+branchId;

		return getJdbcTemplate().query(sql, new RowMapper<AcademicYear>() {
			public AcademicYear mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				AcademicYear academicYear = new AcademicYear();
				academicYear.setId(resultSet.getInt("id"));
				academicYear.setFromDate(resultSet.getString("from_date"));
				academicYear.setToDate(resultSet.getString("to_date"));
				academicYear.setIsCurrentActiveYear(resultSet.getString("is_current_active_year"));
				academicYear.setBranchId(resultSet.getInt("branch_id"));
				return academicYear;
			}
		});		
	}

	@Transactional
	public boolean validateAcademicYear(Integer branchId, Integer academicYearId){
		String sql = null;
		sql = "select * from academic_year where is_current_active_year = 'Y' AND branch_id = "+branchId + " AND id = "+academicYearId;

		List<AcademicYear> academicYearList = getJdbcTemplate().query(sql, new RowMapper<AcademicYear>() {
			public AcademicYear mapRow(ResultSet resultSet, int rownumber)
					throws SQLException {
				AcademicYear academicYear = new AcademicYear();
				academicYear.setId(resultSet.getInt("id"));
				academicYear.setFromDate(resultSet.getString("from_date"));
				academicYear.setToDate(resultSet.getString("to_date"));
				academicYear.setIsCurrentActiveYear(resultSet.getString("is_current_active_year"));
				academicYear.setBranchId(resultSet.getInt("branch_id"));
				return academicYear;
			}
		});		
		
		if(academicYearList != null && academicYearList.size() == 1) {
			return true;
		} else {
			return false;
		}
	}


	@Override
	public Integer getAcademicYearidByDates(String fromDate, String toDate, Integer branchId) {
		String sql = "select id from academic_year where from_date = '" + fromDate + "' and to_date = '"+ toDate +"' ";
		return getJdbcTemplate().queryForObject(sql, Integer.class);
	}
}