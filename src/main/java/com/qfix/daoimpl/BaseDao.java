package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.qfix.utilities.Constants;

@Repository
public class BaseDao extends JdbcDaoSupport {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	final static Logger logger = Logger.getLogger(BaseDao.class);

	public boolean validateAcademicYear(Integer branchId, Integer academicYearId) {
		String sql = "select count(*) as count from academic_year where is_current_active_year = 'Y' AND branch_id = "
				+ branchId + " AND id = " + academicYearId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);

		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateAcademicYear(List<Integer> branchIds,
			Integer academicYearId, boolean includeCurrentActiveYear) {
		String sql = "select count(*) as count from academic_year where branch_id in ("
				+ StringUtils.join(branchIds, ", ")
				+ ") AND id = "
				+ academicYearId
				+ (includeCurrentActiveYear ? " AND is_current_active_year = 'Y'"
						: "");

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);

		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateFeeHead(Integer branchId, Integer headId) {
		String sql = "select count(*) as count from head where id =" + headId
				+ " AND branch_id = " + branchId + " AND is_delete = 'N'";

		List<Integer> headCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (headCountList.size() > 0 && headCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateForumWithBranch(List<Integer> branchIds,
			Integer fourmId) {
		String sql = "select count(*) as count from group_forum where id ="
				+ fourmId + " AND branch_id in("
				+ StringUtils.join(branchIds, ", ") + ") AND is_delete = 'N'";

		List<Integer> fourmCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (fourmCountList.size() > 0 && fourmCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateFeesCode(List<Integer> branchIdList,
			Integer feesCodeId) {
		String sql = "select count(*) as count from fees_code_configuration where id ="
				+ feesCodeId
				+ " AND branch_id in ("
				+ StringUtils.join(branchIdList, ",") + ")";

		List<Integer> feesCodeList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (feesCodeList.size() > 0 && feesCodeList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateFee(List<Integer> branchIds, Integer feeId) {
		String sql = "select count(*) as count from fees where id =" + feeId
				+ " AND branch_id in (" + StringUtils.join(branchIds, ",")
				+ ") AND is_delete = 'N'";

		List<Integer> feesCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (feesCountList.size() > 0 && feesCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateSubject(List<Integer> branchIdList, Integer subjectId) {
		String sql = "select count(*) as count from subject where id ="
				+ subjectId + " AND branch_id in ("
				+ StringUtils.join(branchIdList, ",") + ") AND is_delete = 'N'";

		List<Integer> subjectCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (subjectCountList.size() >= 0 && subjectCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateStandardSubject(List<Integer> branchIdList,
			Integer standardId) {
		String sql = "select count(*) as count from standard_subject as ss "
				+ " INNER JOIN subject as s on s.id = ss.subject_id "
				+ " INNER JOIN standard as st on st.id = ss.standard_id "
				+ " where ss.standard_id =" + standardId
				+ " AND st.branch_id = s.branch_id " + " AND s.branch_id in ("
				+ StringUtils.join(branchIdList, ",")
				+ ") AND ss.is_delete = 'N'";

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);

		if (count >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateStudent(List<Integer> branchIds, Integer studentId) {
		String sql = "select count(*) as count from student where id ="
				+ studentId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ") AND isDelete = 'N'";

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);

		if (count >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateEvent(List<Integer> branchIds, Integer eventId) {
		String sql = "select count(*) as count from event where id =" + eventId
				+ " AND branch_id in (" + StringUtils.join(branchIds, ",")
				+ ") AND status != 'D'";

		List<Integer> eventCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (eventCountList.size() > 0 && eventCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateWorkingDay(List<Integer> branchIds,
			Integer workingDayId) {
		String sql = "select count(*) as count from working_days where id ="
				+ workingDayId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ")";

		List<Integer> workingDayCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rowNumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (workingDayCountList.size() > 0
				&& workingDayCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateHoliDay(List<Integer> branchIds, Integer holiDayId) {
		String sql = "select count(*) as count from holidays where id ="
				+ holiDayId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ")";

		List<Integer> holiDayCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rowNumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (holiDayCountList.size() > 0 && holiDayCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateNotice(List<Integer> branchIds, Integer noticeId) {
		String sql = "select count(*) as count from notice where id ="
				+ noticeId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ")";

		List<Integer> noticeCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (noticeCountList.size() > 0 && noticeCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validateAssignments(List<Integer> branchIds, Integer noticeId) {
		String sql = "select count(*) as count from notice where id ="
				+ noticeId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ")";

		List<Integer> assignmentsCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (assignmentsCountList.size() > 0 && assignmentsCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateStandard(List<Integer> branchIds, Integer standardId) {
		String sql = "select count(*) as count from standard where id ="
				+ standardId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ") AND active = 'Y'";

		List<Integer> standardCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (standardCountList.size() > 0 && standardCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateDivisionWithStandard(List<Integer> branchIds,
			Integer standardId, Integer divisionId) {
		String sql = "select count(*) as count from division where id ="
				+ divisionId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ") AND active = 'Y'";

		System.out.println("validate Division SQL - " + sql);

		List<Integer> divisionCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (divisionCountList.size() > 0 && divisionCountList.get(0).equals(1)) {
			if (standardId != null) {
				sql = "select count(*) as count from standard_division where standard_id = "
						+ standardId
						+ " AND division_id = "
						+ divisionId
						+ " AND branch_id in ("
						+ StringUtils.join(branchIds, ",") + ")";

				divisionCountList.clear();
				divisionCountList = getJdbcTemplate().query(sql,
						new RowMapper<Integer>() {
							public Integer mapRow(ResultSet rs, int rownumber)
									throws SQLException {
								return rs.getInt("count");
							}
						});
				if (divisionCountList.size() > 0
						&& divisionCountList.get(0).equals(1)) {
					return true;
				} else {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	// validateDivision(List<Integer> branchIds, Integer)

	public boolean validateFeesScheduleIds(List<Integer> branchIds,
			List<Long> paymentIds) {
		System.out.println("Number of feesScheduleIds to validate :: "
				+ paymentIds.size());
		String branchIdsStr = StringUtils.join(branchIds, ", ");
		String sql = "select count(*) as count from fees_schedule as fs "
				+ " INNER JOIN fees as f on f.id = fs.fees_id AND f.branch_id in ("
				+ branchIdsStr
				+ ")"
				+ " INNER JOIN student as s on s.user_id = fs.user_id AND s.branch_id in ("
				+ branchIdsStr + ")"
				+ " WHERE s.isDelete = 'N' AND f.is_delete = 'N' "
				+ " AND fs.id in ("
				+ StringUtils.join(paymentIds, ", ") + ")";

		List<Integer> paymentCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		System.out.println("Number of paymentIds found :: "
				+ paymentCountList.get(0));

		if (paymentCountList.size() > 0
				&& paymentCountList.get(0).equals(paymentIds.size())) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkEventBelongsToThisUser(Integer eventId, Integer userId) {
		String sql = "select count(*) as count from user_event "
				+ " where event_id = " + eventId + " AND for_user_id = "
				+ userId + " AND status != '" + Constants.RECORD_DELETED + "'";
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean checkEventCreatedByThisUser(Integer eventId, Integer userId) {
		String sql = "select count(*) as count from event " + " where id = "
				+ eventId + " AND status != '" + Constants.RECORD_DELETED
				+ "' " + " AND sent_by = " + userId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean checkNoticeBelongsToThisUser(Integer noticeId, Integer userId) {
		String sql = "select count(*) as count from notice_individual "
				+ " where notice_id = " + noticeId + " AND for_user_id = "
				+ userId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean checkNoticeCreatedByThisUser(Integer noticeId, Integer userId) {
		String sql = "select count(*) as count from notice " + " where id = "
				+ noticeId + " AND sent_by = " + userId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean validateFeeHead(List<Integer> branchIds, Integer headId) {
		String sql = "select count(*) as count from head where id =" + headId
				+ " AND branch_id in (" + StringUtils.join(branchIds, ",")
				+ ")";

		List<Integer> noticeCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (noticeCountList.size() > 0 && noticeCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateTeacher(List<Integer> branchIds, Integer teacherId) {
		String sql = "select count(*) as count from teacher where id ="
				+ teacherId + " AND branch_id in ("
				+ StringUtils.join(branchIds, ",") + ")";

		List<Integer> noticeCountList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						return rs.getInt("count");
					}
				});

		if (noticeCountList.size() > 0 && noticeCountList.get(0).equals(1)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateTimetable(List<Integer> branchIds,
			Integer timetableId) {
		String sql = "select count(*) as count from timetable "
				+ " where standard_id in (select id from standard "
				+ " where branch_id in (" + StringUtils.join(branchIds, ", ")
				+ ") AND active = 'Y') " + " AND id = " + timetableId
				+ " AND is_delete = 'N'";
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateGroup(List<Integer> branchIds, Integer groupId,
			boolean checkActiveYear, Integer createdBy) {
		String sql = "select count(*) as count from `group` "
				+ " where branch_id in ("
				+ StringUtils.join(branchIds, ", ")
				+ ") "
				+ " AND id = "
				+ groupId
				+ " AND active = 'Y' AND isDelete = 'N' "
				+ (createdBy != null ? " AND created_by = " + createdBy : "")
				+ (checkActiveYear ? " AND academic_year_id = (select id from academic_year "
						+ " where branch_id = `group`.branch_id AND is_current_active_year = 'Y')"
						: "");

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateReports(List<Integer> branchIds, Integer[] reportIds) {
		String sql = "select count(*) as count "
				+ " from student_result as sr "
				+ " INNER JOIN student as s on s.id = sr.student_id "
				+ " WHERE s.branch_id in (" + StringUtils.join(branchIds, ", ")
				+ ") " + " AND sr.id in(" + StringUtils.join(reportIds) + ")";
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count >= reportIds.length) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkForumCreatedByThisUser(Integer forumId, Integer userId,
			boolean checkActiveYear) {
		String sql = "select count(*) as count from group_forum as f "
				+ " INNER JOIN academic_year as ay on ay.branch_id = f.branch_id "
				+ (checkActiveYear ? "AND is_current_active_year = 'Y' " : "")
				+ " where f.id = " + forumId + " AND f.created_by = " + userId
				+ " AND f.is_active = 'Y' AND f.is_delete = 'N' ";

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateRegistrationCode(Integer branchId,
			String registrationCode) {// pitabas
		String sql = "select count(*) as count from student where registration_code ='"
				+ registrationCode + "'" + " AND branch_id = " + branchId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {

			return true;
		} else {
			return false;
		}
	}

	public boolean validateDisplayLayout(Integer displayLayoutId,
			List<Integer> branchIds) {
		String sql = "select count(*) as count from branch_display_layout where branch_id in ("
				+ StringUtils.join(branchIds, ", ")
				+ ") AND is_delete = 'N' AND id = " + displayLayoutId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateLateFees(Integer lateFeesId, List<Integer> branchIds) {
		String sql = "select count(*) as count from late_fees_payment_detail where branch_id in ("
				+ StringUtils.join(branchIds, ", ")
				+ ") AND is_delete = 'N' AND id = " + lateFeesId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateDisplayHead(Integer displayHeadId,
			List<Integer> branchIds) {
		String sql = "select count(*) as count from display_head where branch_id in ("
				+ StringUtils.join(branchIds, ", ")
				+ ") AND is_delete = 'N' AND id = " + displayHeadId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateDisplayTemplate(Integer displayHeadId,
			List<Integer> branchIds) {
		String sql = "select count(*) as count from display_template where branch_id in ("
				+ StringUtils.join(branchIds, ", ")
				+ ") AND is_delete = 'N' AND id = " + displayHeadId;
		
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}
}