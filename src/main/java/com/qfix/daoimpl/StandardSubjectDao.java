package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.qfix.dao.IStandardSubjectDao;
import com.qfix.model.Standard;
import com.qfix.model.StandardSubject;
import com.qfix.model.Subject;

@Slf4j
@Repository
public class StandardSubjectDao extends JdbcDaoSupport implements IStandardSubjectDao{

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	@Transactional
	public boolean uploadStandardSubject(List<StandardSubject> standardSubjectList, Integer branchId) {
		for(StandardSubject standardSubject : standardSubjectList){
			String sql = "insert into standard_subject (standard_id, subject_id) " +
					"values ("+standardSubject.getStandardId()+", "+standardSubject.getSubjectId()+")";
				getJdbcTemplate().update(sql);
		}
		return true;
	}

	@Override
	@Transactional
	public List<StandardSubject> getStandardSubjectsByBranchId(Integer branchId) {
		String sql = "select GROUP_CONCAT(su.name SEPARATOR ', ') as subjects, " +
				" st.displayName as standard , ss.standard_id as standardId" +
				" from " +
				" standard_subject as ss " +
				" INNER JOIN standard as st on st.id = ss.standard_id  " +
				" INNER JOIN academic_year as ay on ay.id = st.academic_year_id AND is_current_active_year = 'Y'" +
				" INNER JOIN subject as su on su.id = ss.subject_id " +
				" where st.branch_id = "+branchId+" AND su.branch_id = " + branchId +
				" AND su.is_delete = 'N' AND ay.branch_id = st.branch_id  " +
				" AND ss.is_delete = 'N' AND st.active = 'Y' group by standard_id";

		System.out.println("Get all standardSubject ----- "+sql);

		List<StandardSubject> standardSubjectList = getJdbcTemplate().query(sql, new RowMapper<StandardSubject>(){
			@Override
			public StandardSubject mapRow(ResultSet resultSet, int arg1) throws SQLException {
				StandardSubject standardSubject = new StandardSubject();
				standardSubject.setSubject(resultSet.getString("subjects"));
				standardSubject.setStandard(resultSet.getString("standard"));
				standardSubject.setStandardId(resultSet.getInt("standardId"));
				return standardSubject;
			}
		} );
		return standardSubjectList;
	}


	@Override
	@Transactional
	public boolean updateStandardSubject(final StandardSubject standardSubject) {

		String sql = "delete from standard_subject where standard_id = "+standardSubject.getPreviousStandardId();

		getJdbcTemplate().update(sql);
		insertStandardSubject(standardSubject);

		return true;
	}


	@Override
	@Transactional
	public boolean insertStandardSubject(final StandardSubject standardSubject) {

		for(Integer subjectId : standardSubject.getSubjectIdList()){
			String sql = "insert into standard_subject (standard_id, subject_id) " +
				"values ("+standardSubject.getStandardId()+", "+subjectId+")";
			getJdbcTemplate().update(sql);
		}
		return true;
	}


	@Override
	@Transactional
	public StandardSubject getStandardSubjectByStandardId(Integer standardId) {
		String sql = "select s.*, b.id as branchId, i.id as instituteId from standard_subject as s"
			+" INNER JOIN standard as st on st.id = s.standard_id"
			+" INNER JOIN branch as b on b.id = st.branch_id"
			+" INNER JOIN institute as i on i.id = b.institute_id where s.standard_id = "+standardId;

		final StandardSubject standardSubject = new StandardSubject();
		List<Integer> subjectIdList = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				if(standardSubject.getBranchId() == null){
					standardSubject.setBranchId(resultSet.getInt("branchId"));
					standardSubject.setStandardId(resultSet.getInt("standard_id"));
					standardSubject.setInstituteId(resultSet.getInt("instituteId"));					
				}
				return resultSet.getInt("subject_id");
			}
		});
		standardSubject.setSubjectIdList(subjectIdList);
		return standardSubject;
	}

	@Override
	@Transactional
	public boolean deleteStandardSubject(Integer id) {
		String sql = "update standard_subject set is_delete = 'Y' where standard_id = "+id;
		getJdbcTemplate().update(sql);
		return true;
	}
	
	

	@Override
	public boolean deleteStandardSubjects(Integer id) {
		String standardSubjectDelete = "DELETE standard_subject FROM standard_subject "
				+ " JOIN standard ON standard.id=standard_subject.standard_id "
				+ " WHERE standard.branch_id = "+id;
		getJdbcTemplate().update(standardSubjectDelete);
		
		String subjectDelete = "DELETE FROM subject where branch_id " + id;
		getJdbcTemplate().update(subjectDelete);
		return true;
	}

	@Override
	public void insertStandardSubjects(Standard standard, List<Subject> subjects) {
		if(subjects != null && subjects.size() > 0){
			for(Subject subject : subjects){
				Integer standardId = standard.getId();
				Integer subjectId = subject.getId();
				String sql = "insert into standard_subject (standard_id, subject_id) values(?,?)";
				Object[] params = new Object[]{standardId,subjectId};
				getJdbcTemplate().update(sql,params);
			}
		}
	}
	
	
	public void insertStandardSubject(Integer standardId, Integer subjectId){
		String sql = "insert into standard_subject (standard_id, subject_id) values(?,?)";
		Object[] params = new Object[]{standardId,subjectId};
		getJdbcTemplate().update(sql,params);
	}
	
	public boolean deleteStandardSubject(Integer standardId, Integer subjectId) {
		String sql = "update standard_subject set is_delete = 'Y' "
				+ "where standard_id = "+standardId + " and subject_id ="+subjectId;
		getJdbcTemplate().update(sql);
		return true;
	}

	@Override
	public void insertYearCourseSubject(Integer yearId, Integer branchId,
			Integer subjectId, Integer courseId, Integer academicYearId) {
		String sql = "insert into year_course_subject (standard_id, division_id, branch_id, subject_id, academic_year_id) values(?,?,?,?,?)";
		Object[] params = new Object[]{yearId, courseId, branchId, subjectId, academicYearId};
		getJdbcTemplate().update(sql,params);
	}

	@Override
	public void deleteYearCourseSubject(Integer yearId, Integer branchId,
			Integer subjectId, Integer courseId, Integer academicYearId) {
		String sql = "update year_course_subject set is_delete = 'Y' "
				+ " where standard_id = "+yearId + " and subject_id ="+subjectId + " and academic_year_id = " + academicYearId
				+ " and branch_id=" + branchId + " and course_id = " + courseId;
		log.info(sql);
		getJdbcTemplate().update(sql);
	}
	
	
	
}
