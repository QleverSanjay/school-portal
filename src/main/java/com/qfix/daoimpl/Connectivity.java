package com.qfix.daoimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qfix.config.AppProperties;
import com.qfix.config.AppProperties.Jdbc;

@Component
public class Connectivity {
	@Autowired
	AppProperties appProperties;

	static Connection conn;
	final static Logger logger = Logger.getLogger(Connectivity.class);
	/**
	 * This method is used for creating connection with the database.
	 * 
	 * @return Connection returns Connection object to which database schema
	 *         connected.
	 */
	public Connection getConnection() {
		
		Jdbc jdbc = appProperties.getJdbc();
		try {
			Class.forName(jdbc.getDriverClassName());
			String url = jdbc.getQfixUrl();
			String username = jdbc.getUsername();
			String password = jdbc.getPassword();
			conn = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			logger.error("", e);
		} catch (ClassNotFoundException e) {
			logger.error("", e);
		}
		return conn;
	}

	/**
	 * This method is used for closing ResultSet, Statement, PreparedStatement
	 * object which is created.
	 * 
	 * @param rs
	 *            The ResultSet object which is to be closed.
	 * @param stmt
	 *            the Statement object which is to be closed.
	 * @param psmt
	 *            the PreparedStatement object which is to be closed.
	 * @return void returns nothing
	 */
	public void closeDatabaseTransaction(ResultSet resultSet, Statement statement,
			PreparedStatement preparedStatement, Connection connection) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			logger.error("", e);
		}
	}
	
	public void closeDatabaseTransaction(ResultSet resultSet, Statement statement,
			PreparedStatement preparedStatement) {
		closeDatabaseTransaction(resultSet, statement,
				preparedStatement, null);
	}
}