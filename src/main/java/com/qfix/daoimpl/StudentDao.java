package com.qfix.daoimpl;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.config.AppProperties;
import com.qfix.config.AppProperties.FileProps;
import com.qfix.dao.IChatDao;
import com.qfix.dao.IStudentDao;
import com.qfix.exceptions.ApplicationException;
import com.qfix.model.AuditLog;
import com.qfix.model.FeesSchedule;
import com.qfix.model.PaymentAccount;
import com.qfix.model.Student;
import com.qfix.model.StudentExcelReportFilter;
import com.qfix.model.StudentParent;
import com.qfix.model.TableOptions;
import com.qfix.model.TableResponse;
import com.qfix.service.client.notification.Notification;
import com.qfix.service.client.notification.Sender;
import com.qfix.service.file.FileFolderService;
import com.qfix.utilities.Constants;
import com.qfix.utilities.DateUtil;
import com.qfix.utilities.FileConstants;
import com.qfix.utilities.SecurityUtil;

@Slf4j
@Repository
public class StudentDao extends JdbcDaoSupport implements IStudentDao {
	final static Logger logger = Logger.getLogger(StudentDao.class);

	@Autowired
	private DriverManagerDataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Autowired
	AppProperties appProperties;

	@Autowired
	IChatDao chatDao;

	@Autowired
	Sender sender;

	@Autowired
	private FileFolderService fileFolderService;

	/**
	 * Generate password by first name and date of birth.
	 * 
	 * @param firstName
	 *            the first name of student
	 * @param dateOfBirth
	 *            the date of birth of student
	 * @return the string
	 * @throws Exception
	 */
	// @Transactional
	private String generatePasswordByNameAndDOB(String firstName,
			String dateOfBirth) {
		String generatedPassword = null;

		Calendar calendar = Calendar.getInstance();

		try {
			calendar.setTime(StringUtils.isEmpty(dateOfBirth) ? new Date()
					: Constants.DATABASE_DATE_FORMAT.parse(dateOfBirth));
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}
		// calendar.setTime(dateOfBirth);
		generatedPassword = firstName + calendar.get(Calendar.DAY_OF_MONTH)
				+ (calendar.get(Calendar.MONTH) + 1)
				+ calendar.get(Calendar.YEAR);
		generatedPassword = generatedPassword.replaceAll(" ", "").toUpperCase();
		return generatedPassword;
	}

	@Transactional
	public List<Student> getStudents(Integer branchId) {
		String selectString = " select s.*, b.name as branchName, st.displayName as standard, "
				+ " d.displayName as division, cs.name as caste, sta.name as state, dis.name as district, tal.name as taluka "
				+ " from "
				+ " student as s INNER JOIN branch as b on b.id = s.branch_id "
				+ " INNER JOIN standard as st on st.id = s.standard_id "
				+ " INNER JOIN division as d on d.id = s.division_id "
				+ " INNER JOIN caste as cs on cs.id = s.caste_id "
				+ " LEFT OUTER JOIN state as sta on sta.id = s.state_id "
				+ " LEFT OUTER JOIN district as dis on dis.id = s.district_id "
				+ " LEFT OUTER JOIN taluka as tal on tal.id = s.taluka_id "
				+ " where "
				+ " s.isDelete = 'N' AND s.branch_id = "
				+ branchId
				+ " "
				+ " AND st.active = 'Y' AND cs.active = 'Y' "
				+ " order by s.first_name, s.last_name ";
		logger.debug("get Student sql --" + selectString);

		List<Student> students = getJdbcTemplate().query(selectString,
				new RowMapper<Student>() {
					@Override
					public Student mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						Student student = new Student();
						student.setId(resultSet.getInt("id"));
						student.setFirstname(resultSet.getString("first_name"));
						student.setFatherName(resultSet
								.getString("father_name"));
						student.setMotherName(resultSet
								.getString("mother_name"));
						student.setSurname(resultSet.getString("last_name"));
						student.setStandardName(resultSet.getString("standard"));
						student.setDivisionName(resultSet.getString("division"));
						student.setCaste(resultSet.getString("caste"));
						student.setRollNumber(resultSet
								.getString("roll_number"));
						student.setRegistrationCode(resultSet
								.getString("registration_code"));
						student.setGender(resultSet.getString("gender"));
						student.setDateOfBirth(resultSet.getString("dob"));
						student.setEmailAddress(resultSet
								.getString("email_address"));
						student.setMobile(resultSet.getString("mobile"));
						student.setAddressLine1(resultSet.getString("line1"));
						student.setArea(resultSet.getString("area"));
						student.setCity(resultSet.getString("city"));
						student.setPincode(resultSet.getString("pincode"));
						student.setReligion(resultSet.getString("religion"));
						student.setActive(resultSet.getString("active"));
						student.setIsDelete(resultSet.getString("isDelete"));
						return student;
					}
				});
		return students;
	}

	@Transactional
	public List<Student> getStudentsByStandardDivision(Integer branchId,
			Integer standardId, Integer divisionId) {
		String selectString = " select s.* "
				+ " from student s where "
				+ " s.isDelete = 'N' AND s.active = 'Y' AND s.branch_id = "
				+ branchId
				+ " "
				+ (standardId != null ? " AND s.standard_id = " + standardId
						+ " " : "")
				+ (divisionId != null ? " AND s.division_id = " + divisionId
						+ " " : "")
				+ " order by s.first_name, s.last_name, s.roll_number, s.registration_code ";
		logger.debug("get Student sql --" + selectString);

		List<Student> students = getJdbcTemplate().query(selectString,
				new RowMapper<Student>() {
					@Override
					public Student mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						Student student = new Student();
						student.setId(resultSet.getInt("id"));
						student.setFirstname(resultSet.getString("first_name"));
						student.setFatherName(resultSet
								.getString("father_name"));
						student.setMotherName(resultSet
								.getString("mother_name"));
						student.setSurname(resultSet.getString("last_name"));

						student.setCasteId(resultSet.getInt("caste_id") == 0 ? null
								: resultSet.getInt("caste_id"));
						student.setStandardId(resultSet.getInt("standard_id") == 0 ? null
								: resultSet.getInt("standard_id"));
						student.setDivisionId(resultSet.getInt("division_id") == 0 ? null
								: resultSet.getInt("division_id"));

						student.setFeesCodeId(getFeesCodesByStudentId(student
								.getId()));
						/*
						 * student.setFeesCodeId(resultSet
						 * 
						 * .getInt("fees_code_configuration_id") == 0 ? null :
						 * resultSet.getInt("fees_code_configuration_id"));
						 */

						student.setRollNumber(resultSet
								.getString("roll_number"));
						student.setRegistrationCode(resultSet
								.getString("registration_code"));
						student.setGender(resultSet.getString("gender"));
						student.setDateOfBirth(resultSet.getString("dob"));
						student.setEmailAddress(resultSet
								.getString("email_address"));
						student.setMobile(resultSet.getString("mobile"));
						student.setAddressLine1(resultSet.getString("line1"));
						student.setArea(resultSet.getString("area"));
						student.setCity(resultSet.getString("city"));
						student.setPincode(resultSet.getString("pincode"));
						student.setReligion(resultSet.getString("religion"));
						student.setActive(resultSet.getString("active"));
						student.setIsDelete(resultSet.getString("isDelete"));
						return student;
					}
				});
		return students;
	}

	@Transactional
	public Student getStudentById(Integer id) {
		String selectString = "select s.*, u.username, b.institute_id, c.name as casteName "
				+ " from student as s "
				+ " INNER JOIN user as u on u.id = s.user_id "
				+ " INNER JOIN branch as b on b.id = s.branch_id "
				+ " LEFT JOIN caste as c on c.id = s.caste_id "
				+ " where s.id = " + id;

		logger.debug("get Student by Id SQl --" + selectString);

		Student student = getJdbcTemplate().query(selectString,
				new RowMapper<Student>() {
					@Override
					public Student mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return mapStudents(resultSet);
					}
				}).get(0);
		return student;
	}

	private Student mapStudents(ResultSet resultSet) throws SQLException {
		Student student = new Student();
		student.setId(resultSet.getInt("id"));
		student.setFirstname(resultSet.getString("first_name"));
		student.setFatherName(resultSet.getString("father_name"));
		student.setMotherName(resultSet.getString("mother_name"));
		student.setSurname(resultSet.getString("last_name"));

		student.setGender(resultSet.getString("gender"));
		student.setDateOfBirth(resultSet.getString("dob"));
		student.setRelationship(resultSet.getString("relation_with_parent"));
		student.setEmailAddress(resultSet.getString("email_address"));
		student.setMobile(resultSet.getString("mobile"));
		student.setAddressLine1(resultSet.getString("line1"));
		student.setArea(resultSet.getString("area"));
		student.setCity(resultSet.getString("city"));
		student.setPincode(resultSet.getString("pincode"));
		student.setReligion(resultSet.getString("religion"));
		student.setCasteId(resultSet.getInt("caste_id") == 0 ? null : resultSet
				.getInt("caste_id"));
		student.setCaste(resultSet.getString("casteName"));
		student.setStandardId(resultSet.getInt("standard_id") == 0 ? null
				: resultSet.getInt("standard_id"));
		student.setDivisionId(resultSet.getInt("division_id") == 0 ? null
				: resultSet.getInt("division_id"));
		student.setRollNumber(resultSet.getString("roll_number"));
		student.setRegistrationCode(resultSet.getString("registration_code"));
		student.setBranchId(resultSet.getInt("branch_id"));
		student.setInstituteId(resultSet.getInt("institute_id"));
		student.setActive(resultSet.getString("active"));
		student.setIsDelete(resultSet.getString("isDelete"));
		student.setPhoto(resultSet.getString("photo"));
		student.setDistrictId(resultSet.getInt("district_id") == 0 ? null
				: resultSet.getInt("district_id"));
		student.setStateId(resultSet.getInt("state_id") == 0 ? null : resultSet
				.getInt("state_id"));
		student.setTalukaId(resultSet.getInt("taluka_id") == 0 ? null
				: resultSet.getInt("taluka_id"));

		student.setFeesCodeId(getFeesCodesByStudentId(student.getId()));
		/*
		 * student.setFeesCodeId(resultSet .getInt("fees_code_configuration_id")
		 * == 0 ? null : resultSet.getInt("fees_code_configuration_id"));
		 */
		student.setCreateStudentLogin(resultSet.getString("create_login"));
		List<StudentParent> studentParentList = getStudentParentByStudentId(
				student.getBranchId(), student.getId());
		student.setStudentParentList(studentParentList);
		student.setStudentUserId(resultSet.getString("username"));
		student.setUserId(resultSet.getInt("user_id"));
		return student;
	}

	private List<Integer> getFeesCodesByStudentId(Integer id) {
		String sql = "select c.id from feescode_student f "
				+ "inner join fees_code_configuration c "
				+ "on f.fees_code_configuration_id=c.id "
				+ "where f.student_id=" + id;

		return getJdbcTemplate().queryForList(sql, Integer.class);

	}

	/**
	 * Gets the school parent profiles associated by student.
	 * 
	 * @param studentId
	 *            the student id
	 * @return the school parent profile by student id
	 */
	// @Transactional
	private List<StudentParent> getStudentParentByStudentId(
			final Integer branchId, final Integer stduentId) {
		final List<StudentParent> studentParentList = new ArrayList<>();

		String sql = " SELECT u.username, pu.*, p.id as parentId, ir.name as income_range, pro.name as profession "
				+ " from parent_upload pu"
				+ " LEFT JOIN branch_student_parent as bsp on bsp.parent_upload_id = pu.id AND bsp.isDelete = 'N'"
				+ " LEFT JOIN parent as p on p.id = bsp.parent_id "
				+ " LEFT JOIN user as u on u.id = p.user_id "
				+ " LEFT JOIN profession as pro on pro.id = p.profession_id "
				+ " LEFT JOIN income_range as ir on ir.id = p.income_range_id "
				+ " where pu.branch_id = "
				+ branchId
				+ " and pu.student_id = "
				+ stduentId + " AND pu.isDelete = 'N'";

		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				final StudentParent studentParent = new StudentParent();

				studentParent.setId(rs.getInt("id"));
				studentParent.setFirstname(rs.getString("firstname"));
				studentParent.setMiddlename(rs.getString("middlename"));
				studentParent.setLastname(rs.getString("lastname"));
				studentParent.setEmail(rs.getString("email"));
				studentParent.setGender(rs.getString("gender"));
				studentParent.setRelationId(rs.getInt("relation_id"));
				studentParent.setPincode(rs.getString("pincode"));
				studentParent.setPrimaryContact(rs.getString("primaryContact"));
				studentParent.setSecondaryContact(rs
						.getString("secondaryContact"));
				studentParent.setArea(rs.getString("area"));
				studentParent.setAddressLine(rs.getString("addressline"));
				studentParent.setCity(rs.getString("city"));
				studentParent.setDob(rs.getString("dob"));
				studentParent.setParentId(rs.getInt("parentId"));
				studentParent.setCreateLogin(rs.getString("create_login"));
				studentParent.setStudentId(stduentId);
				studentParent.setUserId(rs.getInt("user_id"));
				studentParent.setParentUserId(rs.getString("username"));
				studentParent.setProfession(rs.getString("profession"));
				studentParent.setIncomeRange(rs.getString("income_range"));
				studentParentList.add(studentParent);
				return null;
			}
		});
		return studentParentList;
	}

	/*
	 * // @Transactional private void createForumProfile(final Student student)
	 * throws SQLException{
	 * 
	 * // get inserted teacher id from database for user profile forum String
	 * sql = "select u.temp_password as password, " +
	 * "s.email_address as email, s.branch_id as branchId, " +
	 * "bfd.read_only_group_id as rogId, " +
	 * "bfd.forum_category_id as schoolId " +
	 * "from student as s, user as u, branch_forum_detail as bfd " +
	 * "where u.id = s.user_id AND bfd.branch_id = s.branch_id AND s.user_id = "
	 * +student.getUserId();
	 * 
	 * logger.debug("user profile select SQL --"+sql);
	 * 
	 * getJdbcTemplate().query(sql, new RowMapper<String>(){
	 * 
	 * @Override public String mapRow(final ResultSet resultSet, int arg1)
	 * throws SQLException { final String password =
	 * resultSet.getString("password"); final String email =
	 * resultSet.getString("email"); final Integer schoolId =
	 * resultSet.getInt("schoolId"); CreateUserResponse userResponse = new
	 * ForumProfileDao
	 * ().createUserAccount(student.getUsername(),password,email,schoolId,null,
	 * false, false); if(userResponse != null){
	 * insertIntoForumProfile(student.getUsername(), password,
	 * student.getUserId(), userResponse.getForum_profile_id()); } return null;
	 * } }); }
	 * 
	 * // @Transactional protected void insertIntoForumProfile(final String
	 * userName, final String password, final Integer userId, final Integer
	 * forumProfileId) { final String sql =
	 * "insert into user_forum_profile (login, password, " +
	 * "forum_profile_id, is_active, is_delete ,user_id) " +
	 * "values (?,?,?,?,?,?)"; getJdbcTemplate().update(new
	 * PreparedStatementCreator() {
	 * 
	 * @Override public PreparedStatement createPreparedStatement(Connection
	 * connection) throws SQLException { PreparedStatement ps =
	 * connection.prepareStatement(sql); ps.setString(1, userName);
	 * ps.setString(2, password); ps.setInt(3, forumProfileId); ps.setString(4,
	 * "Y"); ps.setString(5, "N"); ps.setInt(6, userId); return ps; } }); }
	 * 
	 * // @Transactional private void createParentForumProfile(final Integer
	 * userId, Integer branchId) throws SQLException{
	 * 
	 * // get inserted teacher id from database for user profile forum String
	 * sql = "select u.temp_password as password, " + "p.email as email, " +
	 * "bfd.read_only_group_id as rogId, " +
	 * "bfd.forum_category_id as forum_category_id " +
	 * "from parent as p , user as u, branch_forum_detail as bfd " +
	 * "where u.id = "
	 * +userId+" AND p.user_id = u.id AND bfd.branch_id = "+branchId
	 * +" group by u.id ";
	 * 
	 * logger.debug("user profile select SQL --"+sql);
	 * 
	 * getJdbcTemplate().query(sql, new RowMapper<String>(){
	 * 
	 * @Override public String mapRow(final ResultSet resultSet, int arg1)
	 * throws SQLException {
	 * 
	 * final String password = resultSet.getString("password"); final String
	 * email = resultSet.getString("email"); final Integer schoolId =
	 * resultSet.getInt("forum_category_id");
	 * 
	 * String sql =
	 * "select count(*) as count from user_forum_profile where user_id = '"
	 * +userId+"' "; getJdbcTemplate().query(sql, new RowMapper<Integer>(){
	 * 
	 * @Override public Integer mapRow(ResultSet resultSet, int arg1) throws
	 * SQLException {
	 * System.out.println("COUNT FOR USER_FORUM_PROFILE ::::"+resultSet
	 * .getInt("count") ); if(resultSet.getInt("count") == 0){ String username =
	 * RandomStringUtils.random(10, true, true) + "@"+Constants.DOMAIN_NAME;
	 * CreateUserResponse userResponse = new
	 * ForumProfileDao().createUserAccount(
	 * username,password,email,schoolId,null, false, false); if(userResponse !=
	 * null){ insertIntoForumProfile(username, password, userId,
	 * userResponse.getForum_profile_id()); } } return null; } }); return null;
	 * } }); }
	 */

	// @Transactional
	private String saveFile(MultipartFile file, Student student,
			boolean isUpdate) throws IOException {
		String filePath = null;
		// && student.isFileChanegd()
		if (file != null) {
			String parentPath = student.getInstituteId() + File.separator
					+ student.getBranchId() + File.separator
					+ FileConstants.PROFILE_DOCUMENTS_PATH
					+ FileConstants.STUDENT_PROFILE_PATH;

			String fileExtension = file.getOriginalFilename().substring(
					file.getOriginalFilename().lastIndexOf("."));
			String fileNameWithExtension = org.apache.commons.lang3.RandomStringUtils
					.random(30, true, true) + fileExtension;

			if (isUpdate) {
				String photo = student.getPhoto();
				fileNameWithExtension = (StringUtils.isEmpty(photo)
						|| photo.endsWith(FileConstants.DEFAULT_PROFILE_IMAGE_NAME) ? fileNameWithExtension
						: photo.substring(photo.lastIndexOf("/") + 1));
			}

			filePath = fileFolderService.createFileInExternalStorage(
					parentPath, fileNameWithExtension, file.getBytes());

		} else if (!isUpdate) {
			FileProps fileProps = appProperties.getFileProps();
			filePath = fileProps.getBaseSystemPath()
					.concat(fileProps.getExternalDocumentsPath()).concat("/");
		}
		filePath = FilenameUtils.separatorsToUnix(filePath);
		logger.debug("Student profile image path --" + filePath);
		return filePath;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public boolean insertStudent(final Student student, MultipartFile file,
			String token) throws Exception {

		boolean flag = false;
		Integer userId = insertIntoUser(student);

		// insert into student_upload table with userId.
		final int studentUploadId = insertIntoStudentUpload(student, userId);
		student.setUserId(userId);
		if (studentUploadId != 0) {
			
			//upload student with multiple feeCode
			List<Integer>feeCodeIds=getFeesCodeIdsByName(student);
			if(feeCodeIds!=null && feeCodeIds.size()>0){
				student.setFeesCodeId(feeCodeIds);
				
			}
			// insert into student table.
			final int studentId = insertIntoStudent(student, studentUploadId, userId);
			student.setId(studentId);
			insertStudentParent(student, token);

			updateProfilePhoto(student, file, userId);
			createPaymentAccount(userId);

			int standardId = student.getStandardId() != null ? student
					.getStandardId() : 0;
			int divisionId = student.getDivisionId() != null ? student
					.getDivisionId() : 0;
			int casteId = student.getCasteId() != null ? student.getCasteId()
					: 0;
			List<Integer> feesCodeId = student.getFeesCodeId() != null ? student
					.getFeesCodeId() : null;

			updateFeesSchedule(standardId, divisionId, casteId, feesCodeId,
					student.getUserId(), student.getBranchId(), studentId);

			flag = true;
		} else {
			throw new Exception("Problem while creating student.");
		}
		logger.debug("Student inserted...");
		return flag;
	}

	// @Transactional
	private void updateProfilePhoto(final Student student,
			final MultipartFile file, final Integer userId) {
		// get inserted student institute Name and branch name
		// System.out.println("Crearting PROFILES :::::::::");
		String sql = "select i.name as instituteName, i.id as instituteId, b.id as branchId, "
				+ "s.id as studentId, b.name as branchName "
				+ "from institute as i, branch as b, student as s where i.id = b.institute_id "
				+ "AND s.branch_id = b.id AND s.user_id = " + userId;
		student.setUserId(userId);
		getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				student.setId(resultSet.getInt("studentId"));
				student.setInstituteId(resultSet.getInt("instituteId"));
				student.setBranchId(resultSet.getInt("branchId"));

				String photoPath = null;
				try {
					photoPath = saveFile(file, student, false);
					updatePhotoPath(student.getId(), photoPath);
					logger.debug("Profile Photo Uploaded..");
				} catch (IOException e) {
					logger.error("Photo not Uploaded..", e);
				}
				/*
				 * Map<Integer, String> groupChatIds =
				 * getGroupChatIdsByStandardAndDivision(student);
				 * 
				 * // create other accounts if createLogin is YES.
				 * if("Y".equals(student.getCreateStudentLogin())){
				 * if(student.getUsername().equals(student.getEmailAddress())){
				 * String n = RandomStringUtils.random(7, false, true); String
				 * userName = (student.getFirstname() != null ?
				 * student.getFirstname().replaceAll("([\\() \\.\\-_])", "") :
				 * "") + (student.getSurname() != null ?
				 * student.getSurname().replaceAll("([\\() \\.\\-_])", "") : "")
				 * + n + "@eduqfix.com"; userName = userName.replaceAll(" ",
				 * ""); student.setUsername(userName); }
				 * 
				 * createPaymentAccount(userId); String password =
				 * generatePasswordByNameAndDOB(student.getFirstname(),
				 * student.getDateOfBirth()); student.setPassword(password); try
				 * { createShoppingProfile(student, userId); } catch
				 * (ShoppingAccountNotCreatedException e) {
				 * logger.error(e.getMessage(), e); }
				 * createForumProfile(student);
				 * 
				 * Long chatUserProfile = createChatProfile(student, userId); //
				 * attach chat groups by standard.
				 * 
				 * updateChatAndGroupData(groupChatIds, chatUserProfile, userId,
				 * student.getId(), true, Constants.ROLE_ID_STUDENT, userId); }
				 * else { updateChatAndGroupData(groupChatIds, 0, userId,
				 * student.getId(), true, Constants.ROLE_ID_STUDENT, userId); }
				 */
				// sendNotification(student, instituteName, branchName, userId);
				return null;
			}
		});
	}

	/**
	 * Gets the group chat ids by standard.
	 * 
	 * @param student
	 *            the student
	 * @return the group chat ids by standard
	 */
	// @Transactional
	private Map<Integer, String> getGroupChatIdsByStandardAndDivision(
			final Student student) {
		final Map<Integer, String> groupChatIds = new HashMap<>();

		String sql = "select g.chat_dialogue_id as groupChatId, g.id as groupId FROM `group` as g "
				+ " INNER JOIN group_standard as gs on g.id = gs.group_id "
				+ " LEFT JOIN group_user as gu on g.id = gu.group_id "
				+ " WHERE g.isDelete = 'N' AND g.active = 'Y' "
				+ " AND gs.standard_id = "
				+ student.getStandardId()
				+ " AND gs.division_id = "
				+ student.getDivisionId()
				+ " group by gs.group_id, standard_id, division_id";

		System.out.println("getGroupChatIdsByStandard SQL :::::::::::\n" + sql);

		getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				groupChatIds.put(resultSet.getInt("groupId"),
						resultSet.getString("groupChatId"));
				return null;
			}
		});
		return groupChatIds;
	}

	/**
	 * Attach or deAttach chat groups by standard. Attaches or deAttaches
	 * student to chat groups based on standards which are assigned to student
	 * or deAssigned in case of update.
	 * 
	 * @param groupChatIds
	 *            the group chat ids
	 * @param chatUserProfileId
	 *            the chat user profile id
	 * @param studentId
	 * @param isAttach
	 *            the is attach
	 * @param roleId
	 */
	// @Transactional
	private void updateChatAndGroupData(Map<Integer, String> groupChatIds,
			long chatUserProfileId, Integer userId, Integer studentId,
			boolean isAttach, Integer roleId, Integer forUserId) {
		System.out.println("Gorup Chat Ids to attach or deattach ::::::::: \n"
				+ groupChatIds);

		if (groupChatIds != null) {
			Set<Integer> groupIdSet = groupChatIds.keySet();
			if (groupIdSet != null && groupIdSet.size() > 0) {

				if (chatUserProfileId != 0) {
					try {
						for (Integer groupId : groupIdSet) {

							String groupChatId = groupChatIds.get(groupId);
							if (isAttach) {
								chatDao.updateChatPublicGroups(groupChatId,
										chatUserProfileId + "", "");
								String sql = "insert into group_user (group_id, user_id) values ("
										+ groupId + ", " + userId + ")";
								getJdbcTemplate().update(sql);
							} else {
								chatDao.updateChatPublicGroups(groupChatId, "",
										chatUserProfileId + "");
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				// Update group related data.
				updateGroupRelatedData(groupIdSet, userId, studentId, isAttach,
						roleId, forUserId);

				if (!isAttach) {
					String groupIds = org.apache.commons.lang.StringUtils.join(
							groupIdSet, ", ");
					String sql = "delete from group_user where group_id in("
							+ groupIds + ") " + "AND user_id = " + userId + "";
					getJdbcTemplate().update(sql);
				}
			}
		}
	}

	// @Transactional
	private void updateGroupRelatedData(Set<Integer> groupIdSet,
			Integer userId, Integer studentId, boolean isAttach,
			Integer roleId, Integer forUserId) {

		updateForumData(groupIdSet, userId, isAttach);
		// if isAttach then insert data as new record for userId.
		if (!isAttach) {
			// Delete from user_event
			String groupIds = org.apache.commons.lang.StringUtils.join(
					groupIdSet, ", ");
			String sql = "update user_event set status = 'D' where user_id = "
					+ userId
					+ " "
					+ " AND event_group_id in (select id from event_group where group_id in ("
					+ groupIds + "))";
			getJdbcTemplate().update(sql);

			// Delete from notice_individual
			sql = "delete from notice_individual where notice_id in"
					+ "(select notice_id from notice_group where group_id in ("
					+ groupIds + ")) " + " AND user_id = " + userId
					+ " AND for_user_id = " + forUserId;
			getJdbcTemplate().update(sql);
		} else {
			// insert notice data and event data.
			insertNoticeData(groupIdSet, userId, forUserId);
			insertEventData(groupIdSet, userId, studentId, roleId, forUserId);
		}
	}

	// @Transactional
	private void updateForumData(Set<Integer> groupIdSet, Integer userId,
			boolean isAttach) {
		try {
			Set<Long> forumGroupIds = getForumGrouIds(groupIdSet);
			if (forumGroupIds != null && forumGroupIds.size() > 0) {
				String sql = "select forum_profile_id from user_forum_profile where user_id = "
						+ userId;
				Integer userForumId = getJdbcTemplate().queryForObject(sql,
						Integer.class);
				if (userForumId != 0) {
					for (Long forumGroupId : forumGroupIds) {
						linkOrDelinkUserToForum(forumGroupId, userForumId,
								isAttach);
					}
				}
			}
		} catch (Exception e) {
			// TODO Remove this catch if forum woriking properly....
			logger.error("Error while linking users with forum...", e);
		}
	}

	// @Transactional
	private void linkOrDelinkUserToForum(long groupForumId, int userForumId,
			boolean isAttach) {
		ForumProfileDao forumProfileDao = new ForumProfileDao();
		Set<Integer> userForumProfileIdList = new HashSet<>();
		userForumProfileIdList.add(userForumId);

		if (isAttach) {
			forumProfileDao.linkUserToForum(groupForumId,
					userForumProfileIdList);
		} else {
			forumProfileDao.deLinkUserToForum(groupForumId,
					userForumProfileIdList);
		}
	}

	// @Transactional
	private Set<Long> getForumGrouIds(Set<Integer> groupIdSet) {
		Set<Long> forumGroupIds = null;
		String groupIds = org.apache.commons.lang.StringUtils.join(groupIdSet,
				", ");
		String sql = "select forum_group_id from group_forum where group_id in("
				+ groupIds + ")";
		List<Long> forumGroupIdList = getJdbcTemplate().query(sql,
				new RowMapper<Long>() {
					@Override
					public Long mapRow(ResultSet resultSet, int arg1)
							throws SQLException {

						return resultSet.getLong("forum_group_id");
					}
				});
		if (forumGroupIdList.size() > 0) {
			forumGroupIds = new HashSet<>(forumGroupIdList);
		}
		return forumGroupIds;
	}

	// @Transactional
	private void insertNoticeData(Set<Integer> groupIdSet,
			final Integer userId, final Integer forUserId) {

		String groupIds = org.apache.commons.lang.StringUtils.join(groupIdSet,
				", ");
		String sql = "select notice_id from notice_group where group_id in ("
				+ groupIds + ")";

		getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				Integer noticeId = resultSet.getInt("notice_id");
				String sql = "select count(*) as count from notice_individual "
						+ "where notice_id = " + noticeId + " AND user_id = "
						+ userId + " AND for_user_id = " + forUserId;

				Integer count = getJdbcTemplate().queryForObject(sql,
						Integer.class);

				if (count == 0) {
					sql = "insert into notice_individual (notice_id, user_id, for_user_id) values ("
							+ noticeId + ", " + userId + ", " + forUserId + ")";
					getJdbcTemplate().update(sql);
				}
				return null;
			}
		});
	}

	// @Transactional
	private void insertEventData(Set<Integer> groupIdSet, final Integer userId,
			final Integer studentId, final Integer roleId,
			final Integer forUserId) {
		for (Integer groupId : groupIdSet) {
			// update event data.
			String sql = "select id, event_id from event_group where group_id = "
					+ groupId;

			getJdbcTemplate().query(sql, new RowMapper<String>() {
				@Override
				public String mapRow(ResultSet resultSet, int arg1)
						throws SQLException {
					Integer eventGroupId = resultSet.getInt("id");
					String sql = "select count(*) as count from user_event where user_id = "
							+ userId + " AND event_group_id = " + eventGroupId;
					Integer count = getJdbcTemplate().queryForObject(sql,
							Integer.class);

					if (count > 0) {
						sql = "update user_event set status = 'N' where user_id = "
								+ userId
								+ " AND event_group_id = "
								+ eventGroupId;
						getJdbcTemplate().update(sql);
					} else {
						sql = "insert into user_event (user_id, role_id, entity_id, event_id, event_group_id, status, for_user_id) "
								+ " values ("
								+ userId
								+ ", "
								+ roleId
								+ ", "
								+ studentId
								+ ", "
								+ resultSet.getInt("event_id")
								+ ", "
								+ eventGroupId + ", 'N', " + forUserId + ")";
						getJdbcTemplate().update(sql);
					}
					return null;
				}
			});
		}

	}

	// @Transactional
	@Override
	public void sendNotification(Integer branchId, Integer standardId,
			Integer divisionId) {
		Notification notification = new Notification();

		Map<String, Object> additionalData = new HashMap<>();
		additionalData.put("branchId", branchId);
		additionalData.put("standardId", standardId);
		additionalData.put("divisionId", divisionId);

		notification.setType("STUDENT_CREATE");
		notification.setBatchEnabled(false);
		notification.sendEmail(true);
		notification.sendSMS(true);

		notification.setAdditionalData(additionalData);
		ObjectMapper mapper = new ObjectMapper();

		try {
			String jsonMessage = mapper.writeValueAsString(notification);
			sender.sendMessage(jsonMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// @Transactional
	private int insertIntoStudent(final Student student,
			final int studentUploadId, final Integer userId) {
		final String studentSql = "insert into student (first_name, father_name, mother_name, last_name, "
				+ "gender, dob ,relation_with_parent, email_address, mobile, line1, area, city, pincode, religion, "
				+ "caste_id, standard_id, division_id, roll_number, active, registration_code, user_id, "
				+ "photo, branch_id, student_upload_id, isDelete, state_id, district_id, taluka_id,  create_login) "
				+ "values( ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection.prepareStatement(
						studentSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, student.getFirstname());
				preparedStatement.setString(2, student.getFatherName());
				preparedStatement.setString(3, student.getMotherName());
				preparedStatement.setString(4, student.getSurname());
				preparedStatement.setString(5, student.getGender());
				preparedStatement.setString(6, StringUtils.isEmpty(student
						.getDateOfBirth()) ? null : student.getDateOfBirth());
				preparedStatement.setString(7, student.getRelationship());
				preparedStatement.setString(8, student.getEmailAddress());
				preparedStatement.setString(9, student.getMobile());
				preparedStatement.setString(10, student.getAddressLine1());
				preparedStatement.setString(11, student.getArea());
				preparedStatement.setString(12, student.getCity());
				preparedStatement.setString(13, student.getPincode());
				preparedStatement.setString(14, student.getReligion());

				if (student.getCasteId() == null) {
					preparedStatement.setNull(15, Types.NULL);
				} else {
					preparedStatement.setInt(15, student.getCasteId());
				}

				if (student.getStandardId() == null) {
					preparedStatement.setNull(16, Types.NULL);
				} else {
					preparedStatement.setInt(16, student.getStandardId());
				}

				if (student.getDivisionId() == null) {
					preparedStatement.setNull(17, Types.NULL);
				} else {
					preparedStatement.setInt(17, student.getDivisionId());
				}

				preparedStatement.setString(18, student.getRollNumber());
				preparedStatement.setString(19, (StringUtils.isEmpty(student
						.getActive()) ? "Y" : student.getActive()));
				preparedStatement.setString(20, student.getRegistrationCode());

				if (userId == null) {
					preparedStatement.setNull(21, Types.NULL);
				} else {
					preparedStatement.setInt(21, userId);
				}

				preparedStatement.setString(22, student.getPhoto());
				preparedStatement.setInt(23, student.getBranchId());
				preparedStatement.setInt(24, studentUploadId);
				preparedStatement.setString(25, "N");

				if (student.getStateId() == null) {
					preparedStatement.setNull(26, Types.NULL);
				} else {
					preparedStatement.setInt(26, student.getStateId());
				}

				if (student.getDistrictId() == null) {
					preparedStatement.setNull(27, Types.NULL);
				} else {
					preparedStatement.setInt(27, student.getDistrictId());
				}

				if (student.getTalukaId() == null) {
					preparedStatement.setNull(28, Types.NULL);
				} else {
					preparedStatement.setInt(28, student.getTalukaId());
				}

				// if (student.getFeesCodeId() == null) {
				// preparedStatement.setNull(29, Types.NULL);
				// } else {
				// preparedStatement.setInt(29, student.getFeesCodeId());
				// }
				preparedStatement.setString(29, student.getCreateStudentLogin());
				return preparedStatement;
			}
		}, holder);
		int studentId = holder.getKey().intValue();

		// insert feesCodes for student
		if(student.getFeesCodeId()!=null && student.getFeesCodeId().size()>0){
			insertFeescodeStudent(student.getFeesCodeId(), studentId);
		}

		insertIntoContact(student.getFirstname(), student.getSurname(), student.getMobile(), student.getEmailAddress(), 
				student.getCity(), student.getPhoto(), userId);
		return studentId;
	}
	
	private List<Integer> getFeesCodeIdsByName(Student student){
		String sql="select id from fees_code_configuration where branch_id="+student.getBranchId() +" and name in( '"+student.getFeesCodeName1()+"'"+",'"+student.getFeesCodeName1()+"'"+",'"+student.getFeesCodeName2()+"'"+",'"+student.getFeesCodeName3()+
				"'"+",'"+student.getFeesCodeName4()+"'"+",'"+student.getFeesCodeName5()+"')";
		List<Integer> feeCodeIds=new ArrayList<Integer>();
		try{
		List<Map<String, Object>>feesCodeIdMap=getJdbcTemplate().queryForList(sql);
		System.out.println("FeesCodeIDs+++++++++++++++++++++++++++++");
		System.out.println(feesCodeIdMap);
		
		
		if(feesCodeIdMap!=null && feesCodeIdMap.size()>0){
			
			for(Map<String,Object> map:feesCodeIdMap){
				
				for(Map.Entry<String, Object> entry:map.entrySet()){
					
					feeCodeIds.add(Integer.parseInt(entry.getValue().toString()));
				}
			
			}
		}
		}catch(Exception e){
			
			e.printStackTrace();
		}


	return feeCodeIds;	
	}
	

	private void insertIntoContact(String firstname, String surname,
			String mobile, String emailAddress, String city, String photo, Integer userId) {
		String sql = "insert into contact (email, phone, user_id, photo, firstname, lastname, city) values (?, ?, ?, ?, ?, ?, ?)";
		
		Object[] args = {emailAddress, mobile, userId, photo, firstname, surname, city};
		
		int[] types = new int[] {
			Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR
		};
		getJdbcTemplate().update(sql, args, types);
		
		System.out.println("Insert contact done ....");
	}


	private void updateContact(String firstname, String surname,
			String mobile, String emailAddress, String city, String photo, Integer userId) {
		String sql = "update contact set email = ?, phone = ?, photo = ?, firstname = ?, lastname = ?, city = ? where user_id = ?";

		Object[] args = {emailAddress, mobile, photo, firstname, surname, city, userId};

		int[] types = new int[] {
			Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR, Types.INTEGER
		};
		getJdbcTemplate().update(sql, args, types);
		System.out.println("Update Contact Done.....");
	}

	private void insertFeescodeStudent(List<Integer> feesCodeId,
			final int studentId) {

		// to delete if exists
		final String studenFeesCodeDelSql = "delete from feescode_student where student_id = " + studentId;

		getJdbcTemplate().update(studenFeesCodeDelSql);


		for (final Integer feesCode : feesCodeId) {
			String studenFeesCodeSql = "insert into feescode_student ( fees_code_configuration_id, student_id, is_active ) "
					+ "values( "+feesCode+", "+studentId+", 'Y')";
			getJdbcTemplate().update(studenFeesCodeSql);
		}
	}

	// @Transactional
	private int insertIntoStudentUpload(final Student student,
			final Integer userId) {
		final String studentuploadSql = "insert into student_upload(first_name, father_name, mother_name, last_name, gender, "
				+ "dob ,relation_with_parent, email_address, mobile, line1, area, city, pincode, religion, "
				+ "caste_id, standard_id, division_id, roll_number, active, registration_code, user_id, "
				+ "photo, branch_id, isDelete, hash_code)"
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection.prepareStatement(
						studentuploadSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, student.getFirstname());
				preparedStatement.setString(2, student.getFatherName());
				preparedStatement.setString(3, student.getMotherName());
				preparedStatement.setString(4, student.getSurname());
				preparedStatement.setString(5, student.getGender());
				preparedStatement.setString(6, StringUtils.isEmpty(student
						.getDateOfBirth()) ? null : student.getDateOfBirth());
				preparedStatement.setString(7, student.getRelationship());
				preparedStatement.setString(8, student.getEmailAddress());
				preparedStatement.setString(9, student.getMobile());
				preparedStatement.setString(10, student.getAddressLine1());
				preparedStatement.setString(11, student.getArea());
				preparedStatement.setString(12, student.getCity());
				preparedStatement.setString(13, student.getPincode());
				preparedStatement.setString(14, student.getReligion());

				if (student.getCasteId() == null) {
					preparedStatement.setNull(15, Types.NULL);
				} else {
					preparedStatement.setInt(15, student.getCasteId());
				}
				if (student.getStandardId() == null) {
					preparedStatement.setNull(16, Types.NULL);
				} else {
					preparedStatement.setInt(16, student.getStandardId());
				}
				if (student.getDivisionId() == null) {
					preparedStatement.setNull(17, Types.NULL);
				} else {
					preparedStatement.setInt(17, student.getDivisionId());
				}
				preparedStatement.setString(18, student.getRollNumber());
				preparedStatement.setString(19, (StringUtils.isEmpty(student
						.getActive()) ? "Y" : student.getActive()));
				preparedStatement.setString(20, student.getRegistrationCode());

				if (userId == null) {
					preparedStatement.setNull(21, Types.NULL);
				} else {
					preparedStatement.setInt(21, userId);
				}
				preparedStatement.setString(22, student.getPhoto());
				preparedStatement.setInt(23, student.getBranchId());
				preparedStatement.setString(24, "N");

				// if (student.getFeesCodeId() == null) {
				// preparedStatement.setNull(25, Types.NULL);
				// } else {
				// preparedStatement.setInt(25, student.getFeesCodeId());
				// }
				preparedStatement.setInt(25, student.hashCode());
				// preparedStatement.setString(26,
				// student.getCreateStudentLogin());
				return preparedStatement;
			}
		}, holder);
		int studentUploadId = holder.getKey().intValue();
		return studentUploadId;
	}

	/**
	 * Insert teacher standard.
	 * 
	 * @param teacherId
	 *            the teacher id
	 * @param teacherStandardList
	 *            the teacher standard list
	 */
	// @Transactional
	private List<Integer> insertStudentParent(final Student student,
			String token) throws Exception {
		List<Integer> parentIdList = new ArrayList<Integer>();
		List<StudentParent> studentParentList = student.getStudentParentList();

		if (studentParentList != null) {

			if (student.getUserId() == null || student.getUserId().equals(0)) {
				String sql = "select user_id from student where id = "
						+ student.getId();
				Integer userId = getJdbcTemplate().queryForObject(sql,
						Integer.class);
				student.setUserId(userId);
			}

			for (final StudentParent studentParent : studentParentList) {
				final List<Integer> parentIds = "Y".equals(studentParent
						.getCreateLogin()) ? insertIntoParent(studentParent,
						token, student, false) : null;

				if ((parentIds == null)
						&& "Y".equals(studentParent.getCreateLogin())) {
					throw new Exception("Problem while creating parent!!");
				}

				int parentUploadId = insertIntoParentUpload(student,
						studentParent);
				if (parentIds != null) {
					parentIdList.addAll(parentIds);
					studentParent.setParentId(null);

					insertStudentParentBranchLink(student.getId(),
							student.getBranchId(), parentIds, parentUploadId);
				}
			}
		}
		return parentIdList;
	}

	private int insertIntoParentUpload(final Student student,
			final StudentParent studentParent) {
		KeyHolder holder = new GeneratedKeyHolder();
		final String studentParentSql = "insert into parent_upload "
				+ " (firstname, middlename, lastname, dob, email, gender, primaryContact, secondaryContact, "
				+ " addressLine, area, city, branch_id, parent_id, relation_id, pincode, student_id, profession_id, income_range_id, create_login) "
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection.prepareStatement(
						studentParentSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, studentParent.getFirstname());
				preparedStatement.setString(2, studentParent.getMiddlename());
				preparedStatement.setString(3, studentParent.getLastname());
				preparedStatement.setString(4, studentParent.getDob());
				preparedStatement.setString(5, studentParent.getEmail());
				preparedStatement.setString(6, studentParent.getGender());
				preparedStatement.setString(7,
						studentParent.getPrimaryContact());
				preparedStatement.setString(8,
						studentParent.getSecondaryContact());
				preparedStatement.setString(9, studentParent.getAddressLine());
				preparedStatement.setString(10, studentParent.getArea());
				preparedStatement.setString(11, studentParent.getCity());
				preparedStatement.setInt(12, student.getBranchId());
				preparedStatement.setNull(13, Types.NULL);
				preparedStatement.setInt(14, studentParent.getRelationId());
				preparedStatement.setString(15, studentParent.getPincode());
				preparedStatement.setInt(16, student.getId());

				if (studentParent.getProfessionId() == null) {
					preparedStatement.setNull(17, Types.NULL);
				} else {
					preparedStatement.setInt(17,
							studentParent.getProfessionId());
				}

				if (studentParent.getIncomeRangeId() == null) {
					preparedStatement.setNull(18, Types.NULL);
				} else {
					preparedStatement.setInt(18,
							studentParent.getIncomeRangeId());
				}
				preparedStatement.setString(19, studentParent.getCreateLogin());
				return preparedStatement;
			}
		}, holder);
		int parentUploadId = holder.getKey().intValue();
		return parentUploadId;
	}

	private List<Integer> insertIntoParent(StudentParent parent, String token,
			Student student, boolean isStudentUpdate) throws Exception {
		logger.debug("Registration process start parent object is :: "
				+ parent.toString());

		logger.debug("Admin Parent registration process start :: check user already exists");

		boolean isUserExists = false;
		String firstname = parent.getFirstname();
		String lastname = parent.getLastname() != null ? parent.getLastname()
				: "";

		if (!isStudentUpdate) {
			List<StudentParent> parentInDatabaseList = retrieveParent(
					firstname, lastname, parent.getEmail(),
					parent.getPrimaryContact(), parent.getParentUserId());

			if (!CollectionUtils.isEmpty(parentInDatabaseList)) {
				List<Integer> parentRegistrationByAdminResponseList = new ArrayList<>();
				for (StudentParent parentObj : parentInDatabaseList) {
					parentRegistrationByAdminResponseList
							.add(parentObj.getId());
				}

				logger.debug("Number of Parents In database :::::: \n\n"
						+ parentInDatabaseList.size());

				if (parentInDatabaseList.size() == 1) {
					updateParentContactDetails(parent,
							parentInDatabaseList.get(0));
				}

				return parentRegistrationByAdminResponseList;
			}
		}
		return createParentLogin(parent, isUserExists, firstname, lastname);
	}

	private Long createUser(String username, String password, int roleId,
			String authenticationType) throws Exception {

		SimpleJdbcInsert insertUser = new SimpleJdbcInsert(getDataSource())
				.withTableName("user")
				.usingColumns("username", "password", "temp_password",
						"authentication_type", "password_salt")
				.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("username", username);

		byte[] randomSalt = SecurityUtil.generateSalt();
		byte[] passwordHash = SecurityUtil.hashPassword(password, randomSalt);

		parameters.put("password", passwordHash);
		parameters.put("authentication_type", authenticationType);
		parameters.put("password_salt", randomSalt);
		parameters.put("temp_password", password);

		Long userId = (Long) insertUser.executeAndReturnKey(parameters);
		logger.debug("createUser -> registered user id::" + userId);

		SimpleJdbcInsert insertUserRole = new SimpleJdbcInsert(getDataSource())
				.withTableName("user_role").usingColumns("user_id", "role_id")
				.usingGeneratedKeyColumns("id");
		parameters = new HashMap<String, Object>(3);
		parameters.put("user_id", userId);
		parameters.put("role_id", roleId);
		insertUserRole.execute(parameters);
		logger.debug("User role registered for::" + userId);

		return userId;

	}

	void createPaymentAccount(String paymentAccountType, Long userId) {
		SimpleJdbcInsert insertUserPaymentAccount = new SimpleJdbcInsert(
				getDataSource()).withTableName("user_payment_account")
				.usingColumns("account_type", "account_number", "user_id");
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("account_type", paymentAccountType);
		parameters.put("account_number",
				PaymentAccount.generateAccountNumberWithCheckDigit());
		parameters.put("user_id", userId);
		insertUserPaymentAccount.execute(parameters);
		logger.debug("Registered QfixPay account for user ::" + userId);
	}

	private List<Integer> createParentLogin(
			StudentParent parentRegistrationByAdminObj, boolean isUserExists,
			String firstname, String lastname) throws Exception {

		String password = RandomStringUtils.random(8, true, true).toUpperCase();
		DataSource dataSourceObj = getDataSource();

		Long userId = createUser(
				parentRegistrationByAdminObj.getParentUserId(), password,
				Constants.ROLE_ID_PARENT, "APPLICATION");
		createPaymentAccount("QFIXPAY", userId);

		SimpleJdbcInsert insertParent = new SimpleJdbcInsert(dataSourceObj)
				.withTableName("parent")
				.usingColumns("firstname", "lastname", "middlename", "dob",
						"gender", "email", "primaryContact",
						"secondaryContact", "user_id", "photo",
						"profession_id", "income_range_id")
				.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(4);
		parameters
				.put("firstname", parentRegistrationByAdminObj.getFirstname());
		parameters.put("lastname", lastname);
		parameters.put("middlename",
				parentRegistrationByAdminObj.getMiddlename());

		java.util.Date dob = null;
		if (parentRegistrationByAdminObj.getDob() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			dob = format.parse(parentRegistrationByAdminObj.getDob());
			parameters.put("dob", dob);
		} else {
			parameters.put("dob", null);
		}
		String email = parentRegistrationByAdminObj.getEmail();
		email = StringUtils.isEmpty(email) ? null : email;

		parameters.put("gender", parentRegistrationByAdminObj.getGender());
		parameters.put("email", email);
		parameters.put("primaryContact",
				parentRegistrationByAdminObj.getPrimaryContact());
		parameters.put("secondaryContact",
				parentRegistrationByAdminObj.getSecondaryContact());
		parameters.put("user_id", userId);
		parameters.put("photo", null);
		parameters.put("profession_id",
				parentRegistrationByAdminObj.getProfessionId());
		parameters.put("income_range_id",
				parentRegistrationByAdminObj.getIncomeRangeId());
		Long parentId = (Long) insertParent.executeAndReturnKey(parameters);
		logger.debug("Registered parent id::" + parentId);
		logger.debug("Registered parent on system");

		logger.debug("Sending emails.");

		Map<String, Object> content = new HashMap<String, Object>();
		content.put("first_name", parentRegistrationByAdminObj.getFirstname());
		content.put("username", parentRegistrationByAdminObj.getParentUserId());
		content.put("password", password);
		logger.debug("Parent successfully registerd.");
		List<Integer> parentRegistrationByAdminResponseList = new ArrayList<>();
		parentRegistrationByAdminResponseList.add(parentId.intValue());

		insertIntoContact(parentRegistrationByAdminObj.getFirstname(), parentRegistrationByAdminObj.getLastname(), parentRegistrationByAdminObj.getPrimaryContact(),
				parentRegistrationByAdminObj.getEmail(), parentRegistrationByAdminObj.getCity(), null, userId.intValue());

		return parentRegistrationByAdminResponseList;
	}

	private void updateParentContactDetails(StudentParent parentToUpdate,
			StudentParent parentInDb) {

		String newEmail = StringUtils.isEmpty(parentToUpdate.getEmail()) ? ""
				: parentToUpdate.getEmail();
		System.out.println(parentInDb);
		String oldEmail = StringUtils.isEmpty(parentInDb.getEmail()) ? ""
				: parentInDb.getEmail();
		String newPhone = StringUtils.isEmpty(parentToUpdate
				.getPrimaryContact()) ? "" : parentToUpdate.getPrimaryContact();
		String oldPhone = StringUtils.isEmpty(parentInDb.getPrimaryContact()) ? ""
				: parentInDb.getPrimaryContact();

		String email = (StringUtils.isEmpty(oldEmail) || !oldEmail
				.equals(newEmail)) ? newEmail : oldEmail;
		email = StringUtils.isEmpty(email) ? null : email;
		String primaryContact = (StringUtils.isEmpty(oldPhone) || !oldPhone
				.equals(newPhone)) ? newPhone : oldPhone;

		boolean contactInformationChanged = (!newEmail
				.equalsIgnoreCase(oldEmail) || !newPhone
				.equalsIgnoreCase(oldPhone));

		/*
		 * String parentStudentCountQuery =
		 * "select count(*) as count from branch_student_parent where branch_student_parent.parent_id = "
		 * + parentInDb.getId() + " and isDelete='N'"; Map<String, Object>
		 * queryParameters = new HashMap<String, Object>();
		 * queryParameters.put("parentId", parentInDb.getId());
		 * 
		 * Integer linkedStudentCount = getJdbcTemplate().queryForObject(
		 * parentStudentCountQuery, Integer.class);
		 */

		// if (linkedStudentCount == 1) {
		String parentUpdateQuery = "update parent set firstname=?, lastname=?, middlename=?,dob=?, "
				+ " gender=?, email=?, primaryContact=?, secondaryContact=?,profession_id=?, "
				+ " income_range_id = ? ";

		if (contactInformationChanged) {
			parentUpdateQuery += ",is_registration_email_sent='N' ";
		}

		parentUpdateQuery += " where id=" + parentInDb.getParentId();

		logger.info("parent update query:::::   " + parentUpdateQuery);

		String lastname = parentToUpdate.getLastname() != null ? parentToUpdate
				.getLastname() : "";

		java.util.Date dob = null;
		if (parentToUpdate.getDob() != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			try {
				dob = format.parse(parentToUpdate.getDob());
			} catch (ParseException e) {
				logger.error(e);
				dob = null;
			}
		}

		Object[] args = { parentToUpdate.getFirstname(), lastname,
				parentToUpdate.getMiddlename(), dob,
				parentToUpdate.getGender(), email, primaryContact,
				parentToUpdate.getSecondaryContact(),
				parentToUpdate.getProfessionId(),
				parentToUpdate.getIncomeRangeId() };

		int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.DATE, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER };
		System.out.println("ARGS :::::::: \n" + Arrays.asList(args));
		getJdbcTemplate().update(parentUpdateQuery, args, argTypes);

		updateContact(parentToUpdate.getFirstname(), parentToUpdate.getLastname(), parentToUpdate.getPrimaryContact(),
				parentToUpdate.getEmail(), parentToUpdate.getCity(), null, parentToUpdate.getUserId());
		logger.debug("Updated parent record where parent_upload_id=::"
				+ parentInDb.getId());

	}

	private List<StudentParent> retrieveParent(String firstname,
			String lastname, String email, String primaryContact,
			String username) {

		String retriveParentQuery = "select * from parent where firstname='"
				+ firstname + "' and lastname='" + lastname
				+ "' and (email = '" + email + "' OR primaryContact = '"
				+ primaryContact
				+ "' OR user_id = (select id from user where username = '"
				+ username + "'))";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("firstname", firstname);
		queryParameters.put("lastname", lastname);
		queryParameters.put("email", email);
		queryParameters.put("primaryContact", primaryContact);

		logger.debug("Retrive parent query :: " + retriveParentQuery);
		logger.debug("queryParameters :: " + queryParameters);

		List<StudentParent> parentList = getJdbcTemplate().query(
				retriveParentQuery, new RowMapper<StudentParent>() {
					@Override
					public StudentParent mapRow(ResultSet rs, int arg1)
							throws SQLException {
						StudentParent parent = new StudentParent();
						parent.setUserId(rs.getInt("user_id"));
						parent.setFirstname(rs.getString("firstname"));
						parent.setLastname(rs.getString("lastname"));
						parent.setEmail(rs.getString("email"));
						parent.setPrimaryContact(rs.getString("primaryContact"));
						parent.setId(rs.getInt("id"));
						// parent.getId();
						parent.setParentId(rs.getInt("id"));
						return parent;
					}
				});

		return parentList;
	}

	// @Transactional
	private void insertStudentParentBranchLink(final int studentId,
			final int branchId, List<Integer> parentIds,
			final int parentUploadId) {

		logger.debug("inside ::::: insertStudentParentBranchLink " + parentIds);
		if (parentIds == null) {
			return;
		}
		for (final Integer parentId : parentIds) {
			String sql = "select count(*) as count from branch_student_parent where student_id = "
					+ studentId
					+ " AND branch_id = "
					+ branchId
					+ " AND isDelete = 'N' AND parent_id = " + parentId;

			logger.debug("insertStudentParentBranchLink SQL:: " + sql);
			Integer count = getJdbcTemplate()
					.queryForObject(sql, Integer.class);

			if (count == 0) {
				final String branchStudentParentSql = "insert into branch_student_parent (branch_id, student_id, parent_id, parent_upload_id) "
						+ "values (?, ?, ?, ?)";

				getJdbcTemplate().update(new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(
							Connection connection) throws SQLException {
						PreparedStatement preparedStatement = connection
								.prepareStatement(branchStudentParentSql);
						preparedStatement.setInt(1, branchId);
						preparedStatement.setInt(2, studentId);
						preparedStatement.setInt(3, parentId);
						preparedStatement.setInt(4, parentUploadId);
						return preparedStatement;
					}
				});
			}
		}
	}

	// @Transactional
	private Integer insertIntoUser(final Student student)
			throws ApplicationException, NoSuchAlgorithmException {
		final String userSql = "insert into user (username, password, password_salt, temp_password, active, isDelete) "
				+ "values (?, ?, ?, ?, ?, ?)";
		final String password = generatePasswordByNameAndDOB(
				student.getFirstname(), student.getDateOfBirth());

		final byte[] randomSalt = SecurityUtil.generateSalt();
		final byte[] passwordHash = SecurityUtil.hashPassword(password,
				randomSalt);

		System.out.println("username :::: " + student.getUsername());
		// insert into user table.
		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatementUser = connection.prepareStatement(
						userSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatementUser.setString(1, student.getStudentUserId());
				preparedStatementUser.setBytes(2, passwordHash);
				preparedStatementUser.setBytes(3, randomSalt);
				preparedStatementUser.setString(4, password);
				preparedStatementUser.setString(5, ("Y".equals(student
						.getCreateStudentLogin()) ? "Y" : "N"));
				preparedStatementUser.setString(6, "N");
				return preparedStatementUser;
			}
		}, holder);
		logger.debug("insert student started..." + student.getStudentUserId());
		student.setPassword(password);
		// get latest insetred user_id from user table.
		final Integer userId = holder.getKey().intValue();
		// insert into user_role
		getJdbcTemplate().update(
				"insert into user_role values(" + userId + ", "
						+ Constants.ROLE_ID_STUDENT + ")");

		return userId;
	}

	// @Transactional
	private void updateStudentRelatedData(Student student,
			Student previousStudent, Integer userId, Integer chatUserProfileId) {

		Map<Integer, String> previousGroups = getGroupChatIdsByStandardAndDivision(previousStudent);
		Map<Integer, String> currentGroups = getGroupChatIdsByStandardAndDivision(student);

		List<Integer> previousGroupList = new ArrayList<>(
				previousGroups.keySet());
		List<Integer> currentGroupList = new ArrayList<>(currentGroups.keySet());

		if (!previousGroupList.containsAll(currentGroupList)
				|| !currentGroupList.containsAll(previousGroupList)) {
			Map<Integer, String> addedGroups = new HashMap<>();
			Map<Integer, String> deletedGroups = new HashMap<>();

			for (Integer groupId : currentGroupList) {
				if (!previousGroupList.contains(groupId)) {
					addedGroups.put(groupId, currentGroups.get(groupId));
				}
			}

			for (Integer groupId : previousGroupList) {
				if (!currentGroupList.contains(groupId)) {
					deletedGroups.put(groupId, previousGroups.get(groupId));
				}
			}
			if (addedGroups.size() > 0) {
				updateChatAndGroupData(addedGroups, chatUserProfileId, userId,
						student.getId(), true, Constants.ROLE_ID_STUDENT,
						userId);
			}
			if (deletedGroups.size() > 0) {
				updateChatAndGroupData(deletedGroups, chatUserProfileId,
						userId, student.getId(), false,
						Constants.ROLE_ID_STUDENT, userId);
			}
		}
	}

	@Transactional()
	public boolean updateStudent(final Student student, final MultipartFile file, String token, boolean isUploadUpdate)
			throws Exception {
		boolean flag = false;
		// get previous student first
		final Student previousStudent = getStudentById(student.getId());
		
		/*upload update with multiple fee code*/

		Integer userId = getJdbcTemplate().queryForObject("select user_id from student where id = " + student.getId(), Integer.class);
		getJdbcTemplate().update( "update user set active = '" + student.getActive() + "' where id = " + userId);

		final List<Integer> preFeesCodeId = getFeesCodesByStudentId(student.getId());
		List<Integer> feesCodeId = student.getFeesCodeId();
		
		/*upload update with multiple fee code*/
		if(feesCodeId==null){
			feesCodeId=getFeesCodeIdsByName(student);
			student.setFeesCodeId(feesCodeId);
			
		}
		
		student.setUserId(userId);
		updateStudentUpload(student);
		updateStudent(student);

		if ("N".equals(previousStudent.getCreateStudentLogin())
				&& "Y".equals(student.getCreateStudentLogin())) {
			System.out.println("Updating When Create Login Y :::::::::::::::::::::::::::::");
			updateProfilePhoto(student, file, userId);
		}

		findAndDeleteStudentParent(student, previousStudent);
		updateStudentParentList(student, previousStudent, token, isUploadUpdate);

		String sql = "select s.id, s.user_id, s.photo, s.branch_id, b.institute_id, "
				+ " ucp.chat_user_profile_id  "
				+ " from student as s "
				+ " INNER JOIN branch as b  on b.id = s.branch_id "
				+ " INNER JOIN user_chat_profile as ucp on ucp.user_id = s.user_id "
				+ " where s.id = " + student.getId();

		final int standardId = student.getStandardId() != null ? student.getStandardId() : 0;
		final int divisionId = student.getDivisionId() != null ? student.getDivisionId() : 0;
		final int casteId = student.getCasteId() != null ? student.getCasteId() : 0;
		
		

		final int preStandardId = previousStudent.getStandardId() != null ? previousStudent.getStandardId() : 0;
		final int preDivisionId = previousStudent.getDivisionId() != null ? previousStudent.getDivisionId() : 0;
		final int preCasteId = previousStudent.getCasteId() != null ? previousStudent.getCasteId() : 0;

		getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int arg1) throws SQLException {
				student.setId(resultSet.getInt("id"));
				student.setInstituteId(resultSet.getInt("institute_id"));
				student.setBranchId(resultSet.getInt("branch_id"));
				student.setPhoto(resultSet.getString("photo"));

				String photoPath;
				try {
					photoPath = saveFile(file, student, true);
					updatePhotoPath(student.getId(), photoPath);
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}

				if (standardId != preStandardId || divisionId != preDivisionId) {
					updateStudentRelatedData(student, previousStudent, resultSet.getInt("user_id"), resultSet.getInt("chat_user_profile_id"));
				}
				return null;
			}
		});

		boolean feesCodeChanged = false;
		if(preFeesCodeId != null && feesCodeId != null && (!preFeesCodeId.containsAll(feesCodeId) || !feesCodeId.containsAll(preFeesCodeId))){
			feesCodeChanged = true;
		}
		else if((feesCodeId == null && preFeesCodeId != null) || (feesCodeId != null && preFeesCodeId == null)){
			feesCodeChanged = true;
		}

		if (standardId != preStandardId || divisionId != preDivisionId || casteId != preCasteId || feesCodeChanged) {

			updateFeesSchedule(standardId, divisionId, casteId, feesCodeId,
					previousStudent.getUserId(), previousStudent.getBranchId(), student.getId());
		}

		// to delete the fees for removed fees_code
		if (feesCodeChanged && preFeesCodeId != null) {
			List<Integer> lstRm = preFeesCodeId;
			if(feesCodeId != null){
				lstRm.removeAll(feesCodeId);
			}
			removeFeesSchedule(lstRm, userId);
		}

		flag = true;
		logger.debug("Studnet Updated...");
		return flag;
	}

	private void removeFeesSchedule(List<Integer> lstRm, Integer userId) {
		
		String  sql = null;
		if(lstRm != null && lstRm.size() > 0){
			sql = "update fees_schedule set is_delete='Y' "
					+ " where status='UNPAID' and user_id = " + userId + " and fees_id in (select id from fees "
					+ " where  fees_code_configuration_id in ("
					+ org.apache.commons.lang3.StringUtils.join(lstRm, ",") + "))";
		}else{
			sql = "update fees_schedule set is_delete='Y' "
					+ " where status='UNPAID' and user_id = " + userId + " and fees_id in (select id from fees "
					+ " where  fees_code_configuration_id in (''))";
		}
		
		// for single time fees
		 System.out.println(sql);
		getJdbcTemplate().update(sql);

		/*
		// for recurring fees
		sql = "update fees_repeat_schedule set is_delete='Y' "
				+ "where fees_id in (select id from fees "
				+ "where recurring = 'Y'  and fees_code_configuration_id in ("
				+ org.apache.commons.lang3.StringUtils.join(lstRm, ",") + "))";
		// System.out.println(sql);
		getJdbcTemplate().update(sql);*/
	}

	boolean isFeesScheduled(Integer feesId, Integer userId) {
		String sql = "select count(*) as count from fees_schedule where user_id = "
				+ userId + " AND fees_id = " + feesId;
		int count = getJdbcTemplate().queryForObject(sql, Integer.class);
		return count > 0;
	}

	// TODO This method could lead to multiple fees mapping for one student depends of the flow used, Reverify and fix this later.
	private void updateFeesSchedule(int standardId, int divisionId,
			int casteId, List<Integer> feesCodeIds, final Integer userId,
			Integer branchId, Integer studentId) {

		logger.debug("Student academic details changed checking fees exist with thos details...");
		String sql = "select f.* from fees as f "
				+ " where f.branch_id = "
				+ branchId
				+ " AND f.is_delete = 'N' "
				+ " AND DATEDIFF(f.todate, CURDATE()) > 0"
				+ " AND  (f.caste_id = "
				+ casteId
				+ " or (f.caste_id is NULL)) AND ("
 				+(feesCodeIds != null && feesCodeIds.size() > 0 ? " f.fees_code_configuration_id in ( "
 						+ org.apache.commons.lang3.StringUtils.join(feesCodeIds, ",")
 						+ " ) OR ": "") +" f.fees_code_configuration_id is NULL) "
				+ " AND ((f.standard_id = "
				+ standardId
				+ " OR f.standard_id is null) "
				+ " AND (f.division_id IS NULL or f.division_id="
				+ divisionId
				+ ") "
				+ " AND (f.student_id = "
				+ studentId
				+ " OR f.student_id is null)) "
				+ " and NOT EXISTS(select id from fees_schedule where fees_schedule.fees_id = f.id AND fees_schedule.user_id = "
				+ userId + " and fees_schedule.is_delete = 'N')";

		logger.debug("get applicable fees for this student sql :::: " + sql);
		System.out.println("sql >>>> " + sql);
		final List<FeesSchedule> feeSchedules = new ArrayList<>();
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				FeesSchedule schedule = new FeesSchedule(0, rs.getInt("id"),
						userId, rs.getDouble("amount"), rs.getDouble("amount"),
						rs.getDate("duedate"), Constants.PAYMENT_STATUS.UNPAID
								.get(), rs.getDate("reminder_start_date"),
						null, rs.getDate("fromdate"));

				feeSchedules.add(schedule);
				return null;

			}
		});
		System.out.println("feeSchedules>>>>>>>>>>>" + feeSchedules);
		insertIntoFeesSchedule(feeSchedules);
	}

	private void insertIntoFeesSchedule(final List<FeesSchedule> feeSchedules) {

		if (feeSchedules != null && !feeSchedules.isEmpty()) {

			String sql = "insert into fees_schedule (fees_id, user_id, base_amount, "
					+ " late_payment_charges, total_amount, due_date, status, "
					+ " reminder_start_date, fee_for_duration, fee_start_date) "
					+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			getJdbcTemplate().batchUpdate(sql,
					new BatchPreparedStatementSetter() {

						@Override
						public void setValues(PreparedStatement ps, int i)
								throws SQLException {
							FeesSchedule schedule = feeSchedules.get(i);
							ps.setInt(1, schedule.getFeesId());
							ps.setInt(2, schedule.getUserId());
							ps.setDouble(3, schedule.getBaseAmount());
							ps.setDouble(4, 0.0);
							ps.setDouble(5, schedule.getTotalAmount());
							ps.setDate(6, schedule.getDueDate());
							ps.setString(7, Constants.PAYMENT_STATUS.UNPAID.get());
							ps.setDate(8, schedule.getReminderStartDate());
							ps.setString(9, schedule.getFeeForDuration());
							ps.setDate(10, schedule.getFeesStartDate());
						}

						@Override
						public int getBatchSize() {
							return feeSchedules.size();
						}
					});
		}
	}

	// @Transactional
	private void updateStudent(final Student student) {

		checkStudentContactDetailsChanged(student);
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				String studentSql = "update student set first_name = ?, father_name = ?, mother_name = ?, "
						+ " last_name = ?, gender = ?,dob = ?, relation_with_parent = ?,email_address = ?, "
						+ " mobile = ?,line1 = ?,area = ?,city = ?,pincode = ?,religion = ?,caste_id = ?, standard_id =?, "
						+ " division_id = ?, roll_number = ?, registration_code = ?, active = ?, branch_id = ?, isDelete = ?, "
						+ " district_id = ?, state_id = ?, taluka_id = ?,  create_login = ? where id = ?";

				PreparedStatement preparedStatement = connection
						.prepareStatement(studentSql);
				preparedStatement.setString(1, student.getFirstname());
				preparedStatement.setString(2, student.getFatherName());
				preparedStatement.setString(3, student.getMotherName());
				preparedStatement.setString(4, student.getSurname());
				preparedStatement.setString(5, student.getGender());
				preparedStatement.setString(6, StringUtils.isEmpty(student
						.getDateOfBirth()) ? null : student.getDateOfBirth());
				preparedStatement.setString(7, student.getRelationship());
				preparedStatement.setString(8, student.getEmailAddress());
				preparedStatement.setString(9, student.getMobile());
				preparedStatement.setString(10, student.getAddressLine1());
				preparedStatement.setString(11, student.getArea());
				preparedStatement.setString(12, student.getCity());
				preparedStatement.setString(13, student.getPincode());
				preparedStatement.setString(14, student.getReligion());

				if (student.getCasteId() == null) {
					preparedStatement.setNull(15, Types.NULL);
				} else {
					preparedStatement.setInt(15, student.getCasteId());
				}

				if (student.getStandardId() == null) {
					preparedStatement.setNull(16, Types.NULL);
				} else {
					preparedStatement.setInt(16, student.getStandardId());
				}

				if (student.getDivisionId() == null) {
					preparedStatement.setNull(17, Types.NULL);
				} else {
					preparedStatement.setInt(17, student.getDivisionId());
				}

				preparedStatement.setString(18, student.getRollNumber());
				preparedStatement.setString(19, student.getRegistrationCode());
				preparedStatement.setString(20, StringUtils.isEmpty(student
						.getActive()) ? "Y" : student.getActive());
				preparedStatement.setInt(21, student.getBranchId());
				preparedStatement.setString(22, "N");

				if (student.getDistrictId() == null) {
					preparedStatement.setNull(23, Types.NULL);
				} else {
					preparedStatement.setInt(23, student.getDistrictId());
				}

				if (student.getStateId() == null) {
					preparedStatement.setNull(24, Types.NULL);
				} else {
					preparedStatement.setInt(24, student.getStateId());
				}

				if (student.getTalukaId() == null) {
					preparedStatement.setNull(25, Types.NULL);
				} else {
					preparedStatement.setInt(25, student.getTalukaId());
				}

				// if (student.getFeesCodeId() == null) {
				// preparedStatement.setNull(26, Types.NULL);
				// } else {
				// preparedStatement.setInt(26, student.getFeesCodeId());
				// }

				preparedStatement.setString(26, student.getCreateStudentLogin());
				preparedStatement.setInt(27, student.getId());
				return preparedStatement;
			}
		});

		updateContact(student.getFirstname(), student.getSurname(), student.getMobile(), student.getEmailAddress(), student.getCity(), student.getPhoto(), student.getUserId());
		
		// insert feesCodes for student
		if(student.getFeesCodeId()!=null && student.getFeesCodeId().size()>0 ){
			insertFeescodeStudent(student.getFeesCodeId(), student.getId());
		}
	}

	/**
	 * Updates registration_email_sent flag to NO when email id or mobile number
	 * is changed.
	 * 
	 * @param Student
	 *            Student to update registrtion_email_sent flag by using mobile
	 *            and email.
	 */
	private void checkStudentContactDetailsChanged(final Student student) {
		String sql = "select email_address, mobile from student where id = "
				+ student.getId();
		getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				String email = resultSet.getString("email_address");
				String mobile = resultSet.getString("mobile");
				boolean isContactDetailsUpdated = false;

				if ((StringUtils.isEmpty(email) && !StringUtils.isEmpty(student
						.getEmailAddress()))
						|| (StringUtils.isEmpty(mobile) && !StringUtils
								.isEmpty(student.getMobile()))) {

					isContactDetailsUpdated = true;
				} else if ((!StringUtils.isEmpty(email) && !email
						.equals(student.getEmailAddress()))
						|| (!StringUtils.isEmpty(mobile) && !mobile
								.equals(student.getMobile()))) {

					isContactDetailsUpdated = true;
				}

				if (isContactDetailsUpdated) {
					logger.debug("Email or Mobile is updated for student id = "
							+ student.getId() + ", first_name = "
							+ student.getFirstname());
					logger.debug("updating registration_email_sent flag to NO....");
					String sql = "update student set is_registration_email_sent = 'N' where id = "
							+ student.getId();
					getJdbcTemplate().update(sql);
				}
				return null;
			}
		});
	}

	/**
	 * Updates registration_email_sent flag to NO when email id or mobile number
	 * is changed.
	 * 
	 * @param StudentParent
	 *            Student to update registrtion_email_sent flag by using mobile
	 *            and email.
	 */
	private boolean updateParentContactDetailsIfChanged(
			final StudentParent studentParent, final Integer studentUserId,
			final Student student, final String token) {

		String sql = "select create_login, email, primaryContact from parent_upload where id = "
				+ studentParent.getId();

		return getJdbcTemplate().queryForObject(sql, new RowMapper<Boolean>() {
			@Override
			public Boolean mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				String email = resultSet.getString("email");
				String primaryContact = resultSet.getString("primaryContact");
				boolean isContactDetailsUpdated = false;

				if ((StringUtils.isEmpty(email) && !StringUtils
						.isEmpty(studentParent.getEmail()))
						|| (StringUtils.isEmpty(primaryContact) && !StringUtils
								.isEmpty(studentParent.getPrimaryContact()))) {

					isContactDetailsUpdated = true;
				} else if ((!StringUtils.isEmpty(email) && !email
						.equals(studentParent.getEmail()))
						|| (!StringUtils.isEmpty(primaryContact) && !primaryContact
								.equals(studentParent.getPrimaryContact()))) {
					isContactDetailsUpdated = true;
				}

				if (isContactDetailsUpdated
						|| (resultSet.getString("create_login").equals("N") && studentParent
								.getCreateLogin().equals("Y"))) {

					/*
					 * // List<Integer> parentIdsToBeDeleted = null;
					 * 
					 * if (isContactDetailsUpdated) { parentIdsToBeDeleted =
					 * deleteParentIfOneChild( studentParent.getStudentId(),
					 * studentParent.getId(), false); }
					 * 
					 * System.out.println("Found Create Login YES :::::::::::");
					 * System.out
					 * .println("Found Create Login YES Registering ...:::::::::::"
					 * ); List<Integer> parentIds = null; try { parentIds =
					 * insertIntoParent(studentParent, token, student);
					 * insertStudentParentBranchLink(student.getId(),
					 * student.getBranchId(), parentIds, studentParent.getId());
					 * 
					 * if (parentIdsToBeDeleted != null) { if (parentIds !=
					 * null) { parentIdsToBeDeleted.removeAll(parentIds); }
					 * 
					 * for (Integer parentId : parentIdsToBeDeleted) {
					 * deleteParent(parentId); } } } catch (Exception e) { throw
					 * new RuntimeException(e); }
					 * studentParent.setParentId(null);
					 */
				}
				return false;
			}
		});
	}

	private StudentParent getPreviousParentById(List<StudentParent> parents,
			Integer id) {
		for (StudentParent parent : parents) {
			if (parent.getId().equals(id)) {
				return parent;
			}
		}
		return null;
	}

	// @Transactional
	private void updateStudentParentList(final Student student,
			Student previousStudent, String token, boolean isUploadUpdate)
			throws Exception {

		List<StudentParent> studentParentList = student.getStudentParentList();

		if (studentParentList != null && studentParentList.size() > 0) {
			List<StudentParent> studentParentListToCreate = new ArrayList<>();

			for (final StudentParent studentParent : studentParentList) {

				if ((!isUploadUpdate && studentParent.getId() != null)
						|| "Update".equals(studentParent.getUploadAction())
						|| "Add".equals(studentParent.getUploadAction())) {

					StudentParent parentInDb = getPreviousParentById(
							previousStudent.getStudentParentList(),
							studentParent.getId());

					boolean insertIntoParent = false;
					if ("Add".equals(studentParent.getUploadAction())) {
						Integer parentUploadId = insertIntoParentUpload(
								student, studentParent);
						studentParent.setId(parentUploadId);
					}
					// updates inside parent table.
					if ((parentInDb == null || "N".equals(parentInDb
							.getCreateLogin()))
							&& "Y".equals(studentParent.getCreateLogin())) {
						insertIntoParent = true;
					}
					if (insertIntoParent) {
						studentParentListToCreate.add(studentParent);
					}

					if (!insertIntoParent && "Y".equals(studentParent.getCreateLogin())) {
						updateParentContactDetails(studentParent, parentInDb);
					}
					if (!"Add".equals(studentParent.getUploadAction())) {
						updateParentUpload(studentParent);
					}
				} else if (studentParent.getId() != null
						&& "Delete".equals(studentParent.getUploadAction())) {
					deleteParentIfOneChild(student.getId(),
							studentParent.getId(), true);
					String sql = "update `parent_upload` set isDelete = 'Y' where id = "
							+ studentParent.getId();
					getJdbcTemplate().update(sql);
				} else if ("Add".equals(studentParent.getUploadAction()) || studentParent.getId() == null) {
					studentParentListToCreate.add(studentParent);
				}
			}

			if(studentParentListToCreate.size() > 0){
				student.setStudentParentList(studentParentListToCreate);
				insertStudentParent(student, token);
			}

//			for (final StudentParent studentParent : studentParentList) {
//				studentParentListToCreate.add(studentParent);
//				if (studentParentListToCreate.size() > 0) {
//					student.setStudentParentList(studentParentListToCreate);
//					insertStudentParent(student, token);
//				}
//			}
		}
	}

	private void updateParentUpload(final StudentParent studentParent) {
		final String updateStudentParentSql = "update parent_upload set firstname=?, "
				+ " middlename=?, lastname=?, dob=?, email=?, gender=?, primaryContact=?, "
				+ " secondaryContact=?, addressLine=?, area=?, city=?, relation_id=?, "
				+ " pincode=? , profession_id = ?, income_range_id = ?, create_login = ? where id = ?";

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection
						.prepareStatement(updateStudentParentSql);
				preparedStatement.setString(1, studentParent.getFirstname());
				preparedStatement.setString(2, studentParent.getMiddlename());
				preparedStatement.setString(3, studentParent.getLastname());
				preparedStatement.setString(4, studentParent.getDob());
				preparedStatement.setString(5, studentParent.getEmail());
				preparedStatement.setString(6, studentParent.getGender());
				preparedStatement.setString(7,
						studentParent.getPrimaryContact());
				preparedStatement.setString(8,
						studentParent.getSecondaryContact());
				preparedStatement.setString(9, studentParent.getAddressLine());
				preparedStatement.setString(10, studentParent.getArea());
				preparedStatement.setString(11, studentParent.getCity());
				preparedStatement.setInt(12, studentParent.getRelationId());
				preparedStatement.setString(13, studentParent.getPincode());

				if (studentParent.getProfessionId() == null) {
					preparedStatement.setNull(14, Types.NULL);
				} else {
					preparedStatement.setInt(14,
							studentParent.getProfessionId());
				}

				if (studentParent.getIncomeRangeId() == null) {
					preparedStatement.setNull(15, Types.NULL);
				} else {
					preparedStatement.setInt(15,
							studentParent.getIncomeRangeId());
				}

				preparedStatement.setString(16, studentParent.getCreateLogin());
				preparedStatement.setInt(17, studentParent.getId());
				return preparedStatement;
			}
		});
	}

	// @Transactional
	private void findAndDeleteStudentParent(Student student,
			Student previousStudent) {

		int studentId = student.getId();
		List<StudentParent> parentUploadExistingList = previousStudent
				.getStudentParentList();

		// check received parentids
		List<StudentParent> studentParentList = student.getStudentParentList();

		if (studentParentList != null && studentParentList.size() > 0) {
			for (StudentParent existingStudentParent : parentUploadExistingList) {

				boolean isParentDeleted = true;
				if (studentParentList != null && studentParentList.size() > 0) {
					for (StudentParent studentParent : studentParentList) {
						if (!"Delete".equals(studentParent.getUploadAction())
								&& studentParent.getId() != null
								&& studentParent.getId().intValue() == existingStudentParent
										.getId().intValue()) {
							isParentDeleted = false;
							break;
						}
					}
				}
				if (isParentDeleted) {
					deleteParentIfOneChild(studentId,
							existingStudentParent.getId(), true);
					String studentUploadDeletesql = "update parent_upload set isDelete = 'Y' where id = "
							+ existingStudentParent.getId();
					getJdbcTemplate().update(studentUploadDeletesql);
				}
			}
		} else {
			for (StudentParent studentParent : parentUploadExistingList) {
				// unlinkStudentParentReferences(studentParent,
				// student.getUserId());
				deleteParentIfOneChild(studentId, studentParent.getId(), true);
				String studentUploadDeletesql = "update parent_upload set isDelete = 'Y' where id = "
						+ studentParent.getId();
				getJdbcTemplate().update(studentUploadDeletesql);
			}
		}
	}

	// @Transactional
	private void unlinkStudentParentReferences(StudentParent parent,
			Integer studentUserId) {

		// delete from branch_student_parent
		String studentParentDeleteSql = "update branch_student_parent set isDelete = 'Y' where parent_id = "
				+ parent.getParentId()
				+ " AND student_id = "
				+ parent.getStudentId();

		getJdbcTemplate().update(studentParentDeleteSql);
		Map<Integer, String> groupChatIds = getGroupChatIdsByStudentId(parent
				.getStudentId());
		Long chatUserProfileId = getChatUserProfileByUserId(parent.getUserId());
		if (chatUserProfileId != null) {
			updateChatAndGroupData(groupChatIds, chatUserProfileId,
					parent.getUserId(), parent.getStudentId(), false,
					Constants.ROLE_ID_PARENT, studentUserId);
		}
	}

	private Long getChatUserProfileByUserId(Integer userId) {
		Long userChatProfile = null;
		try {
			String sql = "select chat_user_profile_id from user_chat_profile where user_id = "
					+ userId;
			userChatProfile = getJdbcTemplate().queryForObject(sql, Long.class);
		} catch (Exception e) {
			logger.error("Chat user profile not found...", e);
		}
		return userChatProfile;
	}

	private Map<Integer, String> getGroupChatIdsByStudentId(Integer studentId) {
		final Map<Integer, String> groupChatIds = new HashMap<>();

		String sql = "select g.chat_dialogue_id as groupChatId, g.id as groupId FROM `group` as g "
				+ " INNER JOIN group_user as gu on g.id = gu.group_id "
				+ " WHERE g.isDelete = 'N' AND g.active = 'Y' "
				+ " AND gu.user_id = (select user_id from student where id = "
				+ studentId + ")" + " group by gu.group_id";

		System.out.println("getGroupChatIdsByStandard SQL :::::::::::\n" + sql);

		getJdbcTemplate().query(sql, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				groupChatIds.put(resultSet.getInt("groupId"),
						resultSet.getString("groupChatId"));
				return null;
			}
		});
		return groupChatIds;
	}

	// @Transactional
	private void updateStudentUpload(final Student student) {
		String sql = "select student_upload_id from student where id = "
				+ student.getId();

		final int studentUploadId = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("student_upload_id");
					}
				}).get(0);

		logger.debug("Update Studnet Started..." + student.getId());

		if (studentUploadId != 0) {
			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					String studentUploadSql = "update student_upload set first_name = ?, father_name = ?, "
							+ "mother_name = ?, last_name = ?, gender = ?,dob = ?, relation_with_parent = ?, "
							+ "email_address = ?, mobile = ?,line1 = ?,area = ?,city = ?,pincode = ?, "
							+ "religion = ?,caste_id = ?, standard_id =?, division_id = ?, roll_number = ?, "
							+ "registration_code = ?, active = ?, branch_id = ?, isDelete = ?,  hash_code = ? where id = ?";

					PreparedStatement preparedStatement = connection
							.prepareStatement(studentUploadSql);
					preparedStatement.setString(1, student.getFirstname());
					preparedStatement.setString(2, student.getFatherName());
					preparedStatement.setString(3, student.getMotherName());
					preparedStatement.setString(4, student.getSurname());
					preparedStatement.setString(5, student.getGender());
					preparedStatement.setString(
							6,
							StringUtils.isEmpty(student.getDateOfBirth()) ? null
									: student.getDateOfBirth());
					preparedStatement.setString(7, student.getRelationship());
					preparedStatement.setString(8, student.getEmailAddress());
					preparedStatement.setString(9, student.getMobile());
					preparedStatement.setString(10, student.getAddressLine1());
					preparedStatement.setString(11, student.getArea());
					preparedStatement.setString(12, student.getCity());
					preparedStatement.setString(13, student.getPincode());
					preparedStatement.setString(14, student.getReligion());

					if (student.getCasteId() == null) {
						preparedStatement.setNull(15, Types.NULL);
					} else {
						preparedStatement.setInt(15, student.getCasteId());
					}

					if (student.getStandardId() == null) {
						preparedStatement.setNull(16, Types.NULL);
					} else {
						preparedStatement.setInt(16, student.getStandardId());
					}

					if (student.getDivisionId() == null) {
						preparedStatement.setNull(17, Types.NULL);
					} else {
						preparedStatement.setInt(17, student.getDivisionId());
					}

					preparedStatement.setString(18, student.getRollNumber());
					preparedStatement.setString(19,
							student.getRegistrationCode());
					preparedStatement.setString(20, StringUtils.isEmpty(student
							.getActive()) ? "Y" : student.getActive());
					preparedStatement.setInt(21, student.getBranchId());
					preparedStatement.setString(22, "N");

					// if (student.getFeesCodeId() == null) {
					// preparedStatement.setNull(23, Types.NULL);
					// } else {
					// preparedStatement.setInt(23, student.getFeesCodeId());
					// }
					preparedStatement.setInt(23, student.hashCode());
					preparedStatement.setInt(24, studentUploadId);
					return preparedStatement;
				}
			});
		}
	}

	@Transactional
	public boolean deleteStudent(final Integer id) {
		boolean flag = false;

		String username = RandomStringUtils.random(40, true, true);
		String mobile = RandomStringUtils.random(5, false, true);
		String sql = "update student set isDelete = 'Y', email_address = '"
				+ username + "', mobile = '" + mobile + "' where id = " + id;
		getJdbcTemplate().update(sql);

		sql = "update student_upload set isDelete = 'Y' where id = (select student_upload_id from student where id = "
				+ id + ")";
		getJdbcTemplate().update(sql);

		sql = "update `user` set isDelete = 'Y', username = '" + username
				+ "' where id = (select user_id from student where id = " + id
				+ ")";
		getJdbcTemplate().update(sql);

		sql = "update `user_forum_profile` set is_delete = 'Y' where id = (select user_id from student where id = "
				+ id + ")";
		getJdbcTemplate().update(sql);

		deleteForumUser(id);
		flag = true;
		logger.debug("Student Deleted..." + id);

		deleteParentIfOneChild(id, null, true);

		sql = "update `parent_upload` set isDelete = 'Y' where student_id = "
				+ id;
		getJdbcTemplate().update(sql);

		sql = "update fees_schedule set is_delete = 'Y' where user_id = (select user_id from student where id = "
				+ id + ")";
		getJdbcTemplate().update(sql);
		return flag;
	}

	private List<Integer> deleteParentIfOneChild(final Integer studentId,
			final Integer parentUploadId,
			final boolean deleteParentInTransactionScope) {

		String sql = "select branch_id, user_id from student where id = "
				+ studentId;

		return getJdbcTemplate().queryForObject(sql,
				new RowMapper<List<Integer>>() {
					@Override
					public List<Integer> mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						final Integer branchId = resultSet.getInt("branch_id");
						final Integer studentUserId = resultSet
								.getInt("user_id");

						String sql = "select p.user_id as parentUserId, "
								+ " bsp.parent_id, bsp.parent_upload_id from branch_student_parent as bsp"
								+ " INNER JOIN parent as p on p.id = bsp.parent_id "
								+ " where  bsp.parent_id in"
								+ (parentUploadId == null ? " (select parent_id from branch_student_parent where student_id = "
										+ studentId
										+ " AND branch_id = "
										+ branchId
										+ " AND isDelete = 'N' group by parent_id)"
										: " (select parent_id from branch_student_parent where parent_upload_id = "
												+ parentUploadId
												+ " AND isDelete = 'N' group by parent_id) ")
								+ " AND bsp.isDelete = 'N' ";
						final Map<Integer, Integer> parentStudentCount = new HashMap<Integer, Integer>();

						System.out
								.println("deleteParentIfOneChild ::::::::::::\n"
										+ sql);
						getJdbcTemplate().query(sql, new RowMapper<Integer>() {
							@Override
							public Integer mapRow(ResultSet rs, int arg1)
									throws SQLException {
								int count = 1;
								int parentId = rs.getInt("parent_id");

								if (parentStudentCount.containsKey(parentId)) {
									count = parentStudentCount.get(parentId) + 1;
								}

								parentStudentCount.put(parentId, count);
								Integer parentUserId = rs
										.getInt("parentUserId");

								StudentParent parent = new StudentParent();
								parent.setId(parentUploadId == null ? rs
										.getInt("parent_upload_id")
										: parentUploadId);
								parent.setParentId(rs.getInt("parent_id"));
								parent.setUserId(parentUserId);
								parent.setStudentId(studentId);
								unlinkStudentParentReferences(parent,
										studentUserId);

								return parentUserId;
							}
						});

						List<Integer> parentIdsToBeDeleted = new ArrayList<Integer>();

						for (Integer parentId : parentStudentCount.keySet()) {

							Integer count = parentStudentCount.get(parentId);
							if (count == 1 && deleteParentInTransactionScope) {
								deleteParent(parentId);

							} else if (count == 1) {
								parentIdsToBeDeleted.add(parentId);
							}
						}
						return parentIdsToBeDeleted;
					}
				});

		/*
		 * if(deleteFromParentUpload){ Student student = new Student();
		 * student.setId(studentId); student.setBranchId(branchId);
		 * 
		 * findAndDeleteStudentParent(student); }
		 */
	}

	private void deleteParent(Integer parentId) {
		String sql;
		String username = RandomStringUtils.random(40, true, true);
		String mobile = RandomStringUtils.random(5, false, true);
		sql = "update parent set email = '" + username
				+ "', primaryContact = '" + mobile + "' where id = " + parentId;
		getJdbcTemplate().update(sql);

		sql = "update user set isDelete = 'Y', username = '" + username + "' "
				+ "where id = (select user_id from parent where id = "
				+ parentId + ")";
		getJdbcTemplate().update(sql);
	}

	// @Transactional
	private void deleteForumUser(Integer id) {
		Set<Integer> userIds = new HashSet<Integer>();
		String sql = "select forum_profile_id from user_forum_profile "
				+ "where user_id = (select user_id from student where id = "
				+ id + ")";

		List<Integer> userIdList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("forum_profile_id");
					}
				});

		userIds.addAll(userIdList);
		new ForumProfileDao().deleteUsers(userIds);
	}

	// @Transactional
	private void createPaymentAccount(final Integer userId) throws SQLException {

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				String sql = "insert into user_payment_account "
						+ "(account_type, user_id, account_number) values(?, ?, ?)";
				PreparedStatement preparedStmt = connection
						.prepareStatement(sql);
				preparedStmt.setString(1, "QFIXPAY");

				if (userId == null) {
					preparedStmt.setNull(2, Types.NULL);
				} else {
					preparedStmt.setInt(2, userId);
				}
				preparedStmt.setLong(3,
						PaymentAccount.generateAccountNumberWithCheckDigit());
				return preparedStmt;
			}
		});
		logger.debug("payment Account created..." + userId);
	}

	// @Transactional
	private void updatePhotoPath(Integer id, String photoPath)
			throws SQLException {
		String sql = "update student set photo = '" + photoPath
				+ "' where id = " + id;
		getJdbcTemplate().update(sql);
		sql = "update student_upload set photo = '" + photoPath
				+ "' where id = " + id;
		getJdbcTemplate().update(sql);
	}

	@Transactional(isolation = Isolation.READ_COMMITTED)
	public boolean uploadStudent(List<Student> students, String token)
			throws Exception {

		for (Student student : students) {
			insertStudent(student, null, token);
		}
		return true;
	}

	@Override
	public TableResponse getStudentReport(StudentExcelReportFilter filter) {
		TableResponse tableResponse = new TableResponse();
		TableOptions tableOptions = filter.getTableOptions();
		int start = tableOptions == null ? 0 : tableOptions.getStart();
		int length = tableOptions == null ? 0 : tableOptions.getLength();
		String searchTxt = (String) (tableOptions == null ? "" : tableOptions
				.getSearch().get("value"));

		String selectString = " select s.*, b.name as branchName, st.displayName as standard, "
				+ " d.displayName as division, cs.name as caste, sta.name as state, dis.name as district, tal.name as taluka, "
				+ " GROUP_CONCAT(fcc.name separator ',') as feesCodeName ";

		String sql = " from "
				+ " student as s INNER JOIN branch as b on b.id = s.branch_id "
				+ " LEFT JOIN standard as st on st.id = s.standard_id AND st.active = 'Y'"
				+ " LEFT JOIN division as d on d.id = s.division_id AND d.active = 'Y'"
				+ (filter.getTeacherUserId() != null ? " INNER JOIN teacher_standard as ts on d.id = ts.division_id "
						+ " AND st.id = ts.standard_id "
						+ " AND ts.teacher_id = (select id from teacher where user_id = "
						+ filter.getTeacherUserId() + ")"
						: "")
				+ " LEFT JOIN feescode_student as fs on fs.student_id = s.id "
				+ " LEFT JOIN caste as cs on cs.id = s.caste_id AND cs.active = 'Y'"
				+ " LEFT JOIN state as sta on sta.id = s.state_id "
				+ " LEFT JOIN district as dis on dis.id = s.district_id "
				+ " LEFT JOIN taluka as tal on tal.id = s.taluka_id "
				+ " LEFT JOIN fees_code_configuration as fcc on fcc.id = fs.fees_code_configuration_id "
				+ " where "
				+ (StringUtils.isEmpty(searchTxt) ? ""
						: " (s.first_name like '%" + searchTxt
								+ "%' or s.last_name like '%" + searchTxt
								+ "%'  OR s.mobile like '%" + searchTxt
								+ "%' OR s.email_address like '%" + searchTxt
								+ "%' OR s.registration_code like '%"
								+ searchTxt + "%' ) AND")
				+ " s.isDelete = 'N' AND s.branch_id = '"
				+ filter.getBranchId()
				+ "' "
				+ (!StringUtils.isEmpty(filter.getStandardId()) ? " AND s.standard_id = '"
						+ filter.getStandardId() + "'"
						: "")
				+ (!StringUtils.isEmpty(filter.getDivisionId()) ? " AND s.division_id = '"
						+ filter.getDivisionId() + "'"
						: "");

		int totalCount = getJdbcTemplate().queryForObject(
				"select count(*) as count " + sql, Integer.class);

		tableResponse
				.setDraw(tableOptions != null ? tableOptions.getDraw() : 0);
		tableResponse.setRecordsTotal(totalCount);
		tableResponse.setRecordsFiltered(totalCount);

		sql += " GROUP BY s.id ";
		if (length > 0) {
			sql += " LIMIT " + start + ", " + length;
		}

		logger.debug("get Student sql --" + selectString);
		final DateUtil dateUtil = new DateUtil();

		List<Student> students = getJdbcTemplate().query(selectString + sql ,
				new RowMapper<Student>() {
					@Override
					public Student mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						Student student = new Student();
						student.setId(resultSet.getInt("id"));
						student.setFirstname(resultSet.getString("first_name"));
						student.setFatherName(resultSet
								.getString("father_name"));
						student.setMotherName(resultSet
								.getString("mother_name"));
						student.setSurname(resultSet.getString("last_name"));
						student.setStandardName(resultSet.getString("standard"));
						student.setDivisionName(resultSet.getString("division"));
						student.setCaste(resultSet.getString("caste"));
						student.setRollNumber(resultSet
								.getString("roll_number"));
						student.setRegistrationCode(resultSet
								.getString("registration_code"));
						student.setGender(resultSet.getString("gender"));
						student.setDateOfBirth(StringUtils.isEmpty(resultSet
								.getString("dob")) ? null
								: Constants.USER_DATE_FORMAT.format(dateUtil
										.getDate(resultSet.getString("dob"))));
						student.setEmailAddress(resultSet
								.getString("email_address"));
						student.setMobile(resultSet.getString("mobile"));
						if (student.getMobile() == null
								|| student.getMobile().equals("")) {
							List<String> numbers = getParentMobile(student
									.getId());
							for (String number : numbers) {
								if (number != null || number != "") {
									student.setMobile(number);
								}
							}
						}
						student.setAddressLine1(resultSet.getString("line1"));
						student.setArea(resultSet.getString("area"));
						student.setCity(resultSet.getString("city"));
						student.setState(resultSet.getString("state"));
						student.setDistrict(resultSet.getString("district"));
						student.setTaluka(resultSet.getString("taluka"));
						student.setPincode(resultSet.getString("pincode"));
						student.setReligion(resultSet.getString("religion"));
						student.setActive(resultSet.getString("active"));
						student.setIsDelete(resultSet.getString("isDelete"));
						 student.setFeesCodeName(resultSet.getString("feesCodeName"));
						return student;
					}
				});

		tableResponse.setStudents(students);
		return tableResponse;
	}

	@Override
	public List<StudentParent> exportParentData(Integer branchId,
			Integer standardId, Integer divisionId) {

		String sql = "select s.first_name as studentFirstName, s.last_name as studentLastName ,s.registration_code as registrationCode,s.roll_number as rollNumber, b.name as branch, p.*, st.displayName as standard, "
				+ " d.displayName as division, u.username as username, u.temp_password as password "
				+ " from "
				+ " parent as p "
				+ " INNER JOIN branch_student_parent as bsp on bsp.parent_id = p.id "
				+ " INNER JOIN student as s on s.id = bsp.student_id "
				+ " INNER JOIN branch as b on b.id = bsp.branch_id "
				+ "  INNER JOIN user as u on u.id = p.user_id "
				+ " LEFT JOIN standard as st on st.id = s.standard_id AND st.active = 'Y'"
				+ " LEFT JOIN division as d on d.id = s.division_id AND d.active = 'Y'"
				+ " where "
				+ " s.isDelete = 'N' AND bsp.isDelete =  'N' AND bsp.branch_id = '"
				+ branchId
				+ "' "
				+ (standardId != null ? " AND s.standard_id = '" + standardId
						+ "'" : "")
				+ (divisionId != null ? " AND s.division_id = '" + divisionId
						+ "'" : "")
				+ " group by s.id order by s.first_name, s.last_name ";

		List<StudentParent> students = getJdbcTemplate().query(sql,
				new RowMapper<StudentParent>() {
					@Override
					public StudentParent mapRow(ResultSet rs, int arg1)
							throws SQLException {
						Student student = new Student();
						student.setFirstname(rs.getString("studentFirstName"));
						student.setSurname(rs.getString("studentLastName"));
						student.setStandardName(rs.getString("standard"));
						student.setBranch(rs.getString("branch"));
						student.setDivisionName(rs.getString("division"));
						student.setRegistrationCode(rs
								.getString("registrationCode"));
						student.setRollNumber(rs.getString("rollNumber"));
						StudentParent parent = new StudentParent();
						parent.setFirstname(rs.getString("firstname"));
						parent.setLastname(rs.getString("lastname"));

						parent.setStudent(student);
						parent.setEmail(rs.getString("email"));
						parent.setPrimaryContact(rs.getString("primaryContact"));
						parent.setUsername(rs.getString("username"));
						parent.setPassword(rs.getString("password"));
						return parent;
					}
				});
//		System.out.println(students);

		return students;
	}

	@Override
	public List<Student> exportStudentData(Integer branchId,
			Integer standardId, Integer divisionId) {

		String sql = " select s.*, b.name as branch, st.displayName as standard, "
				+ " d.displayName as division, u.username as username, u.temp_password as password "
				+ " from "
				+ " student as s INNER JOIN branch as b on b.id = s.branch_id "
				+ " INNER JOIN user as u on u.id = s.user_id "
				+ " LEFT JOIN standard as st on st.id = s.standard_id AND st.active = 'Y'"
				+ " LEFT JOIN division as d on d.id = s.division_id AND d.active = 'Y'"
				+ " where "
				+ " s.create_login = 'Y' AND s.isDelete = 'N' AND s.branch_id = '"
				+ branchId
				+ "' "
				+ (standardId != null ? " AND s.standard_id = '" + standardId
						+ "'" : "")
				+ (divisionId != null ? " AND s.division_id = '" + divisionId
						+ "'" : "")
				+ " group by s.id order by s.first_name, s.last_name ";

		List<Student> students = getJdbcTemplate().query(sql,
				new RowMapper<Student>() {
					@Override
					public Student mapRow(ResultSet rs, int arg1)
							throws SQLException {
						Student student = new Student();
						student.setFirstname(rs.getString("first_name"));
						student.setSurname(rs.getString("last_name"));
						student.setEmailAddress(rs.getString("email_address"));
						student.setMobile(rs.getString("mobile"));
						student.setUsername(rs.getString("username"));
						student.setPassword(rs.getString("password"));
						student.setStandardName(rs.getString("standard"));
						student.setBranch(rs.getString("branch"));
						student.setDivisionName(rs.getString("division"));
						student.setRollNumber(rs.getString("roll_number"));
						student.setRegistrationCode(rs
								.getString("registration_code"));
						return student;
					}
				});
//		System.out.println(students);
		return students;
	}

	String GET_STUDENTS_TO_DOWNLOAD_QUERY = "select s.*, u.username, b.institute_id, c.name as casteName, "
			+ " GROUP_CONCAT(fcc.name separator ',') as feesCodes, "
			+ " std.displayName as standard, d.displayName as division, st.name as state, dist.name as district, t.name as taluka "
			+ " from student as s "
			+ " INNER JOIN user as u on u.id = s.user_id "
			+ " INNER JOIN branch as b on b.id = s.branch_id "
			+ " LEFT JOIN standard as std on std.id = s.standard_id "
			+ " LEFT JOIN division as d on d.id = s.division_id "
			+ " LEFT JOIN feescode_student as fcs on fcs.student_id = s.id "
			+ " LEFT JOIN fees_code_configuration as fcc on fcc.id = fcs.fees_code_configuration_id "
			+ " LEFT JOIN state as st on st.id = s.state_id "
			+ " LEFT JOIN district as dist on dist.id = s.district_id "
			+ " LEFT JOIN taluka as t on t.id = s.taluka_id "
			+ " LEFT JOIN caste as c on c.id = s.caste_id ";

	/*
	 * 
	 * String sql =
	 * " SELECT u.username, pu.*, p.id as parentId, ir.name as income_range, pro.name as profession "
	 * + " from parent_upload pu" +
	 * " LEFT JOIN branch_student_parent as bsp on bsp.parent_upload_id = pu.id AND bsp.isDelete = 'N'"
	 * + " LEFT JOIN parent as p on p.id = bsp.parent_id " +
	 * " LEFT JOIN user as u on u.id = p.user_id " +
	 * " LEFT JOIN profession as pro on pro.id = p.profession_id " +
	 * " LEFT JOIN income_range as ir on ir.id = p.income_range_id " +
	 * " where pu.branch_id = " + branchId + " and pu.student_id = " + stduentId
	 * + " AND pu.isDelete = 'N'";
	 */

	@Override
	public List<Student> getAllStudentsWithParents(Integer branchId) {
		String sql = GET_STUDENTS_TO_DOWNLOAD_QUERY
				+ " where s.isDelete = 'N' AND s.branch_id = " + branchId 
				+ " GROUP BY s.id ";
		logger.debug("get Student by Id SQl --" + sql);
		System.out.println("Download Detail Reprot :::>>\n"+sql);
		List<Student> students = getJdbcTemplate().query(sql,
				new RowMapper<Student>() {
					@Override
					public Student mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						Student student = new Student();
						student.setId(resultSet.getInt("id"));
						student.setFirstname(resultSet.getString("first_name"));
						student.setFatherName(resultSet.getString("father_name"));
						student.setMotherName(resultSet.getString("mother_name"));
						student.setSurname(resultSet.getString("last_name"));

						student.setGender(resultSet.getString("gender"));
						student.setDateOfBirth(resultSet.getString("dob"));
						student.setRelationship(resultSet.getString("relation_with_parent"));
						student.setEmailAddress(resultSet.getString("email_address"));
						student.setMobile(resultSet.getString("mobile"));
						student.setAddressLine1(resultSet.getString("line1"));
						student.setArea(resultSet.getString("area"));
						student.setCity(resultSet.getString("city"));
						student.setPincode(resultSet.getString("pincode"));
						student.setReligion(resultSet.getString("religion"));
						student.setCaste(resultSet.getString("casteName"));

						student.setStandardName(resultSet.getString("standard"));
						student.setDivisionName(resultSet.getString("division"));
						student.setRollNumber(resultSet.getString("roll_number"));
						student.setRegistrationCode(resultSet.getString("registration_code"));
						student.setBranchId(resultSet.getInt("branch_id"));
						student.setInstituteId(resultSet.getInt("institute_id"));
						student.setActive(resultSet.getString("active"));
						student.setIsDelete(resultSet.getString("isDelete"));
						student.setPhoto(resultSet.getString("photo"));
						student.setDistrict(resultSet.getString("district"));
						student.setState(resultSet.getString("state"));
						student.setTaluka(resultSet.getString("taluka"));
						String feesCodes = resultSet.getString("feesCodes");
						/*update by pitabas on 11_12_2017 for multiple fee code in upload update*/
						if(!StringUtils.isEmpty(feesCodes)){
							String[] feesCodeArr = feesCodes.split(",");
							student.setFeesCodeName(feesCodes);
			
						}

						student.setCreateStudentLogin(resultSet.getString("create_login"));

						List<StudentParent> studentParentList = getStudentParentByStudentId(student.getBranchId(), student.getId());
						student.setStudentParentList(studentParentList);
						student.setStudentUserId(resultSet.getString("username"));
						student.setUserId(resultSet.getInt("user_id"));
						return student;
					}
				});

		return students;
	}

	@Transactional(isolation = Isolation.READ_COMMITTED)
	public boolean uploadUpdateStudent(List<Student> students, String token)
			throws Exception {

		for (Student student : students) {
			updateStudent(student, null, token, true);
		}
		return true;
	}

	@Override
	public boolean deleteStudents(List<Student> studentList) {
		for (Student student : studentList) {
			deleteStudent(student.getId());
		}
		return true;
	}

	public List<String> getParentMobile(int id) {
		String sql = "select p.primaryContact from branch_student_parent bsp "
				+ "join parent p on bsp.parent_id = p.id where student_id = "
				+ id;

		List<String> mobileNumbers = getJdbcTemplate().queryForList(sql,
				String.class);
		return mobileNumbers;
	}
	
	public Integer auditStudentDelete(final Integer userId, final Student student, final String ipAddress) throws JsonProcessingException{
		final String query = "insert into audit_log(entity_type, user_id, operation_type, ip_address, old_data) "
				+ "values(?, ?, ?, ?, ?)";
		Integer id = 0;
		KeyHolder holder = new GeneratedKeyHolder();
		final String studentJson = getJson(student);
		getJdbcTemplate().update(new PreparedStatementCreator() {
		
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement stmt = connection.prepareStatement(
						query, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, "STUDENT");
				stmt.setInt(2, userId);
				stmt.setString(3, "DELETE");
				stmt.setString(4, ipAddress);
				stmt.setString(5, studentJson);//;(5, new Date(System.currentTimeMillis() + 5 * 60 * 1000));
				return stmt;
			}
		}, holder);
		
		id = holder.getKey().intValue();
		return id;
	}
	
	public void auditStudentsDelete(Integer userId, List<Student> students, String ipAddress) throws JsonProcessingException{
		for(Student student : students)
			auditStudentDelete(userId, student, ipAddress);
	}
	
	public String getJson(Object o) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(o);
		return json;
	}

	@Override
	public Integer auditStudentUpdate(final Integer userId, Student oldStudent, final String ipAddress) throws JsonProcessingException {
		Student newStudent = getStudentById(oldStudent.getId());
		updateStudentValues(oldStudent);
		updateStudentValues(newStudent);
		final String oldStudentJson = getJson(oldStudent);
		final String newStudentJson = getJson(newStudent);
		
		
		final String query = "insert into audit_log(entity_type, user_id, operation_type, ip_address, old_data, new_data) "
				+ "values(?, ?, ?, ?, ?, ?)";
		Integer id = 0;
		KeyHolder holder = new GeneratedKeyHolder();
		
		getJdbcTemplate().update(new PreparedStatementCreator() {
		
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement stmt = connection.prepareStatement(
						query, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, "STUDENT");
				stmt.setInt(2, userId);
				stmt.setString(3, "UPDATE");
				stmt.setString(4, ipAddress);
				stmt.setString(5, oldStudentJson);
				stmt.setString(6, newStudentJson);//;(5, new Date(System.currentTimeMillis() + 5 * 60 * 1000));
				return stmt;
			}
		}, holder);
		
		id = holder.getKey().intValue();
		return id;
	}

	@Override
	public void auditStudentsUpdate(Integer userId, List<Student> oldStudents, String ipAddress) throws JsonProcessingException {
		for(Student student : oldStudents)
			auditStudentUpdate(userId, student, ipAddress);
	}
	
	public List<Student> getStudentsByIds(List<Student> students){
		List<Student> studentList = new LinkedList<Student>();
		for(Student student : students){
			Student s = getStudentById(student.getId());
			studentList.add(s);
		}
		return studentList;
	}
	
	public List<AuditLog> getDeletedStudentsData(Integer branchId) throws Exception{
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where entity_type='STUDENT' and operation_type = 'DELETE';  ";
		log.info("student log query -> " + sql);
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		log.info("List of student audit before branch extract size -> " + auditlogs.size());
		auditlogs = getStudentsAuditDataByBranchId(auditlogs, branchId);
		log.info("List of logs by branch id after size -> " + auditlogs.size());
		return auditlogs ;
	}
	
	public List<AuditLog> getStudentsAuditDataByBranchId(List<AuditLog> auditLogList, int branchId) throws JsonParseException, JsonMappingException, IOException{
		Iterator<AuditLog> itr = auditLogList.iterator();
		while(itr.hasNext()){
			AuditLog a = itr.next();
			Student s = null;
			
			if(a.getOldData() != null){
				s = getStudentFromJson(a.getOldData());
				log.info("Student branch id -> " + s.getBranchId() + " " + branchId);
				int studentBranchId = s.getBranchId();
				if(studentBranchId != branchId){
					log.info("Student branch id not equal -> " + studentBranchId + " " + branchId);
					itr.remove();
				}
			}
		}
		return auditLogList;
	}
	
	public List<AuditLog> getUpdatedStudentsData(Integer branchId) throws JsonParseException, JsonMappingException, IOException{
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where entity_type='STUDENT' and operation_type = 'UPDATE';  ";
		
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		
		Iterator<AuditLog> itr = auditlogs.iterator();
		while(itr.hasNext()){
			AuditLog a = itr.next();
			Student oldStudent = getStudentFromJson(a.getOldData());
			Student newStudent = getStudentFromJson(a.getNewData());
			if(compareStudent(oldStudent, newStudent))
				continue;
			else
				itr.remove();
		}
		log.info("List of student audit before branch extract size -> " + auditlogs.size());
		auditlogs = getStudentsAuditDataByBranchId(auditlogs, branchId);
		log.info("List of logs by branch id after size -> " + auditlogs.size());
		return auditlogs;
	}
	
	public List<AuditLog> getAuditLog(String operation, Integer branchId) throws Exception{
		String op = operation.toUpperCase();
		String sql = "select al.*, u.username from audit_log al join user u on u.id = al.user_id "
				+ "where operation_type = '"+ op +"' and entity_type = 'STUDENT'";
				
		List<AuditLog> auditlogs = getJdbcTemplate().query(sql,
				new RowMapper<AuditLog>() {
					@Override
					public AuditLog mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						AuditLog auditLog = new AuditLog();
						auditLog.setId(resultSet.getInt("id"));
						auditLog.setEntityType(resultSet.getString("entity_type"));
						auditLog.setIpAddress(resultSet.getString("ip_address"));
						auditLog.setNewData(resultSet.getString("new_data"));
						auditLog.setOldData(resultSet.getString("old_data"));
						auditLog.setOperationDate(resultSet.getString("operation_date"));
						auditLog.setOperationType(resultSet.getString("operation_type"));
						auditLog.setUserId(resultSet.getString("user_id"));
						auditLog.setUsername(resultSet.getString("username"));
						return auditLog;
					}
				});
		auditlogs = getStudentsAuditDataByBranchId(auditlogs, branchId);
		return auditlogs;
	}
	
	public TreeMap<Integer, Object[]> generateStudentExcelData(List<AuditLog> auditLogs, String operationType){
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		int rowNum = 0;
		String studentName = "";
		String newStudentName = "";
		String studentUserName = "";
		String oldMobile = "";
		String newMobile = "";
		String oldEmail = ""; 
		String newEmail = "";
		String oldStandard = "";
		String newStandard = "";
		String oldDivision = "";
		String newDivision = "";
		String oldFatherName = "";
		String newFatherName = "";
		String oldFatherContact = "";
		String newFatherContact = "";
		String oldFatherEmail = "";
		String newFatherEmail = "";
		String oldMotherName = "";
		String newMotherName = "";
		String oldMotherContact = "";
		String newMotherContact = "";
		String oldMotherEmail = "";
		String newMotherEmail = "";
		String oldGuardianName = "";
		String newGuardianName = "";
		String oldGuardianContact = "";
		String newGuardianContact = "";
		String oldGuardianEmail = "";
		String newGuardianEmail = "";
		
		if(operationType.equals("DELETE")){
			Object[] headerArr = new Object[] { "Performed By", "Operation type", 
					"Operation Date",
					"IP Address",
					"Student Name", 
					"Student Username"};
			excelDataMap.put(rowNum, headerArr);
			
			try {
				for (AuditLog auditLog : auditLogs) {
					if (auditLog != null) {
						if(auditLog.getOldData() != null){
							Student s = getStudentFromJson(auditLog.getOldData());
							studentName = s.getFirstname() + " " + s.getSurname();
							studentUserName = s.getStudentUserId();
						}
						rowNum++;
						Object[] dataArr = { auditLog.getUsername(), 
											 auditLog.getOperationType(), 
											 auditLog.getOperationDate(),
											 auditLog.getIpAddress(),
											 studentName,
											 studentUserName};
						excelDataMap.put(rowNum, dataArr);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(operationType.equals("UPDATE")){
			Object[] headerArr = new Object[] { "Performed By", "Operation type", 
					"Operation Date",
					"IP Address",
					"Student Name", 
					"Student Username",
					"New Student Name",
					"Old Mobile",
					"New Mobile",
					"Old Email",
					"New Email",
					"Old Standard",
					"New Standard",
					"Old Division",
					"New Division",
					"Old Father's Name",
					"New Father's Name",
					"Old Father's Contact",
					"New Father's Contact",
					"Old Father's Email",
					"New Father's Email",
					"Old Mother's Name",
					"New Mother's Name",
					"Old Mother's Contact",
					"New Mother's Contact",
					"Old Mother's Email",
					"New Mother's Email",
					"Old Guardian's Name",
					"New Guardian's Name",
					"Old Guardian's Contact",
					"New Guardian's Contact",
					"Old Guardian's Email",
					"New Guardian's Email"};
			excelDataMap.put(rowNum, headerArr);
			
			try {
				for (AuditLog auditLog : auditLogs) {
					if (auditLog != null) {
						if(auditLog.getOldData() != null && auditLog.getNewData() != null){
							Student oldStudent = getStudentFromJson(auditLog.getOldData());
							Student newStudent = getStudentFromJson(auditLog.getNewData());
							if(!compareStudent(oldStudent, newStudent))
								continue;
							studentName = checkNull(oldStudent.getFirstname()) + " " + checkNull(oldStudent.getMiddleName()) + " " + checkNull(oldStudent.getSurname());
							newStudentName = checkNull(newStudent.getFirstname()) + " " + checkNull(newStudent.getMiddleName()) + " " + checkNull(newStudent.getSurname()); 
							studentUserName = checkNull(oldStudent.getStudentUserId());
							oldMobile = checkNull(oldStudent.getMobile());
							newMobile = checkNull(newStudent.getMobile());
							oldEmail = checkNull(oldStudent.getEmailAddress());
							newEmail = checkNull(newStudent.getEmailAddress());
							oldStandard = checkNull(oldStudent.getStandardName());
							newStandard = checkNull(newStudent.getStandardName());
							
							oldFatherName = checkNull(oldStudent.getFatherFirstname()) + " " + checkNull(oldStudent.getFatherMiddleName()) + " " + checkNull(oldStudent.getFatherSurname());
							newFatherName = checkNull(newStudent.getFatherFirstname()) + " " + checkNull(newStudent.getFatherMiddleName()) + " " + checkNull(newStudent.getFatherSurname());
							oldFatherContact = checkNull(oldStudent.getFatherPrimaryMobileNumber());
							newFatherContact = checkNull(newStudent.getFatherPrimaryMobileNumber());
							oldFatherEmail = checkNull(oldStudent.getFatherEmailAddress());
							newFatherEmail = checkNull(newStudent.getFatherEmailAddress());
							
							oldMotherName = checkNull(oldStudent.getMotherFirstname()) + " " + checkNull(oldStudent.getMotherMiddleName()) + " " + checkNull(oldStudent.getMotherSurname());
							newMotherName = checkNull(newStudent.getMotherFirstname()) + " " + checkNull(newStudent.getMotherMiddleName()) + " " + checkNull(newStudent.getMotherSurname());
							oldMotherContact = checkNull(oldStudent.getMotherPrimaryMobileNumber());
							newMotherContact = checkNull(newStudent.getMotherPrimaryMobileNumber());
							oldMotherEmail = checkNull(oldStudent.getMotherEmailAddress());
							newMotherEmail = checkNull(newStudent.getMotherEmailAddress());
							
							oldGuardianName = checkNull(oldStudent.getParentFirstname()) + " " + checkNull(oldStudent.getParentMiddleName()) + " " + checkNull(oldStudent.getParentSurname());
							newGuardianName = checkNull(newStudent.getParentFirstname()) + " " + checkNull(newStudent.getParentMiddleName()) + " " + checkNull(newStudent.getParentSurname());
							oldGuardianContact = checkNull(oldStudent.getParentPrimaryMobileNumber());
							newGuardianContact = checkNull(newStudent.getParentPrimaryMobileNumber());
							oldGuardianEmail = checkNull(oldStudent.getParentEmailAddress());
							newGuardianEmail = checkNull(newStudent.getParentEmailAddress());
						}
						rowNum++;
						Object[] dataArr = { auditLog.getUsername(), 
											 auditLog.getOperationType(), 
											 auditLog.getOperationDate(),
											 auditLog.getIpAddress(),
											 studentName,
											 studentUserName,
											 newStudentName,
											 oldMobile,
											 newMobile,
											 oldEmail,
											 newEmail,
											 oldStandard,
											 newStandard,
											 oldDivision,
											 newDivision,
											 oldFatherName, 
											 newFatherName, 
											 oldFatherContact,
											 newFatherContact,
											 oldFatherEmail,
											 newFatherEmail,
											 oldMotherName, 
											 newMotherName, 
											 oldMotherContact,
											 newMotherContact,
											 oldMotherEmail,
											 newMotherEmail,
											 oldGuardianName, 
											 newGuardianName,
											 oldGuardianContact,
											 newGuardianContact,
											 oldGuardianEmail,
											 newGuardianEmail};
						excelDataMap.put(rowNum, dataArr);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	
		return excelDataMap;
	}
	
	private Student getStudentFromJson(String s) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		Student student = mapper.readValue(s, Student.class);
		return student;
	}
	
	private boolean compareStudent(Student oldStudent, Student newStudent){
		if((oldStudent.getMobile() != null && newStudent.getMobile() != null && !oldStudent.getMobile().equals(newStudent.getMobile())) || 
				(oldStudent.getMobile() == null && newStudent.getMobile() != null))
			return true;
		if((oldStudent.getEmailAddress() != null && newStudent.getEmailAddress() != null && !oldStudent.getEmailAddress().equals(newStudent.getEmailAddress())) ||
				(oldStudent.getEmailAddress() == null && newStudent.getEmailAddress() != null))
			return true;
		if((oldStudent.getRegistrationCode() != null && newStudent.getRegistrationCode() != null && !oldStudent.getRegistrationCode().equals(newStudent.getRegistrationCode())) ||
				(oldStudent.getRegistrationCode() == null && newStudent.getRegistrationCode() != null))
			return true;
		if((oldStudent.getStandardId() != null && newStudent.getStandardId() != null && !oldStudent.getStandardId().equals(newStudent.getStandardId())) ||
				(oldStudent.getStandardId() == null && newStudent.getStandardId() != null))
			return true;
		if((oldStudent.getDivisionId() != null && newStudent.getDivisionId() != null && !oldStudent.getDivisionId().equals(newStudent.getDivisionId())) ||
				(oldStudent.getDivisionId() == null && newStudent.getDivisionId() != null))
			return true;
		if((oldStudent.getFirstname() != null && newStudent.getFirstname() != null && !oldStudent.getFirstname().equals(newStudent.getFirstname())) ||
				(oldStudent.getFirstname() == null && newStudent.getFirstname() != null))
			return true;
		if((oldStudent.getMiddleName() != null && newStudent.getMiddleName() != null && !oldStudent.getMiddleName().equals(newStudent.getMiddleName())) ||
				(oldStudent.getMiddleName() == null && newStudent.getMiddleName() != null))
			return true;
		if((oldStudent.getSurname() != null && newStudent.getSurname() != null && !oldStudent.getSurname().equals(newStudent.getSurname())) ||
				(oldStudent.getSurname() == null && newStudent.getSurname() != null))
			return true;
		if((oldStudent.getFeesCodeId() != null && newStudent.getFeesCodeId() != null && !oldStudent.getFeesCodeId().equals(newStudent.getFeesCodeId())) ||
				(oldStudent.getFeesCodeId() == null && newStudent.getFeesCodeId() != null))
			return true;
		if((oldStudent.getFatherFirstname() != null && newStudent.getFatherFirstname() != null && !oldStudent.getFatherFirstname().equals(newStudent.getFatherFirstname())) ||
				(oldStudent.getFatherFirstname() == null && newStudent.getFatherFirstname() != null))
			return true;
		if((oldStudent.getFatherMiddleName() != null && newStudent.getFatherMiddleName() != null && !oldStudent.getFatherMiddleName().equals(newStudent.getFatherMiddleName())) ||
				(oldStudent.getFatherMiddleName() == null && newStudent.getFatherMiddleName() != null))
			return true;
		if((oldStudent.getFatherSurname() != null && newStudent.getFatherSurname() != null && !oldStudent.getFatherSurname().equals(newStudent.getFatherSurname())) ||
				(oldStudent.getFatherSurname() == null && newStudent.getFatherSurname() != null))
			return true;
		if((oldStudent.getFatherPrimaryMobileNumber() != null && newStudent.getFatherPrimaryMobileNumber() != null && !oldStudent.getFatherPrimaryMobileNumber().equals(newStudent.getFatherPrimaryMobileNumber())) ||
				(oldStudent.getFatherPrimaryMobileNumber() == null && newStudent.getFatherPrimaryMobileNumber() != null))
			return true;
		if((oldStudent.getFatherSecondaryMobileNumber() != null && newStudent.getFatherSecondaryMobileNumber() != null && !oldStudent.getFatherSecondaryMobileNumber().equals(newStudent.getFatherSecondaryMobileNumber())) ||
				(oldStudent.getFatherSecondaryMobileNumber() == null && newStudent.getFatherSecondaryMobileNumber() != null))
			return true;
		if((oldStudent.getFatherEmailAddress() != null && newStudent.getFatherEmailAddress() != null && !oldStudent.getFatherEmailAddress().equals(newStudent.getFatherEmailAddress())) ||
				(oldStudent.getFatherEmailAddress() == null && newStudent.getFatherEmailAddress() != null))
			return true;
		if((oldStudent.getMotherFirstname() != null && newStudent.getMotherFirstname() != null && !oldStudent.getMotherFirstname().equals(newStudent.getMotherFirstname())) ||
				(oldStudent.getMotherFirstname() == null && newStudent.getMotherFirstname() != null))
			return true;
		if((oldStudent.getMotherMiddleName() != null && newStudent.getMotherMiddleName() != null && !oldStudent.getMotherMiddleName().equals(newStudent.getMotherMiddleName())) ||
				(oldStudent.getMotherMiddleName() == null && newStudent.getMotherMiddleName() != null))
			return true;
		if((oldStudent.getMotherSurname() != null && newStudent.getMotherSurname() != null && !oldStudent.getMotherSurname().equals(newStudent.getMotherSurname())) ||
				(oldStudent.getMotherSurname() == null && newStudent.getMotherSurname() != null))
			return true;
		if((oldStudent.getMotherPrimaryMobileNumber() != null && newStudent.getMotherPrimaryMobileNumber() != null && !oldStudent.getMotherPrimaryMobileNumber().equals(newStudent.getMotherPrimaryMobileNumber())) ||
				(oldStudent.getMotherPrimaryMobileNumber() == null && newStudent.getMotherPrimaryMobileNumber() != null))
			return true;
		if((oldStudent.getMotherSecondaryMobileNumber() != null && newStudent.getMotherSecondaryMobileNumber() != null && !oldStudent.getMotherSecondaryMobileNumber().equals(newStudent.getMotherSecondaryMobileNumber())) ||
				(oldStudent.getFatherSecondaryMobileNumber() == null && newStudent.getFatherSecondaryMobileNumber() != null))
			return true;
		if((oldStudent.getMotherEmailAddress() != null && newStudent.getMotherEmailAddress() != null && !oldStudent.getMotherEmailAddress().equals(newStudent.getMotherEmailAddress())) ||
				(oldStudent.getMotherEmailAddress() == null && newStudent.getMotherEmailAddress() != null))
			return true;
		if((oldStudent.getParentFirstname() != null && newStudent.getParentFirstname() != null && !oldStudent.getParentFirstname().equals(newStudent.getParentFirstname())) ||
				(oldStudent.getParentFirstname() == null && newStudent.getParentFirstname() != null))
			return true;
		if((oldStudent.getParentMiddleName() != null && newStudent.getParentFirstname() != null && !oldStudent.getParentFirstname().equals(newStudent.getParentFirstname())) ||
				(oldStudent.getParentFirstname() == null && newStudent.getParentFirstname() != null))
			return true;
		if((oldStudent.getParentSurname() != null && newStudent.getParentSurname() != null && !oldStudent.getParentSurname().equals(newStudent.getParentSurname())) ||
				(oldStudent.getParentSurname() == null && newStudent.getParentSurname() != null))
			return true;
		if((oldStudent.getParentPrimaryMobileNumber() != null && newStudent.getParentPrimaryMobileNumber() != null && !oldStudent.getParentPrimaryMobileNumber().equals(newStudent.getParentPrimaryMobileNumber())) ||
				(oldStudent.getMotherPrimaryMobileNumber() == null && newStudent.getMotherPrimaryMobileNumber() != null))
			return true;
		if((oldStudent.getParentSecondaryMobileNumber() != null && newStudent.getParentSecondaryMobileNumber() != null && !oldStudent.getParentSecondaryMobileNumber().equals(newStudent.getParentSecondaryMobileNumber())) ||
				(oldStudent.getFatherSecondaryMobileNumber() == null && newStudent.getFatherSecondaryMobileNumber() != null))
			return true;
		if((oldStudent.getParentEmailAddress() != null && newStudent.getParentEmailAddress() != null && !oldStudent.getParentEmailAddress().equals(newStudent.getParentEmailAddress())) ||
				(oldStudent.getParentEmailAddress() == null && newStudent.getParentEmailAddress() != null))
			return true;
		
		return false;
	}
	
	String checkNull(String data){
		if(data == null)
			return "";
		else
			return data;
	}
	
	void updateStudentValues(Student student){
		if(student.getDivisionId() != null && student.getDivisionId() > 0){
			
			String sql = "select displayName from division where id = " + student.getDivisionId();
			try{
				String divisionName = getJdbcTemplate().queryForObject(sql, String.class);
				log.info(sql +  " " + divisionName);
				student.setDivisionName(divisionName);
			}catch(Exception e){
				e.printStackTrace();
				log.info(e.getMessage());
			}
		}

		if(student.getStandardId() != null && student.getStandardId() > 0){
			
			String sql = "select displayName from standard where id = " + student.getStandardId();
			try{
				String standardName = getJdbcTemplate().queryForObject(sql, String.class);
				log.info(sql +  " " + standardName);
				student.setStandardName(standardName);
			}catch(Exception e){
				e.printStackTrace();
				log.info(e.getMessage());
			}
		}
		if(student.getFeesCodeId() != null){
			String feesCode = "";
			for(Integer i : student.getFeesCodeId()){
				String sql = "select name from qfix.fees_code_configuration where id = " + i;
				try{
					String name = getJdbcTemplate().queryForObject(sql, String.class);
					feesCode += name + ", ";
				}catch(Exception e){
					e.printStackTrace();
					log.info(e.getMessage());
					break;
				}
			}
			//feesCode = feesCode.substring(0, feesCode.length() - 2);
			student.setFeesCodeName(feesCode);
		}
		if(student.getStudentParentList() != null){
			List<StudentParent> list = student.getStudentParentList();
			for(StudentParent s : list){
				if(s.getRelationId() == 1){
					student.setFatherFirstname(s.getFirstname());
					student.setFatherMiddleName(s.getMiddlename());
					student.setFatherSurname(s.getLastname());
					student.setFatherPrimaryMobileNumber(s.getPrimaryContact());
					student.setFatherSecondaryMobileNumber(s.getSecondaryContact());
					student.setFatherEmailAddress(s.getEmail());
				}if(s.getRelationId() == 2){
					student.setMotherFirstname(s.getFirstname());
					student.setMotherMiddleName(s.getMiddlename());
					student.setMotherSurname(s.getLastname());
					student.setMotherPrimaryMobileNumber(s.getPrimaryContact());
					student.setMotherSecondaryMobileNumber(s.getSecondaryContact());
					student.setMotherEmailAddress(s.getEmail());
				}if(s.getRelationId() == 3){
					student.setParentFirstname(s.getFirstname());
					student.setParentMiddleName(s.getMiddlename());
					student.setParentSurname(s.getLastname());
					student.setParentPrimaryMobileNumber(s.getPrimaryContact());
					student.setParentSecondaryMobileNumber(s.getSecondaryContact());
					student.setParentEmailAddress(s.getEmail());
				}
			}
		}
			
	}
}