package com.qfix.daoimpl;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.qfix.dao.DivisionInterfaceDao;
import com.qfix.dao.ExpressOnboardDao;
import com.qfix.dao.FeesInterfaceDao;
import com.qfix.dao.HeadInterfaceDao;
import com.qfix.dao.IAcademicYearDao;
import com.qfix.dao.IBranchDao;
import com.qfix.dao.IDisplayTemplateDao;
import com.qfix.dao.IHolidayDao;
import com.qfix.dao.ISubjectDao;
import com.qfix.dao.IWorkingDayDao;
import com.qfix.dao.StandardInterfaceDao;
import com.qfix.model.AcademicYear;
import com.qfix.model.Branch;
import com.qfix.model.Course;
import com.qfix.model.DisplayHead;
import com.qfix.model.Division;
import com.qfix.model.ExpressOnboardSession;
import com.qfix.model.ExpressOnboardStandardSubject;
import com.qfix.model.ExpressOnboardStandardSubjectList;
import com.qfix.model.FeesCode;
import com.qfix.model.Head;
import com.qfix.model.Holiday;
import com.qfix.model.LateFeesDetail;
import com.qfix.model.Standard;
import com.qfix.model.Subject;
import com.qfix.model.WorkingDay;

@Repository
public class ExpressOnboardDaoImpl implements ExpressOnboardDao{
	
	@Autowired
	private IBranchDao branchDao;
	
	@Autowired
	private IAcademicYearDao academicYearDao;
	
	@Autowired
	private IWorkingDayDao workingDayDao;
	
	@Autowired
	private IHolidayDao holidayDao;
	
	@Autowired
	private StandardInterfaceDao standardDao;
	
	@Autowired
	private DivisionInterfaceDao divisionDao;
	
	@Autowired
	private HeadInterfaceDao headDao;
	
	@Autowired
	private IDisplayTemplateDao displayHeadDao;
	
	@Autowired
	private FeesInterfaceDao feesDao;
	
	@Autowired
	private ISubjectDao iSubjectDao;
	

	@Override
	public void insert() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Integer instituteId, Integer branchId) {
		// TODO Auto-generated method stub
		
	}

	public ExpressOnboardSession getExpressOnboradSessionData(Integer branchId) throws SQLException{
		Branch branch = branchDao.getBranchById(branchId);
		List<AcademicYear> academicYear = academicYearDao.getActiveYear(branchId);
		WorkingDay workingDay = workingDayDao.getCurrentWorkingDayForBranch(branchId);
		List<Holiday> holidays = holidayDao.getAll(branchId);
		
		Integer ayId = academicYear.get(0).getId();
		
		List<Standard> standardList = standardDao.getStandardDivision(branchId, ayId);
		
		List<Head> heads = headDao.getAll(branchId);
		for(Head h : heads){
			h.setText(h.getName());
		}
		List<DisplayHead> displayHeads = displayHeadDao.getDisplayHeads(branchId);
		for(DisplayHead h : displayHeads){
			h.setText(h.getName());
		}
		List<FeesCode> feeCodes = feesDao.getAllFeesCode(branchId);
		for(FeesCode h : feeCodes){
			h.setText(h.getName());
		}
		List<LateFeesDetail> lateFees = feesDao.getAllLateFees(branchId);
		for(LateFeesDetail h : lateFees){
			h.setText(h.getDescription());
		}
		ExpressOnboardSession ex = new ExpressOnboardSession();
		if(academicYear.size() > 0)
			ex.setAcademicYear(academicYear.get(0));
		ex.setBranch(branch);
		ex.setHolidays(holidays);
		ex.setDisplayHeads(displayHeads);
		ex.setFeeCodes(feeCodes);
		ex.setHeads(heads);
		ex.setLateFees(lateFees);
		ex.setStandards(standardList);
		ex.setWorkingDay(workingDay);
		ex.setStandardSubjectList(getStandardSubjectData(branch, academicYear.get(0)));
		/* need to get subjects and courses */
		return ex;
	}
	
	public void expressOnboardSessionDataDelete(Integer branchId) throws SQLException{
		List<AcademicYear> academicYear = academicYearDao.getActiveYear(branchId);
		WorkingDay workingDay = workingDayDao.getCurrentWorkingDayForBranch(branchId);
		List<Holiday> holidays = holidayDao.getAll(branchId);
		List<Standard> standardList = standardDao.getAll(branchId);
		List<Division> divisionList = divisionDao.getAll(branchId);
		List<Head> heads = headDao.getAll(branchId);
		List<DisplayHead> displayHeads = displayHeadDao.getDisplayHeads(branchId);
		List<FeesCode> feeCodes = feesDao.getAllFeesCode(branchId);
		List<LateFeesDetail> lateFees = feesDao.getAllLateFees(branchId);
		/* need to check subjects and courses */
		if(workingDay != null && holidays.size() > 0 && standardList.size() > 0 
		&& divisionList.size() > 0 && heads.size() > 0 && displayHeads.size() > 0
		&& feeCodes.size() > 0 && lateFees.size() > 0 && academicYear.size() > 0){
			branchDao.deleteExpressSessionData(branchId);
		}
	}
	
	public ExpressOnboardStandardSubjectList getStandardSubjectData(Branch branch, AcademicYear academicYear) throws SQLException{
		ExpressOnboardStandardSubjectList ex = new ExpressOnboardStandardSubjectList();
		List<ExpressOnboardStandardSubject> ess = new LinkedList<ExpressOnboardStandardSubject>();
		List<Standard> standards = standardDao.getAll(branch.getId());
		
		
		String type = branch.getInstituteType();
		
		if(StringUtils.isEmpty(type)){
			return null;
		}
		
		if(type.equals("SCHOOL")){
			
			for(Standard standard : standards){
				ExpressOnboardStandardSubject e = new ExpressOnboardStandardSubject();
				e.setName(standard.getDisplayName());
				List<Subject> subjects = iSubjectDao.getSubjectsByStandardId(branch.getId(), standard.getId());
				List<String> subjectNameList = new LinkedList<String>();
				for(Subject subject : subjects){
					subjectNameList.add(subject.getName());
				}
				e.setSubjects(subjectNameList);
				ess.add(e);
			}
		}else if(branch.getInstituteType().equals("COLLEGE")){
			for(Standard standard : standards){
				ExpressOnboardStandardSubject e = new ExpressOnboardStandardSubject();
				e.setName(standard.getDisplayName());
				
				List<Course> courseList = standardDao.getCoursesByStandardId(branch.getId(), standard.getId(), academicYear.getId());
				List<String> courseNameList = new LinkedList<String>();
				for(Course c : courseList){
					courseNameList.add(c.getName());
				}
				e.setCourses(courseNameList);
				
				List<Subject> subjectList = iSubjectDao.getSubjectsByStandardId(branch.getId(), standard.getId(), academicYear.getId());
				List<String> subjectsName = new LinkedList<String>();
				for(Subject subject : subjectList){
					subjectsName.add(subject.getName());
				}
				e.setSubjects(subjectsName);
					
			}
		}
		return ex;
	}
	
	
	
}
