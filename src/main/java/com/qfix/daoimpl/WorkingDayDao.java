package com.qfix.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.qfix.dao.IWorkingDayDao;
import com.qfix.exceptions.WorkingDayAllreadyExistException;
import com.qfix.model.WorkingDay;

@Repository
public class WorkingDayDao extends JdbcDaoSupport implements IWorkingDayDao{
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	final static Logger logger = Logger.getLogger(WorkingDayDao.class);
	
	
	@Transactional
	public List<WorkingDay> getAll(Integer branchId){
			String sql = null;
			if(branchId != null && branchId != 0){
				sql = "select wd.*,CONCAT(ay.from_date , ' - ' , ay.to_date) as currentYear from working_days as wd, " +
					  "academic_year as ay where wd.current_academic_year = ay.id AND is_current_active_year = 'Y' AND wd.branch_id = "+branchId;
					  
			}else{
				sql = "select * from working_days";
			}
			logger.debug("get all working day SQL --"+sql);
			
			return getJdbcTemplate().query(sql, new RowMapper<WorkingDay>() {
				public WorkingDay mapRow(ResultSet resultSet, int rownumber)throws SQLException {
					WorkingDay workingDay = new WorkingDay();
					workingDay.setId(resultSet.getInt("id"));
					workingDay.setCurrentAcademicYearId(resultSet.getInt("current_academic_year"));
					workingDay.setCurrentAcademicYearStr(resultSet.getString("currentYear"));
					workingDay.setMonday(resultSet.getString("monday"));
					workingDay.setTuesday(resultSet.getString("tuesday"));
					workingDay.setWednesday(resultSet.getString("wednesday"));
					workingDay.setThursday(resultSet.getString("thursday"));
					workingDay.setFriday(resultSet.getString("friday"));
					workingDay.setSaturday(resultSet.getString("saturday"));
					workingDay.setSunday(resultSet.getString("sunday"));
					workingDay.setBranchId(resultSet.getInt("branch_id"));
					workingDay.setSkipTimetableOnHoliday(resultSet.getString("skip_timetable_on_holiday"));
					return workingDay;
				}});
			
	}
	
	
	public WorkingDay getCurrentWorkingDayForBranch(Integer branchId){
		WorkingDay workingDay = null;
		String sql = "select * from working_days where current_academic_year = " +
				"(select id from academic_year where branch_id = "+branchId+" AND is_current_active_year = 'Y')";

		List<WorkingDay> workingDayList = getJdbcTemplate().query(sql, new RowMapper<WorkingDay>() {
			public WorkingDay mapRow(ResultSet resultSet, int rownumber)throws SQLException {
				WorkingDay workingDay = new WorkingDay();
				workingDay.setId(resultSet.getInt("id"));
				workingDay.setCurrentAcademicYearId(resultSet.getInt("current_academic_year"));
				workingDay.setMonday(resultSet.getString("monday"));
				workingDay.setTuesday(resultSet.getString("tuesday"));
				workingDay.setWednesday(resultSet.getString("wednesday"));
				workingDay.setThursday(resultSet.getString("thursday"));
				workingDay.setFriday(resultSet.getString("friday"));
				workingDay.setSaturday(resultSet.getString("saturday"));
				workingDay.setSunday(resultSet.getString("sunday"));
				workingDay.setBranchId(resultSet.getInt("branch_id"));
				workingDay.setSkipTimetableOnHoliday(resultSet.getString("skip_timetable_on_holiday"));
				return workingDay;
			}});

		if(workingDayList .size() > 0){
			workingDay = workingDayList.get(0);
		}
		return workingDay;
	}
		
	private boolean workingDayAlreadyExist(WorkingDay workingDay) {
		String sql = "select count(*) as count from working_days where " +
				     "current_academic_year = "+workingDay.getCurrentAcademicYearId()+" AND " +
					 "branch_id = "+workingDay.getBranchId()+
				     (workingDay.getId() != null ? " AND id != "+workingDay.getId() : "");
		
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0){
			return true;
		}
		return false;
	}
	
	

	@Override
	public void deleteAllWorkingDays(Integer branchId) {
		String query = "delete from working_days where branch_id = " + branchId;
		getJdbcTemplate().update(query);
	}


	@Transactional
	public boolean updateWorkingDay(WorkingDay workingDay) throws WorkingDayAllreadyExistException{ 
		boolean flag = false;
			if(!workingDayAlreadyExist(workingDay)){
				
				String sql = "update working_days set current_academic_year = ?, monday = ?, " +
						"tuesday = ?,wednesday = ?, thursday = ?, friday= ?, saturday = ?, sunday = ?, " +
						"branch_id = ?, skip_timetable_on_holiday = ?, round_robin_timetable_days = ? where id = ?";
				
				Object[] params = new Object[] {
						workingDay.getCurrentAcademicYearId(),
						workingDay.getMonday(),
						workingDay.getTuesday(),
						workingDay.getWednesday(),
						workingDay.getThursday(),
						workingDay.getFriday(),
						workingDay.getSaturday(),
						workingDay.getSunday(),
						workingDay.getBranchId(),
						workingDay.getSkipTimetableOnHoliday(),
						workingDay.getRoundRobinTimetableDays(),
						workingDay.getId()
				};
					
				int[] types = new int[] { Types.INTEGER,Types.VARCHAR,Types.VARCHAR,
										  Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,
										  Types.VARCHAR,Types.VARCHAR,Types.INTEGER,
										  Types.VARCHAR,Types.INTEGER, Types.INTEGER};
					
				getJdbcTemplate().update(sql, params, types);
				
				flag = true;
				logger.debug("working days updated.."+workingDay.getId());
			}
			else {
				throw new WorkingDayAllreadyExistException("Working days for this duration is already exist in this Branch.");
			}
		return flag;
	}

	@Transactional
	public boolean insertWorkingDay(WorkingDay workingDay) throws WorkingDayAllreadyExistException{
		boolean flag= false;
			if(!workingDayAlreadyExist(workingDay)){
			String sql = "insert into working_days (current_academic_year, monday, tuesday, wednesday, " +
					" thursday, friday, saturday, sunday, branch_id, skip_timetable_on_holiday, round_robin_timetable_days) " +
					" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			Object[] params = new Object[] {
					workingDay.getCurrentAcademicYearId(),
					workingDay.getMonday(),
					workingDay.getTuesday(),
					workingDay.getWednesday(),
					workingDay.getThursday(),
					workingDay.getFriday(),
					workingDay.getSaturday(),
					workingDay.getSunday(),
					workingDay.getBranchId(),
					workingDay.getSkipTimetableOnHoliday(),
					workingDay.getRoundRobinTimetableDays()
			};

			int[] types = new int[] { Types.INTEGER,Types.VARCHAR,Types.VARCHAR,
									  Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,
									  Types.VARCHAR,Types.VARCHAR,Types.INTEGER,
									  Types.VARCHAR,Types.INTEGER};

			getJdbcTemplate().update(sql, params, types);
				flag = true;
			}
			else {
				throw new WorkingDayAllreadyExistException("Working days for this duration is already exist in this Branch.");
			}
		return flag;
	}

	@Transactional
	public WorkingDay getWorkingDayById(Integer id){
			String sql = "select wd.*, i.id as instituteId from working_days as wd, branch as b, institute as i " +
					"where wd.branch_id = b.id AND b.institute_id = i.id AND wd.id = "+id;
			
			List<WorkingDay> workingDayList =  getJdbcTemplate().query(sql, new RowMapper<WorkingDay>() {
				public WorkingDay mapRow(ResultSet resultSet, int rownumber)
						throws SQLException {
					WorkingDay workingDay = new WorkingDay();
					workingDay.setId(resultSet.getInt("id"));
					workingDay.setCurrentAcademicYearId(resultSet.getInt("current_academic_year"));
					workingDay.setMonday(resultSet.getString("monday"));
					workingDay.setTuesday(resultSet.getString("tuesday"));
					workingDay.setWednesday(resultSet.getString("wednesday"));
					workingDay.setThursday(resultSet.getString("thursday"));
					workingDay.setFriday(resultSet.getString("friday"));
					workingDay.setSaturday(resultSet.getString("saturday"));
					workingDay.setSunday(resultSet.getString("sunday"));
					workingDay.setBranchId(resultSet.getInt("branch_id"));
					workingDay.setInstituteId(resultSet.getInt("instituteId"));
					workingDay.setSkipTimetableOnHoliday(resultSet.getString("skip_timetable_on_holiday"));
					return workingDay;
				}
			});
		return workingDayList.get(0);
	}

	@Transactional
	public boolean deleteWorkingDay(Integer id) {
			// delete from Working Day ..
			String sql = "delete from working_days where branch_id = "+id;
			getJdbcTemplate().update(sql);
			return true;
	}
}
