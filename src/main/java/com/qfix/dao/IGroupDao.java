package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.model.Group;

@Repository
public interface IGroupDao {

	public boolean insertGroup(Group group);

	public List<Group> getGroupsByBranchId(Integer branchId);

	public List<Group> getAll(Integer instituteId);

	public boolean updateGroup(Group group);

	public Group getGroupById(Integer id);

	public boolean deleteGroup(Integer id);

	public List<Group> getGroupsByTeacherUserId(Integer teacherUserId);
}
