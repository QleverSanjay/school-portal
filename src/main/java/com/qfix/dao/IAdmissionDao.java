package com.qfix.dao;

import org.springframework.stereotype.Repository;

import com.qfix.model.Admission;

@Repository
public interface IAdmissionDao {

	public boolean save(Admission admission);
}
