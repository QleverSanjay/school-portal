package com.qfix.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.exceptions.StandardAllreadyExistException;
import com.qfix.model.Course;
import com.qfix.model.Division;
import com.qfix.model.Standard;

@Repository
public interface StandardInterfaceDao {

	public List<Standard> getAll(Integer branchId) throws SQLException;

	public List<Standard> listStandards();

	Standard getStandardById(Integer id);

	boolean insertStandard(Standard standard)
			throws StandardAllreadyExistException;

	boolean updateStandard(Standard standard)
			throws StandardAllreadyExistException;

	boolean deleteStandard(Integer standardId);

	void deleteStandards(Integer branchId, String[] names);

	void deleteBranchStandards(Integer branchId);

	Standard getStandardByName(String name, Integer branchId,
			Integer academicYearId);

	public Standard insertStandards(Standard standard,List<Division> divisions);
	

	List<Course> getCoursesByStandardId(Integer branchId, Integer standardId, Integer academicYearId);

	List<Standard> getStandardDivision(Integer branchId,Integer ayId);


}
