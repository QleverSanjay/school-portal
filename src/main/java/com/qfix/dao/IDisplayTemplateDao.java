package com.qfix.dao;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.exceptions.ApplicationException;
import com.qfix.model.DisplayHead;
import com.qfix.model.DisplayTemplate;

@Repository
public interface IDisplayTemplateDao {

	void insertDisplayHead(DisplayHead head) throws ApplicationException;
	
	void updateDisplayHead(DisplayHead head) throws ApplicationException;

	List<DisplayHead> getDisplayHeads(Integer branchId);

	DisplayHead getDisplayHead(Integer displayHeadId);

	void deleteDisplayHead(Integer displayHeadId);

	void updateDisplayTemplate(DisplayTemplate template) throws ApplicationException;

	void insertDisplayTemplate(DisplayTemplate template) throws ApplicationException;

	List<DisplayTemplate> getDisplayTemplates(Integer branchId);

	DisplayTemplate getDisplayTemplate(Integer displayTemplateId);

	void deleteDisplayTemplate(Integer displayTemplateId) throws ApplicationException ;

	List<DisplayHead> searchDisplayHeads(String searchText, String excludeIds,
			Integer branchId);

	List<DisplayTemplate> searchDisplayTemplates(String searchText,
			String excludeIds, Integer branchId);

	 ByteArrayOutputStream downloadDisplayTemplate(Integer branchId) throws Exception;

	DisplayTemplate parseExcelFile(MultipartFile file, Integer branchId)
			throws Exception;
}
