package com.qfix.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.model.Vendor;

@Repository
public interface VendorInterfaceDao {
	/*public boolean insertVendor(Vendor vendor) 
			throws BranchAllreadyExistException, IOException;
	
	public boolean updateVendor(Vendor vendor) 
			throws BranchAllreadyExistException, IOException;*/

	public List<Vendor> getAll() throws SQLException;
}
