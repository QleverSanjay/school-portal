package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.model.SchoolBoard;

@Repository
public interface SchoolDao {
	List<SchoolBoard> listBoards();
}
