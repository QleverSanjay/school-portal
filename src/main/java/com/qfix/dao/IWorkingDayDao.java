package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.exceptions.WorkingDayAllreadyExistException;
import com.qfix.model.WorkingDay;

@Repository
public interface IWorkingDayDao {

	List<WorkingDay> getAll(Integer branchId);

	void deleteAllWorkingDays(Integer branchI);
	
	boolean updateWorkingDay(WorkingDay workingDay) throws WorkingDayAllreadyExistException;
	
	boolean insertWorkingDay(WorkingDay workingDay) throws WorkingDayAllreadyExistException;
	
	WorkingDay getWorkingDayById(Integer id);
	
	boolean deleteWorkingDay(Integer id);

	WorkingDay getCurrentWorkingDayForBranch(Integer branchId);
	
}
