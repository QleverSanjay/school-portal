package com.qfix.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.exceptions.HeadAllreadyExistException;
import com.qfix.model.Head;

@Repository
public interface HeadInterfaceDao {
	/*public boolean insertHead(Head head) 
			throws HeadAllreadyExistException, IOException;
	
	public boolean updateHead(Head head) 
			throws HeadAllreadyExistException, IOException;*/

	List<Head> getAll(Integer instituteId) throws SQLException;
	
	Head getHeadById(Integer id);
	
	boolean insertHead(Head head) throws HeadAllreadyExistException;

	boolean updateHead(Head head) throws HeadAllreadyExistException;
	
	boolean deleteHead(Integer headId);
	
	List<Head> getHeadByBranchForDashboard(Integer branchId);
	
	String getHeadNameByFeesId(Integer feesId);

}
