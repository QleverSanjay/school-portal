package com.qfix.dao;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.qfix.model.ExpressOnboardSession;

@Repository
public interface ExpressOnboardDao {

	public void insert();

	public void update(Integer instituteId, Integer branchId );
	
	public ExpressOnboardSession getExpressOnboradSessionData(Integer branchId)  throws SQLException;
}
