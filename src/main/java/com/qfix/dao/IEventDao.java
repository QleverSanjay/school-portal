package com.qfix.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.exceptions.ProfileImageUploadException;
import com.qfix.model.Event;

@Repository
public interface IEventDao {
	public boolean insertEvent(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException;

	public boolean uploadEvent(List<Event> eventList);

	public boolean updateEvent(Event event, MultipartFile file)
			throws ProfileImageUploadException, IOException;

	public List<Event> getEventsByBranchId(Integer branchId);

	public List<Event> getAll(Integer instituteId) throws SQLException;

	public Event getEventById(Integer id);

	public boolean deleteEvent(Integer id);

	public List<Event> getTeacherEvents(Integer teacherId, Integer branchId);

	public List<Event> getSentEventByTeacher(Integer branchId, boolean b, Integer userId);

	public Event getEventByIdToView(Integer id, Integer teacherUserId);
	
	public String saveFile(MultipartFile file, Event event, boolean isUpdate) throws IOException;
}
