package com.qfix.dao;

import org.springframework.stereotype.Repository;

import com.qfix.model.EmailSetting;
import com.qfix.model.SMSSetting;

@Repository
public interface ISettingsDao {
	
	void sendEmail(String emailOfReciever, String subject, String messageBody);
	
	boolean updateSmsSetting(SMSSetting smsSetting);
	
	boolean updateEmailSetting(EmailSetting emailSetting);
	
	EmailSetting getEmailSetting();
	
	SMSSetting getSmsSetting();
}
