package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.model.SubjectCategory;

@Repository
public interface ISubjectCategoryDao {

	boolean uploadSubjectCategory(List<SubjectCategory> subjectList, Integer branchId);

	List<SubjectCategory> getSubjectCategorysByBranchId(Integer branchId);

	boolean updateSubjectCategory(SubjectCategory subject);

	boolean insertSubjectCategory(SubjectCategory subject);

	SubjectCategory getSubjectCategoryById(Integer id);

	boolean deleteSubjectCategory(Integer id);
}
