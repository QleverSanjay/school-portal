package com.qfix.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.qfix.model.Course;
import com.qfix.model.Subject;

@Repository
public interface ISubjectDao {

	boolean uploadSubject(List<Subject> subjectList, Integer branchId);

	List<Subject> getSubjectsByBranchId(Integer branchId);

	boolean updateSubject(Subject subject);
	List<Subject> listSubjects(String type);
	boolean insertSubject(Subject subject);

	Subject getSubjectById(Integer id);

	boolean deleteSubject(Integer id);
	
	List<Subject> getSubjectsByStandardId(Integer branchId, Integer standardId);

	Map<String, Integer> insertSubjects(Integer branchId,List<Subject> subjects);
	
	void removeSubjects(String name, Integer branchId, List<String> subjectsList, Integer academicYearId);
	
	void insertSubjects(String name, Integer branchId, List<String> list, Integer academicYearId);
	
	Subject getSubjectByName(String name, Integer branchId);
	
	void insertYearCourseSubject(String standardName, Integer branchId, Integer academicYearId, List<String> courseList, List<String> subjectList);
	
	void removeYearCourseSubject(String standardName, Integer branchId, Integer academicYearId, List<String> courseList, List<String> subjectList);
	
	Course getCourseByName(String courseName);
	
	boolean insertSubjectExpress(final Subject subject);
	
	List<Subject> getSubjectsByStandardId(Integer branchId, Integer standardId, Integer academicYearId);
}
