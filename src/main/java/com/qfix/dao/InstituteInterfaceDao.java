package com.qfix.dao;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.exceptions.InstituteAllreadyExistException;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Institute;

@Repository
public interface InstituteInterfaceDao {
	
	List<Institute> getAll();
	
	boolean uploadInstitute(List<Institute> instituteList) 
			throws UserAllreadyExistException, InstituteAllreadyExistException, IOException, Exception ;
	
	List<Institute> getAllreadyExistInstituteAdminList();
	
	List<Institute> getAllreadyExistInstituteList();
	
	boolean updateInstitute(Institute institute, MultipartFile file) throws UserAllreadyExistException, InstituteAllreadyExistException, IOException;
	
	Institute getInstituteById(Integer instituteId);
	
	boolean insertInstitute(Institute institute, MultipartFile file) 
			throws Exception;
	
	boolean deleteInstitute(Integer instituteId);

	Integer getInstituteIdByName(String instituteName);
	
	Integer saveInstituteExpressSession(final Integer id, final Integer userId);
	
	List<Institute> getSessionInstitutes(Integer userId);
}
