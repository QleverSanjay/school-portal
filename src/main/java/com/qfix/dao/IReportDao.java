package com.qfix.dao;

import java.util.List;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.model.ReportModel;
import com.qfix.model.StudentReport;
import com.qfix.model.ValidationError;

@Repository
public interface IReportDao{

	Workbook generateReportTemplate(ReportModel reportModel);

	List<StudentReport> getReports(ReportModel reportModel);


	void uploadStudntReport(MultipartFile file, ReportModel reportModel, String title) throws Exception;

	List<ValidationError> getValidationErrors();

	boolean deleteReport(List<Integer> ids);

	
}