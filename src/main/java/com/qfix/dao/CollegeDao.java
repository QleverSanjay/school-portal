package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.model.CollegeYear;
import com.qfix.model.Course;
import com.qfix.model.University;

@Repository
public interface CollegeDao {
	List<University> listUniversitites();
	List<CollegeYear> listYears();
	List<Course> listCourses();
}
