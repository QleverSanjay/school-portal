package com.qfix.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.exceptions.BranchForumNotExistException;
import com.qfix.exceptions.ProfileImageUploadException;
import com.qfix.exceptions.ShoppingAccountNotCreatedException;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Student;
import com.qfix.model.StudentExcelReportFilter;
import com.qfix.model.Teacher;
import com.qfix.model.TeacherStandardSubject;

@Repository
@SuppressWarnings("unused")
public interface TeacherInterfaceDao {
	/*public boolean insertDivision(Teacher teacher, MultipartFile file) 
			throws DivisionAllreadyExistException, IOException;*/

	public List<Teacher> getAll(Integer branchId)throws SQLException;

	public List<Teacher> getTeacherByBranchId(Integer branchId);

	public boolean updateTeacher(Teacher teacher, MultipartFile file) throws ProfileImageUploadException, IOException;

	public boolean insertTeacher(Teacher teacher, MultipartFile file) throws BranchForumNotExistException, ShoppingAccountNotCreatedException, Exception;

	public boolean updateProfile(Teacher teacher, MultipartFile file) throws IOException, Exception;

	public Teacher getTeacherById(Integer id);

	public boolean deleteTeacher(Integer id);

	public boolean uploadTeacher(List<Teacher> teacherList, Integer branchId) throws Exception;
	
	public boolean uploadTeacherStandardSubject(List<TeacherStandardSubject> teacherStandardSubjectList, Integer branchId) throws Exception;

	public List<Teacher> getUploadingFailedRecords();	
	
	public List<Teacher> getTeacherReport(StudentExcelReportFilter filter);

	public Integer getIdByUserId(Integer userId);

	public boolean updateProfileFirstTime(Teacher teacher);

	public List<Teacher> exportTeacherData(Integer branchId,
			Integer standardId, Integer divisionId);
	
	public String saveFile(MultipartFile file, Teacher teacher, boolean isUpdate) throws IOException;
}
