package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.model.Forum;

@Repository
public interface IForumDao {

	public List<Forum> getAll(Integer branchId);
	
}
