package com.qfix.dao;

import org.springframework.stereotype.Repository;

import com.qfix.model.ChatProfile;
import com.qfix.service.user.client.chat.response.group.UpdateGroupResponse;
import com.qfix.service.user.client.chat.response.session.UserResponse;

@Repository
public interface IChatDao {

	public UserResponse createChatAccount(String email, String firstname, String surname, String tags);
	
	public String createChatPublicGroups(String ownerUserName,String ownerPassword,String groupName, String photo);
	
	public UpdateGroupResponse updateChatPublicGroups(String groupId, String occupantsIdsAdd, String occupantsIdsRemove);
	
	public ChatProfile getChatSessionDetails(String chatAccountUsername, String chatAccountPassword, long chatAccountProfileId);	
	
}
