package com.qfix.dao;

import java.io.IOException;
import java.util.List;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.qfix.model.AuditLog;
import com.qfix.model.Student;
import com.qfix.model.StudentExcelReportFilter;
import com.qfix.model.StudentParent;
import com.qfix.model.TableResponse;

@Repository
public interface IStudentDao {

	public boolean uploadStudent(List<Student> students, String token)
			throws Exception;

	public List<Student> getStudents(Integer branchId);

	public List<Student> getStudentsByStandardDivision(Integer branchId,
			Integer standardId, Integer divisionId);

	public Student getStudentById(Integer id);

	public boolean deleteStudent(Integer id);

	public boolean insertStudent(Student student, MultipartFile file,
			String token) throws Exception;

	public boolean updateStudent(Student student, MultipartFile file,
			String token, boolean isUploadUpdate) throws Exception;

	public TableResponse getStudentReport(StudentExcelReportFilter filter);

	void sendNotification(Integer branchId, Integer standardId,
			Integer divisionId);

	public List<StudentParent> exportParentData(Integer branchId, Integer standardId,
			Integer divisionId);

	public List<Student> exportStudentData(Integer branchId, Integer standardId,
			Integer divisionId);

	public List<Student> getAllStudentsWithParents(Integer branchId);
/*	void updateFeesSchedule(int standardId, int divisionId, int casteId,
			int feesCodeId, Integer userId, Integer branchId, Integer studentId);*/
	
	public boolean uploadUpdateStudent(List<Student> students, String token)
			throws Exception;

	public boolean deleteStudents(List<Student> list);
	
	Integer auditStudentDelete(final Integer userId, final Student student, final String ipAddress) throws JsonProcessingException;
	
	void auditStudentsDelete(Integer userId, List<Student> students, String ipAddress) throws JsonProcessingException;
	
	Integer auditStudentUpdate(final Integer userId, final Student newStudent, final String ipAddress) throws JsonProcessingException;
	
	void auditStudentsUpdate(Integer userId, List<Student> newStudents, String ipAddress) throws JsonProcessingException;
	
	List<Student> getStudentsByIds(List<Student> students);
	
	List<AuditLog> getDeletedStudentsData(Integer branchId) throws Exception;
	
	List<AuditLog> getUpdatedStudentsData(Integer branchId) throws JsonParseException, JsonMappingException, IOException;
	
	List<AuditLog> getAuditLog(String operation, Integer branchId) throws Exception;
	
	TreeMap<Integer, Object[]> generateStudentExcelData(List<AuditLog> auditLogs, String operationType);
}
