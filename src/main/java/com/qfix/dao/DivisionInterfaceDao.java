package com.qfix.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.qfix.exceptions.DivisionAllreadyExistException;
import com.qfix.model.Division;

@Repository
public interface DivisionInterfaceDao {
	boolean insertDivision(Division division) 
			throws DivisionAllreadyExistException;

	List<Division> getAll(Integer branchId);
	
	List<Division> getAllByStandardTimetable(Integer standardId);	
	
	void deleteBranchDivisions(Integer branchId);
	
	boolean deleteDivision(Integer divisionId);
	
	Division getDivisionById(Integer id);
	
	boolean updateDivision(Division division) throws DivisionAllreadyExistException;

	List<Division> getUnusedDivisionsByStandardTimetable(Integer timetableId);

	Map<String, Integer> insertDivisions(Integer branchId,Integer academicYearId,List<Division> divisions);
}
