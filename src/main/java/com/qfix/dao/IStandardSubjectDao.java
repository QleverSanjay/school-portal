package com.qfix.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.qfix.model.Standard;
import com.qfix.model.StandardSubject;
import com.qfix.model.Subject;

@Repository
public interface IStandardSubjectDao {

	boolean uploadStandardSubject(List<StandardSubject> subjectList, Integer branchId);

	List<StandardSubject> getStandardSubjectsByBranchId(Integer branchId);

	boolean updateStandardSubject(StandardSubject subject);

	boolean insertStandardSubject(StandardSubject subject);

	StandardSubject getStandardSubjectByStandardId(Integer id);

	boolean deleteStandardSubject(Integer id);
	
	boolean deleteStandardSubjects(Integer id);

	void insertStandardSubjects(Standard standard, List<Subject> subjects);
	
	void insertStandardSubject(Integer standardId, Integer subjectId);
	
	boolean deleteStandardSubject(Integer standardId, Integer subjectId);
	
	void insertYearCourseSubject(Integer yearId, Integer branchId, Integer subjectId, Integer courseId, Integer academicYearId);
	
	void deleteYearCourseSubject(Integer yearId, Integer branchId, Integer subjectId, Integer courseId, Integer academicYearId);
}
