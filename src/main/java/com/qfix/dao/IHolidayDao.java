package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.exceptions.HolidayAllreadyExistException;
import com.qfix.model.Holiday;
import com.qfix.model.PublicHoliday;

@Repository
public interface IHolidayDao {

	List<Holiday> getAll(Integer branchId);
	
	boolean updateHoliday(Holiday holiday) throws HolidayAllreadyExistException;
	
	boolean insertHoliday(Holiday holiday) throws HolidayAllreadyExistException;
	
//	void insertHoliday(Holiday holiday, List<Integer> holidays) throws HolidayAllreadyExistException;

	Holiday getHolidayById(Integer id);

	boolean deleteHoliday(Integer id);

	void deleteHolidays(Integer branchId);
	
	boolean uploadHoliday(List<Holiday> holidayList);
	
	List<PublicHoliday> getPublicHoliday();
}
