package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.model.Branch;
import com.qfix.model.DisplayHead;
import com.qfix.model.ExpressOnboardSession;
import com.qfix.model.RefundConfig;

@Repository
public interface IBranchDao {

	public List<Branch> getAll(Integer instituteId);
	
	public Branch getBranchById(Integer id);
	
	public boolean insertBranch(final Branch branch, MultipartFile file) throws Exception;
	
	public boolean uploadBranch(List<Branch> branchList, Integer instituteId) throws Exception;
	
	public boolean updateBranch(final Branch branch, MultipartFile file) throws Exception;
	
	public boolean deleteBranch(Integer branchId);

	public void updateFeesDisplayLayout(DisplayHead layout);

	void insertFeesDisplayLayout(DisplayHead layout);

	List<DisplayHead> getDisplayLyouts(Integer branchId);

	public boolean deleteDisplayLayout(Integer displayLayoutId);

	public DisplayHead getDisplayLyout(Integer displayLayoutId);

	Integer getBranchIdByName(String name, Integer instituteId);

	public RefundConfig getRefundConfigurationByBranchId(Integer branchId);
	
	public void saveRefundConfiguration(RefundConfig config);
	
	List<Branch> getExpressSessionBranches(Integer branchId, Integer instituteId);
	
	void saveExpressBranchSession(Integer branchId, Integer instituteId, Integer userId);

	public void deleteExpressSessionData(Integer branchId);
	
	Branch getBranchDataById(Integer id);
}
