package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.model.Individual;

@Repository
public interface IIndividualDao {

	public List<Individual> getIndividualsByName(String name, Integer branchId, List<Integer> allreadySelectedIds);
	
	public List<Individual> getStudentsForNoticeSearch(String name,Integer branchId, List<Integer> allreadySelectedIds);
	
	public List<Individual> getStudentsForEdiarySearch(String name,Integer branchId, List<Integer> allreadySelectedIds);
	
	public Individual getIndividualFromStudent(Integer id);
	
	public Individual getIndividualFromTeacher(Integer id);
	
	public Individual getIndividualFromVendor(Integer id);
	
	public List<Individual> getIndividualsGroupByName(String name,Integer instituteId, List<Integer> allreadySelectedIds);

	public List<Individual> getIndividualsByNameExceptGroup(String name,
			Integer groupId, Integer branchId, List<Integer> allreadySelectedIds);

	/*public List<Individual> getIndividualsByNameExceptGroupWhenTeacher(
			String name, Integer groupId, Integer teacherUserId,
			List<Integer> allreadySelectedIds);*/
}
