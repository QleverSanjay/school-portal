package com.qfix.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.model.Timetable;
import com.qfix.model.ValidationError;

@Repository
public interface TimetableInterfaceDao {

	public List<Timetable> getAll(Integer branchId, Integer standardId, Integer divisionId) throws SQLException;
	
	public Timetable getTimetableById(Integer id);

	public boolean deleteTimeTable(Integer id, String token) throws Exception;

	public boolean insertTimetable(Timetable timetable, String token) throws Exception;

	public boolean updateTimetable(Timetable timetable, String token) throws Exception;
	
	public void uploadTimetable(MultipartFile file, Timetable timetable,
			String token)  throws  Exception;
	
	public List<ValidationError> getValidationErrors();
}
