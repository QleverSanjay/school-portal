package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qfix.exceptions.YearAllreadyExistException;
import com.qfix.model.AcademicYear;

@Repository
public interface IAcademicYearDao {

	List<AcademicYear> getAll(Integer branchId);
	
	boolean updateYearStatus(AcademicYear academicYear);
	
	boolean updateAcademicYear(AcademicYear academicYear) throws YearAllreadyExistException;
	
	boolean insertAcademicYear(AcademicYear academicYear)throws YearAllreadyExistException;
	
	AcademicYear getYearById(Integer id);
	
	boolean deleteAcademicYear(Integer id) throws Exception;
	
	List<AcademicYear> getActiveYear(Integer branchId);

	Integer getAcademicYearidByDates(String fromDate, String toDate, Integer branchId);
}
