package com.qfix.dao;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.qfix.model.BranchConfig;
import com.qfix.model.Receipt;

@Repository
public interface IBranchConfigurationDao {
	Receipt getReceiptConfiguration(Integer branchId);
	String saveReceiptConfiguration(Receipt receipt) throws Exception;
	void saveStudentBranchConfig(BranchConfig branchConfig) throws SQLException;

}
