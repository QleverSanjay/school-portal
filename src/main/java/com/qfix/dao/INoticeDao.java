package com.qfix.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.qfix.model.Notice;

@Repository
public interface INoticeDao {

	public List<Notice> getAll(Integer branchId, boolean b, Integer userId);

	public List<Notice> getTeacherNotice(Integer teacherId,Integer branchId);
	
	public boolean insertNotice(Notice notice, MultipartFile file);
	
	public boolean deleteNotice(Integer id);
	
	public Notice getNoticeById(Integer id, Integer teacherId);
	
	public List<Notice> getAllAssignment(Integer branchId, boolean isTeacher, Integer teacherUserId);
}
