package com.qfix.dao;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.qfix.exceptions.ApplicationException;
import com.qfix.exceptions.FeeStructureAlreadyExistException;
import com.qfix.model.AuditLog;
import com.qfix.model.FeePayment;
import com.qfix.model.Fees;
import com.qfix.model.FeesCode;
import com.qfix.model.FeesDescription;
import com.qfix.model.FeesReport;
import com.qfix.model.FeesReportFilter;
import com.qfix.model.IncorrectTransactionIdReport;
import com.qfix.model.LateFeesDetail;
import com.qfix.model.LateFeesPayment;
import com.qfix.model.SchemeCode;
import com.qfix.model.TableResponse;
import com.qfix.model.TransactionReport;

@Repository
public interface FeesInterfaceDao {

	List<Fees> getAll(Integer instituteId);

	boolean updateFees(Fees fees, Integer currentLoggedInUserId);
	
	boolean insertFees(Fees fees, Integer currentLoggedInUserId);
	
	Fees getFeeById(Integer id);
	
	boolean deleteFee(Integer id);

	List<Fees> getFeesByBranchId(Integer branchId);

	List<FeesReport> getFeesReport(FeesReportFilter filter);
	
	List<FeesReport> getFeeSummaryReport(FeesReportFilter filter);

	TableResponse searchFeeReport(FeesReportFilter filter, boolean getDisplayHeads);

	void markFeePaid(FeePayment feePayment) throws Exception;

	void setLateFeesCharge(LateFeesPayment latePayment) throws Exception;

	List<FeesDescription> getFeesPaymentDescription(Long feeScheuleId, Integer roleId, Integer userId);

	void deletePaidFees(List<Long> paymentIdList);

	void updatePaidFees(List<FeesDescription> feesDescriptions);

	List<SchemeCode> getSchemeCodes(Integer branchId, String token);

	void updateLateFeesDetail(LateFeesDetail lateFeesDetail);

	void insertLateFeesDetail(LateFeesDetail lateFeesDetail) throws SQLException;

	void deleteLateFeesDetail(Integer lateFeesId);

	List<LateFeesDetail> getAllLateFees(Integer branchId);

	LateFeesDetail getLateFees(Integer lateFeesId);

	Integer getFeesSummaryReportTotalCount(FeesReportFilter filter);

	TableResponse getOfflinePayments(FeesReportFilter filter) throws ApplicationException;

	List<Fees> downloadFeesToUploadUpdate(Integer branchId, String token);
	int addFeeCode(Fees feeCodeName);


	void updateFeesCode(FeesCode feesCode) throws Exception;


	void insertFeesCode(FeesCode feesCode) throws Exception;
	
	FeesCode getFeesCodeById(Integer id);


	List<FeesCode> getAllFeesCode(Integer branchId);


	void deleteFeesCode(Integer id);


	boolean uploadFees(List<Fees> feesList, Integer currentLoggedInUserId) throws FeeStructureAlreadyExistException;

	boolean uploadUpdateFees(Map<String, List<Fees>> feesMap, Integer currentLoggedInUserId)
			throws FeeStructureAlreadyExistException;
	
	Integer auditFeesDelete(final Integer userId, final Fees fee, final String ipAddress) throws JsonProcessingException;
	
	void auditFeesDelete(Integer userId, List<Fees> fees, String ipAddress) throws JsonProcessingException;

	Integer auditFeesUpdate(Integer userId, Fees oldFees, String ipAddress)
			throws JsonProcessingException;
	
	List<Fees> getFeesByIds(List<Fees> fees);
	
	void auditFeesUpdate(final Integer userId, List<Fees> oldFeesData, final String ipAddress) throws JsonProcessingException;
	
	List<AuditLog> getDeletedFeesData(Integer branchId) throws JsonParseException, JsonMappingException, IOException;
	
	List<AuditLog> getUpdatedFeesData(Integer branchId) throws JsonParseException, JsonMappingException, IOException;
	
	List<AuditLog> getAuditLog(String operation, Integer branchId) throws Exception;
	
	public TreeMap<Integer, Object[]> generateFeesExcelData(List<AuditLog> auditLogs, String operationType);

	public TreeMap<Integer, Object[]> generateFailedTransactionExcelData(List<TransactionReport> transactionReports);
	public List<TransactionReport> getFailedTransaction(Integer branchId);
	
	List<IncorrectTransactionIdReport> getIncorrectTransactionIdReport();
	TreeMap<Integer, Object[]> generateIncorrectTransactionIdReport(List<IncorrectTransactionIdReport> list);
	
	void writeReport(PrintWriter writer, List<IncorrectTransactionIdReport> list);
	
	List<FeesReport> getSettlementDateNullReport(Integer branchId);
	
	TreeMap<Integer, Object[]> generateSettlementDateNullReport(List<FeesReport> list);
}
