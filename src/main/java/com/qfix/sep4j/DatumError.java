package com.qfix.sep4j;

import java.io.Serializable;
import java.text.MessageFormat;

public class DatumError
  implements Serializable
{
  private static final long serialVersionUID = 5587180904535809629L;
  private int recordIndex;
  private String propName;
  private Exception cause;
  
  public int getRecordIndex()
  {
    return this.recordIndex;
  }
  
  public void setRecordIndex(int recordIndex)
  {
    this.recordIndex = recordIndex;
  }
  
  public String getPropName()
  {
    return this.propName;
  }
  
  public void setPropName(String propName)
  {
    this.propName = propName;
  }
  
  public Exception getCause()
  {
    return this.cause;
  }
  
  public void setCause(Exception cause)
  {
    this.cause = cause;
  }
  
  public String toString()
  {
    return MessageFormat.format("recordIndex = {0}, propName = \"{1}\", cause = {2}", new Object[] { Integer.valueOf(this.recordIndex), this.propName, this.cause });
  }
}
