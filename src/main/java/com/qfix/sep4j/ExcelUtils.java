package com.qfix.sep4j;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.qfix.sep4j.support.SepBasicTypeConverts;
import com.qfix.sep4j.support.SepReflectionHelper;

public class ExcelUtils
{
  public static <T> void save(LinkedHashMap<String, String> headerMap, Collection<T> records, OutputStream outputStream)
  {
    save(headerMap, records, outputStream, null, null, true);
  }
  
  public static <T> void save(LinkedHashMap<String, String> headerMap, Collection<T> records, OutputStream outputStream, String datumErrPlaceholder)
  {
    save(headerMap, records, outputStream, datumErrPlaceholder, null, true);
  }
  
  public static <T> void save(LinkedHashMap<String, String> headerMap, Collection<T> records, OutputStream outputStream, String datumErrPlaceholder, List<DatumError> datumErrors)
  {
    save(headerMap, records, outputStream, datumErrPlaceholder, datumErrors, true);
  }
  
  public static <T> void saveIfNoDatumError(LinkedHashMap<String, String> headerMap, Collection<T> records, OutputStream outputStream, String datumErrPlaceholder, List<DatumError> datumErrors)
  {
    save(headerMap, records, outputStream, datumErrPlaceholder, datumErrors, false);
  }
  
  public static <T> List<T> parseIgnoringErrors(Map<String, String> reverseHeaderMap, InputStream inputStream, Class<T> recordClass)
  {
    try
    {
      return parse(reverseHeaderMap, inputStream, null, recordClass);
    }
    catch (InvalidFormatException e1)
    {
      return new ArrayList();
    }
    catch (InvalidHeaderRowException e1) {}
    return new ArrayList();
  }
  
  public static <T> List<T> parse(Map<String, String> reverseHeaderMap, InputStream inputStream, List<CellError> cellErrors, Class<T> recordClass)
    throws InvalidHeaderRowException, InvalidFormatException
  {
    validateReverseHeaderMap(reverseHeaderMap);
    
    validateRecordClass(recordClass);
    
    Workbook workbook = toWorkbook(inputStream);
    if (workbook.getNumberOfSheets() <= 0) {
      return new ArrayList();
    }
    Sheet sheet = workbook.getSheetAt(0);
    
    Map<Short, ColumnMeta> columnMetaMap = parseHeader(reverseHeaderMap, sheet.getRow(0));
    if (columnMetaMap.isEmpty()) {
      throw new InvalidHeaderRowException();
    }
    List<T> records = new ArrayList();
    for (int rowIndex = 1; rowIndex <= 3000; rowIndex++)
    {
      Row row = sheet.getRow(rowIndex);
      if (row == null) {
        break;
      }
      T record = parseDataRow(columnMetaMap, row, rowIndex, recordClass, cellErrors);
      records.add(record);
    }
    return records;
  }
  
  static <T> void save(LinkedHashMap<String, String> headerMap, Collection<T> records, OutputStream outputStream, String datumErrPlaceholder, List<DatumError> datumErrors, boolean stillSaveIfDataError)
  {
    validateHeaderMap(headerMap);
    if (records == null) {
      records = new ArrayList();
    }
    if (outputStream == null) {
      throw new IllegalArgumentException("the outputStream can not be null");
    }
    Workbook wb = new XSSFWorkbook();
    Sheet sheet = wb.createSheet();
    
    createHeaders(headerMap, sheet);
    
    int recordIndex = 0;
    for (T record : records)
    {
      int rowIndex = recordIndex + 1;
      createRow(headerMap, record, recordIndex, sheet, rowIndex, datumErrPlaceholder, datumErrors);
      recordIndex++;
    }
    if (shouldSave(datumErrors, stillSaveIfDataError)) {
      writeWorkbook(wb, outputStream);
    }
  }
  
  static boolean shouldSave(List<DatumError> datumErrors, boolean stillSaveIfDataError)
  {
    if (stillSaveIfDataError) {
      return true;
    }
    if ((datumErrors == null) || (datumErrors.isEmpty())) {
      return true;
    }
    return false;
  }
  
  static void validateRecordClass(Class<?> recordClass)
  {
    if (recordClass == null) {
      throw new IllegalArgumentException("the recordClass can not be null");
    }
  }
  
  static void validateReverseHeaderMap(Map<String, String> reverseHeaderMap)
  {
    if ((reverseHeaderMap == null) || (reverseHeaderMap.isEmpty())) {
      throw new IllegalArgumentException("the reverseHeaderMap can not be null or empty");
    }
    int columnIndex = 0;
    for (Map.Entry<String, String> entry : reverseHeaderMap.entrySet())
    {
      String headerText = (String)entry.getKey();
      String propName = (String)entry.getValue();
      if (StringUtils.isBlank(headerText)) {
        throw new IllegalArgumentException("One header defined in the reverseHeaderMap has a blank headerText. Header Index (0-based) = " + columnIndex);
      }
      if (StringUtils.isBlank(propName)) {
        throw new IllegalArgumentException("One header defined in the reverseHeaderMap has a blank propName. Header Index (0-based) = " + columnIndex);
      }
      columnIndex++;
    }
  }
  
  static void validateHeaderMap(LinkedHashMap<String, String> headerMap)
  {
    if ((headerMap == null) || (headerMap.isEmpty())) {
      throw new IllegalArgumentException("the headerMap can not be null or empty");
    }
    int columnIndex = 0;
    for (Map.Entry<String, String> entry : headerMap.entrySet())
    {
      String propName = (String)entry.getKey();
      if (StringUtils.isBlank(propName)) {
        throw new IllegalArgumentException("One header has a blank propName. Header Index (0-based) = " + columnIndex);
      }
      columnIndex++;
    }
  }
  
  static <T> void setPropertyWithCellValue(Class<T> recordClass, T record, String propName, Object cellStringOrDate)
  {
    IllegalArgumentException noSetterException = new IllegalArgumentException(MessageFormat.format("No suitable setter for property \"{0}\" with cellValue \"{1}\" ", new Object[] { propName, cellStringOrDate }));
    
    List<Method> setters = SepReflectionHelper.findSettersByPropName(recordClass, propName);
    if (setters.isEmpty()) {
      throw noSetterException;
    }
    if (cellStringOrDate == null)
    {
      for (Method setter : setters)
      {
        Class<?> propClass = setter.getParameterTypes()[0];
        if (SepBasicTypeConverts.canFromNull(propClass))
        {
          SepReflectionHelper.invokeSetter(setter, record, null);
          return;
        }
      }
      throw noSetterException;
    }
    if ((cellStringOrDate instanceof Date))
    {
      Method setter = SepReflectionHelper.findSetterByPropNameAndType(recordClass, propName, Date.class);
      if (setter == null) {
        throw noSetterException;
      }
      SepReflectionHelper.invokeSetter(setter, record, cellStringOrDate);
      return;
    }
    String cellText = (String)cellStringOrDate;
    
    Method stringSetter = SepReflectionHelper.findSetterByPropNameAndType(recordClass, propName, String.class);
    if (stringSetter != null)
    {
      SepReflectionHelper.invokeSetter(stringSetter, record, cellText);
      return;
    }
    for (Method setter : setters)
    {
      Class<?> propClass = setter.getParameterTypes()[0];
      if (SepBasicTypeConverts.canFromThisString(cellText, propClass))
      {
        Object propValue = SepBasicTypeConverts.fromThisString(cellText, propClass);
        SepReflectionHelper.invokeSetter(setter, record, propValue);
        return;
      }
    }
    throw noSetterException;
  }
  
  static <T> T createRecordInstance(Class<T> recordClass)
  {
    try
    {
      Constructor<T> constructor = recordClass.getDeclaredConstructor(new Class[0]);
      constructor.setAccessible(true);
      return (T)constructor.newInstance(new Object[0]);
    }
    catch (NoSuchMethodException e)
    {
      throw new RuntimeException(e);
    }
    catch (InstantiationException e)
    {
      throw new RuntimeException(e);
    }
    catch (IllegalAccessException e)
    {
      throw new RuntimeException(e);
    }
    catch (InvocationTargetException e)
    {
      throw new RuntimeException(e);
    }
  }
  
  static Object readCellAsStringOrDate(Cell cell)
  {
    if (cell == null) {
      return null;
    }
    if (cell.getCellType() == 3) {
      return null;
    }
    if (cell.getCellType() == 4) {
      return String.valueOf(cell.getBooleanCellValue());
    }
    if (cell.getCellType() == 5) {
      return null;
    }
    if (cell.getCellType() == 2) {
      return null;
    }
    if (cell.getCellType() == 0)
    {
      if (DateUtil.isCellDateFormatted(cell))
      {
        Date date = cell.getDateCellValue();
        try
        {
          String inputStr = "01-01-1900";
          DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
          Date oldDate = dateFormat.parse(inputStr);
          if (date.before(oldDate)) {
            return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
          }
        }
        catch (ParseException e)
        {
          e.printStackTrace();
        }
        return date;
      }
      double v = cell.getNumericCellValue();
      if (v % 1.0D == 0.0D)
      {
        long v1 = (long) v;
        return String.valueOf(v1);
      }
      return String.valueOf(v);
    }
    if (cell.getCellType() == 1)
    {
      String s = cell.getStringCellValue();
      return StringUtils.trimToNull(s);
    }
    return null;
  }
  
  private static <T> T parseDataRow(Map<Short, ColumnMeta> columnMetaMap, Row row, int rowIndex, Class<T> recordClass, List<CellError> cellErrors)
  {
    T record = createRecordInstance(recordClass);
    for (short columnIndex = 0; columnIndex < row.getLastCellNum(); columnIndex = (short)(columnIndex + 1))
    {
      ColumnMeta columnMeta = (ColumnMeta)columnMetaMap.get(Short.valueOf(columnIndex));
      if ((columnMeta != null) && (columnMeta.propName != null))
      {
        String propName = columnMeta.propName;
        
        Cell cell = row.getCell(columnIndex);
        Object cellStringOrDate = readCellAsStringOrDate(cell);
        try
        {
          setPropertyWithCellValue(recordClass, record, propName, cellStringOrDate);
        }
        catch (Exception e)
        {
          if (cellErrors != null)
          {
            CellError ce = new CellError();
            ce.setColumnIndex(columnIndex);
            ce.setHeaderText(columnMeta.headerText);
            ce.setPropName(propName);
            ce.setRowIndex(rowIndex);
            ce.setCause(e);
            cellErrors.add(ce);
          }
        }
      }
    }
    return record;
  }
  
  private static class ColumnMeta
  {
    public String propName;
    public String headerText;
    
    public String toString()
    {
      return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
  }
  
  private static Map<Short, ColumnMeta> parseHeader(Map<String, String> reverseHeaderMap, Row row)
  {
    Map<Short, ColumnMeta> columnMetaMap = new LinkedHashMap();
    for (short columnIndex = 0; columnIndex < row.getLastCellNum(); columnIndex = (short)(columnIndex + 1))
    {
      Cell cell = row.getCell(columnIndex);
      Object headerObj = readCellAsStringOrDate(cell);
      String headerText = headerObj == null ? "" : headerObj.toString();
      if (headerText != null)
      {
        String propName = (String)reverseHeaderMap.get(headerText);
        if (propName != null)
        {
          ColumnMeta cm = new ColumnMeta();
          cm.headerText = headerText;
          cm.propName = propName;
          columnMetaMap.put(Short.valueOf(columnIndex), cm);
        }
      }
    }
    return columnMetaMap;
  }
  
  private static Row createHeaders(LinkedHashMap<String, String> headerMap, Sheet sheet)
  {
    CellStyle style = sheet.getWorkbook().createCellStyle();
    style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
    style.setFillPattern((short)1);
    style.setBorderBottom((short)1);
    style.setBorderTop((short)1);
    style.setBorderRight((short)1);
    style.setBorderLeft((short)1);
    
    Row header = sheet.createRow(0);
    int columnIndex = 0;
    for (Map.Entry<String, String> entry : headerMap.entrySet())
    {
      String headerText = StringUtils.defaultString((String)entry.getValue());
      Cell cell = createCell(header, columnIndex);
      cell.setCellValue(headerText);
      cell.setCellStyle(style);
      sheet.autoSizeColumn(columnIndex);
      columnIndex++;
    }
    return header;
  }
  
  private static <T> Row createRow(LinkedHashMap<String, String> headerMap, T record, int recordIndex, Sheet sheet, int rowIndex, String datumErrPlaceholder, List<DatumError> datumErrors)
  {
    Row row = sheet.createRow(rowIndex);
    int columnIndex = 0;
    for (Map.Entry<String, String> entry : headerMap.entrySet())
    {
      boolean datumErr = false;
      String propName = (String)entry.getKey();
      Object propValue = null;
      try
      {
        propValue = SepReflectionHelper.getProperty(record, propName);
      }
      catch (Exception e)
      {
        if (datumErrors != null)
        {
          DatumError de = new DatumError();
          de.setPropName(propName);
          de.setRecordIndex(recordIndex);
          de.setCause(e);
          datumErrors.add(de);
        }
        datumErr = true;
        propValue = datumErrPlaceholder;
      }
      String propValueText = propValue == null ? null : propValue.toString();
      Cell cell = createCell(row, columnIndex);
      cell.setCellValue(StringUtils.defaultString(propValueText));
      if (datumErr)
      {
        CellStyle errStyle = sheet.getWorkbook().createCellStyle();
        errStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        errStyle.setFillPattern((short)1);
        cell.setCellStyle(errStyle);
      }
      columnIndex++;
    }
    return row;
  }
  
  private static Cell createCell(Row row, int columnIndex)
  {
    Cell cell = row.createCell(columnIndex);
    return cell;
  }
  
  private static void writeWorkbook(Workbook workbook, OutputStream outputStream)
  {
    try
    {
      workbook.write(outputStream);
    }
    catch (IOException e)
    {
      throw new IllegalStateException(e);
    }
  }
  
  private static Workbook toWorkbook(InputStream inputStream)
    throws InvalidFormatException
  {
    try
    {
      return WorkbookFactory.create(inputStream);
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
  }
}
