package com.qfix.sep4j.support;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SepBasicTypeConverts
{
  private static final Map<Class<?>, CanFromStringTypeMeta> CanFromStringTypeMetas = new LinkedHashMap();
  private static final Map<Class<?>, CanFromNullTypeMeta> CanFromNullTypeMetas = new LinkedHashMap();
  
  static
  {
    init();
  }
  
  private static void init()
  {
    addCanFromNullTypeMeta(new ShortObjectType());
    addCanFromNullTypeMeta(new IntegerObjectType());
    addCanFromNullTypeMeta(new LongObjectType());
    addCanFromNullTypeMeta(new FloatObjectType());
    addCanFromNullTypeMeta(new DoubleObjectType());
    addCanFromNullTypeMeta(new BooleanObjectType());
    addCanFromNullTypeMeta(new BigIntegerType());
    addCanFromNullTypeMeta(new BigDecimalType());
    addCanFromNullTypeMeta(new StringType());
    addCanFromNullTypeMeta(new DateType());
    
    addCanFromStringTypeMeta(new ShortType());
    addCanFromStringTypeMeta(new IntType());
    addCanFromStringTypeMeta(new LongType());
    addCanFromStringTypeMeta(new FloatType());
    addCanFromStringTypeMeta(new DoubleType());
    addCanFromStringTypeMeta(new BooleanType());
    
    addCanFromStringTypeMeta(new ShortObjectType());
    addCanFromStringTypeMeta(new IntegerObjectType());
    addCanFromStringTypeMeta(new LongObjectType());
    addCanFromStringTypeMeta(new FloatObjectType());
    addCanFromStringTypeMeta(new DoubleObjectType());
    addCanFromStringTypeMeta(new BooleanObjectType());
    
    addCanFromStringTypeMeta(new BigIntegerType());
    addCanFromStringTypeMeta(new BigDecimalType());
    
    addCanFromStringTypeMeta(new StringType());
  }
  
  private static void addCanFromStringTypeMeta(CanFromStringTypeMeta meta)
  {
    CanFromStringTypeMetas.put(meta.getType(), meta);
  }
  
  private static void addCanFromNullTypeMeta(CanFromNullTypeMeta meta)
  {
    CanFromNullTypeMetas.put(meta.getType(), meta);
  }
  
  public static boolean canFromNull(Class<?> targetType)
  {
    CanFromNullTypeMeta typeMeta = (CanFromNullTypeMeta)CanFromNullTypeMetas.get(targetType);
    return typeMeta != null;
  }
  
  public static boolean canFromThisString(String str, Class<?> targetType)
  {
    CanFromStringTypeMeta typeMeta = (CanFromStringTypeMeta)CanFromStringTypeMetas.get(targetType);
    if (typeMeta == null) {
      return false;
    }
    try
    {
      typeMeta.fromThisString(str);
      return true;
    }
    catch (RuntimeException e) {}
    return false;
  }
  
  public static Object fromThisString(String str, Class<?> targetType)
  {
    if (!canFromThisString(str, targetType)) {
      throw new IllegalArgumentException("Please call fromThisString(String str, targetType) first to confirm");
    }
    CanFromStringTypeMeta typeMeta = (CanFromStringTypeMeta)CanFromStringTypeMetas.get(targetType);
    return typeMeta.fromThisString(str);
  }
  
  private static abstract interface BasicType
  {
    public abstract Class<?> getType();
  }
  
  private static abstract interface CanFromNullTypeMeta
    extends SepBasicTypeConverts.BasicType
  {}
  
  private static abstract interface CanFromStringTypeMeta
    extends SepBasicTypeConverts.BasicType
  {
    public abstract Object fromThisString(String paramString)
      throws RuntimeException;
  }
  
  private static class ShortType
    implements SepBasicTypeConverts.CanFromStringTypeMeta
  {
    public Class<?> getType()
    {
      return Short.TYPE;
    }
    
    public Object fromThisString(String str)
    {
      str = SepBasicTypeConverts.retainWholeIfDecimalPartZero(str);
      return Short.valueOf(Short.parseShort(str));
    }
  }
  
  private static class IntType
    implements SepBasicTypeConverts.CanFromStringTypeMeta
  {
    public Class<?> getType()
    {
      return Integer.TYPE;
    }
    
    public Object fromThisString(String str)
    {
      str = SepBasicTypeConverts.retainWholeIfDecimalPartZero(str);
      return Integer.valueOf(Integer.parseInt(str));
    }
  }
  
  private static class LongType
    implements SepBasicTypeConverts.CanFromStringTypeMeta
  {
    public Class<?> getType()
    {
      return Long.TYPE;
    }
    
    public Object fromThisString(String str)
    {
      str = SepBasicTypeConverts.retainWholeIfDecimalPartZero(str);
      return Long.valueOf(Long.parseLong(str));
    }
  }
  
  private static class FloatType
    implements SepBasicTypeConverts.CanFromStringTypeMeta
  {
    public Class<?> getType()
    {
      return Float.TYPE;
    }
    
    public Object fromThisString(String str)
    {
      return Float.valueOf(Float.parseFloat(str));
    }
  }
  
  private static class DoubleType
    implements SepBasicTypeConverts.CanFromStringTypeMeta
  {
    public Class<?> getType()
    {
      return Double.TYPE;
    }
    
    public Object fromThisString(String str)
    {
      return Double.valueOf(Double.parseDouble(str));
    }
  }
  
  private static class BooleanType
    implements SepBasicTypeConverts.CanFromStringTypeMeta
  {
    public Class<?> getType()
    {
      return Boolean.TYPE;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        throw new IllegalArgumentException("don't take null for primitive boolean type");
      }
      return Boolean.valueOf(Boolean.parseBoolean(str));
    }
  }
  
  private static class ShortObjectType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return Short.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      str = SepBasicTypeConverts.retainWholeIfDecimalPartZero(str);
      return Short.valueOf(str);
    }
  }
  
  private static class IntegerObjectType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return Integer.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      str = SepBasicTypeConverts.retainWholeIfDecimalPartZero(str);
      return Integer.valueOf(str);
    }
  }
  
  private static class LongObjectType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return Long.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      str = SepBasicTypeConverts.retainWholeIfDecimalPartZero(str);
      return Long.valueOf(str);
    }
  }
  
  private static class FloatObjectType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return Float.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      return Float.valueOf(str);
    }
  }
  
  private static class DoubleObjectType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return Double.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      return Double.valueOf(str);
    }
  }
  
  private static class BooleanObjectType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return Boolean.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      return Boolean.valueOf(str);
    }
  }
  
  private static class BigIntegerType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return BigInteger.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      str = SepBasicTypeConverts.retainWholeIfDecimalPartZero(str);
      return new BigInteger(str);
    }
  }
  
  private static class BigDecimalType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return BigDecimal.class;
    }
    
    public Object fromThisString(String str)
    {
      if (str == null) {
        return null;
      }
      return new BigDecimal(str);
    }
  }
  
  private static class StringType
    implements SepBasicTypeConverts.CanFromStringTypeMeta, SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return String.class;
    }
    
    public Object fromThisString(String str)
    {
      return str;
    }
  }
  
  private static class DateType
    implements SepBasicTypeConverts.CanFromNullTypeMeta
  {
    public Class<?> getType()
    {
      return Date.class;
    }
  }
  
  static String retainWholeIfDecimalPartZero(String s)
  {
    if (s == null) {
      return s;
    }
    try
    {
      BigDecimal d = new BigDecimal(s);
      s = d.toPlainString();
    }
    catch (NumberFormatException e)
    {
      return s;
    }
    Pattern pattern = Pattern.compile("^(\\d+)\\.0*$");
    Matcher matcher = pattern.matcher(s);
    if (matcher.find()) {
      return matcher.group(1);
    }
    return s;
  }
}