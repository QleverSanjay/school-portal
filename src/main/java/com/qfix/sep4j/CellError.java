package com.qfix.sep4j;

import java.io.Serializable;
import java.text.MessageFormat;

public class CellError
  implements Serializable
{
  private static final long serialVersionUID = -6160610503938820467L;
  private int rowIndex;
  private int columnIndex;
  private String propName;
  private String headerText;
  private Exception cause;
  
  public int getRowIndex()
  {
    return this.rowIndex;
  }
  
  public int getRowIndexOneBased()
  {
    return getRowIndex() + 1;
  }
  
  public void setRowIndex(int rowIndex)
  {
    this.rowIndex = rowIndex;
  }
  
  public int getColumnIndex()
  {
    return this.columnIndex;
  }
  
  public int getColumnIndexOneBased()
  {
    return getColumnIndex() + 1;
  }
  
  public void setColumnIndex(int columnIndex)
  {
    this.columnIndex = columnIndex;
  }
  
  public String getPropName()
  {
    return this.propName;
  }
  
  public void setPropName(String propName)
  {
    this.propName = propName;
  }
  
  public String getHeaderText()
  {
    return this.headerText;
  }
  
  public void setHeaderText(String headerText)
  {
    this.headerText = headerText;
  }
  
  public Exception getCause()
  {
    return this.cause;
  }
  
  public void setCause(Exception cause)
  {
    this.cause = cause;
  }
  
  public String toString()
  {
    return MessageFormat.format("rowIndex = {0}, columnIndex = {1}, propName = \"{2}\", headerText = \"{3}\", cause = {4} ", new Object[] { Integer.valueOf(this.rowIndex), Integer.valueOf(this.columnIndex), this.propName, this.headerText, this.cause });
  }
}