package com.qfix.mq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.qfix.config.AppProperties;
import com.qfix.config.AppProperties.Rabbit;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.amqp.core.Queue;

@Configuration
public class MessageQueueConfiguration {

	@Autowired
	AppProperties appProps;
	
	private ConnectionFactory factory;
	private Connection connection;
	private Channel channel;
	
	
	public ConnectionFactory connectionFactory(){
		Rabbit rabbit = appProps.getRabbit();
		factory = new ConnectionFactory();
		factory.setHost(rabbit.getHost());
        factory.setPort(rabbit.getPort());
        factory.setUsername(rabbit.getUsername());
        factory.setPassword(rabbit.getPassword());
        return factory;
	}
	
	@Bean
	public Channel channel() throws IOException, TimeoutException{
		connection = connectionFactory().newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(appProps.getRabbit().getFileUploadQueue(), true, false, false, null);
		return channel;
	}
	
    @Bean
    public Queue fileUploadQueue() {
        return new Queue(appProps.getRabbit().getFileUploadQueue());
    }
	
	public void close() throws IOException, TimeoutException{
		channel().close();
	}
}
