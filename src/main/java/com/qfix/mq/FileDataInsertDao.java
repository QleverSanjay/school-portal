package com.qfix.mq;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.ResultSet;

import lombok.extern.slf4j.Slf4j;

import org.springframework.jdbc.core.RowMapper;

import com.qfix.model.FileUploadJob;
import com.qfix.model.IncorrectTransactionIdReport;
import com.qfix.model.PaymentReportSummary;
import com.mysql.jdbc.Statement;
import com.qfix.config.AppProperties;

@Slf4j
@Repository
public class FileDataInsertDao extends JdbcDaoSupport {

	@Autowired
	private DriverManagerDataSource dataSource;

	@Autowired
	private AppProperties appProperties;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	public List<FileUploadJob> getFileUploadJobs(Integer userId, Integer branchId){		
		String sql = "select fum.*, concat(c.firstname, ' ', ifnull(c.lastname, '')) as uploadedBy "		
				+ " from file_upload_metadata as fum INNER JOIN contact as c on c.user_id = fum.uploaded_by "		
				+ " where branch_id = " + branchId + " and fum.uploaded_at > curdate() - 2";		
		if(userId != null ){		
			sql += " AND uploaded_by = " + userId;		
		}		
		log.info(sql);		
		List<FileUploadJob> fileUploads= getJdbcTemplate().query(sql, new RowMapper<FileUploadJob>() {		
			@Override		
			public FileUploadJob mapRow(ResultSet rs, int arg1) throws SQLException {		
				FileUploadJob job = new FileUploadJob();		
				job.setId(rs.getInt("id"));		
				job.setJobStatus(rs.getString("job_status"));		
				job.setBranchId(rs.getInt("branch_id"));		
				job.setTotalRecords(rs.getInt("total_records"));		
				job.setInvalidRecords(rs.getInt("invalid_records"));		
				job.setValidRecords(rs.getInt("valid_records"));		
				job.setUploadedBy(rs.getString("uploadedBy"));		
				job.setUploadedAt(rs.getString("uploaded_at"));		
				job.setInvalidDataFilePath(rs.getString("invalid_data_file_path"));		
				return job;		
			}		
		});		
		return fileUploads;		
	}		
	public FileUploadJob getFileUploadJobById(Integer id){		
		String sql = "select * from file_upload_metadata where id = "+ id;		
		List<FileUploadJob> fileUploads= getJdbcTemplate().query(sql, new RowMapper<FileUploadJob>() {		
			@Override		
			public FileUploadJob mapRow(ResultSet rs, int arg1) throws SQLException {		
				FileUploadJob job = new FileUploadJob();		
				job.setId(rs.getInt("id"));		
				job.setJobStatus(rs.getString("job_status"));		
				job.setBranchId(rs.getInt("branch_id"));		
				job.setTotalRecords(rs.getInt("total_records"));		
				job.setInvalidRecords(rs.getInt("invalid_records"));		
				job.setValidRecords(rs.getInt("valid_records"));		
				job.setInvalidDataFilePath(rs.getString("invalid_data_file_path"));		
				job.setInvalidRecordData(rs.getString("invalid_record_data"));		
				return job;		
			}		
		});		
		if(fileUploads != null && fileUploads.size() > 0){		
			return fileUploads.get(0);		
		}		
		return null;		
	}
	
	public FileUploadJob getFileUploadJobByBranchId(Integer branchId){		
		String sql = "select * from file_upload_metadata where (job_status is null or job_status = 'STARTED') and branch_id = "+ branchId;		
		List<FileUploadJob> fileUploads= getJdbcTemplate().query(sql, new RowMapper<FileUploadJob>() {		
			@Override		
			public FileUploadJob mapRow(ResultSet rs, int arg1) throws SQLException {		
				FileUploadJob job = new FileUploadJob();		
				job.setId(rs.getInt("id"));		
				job.setJobStatus(rs.getString("job_status"));		
				job.setBranchId(rs.getInt("branch_id"));		
				job.setTotalRecords(rs.getInt("total_records"));		
				job.setInvalidRecords(rs.getInt("invalid_records"));		
				job.setValidRecords(rs.getInt("valid_records"));		
				job.setInvalidDataFilePath(rs.getString("invalid_data_file_path"));		
				job.setInvalidRecordData(rs.getString("invalid_record_data"));		
				return job;		
			}		
		});		
		if(fileUploads != null && fileUploads.size() > 0){		
			return fileUploads.get(0);		
		}		
		return null;		
	}


	@Transactional
	public Integer insertFileMetaData(final MultipartFile file, final Integer branchId) {
		String tempFilePath;
		Integer id = 0;
		
		

		String filePath = appProperties.getFileProps().getBaseSystemPath()
				+ appProperties.getFileProps().getStudentUploadFilePath();
		InputStream is = null;
		OutputStream os = null;
		try {
			is = file.getInputStream();

			os = new FileOutputStream(filePath + file.getOriginalFilename());
			tempFilePath = filePath + file.getOriginalFilename();
			final String finalPath = tempFilePath; 

			int c;
			while ((c = is.read()) != -1) {
				os.write(c);
			}
			
			final String insertFileUploadData = "insert into file_upload_metadata(file_path, unique_file_id, branch_id) "
					+ "values(?, ?, ?)";

			KeyHolder holder = new GeneratedKeyHolder();

			getJdbcTemplate().update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					PreparedStatement stmt = connection.prepareStatement(
							insertFileUploadData, Statement.RETURN_GENERATED_KEYS);
					stmt.setString(1, finalPath);
					stmt.setInt(2,branchId);
					stmt.setInt(3, branchId);

					return stmt;
				}
			}, holder);
			
			id = holder.getKey().intValue();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		
		return id;
	}
	
	@Transactional
	public void insertUserId(int userId, int fileId){
		String sql = "update file_upload_metadata set uploaded_by = " + userId + " where id = " + fileId;
		try
		{
			getJdbcTemplate().update(sql);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Transactional
	public Integer insertPaymentSummaryReport(final PaymentReportSummary paymentReportSummary){
		final String insertSummaryReportData = "insert into payment_summary_report(report_date, number_of_payments, grand_total, "
				+ "submerchant_charges, refund_adjusted, chargeback_adjusted, chargeback_reversal, total_net_amount, uploaded_by) "
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();
		Integer id;
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement stmt = connection.prepareStatement(insertSummaryReportData, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, paymentReportSummary.getReportDate());
				stmt.setDouble(2, paymentReportSummary.getNumberOfPayments());
				stmt.setDouble(3, paymentReportSummary.getGrandTotal());
				stmt.setDouble(4, paymentReportSummary.getSubmerchantCharges());
				stmt.setDouble(5, paymentReportSummary.getRefundAdjusted());
				stmt.setDouble(6, paymentReportSummary.getChargebackAdjusted());
				stmt.setDouble(7, paymentReportSummary.getChargebackReversal());
				stmt.setDouble(8, paymentReportSummary.getTotalNetAmount());
				stmt.setString(9, paymentReportSummary.getUploadedBy());
				return stmt;
			}
		}, holder);
		id = holder.getKey().intValue();
		log.info("Inserted payment report summary data with ID " + id);
		return id;
	}
	
	public void insertSettlementDate(String transactionID, String settlementDate, Integer reportSummaryID
			, Double settlementAmount, String qfixRefNumber){
		String query = "update payment.payment_detail pd set pd.settlement_date = '" + settlementDate 
				+ "', pd.settlement_amount ="+ settlementAmount +" where pd.payment_gateway_transaction_id = '" + transactionID +"'"
						+ "or pd.qfix_reference_number = '" + qfixRefNumber + "'"; 
		
		getJdbcTemplate().update(query);
		log.info("Updating payment.payment_detail -> " + query);
		
		query = "update  qfix.payment_audit pa "
				+ "join payment.payment_detail pd on pa.qfix_reference_number = pd.qfix_reference_number "
				+ "set  pa.settlement_date = '" + settlementDate + "', pa.summary_report_id = "+ reportSummaryID 
				+ ", pa.settlement_amount="+ settlementAmount +"  where pd.payment_gateway_transaction_id = '"+ transactionID +"'"
				+ "or pd.qfix_reference_number = '" + qfixRefNumber + "'";
		getJdbcTemplate().update(query);
		log.info("Updating qfix.payment_audit -> " + query);
	}
	
	public String getUsernameById(Integer id){
		String sql = "select username from user where id = " + id;
		String username = getJdbcTemplate().queryForObject(sql, String.class);
		return username;
	}
	
	public boolean getPaymentTransactionId(String id, String qfixReferenceNumber){
		String sql = "select count(*) from payment.payment_detail where payment_gateway_transaction_id = " + id + " or qfix_reference_number = '" + qfixReferenceNumber+"'"; 
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count > 0)
			return true;
		return false;
	}
	
	public void reportIncorrectTransactionId(final IncorrectTransactionIdReport i){
		final String sql = "INSERT INTO incorrect_transaction_id (transaction_id, date, amount, "
				+ "type, merchant_id, bank_name, qfix_reference_number, "
				+ "bank_transaction_id, drawer_name, pickup_location, pickup_point, "
				+ "cl_location, prod_code) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, i.getTransactionId());
				stmt.setString(2, i.getDate());
				stmt.setDouble(3, i.getAmount());
				stmt.setString(4,  i.getType());
				stmt.setString(5, i.getMerchantId());
				stmt.setString(6,  i.getBankName());
				stmt.setString(7,  i.getQfixReferenceNumber());
				stmt.setString(8,  i.getBankTransactionId());
				stmt.setString(9,  i.getDrawerName());
				stmt.setString(10,  i.getPickupLocation());
				stmt.setString(11,  i.getPickupPoint());
				stmt.setString(12,  i.getClLocation());
				stmt.setString(13,  i.getProdCode());
				return stmt;
			}
		}, holder);
	}

	public Integer insertMissingTransactionId(List<IncorrectTransactionIdReport> report){
		Map<String, IncorrectTransactionIdReport> map = new HashMap<String, IncorrectTransactionIdReport>();
		for(IncorrectTransactionIdReport id : report){
			insertMissingTransactionId(map, id);
		}
		try{
			for(String s : map.keySet()){
				IncorrectTransactionIdReport i = map.get(s);
				insertSettlementDate(i.getTransactionId(), i.getDate(), 0, i.getAmount(), i.getQfixReferenceNumber());
			}
		}catch(Exception e){
			log.info("Failed in inserting missing transaction id upload");
			return -1;
		}
		return 1;
	}
	
	private void insertMissingTransactionId(Map<String, IncorrectTransactionIdReport> map, IncorrectTransactionIdReport report){
		if(map.containsKey(report.getTransactionId())){
			IncorrectTransactionIdReport i = map.get(report.getTransactionId());
			i.setAmount(i.getAmount() + report.getAmount());
			map.put(report.getTransactionId(), i);
		}else
			map.put(report.getTransactionId(), report);
	}
	
	public void deleteMissingTransactionId(List<IncorrectTransactionIdReport> report){
		String sql = "delete from incorrect_transaction_id where transaction_id = ";
		for(IncorrectTransactionIdReport i : report){
			getJdbcTemplate().update(sql + i.getTransactionId());
		}
	}
}
