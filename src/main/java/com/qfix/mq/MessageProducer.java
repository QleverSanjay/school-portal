package com.qfix.mq;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.qfix.config.AppProperties;
import com.qfix.config.AppProperties.Jdbc;
import com.qfix.config.AppProperties.Rabbit;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Service
public class MessageProducer {
	
	@Autowired
	AppProperties appProps;
	
	@Autowired
	MessageQueueConfiguration configuration;
	
	/*@Autowired
	MessageSender sender;*/
	
	public void publishMessage(String message) throws Exception{
		configuration.channel().basicPublish("", appProps.getRabbit().getFileUploadQueue(), null, message.getBytes("UTF-8"));
		
		//sender.send(message);
		System.out.println("Sent '" + message + "'");
	}
	
	public void closeConnection() throws IOException, TimeoutException{
		configuration.channel().close();
		configuration.close();
	}
	
	/*AppProperties appProps;
	
	private ConnectionFactory factory;
	private Connection connection;
	private Channel channel;
	
	@Autowired
	public MessageProducer(AppProperties appProps) throws IOException, TimeoutException {
		this.appProps = appProps;
		initialize();
	}
	
	//@PostConstruct
	@Bean
	private void initialize() throws IOException, TimeoutException{
		
		/*Jdbc jdbc = appProps.getJdbc();
		System.out.println(jdbc.getDriverClassName());*/
		
//		Rabbit rabbit = appProps.getRabbit();
//		factory = new ConnectionFactory();
//        factory.setHost(rabbit.getHost());
//        factory.setPort(rabbit.getPort());
//        factory.setUsername(rabbit.getUsername());
//        factory.setPassword(rabbit.getPassword());
//		
//		/*factory.setHost("127.0.0.1");
//		factory.setPort(5672);
//		factory.setUsername("guest");
//		factory.setPassword("guest");*/
//		connection = factory.newConnection();
//        channel = connection.createChannel();
//        channel.queueDeclare(appProps.getRabbit().getFileUploadQueue(), true, false, false, null);
	//}
	
	/*public void publishMessage(String message) throws UnsupportedEncodingException, IOException{
		channel.basicPublish("", appProps.getRabbit().getFileUploadQueue(), null, message.getBytes("UTF-8"));
		System.out.println("Sent '" + message + "'");
	}
	
	public void closeConnection() throws IOException, TimeoutException{
		channel.close();
        connection.close();
	}*/
	
    
}
