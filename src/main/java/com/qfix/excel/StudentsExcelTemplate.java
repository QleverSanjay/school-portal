package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qfix.model.Student;
import com.qfix.model.StudentParent;

@Repository
public class StudentsExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{

	List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	@Autowired
	ExcelTemplateGenerator generator;
	
	private List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, boolean isUpdate) {
		columnDetailsList = new ArrayList<ExcelColumnDetails>();

		List<String> actions = new ArrayList<>();
		actions.add("Add");
		actions.add("Update");
		actions.add("Delete");

		// Student Started
		if(isUpdate){
			ExcelColumnDetails column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.STUDENT_ID_LABEL);
			column.setDataType("String");
			column.setHideColumn(true);
			columnDetailsList.add(column);
			
			column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.STUDENT_ACTION_LABEL);
			column.setDataType("String");
			column.setListOptions(actions);
			column.setSelectFromList(true);
			columnDetailsList.add(column);
		}

		getStudentColumnDetails(branchId);

		// Student End

		// Father Start
		if(isUpdate){
			ExcelColumnDetails column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.FATHER_ID_LABEL);
			column.setDataType("String");
			column.setHideColumn(true);
			columnDetailsList.add(column);

			column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.FATHER_ACTION_LABEL);
			column.setDataType("String");
			column.setListOptions(actions);
			column.setSelectFromList(true);
			columnDetailsList.add(column);
		}

		getFatherColumnDetails();
		
		// Father End
		
		// Mother Start
		if(isUpdate){
			ExcelColumnDetails column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.MOTHER_ID_LABEL);
			column.setDataType("String");
			column.setHideColumn(true);
			columnDetailsList.add(column);

			column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.MOTHER_ACTION_LABEL);
			column.setDataType("String");
			column.setListOptions(actions);
			column.setSelectFromList(true);
			columnDetailsList.add(column);
		}

		getMotherColumnDetails();

		// Mother End

		// Guardian Start
		if(isUpdate){
			ExcelColumnDetails column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.GUARDIAN_ID_LABEL);
			column.setDataType("String");
			column.setHideColumn(true);
			columnDetailsList.add(column);

			column = new ExcelColumnDetails();
			column.setColumnName(UploadConstants.GUARDIAN_ACTION_LABEL);
			column.setDataType("String");
			column.setListOptions(actions);
			column.setSelectFromList(true);
			columnDetailsList.add(column);
		}

		getParentColumnDetails();

		// Guardian End
    	return columnDetailsList;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
    	return getColumnDetails(workbookName, branchId, false);
	}
	
	private void getStudentColumnDetails(Integer branchId){
		
		
		ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FIRST_NAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MIDDLE_NAME);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.SURNAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.USERID_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STUDENT_CREATE_LOGIN_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);
    	
    	/*columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Father Name");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Mother Name");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);*/

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GENDER_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getGenderList());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.BIRTH_DATE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EMAIL_ADDRESS_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOBILE_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.ADDRESS_LINE_1_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.AREA_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STATE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.STATE));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.DISTRICT_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.DISTRICT));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TALUKA_CITY_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.TALUKA));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TOWN_VILLAGE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.PINCODE_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.RELIGION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.RELIGION));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.CASTE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.CASTE));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STANDARD_LABEL);
    	columnDetails.setSelectFromList(true);
    	List<String> standardList = getStandards(branchId);
    	System.out.println("Standard List ::::::: \n"+standardList);
    	columnDetails.setListOptions(standardList);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.DIVISION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDivisions(branchId));
    	columnDetailsList.add(columnDetails);

    	/*made changes by pitabas on 14_09_2017*/

    	/*columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STUDENT_FEES_CODE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getAllFeesCodes(branchId));
    	columnDetailsList.add(columnDetails);*/

    	for(int i=1;i<=5;i++){
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.STUDENT_FEES_CODE_LABEL+i);
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getAllFeesCodes(branchId));
	    	columnDetailsList.add(columnDetails);
    	}

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.ROLL_NUMBER_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STUDENT_REGISTRATION_CODE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.ACTIVE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);
    	
	}
	
	private void getFatherColumnDetails(){
		ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_FIRST_NAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

		columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_MIDDLE_NAME_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_SURNAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_USERID_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_CREATE_LOGIN_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_BIRTH_DATE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_EMAIL_ADDRESS_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_PRIMARY_MOBILE_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
		columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_SECONDARY_MOBILE_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_ADDRESS_LINE_1_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_AREA_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	/*columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_STATE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.FATHER_STATE));
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_DISTRICT_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.FATHER_DISTRICT));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_TALUKA_CITY_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.FATHER_TALUKA));
    	columnDetailsList.add(columnDetails); */
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_TOWN_VILLAGE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_PINCODE_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);  
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_PROFESSION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getProfessions());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FATHER_INCOME_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getIncomes());
    	columnDetailsList.add(columnDetails);
    	
	}
	
	private void getMotherColumnDetails(){
		ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_FIRST_NAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

		columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_MIDDLE_NAME_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_SURNAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_USERID_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_CREATE_LOGIN_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_BIRTH_DATE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_EMAIL_ADDRESS_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_PRIMARY_MOBILE_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
		columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_SECONDARY_MOBILE_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_ADDRESS_LINE_1_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_AREA_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	/*columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_STATE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.MOTHER_STATE));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_DISTRICT_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.MOTHER_DISTRICT));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_TALUKA_CITY_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.MOTHER_TALUKA));
    	columnDetailsList.add(columnDetails); */
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_TOWN_VILLAGE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_PINCODE_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);  

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_PROFESSION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getProfessions());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.MOTHER_INCOME_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getIncomes());
    	columnDetailsList.add(columnDetails);

	}

	private void getParentColumnDetails(){
		ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_FIRST_NAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

		columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_MIDDLE_NAME_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_SURNAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_USERID_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_CREATE_LOGIN_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_GENDER_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getGenderList());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_BIRTH_DATE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_EMAIL_ADDRESS_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_PRIMARY_MOBILE_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
		columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_SECONDARY_MOBILE_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_ADDRESS_LINE_1_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_AREA_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	/*columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.PARENT_STATE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.PARENT_STATE));
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.PARENT_DISTRICT_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.PARENT_DISTRICT));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.PARENT_TALUKA_CITY_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.PARENT_TALUKA));
    	columnDetailsList.add(columnDetails); */
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_TOWN_VILLAGE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_PINCODE_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails); 
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_RELATION_WITH_STUDENT);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.RELATION));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_PROFESSION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getProfessions());
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.GUARDIAN_INCOME_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getIncomes());
    	columnDetailsList.add(columnDetails);

	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		return null;
	}

	@Override
	public void setToken(String token) {
		
	}


	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{
		List<String> dependentFields = new ArrayList<>();
		dependentFields.add(UploadConstants.DEPENDENT_ADDRESS);
		dependentFields.add(UploadConstants.DEPENDENT_STANDARD);
		generator.prepareFile("", excelFileName, getColumnDetails(filePath, branchId), dependentFields, branchId, false, null);

		return generator.getExcelTemplateFile();
	}


	public byte[] downloadStudents(List<Student> students, Integer branchId) throws IOException {
		List<String> dependentFields = new ArrayList<>();
		dependentFields.add(UploadConstants.DEPENDENT_ADDRESS);
		dependentFields.add(UploadConstants.DEPENDENT_STANDARD);

		List<ExcelColumnDetails> columnDetails = getColumnDetails("", branchId, true);

		String[][] dataArray = getDataArray(students, columnDetails.size());
		/*List<Integer> readOnlyColumnIndexes = new ArrayList<>();
		readOnlyColumnIndexes.add(1);
		readOnlyColumnIndexes.add(27);
		readOnlyColumnIndexes.add(44);
		readOnlyColumnIndexes.add(61);*/

		generator.prepareFile("", "eduqfix-upload-student-template.xlsx",  
				columnDetails, dependentFields, branchId, false, dataArray);

		return generator.getExcelTemplateFile();
	}

	private String[][] getDataArray(List<Student> students, int columnLength) {
		String[][] dataArray = new String[students.size()][columnLength + 1];

		for(int i =0; i< students.size(); i++ ){
			Student student = students.get(i);
			int j=0;
			dataArray[i][j ++] = student.getId() +"";
			dataArray[i][j ++] = "";
			dataArray[i][j ++] = student.getFirstname();
			dataArray[i][j ++] = student.getMiddleName();
			dataArray[i][j ++] = student.getSurname();
			dataArray[i][j ++] = student.getUsername();
			dataArray[i][j ++] = student.getCreateStudentLogin();
			dataArray[i][j ++] = student.getGender();
			dataArray[i][j ++] = student.getDateOfBirth();
			dataArray[i][j ++] = student.getEmailAddress();
			dataArray[i][j ++] = student.getMobile();
			dataArray[i][j ++] = student.getAddressLine1();
			dataArray[i][j ++] = student.getArea();
			dataArray[i][j ++] = student.getState();
			dataArray[i][j ++] = student.getDistrict();
			dataArray[i][j ++] = student.getTaluka();
			dataArray[i][j ++] = student.getCity();
			dataArray[i][j ++] = student.getPincode();
			dataArray[i][j ++] = student.getReligion();
			dataArray[i][j ++] = student.getCaste();
			dataArray[i][j ++] = student.getStandardName();
			dataArray[i][j ++] = student.getDivisionName();
			/* changes made for multiple feeCode in student by pitabas on 2017_12_11*/
			//dataArray[i][j ++] = student.getFeesCodeName();
			if(!StringUtils.isEmpty(student.getFeesCodeName())){
				String[] arr = student.getFeesCodeName().split(",");
				for(String s : arr){
					dataArray[i][j ++] = s;
				}
				for(int blankCol = 0; blankCol < 5 - arr.length; blankCol++){
					dataArray[i][j ++] = "";
				}
			}
			else{
				
				for(int blankCol = 0; blankCol < 5; blankCol++){
					dataArray[i][j ++] = "";
				}
			}
			/* changes made for multiple feeCode in student by pitabas*/
			
			
			dataArray[i][j ++] = student.getRollNumber();
			dataArray[i][j ++] = student.getRegistrationCode();
			dataArray[i][j ++] = student.getActive();
			
			createParentDataArray(dataArray, i, j, student);
		}
		return dataArray;
	}

	private void createParentDataArray(String[][] dataArray, int i, int j,
			Student student) {
		List<StudentParent> parents = student.getStudentParentList();

		if(parents != null && parents.size() > 0){
			StudentParent father = null;
			StudentParent mother = null;
			StudentParent guardian = null;

			for(StudentParent parent : parents ){
				if(parent.getRelationId() == 1){
					father = parent;
				}
				else if(parent.getRelationId() == 2){
					mother = parent;
				}
				else if(parent.getRelationId() == 3){
					guardian = parent;
				}
			}

			if(father != null){
				createParentDataArray(dataArray, i, j, father);
			}

			j += 17;

			if(mother != null){
				createParentDataArray(dataArray, i, j, mother);
			}

			j += 17;

			if(guardian != null){
				createParentDataArray(dataArray, i, j, guardian);
			}
		}
	}


	private void createParentDataArray(String[][] dataArray, int i, int j,
			StudentParent parent) {

		dataArray[i][j ++] = parent.getId() +"";
		dataArray[i][j ++] = "";
		dataArray[i][j ++] = parent.getFirstname();
		dataArray[i][j ++] = parent.getMiddlename();
		dataArray[i][j ++] = parent.getLastname();
		dataArray[i][j ++] = parent.getParentUserId();
		dataArray[i][j ++] = parent.getCreateLogin();
		dataArray[i][j ++] = parent.getDob();
		dataArray[i][j ++] = parent.getEmail();
		dataArray[i][j ++] = parent.getPrimaryContact();
		dataArray[i][j ++] = parent.getSecondaryContact();
		dataArray[i][j ++] = parent.getAddressLine();
		dataArray[i][j ++] = parent.getArea();
		dataArray[i][j ++] = parent.getCity();
		dataArray[i][j ++] = parent.getPincode();
		dataArray[i][j ++] = parent.getProfession();
		dataArray[i][j ++] = parent.getIncomeRange();
	}
}
