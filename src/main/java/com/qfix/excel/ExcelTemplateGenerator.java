package com.qfix.excel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.qfix.daoimpl.MasterDao;
import com.qfix.model.Master;

@Service
@Scope(value="prototype")
public class ExcelTemplateGenerator {
	
	
	String numbers[] = {"0","1","2","3","4","5","6","7","8","9"};
	public static void main(String[] args) {
		//new ExcelTemplateGenerator("", "abc.xlsx");
		ExcelTemplateGenerator e = new ExcelTemplateGenerator();
		e.getColumnDetails("eduqfix-upload-fees-template", 1);
	}

	private Workbook workbook;
	@Autowired
	private MasterDao masterDao;
	CellStyle styleString;
    CellStyle styleDate;
	public ExcelTemplateGenerator(){
		
	}
	
	public void prepareFile(String filePath, String workbookName, List<ExcelColumnDetails> columnDetails) {
		prepareFile(filePath, workbookName, columnDetails, null, null, false, null) ;
    }

	public void prepareFile(String filePath, String workbookName, List<ExcelColumnDetails> columnDetails, boolean useExistingTemplate) {
		prepareFile(filePath, workbookName, columnDetails, null, null, useExistingTemplate, null) ;
    }

	public void prepareFile(String filePath, String workbookName,
			List<ExcelColumnDetails> columnDetails,
			List<String> dependentFields, Integer branchId,
			boolean useExistingTemplate, String[][] dataArray) {
		File file = null;
        FileOutputStream fos = null;
        Workbook workbook = null;
        Sheet sheet = null;
        DataValidationHelper dvHelper = null;
        DataValidationConstraint dvConstraint = null;
        DataValidation validation = null;
        CellRangeAddressList addressList = null;
        String sheetName = "LinkedValidations";
       int rowStartPosition = 0;
       int columnStartPosition = 0;

        try {
        	 //workbook = new XSSFWorkbook();
            if (workbookName.endsWith(".xlsx")) {
                workbook = new XSSFWorkbook();
            } else {
                workbook = new HSSFWorkbook();
            }

            if(useExistingTemplate) {
            	FileInputStream input_document = new FileInputStream(new File(filePath+workbookName));
            	 workbook = new XSSFWorkbook(input_document);
            	 sheet = workbook.getSheet(sheetName);
            	 rowStartPosition = 5;
            	 columnStartPosition = 1;
    		} else {
    			sheet = workbook.createSheet(sheetName);
    		}
            for(int i = 0; i < columnDetails.size(); i++){
            	if(columnDetails.get(i).isHideColumn())
            		sheet.setColumnHidden(i, true);
            }
            
            createFileData(columnDetails, dependentFields, branchId, workbook,
					sheet, dvHelper, rowStartPosition, columnStartPosition);

            sheet.createFreezePane(0, 1);
            if(dataArray != null){
            	/*CellStyle unlockedCellStyle = workbook.createCellStyle();
            	unlockedCellStyle.setLocked(false);*/
            	for(int i =0; i < dataArray.length; i ++){
                	Row row = sheet.createRow(i+1);
                	for(int j =0; j < columnDetails.size(); j ++){
                		Cell cell = row.createCell(j);
                		cell.setCellValue(dataArray[i][j]);
                		/*if(readOnlyColumnIndexes == null || !readOnlyColumnIndexes.contains(j + 1)){
                			cell.setCellStyle(unlockedCellStyle);
                		}*/
                	}
                }
            }
            //sheet.protectSheet("password");
            file = new File(workbookName);
            fos = new FileOutputStream(file);
            workbook.write(fos);
            this.workbook = workbook;
            //workbook.close();
        } catch (IOException ioEx) {
//            System.out.println("Caught a: " + ioEx.getClass().getName());
//            System.out.println("Message: " + ioEx.getMessage());
//            System.out.println("Stacktrace follws:.....");
            ioEx.printStackTrace(System.out);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                    fos = null;
                }
            } catch (IOException ioEx) {
                System.out.println("Caught a: " + ioEx.getClass().getName());
                System.out.println("Message: " + ioEx.getMessage());
                System.out.println("Stacktrace follws:.....");
                ioEx.printStackTrace(System.out);
            }
        }
	}

	private void createFileData(List<ExcelColumnDetails> columnDetails,
			List<String> dependentFields, Integer branchId, Workbook workbook,
			Sheet sheet, DataValidationHelper dvHelper, int rowStartPosition,
			int columnStartPosition) {
		// Integer rowNumber=5000;
		DataValidationConstraint dvConstraint;
		DataValidation validation;
		CellRangeAddressList addressList;
		/*((XSSFSheet)sheet).enableLocking();
		CellStyle editableStyle = workbook.createCellStyle();
		editableStyle.setLocked(false);*/
		styleString = workbook.createCellStyle();
		styleDate = workbook.createCellStyle();
		styleString.setDataFormat(workbook.createDataFormat().getFormat("0"));
		styleDate.setDataFormat(workbook.createDataFormat().getFormat("@"));
		/*
		XSSFDataFormat format = (XSSFDataFormat) workbook.createDataFormat();
		styleDate.setDataFormat(format.getFormat("@"));
		System.out.println("This date format is taken for date :::::" + format.getFormat("@"));
		*/
		Row row = sheet.createRow(rowStartPosition);
		Cell cell;
		int columnNumber =columnStartPosition;

		if (dependentFields != null ){																		//Setting Name, data in excel after 800 rows
			if (dependentFields.contains(UploadConstants.DEPENDENT_ADDRESS)) {
				setDependentDropDownDataForAddress(sheet);
			} 
			
			if (dependentFields.contains(UploadConstants.DEPENDENT_STANDARD)) {
				setDependentDropDownDataForStandard(sheet, branchId);
			}
			
			if (dependentFields.contains(UploadConstants.DEPENDENT_SUBJECT)) {
				setDependentDropDownDataForStandardSubject(sheet, branchId);
			}
		}

		String columnName;
		/* Added the change for the excel header to have background color --Ameya Sawant*/
		XSSFCellStyle cellColorStyle = (XSSFCellStyle) workbook.createCellStyle();
		cellColorStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		cellColorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		/* Added the change for the excel header to have background color --Ameya Sawant*/
		int drowpdownDataRowNumber = 12000;
		for (ExcelColumnDetails column: columnDetails) {													//Check cases and insert dependant dropdown data in column
			columnName = column.getColumnName();
			if(columnNumber > 120) break;
			cell = row.createCell(columnNumber);
			cell.setCellValue(columnName);

			//cell.setCellStyle(generelStyle);
//			System.out.println("Column title :::       "+column.getColumnName());

			if(column.isSelectFromList() ) {
				addressList = new CellRangeAddressList(rowStartPosition+1, 800, columnNumber, columnNumber);

				if(dependentFields != null && dependentFields.size() > 0) {

					//TODO : Below code needs to be refactored.
		        	switch (columnName) {
					case UploadConstants.STATE_LABEL + UploadConstants.STAR_LABEL:
						 dvHelper = sheet.getDataValidationHelper();
				         dvConstraint = dvHelper.createFormulaListConstraint("ADDRESS");
				         System.out.println("In State");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.DISTRICT_LABEL + UploadConstants.STAR_LABEL:
						String col = getExcelColumnName(columnNumber -1);
						String cellRange= col+"2:"+col+"2";
						System.out.println("In District");
//						System.out.println("columnNumber   " +columnNumber+"     col   :: "+ col+"    cellRange : "+cellRange);
						dvConstraint = dvHelper.createFormulaListConstraint(
				                 "INDIRECT("+cellRange+")");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.TALUKA_CITY_LABEL + UploadConstants.STAR_LABEL:
						col = getExcelColumnName(columnNumber -1);
						cellRange= col+"2:"+col+"2";
//						System.out.println("In Taluka");
//						System.out.println("columnNumber   " +columnNumber+"     col   :: "+ col+"    cellRange : "+cellRange);
						dvConstraint = dvHelper.createFormulaListConstraint(
				                 "INDIRECT("+cellRange+")");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.STATE_LABEL:
						 dvHelper = sheet.getDataValidationHelper();
				         dvConstraint = dvHelper.createFormulaListConstraint("ADDRESS");
//				         System.out.println("In State");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.DISTRICT_LABEL:
						col = getExcelColumnName(columnNumber -1);
						cellRange= col+"2:"+col+"2";
//						System.out.println("In District");
//						System.out.println("columnNumber   " +columnNumber+"     col   :: "+ col+"    cellRange : "+cellRange);
						dvConstraint = dvHelper.createFormulaListConstraint(
				                 "INDIRECT("+cellRange+")");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.TALUKA_CITY_LABEL:
						col = getExcelColumnName(columnNumber -1);
						cellRange= col+"2:"+col+"2";
//						System.out.println("In Taluka");
//						System.out.println("columnNumber   " +columnNumber+"     col   :: "+ col+"    cellRange : "+cellRange);
						dvConstraint = dvHelper.createFormulaListConstraint(
				                 "INDIRECT("+cellRange+")");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.STANDARD_LABEL + UploadConstants.STAR_LABEL:
						dvHelper = sheet.getDataValidationHelper();
						 dvHelper = sheet.getDataValidationHelper();
				         dvConstraint = dvHelper.createFormulaListConstraint("STANDARD");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
				    
						break;

					case UploadConstants.DIVISION_LABEL + UploadConstants.STAR_LABEL:
						col = getExcelColumnName(columnNumber -1);
						cellRange= col+"2:"+col+"2";
						//System.out.println("columnNumber   " +columnNumber+"     col   :: "+ col+"    cellRange : "+cellRange);
						dvConstraint = dvHelper.createFormulaListConstraint(
				                 "INDIRECT("+cellRange+")");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.STANDARD_LABEL:
						dvHelper = sheet.getDataValidationHelper();
						 dvHelper = sheet.getDataValidationHelper();
				         dvConstraint = dvHelper.createFormulaListConstraint("STANDARD");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);

						break;

					case UploadConstants.DIVISION_LABEL:
						col = getExcelColumnName(columnNumber -1);
						cellRange= col+"2:"+col+"2";
						//System.out.println("columnNumber   " +columnNumber+"     col   :: "+ col+"    cellRange : "+cellRange);
						dvConstraint = dvHelper.createFormulaListConstraint(
				                 "INDIRECT("+cellRange+")");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					case UploadConstants.SUBJECT_NAME_LABEL + UploadConstants.STAR_LABEL:
						col = getExcelColumnName(columnNumber - 2);
						cellRange= col+"2:"+col+"2";
						//System.out.println("columnNumber   " +columnNumber+"     col   :: "+ col+"    cellRange : "+cellRange);
						dvConstraint = dvHelper.createFormulaListConstraint(
				                 "INDIRECT(CONCATENATE("+cellRange+",\"A\"))");
				         validation = dvHelper.createValidation(dvConstraint, addressList);
				         sheet.addValidationData(validation);
						break;

					default:
						setDropDownData(sheet, column.getListOptions(), columnNumber, drowpdownDataRowNumber);
						dvHelper = sheet.getDataValidationHelper();
						String s = getExcelColumnName(drowpdownDataRowNumber);
		                String dataCells = "$"+s+"$"+drowpdownDataRowNumber+":$"+s+"$"+(drowpdownDataRowNumber + column.getListOptions().size());
//		                System.out.println("At empty field ::   "+dataCells);
		                dvConstraint = dvHelper.createFormulaListConstraint(dataCells);
		                validation = dvHelper.createValidation(dvConstraint, addressList);
		                sheet.addValidationData(validation);
		                drowpdownDataRowNumber += 1  + (column.getListOptions() !=null ?  column.getListOptions().size() : 0);
						break;
					}
				} else {
					setDropDownData(sheet, column.getListOptions(), columnNumber, drowpdownDataRowNumber);
					dvHelper = sheet.getDataValidationHelper();
					String s = getExcelColumnName(drowpdownDataRowNumber);
		            String dataCells = "$"+s+"$"+drowpdownDataRowNumber+":$"+s+"$"+(drowpdownDataRowNumber + column.getListOptions().size());
		            //System.out.println("At empty field ::   "+dataCells);
		            dvConstraint = dvHelper.createFormulaListConstraint(dataCells);
		            validation = dvHelper.createValidation(dvConstraint, addressList);
		            sheet.addValidationData(validation);
		            drowpdownDataRowNumber += 1  + (column.getListOptions() !=null ?  column.getListOptions().size() : 0);
				}
			}

			if(column.getDataType() != null && column.getDataType().equalsIgnoreCase(UploadConstants.DATATYPE_STRING)) {
				cell.setCellStyle(styleString);
			} else if(column.getDataType() != null && column.getDataType().equalsIgnoreCase(UploadConstants.DATATYPE_DATE)) {
//				System.out.println("Setting date format in excel :::::::::::::::::::::::::");
				sheet.setDefaultColumnStyle(columnNumber, styleDate);
			}
			/* Added the change for the excel header to have background color --Ameya Sawant*/	
			if(columnName.contains("*")){
				cell.setCellStyle(cellColorStyle);
			}
			/* Added the change for the excel header to have background color --Ameya Sawant*/
			columnNumber++;

		}
		/*for (int i = 1; i < 800; i++) {
			row = sheet.createRow(i);
		    for (int j =0; j < columnDetails.size(); j++) {
		    	System.out.println(i +" :: "+j);
		    	cell = row.createCell(j);
		    	//cell.setCellValue("");
		    	cell.setCellStyle(editableStyle);
		    }
		}*/
	}

	private void setDependentDropDownDataForStandard(Sheet sheet, Integer branchId) {
//	     System.out.println("Callling master dao");
//	     System.out.println(masterDao.getStandardSelectionData(branchId));
	     List<List<String>> standardList = masterDao.getStandardSelectionData(branchId);
	     int rowCount = 9500;
	     int standardCellCount = 2000;
	     int divisionCellCount = 2000;
	     Cell standardCell = null;
	     Cell divisionCell = null;
	     Name nameTemp = null;
	     Row stateRow = sheet.createRow(rowCount);
	     Row districtRow = sheet.createRow(rowCount+1);
	     String standard = null, division = null;
	     String tempStandard = null;
	     int standardCellCount1= standardCellCount;
	     int divisionCellCount1 = divisionCellCount;
	     if(!standardList.isEmpty()){
		     for (List<String> record : standardList) {
//		    	 System.out.println("Size of standardList :: "+standardList);

		    	 standard = checkForStartsWithNumbers(record.get(0).trim());
		    	 division = checkForStartsWithNumbers(record.size() > 1 ? record.get(1).trim() : "");

		    	 standard = standard.replace(" ", "_");
		    	 division =  division.replace(" ", "_");
		    	 if(standard != null && !standard.equals(tempStandard)) {
		    		 if(tempStandard != null) {

			    		 nameTemp = sheet.getWorkbook().createName();
				         nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(divisionCellCount1)+"$"+(rowCount+2)+":$"+getExcelColumnName(divisionCellCount-1)+"$"+(rowCount+2));
				          nameTemp.setNameName(tempStandard);
				         //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));
				         divisionCellCount1= divisionCellCount;
		    		 }
		    		 standardCell = stateRow.createCell(standardCellCount++);
		    		 standardCell.setCellValue(standard);
			         tempStandard = standard;
		    	 }

		    	 divisionCell = districtRow.createCell(divisionCellCount++);
	    		 divisionCell.setCellValue(division);
		     }
		     nameTemp = sheet.getWorkbook().createName();
		     nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(divisionCellCount1)+"$"+(rowCount+2)+":$"+getExcelColumnName(divisionCellCount-1)+"$"+(rowCount+2));
		     nameTemp.setNameName(tempStandard);
		     //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));

		     nameTemp = sheet.getWorkbook().createName();
		     nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(standardCellCount1)+"$"+(rowCount+1)+":$"+getExcelColumnName(standardCellCount-1)+"$"+(rowCount+1));
		     nameTemp.setNameName("STANDARD");
		     //System.out.println(tempDivision+"   >>>>SSS "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+1)+"     >> "+standardCellCount1+"       >>>>>>>> "+standardCellCount+"   >>>>>>>>> "+getExcelColumnName(standardCellCount1)+"       >>>>>>>> "+getExcelColumnName(standardCellCount));
	     }
	}

	private void setDependentDropDownDataForStandardSubject(Sheet sheet, Integer branchId) {
	     System.out.println("Callling master dao");
//	     System.out.println(masterDao.getStandardSelectionData(branchId));
	     List<List<String>> standardDivisionList = masterDao.getStandardSelectionData(branchId);
	     List<List<String>> standardSubjectList = masterDao.getStandardBasedSubjectSelectionData(branchId);
	     int rowCount = 9500;
	     int standardCellCount = 2000;
	     int divisionCellCount = 2000;
	     int subjectCellCount = 2000;
	     Cell standardCell = null;
	     Cell divisionCell = null;
	     Cell subjectCell = null;
	     Name nameTemp = null;
	     Row firstRow = sheet.createRow(rowCount);
	     Row secondRow = sheet.createRow(rowCount+1);
	     Row thirdRow = sheet.createRow(rowCount+2);
	     String standard = null, division = null, subject = null;
	     String tempStandard = null;
	     int standardCellCount1= standardCellCount;
	     int divisionCellCount1 = divisionCellCount;
	     int subjectCellCount1 = subjectCellCount;

	     if(!standardDivisionList.isEmpty()){
	    	 for (List<String> record : standardDivisionList) {			
		    		 standard = checkForStartsWithNumbers(record.get(0).trim());
		    		 standard = standard.replace(" ", "_");
		    		 division = checkForStartsWithNumbers(record.size() > 1 ? record.get(1).trim() : ""); 
		    		 division =  division.replace(" ", "_");
		    		 if(standard != null && !standard.equals(tempStandard)) {
		    		 if(tempStandard != null) {

			    		 nameTemp = sheet.getWorkbook().createName();
				         nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(divisionCellCount1)+"$"+(rowCount+2)+":$"+getExcelColumnName(divisionCellCount-1)+"$"+(rowCount+2));
				          nameTemp.setNameName(tempStandard);
				         //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));
				         divisionCellCount1= divisionCellCount;
		    		 }
		    		 standardCell = firstRow.createCell(standardCellCount++);
		    		 standardCell.setCellValue(standard);
			         tempStandard = standard;
		    	 }

		    	 divisionCell = secondRow.createCell(divisionCellCount++);
	    		 divisionCell.setCellValue(division);
		     }

		     nameTemp = sheet.getWorkbook().createName();
		     nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(divisionCellCount1)+"$"+(rowCount+2)+":$"+getExcelColumnName(divisionCellCount-1)+"$"+(rowCount+2));
		     nameTemp.setNameName(tempStandard);
		     //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));

		     nameTemp = sheet.getWorkbook().createName();
		     nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(standardCellCount1)+"$"+(rowCount+1)+":$"+getExcelColumnName(standardCellCount-1)+"$"+(rowCount+1));
		     nameTemp.setNameName("STANDARD");
		     //System.out.println(tempDivision+"   >>>>SSS "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+1)+"     >> "+standardCellCount1+"       >>>>>>>> "+standardCellCount+"   >>>>>>>>> "+getExcelColumnName(standardCellCount1)+"       >>>>>>>> "+getExcelColumnName(standardCellCount));

	     }
	     tempStandard = null;

	     if(!standardSubjectList.isEmpty()){
	    	 for (List<String> record : standardSubjectList) {

		    		 standard = checkForStartsWithNumbers(record.get(0).trim());
		    		 standard = standard.replace(" ", "_");
		    		 division = checkForStartsWithNumbers(record.size() > 1 ? record.get(1).trim() : ""); 
		    		 subject =  division.replace(" ", "_");
	    		 
		    		 if(standard != null && !standard.equals(tempStandard)) {
		    		 if(tempStandard != null) { 
		    			 nameTemp = sheet.getWorkbook().createName();
		    			 nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(subjectCellCount1)+"$"+(rowCount+3)+":$"+getExcelColumnName(subjectCellCount-1)+"$"+(rowCount+3));
		    			 nameTemp.setNameName(tempStandard+"A");
		    			 //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));
			         
				          subjectCellCount1= subjectCellCount;
		    		 }
			         tempStandard = standard;
		    	 }
		         
		    	 subjectCell = thirdRow.createCell(subjectCellCount++);
		    	 subjectCell.setCellValue(subject);
		     }
		     nameTemp = sheet.getWorkbook().createName();
			 nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(subjectCellCount1)+"$"+(rowCount+3)+":$"+getExcelColumnName(subjectCellCount-1)+"$"+(rowCount+3));
			 nameTemp.setNameName(tempStandard+"A");
	     }
	     //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));
	     
	}
	
	private void setDependentDropDownDataForAddress(Sheet sheet) {
	     List<List<String>> addressList = masterDao.getAddressSelectionData();
	     
	     int rowCount = 10000;
	     int stateCellCount = 4000;
	     int districtCellCount = 4000;
	     int talukaCellCount = 4000;
	     Cell stateCell = null;
	     Cell districtCell = null;
	     Cell talukaCell = null;
	     Name nameTemp = null;
	     Row stateRow = sheet.createRow(rowCount);
	     Row districtRow = sheet.createRow(rowCount+1);
	     Row talukeRow = sheet.createRow(rowCount+2);
	     String state = null, district = null, taluka = null;
	     String tempState = null, tempDistrict = null;
	     int stateCellCount1= stateCellCount;
	     int districtCellCount1 = districtCellCount;
	     int talukaCellCount1= talukaCellCount;
	     for (List<String> record : addressList) {
	    	 
	    	 state = record.get(0).trim().replace(" ", "_");
	    	 district =  record.get(1).trim().replace(" ", "_");
	    	 taluka = record.get(2).trim().replace(" ", "_");
	    	 if(state != null && !state.equals(tempState)) {
	    		 if(tempState != null) {
		    		 nameTemp = sheet.getWorkbook().createName();
			         nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(districtCellCount1)+"$"+(rowCount+2)+":$"+getExcelColumnName(districtCellCount-1)+"$"+(rowCount+2));
			         nameTemp.setNameName(tempState);
			         //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));
		    		 //System.out.println("in state != null && !state.equals(tempState) with this tempstate"+tempState);
			         districtCellCount1= districtCellCount;
	    		 }
	    		 stateCell = stateRow.createCell(stateCellCount++);
	    		 stateCell.setCellValue(state);
		         tempState = state;
	    	 }
	         
	    	 if(district != null && !district.equals(tempDistrict)) {
	    		 if(tempDistrict != null) {
		    		 nameTemp = sheet.getWorkbook().createName();
			         nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(talukaCellCount1)+"$"+(rowCount+3)+":$"+getExcelColumnName(talukaCellCount-1)+"$"+(rowCount+3));
			         nameTemp.setNameName(tempDistrict);
			         //System.out.println(tempDistrict+"   >>>>TTTT "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+3)+"     >> "+talukaCellCount1+"       >>>>>>>> "+talukaCellCount+"   >>>>>>>>> "+getExcelColumnName(talukaCellCount1)+"       >>>>>>>> "+getExcelColumnName(talukaCellCount));
			         //System.out.println("in district != null && !district.equals(tempDistrict) with this tempstate"+tempDistrict);
			         talukaCellCount1= talukaCellCount;
	    		 }
	    		 districtCell = districtRow.createCell(districtCellCount++);
	    		 districtCell.setCellValue(district);
		         tempDistrict = district;
		         
	    	 }
	         
	    	 talukaCell = talukeRow.createCell(talukaCellCount++);
	    	 talukaCell.setCellValue(taluka);
	     }
	     nameTemp = sheet.getWorkbook().createName();
	     nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(districtCellCount1)+"$"+(rowCount+2)+":$"+getExcelColumnName(districtCellCount-1)+"$"+(rowCount+2));
	     nameTemp.setNameName(tempState);
	     //System.out.println(tempState+"   >>>>DDDD "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+2)+"     >> "+districtCellCount1+"       >>>>>>>> "+districtCellCount+"   >>>>>>>>> "+getExcelColumnName(districtCellCount1)+"       >>>>>>>> "+getExcelColumnName(districtCellCount));
	     
		 nameTemp = sheet.getWorkbook().createName();
	     nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(talukaCellCount1)+"$"+(rowCount+3)+":$"+getExcelColumnName(talukaCellCount-1)+"$"+(rowCount+3));
	     nameTemp.setNameName(tempDistrict);
	     //System.out.println(tempDistrict+"   >>>>TTTT "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+3)+"     >> "+talukaCellCount1+"       >>>>>>>> "+talukaCellCount+"   >>>>>>>>> "+getExcelColumnName(talukaCellCount1)+"       >>>>>>>> "+getExcelColumnName(talukaCellCount));
	     
	     nameTemp = sheet.getWorkbook().createName();
	     nameTemp.setRefersToFormula("LinkedValidations!$"+getExcelColumnName(stateCellCount1)+"$"+(rowCount+1)+":$"+getExcelColumnName(stateCellCount-1)+"$"+(rowCount+1));
	     nameTemp.setNameName("ADDRESS");
	     //System.out.println(tempDistrict+"   >>>>SSS "+sheet.getWorkbook().getNumberOfNames()+"   >>>>>>>>> "+(rowCount+1)+"     >> "+stateCellCount1+"       >>>>>>>> "+stateCellCount+"   >>>>>>>>> "+getExcelColumnName(stateCellCount1)+"       >>>>>>>> "+getExcelColumnName(stateCellCount));
	     
	     
	}

	public byte[] getExcelTemplateFile() throws IOException{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
		    workbook.write(bos);
		} finally {
		    bos.close();
		}
		byte[] bytes = bos.toByteArray();

		return bytes;
	}

    private void setDropDownData(Sheet dataSheet, List<String> data, int counter, int rowNumber) {
    	Row row = null;
        Cell cell = null;
        int j=0;
        for(int i =rowNumber; i < data.size()+rowNumber; i++) {
        	row = dataSheet.createRow(i);
        	cell = row.createCell(rowNumber);
        	cell.setCellValue(data.get(j));
        	//System.out.println("ROW :: "+ i +"          column :: "+rowNumber +"             -"+data.get(j));
        	j++;
        }

        String s = getExcelColumnName(rowNumber);
        String dataCells = "$"+s+"$"+rowNumber+":$"+s+"$"+(rowNumber + data.size());
        //System.out.println("At datalist field ::   "+dataCells);

        // return rowNumber + data.size();
    }

    private static String getExcelColumnName(int number) {
        final StringBuilder sb = new StringBuilder();

        int num = number;
        while (num >=  0) {
            int numChar = (num % 26)  + 65;
            sb.append((char)numChar);
            num = (num  / 26) - 1;
        }
        return sb.reverse().toString();
    }
 
    private List<String> getDropDownData(String dataFor) {
    	List<String> dataList = new ArrayList<String>();
    	List<Master> masterList = null;
    	switch (dataFor) {
		case UploadConstants.STATE:
			masterList = masterDao.getAllState();
			break;
		case UploadConstants.DISTRICT:
			masterList = masterDao.getAllDistrict();
			break;

		case UploadConstants.TALUKA:
			masterList = masterDao.getAllTaluka();
			break;
		default:
		}
    	
    	for (Master master : masterList) {
			dataList.add(master.getName());
		}
    	return  dataList;
    }

    public List<ExcelColumnDetails> getColumnDetails(String workbookName, Integer branchId) {
    	
    	List<ExcelColumnDetails> columnDetails = new ArrayList<ExcelColumnDetails>();
    	switch (workbookName) {
		case UploadConstants.BRANCH_TEMPLATE:
			columnDetails = getColumnDetailsForBranch();
			break;

		case UploadConstants.FEES_TEMPLATE:
			columnDetails = TemplateGenerator.FEES.getColumnDetails(branchId);
			break;

		case UploadConstants.HOLIDAY_TEMPLATE:
			
			break;
		
		case UploadConstants.INSTITUTE_TEMPLATE:
			
			break;
			
		case UploadConstants.TEACHER_TEMPLATE:
				
			break;
		
			
		default:
			break;
		}
    	return columnDetails;
    	
    }
    
    
    
    private List<ExcelColumnDetails> getColumnDetailsForBranch() {
List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
    	
    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Name");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Address");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Area");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("City");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Pincode");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Contact Number");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Email Address");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Website URL");
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("State");
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDropDownData(UploadConstants.STATE));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("District");
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDropDownData(UploadConstants.DISTRICT));
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName("Taluka");
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDropDownData(UploadConstants.TALUKA));
    	columnDetailsList.add(columnDetails);
    	
    	return columnDetailsList;
    }

    String checkForStartsWithNumbers(String name){															//Replace starting with number
		for(int i = 0; i < numbers.length ; i++){
			name = (name.startsWith(numbers[i]) ? "__" + name : name);
		}
		return name;
	}
}
