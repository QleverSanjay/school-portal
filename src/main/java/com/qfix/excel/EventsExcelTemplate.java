package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EventsExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{

	@Autowired
	ExcelTemplateGenerator generator;

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
		
		List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_NAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_DESCRIPTION_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_START_DATE_LABEL + UploadConstants.DATE_FORMAT_LABEL + UploadConstants.STAR_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_START_TIME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_END_DATE_LABEL + UploadConstants.DATE_FORMAT_LABEL + UploadConstants.STAR_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_END_TIME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.RECURRING_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_REMINDER_TIME_IN_MINUTES_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_VENUE_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_SUMMARY_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STANDARD_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getStandards(branchId));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.DIVISION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDivisions(branchId));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_EVENTS_FOR_STUDENTS_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_EVENTS_FOR_PARENTS_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_EVENTS_FOR_TEACHERS_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_EVENTS_FOR_VENDORS_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_FREQUENCY_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getFrequencyList(false));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_BY_DAY_ONLY_IF_FREQUENCY_IS_WEEKLY_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.EVENT_BY_DATE_ONLY_IF_FREQUENCY_IS_MONTHLY_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	return columnDetailsList;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{
		List<String> dependentFields = new ArrayList<>();
		dependentFields.add(UploadConstants.DEPENDENT_STANDARD);
		generator.prepareFile("", excelFileName, getColumnDetails(filePath, branchId), dependentFields, branchId, false, null);

		return generator.getExcelTemplateFile();
	}
	
	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		return null;
	}

	@Override
	public void setToken(String token) {
		// TODO Auto-generated method stub
		
	}
}
