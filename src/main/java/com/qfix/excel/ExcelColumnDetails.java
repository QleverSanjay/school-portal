package com.qfix.excel;

import java.util.List;

public class ExcelColumnDetails {
	
	private String columnName;
	private boolean hideColumn;
	
	private boolean selectFromList;
	
	private List<String> listOptions;
	
	private String dataType;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public boolean isSelectFromList() {
		return selectFromList;
	}

	public void setSelectFromList(boolean selectFromList) {
		this.selectFromList = selectFromList;
	}


	public List<String> getListOptions() {
		return listOptions;
	}

	public void setListOptions(List<String> listOptions) {
		this.listOptions = listOptions;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public boolean isHideColumn() {
		return hideColumn;
	}

	public void setHideColumn(boolean hideColumn) {
		this.hideColumn = hideColumn;
	}
}
