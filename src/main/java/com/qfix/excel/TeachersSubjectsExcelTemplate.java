package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TeachersSubjectsExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{
	@Autowired
	ExcelTemplateGenerator generator;
	
	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
		List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_NAME_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getTeachers(branchId));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STANDARD_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getStandards(branchId));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.DIVISION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDivisions(branchId));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.SUBJECT_NAME_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getSubjects(branchId));
    	columnDetailsList.add(columnDetails);
    	
    	
    	return columnDetailsList;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{
		List<String> dependentFields = new ArrayList<>();
		dependentFields.add(UploadConstants.DEPENDENT_SUBJECT);
		generator.prepareFile("", excelFileName, getColumnDetails(filePath, branchId), dependentFields, branchId, false, null);

		return generator.getExcelTemplateFile();
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		return null;
	}

	@Override
	public void setToken(String token) {
		// TODO Auto-generated method stub
		
	}
}
