package com.qfix.excel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExcelTemplateFactory {
	@Autowired
	FeesExcelTemplate feesExcelTemplate;
	
	@Autowired
	StudentsExcelTemplate studentsExcelTemplate;
	
	@Autowired
	EventsExcelTemplate eventsExcelTemplate;
	
	@Autowired
	StandardsSubjectsExcelTemplate standardsSubjectsExcelTemplate;
	
	@Autowired
	TimetablesExcelTemplate timetablesExcelTemplate;
	
	@Autowired
	TeachersSubjectsExcelTemplate teachersSubjectsExcelTemplate;
	
	@Autowired
	BranchesExcelTemplate branchesExcelTemplate;
	
	@Autowired 
	HolidaysExcelTemplate holidaysExcelTemplate;

	@Autowired
	InstitutesExcelTemplate institutesExcelTemplate;
	
	@Autowired
	TeachersExcelTemplate teachersExcelTemplate;

	@Autowired
	SubjectsExcelTemplate subjectsExcelTemplate;

	public ExcelTemplate getExcelTemplate(String templateName) {
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<"+templateName);
		ExcelTemplate template = null;
		switch (templateName) {
		case UploadConstants.BRANCH_TEMPLATE:
			template = branchesExcelTemplate;
			break;

		case UploadConstants.FEES_TEMPLATE:
			template = feesExcelTemplate;
			break;
		
		case UploadConstants.HOLIDAY_TEMPLATE:
			template = holidaysExcelTemplate;
			break;
		
		case UploadConstants.INSTITUTE_TEMPLATE:
			template = institutesExcelTemplate;
			break;
			
		case UploadConstants.TEACHER_TEMPLATE:
			template = teachersExcelTemplate;	
			break;
		
		case UploadConstants.STUDENT_TEMPLATE:
			template = studentsExcelTemplate;	
			break;
		
		case UploadConstants.EVENT_TEMPLATE:
			template = eventsExcelTemplate;	
			break;
		
		case UploadConstants.SUBJECT_TEMPLATE:
			template = subjectsExcelTemplate;	
			break;
		
		case UploadConstants.STANDARD_SUBJECT_TEMPLATE:
			template = standardsSubjectsExcelTemplate;	
			break;
		
		case UploadConstants.TIMETABLE_TEMPLATE:
			template = timetablesExcelTemplate;	
			break;
		
		case UploadConstants.TEACHER_SUBJECT_TEMPLATE:
			template = teachersSubjectsExcelTemplate;	
			break;
			
		default:
			break;
		}
		return template;
	}
	
}
