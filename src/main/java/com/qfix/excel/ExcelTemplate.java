package com.qfix.excel;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface ExcelTemplate {
	
	List<ExcelColumnDetails> getColumnDetails(String workbookName, Integer branchId) ;
	
	List<ExcelColumnDetails> getColumnDetails(String workbookName, Integer branchId, Integer standardId, Integer repeatCount) ;

	byte[] getExcelTemplateFile(String filePath, String excelFileName, Integer branchId) throws IOException;
	
	byte[] getExcelTemplateFile(String filePath, String excelFileName, Integer branchId, Integer standardId, Integer repeatCount) throws IOException;
	
	void setToken(String token);
}
