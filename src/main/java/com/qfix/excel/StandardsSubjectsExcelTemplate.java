package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StandardsSubjectsExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{


	@Autowired
	ExcelTemplateGenerator generator;

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
		List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STANDARDSUBJECT_STANDARD_NAME_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getStandards(branchId));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STANDARDSUBJECT_SUBJECT_NAME_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getSubjects(branchId));
    	columnDetailsList.add(columnDetails);
    	
    	
    	return columnDetailsList;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{
		generator.prepareFile("", excelFileName, getColumnDetails(filePath, branchId));
		
		return generator.getExcelTemplateFile();
	}
	
	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		return null;
	}

	@Override
	public void setToken(String token) {
		// TODO Auto-generated method stub
		
	}
}
