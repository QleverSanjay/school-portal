package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TimetablesExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{

	@Autowired
	ExcelTemplateGenerator generator;

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
    	ExcelColumnDetails columnDetails;
    	columnDetails = new ExcelColumnDetails();
    	/*columnDetails.setColumnName("");
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDays());
    	columnDetailsList.add(columnDetails);*/
    	List<String> subjectList = getSubjectsByStandards(branchId, standardId);
    	subjectList.add("BREAK");
    	for(int i=0; i < repeatCount; i++) {
    		
    		//Start Time Hour
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.TIME_TABLE_START_TIME_HOUR_LABEL + UploadConstants.STAR_LABEL);
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getHoursList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	//Start Time Minutes
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.TIME_TABLE_START_TIME_MINUTES_LABEL + UploadConstants.STAR_LABEL);
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getMinutesList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	//End Time Hour
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.TIME_TABLE_END_TIME_HOUR_LABEL + UploadConstants.STAR_LABEL);
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getHoursList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	//End Time Minutes
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.TIME_TABLE_END_TIME_MINUTES_LABEL + UploadConstants.STAR_LABEL);
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getMinutesList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	//Subject
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.TIME_TABLE_SUBJECT_LABEL + UploadConstants.STAR_LABEL);
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(subjectList);
	    	columnDetailsList.add(columnDetails);
	    
    	}
    	
    	return columnDetailsList;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{
		return null;
		
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
		return null;
	}	

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		generator.prepareFile(filePath, excelFileName, getColumnDetails(filePath, branchId, standardId, repeatCount), true);

		return generator.getExcelTemplateFile();
	}

	@Override
	public void setToken(String token) {
		// TODO Auto-generated method stub
		
	}
}
