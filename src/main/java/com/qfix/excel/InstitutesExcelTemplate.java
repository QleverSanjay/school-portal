package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InstitutesExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{
	

	@Autowired
	ExcelTemplateGenerator generator;

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
		List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_NAME_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_CODE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_ADMIN_FIRST_NAME_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_ADMIN_LAST_NAME_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_ADMIN_EMAIL_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_GENDER_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setListOptions(getGenderList());
    	columnDetails.setSelectFromList(true);
    	columnDetailsList.add(columnDetails);
    	

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_PRIMARY_CONTACT_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_CITY_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_PINCODE_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.INSTITUTE_STATUS_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);
    	
    	return columnDetailsList;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{
		generator.prepareFile("", excelFileName, getColumnDetails(filePath, branchId));

		return generator.getExcelTemplateFile();
	}
	
	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		return null;
	}

	@Override
	public void setToken(String token) {
		// TODO Auto-generated method stub
		
	}
}
