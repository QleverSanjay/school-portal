package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TeachersExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{
	@Autowired
	ExcelTemplateGenerator generator;
	
	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
		List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_FIRST_NAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_MIDDLE_NAME_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_LAST_NAME_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_BIRTH_DATE_LABEL + UploadConstants.DATE_FORMAT_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_EMAIL_ADDRESS_LABEL + UploadConstants.STAR_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_GENDER_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getGenderList());
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_PRIMARY_NUMBER_LABEL + UploadConstants.STAR_LABEL );
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_SECONDARY_NUMBER_LABEL);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_ADDRESS_LINE1_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_ADDRESS_AREA_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.STATE_LABEL );
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.STATE));
    	columnDetailsList.add(columnDetails);
    	
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.DISTRICT_LABEL );
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.DISTRICT));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TALUKA_CITY_LABEL );
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.TALUKA));
    	columnDetailsList.add(columnDetails);

    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_TOWN_VILLAGE_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_PINCODE_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.TEACHER_ACTIVE_LABEL );
    	columnDetails.setListOptions(getYesNoList());
    	columnDetails.setSelectFromList(true);
    	columnDetailsList.add(columnDetails);
    	
    	return columnDetailsList;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{
		List<String> dependentFields = new ArrayList<>();
		dependentFields.add(UploadConstants.DEPENDENT_ADDRESS);
		generator.prepareFile("", excelFileName, getColumnDetails(filePath, branchId), dependentFields, branchId, false, null);

		return generator.getExcelTemplateFile();
	}
	
	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		return null;
	}

	@Override
	public void setToken(String token) {
		// TODO Auto-generated method stub
		
	}
}
