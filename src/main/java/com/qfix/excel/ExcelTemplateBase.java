package com.qfix.excel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qfix.dao.DivisionInterfaceDao;
import com.qfix.dao.FeesInterfaceDao;
import com.qfix.dao.HeadInterfaceDao;
import com.qfix.dao.ISubjectDao;
import com.qfix.dao.StandardInterfaceDao;
import com.qfix.dao.TeacherInterfaceDao;
import com.qfix.daoimpl.FeesDao;
import com.qfix.daoimpl.MasterDao;
import com.qfix.model.Currency;
import com.qfix.model.Division;
import com.qfix.model.Head;
import com.qfix.model.Master;
import com.qfix.model.Standard;
import com.qfix.model.Subject;
import com.qfix.model.Teacher;

@Service
public class ExcelTemplateBase {

	@Autowired
	protected HeadInterfaceDao headDAO;
	@Autowired
	protected StandardInterfaceDao standardDAO;
	@Autowired
	protected DivisionInterfaceDao divisionDAO;
	@Autowired
	protected ISubjectDao subjectDAO;
	@Autowired
	protected TeacherInterfaceDao teacherDAO;

	@Autowired
	MasterDao masterDao;

	public String a;

	@Autowired
	protected FeesInterfaceDao feesDAO;

	// protected MasterDao masterDao;

	public List<String> getCurrencies() {
		List<Currency> currencyList = masterDao.getAllCurrencies();
		List<String> currencies = new ArrayList<String>();
		if(currencyList != null && currencyList.size() > 0){
			for (Currency currency : currencyList){
				currencies.add(currency.getCode());
			}
		}
		return currencies;
	}

	protected List<String> getFeeHeads(Integer branchId) {
		List<String> feeHeads = new ArrayList<String>();
		List<Head> heads = headDAO.getHeadByBranchForDashboard(branchId);
		for (Head head : heads) {
			feeHeads.add(head.getName());
		}

		return feeHeads;
	}

	protected List<String> getStandards(Integer branchId) {
		List<String> standards = new ArrayList<String>();
		try {
			List<Standard> standardList = standardDAO.getAll(branchId);
//			System.out.println("StandardList :::::\n" + new com.google.gson.Gson().toJson(standardList));
			for (Standard standard : standardList) {
				standards.add(standard.getDisplayName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return standards;
	}

	protected List<String> getTeachers(Integer branchId) {
		List<String> teachers = new ArrayList<String>();
		try {
			List<Teacher> teacherList = teacherDAO.getTeacherByBranchId(branchId);
			for (Teacher teacher : teacherList) {
				teachers.add(teacher.getFirstName() + " "
						+ teacher.getLastName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return teachers;
	}

	protected List<String> getDivisions(Integer branchId) {
		List<String> divisions = new ArrayList<String>();
		try {
			List<Division> divisionList = divisionDAO.getAll(branchId);
			for (Division division : divisionList) {
				divisions.add(division.getDisplayName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return divisions;
	}

	protected List<String> getProfessions() {
		List<String> professionList = new ArrayList<String>();
		try {
			List<Master> masterList = masterDao.getAllProfessions();
			for (Master master : masterList) {
				professionList.add(master.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("This is profession List :: " + professionList);
		return professionList;

	}

	protected List<String> getIncomes() {
		List<String> incomeList = new ArrayList<String>();
		try {
			List<Master> masterList = masterDao.getAllIncomes();
			for (Master master : masterList) {
				incomeList.add(master.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return incomeList;
	}

	protected List<String> getSubjects(Integer branchId) {
		List<String> subjects = new ArrayList<String>();
		try {
			List<Subject> subjectList = subjectDAO
					.getSubjectsByBranchId(branchId);
			for (Subject subject : subjectList) {
				subjects.add(subject.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return subjects;
	}

	protected List<String> getAllFeesCodes(Integer branchId) {
		List<String> feesCodesList = new ArrayList<String>();
		try {
			List<Master> feesCodeList = masterDao.getAllFeesCode(branchId);
			for (Master master : feesCodeList) {
				feesCodesList.add(master.getName());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Fees Codes :::::>>>" + feesCodesList);
		return feesCodesList;
	}

	protected List<String> getSubjectsByStandards(Integer branchId,
			Integer standardId) {
		List<String> subjects = new ArrayList<String>();
		try {
			List<Subject> subjectList = subjectDAO.getSubjectsByStandardId(
					branchId, standardId);
			for (Subject subject : subjectList) {
				subjects.add(subject.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return subjects;
	}

	protected static List<String> getYesNoList() {
		List<String> list = new ArrayList<String>();
		list.add("Y");
		list.add("N");
		return list;
	}

	protected static List<String> getHoursList() {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				list.add("0" + i);
			} else {
				list.add("" + i);
			}
		}
		return list;
	}

	protected static List<String> getMinutesList() {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < 60; i++) {
			if (i < 10) {
				list.add("0" + i);
			} else {
				list.add("" + i);
			}
		}
		return list;
	}

	protected static List<String> getDays() {
		List<String> list = new ArrayList<String>();
		list.add("Monday");
		list.add("Tuesday");
		list.add("Wednesday");
		list.add("Thursday");
		list.add("Friday");
		list.add("Saturday");
		list.add("Sunday");
		return list;
	}

	protected static List<String> getFrequencyList(boolean addHalfYearly) {
		List<String> list = new ArrayList<String>();
		list.add("Daily");
		list.add("Weekly");
		list.add("Monthly");
		if (addHalfYearly) {
			list.add("Half-Yearly");
		}
		list.add("Yearly");
		return list;
	}

	protected static List<String> getGenderList() {
		List<String> list = new ArrayList<String>();
		list.add("M");
		list.add("F");
		return list;
	}

	protected List<String> getMasterData(String name) {
		List<String> masterData = new ArrayList<String>();
		List<Master> masterList = new ArrayList<Master>();
		switch (name) {
		case UploadConstants.CASTE:
			masterList = masterDao.getAllCaste();
			break;
		case UploadConstants.RELIGION:
			masterList = masterDao.getAllReligion();
			break;
		case UploadConstants.STATE:
			masterList = masterDao.getAllState();
			break;
		case UploadConstants.DISTRICT:
			masterList = masterDao.getAllDistrict();
			break;
		case UploadConstants.TALUKA:
			masterList = masterDao.getAllTaluka();
			break;
		case UploadConstants.RELATION:
			masterList = masterDao.getRelations();
			break;
		default:
			break;
		}

		if (masterList.size() > 0) {
			for (Master master : masterList) {
				masterData.add(master.getName());
			}
		}

		return masterData;
	}

	public void setHeadDAO(HeadInterfaceDao headDAO) {
		this.headDAO = headDAO;
	}

	public void setStandardDAO(StandardInterfaceDao standardDAO) {
		this.standardDAO = standardDAO;
	}

	public void setDivisionDAO(DivisionInterfaceDao divisionDAO) {
		this.divisionDAO = divisionDAO;
	}

	public void setSubjectDAO(ISubjectDao subjectDAO) {
		this.subjectDAO = subjectDAO;
	}

	public void setTeacherDAO(TeacherInterfaceDao teacherDAO) {
		this.teacherDAO = teacherDAO;
	}

	public void setFeesDAO(FeesDao feesDAO) {
		this.feesDAO = feesDAO;
	}

}
