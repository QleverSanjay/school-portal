package com.qfix.excel;

public interface UploadConstants {

	public String STATE = "State";

	public String DISTRICT = "District";

	public String TALUKA = "Taluka";

	public String CASTE = "Caste";

	public String STANDARD = "Standard";

	public String DATE_FORMAT_LABEL = "(DD/MM/YYYY)";

	public String STAR_LABEL = "*";

	public String BRANCH_TEMPLATE = "eduqfix-upload-branch-template.xlsx";
	public String ENROLLMENT_TEMPLATE = "eduqfix-upload-enrollment-template.xlsx";
	public String FEES_TEMPLATE = "eduqfix-upload-fees-template.xlsx";
	public String HOLIDAY_TEMPLATE = "eduqfix-upload-holiday-template.xlsx";
	public String INSTITUTE_TEMPLATE = "eduqfix-upload-institute-template.xlsx";
	public String STANDARD_SUBJECT_TEMPLATE = "eduqfix-upload-standardSubject-template.xlsx";
	public String STUDENT_TEMPLATE = "eduqfix-upload-student-template.xlsx";
	public String SUBJECT_TEMPLATE = "eduqfix-upload-subject-template.xlsx";
	public String TEACHER_TEMPLATE = "eduqfix-upload-teacher-template.xlsx";
	public String TEACHER_SUBJECT_TEMPLATE = "eduqfix-upload-teacher-subject-template.xlsx";
	public String EVENT_TEMPLATE = "eduqfix-upload-event-template.xlsx";
	public String TIMETABLE_TEMPLATE = "eduqfix-upload-timetable-template.xlsx";

	public String DATATYPE_STRING = "DATATYPE_STRING";
	public String DATATYPE_DATE = "DATATYPE_DATE";

	public String RELIGION = "Religion";

	// For dendent fields
	public String DEPENDENT_ADDRESS = "ADDRESS";
	public String DEPENDENT_STANDARD = "STANDARD";
	public String DEPENDENT_SUBJECT = "SUBJECT";

	// For Institute
	public String INSTITUTE_NAME_LABEL = "Name";
	public String INSTITUTE_CODE_LABEL = "Code (Code must be 4 to 16 letters and only alphabets and numbers are allowed)";
	public String INSTITUTE_ADMIN_FIRST_NAME_LABEL = "Admin Firstname";
	public String INSTITUTE_ADMIN_LAST_NAME_LABEL = "Admin Lastname";
	public String INSTITUTE_ADMIN_EMAIL_LABEL = "Admin Email";
	public String INSTITUTE_GENDER_LABEL = "Gender";
	public String INSTITUTE_PRIMARY_CONTACT_LABEL = "Primary Contact";
	public String INSTITUTE_CITY_LABEL = "City";
	public String INSTITUTE_PINCODE_LABEL = "Pincode";
	public String INSTITUTE_STATUS_LABEL = "Status";

	// For Branch

	public String BRANCH_NAME_LABEL = "Name";
	public String BRANCH_ADDRESS_LINE1_LABEL = "Address Line 1";
	public String BRANCH_AREA_LABEL = "Area";
	public String BRANCH_CITY_LABEL = "City";
	public String BRANCH_PINCODE_LABEL = "Pincode";
	public String BRANCH_CONTACT_NUMBER_LABEL = "Contact Number";
	public String BRANCH_EMAIL_ADDRESS_LABEL = "Email Address";
	public String BRANCH_WEBSITE_URL_LABEL = "Website URL";
	public String BRANCH_STATE_LABEL = "State";
	public String BRNACH_DISTRICT_LABEL = "District";
	public String BRANCH_TALUKA_CITY_LABEL = "Taluka/City";
	public String BRANCH_STATUS_LABEL = "Status";

	// For Holiday

	public String HOLIDAY_TITLE_LABEL = "Title";
	public String HOLIDAY_DESCRIPTION_LABEL = "Description";
	public String HOLIDAY__FROM_DATE_LABEL = "From Date";
	public String HOLIDAY_TO_DATE_LABEL = "To Date";

	// For StandardsSubjects
	public String STANDARDSUBJECT_STANDARD_NAME_LABEL = "Standard Name";
	public String STANDARDSUBJECT_SUBJECT_NAME_LABEL = "Subject Name";

	// For Subject

	public String SUBJECT_TITLE_LABEL = "Title";
	public String SUBJECT_CODE_LABEL = "Code";
	public String SUBJECT_THEORY_MARKS_LABEL = "Theory Marks";
	public String SUBJECT_PRACTICAL_MARKS_LABEL = "Practical Marks";

	// For Fees

	public String FEES_HEAD_LABEL = "Head";
	public String FEES_DESCRIPTION_LABEL = "Description";
	public String FEES_AMOUNT_LABEL = "Amount";
	public String FEES_ALLOW_REPEAT_ENTRIES_LABEL = "Repeat this fees";
	public String FEES_CURRENCY_LABEL = "Currency";
	public String FEES_DUE_DATE_LABEL = "Due Date";
	public String FEES_START_DATE_LABEL = "Start Date";
	public String FEES_END_DATE_LABEL = "End Date";
	public String FEES_CASTE_LABEL = "Caste";
	public String FEES_STANDARD_LABEL = "Standard/Course";
	public String FEES_DIVISION_LABEL = "Division";
	public String FEES_EMAIL_REMINDER_LABEL = "Email Reminder";
	public String FEES_SMS_REMINDER_LABEL = "SMS Reminder";
	public String FEES_MOBILE_NOTIFICATION_LABEL = "Mobile Notification";
	public String FEES_FREQUENCY_LABEL = "Frequency";
	public String FEES_LATE_PAYMENT_CHARGES_LABEL = "Late Payment Charges";
	public String FEES_STUDENT_NAME_LABEL = "Student Name";
	public String FEES_ROLL_NO_LABEL = "Roll No";
	public String FEES_REGISTRATION_CODE_LABEL = "Registration Code";
	public String FEES_IS_SPLIT_PAYMENT_LABEL = "Is Split Payment";
	public String FEES_FEES_SCHEME_CODE_LABEL = "Fees Scheme Code";
	public String FEES_FEES_SPLI_AMOUNT_LABEL = "Fees Split Amount (If Split Payment is yes)";
	public String FEES_FEES_SEED_SCHEME_CODE__LABEL = "Fees Seed Scheme Code(If Split Payment is yes)";
	public String FEES_FEES_SEED_SPLIT_AMOUNT_LABEL = "Fees Seed Split Amount(If Split Payment is yes)";
	public String FEES_IS_PARTIAL_PAYMENT_ALLOWED_LABEL = "Is Partial Payment Allowed";
	public String FEES_FEES_CODE_LABEL = "Fees Code";
	public String REMINDER_START_DATE = "Reminder Start Date (DD/MM/YYYY)";

	// For student
	public String USERID_LABEL = "Student User Id";
	public String FIRST_NAME_LABEL = "First Name";
	public String MIDDLE_NAME = "Middle Name";
	public String SURNAME_LABEL = "Surname";
	public String GENDER_LABEL = "Gender";
	public String BIRTH_DATE_LABEL = "Date of Birth (DD/MM/YYYY)";
	public String EMAIL_ADDRESS_LABEL = "E-Mail Address";
	public String MOBILE_NUMBER_LABEL = "Mobile No";
	public String ADDRESS_LINE_1_LABEL = "Address Line 1";
	public String AREA_LABEL = "Area";
	public String STATE_LABEL = "State";
	public String DISTRICT_LABEL = "District";
	public String TALUKA_CITY_LABEL = "Taluka/City";
	public String TOWN_VILLAGE_LABEL = "Town/Village";
	public String PINCODE_LABEL = "Pincode";
	public String RELIGION_LABEL = "Religion";
	public String CASTE_LABEL = "Caste";
	public String STANDARD_LABEL = "Standard/Course";
	public String DIVISION_LABEL = "Division";
	public String ROLL_NUMBER_LABEL = "Roll Number";
	public String STUDENT_REGISTRATION_CODE_LABEL = "Student Registration Code";
	public String ACTIVE_LABEL = "Active";
	public String FATHER_PROFESSION_LABEL = "Father's Profession";
	public String FATHER_INCOME_LABEL = "Father's Income";
	public String MOTHER_PROFESSION_LABEL = "Mother's Profession";
	public String MOTHER_INCOME_LABEL = "Mother's Income";
	public String GUARDIAN_PROFESSION_LABEL = "Guardian's Profession";
	public String GUARDIAN_INCOME_LABEL = "Guardian's Income";
	public String STUDENT_CREATE_LOGIN_LABEL = "Create Student Login";
	public String STUDENT_ID_LABEL = "Student Id";
	public String STUDENT_ACTION_LABEL = "Student Action";

	public String FATHER_FIRST_NAME_LABEL = "Father's First Name";
	public String FATHER_MIDDLE_NAME_LABEL = "Father's Middle Name";
	public String FATHER_SURNAME_LABEL = "Father's Surname";
	public String FATHER_USERID_LABEL = "Father User Id";
	public String FATHER_BIRTH_DATE_LABEL = "Father's Date of Birth (DD/MM/YYYY)";
	public String FATHER_EMAIL_ADDRESS_LABEL = "Father's E-Mail Address";
	public String FATHER_PRIMARY_MOBILE_NUMBER_LABEL = "Father's Primary Mobile No";
	public String FATHER_SECONDARY_MOBILE_NUMBER_LABEL = "Father's Secondary Mobile No";
	public String FATHER_ADDRESS_LINE_1_LABEL = "Father's Address Line 1";
	public String FATHER_AREA_LABEL = "Father's Area";
	public String FATHER_STATE_LABEL = "Father's State";
	public String FATHER_DISTRICT_LABEL = "Father's District";
	public String FATHER_TALUKA_CITY_LABEL = "Father's Taluka/City";
	public String FATHER_TOWN_VILLAGE_LABEL = "Father's Town/Village";
	public String FATHER_PINCODE_LABEL = "Father's Pincode";
	public String FATHER_CREATE_LOGIN_LABEL = "Create Father Login";
	public String FATHER_ID_LABEL = "Father Id";
	public String FATHER_ACTION_LABEL = "Father Action";

	public String MOTHER_FIRST_NAME_LABEL = "Mother's First Name";
	public String MOTHER_MIDDLE_NAME_LABEL = "Mother's Middle Name";
	public String MOTHER_SURNAME_LABEL = "Mother's Surname";
	public String MOTHER_USERID_LABEL = "Mother User Id";
	public String MOTHER_BIRTH_DATE_LABEL = "Mother's Date of Birth (DD/MM/YYYY)";
	public String MOTHER_EMAIL_ADDRESS_LABEL = "Mother's E-Mail Address";
	public String MOTHER_PRIMARY_MOBILE_NUMBER_LABEL = "Mother's Primary Mobile No";
	public String MOTHER_SECONDARY_MOBILE_NUMBER_LABEL = "Mother's Secondary Mobile No";
	public String MOTHER_ADDRESS_LINE_1_LABEL = "Mother's Address Line 1";
	public String MOTHER_AREA_LABEL = "Mother's Area";
	public String MOTHER_STATE_LABEL = "Mother's State";
	public String MOTHER_DISTRICT_LABEL = "Mother's District";
	public String MOTHER_TALUKA_CITY_LABEL = "Mother's Taluka/City";
	public String MOTHER_TOWN_VILLAGE_LABEL = "Mother's Town/Village";
	public String MOTHER_PINCODE_LABEL = "Mother's Pincode";
	public String MOTHER_CREATE_LOGIN_LABEL = "Create Mother Login";
	public String MOTHER_ID_LABEL = "Mother Id";
	public String MOTHER_ACTION_LABEL = "Mother Action";

	public String GUARDIAN_FIRST_NAME_LABEL = "Guardian's First Name";
	public String GUARDIAN_MIDDLE_NAME_LABEL = "Guardian's Middle Name";
	public String GUARDIAN_SURNAME_LABEL = "Guardian's Surname";
	public String GUARDIAN_USERID_LABEL = "Guardian User Id";
	public String GUARDIAN_BIRTH_DATE_LABEL = "Guardian's Date of Birth (DD/MM/YYYY)";
	public String GUARDIAN_EMAIL_ADDRESS_LABEL = "Guardian's E-Mail Address";
	public String GUARDIAN_PRIMARY_MOBILE_NUMBER_LABEL = "Guardian's Primary Mobile No";
	public String GUARDIAN_SECONDARY_MOBILE_NUMBER_LABEL = "Guardian's Secondary Mobile No";
	public String GUARDIAN_ADDRESS_LINE_1_LABEL = "Guardian's Address Line 1";
	public String GUARDIAN_AREA_LABEL = "Guardian's Area";
	public String GUARDIAN_STATE_LABEL = "Guardian's State";
	public String GUARDIAN_DISTRICT_LABEL = "Guardian's District";
	public String GUARDIAN_TALUKA_CITY_LABEL = "Guardian's Taluka/City";
	public String GUARDIAN_TOWN_VILLAGE_LABEL = "Guardian's Town/Village";
	public String GUARDIAN_PINCODE_LABEL = "Guardian's Pincode";
	public String GUARDIAN_RELATION_WITH_STUDENT = "Guardian's Relation with Student";
	public String GUARDIAN_GENDER_LABEL = "Guardian's Gender";
	public String GUARDIAN_CREATE_LOGIN_LABEL = "Create Guardian Login";
	public String GUARDIAN_ID_LABEL = "Guardian Id";
	public String GUARDIAN_ACTION_LABEL = "Guardian Action";

	public String STUDENT_FEES_CODE_LABEL = "Fees Codes";
	/*added by pitabas on 09_12_2017*/
	public String STUDENT_FEES_CODE_LABEL1 = "Fees Codes1";
	public String STUDENT_FEES_CODE_LABEL2 = "Fees Codes2";
	public String STUDENT_FEES_CODE_LABEL3 = "Fees Codes3";
	public String STUDENT_FEES_CODE_LABEL4 = "Fees Codes4";
	public String STUDENT_FEES_CODE_LABEL5 = "Fees Codes5";
	/*added by pitabas on 09_12_2017*/
	public String RELATION = "Relation";

	// For Teacher

	public String TEACHER_FIRST_NAME_LABEL = "First Name";
	public String TEACHER_MIDDLE_NAME_LABEL = "Middle Name";
	public String TEACHER_LAST_NAME_LABEL = "Last Name";
	public String TEACHER_BIRTH_DATE_LABEL = "Birth Date";
	public String TEACHER_EMAIL_ADDRESS_LABEL = "Email Address";
	public String TEACHER_GENDER_LABEL = "Gender";
	public String TEACHER_PRIMARY_NUMBER_LABEL = "Primary Number";
	public String TEACHER_SECONDARY_NUMBER_LABEL = "Secondary Number";
	public String TEACHER_ADDRESS_LINE1_LABEL = "Address Line 1";
	public String TEACHER_ADDRESS_AREA_LABEL = "Address Area";
	public String TEACHER_STATE_LABEL = "State";
	public String TEACHER_DISTRICT_LABEL = "District";
	public String TEACHER_TALUKA_CITY_LABEL = "Taluka/City";
	public String TEACHER_TOWN_VILLAGE_LABEL = "Town/Village";
	public String TEACHER_PINCODE_LABEL = "Pincode";
	public String TEACHER_ACTIVE_LABEL = "Active";

	// For Teacher subject
	public String TEACHER_NAME_LABEL = "Teacher";
	public String SUBJECT_NAME_LABEL = "Subject";

	// For Event

	public String EVENT_NAME_LABEL = "Name";
	public String EVENT_DESCRIPTION_LABEL = "Description";
	public String EVENT_START_DATE_LABEL = "Start Date";
	public String EVENT_START_TIME_LABEL = "Start Time";
	public String EVENT_END_DATE_LABEL = "End Date";
	public String EVENT_END_TIME_LABEL = "End Time";
	public String RECURRING_LABEL = "Recurring";
	public String EVENT_REMINDER_TIME_IN_MINUTES_LABEL = "Reminder Time(in minutes)";
	public String EVENT_VENUE_LABEL = "Venue";
	public String EVENT_STANDARD_LABEL = "Standard/Course";
	public String EVENT_DIVISION_LABEL = "Division";
	public String EVENT_EVENTS_FOR_STUDENTS_LABEL = "Event for Students";
	public String EVENT_EVENTS_FOR_PARENTS_LABEL = "Event for Parents";
	public String EVENT_EVENTS_FOR_TEACHERS_LABEL = "Event for Teachers";
	public String EVENT_EVENTS_FOR_VENDORS_LABEL = "Event for Vendors";
	public String EVENT_FREQUENCY_LABEL = "Frequency";
	public String EVENT_BY_DAY_ONLY_IF_FREQUENCY_IS_WEEKLY_LABEL = "By Day (Only if Frequency is Weekly)";
	public String EVENT_BY_DATE_ONLY_IF_FREQUENCY_IS_MONTHLY_LABEL = "By Date (Only if Frequency is Monthly)";
	public String EVENT_SUMMARY_LABEL = "Summary";

	// For TimeTable
	public String TIME_TABLE_START_TIME_HOUR_LABEL = "Start Time Hour";
	public String TIME_TABLE_START_TIME_MINUTES_LABEL = "Start Time Minutes";
	public String TIME_TABLE_END_TIME_HOUR_LABEL = "End Time Hour";
	public String TIME_TABLE_END_TIME_MINUTES_LABEL = "End Time Minutes";
	public String TIME_TABLE_SUBJECT_LABEL = "Subject";

	public String DISPLAY_TEMPLATE_NAME = "Display Template Name";
	public String DISPLAY_TEMPLATE_DISCRIPTION = "Display Template Description";

	public String RECURRING_FREQUENCY_LABEL = "Recurring Frequency";

	public String RECURRING_BY_DAY_ONLY_IF_FREQUENCY_IS_DAILY_LABEL = "Fees Days (Only if recurring Frequency is Daily) ";

	public String FEES_TYPE_LABEL = "Fees Type";

	public String FEES_DISPLAY_TEMPLATE_LABEL = "Display Template (Only if Fees Type is Group Fee)";

	public String FEES_ID = "Fees Id";

	public String FEES_ACTION = "Fees Action";


}
