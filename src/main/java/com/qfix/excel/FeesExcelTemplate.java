package com.qfix.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qfix.dao.IDisplayTemplateDao;
import com.qfix.model.DisplayTemplate;
import com.qfix.model.Fees;
import com.qfix.model.FeesRecurringDetail;
import com.qfix.model.LateFeesDetail;
import com.qfix.model.SchemeCode;

@Repository
public class FeesExcelTemplate extends ExcelTemplateBase implements ExcelTemplate{

	private String token;

	@Autowired
	ExcelTemplateGenerator generator;
	
	public void setToken(String token){
		this.token = token;
	}

	@Autowired
	IDisplayTemplateDao displayTemplateDao;


	private List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, boolean isUploadUpdate, String token) {
		List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();

		ExcelColumnDetails columnDetails = null;

		if(isUploadUpdate){
			List<String> actions = new ArrayList<>();
			actions.add("Add");
			actions.add("Update");
			actions.add("Delete");

			columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.FEES_ID);
	    	columnDetails.setHideColumn(true);
	    	columnDetailsList.add(columnDetails);

	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName(UploadConstants.FEES_ACTION);
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(actions);
	    	columnDetailsList.add(columnDetails);
		}

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_HEAD_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getFeeHeads(branchId));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_DESCRIPTION_LABEL );
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_CASTE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getMasterData(UploadConstants.CASTE));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_DUE_DATE_LABEL+UploadConstants.DATE_FORMAT_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_START_DATE_LABEL+UploadConstants.DATE_FORMAT_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_END_DATE_LABEL+UploadConstants.DATE_FORMAT_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_CURRENCY_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setListOptions(getCurrencies());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_AMOUNT_LABEL+UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_ALLOW_REPEAT_ENTRIES_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.RECURRING_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.RECURRING_FREQUENCY_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getFrequencyList(true));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.RECURRING_BY_DAY_ONLY_IF_FREQUENCY_IS_DAILY_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_TYPE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getFeesTypeList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_DISPLAY_TEMPLATE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDisplayTemplateList(branchId));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_LATE_PAYMENT_CHARGES_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getLateFeeList(branchId));
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_STANDARD_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getStandards(branchId));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_DIVISION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getDivisions(branchId));
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_STUDENT_NAME_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_ROLL_NO_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_REGISTRATION_CODE_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_FEES_CODE_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getAllFeesCodes(branchId));
    	columnDetailsList.add(columnDetails);
    	
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_IS_PARTIAL_PAYMENT_ALLOWED_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);
    	
    	List<String> schemeCodeList = getSchemeCodes(branchId, token);
    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_FEES_SCHEME_CODE_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(schemeCodeList);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_IS_SPLIT_PAYMENT_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_FEES_SEED_SCHEME_CODE__LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(schemeCodeList);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_FEES_SEED_SPLIT_AMOUNT_LABEL + UploadConstants.STAR_LABEL);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_STRING);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.REMINDER_START_DATE);
    	columnDetails.setSelectFromList(false);
    	columnDetails.setDataType(UploadConstants.DATATYPE_DATE);
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_EMAIL_REMINDER_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_SMS_REMINDER_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_MOBILE_NOTIFICATION_LABEL);
    	columnDetails.setSelectFromList(true);
    	columnDetails.setListOptions(getYesNoList());
    	columnDetailsList.add(columnDetails);

    	columnDetails = new ExcelColumnDetails();
    	columnDetails.setColumnName(UploadConstants.FEES_FREQUENCY_LABEL);
    	columnDetails.setSelectFromList(true);
    	List<String> listOptions = getFrequencyList(false);
    	listOptions.remove(listOptions.size()-1);
    	columnDetails.setListOptions(listOptions);
    	columnDetailsList.add(columnDetails);

    	return columnDetailsList;
	}

	private List<String> getDisplayTemplateList(Integer branchId) {
		List<DisplayTemplate > displayTemplates = displayTemplateDao.getDisplayTemplates(branchId);
		List<String> displayTemplateList = new ArrayList<>();
		
		if(displayTemplates !=null && displayTemplates.size() > 0){
			for(DisplayTemplate template : displayTemplates){
				displayTemplateList.add(template.getName());
			}
		}
		return displayTemplateList;
	}

	private List<String> getFeesTypeList() {
		List<String> list = new ArrayList<String>();
		list.add("Single Fee");
		list.add("Group Fee");
		return list;
	}

	protected List<String> getLateFeeList(Integer branchId) {
		List<String> lateFeesStrList = new ArrayList<String>();
		List<LateFeesDetail> lateFeesList = feesDAO.getAllLateFees(branchId);
		for (LateFeesDetail head : lateFeesList) {
			lateFeesStrList.add(head.getDescription());
		}

		return lateFeesStrList;
	}

	private List<String> getSchemeCodes(Integer branchId, String token) {
		List<String> feesSchemeCodeList = new ArrayList<String>();
		List<SchemeCode> shcemeCodeList = feesDAO.getSchemeCodes(branchId, token);
		for(SchemeCode schemeCode : shcemeCodeList){
			feesSchemeCodeList.add(schemeCode.getName());
		}
		return feesSchemeCodeList;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId) throws IOException{

		List<String> dependentFields = new ArrayList<>();
		dependentFields.add(UploadConstants.DEPENDENT_ADDRESS);
		dependentFields.add(UploadConstants.DEPENDENT_STANDARD);
		generator.prepareFile("", excelFileName, getColumnDetails(filePath, branchId, false, token), dependentFields, branchId, false, null);

		return generator.getExcelTemplateFile();
	}


	public byte[] getExcelTemplateFile(List<Fees> feesList,
			Integer branchId, String token) throws IOException{

		List<String> dependentFields = new ArrayList<>();
		dependentFields.add(UploadConstants.DEPENDENT_ADDRESS);
		dependentFields.add(UploadConstants.DEPENDENT_STANDARD);
		List<ExcelColumnDetails> columnDetails = getColumnDetails("", branchId, true, token);

		String[][] dataArray = getDataArray(feesList, columnDetails.size());

		generator.prepareFile("", "eduqfix-upload-student-template.xlsx", columnDetails, dependentFields, branchId, false, dataArray);

		return generator.getExcelTemplateFile();
	}

	private String[][] getDataArray(List<Fees> feesList, int columnLength) {
		String[][] dataArray = new String[feesList.size()][columnLength + 1];

		for(int i=0 ;i < feesList.size(); i++){
			Fees fees = feesList.get(i);
			int j=0;
			dataArray[i][j ++] = fees.getId() +"";
			dataArray[i][j ++] = "";
			dataArray[i][j ++] = fees.getHead();
			dataArray[i][j ++] = fees.getDescription(); 
			dataArray[i][j ++] = fees.getCaste();
			dataArray[i][j ++] = fees.getDueDate();
			dataArray[i][j ++] = fees.getFromDate();
			dataArray[i][j ++] = fees.getToDate();
			dataArray[i][j ++] = fees.getCurrency();
			dataArray[i][j ++] = fees.getAmount();
			dataArray[i][j ++] = fees.getAllowRepeatEntries();

			if("Y".equals(fees.getRecurring())){
				FeesRecurringDetail recurringDetail = fees.getRecurringDetail();
				dataArray[i][j ++] =  "Y";
				dataArray[i][j ++] =  recurringDetail.getFrequency();
				dataArray[i][j ++] =  (recurringDetail.getWorkWeeks() != null ? recurringDetail.getWorkWeeks() : "");
			}
			else {
				dataArray[i][j ++] =  "N";
				dataArray[i][j ++] =  "";
				dataArray[i][j ++] =  "";
			}

			dataArray[i][j ++] =  fees.getFeeType();
			dataArray[i][j ++] =  fees.getDisplayTemplateName();
			dataArray[i][j ++] =  fees.getLateFeeDetailName();
			dataArray[i][j ++] =  fees.getStandard();
			dataArray[i][j ++] =  fees.getDivision();
			dataArray[i][j ++] =  fees.getStudentName();
			dataArray[i][j ++] =  fees.getRollNo();
			dataArray[i][j ++] =  fees.getRegistrationCode();
			dataArray[i][j ++] =  fees.getFeesCodeName();
			dataArray[i][j ++] =  fees.getIsPartialPaymentAllowed();
			dataArray[i][j ++] =  fees.getSchemeCode();
			dataArray[i][j ++] =  fees.getIsSplitPayment();
			dataArray[i][j ++] =  fees.getSeedSchemeCode();
			dataArray[i][j ++] =  fees.getSeedSplitAmount();
			dataArray[i][j ++] =  fees.getReminderStartDate();
			dataArray[i][j ++] =  fees.getIsEmail();
			dataArray[i][j ++] =  fees.getIsSms();
			dataArray[i][j ++] =  fees.getIsNotification();
			dataArray[i][j ++] =  fees.getFrequency();
		}
		return dataArray;
	}

	@Override
	public byte[] getExcelTemplateFile(String filePath, String excelFileName,
			Integer branchId, Integer standardId, Integer repeatCount)
			throws IOException {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId, Integer standardId, Integer repeatCount) {
		return null;
	}

	@Override
	public List<ExcelColumnDetails> getColumnDetails(String workbookName,
			Integer branchId) {
		// TODO Auto-generated method stub
		return null;
	}	
}
