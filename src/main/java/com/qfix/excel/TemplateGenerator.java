package com.qfix.excel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.qfix.dao.HeadInterfaceDao;
import com.qfix.daoimpl.HeadDao;
import com.qfix.daoimpl.MasterDao;
import com.qfix.daoimpl.StandardDao;
import com.qfix.model.Head;
import com.qfix.model.Master;
import com.qfix.model.Standard;

public enum TemplateGenerator {
	

	FEES{
		public List<ExcelColumnDetails> getColumnDetails(Integer branchId) {
			List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Head");
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getFeeHeads(branchId));
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("description");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("due date");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("start date");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);
	    	
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("to date");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);

	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Caste");
	    	columnDetails.setSelectFromList(true);
//	    	columnDetails.setListOptions(getMasterData(UploadConstants.CASTE));
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("standard");
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getStandards(branchId));
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("isEmail");
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getYesNoList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("isSMS");
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getYesNoList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("isNotification");
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getYesNoList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Frequency");
	    	columnDetails.setSelectFromList(true);
	    	columnDetails.setListOptions(getFrequencyList());
	    	columnDetailsList.add(columnDetails);
	    	
	    	
	    	return columnDetailsList;
		}
		
		private List<String> getFeeHeads(Integer branchId) {
			List<String> feeHeads = new ArrayList<String>();
			HeadDao headDao = new HeadDao();
			List<Head> heads = headDao.getHeadByBranchForDashboard(branchId);
			for (Head head : heads) {
				feeHeads.add(head.getName());
			}
			
			return feeHeads;
		}
	}, 
	HOLIDAY{
		public List<ExcelColumnDetails> getColumnDetails(Integer branchId) {
			List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	    	
	    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Name");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);

	    	
	    	return columnDetailsList;
		}
	}, 
	INSTITUTE{
		public List<ExcelColumnDetails> getColumnDetails() {
			List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	    	
	    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Name");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);

	    	
	    	return columnDetailsList;
		}
	}, 
	TEACHER{
		public List<ExcelColumnDetails> getColumnDetails(Integer branchId) {
			List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	    	
	    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Name");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);

	    	
	    	return columnDetailsList;
		}
	}, 
	STUDENT{
		public List<ExcelColumnDetails> getColumnDetails(Integer branchId) {
			List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	    	
	    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Name");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);

	    	
	    	return columnDetailsList;
		}
	}, 
	EVENT{
		public List<ExcelColumnDetails> getColumnDetails(Integer branchId) {
			List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	    	
	    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Name");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);

	    	
	    	return columnDetailsList;
		}
	}, 
	BRANCH{
		public List<ExcelColumnDetails> getColumnDetails() {
			List<ExcelColumnDetails> columnDetailsList = new ArrayList<ExcelColumnDetails>();
	    	
	    	ExcelColumnDetails columnDetails = new ExcelColumnDetails();
	    	columnDetails.setColumnName("Name");
	    	columnDetails.setSelectFromList(false);
	    	columnDetailsList.add(columnDetails);

	    	columnDetailsList.add(columnDetails);
	    	
	    	return columnDetailsList;
		}
	};

	private static List<String> getStandards(Integer branchId) {
		List<String> standards = new ArrayList<String>();
		StandardDao standardDao = new StandardDao();
		try {
			List<Standard> standardList = standardDao.getAll(branchId);
			for (Standard standard : standardList) {
				standards.add(standard.getDisplayName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return standards;
	}
	
	private static List<String> getYesNoList(){
		List<String> list = new ArrayList<String>();
		list.add("Y");
		list.add("N");
		return list;
	}
	
	private static List<String> getFrequencyList(){
		List<String> list = new ArrayList<String>();
		list.add("Daily");
		list.add("Weekly");
		list.add("Monthly");
		return list;
	}
	
/*	private List<String> getMasterData(String name) {
		List<String> masterData = new ArrayList<String>();
		List<Master> masterList = new ArrayList<Master>();
		switch (name) {
		case UploadConstants.CASTE:
			masterList = masterDao.getAllCaste();
			break;
		default:
			break;
		}
		
		if (masterList.size() > 0) {
			for (Master master: masterList) {
				masterData.add(master.getName());
			}
		}
		
		return masterData;
	}*/

	public List<ExcelColumnDetails> getColumnDetails(Integer branchId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Autowired
	@Qualifier("HeadDAO")
	HeadInterfaceDao headDAO;
	
	@Autowired 
	MasterDao masterDao;
}
