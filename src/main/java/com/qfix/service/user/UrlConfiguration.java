package com.qfix.service.user;

import com.qfix.utilities.Constants;

public interface UrlConfiguration {
	// for live = http://10.80.76.14:8080
	public String GET_USER_SERVICE_DETAILS_URL = "http://localhost:8080/user/login";
	public String REGISTER_PARENT_URL = "http://localhost:8080//parent/register_by_admin";
	public String GET_TEACHER_SERVICE_DETAILS_URL = "http://localhost:8080/user/identity?role="+Constants.ROLE_TEACHER.toString();
	public String UPDATE_TIMETABLE_SERVICE_DETAILS_URL = "http://localhost:8080/branch/";
	public String GET_SCHEME_CODES_URL = "http://localhost:8080/payment/scheme";
	
	//payment_config_id
}