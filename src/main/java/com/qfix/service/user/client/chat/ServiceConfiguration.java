package com.qfix.service.user.client.chat;

import java.text.MessageFormat;

import org.springframework.http.HttpMethod;

public class ServiceConfiguration {
	
	private String url;
	
	private Object[] urlParameters;
	
	private boolean replaceParameters = false;
	
	private HttpMethod method;
	
	public ServiceConfiguration(String url, HttpMethod method) {
		this.url = url;
		this.method = method;		
	}
	
	public ServiceConfiguration(String url, HttpMethod method, boolean replaceParameters) {
		this(url, method);
		this.replaceParameters = replaceParameters;		
	}
	
	
	

	public String getUrl() {
		if(replaceParameters) {
			MessageFormat mf = new MessageFormat(url);
			url = mf.format(urlParameters);			
		}
		
		
		return url;
	}

	public HttpMethod getMethod() {
		return method;
	}

	public void setUrlParameters(Object[] urlParameters) {
		this.urlParameters = urlParameters;
	}
	
	

}
