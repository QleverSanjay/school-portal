package com.qfix.service.user.client.chat.request.group;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateGroupRequest implements Request{

	@JsonProperty(value="group_id")
	@NotNull
	String groupId;	
	
	@JsonProperty(value="add_occupants_ids")
	String occupantsIdsAdd;
	
	@JsonProperty(value="remove_occupants_ids")
	String occupantsIdsRemove;



	public String getGroupId() {
		return groupId;
	}


	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}


	public String getOccupantsIdsAdd() {
		return occupantsIdsAdd;
	}


	public void setOccupantsIdsAdd(String occupantsIdsAdd) {
		this.occupantsIdsAdd = occupantsIdsAdd;
	}


	public String getOccupantsIdsRemove() {
		return occupantsIdsRemove;
	}


	public void setOccupantsIdsRemove(String occupantsIdsRemove) {
		this.occupantsIdsRemove = occupantsIdsRemove;
	}


	@Override
	public String toString() {
		return "GroupRequest [groupId=" + groupId + ", occupantsIdsAdd=" + occupantsIdsAdd + ", occupantsIdsRemove=" + occupantsIdsRemove + "]";
	}

}
