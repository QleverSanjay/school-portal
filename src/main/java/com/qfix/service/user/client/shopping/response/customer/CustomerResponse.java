package com.qfix.service.user.client.shopping.response.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerResponse {

	@XmlElement(name = "status")
	private String status;

	@XmlElement(name = "text")
	private String text;

	@XmlElement(name = "customer_id")
	private int customerId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "CustomerResponse [status=" + status + ", text=" + text
				+ ", customerId=" + customerId + "]";
	}

}
