package com.qfix.service.user.client.forum.request.delete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteForumRequest implements Request {

	@JsonProperty(value = "forumId")
	Integer forumId;

	public Integer getForumId() {
		return forumId;
	}

	public void setForumId(Integer forumId) {
		this.forumId = forumId;
	}

	@Override
	public String toString() {
		return "DeleteForumRequest [forumId=" + forumId + "]";
	}

}