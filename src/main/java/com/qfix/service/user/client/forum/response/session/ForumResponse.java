package com.qfix.service.user.client.forum.response.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.response.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForumResponse implements Response{
	
	@JsonProperty("forum_id")
	long forum_id;
	
	@JsonProperty("forum_gid")
	long forum_gid;

	public long getForum_id() {
		return forum_id;
	}

	public void setForum_id(long forum_id) {
		this.forum_id = forum_id;
	}
	
	public long getForum_gid() {
		return forum_gid;
	}

	public void setForum_gid(long forum_gid) {
		this.forum_gid = forum_gid;
	}

	@Override
	public String toString() {
		return "ForumResponse [forum_id=" + forum_id + ", forum_gid="
				+ forum_gid + "]";
	}
}
