package com.qfix.service.user.client.shopping.response.session;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qfix.service.user.client.chat.response.Response;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ShoppingSessionResponse implements Response{
	@XmlElement(name="status")
	String status;
	
	String cookies;
	
	Date cookieExpiresOn;
	
	public String getCookies() {
		return cookies;
	}



	public void setCookies(String cookies) {
		this.cookies = cookies;
	}



	public Date getCookieExpiresOn() {
		return cookieExpiresOn;
	}



	public void setCookieExpiresOn(Date cookieExpiresOn) {
		this.cookieExpiresOn = cookieExpiresOn;
	}



	@Override
	public String toString() {
		return "CreateShoppingSession [status=" + status + "]";
	}



	public Object getSuccessResponse() {
		// TODO Auto-generated method stub
		return null;
	}

}
