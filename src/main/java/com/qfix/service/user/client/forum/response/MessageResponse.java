package com.qfix.service.user.client.forum.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.response.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageResponse implements Response {

	@JsonProperty("Message")
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MessageResponse [message=" + message + "]";
	}
}