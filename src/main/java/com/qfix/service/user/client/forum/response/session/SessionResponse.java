package com.qfix.service.user.client.forum.response.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.model.LoginResponse;
import com.qfix.service.user.client.chat.response.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionResponse implements Response {
	@JsonProperty("Message")
	String Message;

	@JsonProperty("Message")
	String cookie;

	@JsonProperty("success")
	LoginResponse successResponse;

	@JsonProperty("error")
	LoginResponse errorResponse;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public LoginResponse getSuccessResponse() {
		return successResponse;
	}

	public void setSuccessResponse(LoginResponse successResponse) {
		this.successResponse = successResponse;
	}

	public LoginResponse getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(LoginResponse errorResponse) {
		this.errorResponse = errorResponse;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	@Override
	public String toString() {
		return "SessionResponse [Message=" + Message + ", cookie=" + cookie
				+ ", successResponse=" + successResponse + ", errorResponse="
				+ errorResponse + "]";
	}

}
