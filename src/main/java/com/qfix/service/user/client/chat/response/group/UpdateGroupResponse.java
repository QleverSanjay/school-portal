package com.qfix.service.user.client.chat.response.group;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.response.Response;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateGroupResponse implements Response{
	@JsonProperty("add_request_status")
	String addRequestStatus;
	@JsonProperty("remove_request_status")
	String removeRequestStatus;

	public String getAddRequestStatus() {
		return addRequestStatus;
	}

	public void setAddRequestStatus(String addRequestStatus) {
		this.addRequestStatus = addRequestStatus;
	}

	public String getRemoveRequestStatus() {
		return removeRequestStatus;
	}

	public void setRemoveRequestStatus(String removeRequestStatus) {
		this.removeRequestStatus = removeRequestStatus;
	}

	@Override
	public String toString() {
		return "UpdateGroupResponse [addRequestStatus=" + addRequestStatus + ", removeRequestStatus="
				+ removeRequestStatus  + "]";
	}

}
