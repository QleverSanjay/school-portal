package com.qfix.service.user.client.chat.request.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRequest implements Request{

	@JsonProperty(value="firstname")
	String firstname;	
	
	@JsonProperty(value="surname")
	String surname;
	
	@JsonProperty(value="tags")
	String tags;
	
	@JsonProperty(value="email")
	String email;
	

	

	public String getFirstname() {
		return firstname;
	}




	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}




	public String getSurname() {
		return surname;
	}




	public void setSurname(String surname) {
		this.surname = surname;
	}




	public String getTags() {
		return tags;
	}




	public void setTags(String tags) {
		this.tags = tags;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	@Override
	public String toString() {
		return "User [firstname=" + firstname + ", surname=" + surname + ", tags=" + tags + ", email=" + email + "]";
	}

}
