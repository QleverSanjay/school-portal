package com.qfix.service.user.client.forum.response.addUser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.response.Response;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserResponse implements Response{
	@JsonProperty("forum_profile_id")
	Integer forum_profile_id;

	public Integer getForum_profile_id() {
		return forum_profile_id;
	}

	public void setForum_profile_id(Integer forum_profile_id) {
		this.forum_profile_id = forum_profile_id;
	}

	@Override
	public String toString() {
		return "CreateUserResponse [forum_profile_id=" + forum_profile_id + "]";
	}
}
