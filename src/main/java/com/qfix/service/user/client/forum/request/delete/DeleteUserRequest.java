package com.qfix.service.user.client.forum.request.delete;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteUserRequest implements Request {

	@JsonProperty(value = "uid")
	Set<Integer> userIds;

	public Set<Integer> getUserIds() {
		return userIds;
	}

	public void setUserIds(Set<Integer> userIdList) {
		this.userIds = userIdList;
	}

	@Override
	public String toString() {
		return "DeleteUserRequest [userIdList=" + userIds + "]";
	}

}