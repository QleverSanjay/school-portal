package com.qfix.service.user.client.chat.request.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionRequest implements Request{
	
	@JsonProperty("login")
	String login;

	@JsonProperty("password")
	String password;

	

	public String getLogin() {
		return login;
	}



	public void setLogin(String login) {
		this.login = login;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	@Override
	public String toString() {
		return "SessionRequest [login=" + login + ", password="
				+ password + "]";
	}

}
