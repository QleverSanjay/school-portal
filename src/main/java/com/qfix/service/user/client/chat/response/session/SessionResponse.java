package com.qfix.service.user.client.chat.response.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.response.Response;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionResponse implements Response{
	@JsonProperty("application_id")
	String applicationId;
	@JsonProperty("auth_key")
	String authKey;
	@JsonProperty("timestamp")
	String timestamp;
	@JsonProperty("nonce")
	String nonce;
	@JsonProperty("signature")
	String signature;
	@JsonProperty("user")
	UserResponse user;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public UserResponse getUser() {
		return user;
	}

	public void setUser(UserResponse user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "CreateSession [applicationId=" + applicationId + ", authKey="
				+ authKey + ", timestamp=" + timestamp + ", nonce=" + nonce
				+ ", signature=" + signature + ", user=" + user + "]";
	}

}
