package com.qfix.service.user.client.shopping;

import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

public class HttpResHeaderTest {

	final static Logger logger = Logger.getLogger(HttpResHeaderTest.class);
	
	public void call() {
		try {
			 
           URL obj = new URL("http://localhost/shopping/xmlconnect/configuration/index/app_code/defand1/screen_size/1600x381");
           URLConnection conn = obj.openConnection();
           conn.setDoOutput(true);
           Map<String, List<String>> map = conn.getHeaderFields();

           System.out.println("Printing All Response Header for URL: "
                   + obj.toString() + "\n");

           for (Map.Entry<String, List<String>> entry : map.entrySet()) {
               
           }

           
           List<String> contentLength = map.get("Content-Length");

           if (contentLength == null) {
               System.out
               .println("'Content-Length' doesn't present in Header!");
           } else {
               for (String header : contentLength) {
                   
               }
           }
           
           List<String> response = map.get("Set-Cookie");
           
           
           OutputStream stream = conn.getOutputStream();
           
           
     /*      RestTemplate restTemplate = createRestTemplate();
           ObjectMapper mapper = new ObjectMapper();
   		// Convert object to JSON string
   		String jsonInString = mapper.writeValueAsString(request);
   		

   		HttpEntity<String> entity = new HttpEntity<String>(jsonInString,
   				getHeader(token, map.get("Set-Cookie")));
   	//	entity.getHeaders().add("Set-Cookie", getCookies().toString());
   		

   		ResponseEntity<String> createSessionResponse = restTemplate.exchange(
   				serviceConfiguration.getUrl(),
   				serviceConfiguration.getMethod(), entity, String.class);*/

       } catch (Exception e) {
    	   logger.error("", e);
       }
	}
	
	
	private RestTemplate createRestTemplate() {
	    RestTemplate restTemplate = null;
	/*    final CommonsClientHttpRequestFactory requestFactory = new CommonsClientHttpRequestFactory();
	    requestFactory.setReadTimeout(1500);
	    restTemplate = new RestTemplate(requestFactory);
	 */   return restTemplate;
	}
	
	private HttpHeaders getHeader(String token, List<String> cookie) {

		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML_VALUE);
		headers.set("QuickBlox-REST-API-Version", "0.1.0");
		headers.set(HttpHeaders.ACCEPT, MediaType.TEXT_XML_VALUE);
		if (!StringUtils.isEmpty(token)) {
			headers.set("QB-Token", token);
		}
		headers.set(HttpHeaders.COOKIE, cookie.toString());
		
		return headers;
	}

}
