package com.qfix.service.user.client.chat.request.group;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupRequest implements Request{

	@JsonProperty(value="owner_username")
	@NotNull
	String groupOwnerUsername;	
	
	@JsonProperty(value="owner_password")
	@NotNull
	String groupOwnerPassword;	
	
	@JsonProperty(value="group_name")
	@NotNull
	String groupName;	
	
	@JsonProperty(value="photo_id")
	String photoId;
	
	public String getGroupOwnerUsername() {
		return groupOwnerUsername;
	}

	public void setGroupOwnerUsername(String groupOwnerUsername) {
		this.groupOwnerUsername = groupOwnerUsername;
	}

	public String getGroupOwnerPassword() {
		return groupOwnerPassword;
	}


	public void setGroupOwnerPassword(String groupOwnerPassword) {
		this.groupOwnerPassword = groupOwnerPassword;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getPhotoId() {
		return photoId;
	}


	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	@Override
	public String toString() {
		return "GroupRequest [groupOwnerUsername=" + groupOwnerUsername
				+ ", groupOwnerPassword=" + groupOwnerPassword + ", groupName="
				+ groupName + ", photoId=" + photoId + "]";
	}
}
