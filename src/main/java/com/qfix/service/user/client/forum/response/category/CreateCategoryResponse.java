package com.qfix.service.user.client.forum.response.category;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.response.Response;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateCategoryResponse implements Response{
	@JsonProperty("category_id")
	String school_id;
	@JsonProperty("rwgid")
	String rwgid;
	@JsonProperty("rogid")
	String rogid;
	
	
	public String getSchool_id() {
		return school_id;
	}
	public void setSchool_id(String school_id) {
		this.school_id = school_id;
	}
	public String getRwgid() {
		return rwgid;
	}
	public void setRwgid(String rwgid) {
		this.rwgid = rwgid;
	}
	public String getRogid() {
		return rogid;
	}
	public void setRogid(String rogid) {
		this.rogid = rogid;
	}
	@Override
	public String toString() {
		return "CreateCategoryResponse [school_id=" + school_id + ", rwgid="
				+ rwgid + ", rogid=" + rogid + "]";
	}
	
}
