package com.qfix.service.user.client.chat.response.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.qfix.service.user.client.chat.response.Response;

@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
@JsonTypeName(value = "user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponse implements Response{
	
	@JsonProperty("id")
	long chatUserProfileId;
	@JsonProperty("email")
	String email;
	@JsonProperty("owner_id")
	long ownerId;
	@JsonProperty("login")
	String login;
	@JsonProperty("user_tags")
	String tags;
	
	@JsonProperty("password")
	String password;
	
	public long getChatUserProfileId() {
		return chatUserProfileId;
	}
	public void setId(long chatUserProfileId) {
		this.chatUserProfileId = chatUserProfileId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	@Override
	public String toString() {
		return "CreateUser [chatUserProfileId=" + chatUserProfileId + ", email=" + email + ", ownerId="
				+ ownerId + ", login=" + login + ", tags=" + tags + "]";
	}
	
	

}
