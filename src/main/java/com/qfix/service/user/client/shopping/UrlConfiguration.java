package com.qfix.service.user.client.shopping;

import org.springframework.http.HttpMethod;

// TODO : Read from a XML configuration file.
public class UrlConfiguration {
	// For live = htttp://www.eduqfix.com
	public static final ServiceConfiguration GET_SERVER_CONFIG = new ServiceConfiguration("http://localhost/shopping/xmlconnect/configuration/index/app_code/defand1/screen_size/1600x381/",HttpMethod.GET);

	public static final ServiceConfiguration CREATE_SESSION_CONFIG = new ServiceConfiguration("http://localhost/shopping/xmlconnect/Customer/login",HttpMethod.POST);

	public static final ServiceConfiguration CREATE_CUSTOMER_CONFIG = new ServiceConfiguration("http://localhost/shopping/xmlconnect/Customer/save",HttpMethod.POST);

	public static final String GET_VENDOR_REPORTS_CONFIG_URL = "http://localhost/shopping/xmlconnect/report/branch/id/";

	public static final ServiceConfiguration ATTCH_VENDOR_CONFIG_URL = new ServiceConfiguration("http://localhost/shopping/xmlconnect/vendor/branches/", HttpMethod.POST);

	public static final ServiceConfiguration DE_ATTCH_VENDOR_CONFIG_URL = new ServiceConfiguration("http://localhost/shopping/xmlconnect/vendor/branchesremove", HttpMethod.POST);
}