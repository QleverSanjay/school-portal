package com.qfix.service.user.client.chat.response.group;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.response.Response;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateGroupResponse implements Response{
	@JsonProperty("_id")
	String groupChatId;
	@JsonProperty("xmpp_room_jid")
	String roomId;
	

	public String getGroupChatId() {
		return groupChatId;
	}

	public void setGroupChatId(String groupChatId) {
		this.groupChatId = groupChatId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}


	@Override
	public String toString() {
		return "CreateGroupResponse [groupChatId=" + groupChatId + ", roomId="
				+ roomId  + "]";
	}

}
