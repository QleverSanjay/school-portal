package com.qfix.service.user.client.forum.request.category;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryRequest implements Request{

	@JsonProperty(value="name")
	@NotNull
	String name;	
	
	@JsonProperty(value="forum_desc")
	String forum_desc;
	
	@JsonProperty(value="type")
	String type;
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getForum_desc() {
		return forum_desc;
	}


	public void setForum_desc(String forum_desc) {
		this.forum_desc = forum_desc;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "CategoryRequest [name=" + name + ", forum_desc=" + forum_desc
				+ ", type=" + type + "]";
	}
}
