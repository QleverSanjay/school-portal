package com.qfix.service.user.client.shopping;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import com.qfix.model.Report;

@XmlAccessorType(XmlAccessType.FIELD)
public class VendorReportResponse {

	@XmlAttribute
	private Report report;

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	@Override
	public String toString() {
		return "VendorReportResponse [report=" + report + "]";
	}

}
