package com.qfix.service.user.client.forum.request.delete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteCategoryRequest implements Request {

	@JsonProperty(value = "forumId")
	Integer categoryId;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer forumId) {
		this.categoryId = forumId;
	}

	@Override
	public String toString() {
		return "DeleteForumRequest [forumId=" + categoryId + "]";
	}

}