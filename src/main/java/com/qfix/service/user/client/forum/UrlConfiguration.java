package com.qfix.service.user.client.forum;

import org.springframework.http.HttpMethod;

// TODO : Read from a XML configuration file.
public class UrlConfiguration {
	// for live - http://10.80.41.14
	public static final ServiceConfiguration CREATE_SESSION_CONFIG = new ServiceConfiguration("http://localhost/collab/api/auth.php?action=login",HttpMethod.POST);
	
	public static final ServiceConfiguration CREATE_CATEGORY_CONFIG = new ServiceConfiguration("http://localhost/collab/api/create.php?action=category",HttpMethod.POST);
	
	public static final ServiceConfiguration CREATE_FORUM_CONFIG = new ServiceConfiguration("http://localhost/collab/api/create.php?action=forum",HttpMethod.POST);
	
	public static final ServiceConfiguration CREATE_USER_CONFIG = new ServiceConfiguration("http://localhost/collab/api/users.php?action=add",HttpMethod.POST);
	
	public static final ServiceConfiguration DUMMY_CALL_CONFIG = new ServiceConfiguration("http://localhost/collab/",HttpMethod.GET);

	public static final ServiceConfiguration LINK_USER_TO_FORUM_CONFIG = new ServiceConfiguration("http://localhost/collab/api/users.php?action=linkuser",HttpMethod.POST);
	
	public static final ServiceConfiguration DELINK_USER_TO_FORUM_CONFIG = new ServiceConfiguration("http://localhost/collab/api/users.php?action=delinkuser",HttpMethod.POST);
	
	public static final ServiceConfiguration LOGIN_FORUM_CONFIG = new ServiceConfiguration("http://localhost/collab/api/auth.php?action=login",HttpMethod.POST);
	//	public static final ServiceConfiguration UPDATE_GROUP_CONFIG = new ServiceConfiguration("http://v2stech-pc:8080/chat-service/groups/update",HttpMethod.PUT);
	
	public static final ServiceConfiguration DELETE_FORUM_CONFIG = new ServiceConfiguration("http://localhost/collab/api/delete.php?action=forum", HttpMethod.POST);
	
	public static final ServiceConfiguration DELETE_CATEGORY_CONFIG = new ServiceConfiguration("http://localhost/collab/api/delete.php?action=category", HttpMethod.POST);
	
	public static final ServiceConfiguration DELETE_USER_CONFIG = new ServiceConfiguration("http://localhost/collab/api/users.php?action=delete", HttpMethod.POST);
	
	
}