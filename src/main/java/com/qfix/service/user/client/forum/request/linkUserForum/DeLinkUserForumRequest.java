package com.qfix.service.user.client.forum.request.linkUserForum;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeLinkUserForumRequest implements Request {

	@JsonProperty(value = "group_id")
	Integer groupId;

	@JsonProperty(value = "user_id")
	List<Integer> userIdList;

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public List<Integer> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<Integer> userIdList) {
		this.userIdList = userIdList;
	}
}