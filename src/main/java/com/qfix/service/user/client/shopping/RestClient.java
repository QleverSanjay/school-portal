package com.qfix.service.user.client.shopping;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

public class RestClient {

	
	private Date cookieExpiresOn;
	private HttpHeaders headers;
	final static Logger logger = Logger.getLogger(RestClient.class);
	
	/*
	 * public static void main(String[] args) throws Exception{ RestClient
	 * client = new RestClient(); client.createSession("v2stech","Mumbai@556");
	 * 
	 * }
	 */

	private HttpHeaders getHeader() throws JsonProcessingException {

		/*headers = new HttpHeaders();
		headers.add("Cookie",  getFrontendCookieText(headerText) +"app_code=defand1; screen_size=1600x381"); */
		if(cookieExpiresOn == null || (new Date()).before(cookieExpiresOn)) { //getCookieExpireTime(headerText)
			headers = new HttpHeaders();
			String cookies = generateCookies();
			headers.add("Cookie",  cookies +"app_code=defand1; screen_size=1600x381"); 
		} 
		
		return headers;
	}
	
	private boolean isCookieExpired() {
		
		return false;
	}
	
	private String generateCookies() throws JsonProcessingException  {
		
		MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<String, String>();
		
		valueMap.add("username", "vikas@test.com");
		valueMap.add("password", "Vikas@123");
		
		
		ResponseEntity<String> response =  executeRequest(valueMap, UrlConfiguration.CREATE_SESSION_CONFIG, true );
		cookieExpiresOn = getCookieExpireTime(response.getHeaders().toString());
		return getFrontendCookieText(response.getHeaders().toString());
	}
	
	public static String getFrontendCookieText(String cookieText) {
		String firstStr = cookieText.substring(cookieText.lastIndexOf("frontend"));
		String frontendText =  firstStr.substring(0, firstStr.indexOf(";")+1);
		
		return frontendText;
	}
	
	public static Date getCookieExpireTime(String cookieText) {
		Date date = null;
		try {
			String firstStr = cookieText.substring(cookieText
					.lastIndexOf("expires"));
			String frontendText = firstStr.substring(firstStr.indexOf("=") + 1,
					firstStr.indexOf(";"));

			String string = frontendText;
			DateFormat format = new SimpleDateFormat(
					"EEE, dd-MMM-yyyy HH:mm:ss z", Locale.ENGLISH);

			date = format.parse(string);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
		return date;
	}

	public ResponseEntity<String> executeRequest(MultiValueMap<String, String> valueMap,
			ServiceConfiguration serviceConfiguration, boolean loginRequired)
			throws JsonProcessingException {
		return executeRequest(valueMap, serviceConfiguration, null, loginRequired);
	}

	public ResponseEntity<String> executeRequest(MultiValueMap<String, String> valueMap,
			ServiceConfiguration serviceConfiguration, String token, boolean loginRequired)
			throws JsonProcessingException {
		return executeRequest(valueMap, serviceConfiguration, token, null, loginRequired);
	}

	public ResponseEntity<String> executeRequest(MultiValueMap<String, String> valueMap,
			ServiceConfiguration serviceConfiguration, String token,
			Object[] urlParameters, boolean loginRequired) throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();


		HttpHeaders headers = null;
		if(loginRequired) {
			headers = new HttpHeaders();
			headers.add("Cookie", "app_code=defand1; screen_size=1600x381"); 
		} else {
			headers = getHeader();
		}

		HttpEntity<?> entity = new HttpEntity<>(valueMap,
				headers);

		String url = (urlParameters != null) ? getUrl(
				serviceConfiguration.getUrl(), urlParameters)
				: serviceConfiguration.getUrl();
		
		//HttpEntity<?> request = new HttpEntity<>(map, requestHeaders1);
		ResponseEntity<String> response =
			    		restTemplate.exchange(url, serviceConfiguration.getMethod(), entity, String.class, valueMap);		
				
		//ResponseEntity<String> createSessionResponse = restTemplate.exchange(
		//		url,
		//		serviceConfiguration.getMethod(), entity, String.class); 

		return response;

	}

	public ResponseEntity<String> executeRequest(
			ServiceConfiguration serviceConfiguration, String token)
			throws JsonProcessingException {

		return executeRequest(null, serviceConfiguration, token, false);

	}

	private String getUrl(String url, Object[] urlParameters) {
		MessageFormat mf = new MessageFormat(url);
		return mf.format(urlParameters);
	}

}