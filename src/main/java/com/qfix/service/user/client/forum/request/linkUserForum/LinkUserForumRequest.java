package com.qfix.service.user.client.forum.request.linkUserForum;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkUserForumRequest implements Request {

	@JsonProperty(value = "group_id")
	long groupId;

	@JsonProperty(value = "user_id")
	Set<Integer> userIdList;

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public Set<Integer> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(Set<Integer> userIdList) {
		this.userIdList = userIdList;
	}
}