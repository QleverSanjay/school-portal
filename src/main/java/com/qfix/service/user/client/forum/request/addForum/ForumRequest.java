package com.qfix.service.user.client.forum.request.addForum;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForumRequest implements Request {

	@JsonProperty(value = "name")
	String name;

	@JsonProperty(value = "one_way")
	String one_way;

	@JsonProperty(value = "parent_id")
	Integer parent_id;

	@JsonProperty(value = "forum_desc")
	String forum_desc;

	@JsonProperty(value = "type")
	String type;

	@JsonProperty(value = "school_id")
	Integer school_id;

	public Integer getSchool_id() {
		return school_id;
	}

	public void setSchool_id(Integer school_id) {
		this.school_id = school_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParent_id() {
		return parent_id;
	}

	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}

	public String getForum_desc() {
		return forum_desc;
	}

	public void setForum_desc(String forum_desc) {
		this.forum_desc = forum_desc;
	}

	public String getOne_way() {
		return one_way;
	}

	public void setOne_way(String one_way) {
		this.one_way = one_way;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ForumRequest [name=" + name + ", one_way=" + one_way
				+ ", parent_id=" + parent_id + ", forum_desc=" + forum_desc
				+ ", type=" + type + ", school_id=" + school_id + "]";
	}
}
