package com.qfix.service.user.client.forum.request.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionRequest implements Request{
	
	@JsonProperty("username")
	String username;

	@JsonProperty("user_password")
	String user_password;

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	@Override
	public String toString() {
		return "SessionRequest [username=" + username + ", user_password="
				+ user_password + "]";
	}
}
