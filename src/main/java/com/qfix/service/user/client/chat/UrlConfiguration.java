package com.qfix.service.user.client.chat;

import org.springframework.http.HttpMethod;

// TODO : Read from a XML configuration file.
public class UrlConfiguration {
	// for live = http://10.80.76.13:8080
	public static final ServiceConfiguration CREATE_SESSION_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/chat/login",HttpMethod.POST);
	
	public static final ServiceConfiguration CREATE_USER_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/chat/users",HttpMethod.POST);
	
	public static final ServiceConfiguration CREATE_GROUP_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/groups/public",HttpMethod.POST);
	
	public static final ServiceConfiguration UPDATE_GROUP_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/groups/update",HttpMethod.PUT);
	
}