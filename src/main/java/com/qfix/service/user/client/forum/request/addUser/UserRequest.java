package com.qfix.service.user.client.forum.request.addUser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.qfix.service.user.client.chat.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRequest implements Request {

	@JsonProperty(value = "username")
	String username;

	@JsonProperty(value = "user_password")
	String user_password;

	@JsonProperty(value = "user_email")
	String email;

	@JsonProperty(value = "user_gid")
	Integer user_gid;

	@JsonProperty(value = "school_id")
	Integer school_id;

	@JsonProperty(value = "allow_attachment")
	String allow_attachment;

	@JsonProperty(value = "is_admin")
	String is_admin;

	public String getIs_admin() {
		return is_admin;
	}

	public void setIs_admin(String is_admin) {
		this.is_admin = is_admin;
	}

	public String getAllow_attachment() {
		return allow_attachment;
	}

	public void setAllow_attachment(String allow_attachment) {
		this.allow_attachment = allow_attachment;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSchool_id() {
		return school_id;
	}

	public void setSchool_id(Integer school_id) {
		this.school_id = school_id;
	}

	public Integer getUser_gid() {
		return user_gid;
	}

	public void setUser_gid(Integer user_gid) {
		this.user_gid = user_gid;
	}

	@Override
	public String toString() {
		return "UserRequest [username=" + username + ", user_password="
				+ user_password + ", email=" + email + ", user_gid=" + user_gid
				+ ", school_id=" + school_id + ", allow_attachment="
				+ allow_attachment + ", is_admin=" + is_admin + "]";
	}
}
