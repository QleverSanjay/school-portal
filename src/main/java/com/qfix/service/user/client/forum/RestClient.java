package com.qfix.service.user.client.forum;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.service.user.client.chat.request.Request;

public class RestClient {

//	private static final Logger log = Logger.getLogger(RestClient.class);

	/*
	 * public static void main(String[] args) throws Exception{ RestClient
	 * client = new RestClient(); client.createSession("v2stech","Mumbai@556");
	 * 
	 * }
	 */
	
	private HttpHeaders getHeader() {
		return getHeader(null);
	}

	private HttpHeaders getHeader(String token) {

		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

		if (!StringUtils.isEmpty(token)) {
			headers.set("QB-Token", token);
		}
		
		
		return headers;
	}

	public ResponseEntity<String> executeRequest(Request request,
			ServiceConfiguration serviceConfiguration)
			throws JsonProcessingException {
		return executeRequest(request, serviceConfiguration, null);
	}

	public ResponseEntity<String> executeRequest(Request request,
			ServiceConfiguration serviceConfiguration, String cookie)
			throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();

		ObjectMapper mapper = new ObjectMapper();
		// Convert object to JSON string
		String jsonInString = mapper.writeValueAsString(request);
		
		
		HttpHeaders headers = getHeader();
		//headers.add(HttpHeaders.COOKIE, cookie);
		
		HttpEntity<String> entity = new HttpEntity<String>(jsonInString,headers);

		ResponseEntity<String> createSessionResponse = restTemplate.exchange(
				serviceConfiguration.getUrl(),
				serviceConfiguration.getMethod(), entity, String.class);

		return createSessionResponse;

	}
	
	public ResponseEntity<String> executeRequest(ServiceConfiguration serviceConfiguration)
			throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<String> entity = new HttpEntity<String>("",
				getHeader());

		ResponseEntity<String> createSessionResponse = restTemplate.exchange(
				serviceConfiguration.getUrl(),
				serviceConfiguration.getMethod(), entity, String.class);
		
		
		return createSessionResponse;

	}

}