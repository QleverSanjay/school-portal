package com.qfix.service.report;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.qfix.model.StudentReport;
import com.qfix.model.Subject;
import com.qfix.utilities.ReadWriteExcelUtil;
import com.qfix.utilities.ReportHeaderConstants;

public class ReportUploadService {


	private List<StudentReport> studentReports;

	private int headerRowIndex;
	private Map<Integer, String> headerIndexMap;
	private Map<Integer, String> uploadedSubjectIndexMap;
	private List<Integer> subjectIndexSet;


	public List<StudentReport> generateStudentReport(MultipartFile file, Integer branchId, Integer academicYearid, 
			Integer standardId, Integer divisionId,String semester) throws Exception {

		studentReports = new ArrayList<>();
		startReadingFile(file);
		return studentReports;
	}

	private void startReadingFile(MultipartFile file) throws Exception {
		InputStream fileInputStream = file.getInputStream(); 
		try {
			Sheet sheet = new ReadWriteExcelUtil().getSheetByReadingExcelFile(
					fileInputStream, file.getOriginalFilename().endsWith(".xls"));
			if(sheet != null){
				Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) {
					Row nextRow = rowIterator.next();
					startReadingRow(nextRow);
				}
			}
		}finally{
			if(fileInputStream != null){
				try {
					fileInputStream.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	private void startReadingRow(Row row) throws Exception {
		Iterator<Cell> cellIterator = row.iterator();

		while(cellIterator.hasNext()){
			Cell nextCell = cellIterator.next();
			String cellData = ReadWriteExcelUtil.getCellData(nextCell);
			
			if(cellData != null){
				cellData = cellData.trim();
				System.out.println(cellData);
				switch(cellData){
					case ReportHeaderConstants.STUDENT_ID_TEXT:
						int columnIndex = nextCell.getColumnIndex();
						headerIndexMap = new HashMap<>();
						headerIndexMap.put(columnIndex, ReportHeaderConstants.STUDENT_ID_TEXT);
						headerRowIndex = nextCell.getRowIndex()+1;
						prepareHeaderIndexAndSubjectList(cellIterator);
						subjectIndexSet = new ArrayList<>(getUploadedSubjects());

						if(subjectIndexSet == null || subjectIndexSet.size() == 0){
							throw new Exception("Subjects are not available please try by downloading template again.");
						}

			            break;
					default :
						int rowIndex = nextCell.getRowIndex();
						if(headerRowIndex != 0 && rowIndex == (headerRowIndex)){
							if(cellData.equalsIgnoreCase(ReportHeaderConstants.THEROY_MARKS_TEXT)){
								headerIndexMap.put(nextCell.getColumnIndex(), ReportHeaderConstants.THEROY_MARKS_TEXT);
							}
							prepareHeaderIndexForSubjectEntitis(cellIterator);
						}
						else if(headerRowIndex != 0 && rowIndex >= (headerRowIndex + 1)){
							StudentReport studentReport = new StudentReport();
							studentReport.setId((int)Double.parseDouble(cellData));
							studentReport = readStudentMarksData(cellIterator, studentReport);
							studentReports.add(studentReport);
						}
						break;
				}
				break;
			}
		}		
	}

	private StudentReport readStudentMarksData(Iterator<Cell> cellIterator, StudentReport studentReport) {
		Subject subject = null;
		List<Subject> subjectList = new ArrayList<>();
		setSubjectList(subjectList);
		System.out.println("headerIndexMap :::: "+headerIndexMap);

		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			int columnIndex = cell.getColumnIndex();
			String cellData = ReadWriteExcelUtil.getCellData(cell);
			if(headerIndexMap.containsKey(columnIndex)){
				String cellDataFor = headerIndexMap.get(columnIndex);

				System.out.println("cellDataFor:::::::"+cellDataFor+"="+cellData+", index="+columnIndex);

				if(cellDataFor != null){
					subject = getSubjectByColumnIndex(columnIndex, subjectList, cellDataFor);
					switch (cellDataFor){
					case ReportHeaderConstants.STUDENT_ID_TEXT :
						studentReport.setId(Integer.parseInt(cellDataFor));
						break;

					case ReportHeaderConstants.STUDENT_NAME_TEXT :
						studentReport.setStudentName(cellData);
						break;

					case ReportHeaderConstants.ROLE_NUMBER_TEXT:
						studentReport.setRoleNumber(cellData);
						break;

					case ReportHeaderConstants.THEROY_MARKS_TEXT:
						System.out.println("Setting Theory marks::::"+subject.getName()+", marks = "+cellData);
						subject.setTheory(cellData);
						System.out.println("Theory Marks Setted:::::"+subject.getTheory());
						break;

					case ReportHeaderConstants.PRACTICAL_MARKS_TEXT:
						System.out.println("Setting Practical marks::::"+subject.getName()+", marks = "+cellData);
						subject.setPractical(cellData);
						break;

					case ReportHeaderConstants.OBTAINED_MARKS_TEXT:
						subject.setObtainedMarks(cellData);
						break;

					case ReportHeaderConstants.CATEGORY_TEXT:
						subject.setCategory(cellData);
						break;

					/*case ReportHeaderConstants.TOTAL_MARKS_TEXT:
						if(subject == null){
							subject = new Subject();	
						}
						subject.setTotalMarks(cellData);
						break;*/
					case ReportHeaderConstants.REMARKS_TEXT:
						subject.setRemarks(cellData);
						break;

					case ReportHeaderConstants.GROSS_TOTAL_TEXT:
						studentReport.setGrossTotal(cellData);
						break;

					case ReportHeaderConstants.RESULT_TEXT:
						studentReport.setResult(cellData);
						break;

					case ReportHeaderConstants.FINAL_GRADE_TEXT:
						studentReport.setFinalGrade(cellData);
						break;

					case ReportHeaderConstants.POSITION_TEXT:
						if(!StringUtils.isEmpty(cellData) && cellData.indexOf(".") != -1){
							cellData = cellData.substring(0, cellData.indexOf("."));
						}
						studentReport.setPosition(cellData);
						break;

					case ReportHeaderConstants.GENERAL_REMARK_TEXT:
						studentReport.setGeneralRemark(cellData);
						break;
					}
				}
			}
		}
		System.out.println(new Gson().toJson(subjectList));
		studentReport.setSubjectList(subjectList);
		
		return studentReport;
	}

	private void setSubjectList(List<Subject> subjectList) {
		if(subjectIndexSet != null && subjectIndexSet.size() > 0){
			for(int i =0 ; i < subjectIndexSet.size(); i++){
				Subject subject = new Subject();
				subject.setName(uploadedSubjectIndexMap.get(subjectIndexSet.get(i)));
				subjectList.add(subject);
			}
		}
	}

	private Subject getSubjectByColumnIndex(int columnIndex, List<Subject> subjectList, String cellDataFor) {
		Subject subject = null;
		// get Subject when cellDataFor is subject sub-entity like theory marks, practical marks, obtained marks, category or remarks.

		if(cellDataFor.equals(ReportHeaderConstants.THEROY_MARKS_TEXT) || cellDataFor.equals(ReportHeaderConstants.PRACTICAL_MARKS_TEXT) || 
				cellDataFor.equals(ReportHeaderConstants.OBTAINED_MARKS_TEXT) || cellDataFor.equals(ReportHeaderConstants.CATEGORY_TEXT) || 
				cellDataFor.equals(ReportHeaderConstants.REMARKS_TEXT)){

			for(int i=0; i < subjectIndexSet.size(); i++){
				Integer subjectStartIndex = subjectIndexSet.get(i);
				Integer subjectEndIndex = (i != subjectIndexSet.size() -1 ? subjectIndexSet.get(i + 1) : null);

				if(subjectEndIndex == null || (columnIndex >= subjectStartIndex && columnIndex < subjectEndIndex)){
					subject = subjectList.get(i);
					break;
				}
			}
		}
		return subject;
	}

	private void prepareHeaderIndexForSubjectEntitis(Iterator<Cell> cellIterator) {
		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			String cellData = ReadWriteExcelUtil.getCellData(cell);
			if(cellData != null){
				if(cellData.equalsIgnoreCase(ReportHeaderConstants.THEROY_MARKS_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.THEROY_MARKS_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.PRACTICAL_MARKS_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.PRACTICAL_MARKS_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.OBTAINED_MARKS_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.OBTAINED_MARKS_TEXT);
				}

				/*else if(cellData.equalsIgnoreCase(ReportHeaderConstants.TOTAL_MARKS_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.TOTAL_MARKS_TEXT);
				}*/

				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.CATEGORY_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.CATEGORY_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.REMARKS_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.REMARKS_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.GROSS_TOTAL_TEXT)){
					
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.GROSS_TOTAL_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.RESULT_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.RESULT_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.FINAL_GRADE_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.FINAL_GRADE_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.POSITION_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.POSITION_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.GENERAL_REMARK_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.GENERAL_REMARK_TEXT);
				}
			}
		}		
	}

	private void prepareHeaderIndexAndSubjectList(Iterator<Cell> cellIterator) throws Exception {
		uploadedSubjectIndexMap = new HashMap<>();

		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			String cellData = ReadWriteExcelUtil.getCellData(cell);
			
			if(cellData != null){
				if(cellData.equalsIgnoreCase(ReportHeaderConstants.STUDENT_NAME_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.STUDENT_NAME_TEXT);
				}
				else if(cellData.equalsIgnoreCase(ReportHeaderConstants.ROLE_NUMBER_TEXT)){
					headerIndexMap.put(cell.getColumnIndex(), ReportHeaderConstants.ROLE_NUMBER_TEXT);
				}
				else {
					uploadedSubjectIndexMap.put(cell.getColumnIndex(), cellData);
				}
			}
		}
	}

	public Collection<Integer> getUploadedSubjects() {
		if(uploadedSubjectIndexMap != null){
			return new TreeSet<>(uploadedSubjectIndexMap.keySet());
		}
		else {
			return null;
		}
	}


}
