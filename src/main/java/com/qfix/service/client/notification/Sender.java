package com.qfix.service.client.notification;
import java.util.Map;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.qfix.utilities.AppConstants;

/**
 * Create a template of the tutorial bean defined in the XML file and send 10 message 
 * to the MYEXCHANGE configured in the notification-listener-context.xml file with the routing key
 *"my.routingkey.1"
 *
 */
@Service
public class Sender {

	AmqpTemplate template;

	@Autowired
	public void setTemplate(AmqpTemplate template) {
		this.template = template;
	}
/*
 * PREVIOUSLY USED METHOD
	public void sendEmail(String receiverEmail, String attachment, 
			String templateCode, Map<String, String> emailConent) {

		Gson gson = new Gson();
		
		Notification email = new Notification();
		email.setType(AppConstants.TYPE_EMAIL); // user.getUsername() From db
		email.setTo(receiverEmail);
		email.setAttachments(attachment);
		email.setTemplateCode(templateCode);
		email.setEmailContent(emailConent);
		
		String jsonSend = gson.toJson(email);
		System.out.println(jsonSend);
		sendMessage(jsonSend);		
	}

*/

	public void sendSms(String primaryContact, String templateCode, Map<String, String> smsContent) {

		Gson gson = new Gson();
		System.out.println("Got te data**************************************************");
		System.out.println(smsContent);
		Sms sms = new Sms();
		sms.setType(AppConstants.TYPE_SMS); // user.getUsername() From db
		sms.setContactNumber(primaryContact);
		sms.setTemplateCode(templateCode);
		sms.setSmsContent(smsContent);

		String jsonSend = gson.toJson(sms);
		System.out.println("JSOn-------------"+jsonSend);
		sendMessage(jsonSend);
	}

	public String sendMessage(String jsonMessage) {

		System.out.println("***********************SENDING NOTIFICATION***************************\n"+jsonMessage);
		try {
			template.convertAndSend("qfix.routingkey.notifcation",jsonMessage);	
		}
		catch(Exception e){
			e.printStackTrace();
		}
    	return "Success!";
    }
}