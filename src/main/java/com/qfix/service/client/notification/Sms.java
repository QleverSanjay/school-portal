package com.qfix.service.client.notification;

import java.util.Map;
//Class to create template
public class Sms {
	public String type;
	public String password;
	public String contactNumber;
	public String templateCode;
	public Map<String,String> smsContent;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getTemplateCode() {
		return templateCode;
	}
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}
	public Map<String, String> getSmsContent() {
		return smsContent;
	}
	public void setSmsContent(Map<String, String> smsContent) {
		this.smsContent = smsContent;
	}
	@Override
	public String toString() {
		return "Sms [type=" + type + ", password=" + password
				+ ", contactNumber=" + contactNumber + ", templateCode="
				+ templateCode + ", smsContent=" + smsContent + "]";
	}
	
	
}
