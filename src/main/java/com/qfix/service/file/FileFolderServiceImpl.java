package com.qfix.service.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qfix.config.AppProperties;

@Repository
public class FileFolderServiceImpl implements FileFolderService {

	@Autowired
	private AppProperties appProperties;
	
	final static Logger logger = Logger.getLogger(FileFolderServiceImpl.class);
	// Used for creating other than institutes level folders.
	@Override
	public void createExternalStorage(String parentPath, String folderName) throws IOException {
		String newFolderPath = appProperties.getFileProps().getBaseSystemPath() 
				+ appProperties.getFileProps().getExternalDocumentsPath() 
				+ parentPath + folderName;
		newFolderPath = FilenameUtils.separatorsToSystem(newFolderPath);
		
		File file = new File(newFolderPath);

		if(!file.exists()){
			file.mkdirs();
		}
	}

	// Used for creating  the institutes level folders.
	@Override
	public void createExternalStorage(String folderName) throws IOException {
		
		String newFolderPath = appProperties.getFileProps().getBaseSystemPath() 
				+ appProperties.getFileProps().getExternalDocumentsPath()+folderName;
		newFolderPath = FilenameUtils.separatorsToSystem(newFolderPath);
		
		File file = new File(newFolderPath);

		if(!file.exists()){
			file.mkdirs();
		}
	}

	@Override
	public void createInternalStorage(String parentPath, String folderName) throws Exception {
		
		String newFolderPath = appProperties.getFileProps().getBaseSystemPath() 
				+ appProperties.getFileProps().getInternalDocumentsPath() 
				+ parentPath + folderName;
		newFolderPath = FilenameUtils.separatorsToSystem(newFolderPath);
		
		File file = new File(newFolderPath);

		if(!file.exists()){
			file.mkdirs();
		}
	}

	@Override
	public void createInternalStorage(String folderName) throws Exception {
		String newFolderPath = appProperties.getFileProps().getBaseSystemPath() 
				+ appProperties.getFileProps().getInternalDocumentsPath() + folderName;
		
		newFolderPath = FilenameUtils.separatorsToSystem(newFolderPath);
		
		
		File file = new File(newFolderPath);

		if(!file.exists()){
			file.mkdirs();
		}
	}

	
	@Override
	public String createFileInExternalStorage(String parentPath, String fileNameWithExtension, 
			byte[] fileData) throws IOException {

		String relativePath = null;
		try {
			String newFilePath = appProperties.getFileProps().getBaseSystemPath() 
						+ appProperties.getFileProps().getExternalDocumentsPath()
						+ parentPath + fileNameWithExtension;

			newFilePath = FilenameUtils.separatorsToSystem(newFilePath);
			
			
			File file = new File(newFilePath);
			
			writeToFile(file, fileData);
			
			relativePath = appProperties.getFileProps().getExternalDocumentsPath()
					+ parentPath + fileNameWithExtension;
		}catch(Exception e){
			throw new IOException("Profile image not uploaded.",e);
		}

		return relativePath;
	}

	@Override
	public String createFileInExternalStorage(String fileNameWithExtension, byte[] fileData) 
			throws IOException {
		String relativePath = null;
		try {
			String newFilePath = appProperties.getFileProps().getBaseSystemPath() 
					+ appProperties.getFileProps().getExternalDocumentsPath()
					+ fileNameWithExtension;

			newFilePath = FilenameUtils.separatorsToSystem(newFilePath);
			

			File file = new File(newFilePath);
	
			writeToFile(file, fileData);
			relativePath = appProperties.getFileProps().getExternalDocumentsPath() + fileNameWithExtension;
			
		}catch(Exception e){
			throw new IOException("Profile image not uploaded.",e);
		}
		return relativePath;
	}

	
	@Override
	public String createFileInInternalStorage(String parentPath, String fileNameWithExtension, 
			byte[] fileData) throws IOException {
		String relativePath = null;
		try {
			String newFilePath = appProperties.getFileProps().getBaseSystemPath() 
					+ appProperties.getFileProps().getInternalDocumentsPath() 
					+ parentPath + fileNameWithExtension;

			newFilePath = FilenameUtils.separatorsToSystem(newFilePath);
			

			File file = new File(newFilePath);
			writeToFile(file, fileData);
			
			relativePath = appProperties.getFileProps().getInternalDocumentsPath() 
					+ parentPath + fileNameWithExtension;
						
		}catch(Exception e){
			throw new IOException("Profile image not uploaded.",e);
		}
		
		return relativePath;
	}

	
	@Override
	public String createFileInInternalStorage(String fileNameWithExtension, 
			byte[] fileData) throws IOException {
		String relativePath = null;
		try {
			String newFilePath = appProperties.getFileProps().getBaseSystemPath() 
					+ appProperties.getFileProps().getInternalDocumentsPath() 
					+ fileNameWithExtension;
	
			newFilePath = FilenameUtils.separatorsToSystem(newFilePath);
			
			
			File file = new File(newFilePath);
			writeToFile(file, fileData);
			
			relativePath = appProperties.getFileProps().getInternalDocumentsPath() 
					+ fileNameWithExtension;

		}catch(Exception e){
			throw new IOException("Profile image not uploaded.",e);
		}
		return relativePath;
	}

	private void writeToFile(File file, byte[] fileData) throws IOException{
		FileOutputStream writer = null;
		try {
			file.getParentFile().mkdirs();
			if(!file.exists()){
				file.createNewFile();
			}
			writer = new FileOutputStream(file);
			writer.write(fileData);			
		}		
		finally{
	        try {
	        	if(writer != null){
	        		writer.close();	
	        	}	        	
	        }catch(IOException e){
	        	logger.error("", e);
	        }
		}
	}
}
