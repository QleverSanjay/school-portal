package com.qfix.service.file;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Repository;

@Repository
public interface FileFolderService {

	public String NOTICE_DOCUMENTS_PATH = "notices" + File.separator;
	public String ASSIGNMENTS_DOCUMENTS_PATH = "assignments" + File.separator;
	public String PROFILE_DOCUMENTS_PATH = "profiles" + File.separator;
	public String EVENT_DOCUMENTS_PATH = "events" + File.separator;
	public String UPLOAD_DOCUMENTS_PATH = "upload" + File.separator;

	public String DEFAULT_IMAGE_NAME = "no-images.png";
	public String DEFAULT_PROFILE_IMAGE_NAME = "no-profile.png";

	// public String DEFAULT_PROFILE_IMAGE_PATH =
	// ReadPropertiesUtil.properties.getProperty("ROOT_EXTERNAL_DOCUMENTS_PATH")
	// + PROFILE_DOCUMENTS_PATH+ DEFAULT_PROFILE_IMAGE_NAME;
	//
	// public String DEFAULT_IMAGE_PATH =
	// ReadPropertiesUtil.properties.getProperty("ROOT_EXTERNAL_DOCUMENTS_PATH")
	// + DEFAULT_IMAGE_NAME;

	public String TEACHER_PROFILE_PATH = "teachers/";
	public String STUDENT_PROFILE_PATH = "students/";
	public String SCHOOL_ADMIN_PROFILE_PATH = "school_admins/";
	public String PARENT_PROFILE_PATH = "parents/";

	public void createExternalStorage(String parentpath, String folderName)
			throws Exception;

	public void createExternalStorage(String folderName) throws Exception;

	public void createInternalStorage(String parentpath, String folderName)
			throws Exception;

	public void createInternalStorage(String folderName) throws Exception;

	public String createFileInExternalStorage(String parentpath,
			String fileNamewithextendsion, byte[] fileData) throws IOException;

	public String createFileInExternalStorage(String fileNamewithextendsion,
			byte[] fileData) throws IOException;

	public String createFileInInternalStorage(String parentpath,
			String fileNamewithextendsion, byte[] fileData) throws IOException;

	public String createFileInInternalStorage(String fileNamewithextendsion,
			byte[] fileData) throws IOException;
}