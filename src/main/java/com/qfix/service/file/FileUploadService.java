package com.qfix.service.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.opencsv.CSVReader;
import com.qfix.model.FileUploadJob;
import com.qfix.model.IncorrectTransactionIdReport;
import com.qfix.model.PaymentReportDetail;
import com.qfix.model.PaymentReportSummary;
import com.qfix.mq.FileDataInsertDao;

@Slf4j
@Service
public class FileUploadService {
	@Autowired
	FileDataInsertDao fileDataInsertDao;
	
	private final static String ONLINE = "ONLINE";
	private final static String OFFLINE = "OFFLINE";
	
	

	public int getBranchId(String data) throws JsonProcessingException, IOException{
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		
		Integer branchId = ((node != null && node.get("branchId") != null) ? 
								node.get("branchId").asInt()  : null);
		return branchId;
	}

	public Integer insertFileMetaData(MultipartFile file, Integer branchId) {
		return fileDataInsertDao.insertFileMetaData(file, branchId);
	}

	public List<FileUploadJob> getFileUploadJobs(Integer userId, Integer branchId) {
		return fileDataInsertDao.getFileUploadJobs(userId, branchId);
	}

	public FileUploadJob getFileUploadJobById(Integer id) {
		return fileDataInsertDao.getFileUploadJobById(id);
	}
	
	public FileUploadJob getFileUploadJobByBranchId(Integer id) {
		return fileDataInsertDao.getFileUploadJobByBranchId(id);
	}

	public byte[] getInvalidFile(Integer id ) throws Exception{
		FileUploadJob job = fileDataInsertDao.getFileUploadJobById(id);
		if(job != null && !StringUtils.isEmpty(job.getInvalidDataFilePath())){
			return IOUtils.toByteArray(new FileInputStream(new File(job.getInvalidDataFilePath())));
		}
		return null;
	}
	
	public String reportType(MultipartFile file) throws IOException{
		CSVReader reader = readFile(file);
		String[] line = reader.readNext();
		if(line[0].equals("row_type"))
			return OFFLINE;
		else if(line[0].contains("PAYMENT REPORT ON"))
			return ONLINE;
		else 
			return null;
	}
	
	public Integer uploadFile(MultipartFile file, Integer userId) throws IOException, ParseException{
		if(reportType(file).equals(OFFLINE))
			return offlineReport(file, userId);
		else if(reportType(file).equals(ONLINE))
			return paymentReportSummary(file, userId);
		else
			return -1;
	}
	
	private CSVReader readFile(MultipartFile file) throws IOException{
		InputStream is = file.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		CSVReader reader = new CSVReader(br);
		return reader;
	}
	
	private Integer offlineReport(MultipartFile file, Integer userId) throws IOException{
		CSVReader reader = readFile(file);
		Map<String, PaymentReportDetail> map = new HashMap<String, PaymentReportDetail>();
		for(int i = 0; i < 2; i++)
			reader.readNext();
		String [] nextLine;
		while((nextLine = reader.readNext()) != null){
			String transactionId = nextLine[11];
			
			String date = nextLine[5];
			String day = Character.toString(date.charAt(0)) + Character.toString(date.charAt(1)) + "";
			String month = Character.toString(date.charAt(2)) + Character.toString(date.charAt(3)) + "-";
			String year = Character.toString(date.charAt(4)) + Character.toString(date.charAt(5)) + 
					Character.toString(date.charAt(6)) + Character.toString(date.charAt(7)) + "-";
			
			String settlementDate = year + month + day + " 00:00:00";
			String qfixRefNumber = nextLine[24];
			Double paymentAmount = Double.valueOf(nextLine[13]);
			IncorrectTransactionIdReport i = new IncorrectTransactionIdReport();
			i.setAmount(paymentAmount);
			i.setTransactionId(transactionId);
			i.setDate(settlementDate);
			if(!existTransactionId(i, nextLine, OFFLINE))
				continue;
			PaymentReportDetail p = new PaymentReportDetail(transactionId, settlementDate, paymentAmount);
			p.setQfixReferenceNumber(qfixRefNumber);
			addPaymentDetail(map, p);
			log.info(p.toString());
			log.info("Offline settlement date -> " + settlementDate);
			//insertSettlementDate(transactionId, settlementDate, 0, paymentAmount);
		}
		for(PaymentReportDetail p : map.values()){
			log.info("Settlement record -> " + p);
			insertSettlementDate(p.getTransactionID(), p.getSettlementDate(), 0, p.getSettlementAmount(), p.getQfixReferenceNumber());
		}
		return 1;
	}
	
	private Integer paymentReportSummary(MultipartFile file, Integer userId) throws IOException, ParseException{
		CSVReader reader = readFile(file);
		String [] line;
		
		PaymentReportSummary paymentReportSummary = new PaymentReportSummary();
		List<String[]> summaryList = new LinkedList<String[]>();
		Integer id;
		for(int i = 0; i < 14; i++){
			line = reader.readNext();
			if(line[0].equals("") || (line[0].length() == 0) || (line[0] == null) || (line[0].equals("\n") || line[0].equals(" "))){ 
				continue;
			}
			summaryList.add(line);
		}
		
		String username = fileDataInsertDao.getUsernameById(userId);
		
		line = summaryList.get(0);
		String[] split = splitColon(line);
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		Date reportDate = formatter.parse(split[1]);
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String rDate = formatter.format(reportDate);
		paymentReportSummary.setReportDate(rDate);
		paymentReportSummary.setNumberOfPayments(Integer.parseInt(summaryList.get(1)[2]));
		paymentReportSummary.setGrandTotal(Double.valueOf(summaryList.get(2)[2]));
		paymentReportSummary.setSubmerchantCharges(Double.valueOf(summaryList.get(3)[2]));
		paymentReportSummary.setRefundAdjusted(Double.valueOf(summaryList.get(4)[2]));
		paymentReportSummary.setChargebackAdjusted(Double.valueOf(summaryList.get(5)[2]));
		paymentReportSummary.setChargebackReversal(Double.valueOf(summaryList.get(6)[2]));
		paymentReportSummary.setTotalNetAmount(Double.valueOf(summaryList.get(7)[2]));
		paymentReportSummary.setUploadedBy(username);
		log.info(paymentReportSummary.toString());
		id = fileDataInsertDao.insertPaymentSummaryReport(paymentReportSummary);
		insertSettlementDate(reader, id);
		return id;
	}
	
	private void insertSettlementDate(CSVReader reader, Integer id) throws IOException{
		String[] nextLine;
		Map<String, PaymentReportDetail> map = new HashMap<String, PaymentReportDetail>();
		while((nextLine = reader.readNext()) != null){
			if(!isBlank(nextLine))
				break;
		}
		
		nextLine = reader.readNext(); // skipping header
		
		while((nextLine = reader.readNext()) != null){
			if(isBlank(nextLine))
				break;
			String tpslTransactionID = nextLine[4];
			String paymentDate = nextLine[13];
			Double paymentAmount = Double.valueOf(nextLine[7]);
			String qfixReferenceNumber = nextLine[5];
			IncorrectTransactionIdReport i = new IncorrectTransactionIdReport();
			i.setAmount(paymentAmount);
			i.setTransactionId(tpslTransactionID);
			i.setDate(paymentDate);
			if(!existTransactionId(i, nextLine, ONLINE))
				continue;
			log.info(tpslTransactionID + " " + paymentDate);
			PaymentReportDetail p = new PaymentReportDetail(tpslTransactionID, paymentDate, paymentAmount);
			p.setQfixReferenceNumber(qfixReferenceNumber);
			addPaymentDetail(map, p);
			log.info(p.toString());
			//insertSettlementDate(tpslTransactionID, paymentDate, id, paymentAmount);
		}
		for(PaymentReportDetail p : map.values()){
			insertSettlementDate(p.getTransactionID(), p.getSettlementDate(), id, p.getSettlementAmount(), p.getQfixReferenceNumber());
		}
	}
	
	private boolean isBlank(String[] line){
		if(line[0].equals("") || (line[0].length() == 0) || (line[0] == null) || (line[0].equals("\n") || line[0].equals(" "))){ 
			return true;
		}
		return false;
	}
	
	public void insertSettlementDate(String transactionID, String settlementDate, Integer reportSummaryID
									, Double settlementAmount, String qfixReferenceNumber){
		fileDataInsertDao.insertSettlementDate(transactionID, settlementDate, reportSummaryID, settlementAmount, qfixReferenceNumber);
	}
	
	private String[] splitColon(String[] line){
		return line[0].split(":");
	}
	
	private void addPaymentDetail(Map<String, PaymentReportDetail> map, PaymentReportDetail pd){
		if(map.containsKey(pd.getTransactionID())){
			PaymentReportDetail p = map.get(pd.getTransactionID());
			p.setSettlementAmount(p.getSettlementAmount() + pd.getSettlementAmount());
			map.put(pd.getTransactionID(), p);
		}else{
			map.put(pd.getTransactionID(), pd);
		}
	}

	private boolean existTransactionId(IncorrectTransactionIdReport i, String[] line, String type){
		i = fillIncorrectTransactionIdReport(i, line, type);
		if(!fileDataInsertDao.getPaymentTransactionId(i.getTransactionId(), i.getQfixReferenceNumber())){
			log.info(i.getTransactionId() + " OR " + i.getQfixReferenceNumber() + " transaction id doesnt exist");
			fileDataInsertDao.reportIncorrectTransactionId(i);
		}
		return true;
	}
	
	private IncorrectTransactionIdReport fillIncorrectTransactionIdReport(IncorrectTransactionIdReport i, String[] line, String type){
		if(type.equals(OFFLINE)){
			i.setPickupLocation(line[8]);
			i.setPickupPoint(line[9]);
			i.setBankName(line[18]);
			i.setClLocation(line[19]);
			i.setProdCode(line[7]);
			i.setDrawerName(line[22]);
			i.setQfixReferenceNumber(line[24]);
			i.setType(OFFLINE);
		}else if(type.equals(ONLINE)){
			i.setMerchantId(line[1]);
			i.setBankName(line[3]);
			i.setQfixReferenceNumber(line[5]);
			i.setBankTransactionId(line[7]);
			i.setDrawerName(line[14]);
			i.setType(ONLINE);
		}
		log.info(i.toString());
		return i;
	}
}