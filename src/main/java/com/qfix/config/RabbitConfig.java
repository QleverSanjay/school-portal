package com.qfix.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.qfix.config.AppProperties.Rabbit;

//@Configuration
public class RabbitConfig {

	@Autowired
	private AppProperties appProps;

	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		Rabbit rabbit = appProps.getRabbit();
		connectionFactory.setHost(rabbit.getHost());
		connectionFactory.setPort(rabbit.getPort());
		connectionFactory.setUsername(rabbit.getUsername());
		connectionFactory.setPassword(rabbit.getPassword());
		
		return connectionFactory;
	}
	
	@Bean
	Queue queue(){
		Rabbit rabbit = appProps.getRabbit();
		return new Queue(rabbit.getQueue(),true);
	}
	
	TopicExchange topicExchange(){
		Rabbit rabbit = appProps.getRabbit();
		return new TopicExchange(rabbit.getExchange());
	}
	
	@Bean
    Binding binding(Queue queue, TopicExchange exchange) {
		Rabbit rabbit = appProps.getRabbit();
        return BindingBuilder.bind(queue).to(exchange).with(rabbit.getQueue());
    }

	@Bean
	public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
		
		Rabbit rabbit = appProps.getRabbit();
	
		RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setExchange(rabbit.getExchange());
		rabbitTemplate.setRoutingKey(rabbit.getRoutingKey());
		return rabbitTemplate;
	}

	@Bean
	public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
		return new RabbitAdmin(connectionFactory);
	}

}
