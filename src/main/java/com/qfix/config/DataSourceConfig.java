package com.qfix.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.qfix.config.AppProperties.Jdbc;

@Configuration
@EnableTransactionManagement
public class DataSourceConfig {
	@Autowired
	private AppProperties appProperties;

	@Bean
	@Primary
	public DriverManagerDataSource dataSource() {

		Jdbc jdbc = appProperties.getJdbc();
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(jdbc.getDriverClassName());
		dataSource.setUrl(jdbc.getQfixUrl());
		dataSource.setUsername(jdbc.getUsername());
		dataSource.setPassword(jdbc.getPassword());

		return dataSource;
	}
	
	@Bean
	public DriverManagerDataSource paymentDbDataSource(){
		Jdbc jdbc = appProperties.getJdbc();
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(jdbc.getDriverClassName());
		dataSource.setUrl(jdbc.getPaymentUrl());
		dataSource.setUsername(jdbc.getUsername());
		dataSource.setPassword(jdbc.getPassword());

		return dataSource;

	}

	@Bean
	public DataSourceTransactionManager transaction(
			DriverManagerDataSource dataSource) {
		DataSourceTransactionManager transaction = new DataSourceTransactionManager();
		transaction.setDataSource(dataSource);
		return transaction;
	}
}
