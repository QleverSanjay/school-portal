package com.qfix.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "props")
public class AppProperties {

	@NestedConfigurationProperty
	private Jdbc jdbc;

	@NestedConfigurationProperty
	private HazelCast hazelCast;

	@NestedConfigurationProperty
	private FileProps fileProps;
	
	@NestedConfigurationProperty
	private Rabbit rabbit;

	@Data
	@NoArgsConstructor
	public static class Jdbc {
		private String driverClassName;
		private String qfixUrl;
		private String paymentUrl;
		private String username;
		private String password;
	}

	@Data
	@NoArgsConstructor
	public static class HazelCast {

		private String name;
		private String password;
		private String value;
		private int defaultPort;
		private String clientMemeber;
		private String mapConfigName;
		List<String> data = new ArrayList<>();
		Set<String> set = new HashSet(data);
	}
	
	@Data
	@NoArgsConstructor
	public static class FileProps {
		private String baseSystemPath;
		private String externalDocumentsPath;
		private String internalDocumentsPath;
		private String uploadCsvTemplatesPath;
		private String studentUploadFilePath;
	}
	
	@Data
	@NoArgsConstructor
	public static class Rabbit {
		private String host;
		private int port;
		private String username;
		private String password;
		private String routingKey;
		private String fileUploadQueue;
		private String queue;
		private String exchange;
	}
}
