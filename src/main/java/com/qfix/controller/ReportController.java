package com.qfix.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.IReportDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.ReportModel;
import com.qfix.model.StudentReport;
import com.qfix.model.ValidationError;
import com.qfix.utilities.Constants;
import com.qfix.validator.MasterValidator;

@RestController
@RequestMapping(value = "/reports")
public class ReportController extends BaseController{

	@Autowired
	IReportDao reportDao;

	@Autowired
	MasterValidator masterValidator;

	final static Logger logger = Logger.getLogger(ReportController.class);
	@RequestMapping(value = "/uploadReportFile" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session, @RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws IOException {

		String result = null;
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		if (node != null) {
			System.out.println("GOT DATA AS ::::::: "+data);
			Integer branchId = ((node.get("branchId") != null) ? node.get( "branchId").asInt() : null);
			Integer instituteId = ((node.get("instituteId") != null) ? node .get("instituteId").asInt() : null);
			Integer academicYearId = ((node.get("academicYearId") != null) ? node.get( "academicYearId").asInt() : null);
			Integer standardId = ((node.get("standardId") != null) ? node.get( "standardId").asInt() : null);
			Integer divisionId = ((node.get("divisionId") != null) ? node.get( "divisionId").asInt() : null);
			String semester = ((node.get("semester") != null) ? node.get( "semester").asText() : null);
			Integer resultTypeId = ((node.get("resultTypeId") != null) ? node.get( "resultTypeId").asInt() : null);
			String title = ((node.get("title") != null) ? node.get( "title").asText() : null);

			try {
				checkUserBranch(branchId, session);
				if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
					validateAcademicYear(branchId, academicYearId);
					if(Constants.ROLE_ID_TEACHER.equals(session.getAttribute("roleId"))){
						masterValidator.isStandardAndDivisionApplicableForTeacher((Integer) session.getAttribute("userId"), standardId, divisionId);
					}
					else {
						validateStandard(branchId, standardId);
						validateDivision(branchId, standardId, divisionId);
					}
				}

				ReportModel reportModel = new ReportModel();
				reportModel.setBranchId(branchId);
				reportModel.setInstituteId(instituteId);
				reportModel.setAcademicYearId(academicYearId);
				reportModel.setStandardId(standardId);
				reportModel.setDivisionId(divisionId);
				reportModel.setSemester(semester);
				reportModel.setResultTypeId(resultTypeId);
				reportDao.uploadStudntReport(file, reportModel, title);
				result = "{\"status\" : \"success\", \"message\" : \"Student report uploaded successfully.\"," +
						"\"records\" : "+mapper.writeValueAsString(reportDao.getReports(reportModel))+"}";

			} catch (Exception e) {
				logger.error("", e);
				List<ValidationError> validationErrors = reportDao.getValidationErrors();
				if(validationErrors != null && validationErrors.size() > 0){
					ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

					result = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
							"\"records\":"+ow.writeValueAsString(validationErrors)+"}";
				}
				else {
					result = "{\"status\" : \"fail\", \"message\" : \""+e.getMessage()+"\"}";	
				}
			}
		} else {
			result = "{\"status\" : \"fail\", \"message\" : \"Invalid Request.\"}";
		}

		return result;
	}


	@RequestMapping(value = "/downloadReportTemplate" ,method = RequestMethod.GET)
	@ResponseBody
	public void downloadReportTemplate(HttpSession session, @RequestParam(required = false, value = "instituteId") Integer instituteId,
			@RequestParam(value = "branchId") Integer branchId, 
			@RequestParam(value = "academicYearId") Integer academicYearId, 
			@RequestParam(value = "standardId") Integer standardId, 
			@RequestParam(value = "divisionId") Integer divisionId, 
			@RequestParam(value = "semester") String semester,
			@RequestParam(value = "resultTypeId") Integer resultTypeId,
			HttpServletResponse response ) throws IOException, UnauthorizedException {

		checkUserBranch(branchId, session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			validateAcademicYear(branchId, academicYearId);
			if(Constants.ROLE_ID_TEACHER.equals(session.getAttribute("roleId"))){
				masterValidator.isStandardAndDivisionApplicableForTeacher((Integer) session.getAttribute("userId"), standardId, divisionId);
			}
			else {
				validateStandard(branchId, standardId);
				validateDivision(branchId, standardId, divisionId);
			}
		}

		ReportModel reportModel  = new ReportModel();
		reportModel.setInstituteId(instituteId);
		reportModel.setBranchId(branchId);
		reportModel.setAcademicYearId(academicYearId);
		reportModel.setStandardId(standardId);
		reportModel.setDivisionId(divisionId);
		reportModel.setSemester(semester);
		reportModel.setResultTypeId(resultTypeId);

		Workbook workbook = reportDao.generateReportTemplate(reportModel);
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=Report_Template.xls");
		ServletOutputStream outStream = response.getOutputStream();

		try {
			workbook.write(outStream); // Write workbook to response.
		}finally{
			if(outStream != null){
				try {
					outStream.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}


	 /**
	 * This method is used to delete a teacher.
	 * @param id
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	 public String deleteReport(HttpSession session, @RequestParam("ids") Integer[] ids) throws UnauthorizedException {
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 validateReports((List<Integer>)session.getAttribute("branchIdList"), ids);
		 }

		 boolean flag = false;
		 String result = null;
		 flag =	reportDao.deleteReport(Arrays.asList(ids));
		 if(flag == true){
		 		result = "{\"status\":\"success\", \"message\" : \"Student report deleted successfully.\"}";
		 }
		 else{
			result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting student report.\"}";
		 }
		 return result;
	 }


	@RequestMapping(value = "/getReports" ,method = RequestMethod.GET)
	@ResponseBody
	public List<StudentReport> getReports(HttpSession session, 
			@RequestParam(value = "branchId") Integer branchId, 
			@RequestParam(value = "academicYearId") Integer academicYearId,
			@RequestParam(value = "standardId") Integer standardId,
			@RequestParam(value = "divisionId") Integer divisionId, 
			@RequestParam(value = "semester") String semester,
			@RequestParam(value = "resultTypeId") Integer resultTypeId,
			@RequestParam(required = false, value = "rollNumber") String rollNumber,
			HttpServletResponse response ) throws IOException, UnauthorizedException {

		checkUserBranch(branchId, session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			validateAcademicYear(branchId, academicYearId);
			if(Constants.ROLE_ID_TEACHER.equals(session.getAttribute("roleId"))){
				masterValidator.isStandardAndDivisionApplicableForTeacher((Integer) session.getAttribute("userId"), standardId, divisionId);
			}
			else {
				validateStandard(branchId, standardId);
				validateDivision(branchId, standardId, divisionId);
			}
		}

		ReportModel reportModel  = new ReportModel();
		reportModel.setBranchId(branchId);
		reportModel.setAcademicYearId(academicYearId);
		reportModel.setStandardId(standardId);
		reportModel.setDivisionId(divisionId);
		reportModel.setSemester(semester);
		reportModel.setRollNumber(rollNumber);
		reportModel.setResultTypeId(resultTypeId);

		List<StudentReport> studentReportList =reportDao.getReports(reportModel);

		return studentReportList;
	}
}






