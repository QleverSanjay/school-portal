package com.qfix.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qfix.dao.HeadInterfaceDao;
import com.qfix.exceptions.HeadAllreadyExistException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Head;
import com.qfix.utilities.Constants;

@RestController
@RequestMapping(value = "/head")
public class HeadController extends BaseController {

	 final static Logger logger = Logger.getLogger(HeadController.class);
	 @Autowired
	 HeadInterfaceDao headDAO;

	 /**
	 * This method is used to get list of heads.
	 * @return List<Head>
	 * @throws UnauthorizedException 
	 */
	 @RequestMapping(value = "getHeadByBranch" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Head> getAllHeadsByBranch(@RequestParam("branchId") Integer branchId, HttpSession session)throws SQLException, UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return headDAO.getAll(branchId);
	 }
	
	 @RequestMapping(value = "getHeadByBranchForDashboard" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Head> getHeadByBranchForDashboard(@RequestParam("branchId") Integer branchId, HttpSession session) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return headDAO.getHeadByBranchForDashboard(branchId);
	 }


	 /**
	 * This method is called when we add new head or edit head. Based on the head Id it will decide whether to add or edit a head. 
	 * @param head
	 * @throws JsonProcessingException 
	 * @throws UnauthorizedException 
	 * @throws IOException 
	 */
	 @RequestMapping(method = RequestMethod.POST)
	 @ResponseBody
	 public String saveHead(HttpServletRequest request , HttpSession session, @RequestBody Head head) throws JsonProcessingException, UnauthorizedException{
		 String result = null;
		 try {
			 checkUserBranch(head.getBranchId(), session);
			if (head.getId() == null) {
				if(headDAO.insertHead(head)){
					result = "{\"status\":\"success\", \"message\":\"Fees Head inserted successfully\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while inserting fees head\"}";
				}
			}
			else {
				if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
					validateFeeHead(head.getBranchId(), head.getId());
				}
				if(headDAO.updateHead(head)){
					result = "{\"status\":\"success\", \"message\":\"Fees Head updated successfully\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while updating fees head\"}";
				}
			}
		} 
		catch (HeadAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		} 

		 return result;
	}


	 /**
	 * This method is used to get a single head.
	 * @param id
	 * @return Divisions
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Head getHead(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 validateFeeHead((List<Integer>) session.getAttribute("branchIdList"), id);
		 }
		 Head head = headDAO.getHeadById(id);
		 return head;
	 }


	 /**
	 * This method is used to delete a head.
	 * @param id
	 * @throws UnauthorizedException 
     */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	 public String deleteHead(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 validateFeeHead((List<Integer>) session.getAttribute("branchIdList"), id);
		 }
		 String result = "";
		 if(headDAO.deleteHead(id)){
	 		 result = "{\"status\" : \"success\", \"message\" : \"Fees Head deleted successfully.\" }";
		 }
		 else {
			 result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete fees head\" }";
		 }
		 return result;
	 }	
}
