package com.qfix.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.dao.IAdmissionDao;

@Controller
@RequestMapping(value = "/admission")
public class AdmissionController {

	@Autowired
	IAdmissionDao admissionDao;

	@RequestMapping(value = "/save", method = RequestMethod.GET)
	@ResponseBody
	public void save() {
		admissionDao.save(null);
	}
}
