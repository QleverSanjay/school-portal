package com.qfix.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.dao.IIndividualDao;
import com.qfix.model.Individual;
import com.qfix.model.User;
import com.qfix.utilities.Constants;

@RestController
@RequestMapping(value = "/individuals")
public class IndividualController {

	
	User user;
	
	@Autowired
	IIndividualDao individualDao;
	
	 /**
	 * This method is used to get list of students.
	 * @return List<Students>
	 */
	 @RequestMapping(value = "/search" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Individual> getStudentsBySearchData(HttpSession session, @RequestParam(value = "branchId") Integer branchId,
			 @RequestParam(value = "name",required = false) String name,
			 @RequestParam(value = "allreadySelectedIdss")List<Integer> allreadySelectedIds ){
		 
		 List<Individual> individualList = individualDao.getIndividualsByName(name, branchId,allreadySelectedIds );

		 return individualList;
	 }

	 /**
	 * This method is used to get list of students.
	 * @return List<Students>
	 */
	 @RequestMapping(value = "/searchForGroup" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Individual> getIndividualBySearchData(HttpSession session, @RequestParam(value = "branchId") Integer branchId,
			 @RequestParam(value = "name",required = true) String name,
			 @RequestParam(value = "groupId",required = false) Integer groupId,
			 @RequestParam(value = "allreadySelectedIdss")List<Integer> allreadySelectedIds ){

		 List<Individual> individualList = individualDao.getIndividualsByNameExceptGroup(name, groupId, branchId, allreadySelectedIds );

		 return individualList;
	 }

/*
	 *//**
	 * This method is used to get list of students.
	 * @return List<Students>
	 *//*
	 @RequestMapping(value = "/searchForGroupWhenTeacher" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Individual> searchForGroupWhenTeacher(HttpSession session, @RequestParam(value = "teacherUserId") Integer teacherUserId,
			 @RequestParam(value = "name",required = true) String name,
			 @RequestParam(value = "groupId",required = false) Integer groupId,
			 @RequestParam(value = "allreadySelectedIdss")List<Integer> allreadySelectedIds ){

		 List<Individual> individualList = individualDao.getIndividualsByNameExceptGroupWhenTeacher(name, groupId, teacherUserId, allreadySelectedIds );

		 return individualList;
	 }*/

	 /**
	 * This method is used to get list of students.
	 * @return List<Students>
	 */
	 @RequestMapping(value = "/searchForNotice" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Individual> getStudentsForNotice(HttpSession session, 
			 @RequestParam(value = "branchId",required = true) Integer branchId,
			 @RequestParam(value = "name",required = false) String name, 
			 @RequestParam(value = "allreadySelectedIdss")List<Integer> allreadySelectedIds ){
		 
		 user = (User)session.getAttribute("currentUser");
		 
		 List<Individual> individualList = individualDao.getStudentsForNoticeSearch(name,branchId,allreadySelectedIds);
		 
		 
		 return individualList;
	 }
	 
	 
	 @RequestMapping(value = "/searchForEdiary" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Individual> getStudentsForEdiary(HttpSession session, 
			 @RequestParam(value = "branchId",required = true) Integer branchId,
			 @RequestParam(value = "name",required = false) String name, 
			 @RequestParam(value = "allreadySelectedIdss")List<Integer> allreadySelectedIds ){
		 
		 user = (User)session.getAttribute("currentUser");
		 
		 List<Individual> individualList = individualDao.getStudentsForEdiarySearch(name,branchId,allreadySelectedIds);
		 
		 
		 return individualList;
	 }
	 
	 
	 @RequestMapping(value = "/groupSearch" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Individual> getStudentGroupBySearchData(HttpSession session,@RequestParam(value = "name",required = false) String name,
			 @RequestParam(value = "allreadySelectedIdss")List<Integer> allreadySelectedIds ){
		 user = (User)session.getAttribute("currentUser");
		 
		 List<Individual> individualList = individualDao.getIndividualsGroupByName(name,
				 (user.getRoleId() == Constants.ROLE_ID_SCHOOL_ADMIN ? user.getInstituteId(): null),allreadySelectedIds);
		 
		 return individualList;
	 }
}
