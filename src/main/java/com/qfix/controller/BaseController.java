package com.qfix.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.qfix.daoimpl.BaseDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.utilities.Constants;

public class BaseController {

	private static final Logger log = Logger.getLogger(BaseController.class);

	@Autowired
	BaseDao baseDAO;

	void checkUserBranch(Integer branchId, HttpSession session)
			throws UnauthorizedException {

		log.debug("User accessing branch ** ::::::::::" + branchId);
		@SuppressWarnings("unchecked")
		List<Integer> branchIdList = (List<Integer>) session
				.getAttribute("branchIdList");
		System.out.println(branchIdList);
		
		if(Constants.ROLE_ID_BANK_PARTNER.equals(session.getAttribute("roleId")))
			return;
		
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))
				&& (branchIdList == null || branchIdList.size() == 0 || !branchIdList
						.contains(branchId))) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch.");
		}
	}

	void validateAcademicYear(Integer branchId, Integer academicYearId)
			throws UnauthorizedException {
		log.debug("User accessing academic year ** ::::::::::" + academicYearId);

		if (!baseDAO.validateAcademicYear(branchId, academicYearId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or academic year.");
		}
	}

	void validateAcademicYear(List<Integer> branchIds, Integer academicYearId,
			boolean includeCurrentActiveYear) throws UnauthorizedException {
		log.debug("User accessing academic year ** ::::::::::" + academicYearId);

		if (!baseDAO.validateAcademicYear(branchIds, academicYearId,
				includeCurrentActiveYear)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or academic year.");
		}
	}

	void validateFeesCode(List<Integer> branchIdList, Integer feesCodeId)
			throws UnauthorizedException {
		log.debug("User accessing Fees Code ** :::::::::" + feesCodeId);
		if (!baseDAO.validateFeesCode(branchIdList, feesCodeId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belongs to this branch or Fees code");
		}
	}

	/*
	 * void validateAcademicYear(List<Integer> branchIds, Integer
	 * academicYearId) throws UnauthorizedException {
	 * log.debug("User accessing academic year ** ::::::::::" + academicYearId);
	 * 
	 * if (!baseDAO.validateAcademicYear(branchIds, academicYearId)) { throw new
	 * UnauthorizedException(
	 * "Error occured as user do not belong to this branch or academic year.");
	 * } }
	 */

	void validateForumWithBranch(List<Integer> branchIdList, Integer forumId)
			throws UnauthorizedException {
		if (!baseDAO.validateForumWithBranch(branchIdList, forumId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or forum.");
		}
	}

	void validateFeeHead(Integer branchId, Integer feeHeadId)
			throws UnauthorizedException {
		if (!baseDAO.validateFeeHead(branchId, feeHeadId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or fee head.");
		}
	}

	void validateFee(List<Integer> branchIds, Integer feeId)
			throws UnauthorizedException {
		if (!baseDAO.validateFee(branchIds, feeId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or fee.");
		}
	}

	void validateSubject(List<Integer> branchIdList, Integer subjectId)
			throws UnauthorizedException {
		if (!baseDAO.validateSubject(branchIdList, subjectId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or subject");
		}
	}

	void validateStudent(List<Integer> branchIds, Integer studentId)
			throws UnauthorizedException {
		if (!baseDAO.validateStudent(branchIds, studentId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or student.");
		}
	}

	void validateEvent(List<Integer> branchIds, Integer eventId)
			throws UnauthorizedException {
		if (!baseDAO.validateEvent(branchIds, eventId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or event.");
		}
	}

	void validateWorkingDay(List<Integer> branchIds, Integer workingDayId)
			throws UnauthorizedException {
		if (!baseDAO.validateWorkingDay(branchIds, workingDayId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or Working Day.");
		}
	}

	void validateHoliDay(List<Integer> branchIds, Integer holidayDayId)
			throws UnauthorizedException {
		if (!baseDAO.validateHoliDay(branchIds, holidayDayId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or holiday.");
		}
	}

	void validateStandard(Integer branchId, Integer standardId)
			throws UnauthorizedException {
		List<Integer> branchList = new ArrayList<>();
		branchList.add(branchId);
		validateStandard(branchList, standardId);
	}

	void validateDivision(Integer branchId, Integer standardId,
			Integer divisionId) throws UnauthorizedException {
		List<Integer> branchList = new ArrayList<>();
		branchList.add(branchId);
		validateDivision(branchList, standardId, divisionId);
	}

	void validateStandard(List<Integer> branchIds, Integer standardId)
			throws UnauthorizedException {
		if (!baseDAO.validateStandard(branchIds, standardId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard.");
		}
	}

	void validateNotice(List<Integer> branchIds, Integer noticeId)
			throws UnauthorizedException {
		if (!baseDAO.validateNotice(branchIds, noticeId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or notice.");
		}
	}
	
	void validateAssignments(List<Integer> branchIds, Integer assignmentsId)
			throws UnauthorizedException {
		if (!baseDAO.validateAssignments(branchIds, assignmentsId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or notice.");
		}
	}

	void validateDivision(List<Integer> branchIds, Integer standardId,
			Integer divisionId) throws UnauthorizedException {
		if (!baseDAO.validateDivisionWithStandard(branchIds, standardId,
				divisionId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard-division.");
		}
	}

	void validateFeesScheduleIds(List<Integer> branchIds, List<Long> paymentIds)
			throws UnauthorizedException {
		if (!baseDAO.validateFeesScheduleIds(branchIds, paymentIds)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard-division.");
		}
	}

	void checkEventBelongsToThisUser(Integer eventId, Integer userId)
			throws UnauthorizedException {
		if (!baseDAO.checkEventBelongsToThisUser(eventId, userId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this event.");
		}
	}

	void checkEventCreatedByThisUser(Integer eventId, Integer userId)
			throws UnauthorizedException {
		if (!baseDAO.checkEventCreatedByThisUser(eventId, userId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this event.");
		}
	}

	void checkNoticeBelongsToThisUser(Integer noticeId, Integer userId)
			throws UnauthorizedException {
		if (!baseDAO.checkNoticeBelongsToThisUser(noticeId, userId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this notice.");
		}
	}

	void checkNoticeCreatedByThisUser(Integer noticeId, Integer userId)
			throws UnauthorizedException {
		if (!baseDAO.checkNoticeCreatedByThisUser(noticeId, userId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this notice.");
		}
	}
	
	void checkAssignmentsCreatedByThisUser(Integer assignmentsId, Integer userId)
			throws UnauthorizedException {
		if (!baseDAO.checkNoticeCreatedByThisUser(assignmentsId, userId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this assignments.");
		}
	}

	void validateFeeHead(List<Integer> branchIds, Integer eventId)
			throws UnauthorizedException {
		if (!baseDAO.validateFeeHead(branchIds, eventId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard-division.");
		}
	}

	void validateTeacher(Integer branchId, Integer teacherId)
			throws UnauthorizedException {
		List<Integer> branchIds = new ArrayList<>();
		branchIds.add(branchId);
		validateTeacher(branchIds, teacherId);
	}

	void validateTeacher(List<Integer> branchIds, Integer teacherId)
			throws UnauthorizedException {
		if (!baseDAO.validateTeacher(branchIds, teacherId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard-division.");
		}
	}

	void validateTimetable(List<Integer> branchIds, Integer timetableId)
			throws UnauthorizedException {
		if (!baseDAO.validateTimetable(branchIds, timetableId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard-division.");
		}
	}

	void validateTimetable(Integer branchId, Integer timetableId)
			throws UnauthorizedException {
		List<Integer> branchIds = new ArrayList<>();
		branchIds.add(branchId);
		validateTimetable(branchIds, timetableId);
	}

	void validateGroup(Integer branchId, Integer groupId,
			boolean checkActiveYear, Integer createdBy)
			throws UnauthorizedException {
		List<Integer> branchIds = new ArrayList<>();
		branchIds.add(branchId);
		validateGroup(branchIds, groupId, checkActiveYear, createdBy);
	}

	void validateGroup(List<Integer> branchIds, Integer groupId,
			boolean checkActiveYear, Integer createdBy)
			throws UnauthorizedException {
		if (!baseDAO.validateGroup(branchIds, groupId, checkActiveYear,
				createdBy)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or group.");
		}
	}

	void validateReports(List<Integer> branchIds, Integer[] reportIds)
			throws UnauthorizedException {
		if (!baseDAO.validateReports(branchIds, reportIds)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or student report.");
		}
	}

	void validateSubjectForStandardSubject(List<Integer> branchIds,
			Integer standardId) throws UnauthorizedException {
		if (!baseDAO.validateStandardSubject(branchIds, standardId)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard-subject.");
		}
	}

	void checkForumCreatedByThisUser(Integer forumId, Integer userId,
			boolean checkActiveYear) throws UnauthorizedException {
		if (!baseDAO.checkForumCreatedByThisUser(forumId, userId,
				checkActiveYear)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or standard-subject.");
		}
	}

	void validateRegistrationCode(Integer branchId, String registrationCode)
			throws UnauthorizedException {
		if (!baseDAO.validateRegistrationCode(branchId, registrationCode)) {
			throw new UnauthorizedException(
					"registrationCode is not available please enter valid registration code");
		}

	}

	void validateDisplayLayout(Integer displayLayoutId, Integer branchId)
			throws UnauthorizedException {
		List<Integer> branchIds = new ArrayList<>();
		branchIds.add(branchId);
		validateDisplayLayout(displayLayoutId, branchIds);
	}

	void validateDisplayLayout(Integer displayLayoutId, List<Integer> branchIds)
			throws UnauthorizedException {
		if (!baseDAO.validateDisplayLayout(displayLayoutId, branchIds)) {
			throw new UnauthorizedException(
					"registrationCode is not available please enter valid registration code");
		}
	}


	void validateLateFees(Integer lateFeesId, List<Integer> branchIds) throws UnauthorizedException {
		if (!baseDAO.validateLateFees(lateFeesId, branchIds)) {
			throw new UnauthorizedException(
					"registrationCode is not available please enter valid registration code");
		}
	}

	public void validateDisplayHead(Integer displayHeadId,
			List<Integer> branchIds) throws UnauthorizedException {
		if (!baseDAO.validateDisplayHead(displayHeadId, branchIds)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or group.");
		}
	}
	
	public void validateDisplayTemplate(Integer displayTemplateId,
			List<Integer> branchIds) throws UnauthorizedException {
		if (!baseDAO.validateDisplayTemplate(displayTemplateId, branchIds)) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch or group.");
		}
	}
}
