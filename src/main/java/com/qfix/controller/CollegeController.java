package com.qfix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.dao.CollegeDao;
import com.qfix.model.CollegeYear;
import com.qfix.model.Course;
import com.qfix.model.University;

@RestController
@RequestMapping(value="/college")
public class CollegeController {
	
	@Autowired
	private CollegeDao collegeDao; 

	@RequestMapping(value="/universities")
	public List<University> listUniversities(){
		return collegeDao.listUniversitites();
	}
	
	@RequestMapping(value="/years")
	public List<CollegeYear> listYears(){
		return collegeDao.listYears();
	}
	
	@RequestMapping(value="/courses")
	public List<Course> listCourses(){
		return collegeDao.listCourses();
	}
}
