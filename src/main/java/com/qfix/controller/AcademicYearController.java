package com.qfix.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.qfix.dao.IAcademicYearDao;
import com.qfix.dao.IHolidayDao;
import com.qfix.dao.IWorkingDayDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.exceptions.WorkingDayAllreadyExistException;
import com.qfix.exceptions.YearAllreadyExistException;
import com.qfix.model.AcademicYear;
import com.qfix.model.Holiday;
import com.qfix.model.ValidationError;
import com.qfix.model.WorkingDay;
import com.qfix.model.wrapper.AcademicYearWrapper;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.HolidayValidator;
import com.qfix.validator.MasterValidator;

@RestController
@RequestMapping(value = "/academicyear")
public class AcademicYearController extends BaseController {

	@Autowired
	private IAcademicYearDao academicYearDao;

	@Autowired
	private MasterValidator masterValidator;

	@Autowired
	IWorkingDayDao workingDayDao;

	@Autowired
	IHolidayDao holidayDao;

	@Autowired
	HolidayValidator holidayValidator;
	
	@Autowired
	private Gson gson;

	final static Logger logger = Logger.getLogger(AcademicYearController.class);

	@RequestMapping(value = "/getAcademicYearByBranch", method = RequestMethod.GET)
	public List<AcademicYear> getAcademicYear(HttpSession session,
			HttpServletResponse response,
			@RequestParam("branchId") Integer branchId) throws Exception {
		checkUserBranch(branchId, session);
		return academicYearDao.getAll(branchId);
	}

	/**
	 * 
	 * @param session
	 * @param instituteId
	 * @param branchId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getActiveYearByBranch", method = RequestMethod.GET)
	public List<AcademicYear> getActiveYear(HttpSession session,
			HttpServletResponse response,
			@RequestParam(value = "branchId") Integer branchId)
			throws Exception {
		checkUserBranch(branchId, session);
		return academicYearDao.getAll(branchId);
	}

	/**
	 * This method is called when we add new Fees or edit Fees. Based on the Fee
	 * Id it will decide whether to add or edit a Fee.
	 * 
	 * @param Fees
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public String saveAcademicYear(HttpServletResponse response,
			@RequestBody AcademicYear academicYear, HttpSession session)
			throws Exception {

		boolean flag = false;
		String result = null;

		try {
			checkUserBranch(academicYear.getBranchId(), session);

			if (academicYear.getId() != null) {
				if (!session.getAttribute("roleId").equals(
						Constants.ROLE_ID_ADMIN)) {
					validateAcademicYear(
							(List<Integer>) session
									.getAttribute("branchIdList"),
							academicYear.getId(), false);
				}

				flag = academicYearDao.updateAcademicYear(academicYear);
				if (flag) {
					result = "{\"status\":\"success\", \"message\" : \"Academic year updated successfully.\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating academic year.\"}";
				}
			} else {
				flag = academicYearDao.insertAcademicYear(academicYear);
				if (flag) {
					Integer lastInsertedId = academicYearDao
							.getAcademicYearidByDates(
									academicYear.getFromDate(),
									academicYear.getToDate(),
									academicYear.getBranchId());
					result = "{\"status\":\"success\", \"message\" : \"Academic year added successfully.\", \"lastInsertedId\" : "
							+ lastInsertedId + "}";
				} else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding academic year.\"}";
				}
			}
		} catch (YearAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\"" + e.getMessage()
					+ "\"}";
			logger.error("", e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
		logger.debug("save Acadmic year response --" + result);
		return result;
	}

	/**
	 * This method is used to get a single fee.
	 * 
	 * @param id
	 * @return fee
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public AcademicYear getAcademicYear(HttpSession session,
			@PathVariable Integer id) throws UnauthorizedException {
		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateAcademicYear(
					(List<Integer>) session.getAttribute("branchIdList"), id,
					false);
		}

		AcademicYear academicYear = academicYearDao.getYearById(id);
		logger.debug("getAcadmic year response --" + academicYear);
		return academicYear;
	}

	/**
	 * This method is used to delete a fee.
	 * 
	 * @param id
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	public String deleteAcademicYear(HttpSession session,
			@RequestParam Integer id) throws UnauthorizedException {
		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateAcademicYear(
					(List<Integer>) session.getAttribute("branchIdList"), id,
					false);
		}

		boolean flag = false;
		String result = null;
		try {
			flag = academicYearDao.deleteAcademicYear(id);
			if (flag == true) {
				result = "{\"status\":\"success\", \"message\" : \"Year deleted successfully.\"}";
			} else {
				result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting year.\"}";
			}
		} catch (Exception e) {
			result = "{\"status\":\"fail\", \"message\" : \"" + e.getMessage()
					+ "\"}";
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/express", method = RequestMethod.POST)
	public String saveExpressOnboardAcademicYear(
			@RequestBody AcademicYearWrapper academicYearWrapper,
			HttpSession session) throws Exception {
		boolean flag = false;
		String result = null;
		HashMap<String,Object> response;
		AcademicYear academicYear = academicYearWrapper.getAcademicYear();
		WorkingDay workingDay = academicYearWrapper.getWorkingDay();
		List<Holiday> holidays = academicYearWrapper.getHolidays();
		Integer branchId = academicYearWrapper.getBranchId();

		Integer currentAcademicYearId = academicYear.getId();
		Integer workingDayId = null;
		if(workingDay != null){			
		 workingDayId= workingDay.getId();
		}

		try {
			checkUserBranch(branchId, session);
		} catch (UnauthorizedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if (currentAcademicYearId != null) { 
				if (!session.getAttribute("roleId").equals(
						Constants.ROLE_ID_ADMIN)) {
					validateAcademicYear(
							(List<Integer>) session
									.getAttribute("branchIdList"),
							academicYear.getId(), false);
					
				}
				flag = academicYearDao.updateAcademicYear(academicYear);
				if (!flag) {
					return "{\"status\":\"fail\", \"message\":\"Something bad happened, try again after sometime\"}";
				}
			} else {
				flag = academicYearDao.insertAcademicYear(academicYear);
				if (flag) {
					currentAcademicYearId = academicYearDao
							.getAcademicYearidByDates(
									academicYear.getFromDate(),
									academicYear.getToDate(),
									academicYearWrapper.getBranchId());
				} else {
					return "{\"status\":\"fail\", \"message\":\"Something bad happened, try again after sometime\"}";
				}
			}
		} catch (YearAllreadyExistException e) {
//			logger.error("", e);
//			return "{\"status\":\"fail\", \"message\":\"" + e.getMessage()
//					+ "\"}";
//			continue;
		} 

		if (workingDay != null) {
//			workingDayDao.deleteWorkingDay(branchId);
			workingDay.setCurrentAcademicYearId(currentAcademicYearId);
			if (workingDay.getId() != null) {
				if (!Constants.ROLE_ID_ADMIN.equals(session
						.getAttribute("roleId"))) {
					validateWorkingDay(
							(List<Integer>) session
									.getAttribute("branchIdList"),
							workingDay.getId());
				}
				flag = workingDayDao.updateWorkingDay(workingDay);
				if (!flag) {
					response = new HashMap<String, Object>();
					response.put("status", "fail");
					response.put("message", "Something bad happened, try again after sometime");
					response.put("academicYearId", currentAcademicYearId);
					return getJson(response);
				}
			} else if(workingDay != null){
				try{
				flag = workingDayDao.insertWorkingDay(workingDay);
					if (flag) {
						workingDayId = workingDayDao.getCurrentWorkingDayForBranch(
								branchId).getId();
					}
				}catch(WorkingDayAllreadyExistException e){
					academicYearDao.deleteAcademicYear(currentAcademicYearId);
					response = new HashMap<String, Object>();
					response.put("status", "fail");
					response.put("message", "Working days already exist for this branch");
					return getJson(response);	
				}
			}
		}

		if (holidays != null && holidays.size() > 0) {
			try {
				Holiday holiday = holidays.get(0);
				checkUserBranch(holiday.getBranchId(), session);

				if (!Constants.ROLE_ID_ADMIN.equals(session
						.getAttribute("roleId"))) {
					validateAcademicYear(holiday.getBranchId(),
							holiday.getCurrentAcademicYearId());
				}

				holidayValidator.setUpload(false);
				List<ValidationError> errors = new ArrayList<ValidationError>();
				for (Holiday tempHoliday : holidays) {
					ValidationError error = ValidatorUtil.validate(tempHoliday,
							holidayValidator);
					if (error != null) {
						academicYearDao.deleteAcademicYear(currentAcademicYearId);
						workingDayDao.deleteWorkingDay(workingDayId);
						
						String title = holiday.getTitle();
						error.setTitle(title);
						errors.add(error);
						ObjectMapper mapper = new ObjectMapper();
						response = new HashMap<String, Object>();
						response.put("status","fail");
						response.put("isValidationError", "true");
						response.put("message", error.getErrors().get(0));
						response.put("records", mapper.writeValueAsString(errors.get(0).getErrors()));
						response.put("academicYearId", currentAcademicYearId);
						response.put("workingDayId", workingDayId);
						return getJson(response);
					}
				}
				
				holidayDao.deleteHolidays(branchId);
				for (Holiday tempHoliday : holidays) {
					tempHoliday.setCurrentAcademicYearId(currentAcademicYearId);
					flag = holidayDao.insertHoliday(tempHoliday);
					if (flag) {
						result = "{\"status\":\"success\", \"message\" : \"Holiday added successfully.\"}";
					} else {
						result = "{\"status\":\"fail\", \"message\" : \"Problem while adding holiday.\",\"academicYearId\" : "
							+ currentAcademicYearId + ",\"workingDayId\" : "
							+ workingDayId + "}";
						academicYearDao.deleteAcademicYear(currentAcademicYearId);
						workingDayDao.deleteWorkingDay(workingDayId);
						response = new HashMap<String, Object>();
						response.put("status","fail");
						response.put("message", "Cannot add holidays. Try again after sometime");
						response.put("academicYearId", currentAcademicYearId);
						response.put("workingDayId", workingDayId);
						return getJson(response);
					}
				}
			} catch (Exception e) {
				logger.error("Problem while updating holiday...", e);
			}
		}
		
		response = new HashMap<String,Object>();
		response.put("status", "success");
		response.put("academicYearId", currentAcademicYearId);
		response.put("workingDayId", workingDayId);
		response.put("message", "Academic year saved");

		return getJson(response);
	}
	
	private String getJson(HashMap<String,Object> response){
		return gson.toJson(response);
	}
}
