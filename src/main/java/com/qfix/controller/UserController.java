package com.qfix.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.daoimpl.UserDao;
import com.qfix.exceptions.InvalidCredintialsException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.ForgotPasswordModel;
import com.qfix.model.User;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	final static Logger logger = Logger.getLogger(UserController.class);
	
	@Autowired
	private UserDao userDao;
	 
	/* @RequestMapping(value = "/login",method = RequestMethod.POST)
	 @ResponseBody*/
	 
		/*public String submitLogin(@RequestBody User user, HttpServletRequest request, HttpServletResponse response ) {
		
		boolean flag = false;
		String result = null;
		HttpSession session = request.getSession();
		String username = user.getUsername();
		String password = user.getPassword();
		
		try {
			
			if(username != null && password != null){
				flag = new UserDao().isValidUser(username, password);
				if(flag){
					user = new UserDao().getUserByUsername(username);
					session.setAttribute("currentUser", user);
					ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
					result = "{\"RESULT\":\"OK\", \"MESSAGE\" : \"Login successfully.\", \"record\" : "+ow.writeValueAsString(user)+"}";
				}
				else {
					result = "{\"RESULT\":\"ERROR\", \"MESSAGE\" : \"Invalid UserName or Password.\"}";
				}
			}
			
		} catch (Exception e) {
			logger.error("", e);
		}
		 
		return result;
	}
	 */
	
	
	 @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
	 @ResponseBody
	 public String forgotPassword(@RequestBody String username, HttpSession session){
		 String result = "";
		 try {
			 String otp = new UserDao().sendForgotPasswordEmail(username);
			 if(otp != null){
				 ForgotPasswordModel forgotPasswordModel = new ForgotPasswordModel();
				 forgotPasswordModel.setUsername(username);
				 forgotPasswordModel.setOtp(otp);
				 
				 session.setAttribute("forgotPasswordOTP", forgotPasswordModel);
				 result = "{\"status\" : \"success\", \"message\" : " +
				 		"\"OTP sent on your registered email address please use this to change your password\"}";	 
			 }
			 else {
				 result = "{\"status\" : \"fail\", \"message\" : " +
					 		"\"Can`t generate otp for this username.\"}";
			 }
			 
			 
		} catch (Exception e) {
			if(e instanceof InvalidCredintialsException ){
				result = "{\"status\" : \"fail\", \"message\" : " +
				 		"\""+e.getMessage()+"\"}";
			}
			/*else {
				result = "{\"status\" : \"fail\", \"message\" : " +
				 		"\"Problem while sending otp.\"}";	
			}*/
			logger.error("", e);
		}
		 return result;
	 }
	 
	 @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	 @ResponseBody
	 public String changePassword(@RequestBody ForgotPasswordModel forgotPasswordModel, HttpSession session){
		 String result = "";
		 try {
			 ForgotPasswordModel forgotPasswordModelFromSession = (ForgotPasswordModel) session.getAttribute("forgotPasswordOTP");

			 if(forgotPasswordModelFromSession.getOtp().equals(forgotPasswordModel.getOtp())){
				 if(new UserDao().changePassword(forgotPasswordModel)){
					 result = "{\"status\" : \"success\", \"message\" : " +
						 		"\"Password has been chnaged please login.\"}";
					 session.removeAttribute("forgotPasswordOTP");
				 }
				 else {
					 result = "{\"status\" : \"fail\", \"message\" : " +
						 		"\"Problem while chnaging password.\"}";
				 }
			 }
			 else {
				 result = "{\"status\" : \"fail\", \"message\" : " +
					 		"\"Incorrect OTP\"}";
			 }			 
		} catch (Exception e) {
			result = "{\"status\" : \"fail\", \"message\" : " +
			 		"\"Problem with server please call administrator.\"}";
			logger.error("", e);
		}
		 return result;
	 }

	 @RequestMapping(value = "/user-role",method = RequestMethod.POST)
	 @ResponseBody
	public User getRoleBasedOnUser(HttpSession session, HttpServletResponse response, @RequestBody User user) throws SQLException, UnauthorizedException {
		 try {
			 user = userDao.getUserDetails(user.getUsername(), user.getPassword(), true);
			 if(user != null){
				 session.setAttribute("token", user.getToken());
				 session.setAttribute("currentUser", user);
				 session.setAttribute("roleId", user.getRoleId());
				 session.setAttribute("userId", user.getId());
				 session.setAttribute("branchId", user.getBranchId());
				 session.setAttribute("instituteId", user.getInstituteId());
				 session.setAttribute("branchIdList", user.getBranchIdList());
				 session.setMaxInactiveInterval(60*30);
			 }
		 }catch(Exception e){
			 response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			 throw e;
		 }
		return user;
	}

	 @RequestMapping(value = "/logout", method = RequestMethod.GET)
	 @ResponseBody
	 public void logout(HttpServletRequest request, HttpServletResponse response){
		 HttpSession session = request.getSession(false);
		 if(session != null) {
			 session.invalidate();
		 }
	 }
}
