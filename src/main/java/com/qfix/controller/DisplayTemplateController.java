package com.qfix.controller;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.qfix.dao.IDisplayTemplateDao;
import com.qfix.exceptions.ApplicationException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.DisplayHead;
import com.qfix.model.DisplayTemplate;
import com.qfix.model.ValidationError;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.DisplayTemplateValidator;

@RestController
public class DisplayTemplateController extends BaseController {
	final static Logger logger = Logger
			.getLogger(DisplayTemplateController.class);

	@Autowired
	IDisplayTemplateDao displayTemplateDao;

	@Autowired
	DisplayTemplateValidator displayTemplateValidator;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/displayHead", method = RequestMethod.POST)
	public String saveDisplayHead(HttpSession session,
			@RequestBody @NotNull @Valid DisplayHead head)
			throws UnauthorizedException {

		checkUserBranch(head.getBranchId(), session);
		String response = "{\"status\":\"success\", \"message\":\"Display Head Saved.\"}";
		try {
			if (head.getId() != null) {
				if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
					validateDisplayHead(head.getId(),
							(List<Integer>) session.getAttribute("branchIdList"));
				}
				displayTemplateDao.updateDisplayHead(head);
			} else {
				displayTemplateDao.insertDisplayHead(head);
				response = "{\"status\":\"success\", \"message\":\"Display Head Saved.\", \"generated_id\" : "+head.getId()+"}";
			}
		} catch (ApplicationException e) {
			logger.error("", e);
			response = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
		}
		return response;
	}

	@RequestMapping(value = "/displayHeads", method = RequestMethod.GET)
	public List<DisplayHead> getAllDisplayHead(HttpSession session,
			@NotNull @RequestParam Integer branchId)
			throws UnauthorizedException {

		checkUserBranch(branchId, session);
		return displayTemplateDao.getDisplayHeads(branchId);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/displayHead", method = RequestMethod.GET)
	public DisplayHead getDisplayHead(HttpSession session,
			@NotNull @RequestParam Integer displayHeadId)
			throws UnauthorizedException {

		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateDisplayHead(displayHeadId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}
		return displayTemplateDao.getDisplayHead(displayHeadId);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/displayHead", method = RequestMethod.DELETE)
	public void deleteDisplayHead(HttpSession session,
			@NotNull @RequestParam Integer displayHeadId)
			throws UnauthorizedException {

		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateDisplayHead(displayHeadId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}
		displayTemplateDao.deleteDisplayHead(displayHeadId);
	}

 //*** Display Templates Starts here.....

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/displayTemplate", method = RequestMethod.POST)
	public String saveDisplayTemplate(HttpSession session,
			@RequestBody @NotNull @Valid DisplayTemplate template)
					throws UnauthorizedException {

		checkUserBranch(template.getBranchId(), session);
		String response = "{\"status\":\"success\", \"message\":\"Display Template Saved.\"}";
		try {
			if(template.getDisplayHeadList() != null ){
				int i =0;
				for(DisplayHead head : template.getDisplayHeadList()){
					head.setDisplayOrder(i++);
				}
			}
			if (template.getId() != null) {
				if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
					validateDisplayTemplate(template.getId(),
							(List<Integer>) session.getAttribute("branchIdList"));
				}
				displayTemplateDao.updateDisplayTemplate(template);
			} else {
				displayTemplateDao.insertDisplayTemplate(template);
			}
		}catch(ApplicationException e){
			logger.error("", e);
			response = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
		}
		return response;
	}

	@RequestMapping(value = "/displayTemplates", method = RequestMethod.GET)
	public List<DisplayTemplate> getAllDisplayTemplate(HttpSession session,
			@NotNull @RequestParam Integer branchId)
					throws UnauthorizedException {
		
		checkUserBranch(branchId, session);
		return displayTemplateDao.getDisplayTemplates(branchId);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/displayTemplate", method = RequestMethod.GET)
	public DisplayTemplate getDisplayTemplate(HttpSession session,
			@NotNull @RequestParam Integer displayTemplateId)
					throws UnauthorizedException {
		
		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateDisplayTemplate(displayTemplateId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}
		return displayTemplateDao.getDisplayTemplate(displayTemplateId);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/displayTemplate", method = RequestMethod.DELETE)
	public String deleteDisplayTemplate(HttpSession session,
			@NotNull @RequestParam Integer displayTemplateId)
					throws UnauthorizedException {

		String response = "{\"status\":\"success\", \"message\":\"Display Template Deleted.\"}";
		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateDisplayTemplate(displayTemplateId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}
		try {
			displayTemplateDao.deleteDisplayTemplate(displayTemplateId);
		}catch(Exception e){
			logger.error("", e);
			response = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
		}
		return response;
	}

	@RequestMapping(value = "/displayHead/search", method = RequestMethod.GET)
	public List<DisplayHead> searchDisplayHeads(HttpSession session, @NotNull @RequestParam(value = "search_text", required = true) String searchText,
					@RequestParam(value = "exclude_head_id", required = false) String excludeIds,
					@RequestParam(value = "branch_id", required = true) Integer branchId) throws Exception {

		checkUserBranch(branchId, session);

		return displayTemplateDao.searchDisplayHeads(searchText, excludeIds, branchId);
	}

	@RequestMapping(value = "/displayTemplate/search", method = RequestMethod.GET)
	public List<DisplayTemplate> searchDisplayTemplates(HttpSession session, @NotNull @RequestParam(value = "search_text", required = true) String searchText,
			@RequestParam(value = "exclude_head_id", required = false) String excludeIds,
			@RequestParam(value = "branch_id", required = true) Integer branchId) throws Exception {
		
		checkUserBranch(branchId, session);
		
		return displayTemplateDao.searchDisplayTemplates(searchText, excludeIds, branchId);
	}

	@RequestMapping(value = "/displayTemplate/download", method = RequestMethod.GET)
	public void downloadDisplayTemplates(HttpSession session, HttpServletResponse response,
			@RequestParam(value = "branch_id", required = true) Integer branchId)
			throws Exception {

		checkUserBranch(branchId, session);

		ByteArrayOutputStream bo = displayTemplateDao.downloadDisplayTemplate(branchId);
		byte[] arr = bo.toByteArray();
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition","attachment; filename=eduqfix-upload-display-template.xlsx");
		response.getOutputStream().write(arr);
	}

	@RequestMapping(value = "/displayTemplate/upload", method = RequestMethod.POST)
	public String uploadDisplayTemplates(HttpSession session, HttpServletResponse response,
			@RequestParam("data") String data,
			@RequestParam("file") MultipartFile file)
			throws Exception {

		JsonNode node = new ObjectMapper().readTree(data);
		Integer branchId = ((node != null && node.get("branchId") != null) ? node .get("branchId").asInt() : null);
		checkUserBranch(branchId, session);

		DisplayTemplate template = displayTemplateDao.parseExcelFile(file, branchId);
		String json = "{\"status\":\"success\", \"message\": \"Display Template uploaded.\"}";
		System.out.println(new Gson().toJson(template));

		if(template != null){
			template.setBranchId(branchId);
			template.setActive("Y");

			List<ValidationError> validationList = validateTemplate(template);
			ObjectWriter ow = new ObjectMapper().writer()
					.withDefaultPrettyPrinter();

			if (validationList != null && validationList.size() > 0) {
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\": "
						+ "\"Some of the data you have provided is incorrect.\", "
						+ "\"records\":"
						+ ow.writeValueAsString(validationList) + "}";
				return json;
			}
			displayTemplateDao.insertDisplayTemplate(template);
		}
		else {
			json = "{\"status\":\"fail\", \"message\": \"Problem while uploading Display Template.\"}";
		}
		return json;
	}

	private List<ValidationError> validateTemplate(DisplayTemplate template) {
		ValidationError validationError = ValidatorUtil.validateForAllErrors(template, displayTemplateValidator);
		List<ValidationError> validationList = new ArrayList<>();
		if (validationError != null) {
			validationError.setTitle(!StringUtils.isEmpty(template.getName()) ? template.getName() : "Not Available");
			validationList.add(validationError);
		}
		return validationList;
	}
}
