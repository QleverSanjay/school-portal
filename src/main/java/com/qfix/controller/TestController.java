package com.qfix.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/test")
public class TestController extends JdbcDaoSupport{
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	

	@Transactional
	@RequestMapping("/reference")
	public String getQfixReferenceNumber(){
		String query = "SELECT qfix_reference_number from payment.payment_detail WHERE id = 11";
		
		return getJdbcTemplate().queryForObject(query, String.class);
	}
	
}
