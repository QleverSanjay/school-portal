package com.qfix.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.daoimpl.MasterDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Currency;
import com.qfix.model.Master;
import com.qfix.model.Standard;
import com.qfix.model.User;
import com.qfix.utilities.Constants;

@Controller
@RequestMapping(value = "/masters")
public class MasterDataController extends BaseController{

	User user;
	@Autowired
	MasterDao masterDao;

	@RequestMapping(value = "/headMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getHeadMasters() {
		List<Master> headList = masterDao.getAllHead();
		return headList;
	}

	@RequestMapping(value = "/boards", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getBoards() {
		List<Master> headList = masterDao.getAllBoards();
		return headList;
	}
	
	@RequestMapping(value = "/boardMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getBoardMasters() {
		List<Master> boardList = masterDao.getAllBoard();
		return boardList;
	}

	@RequestMapping(value = "/feesCodeMasters/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getFeesCodeMasters(HttpSession session, @PathVariable("id")Integer branchId) throws UnauthorizedException {
		checkUserBranch(branchId, session);
		List<Master> headList = masterDao.getAllFeesCode(branchId);
		return headList;
	}

	
	@RequestMapping(value = "/incomeRangeMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getIncomeMasters() {
		List<Master> incomeRangeList = masterDao.getAllIncomes();
		return incomeRangeList;
	}

	@RequestMapping ( value = "/currencies" , method = RequestMethod.GET)
	@ResponseBody
	public List<Currency> getCurrencies(){
		List<Currency> currencyList = masterDao.getAllCurrencies();
		return currencyList;
	}

	@RequestMapping(value = "/professionMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getProfessionMasters() {
		List<Master> professionList = masterDao.getAllProfessions();
		return professionList;
	}

	@RequestMapping(value = "/subjectMasters/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getAllSubjects(HttpSession session, @PathVariable("id") Integer branchId) throws UnauthorizedException {
		checkUserBranch(branchId, session);
		List<Master> subjectList = masterDao.getAllSubjects(branchId);
		return subjectList;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/subjectByStandard/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getSubjectsByStandard(HttpSession session, @PathVariable("id") Integer standardId) throws UnauthorizedException {
		
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateStandard((List<Integer>)session.getAttribute("branchIdList"), standardId);
		}
		List<Master> subjectList = masterDao.getSubjectsByStandard(standardId);
		return subjectList;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getFeesByHead/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getFeesByHead(HttpSession session, @PathVariable("id") Integer headId) throws UnauthorizedException {
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateFeeHead((List<Integer>) session.getAttribute("branchIdList"), headId);
		}
		List<Master> feesList = masterDao.getFeesByHead(headId);
		return feesList;
	}

	@RequestMapping(value = "/branchMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getBranchMasters(HttpSession session) {
		user = (User)session.getAttribute("currentUser");
		List<Master> branchList = masterDao.getBranchByInstitute((user != null && user.getRoleId() == Constants.ROLE_ID_SCHOOL_ADMIN ? user.getInstituteId() : null));
		return branchList;
	}
	
	@RequestMapping(value = "/getFilterdBranch/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getFilterdBranch(HttpSession session, @PathVariable Integer id) {
		List<Master> branchList = null;
		if(session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			branchList = masterDao.getFilterdBranch(id);
		}
		else if(session.getAttribute("roleId").equals(Constants.ROLE_ID_SCHOOL_ADMIN)){
			branchList = masterDao.getUserBasedBranchList((Integer) session.getAttribute("userId"));
		}
		else if(session.getAttribute("roleId").equals(Constants.ROLE_ID_BANK_PARTNER)){
			branchList = masterDao.getFilterdBranch(id);
		}
		return branchList;
	}

	@RequestMapping(value = "/getFilterdStandards/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Standard> getFilterdStandards(HttpSession session, @PathVariable("id") Integer branchId) throws UnauthorizedException {
		checkUserBranch(branchId, session);
		List<Standard> standardList = masterDao.getFilterdStandards(branchId);
		return standardList;
	}

	/**
	 * This method is used to get list of standards based on teacher id and branch.
	 * @return List<Standards>
	 * @throws UnauthorizedException 
	 */
	 @RequestMapping(value = "getFilterdStandardsByTeacher", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Standard> getFilterdStandardsByTeacher(HttpSession session,
			 @RequestParam("branchId") Integer branchId) throws UnauthorizedException{
		 	checkUserBranch(branchId, session);
			List<Standard> standardList = masterDao.getFilterdStandardsByTeacher((Integer) session.getAttribute("userId"),branchId);
			return standardList;
	 }

	/*@RequestMapping(value = "/getFilterdDivisions/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getFilterdDivisions(@PathVariable("id") Integer standard) {
		List<Master> branchList = masterDao.getFilterdDivisions(standard);
		return branchList;
	}*/
	
	@RequestMapping(value = "/instituteMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getInstituteMasters() {
		List<Master> branchList = masterDao.getAllInstitute();
		return branchList;
	}

	@RequestMapping(value = "/casteMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getCasteMasters() {
		List<Master> casteList = masterDao.getAllCaste();
		return casteList;
	}
	

	@RequestMapping(value = "/religionMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getReligionMasters() {
		List<Master> casteList = masterDao.getAllReligion();
		return casteList;
	}
	

	@RequestMapping(value = "/stateMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getStateMasters() {
		List<Master> casteList = masterDao.getAllState();
		return casteList;
	}
	

	@RequestMapping(value = "/districtMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getDistrictMasters() {
		List<Master> casteList = masterDao.getAllDistrict();
		return casteList;
	}
	
	@RequestMapping(value = "/districtMasters/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getDistrictsByState(@PathVariable("id") Integer stateId) {
		List<Master> headList = masterDao.getDistrictByState(stateId);
		return headList;
	}
	

	@RequestMapping(value = "/getGroupsByBranch", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getGroupsByBranch(HttpSession session, @RequestParam(value = "branchId") Integer branchId,
			 @RequestParam(value = "name",required = false) String name,
			 @RequestParam(value = "allreadySelectedIds")List<Integer> allreadySelectedIds) throws UnauthorizedException {
		List<Master> groupList = masterDao.getGroupsByBranchAndName(branchId, name, allreadySelectedIds);
		
		return groupList;
	}
	
	@RequestMapping(value = "/groupMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getGroupMasters() {
		List<Master> groupList = masterDao.getAllGroup();
		return groupList;
	}
	
	@RequestMapping(value = "/talukaMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getTalukaMasters() {
		List<Master> casteList = masterDao.getAllTaluka();
		return casteList;
	}
	
	@RequestMapping(value = "/talukaMasters/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getTalukasByDistrict(@PathVariable("id") Integer districtId) {
		List<Master> headList = masterDao.getTalukasByDistrict(districtId);
		return headList;
	}
	
	@RequestMapping(value = "/talukaMasters/state/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getTalukaByState(@PathVariable("id") Integer stateId){
		return masterDao.getTalukaByState(stateId);
	}
	
	@RequestMapping(value = "/searchForInstitute" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Master> getInstituteFilter(HttpSession session, 
			 @RequestParam(value = "name",required = false) String name, 
			 @RequestParam(value = "allreadySelectedIdss")List<Integer> allreadySelectedIds ){
		 
		 user = (User)session.getAttribute("currentUser");
		 
		 List<Master> instituteList = masterDao.getAllSearchInstitute(name,allreadySelectedIds);
		 
		 
		 return instituteList;
	 }
	
	@RequestMapping(value = "/resultTypeMasters", method = RequestMethod.GET)
	@ResponseBody
	public List<Master> getResultTypeMasters() {
		List<Master> resultTypeList = masterDao.getAllResultTypes();
		return resultTypeList;
	}
}
