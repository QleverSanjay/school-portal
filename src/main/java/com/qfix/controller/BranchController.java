package com.qfix.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.ExpressOnboardDao;
import com.qfix.dao.IBranchDao;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Branch;
import com.qfix.model.ExpressOnboardSession;
import com.qfix.model.RefundConfig;
import com.qfix.model.ValidationError;
import com.qfix.sep4j.ExcelUtils;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.BranchValidator;
import com.qfix.validator.MasterValidator;

@RestController
@RequestMapping(value = "/branch")
public class BranchController extends BaseController {
	final static Logger logger = Logger.getLogger(BranchController.class);

	@Autowired
	IBranchDao branchDao;

	@Autowired
	BranchValidator branchValidator;

	@Autowired
	MasterValidator masterValidator;
	
	@Autowired
	ExpressOnboardDao expressOnboardDao;

	/**
	 * This method is used to upload file and show the results on the screen.
	 * 
	 * @param file
	 * @return List<Branchs>
	 * @throws SQLException
	 * @throws IOException
	 * @throws IOException
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session, HttpServletResponse response,
			@RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws Exception {
			
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		Integer instituteId = ((node != null && node.get("instituteId") != null) ? node
				.get("instituteId").asInt() : null);

		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))
				&& !instituteId.equals(session.getAttribute("instituteId"))) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this institute.");
		}

		Map<String, String> columnsMap = new HashMap<String, String>();

		columnsMap.put(UploadConstants.BRANCH_NAME_LABEL
				+ UploadConstants.STAR_LABEL, "name");
		columnsMap.put(UploadConstants.BRANCH_ADDRESS_LINE1_LABEL
				+ UploadConstants.STAR_LABEL, "addressLine");
		columnsMap.put(UploadConstants.BRANCH_AREA_LABEL
				+ UploadConstants.STAR_LABEL, "area");
		columnsMap.put(UploadConstants.BRANCH_CITY_LABEL
				+ UploadConstants.STAR_LABEL, "city");
		columnsMap.put(UploadConstants.BRANCH_PINCODE_LABEL
				+ UploadConstants.STAR_LABEL, "pinCode");
		columnsMap.put(UploadConstants.BRANCH_CONTACT_NUMBER_LABEL
				+ UploadConstants.STAR_LABEL, "contactNumber");
		columnsMap.put(UploadConstants.BRANCH_EMAIL_ADDRESS_LABEL
				+ UploadConstants.STAR_LABEL, "contactEmail");
		columnsMap.put(UploadConstants.BRANCH_WEBSITE_URL_LABEL, "websiteUrl");
		columnsMap.put(
				UploadConstants.STATE_LABEL + UploadConstants.STAR_LABEL,
				"state");
		columnsMap.put(UploadConstants.DISTRICT_LABEL
				+ UploadConstants.STAR_LABEL, "district");
		columnsMap.put(UploadConstants.TALUKA_CITY_LABEL
				+ UploadConstants.STAR_LABEL, "taluka");
		columnsMap.put(UploadConstants.BRANCH_STATUS_LABEL
				+ UploadConstants.STAR_LABEL, "status");

		List<Branch> branchList = ExcelUtils.parseIgnoringErrors(columnsMap,
				file.getInputStream(), Branch.class);

		String json = null;

		if (branchList != null && branchList.size() > 0) {
			List<ValidationError> validationList = validateAndPrepareBranchList(
					branchList, instituteId);

			ObjectWriter ow = new ObjectMapper().writer()
					.withDefaultPrettyPrinter();

			if (validationList.size() > 0) {
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\": "
						+ "\"Some of the data you have provided is incorrect.\", "
						+ "\"records\":"
						+ ow.writeValueAsString(validationList) + "}";
				return json;
			}

			try {
				if (branchDao.uploadBranch(branchList, instituteId)) {
					branchList = branchDao.getAll(instituteId);
					json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done.\", "
							+ "\"records\":"
							+ ow.writeValueAsString(branchList) + "}";
				}
			} catch (IOException e) {
				logger.error("", e);
				json = "{\"status\":\"fail\", \"message\":\"Problem while uploading file.\"}";
			}
		} else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading. File might be improper.\", "
					+ "\"records\":[]}";
		}
		return json;
	}

	private List<ValidationError> validateAndPrepareBranchList(
			List<Branch> branchList, Integer instituteId) {
		int row = 1;
		List<ValidationError> validationList = new ArrayList<>();
		branchValidator.setUpload(true);
		branchValidator.setBranchList(branchList);
		int index = 0;
		for (Branch branch : branchList) {
			row++;
			branch.setInstituteId(instituteId);
			if (!StringUtils.isEmpty(branch.getState())) {
				branch.setState(branch.getState().replace("_", " "));
			}
			if (!StringUtils.isEmpty(branch.getDistrict())) {
				branch.setDistrict(branch.getDistrict().replace("_", " "));
			}
			if (!StringUtils.isEmpty(branch.getTaluka())) {
				branch.setTaluka(branch.getTaluka().replace("_", " "));
			}
			branchValidator.setIndex(index);
			ValidationError validationError = ValidatorUtil.validate(branch,
					branchValidator);

			if (validationError != null) {
				validationError.setRow("ROW : " + row);
				validationError
						.setTitle(!StringUtils.isEmpty(branch.getName()) ? branch
								.getName() : "Not Available");
				validationList.add(validationError);
			}
			index++;
		}
		return validationList;
	}

	/**
	 * This method is used to get list of branchs.
	 * 
	 * @return List<Branchs>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Branch> getAllBranchesByInstitute(HttpSession session,
			HttpServletResponse response,
			@RequestParam("instituteId") Integer instituteId) throws Exception {

		if(Constants.ROLE_ID_BANK_PARTNER.equals(session.getAttribute("roleId")))
			return branchDao.getAll(instituteId);
		
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))
				&& !instituteId.equals(session.getAttribute("instituteId"))) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this institute.");
		}
		return branchDao.getAll(instituteId);
	}

	/**
	 * This method is called when we add new branch or edit branch. Based on the
	 * branch Id it will decide whether to add or edit a branch.
	 * 
	 * @param branch
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public String saveBranch(HttpServletResponse response, HttpSession session,
							@RequestParam("data") String data, 
							@RequestParam(value = "file",required = false) MultipartFile file) throws Exception {

		System.out.println("Saving branch");
		ObjectMapper mapper = new ObjectMapper();
		Branch branch = mapper.readValue(data, Branch.class);
		// Validate before insert or update.
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))
				&& !branch.getInstituteId().equals(
						session.getAttribute("instituteId"))) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this institute.");
		}
		String result = null;
		branchValidator.setUpload(false);
		//ObjectMapper mapper = new ObjectMapper();
		ValidationError error = ValidatorUtil.validate(branch, branchValidator);

		if (error != null) {
			error.setTitle(branch.getName());
			result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", "
					+ "\"message\":\"Invalida Data provided\", "
					+ "\"records\" : " + mapper.writeValueAsString(error) + "}";
			return result;
		}

		try {
			if (branch.getId() == null) {
				if (branchDao.insertBranch(branch, file)) {
					if (!session.getAttribute("roleId").equals(
							Constants.ROLE_ID_ADMIN)) {
						List<Integer> branchIdList = (List<Integer>) session
								.getAttribute("branchIdList");
						branchIdList.add(branch.getId());
						session.setAttribute("branchIdList", branchIdList);
					}
					Integer lastInsertedId = null;
					if("Y".equals(branch.getIsExpressOnboard())){
						try {
							lastInsertedId = branchDao.getBranchIdByName(branch.getName(), branch.getInstituteId());
							branchDao.saveExpressBranchSession(lastInsertedId, branch.getInstituteId(), (Integer) session.getAttribute("userId"));
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					result = "{\"status\":\"success\", \"message\":\"Branch created successfully\", \"lastInsertedId\" : "+lastInsertedId+"}";
				} else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while createting branch\"}";
				}
			} else {
				checkUserBranch(branch.getId(), session);
				if (branchDao.updateBranch(branch, file)) {
					result = "{\"status\":\"success\", \"message\":\"Branch updated successfully\", \"lastInsertedId\" : "+branch.getId()+"}";
				} else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while updating branch\"}";
				}
			}
		} catch (Exception e) {
			logger.error("", e);
			if (e instanceof UnauthorizedException) {
				throw e;
			} else {
				result = "{\"status\":\"fail\", \"message\":\"Problem while updating branch\"}";
			}
		}
		return result;
	}

	/**
	 * This method is used to get a single branch.
	 * 
	 * @param id
	 * @return Branch
	 * @throws Exception
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Branch getBranch(HttpSession session, HttpServletResponse response,
			@PathVariable Integer id) throws Exception {
		checkUserBranch(id, session);
		Branch branch = branchDao.getBranchById(id);
		return branch;
	}

	/**
	 * This method is used to delete a branch.
	 * 
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteBranch(HttpSession session,
			HttpServletResponse response, @RequestParam Integer id)
			throws Exception {

		checkUserBranch(id, session);
		String result = "";
		try {
			if (branchDao.deleteBranch(id)) {
				result = "{\"status\" : \"success\", \"message\" : \"Branch deleted successfully\" }";
			} else {
				result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete branch\" }";
			}
		} catch (Exception e) {
			result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete branch\" }";
		}
		return result;
	}

	/**
	 * This method is used to get a branch refund configuration.
	 * 
	 * @param branchId
	 * @return RefundConfig
	 * @throws Exception
	 */
	@RequestMapping(value = "/refund-config/{branchId}", method = RequestMethod.GET)
	@ResponseBody
	public RefundConfig getBranchRefundConfig(HttpSession session, HttpServletResponse response,
			@PathVariable Integer branchId) throws Exception {
		checkUserBranch(branchId, session);
		RefundConfig config = branchDao.getRefundConfigurationByBranchId(branchId);
		return config;
	}

	/**
	 * This method is used to get a branch refund configuration.
	 * 
	 * @param branchId
	 * @return RefundConfig
	 * @throws Exception
	 */
	@RequestMapping(value = "/refund-config", method = RequestMethod.POST)
	@ResponseBody
	public String saveBranchRefundConfig(HttpSession session, HttpServletResponse response,
			@RequestBody RefundConfig config) throws Exception {
		checkUserBranch(config.getBranchId(), session);
		String result = "{\"status\" : \"success\", \"message\" : \"Refund Configuration updated successfully\" }";
		try {
			branchDao.saveRefundConfiguration(config);
		}catch(Exception e){
			e.printStackTrace();
			result = "{\"status\" : \"fail\", \"message\" : \"Failed to update Refund Configuration\" }";
		}
		return result;
	}
	
	@RequestMapping(value="/expressSessionBranches/{instituteId}", method=RequestMethod.GET)
	public List<Branch> getExpressSessionBranches(@PathVariable("instituteId") Integer instituteId, HttpSession session){
		if(instituteId == null)
			instituteId = (Integer)session.getAttribute("instituteId");
		return branchDao.getExpressSessionBranches(instituteId, (Integer)session.getAttribute("userId"));
	}
	
	@RequestMapping(value="/expressSessionBranchDetail/{id}", method=RequestMethod.GET)
	public ExpressOnboardSession getExpressSessionBranchDetail(@PathVariable("id") Integer id, HttpSession session) throws UnauthorizedException, SQLException{
		return expressOnboardDao.getExpressOnboradSessionData(id);
	}
	
	
	

	/*
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveDisplayLayout", method = RequestMethod.POST)
	public String saveDisplayLayout(HttpSession session,
			@Valid @RequestBody DisplayHead layout)
			throws UnauthorizedException {

		String res = "";
		checkUserBranch(layout.getBranchId(), session);
		if (layout.getId() != null) {
			if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
				validateDisplayLayout(layout.getId(),
						(List<Integer>) session.getAttribute("branchIdList"));
			}
			branchDao.updateFeesDisplayLayout(layout);
			res = "{\"status\" : \"success\", \"message\" : \"Display layout updated successfully\" }";
		} else {
			branchDao.insertFeesDisplayLayout(layout);
			res = "{\"status\" : \"success\", \"message\" : \"Display layout added successfully\" }";
		}
		return res;
	}

	@RequestMapping(value = "/getDisplayLayouts", method = RequestMethod.GET)
	public List<DisplayHead> getDisplayLayouts(HttpSession session,
			@RequestParam(value = "branchId", required = true) Integer branchId)
			throws UnauthorizedException {

		List<DisplayHead> displayLayouts = null;
		checkUserBranch(branchId, session);
		displayLayouts = branchDao.getDisplayLyouts(branchId);
		return displayLayouts;
	}
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editDisplayLayout", method = RequestMethod.GET)
	public DisplayHead editDisplayLayouts(HttpSession session,
			@RequestParam(value = "id", required = true) Integer displayLayoutId)
			throws UnauthorizedException {

		if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			validateDisplayLayout(displayLayoutId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}
		DisplayHead displayLayout = branchDao.getDisplayLyout(displayLayoutId);
		return displayLayout;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/deleteDisplayLayout", method = RequestMethod.DELETE)
	public String deleteDisplayLayout(
			HttpSession session,
			@RequestParam(value = "displayLayoutId", required = true) Integer displayLayoutId)
			throws UnauthorizedException {
		String res = "";
		if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			validateDisplayLayout(displayLayoutId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}

		if(branchDao.deleteDisplayLayout(displayLayoutId)){
			res = "{\"status\" : \"success\", \"message\" : \"Display layout deleted successfully\" }";
		}
		else {
			res = "{\"status\" : \"fail\", \"message\" : \"Cant delete Display layout\" }";
		}
		return res;
	}*/
}
