package com.qfix.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.DivisionInterfaceDao;
import com.qfix.dao.TimetableInterfaceDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Division;
import com.qfix.model.ReportModel;
import com.qfix.model.Timetable;
import com.qfix.model.ValidationError;
import com.qfix.utilities.Constants;

@RestController
@RequestMapping(value = "/timetable")
public class TimetableController extends BaseController{

	final static Logger logger = Logger.getLogger(TimetableController.class);
	@Autowired
	TimetableInterfaceDao timetableDAO;

	@Autowired
	DivisionInterfaceDao divisionDAO;

	/**TimetableDao
	 * This method is used to get list of standards.
	 * checkUserBranch method is use to check branch level security, that is branch is valid or not for user
	 * @return List<Standards>
	 */
	 @RequestMapping(value = "getTimetableByBranchStandard" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Timetable> getTimetableByBranchStandard(HttpSession session, @RequestParam("branchId") Integer branchId,
			 @RequestParam(value = "standardId",required = false) Integer standardId,
			 @RequestParam(value = "divisionId",required = false) Integer divisionId)throws Exception{
		 checkUserBranch(branchId, session);
		 return timetableDAO.getAll(branchId, standardId, divisionId);
	 }


	 /**
	  * This method is called when we add new Timetable or edit timetable. 
	  * Based on the timetable Id it will decide whether to add or edit a timetable. 
	  * @param Timetable
	 * @throws UnauthorizedException 
	  */
	@RequestMapping(method = RequestMethod.POST)
	 @ResponseBody 
	public String saveTimetable(@RequestBody Timetable timetable,HttpSession session) throws UnauthorizedException {
		checkUserBranch(timetable.getBranchId(), session);
		 boolean flag = false;
		 String result = null;
 		 try {
			String token = (String) session.getAttribute("token");
			if(timetable.getId() != null){
				if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
					validateTimetable(timetable.getBranchId(), timetable.getId());
				}
				flag = timetableDAO.updateTimetable(timetable, token);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Timetable updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating Timetable.\"}";
				}
			}
			else {
				flag = timetableDAO.insertTimetable(timetable, token);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Timetable added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding Timetable.\"}";
				}
			}
		} catch (Exception e) {
			logger.error("", e);
			result = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\"}";
		}

		return result;
	}

	/**
	* This method is used to delete a branch.
	* @param id
	* @throws UnauthorizedException 
	*/
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteTimetable(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			validateTimetable((List<Integer>)session.getAttribute("branchIdList"), id);
		}

		String result = "";
		String token = (String) session.getAttribute("token");

		try {
			if(timetableDAO.deleteTimeTable(id, token)){
				result = "{\"status\" : \"success\", \"message\" : \"Timetable deleted successfully\" }";
			}
			else {
				result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete timetable\" }";
			}
		} catch (Exception e) {
			result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete timetable\" }";
			e.printStackTrace();
		}
		return result;
	}	

	 /**
	  * This method is used to get a single timetbale.
	  * @param id
	  * @return Standards
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Timetable getTimetable(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 validateTimetable((List<Integer>)session.getAttribute("branchIdList"), id);
		 }
		 Timetable timetable = timetableDAO.getTimetableById(id);
		 return timetable;
	 } 


	@RequestMapping(value = "/uploadTimetableFile" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session, @RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws Exception {
		 // getAllByStandardTimetable
		String result = null;
		String token = (String) session.getAttribute("token");
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		if (node != null) {
			Integer branchId = ((node.get("branchId") != null) ? node.get( "branchId").asInt() : null);
			Integer instituteId = ((node.get("instituteId") != null) ? node .get("instituteId").asInt() : null);
			Integer academicYearId = ((node.get("academicYearId") != null) ? node.get( "academicYearId").asInt() : null);
			Integer standardId = ((node.get("standardId") != null) ? node.get( "standardId").asInt() : null);
			Integer divisionId = ((node.get("divisionId") != null) ? node.get( "divisionId").asInt() : null);

			checkUserBranch(branchId, session);
			if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
				if(standardId != null){
					validateStandard(branchId, standardId);
				}
				if(divisionId != null){
					validateDivision(branchId, standardId, divisionId);
				}
			}

			List<Integer> divisionIdList = new ArrayList<>();
			
			Timetable timetable = new Timetable();
			timetable.setAcademicYearId(academicYearId);
			timetable.setBranchId(branchId);
			
			timetable.setInstituteId(instituteId);
			timetable.setStandardId(standardId);

			try {
				List<Division> divisionList = divisionDAO.getAllByStandardTimetable(standardId);
				if(divisionList != null && divisionList.size() > 0){
					if(divisionId == null){
						for(Division division : divisionList){
							divisionIdList.add(division.getId());
						}
					}
					else {
						boolean isValidDivisionIdSpecified = false;
						for(Division division : divisionList){
							if(division.getId().equals(divisionId)){
								isValidDivisionIdSpecified = true;
								divisionIdList.add(divisionId);
								break;
							}
						}

						if(!isValidDivisionIdSpecified) {
							result = "{\"status\" : \"fail\", \"message\" : \"Timetable already exists for this standard and division combination.\"}";	
							return result;
						}
					}

					timetable.setDivisionIdList(divisionIdList);
					timetableDAO.uploadTimetable(file, timetable, token);
					ReportModel reportModel = new ReportModel();
					reportModel.setBranchId(branchId);
					reportModel.setInstituteId(instituteId);
					reportModel.setAcademicYearId(academicYearId);
					reportModel.setStandardId(standardId);
					reportModel.setDivisionId(divisionId);

					result = "{\"status\" : \"success\", \"message\" : \"Timetable uploaded successfully.\"," +
							"\"records\" : "+mapper.writeValueAsString(timetableDAO.getAll(branchId, standardId, divisionId))+"}";
				}
				else {
					result = "{\"status\" : \"fail\", \"message\" : \"Divisions Not available for this standard.\"}";	
				}
			} catch (Exception e) {

				List<ValidationError> validationErrors = timetableDAO.getValidationErrors();
				if(validationErrors != null && validationErrors.size() > 0){
					ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

					result = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
							"\"records\":"+ow.writeValueAsString(validationErrors)+"}";
				}
				else {
					result = "{\"status\" : \"fail\", \"message\" : \""+e.getMessage()+"\"}";	
				}

				logger.error("", e);
			}
		} else {
			result = "{\"status\" : \"fail\", \"message\" : \"Invalid Request.\"}";
		}

		return result;
	}
	 
	 
	
	
}
