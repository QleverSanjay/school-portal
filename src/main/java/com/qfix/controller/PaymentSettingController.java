package com.qfix.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.daoimpl.PaymentSettingDao;
import com.qfix.model.PaymentSetting;

@Controller
@RequestMapping(value = "/paymentSetting")
public class PaymentSettingController {
	final static Logger logger = Logger.getLogger(PaymentSettingController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<PaymentSetting> getAllPaymentSettings(HttpSession session, @RequestParam("instituteId") Integer instituteId) {
		System.out.println("Institute Id ---"+instituteId);
		return new PaymentSettingDao().getAll(instituteId);
	}


	
	/**
	  * This method is called when we add new PaymentSetting or edit PaymentSetting. Based on the Fee Id it will decide whether to add or edit a Fee. 
	  * @param PaymentSetting
	  */
	 @RequestMapping(method = RequestMethod.POST)
	 @ResponseBody 
	public String savePaymentSetting(@RequestBody PaymentSetting paymentSetting) {
		 boolean flag = false;
		String result = null;
		//paymentSetting.setBranchId((user.getRoleId() == 6)? user.getBranchId()  : paymentSetting.getBranchId());
		 try {

			if(paymentSetting.getId() != null){
				flag = new PaymentSettingDao().updatePaymentSetting(paymentSetting);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Payment setting updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating payment setting.\"}";
				}
			}
			else {
				flag = new PaymentSettingDao().insertPaymentSetting(paymentSetting);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Payment setting added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding payment setting.\"}";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("", e);
			result = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\"}";
		}
		 
		return result;
	}
	 
	 
	 /**
	  * This method is called when we add new Default PaymentSetting or edit PaymentSetting. Based on the Fee Id it will decide whether to add or edit a Fee. 
	  * @param PaymentSetting
	  */
	 @RequestMapping(value = "/setDefaultPaymentSetting", method = RequestMethod.POST)
	 @ResponseBody
	public String saveDefaultPaymentSetting(@RequestBody PaymentSetting paymentSetting) {

		String result = new PaymentSettingDao().setDefaultPaymentSettings(paymentSetting);
		return result;
	}

	 
	 /**
	  * This method is used to get a single fee.
	  * @param id
	  * @return fee
	  */
	 @RequestMapping(value = "/getDefaultPaymentSetting",method = RequestMethod.GET)
	 @ResponseBody
	 public PaymentSetting getDefaultPaymentSetting(){
		 return new PaymentSettingDao().getDefaultPaymentSetting();
	 }
	 
	 /**
	  * This method is used to get a single fee.
	  * @param id
	  * @return fee
	  */
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public PaymentSetting getPaymentSetting(@PathVariable Integer id){
		 return new PaymentSettingDao().getPaymentSettingById(id);
	 }
	
	 /**
	  * This method is used to delete a fee.
	  * @param id
	  */
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deletePaymentSetting(@RequestParam Integer id) {
		 
		 boolean flag = false;
		 String result = null;
		 flag =	new PaymentSettingDao().deletePaymentSetting(id);
		 if(flag == true){
				result = "{\"status\":\"success\", \"message\" : \"PaymentSetting deleted successfully.\"}";
			}
			else{
				result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting paymentSetting.\"}";
			}
			return result;
	}
}
