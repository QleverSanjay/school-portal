package com.qfix.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.IHolidayDao;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Holiday;
import com.qfix.model.PublicHoliday;
import com.qfix.model.ValidationError;
import com.qfix.sep4j.ExcelUtils;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.HolidayValidator;
import com.sun.istack.NotNull;

@RestController
@RequestMapping(value = "/holiday")
public class HolidayController extends BaseController{

	@Autowired
	IHolidayDao holidayDao;

	@Autowired
	HolidayValidator holidayValidator;

	final static Logger logger = Logger.getLogger(HolidayController.class);

	@RequestMapping(value = "/getHolidayByBranch",method = RequestMethod.GET)
	@ResponseBody
	public List<Holiday> getHoliday(HttpSession session,@RequestParam("branchId") Integer branchId) throws Exception{
		checkUserBranch(branchId, session);
		return holidayDao.getAll(branchId);
	}

	@RequestMapping(value = "/uploadHoliday" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session, @RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws Exception{
		List<Holiday> holidayList = null;

		Map<String, String> columnsMap = new HashMap<String, String>();
		
		columnsMap.put(UploadConstants.HOLIDAY_TITLE_LABEL+UploadConstants.STAR_LABEL, "title");
		columnsMap.put(UploadConstants.HOLIDAY_DESCRIPTION_LABEL , "description");
		columnsMap.put(UploadConstants.HOLIDAY__FROM_DATE_LABEL+UploadConstants.DATE_FORMAT_LABEL+UploadConstants.STAR_LABEL , "fromDate");
		columnsMap.put(UploadConstants.HOLIDAY_TO_DATE_LABEL+UploadConstants.DATE_FORMAT_LABEL+UploadConstants.STAR_LABEL , "toDate");	
		
	//	holidayList = CSVFileReader.readFile(file.getInputStream(), columns, new Holiday(), Holiday.class);
		holidayList = ExcelUtils.parseIgnoringErrors(columnsMap, file.getInputStream(), Holiday.class);
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		Integer branchId = ((node != null && node.get("branchId") != null) ? node.get("branchId").asInt()  : null);
		Integer academicYearId = ((node != null && node.get("academicYearId") != null) ? node.get("academicYearId").asInt()  : null);
		checkUserBranch(branchId, session);
		if(holidayList != null && holidayList.size() > 0){ 
			List<ValidationError> validationList = validateHolidayList(holidayList, branchId, academicYearId);
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

			if(validationList.size() > 0){
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
						"\"records\":"+ow.writeValueAsString(validationList)+"}";
				return json;
			}
			try {
				if(holidayDao.uploadHoliday(holidayList)){
					holidayList = holidayDao.getAll(branchId);
					json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", " +
							"\"records\":"+ow.writeValueAsString(holidayList)+"}";
				}
			} catch (Exception e) {
				json = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
				logger.error("", e);
			}
		}
		else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", " +
						"\"records\":[]}";
		}
		
		
		return json;
	}

	private List<ValidationError> validateHolidayList(List<Holiday> holidayList, Integer branchId, Integer academicYearId) {

		int row = 1;
		List<ValidationError> validationList = new ArrayList<>();
		holidayValidator.setUpload(true);
		holidayValidator.setHolidayList(holidayList);
		int index = 0;

		for(Holiday holiday : holidayList){
			row ++;
			holiday.setBranchId(branchId);
			holiday.setCurrentAcademicYearId(academicYearId);
			holidayValidator.setIndex(index);
			ValidationError validationError = ValidatorUtil.validate(holiday, holidayValidator);
			if(validationError != null){
				validationError.setRow("ROW : "+row);
				validationError.setTitle(holiday.getTitle());
				validationList.add(validationError);
			}
			index ++;
		}
		return validationList;
	}

	
	/**
	  * This method is called when we add new holidays or edit holidays. Based on the holidays Id it will decide whether to add or edit a holidays. 
	  * @param holidays
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	 @ResponseBody 
	public String saveHoliday(@RequestBody Holiday holiday,HttpSession session) {
		 boolean flag = false;
		String result = null;
		 try {
			 
			 checkUserBranch(holiday.getBranchId(), session);
			 
			 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
					validateAcademicYear(holiday.getBranchId(), holiday.getCurrentAcademicYearId());
				}
				
			 holidayValidator.setUpload(false);
			 ValidationError error = ValidatorUtil.validate(holiday, holidayValidator);
				ObjectMapper mapper = new ObjectMapper();
				if(error != null){
					String title = "Invalid holiday details.";
					error.setTitle(title);
					result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
						"\"message\":\"Invalida Data provided\", " +
						"\"records\" : "+mapper.writeValueAsString(error)+"}";
					return result;
				}
				
			 if(holiday.getId() != null){
				 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
					 	validateHoliDay((List<Integer>)session.getAttribute("branchIdList"), holiday.getId());
					}
				flag = holidayDao.updateHoliday(holiday);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Holiday updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating holiday.\"}";
				}
			 }
			 else {
				flag = holidayDao.insertHoliday(holiday);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Holiday added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding holiday.\"}";
				}
			 }
		 }
		 catch (Exception e) {
			 logger.error("Problem while updating holiday...", e);
		}
		 
		return result;
	}

	 @SuppressWarnings("unchecked")
		@RequestMapping(value = "/multiple", method = RequestMethod.POST)
		 @ResponseBody 
		public String saveMultipleHoliday(@RequestBody @NotNull List<Holiday> holidays,HttpSession session) {
			 boolean flag = false;
			String result = null;
			 try {

				 Holiday holiday = holidays.get(0);
				 checkUserBranch(holiday.getBranchId(), session);

				 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
					 validateAcademicYear(holiday.getBranchId(), holiday.getCurrentAcademicYearId());
				 }

				 holidayValidator.setUpload(false);
				 List<ValidationError> errors = new ArrayList<ValidationError>();
				 for (Holiday tempHoliday : holidays){
					 ValidationError error = ValidatorUtil.validate(tempHoliday, holidayValidator);
					 if(error != null){
						 String title = holiday.getTitle();
						 error.setTitle(title);
						 errors.add(error);
					 }
				 }

				 ObjectMapper mapper = new ObjectMapper();
				 if(errors.size() > 0){
					 result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
						"\"message\":\"Invalid Data provided\", " +
						"\"records\" : "+mapper.writeValueAsString(errors)+"}";
					 return result;
				 }
				for(Holiday tempHoliday : holidays){
					flag = holidayDao.insertHoliday(tempHoliday);
					if(flag){
						result = "{\"status\":\"success\", \"message\" : \"Holiday added successfully.\"}";
					}else {
						result = "{\"status\":\"fail\", \"message\" : \"Problem while adding holiday.\"}";
					}
				}
			 }
			 catch (Exception e) {
				 logger.error("Problem while updating holiday...", e);
			}
			 
			return result;
		}
	 
	
	 /**
	  * This method is used to get a single fee.
	  * @param id
	  * @return fee
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Holiday getHoliday(@PathVariable Integer id, HttpSession session) throws UnauthorizedException{
		
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
				validateHoliDay((List<Integer>)session.getAttribute("branchIdList"), id);
			}
		
		 return holidayDao.getHolidayById(id);
	 }
	
	 /**
	  * This method is used to delete a fee.
	  * @param id
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteHoliday(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateHoliDay((List<Integer>)session.getAttribute("branchIdList"), id);
		 }
		 
		 boolean flag = false;
		 String result = null;
		 flag =	holidayDao.deleteHoliday(id);
		 if(flag == true){
				result = "{\"status\":\"success\", \"message\" : \"Holiday deleted successfully.\"}";
			}
			else{
				result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting holiday.\"}";
			}
			return result;
	}
	 
	 @RequestMapping(value="/publicHolidays", method=RequestMethod.GET)
	 @Transactional
	 public List<PublicHoliday> getPublicHoliday(){
		 List<PublicHoliday> list = holidayDao.getPublicHoliday();
		 return list;
	 }
}
