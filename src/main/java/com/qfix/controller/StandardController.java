package com.qfix.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.qfix.dao.DivisionInterfaceDao;
import com.qfix.dao.StandardInterfaceDao;
import com.qfix.exceptions.StandardAllreadyExistException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Division;
import com.qfix.model.ExpressOnboard;
import com.qfix.model.Standard;
import com.qfix.utilities.Constants;

@RestController
@RequestMapping(value = "/standard")
public class StandardController extends BaseController{

	final static Logger logger = Logger.getLogger(StandardController.class);
	@Autowired
	StandardInterfaceDao standardDAO;

	@Autowired
	DivisionInterfaceDao divisionDao;
	
	@Autowired
	private Gson gson; 

	/**
	 * This method is used to get list of standards.
	 * @return List<Standards>
	 */
	 @RequestMapping(value = "getStandardByBranch" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Standard> getAllStandardsByBranch(HttpSession session, @RequestParam("branchId") Integer branchId)throws Exception{
		 checkUserBranch(branchId, session);
		 return standardDAO.getAll(branchId);
	 }
	
	
	 /**
	  * This method is called when we add new standard or edit standard. Based on the standard Id it will decide whether to add or edit a standard. 
	  * @param standard
	 * @throws JsonProcessingException 
	 * @throws UnauthorizedException 
	 * @throws IOException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	 @ResponseBody
	public String saveStandard(HttpServletRequest request , HttpSession session, @RequestBody Standard standard) throws JsonProcessingException, UnauthorizedException{
		 //User user = (User)session.getAttribute("currentUser");

		 String result = null;
		 
		 try {
			 
			 checkUserBranch(standard.getBranchId(), session);
			 
			 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			 		validateAcademicYear(standard.getBranchId(), standard.getAcademicYearId());
			 		if(standard.getDivisionIdList() != null){
			 			for(Integer divisionId : standard.getDivisionIdList()){
			 				validateDivision(standard.getBranchId(), null, divisionId);
			 			}
			 		}
			 }
			 
			if(standard.getId() == null) {
				 
				if(standardDAO.insertStandard(standard)){
					result = "{\"status\":\"success\", \"message\":\"Standard inserted successfully\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while inserting standard\"}";
					}
				}	
				else {
					
					if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
						 validateStandard((List<Integer>)session.getAttribute("branchIdList"), standard.getId());
						}
					
					if(standardDAO.updateStandard(standard)){
						result = "{\"status\":\"success\", \"message\":\"Standard updated successfully\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while updating standard\"}";
				}
			}
		} 
		catch (StandardAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		} 

		 return result;
	}

	 /**
	  * This method is used to get a single standard.
	  * @param id
	  * @return Standards
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Standard getStandard(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateStandard((List<Integer>)session.getAttribute("branchIdList"), id);
			}
		 
		 Standard standard = standardDAO.getStandardById(id);
		 return standard;
	 }

	 /**
	  * This method is used to delete a standard.
	  * @param id
	  */
 @SuppressWarnings("unchecked")
 @RequestMapping(method = RequestMethod.DELETE)
 @ResponseBody
	public String deleteStandard(HttpSession session, @RequestParam Integer id) throws UnauthorizedException{
		 
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateStandard((List<Integer>)session.getAttribute("branchIdList"), id);
			}

		String result = "";
		if(standardDAO.deleteStandard(id)){
			result = "{\"status\" : \"success\", \"message\" : \"Standard deleted successfully.\" }";
		}
		else {
			result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete standard\" }";
		}
		return result;
	}


	/**
	 * This method is used to delete a standard.
	 * @param id
	 * @throws JsonProcessingException 
	 */
	@RequestMapping(value = "/express" ,method = RequestMethod.POST)
	@ResponseBody
	public String saveStandards(HttpSession session,
			@RequestParam("ayId")Integer ayId,
			@RequestParam("branchId")Integer branchId,
			@RequestBody ExpressOnboard onboard) throws Exception{
		
		checkUserBranch(branchId, session);
		
		String[] standardsToRemove = onboard.getStandardsToRemove();
		
		if(standardsToRemove != null && standardsToRemove.length > 0){
			standardDAO.deleteStandards(branchId, standardsToRemove);
		}
		
		List<Standard> standards = onboard.getStandards();		
		for(Standard standard:standards){
			String name = standard.getName();
			
			if(StringUtils.isEmpty(name)){
				name = standard.getText();
				standard.setName(name);
			}
			
			Standard savedStandard = standardDAO.getStandardByName(name, branchId, ayId);
			if(savedStandard != null){
				standard.setId(savedStandard.getId());;
			}
			standard.setAcademicYearId(ayId);
			standard.setBranchId(branchId);
			standard.setDisplayName(name);
			List<Division> divisions = standard.getDivisionList();
			divisionDao.insertDivisions(branchId, ayId, divisions);
			standardDAO.insertStandards(standard, divisions);
		}
		
		HashMap<String,Object> response = new HashMap<String, Object>();
		response.put("status", "success");
		response.put("message", "Standard inserted successfully");
		response.put("lastInsertedStandrads", standards);
		
		return getJson(response);
	}
	
	
	@RequestMapping(value = "/list")
	public List<Standard> listSchoolStandards(){
		return standardDAO.listStandards();
	}
	
	private <T> String getJson(T t){
		return gson.toJson(t);
	}
}
