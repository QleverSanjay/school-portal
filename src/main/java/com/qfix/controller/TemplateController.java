package com.qfix.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.config.AppProperties;

import com.qfix.config.AppProperties.FileProps;
import com.qfix.excel.ExcelTemplate;
import com.qfix.excel.ExcelTemplateFactory;

@RestController
@RequestMapping(value = "/template")
public class TemplateController {

	final static Logger logger = Logger.getLogger(TemplateController.class);

	@Autowired
	ExcelTemplateFactory excelTemplateFactory;

	@Autowired
	private AppProperties appProperties;

	/**
	 * This method is used to get list of divisions.
	 * 
	 * @return List<Divisions>
	 * @throws IOException
	 */
	 @RequestMapping(value = "/download" ,method = RequestMethod.GET)
	 @ResponseBody
	 public String downloadCSVTemplate(HttpSession session , @RequestParam("excelFileName") String excelFileName, @RequestParam(value="branchId", required=false)  Integer branchId,
			 HttpServletResponse response) throws IOException{
		 logger.debug("Download template -- "+excelFileName);
		 FileProps fileProps = appProperties.getFileProps();
		 String filePath = fileProps.getBaseSystemPath().concat(fileProps.getUploadCsvTemplatesPath()).concat("/");

		 ExcelTemplate template = excelTemplateFactory.getExcelTemplate(excelFileName); 
		 template.setToken((String)session.getAttribute("token"));
		 byte[] arr = template.getExcelTemplateFile(filePath, excelFileName, branchId);
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		 response.setHeader("Content-Disposition", "attachment; filename="+ excelFileName);
		 response.getOutputStream().write(arr);
		 return null;
	 }

	 
	 @RequestMapping(value = "/downloadTimetableTemplate" ,method = RequestMethod.GET)
	 @ResponseBody
	 public String downloadTimetableExcelTemplate(@RequestParam("excelFileName") String excelFileName, @RequestParam(value="branchId", required=false)  Integer branchId,
			 @RequestParam(value="standardId", required=false)  Integer standardId, HttpServletResponse response) throws IOException{
		 logger.debug("Download dynamic excel template -- "+excelFileName);

		 FileProps fileProps = appProperties.getFileProps();
		 String filePath = fileProps.getBaseSystemPath().concat(fileProps.getUploadCsvTemplatesPath()).concat("/");

		 ExcelTemplate template = excelTemplateFactory.getExcelTemplate(excelFileName); 

		 byte[] arr = template.getExcelTemplateFile(filePath, excelFileName, branchId, standardId, 10);
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		 response.setHeader("Content-Disposition", "attachment; filename="+ excelFileName);
		 response.getOutputStream().write(arr);
		 return null;
	 }

	public void setExcelTemplateFactory(ExcelTemplateFactory excelTemplateFactory) {
		this.excelTemplateFactory = excelTemplateFactory;
	}

}