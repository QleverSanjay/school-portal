package com.qfix.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.IStudentDao;
import com.qfix.excel.StudentsExcelTemplate;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.AuditLog;
import com.qfix.model.Student;
import com.qfix.model.StudentExcelReportFilter;
import com.qfix.model.StudentParent;
import com.qfix.model.TableResponse;
import com.qfix.model.ValidationError;
import com.qfix.sep4j.ExcelUtils;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ReadWriteExcelUtil;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.MasterValidator;
import com.qfix.validator.StudentValidator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/students")
@Scope(value = "session")
public class StudentController extends BaseController{

	final static Logger logger = Logger.getLogger(StudentController.class);
	@Autowired
	IStudentDao studentDao;

	@Autowired
	StudentValidator studentValidator;

	@Autowired
	MasterValidator masterValidator;

	@Autowired
	StudentsExcelTemplate studentsExcelTemplate;

	Map<Integer, Map<String, Integer>> TALUKA_MAP_BY_DISTRICT;
	Map<Integer, Map<String, Integer>> DISTRICT_MAP_BY_STATE;
	Map<String, Integer> STATE_MAP;
	Map<String, Integer> INCOME_RANGE_MAP;
	Map<String, Integer> PROFESSION_MAP;

	private static Map<String, String> STUDENT_UPLOAD_COLUMNS_MAP = new HashMap<String, String>();
	private static Map<String, String> FATHER_UPLOAD_COLUMNS_MAP = new HashMap<String, String>();
	private static Map<String, String> MOTHER_UPLOAD_COLUMNS_MAP = new HashMap<String, String>();
	private static Map<String, String> GUARDIAN_UPLOAD_COLUMNS_MAP = new HashMap<String, String>();

	public StudentController() {
		super();
		System.out.println("Creating constructor....");
	}

	static {
		// STUDENT MAP
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.FIRST_NAME_LABEL + UploadConstants.STAR_LABEL , "firstname");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.MIDDLE_NAME, "middleName");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.SURNAME_LABEL + UploadConstants.STAR_LABEL, "surname");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.USERID_LABEL + UploadConstants.STAR_LABEL, "studentUserId");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_CREATE_LOGIN_LABEL, "createStudentLogin");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.GENDER_LABEL + UploadConstants.STAR_LABEL, "gender");	
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.BIRTH_DATE_LABEL, "dateOfBirth");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.EMAIL_ADDRESS_LABEL, "emailAddress");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOBILE_NUMBER_LABEL, "mobile");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.ADDRESS_LINE_1_LABEL, "addressLine1");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.AREA_LABEL, "area");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STATE_LABEL, "state");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.DISTRICT_LABEL, "district");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.TALUKA_CITY_LABEL, "taluka");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.TOWN_VILLAGE_LABEL, "city");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.PINCODE_LABEL, "pincode");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.RELIGION_LABEL, "religion");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.CASTE_LABEL, "caste");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STANDARD_LABEL, "standardName");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.DIVISION_LABEL, "divisionName");
		/*STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_FEES_CODE_LABEL, "feesCodeName");*/
		/*added by pitabas on 09_12_2017*/
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_FEES_CODE_LABEL1, "feesCodeName1");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_FEES_CODE_LABEL2, "feesCodeName2");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_FEES_CODE_LABEL3, "feesCodeName3");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_FEES_CODE_LABEL4, "feesCodeName4");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_FEES_CODE_LABEL5, "feesCodeName5");
		/*added by pitabas on 09_12_2017*/
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.ROLL_NUMBER_LABEL, "rollNumber");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.STUDENT_REGISTRATION_CODE_LABEL, "registrationCode");
		STUDENT_UPLOAD_COLUMNS_MAP.put(UploadConstants.ACTIVE_LABEL, "active");
		
		// FATHER MAP
		
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_FIRST_NAME_LABEL +  UploadConstants.STAR_LABEL , "fatherFirstname");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_MIDDLE_NAME_LABEL , "fatherMiddleName");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_SURNAME_LABEL + UploadConstants.STAR_LABEL, "fatherSurname");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_USERID_LABEL + UploadConstants.STAR_LABEL, "fatherUserId");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_CREATE_LOGIN_LABEL, "createFatherLogin");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_BIRTH_DATE_LABEL, "fatherBirthDate");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_EMAIL_ADDRESS_LABEL, "fatherEmailAddress");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_PRIMARY_MOBILE_NUMBER_LABEL, "fatherPrimaryMobileNumber");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_SECONDARY_MOBILE_NUMBER_LABEL , "fatherSecondaryMobileNumber");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_ADDRESS_LINE_1_LABEL, "fatherAddressLine1");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_AREA_LABEL, "fatherArea");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_TOWN_VILLAGE_LABEL, "fatherCity");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_PINCODE_LABEL, "fatherPincode");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_PROFESSION_LABEL, "fatherProfession");
		FATHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.FATHER_INCOME_LABEL, "fatherIncome");
		
		// MOTHER MAP
		
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_FIRST_NAME_LABEL + UploadConstants.STAR_LABEL , "motherFirstname");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_MIDDLE_NAME_LABEL , "motherMiddleName");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_SURNAME_LABEL + UploadConstants.STAR_LABEL , "motherSurname");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_USERID_LABEL + UploadConstants.STAR_LABEL , "motherUserId");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_CREATE_LOGIN_LABEL, "createMotherLogin");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_BIRTH_DATE_LABEL, "motherBirthDate");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_EMAIL_ADDRESS_LABEL, "motherEmailAddress");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_PRIMARY_MOBILE_NUMBER_LABEL, "motherPrimaryMobileNumber");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_SECONDARY_MOBILE_NUMBER_LABEL , "motherSecondaryMobileNumber");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_ADDRESS_LINE_1_LABEL, "motherAddressLine1");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_AREA_LABEL, "motherArea");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_TOWN_VILLAGE_LABEL, "motherCity");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_PINCODE_LABEL, "motherPincode");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_PROFESSION_LABEL, "motherProfession");
		MOTHER_UPLOAD_COLUMNS_MAP.put(UploadConstants.MOTHER_INCOME_LABEL, "motherIncome");
		
		// GUARDIAN MAP
		
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_FIRST_NAME_LABEL +  UploadConstants.STAR_LABEL, "parentFirstname");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_MIDDLE_NAME_LABEL , "parentMiddleName");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_SURNAME_LABEL + UploadConstants.STAR_LABEL, "parentSurname");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_USERID_LABEL + UploadConstants.STAR_LABEL, "parentUserId");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_CREATE_LOGIN_LABEL, "createParentLogin");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_GENDER_LABEL + UploadConstants.STAR_LABEL, "parentGender");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_BIRTH_DATE_LABEL, "parentBirthDate");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_EMAIL_ADDRESS_LABEL, "parentEmailAddress");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_PRIMARY_MOBILE_NUMBER_LABEL, "parentPrimaryMobileNumber");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_SECONDARY_MOBILE_NUMBER_LABEL , "parentSecondaryMobileNumber");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_ADDRESS_LINE_1_LABEL, "parentAddressLine1");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_AREA_LABEL, "parentArea");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_TOWN_VILLAGE_LABEL, "motherCity");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_PINCODE_LABEL, "parentPincode");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_RELATION_WITH_STUDENT, "parentRelationship");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_PROFESSION_LABEL, "parentProfession");
		GUARDIAN_UPLOAD_COLUMNS_MAP.put(UploadConstants.GUARDIAN_INCOME_LABEL, "parentIncome");
	}
	
	
	@SuppressWarnings("unchecked")
	private void loadDataFromContext(HttpServletRequest request) {
		if(STATE_MAP == null){
			STATE_MAP = (Map<String, Integer>) request.getServletContext().getAttribute("STATE_MAP");
			if(STATE_MAP == null){
				STATE_MAP = masterValidator.getAllStates();
				request.getServletContext().setAttribute("STATE_MAP", STATE_MAP);
			}
		}
		if(DISTRICT_MAP_BY_STATE == null){
			DISTRICT_MAP_BY_STATE = (Map<Integer, Map<String, Integer>>) request.getServletContext().getAttribute("DISTRICT_MAP_BY_STATE");
			if(DISTRICT_MAP_BY_STATE == null){
				DISTRICT_MAP_BY_STATE = masterValidator.getAllDistrictsUgainstState();
				request.getServletContext().setAttribute("DISTRICT_MAP_BY_STATE", DISTRICT_MAP_BY_STATE);
			}
		}
		if(TALUKA_MAP_BY_DISTRICT == null){
			TALUKA_MAP_BY_DISTRICT = (Map<Integer, Map<String, Integer>>) request.getServletContext().getAttribute("TALUKA_MAP_BY_DISTRICT");
			if(TALUKA_MAP_BY_DISTRICT == null){
				TALUKA_MAP_BY_DISTRICT = masterValidator.getAllTalukasUgainstDistrict();
				request.getServletContext().setAttribute("TALUKA_MAP_BY_DISTRICT", TALUKA_MAP_BY_DISTRICT);
			}
		}
		if(INCOME_RANGE_MAP == null){
			INCOME_RANGE_MAP = (Map<String, Integer>) request.getServletContext().getAttribute("INCOME_RANGE_MAP");
			if(INCOME_RANGE_MAP == null){
				INCOME_RANGE_MAP = masterValidator.getAllIncomeRanges();
				request.getServletContext().setAttribute("INCOME_RANGE_MAP", INCOME_RANGE_MAP);
			}
		}
		if(PROFESSION_MAP == null){
			PROFESSION_MAP = (Map<String, Integer>) request.getServletContext().getAttribute("PROFESSION_MAP");
			if(PROFESSION_MAP == null){
				PROFESSION_MAP = masterValidator.getProfessions();
				request.getServletContext().setAttribute("PROFESSION_MAP", PROFESSION_MAP);
			}
		}
	}

	/**
	 * This method is used to upload file and show the results on the screen.
	 * @param file
	 * @return List<Student>
	 * @throws Exception 
	 */

	@RequestMapping(value = "/uploadFile" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpServletRequest request, HttpSession session, @RequestParam("data") String data, @RequestParam("file") MultipartFile file) throws Exception{

		Map<String, String> columnsMap = new HashMap<String, String>();
		columnsMap.putAll(STUDENT_UPLOAD_COLUMNS_MAP);
		columnsMap.putAll(FATHER_UPLOAD_COLUMNS_MAP);
		columnsMap.putAll(MOTHER_UPLOAD_COLUMNS_MAP);
		columnsMap.putAll(GUARDIAN_UPLOAD_COLUMNS_MAP);

		//List<Student> students = CSVFileReader.readFile(file.getInputStream(), columns, new Student(), Student.class);
		logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Reading file started at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		loadDataFromContext(request);
		
		InputStream fileInputStream = file.getInputStream();
		List<Student> students = null;
		try {
			students = ExcelUtils.parseIgnoringErrors(columnsMap, fileInputStream, Student.class);
			logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Reading file completed at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			System.out.println(students.get(0).getFeesCodeName1());
		}finally{
			if(fileInputStream != null){
				fileInputStream.close();
			}
		}

		String jsonData = null;
		if(students != null && students.size() > 0){
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.readTree(data);

			Integer branchId = ((node != null && node.get("branchId") != null) ? 
									node.get("branchId").asInt()  : null);
			checkUserBranch(branchId, session);
			List<ValidationError> validationList = validateAndPrepareStudents(students, branchId, null);

			if(validationList.size() > 0){
				jsonData = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
						"\"records\":"+ow.writeValueAsString(validationList)+"}";
				return jsonData;
			}

			String token = (String) session.getAttribute("token");
			try {
				logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Validation completed at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				if(studentDao.uploadStudent(students, token)){
					StudentExcelReportFilter filter = new StudentExcelReportFilter();
					filter.setBranchId(branchId);

					jsonData = "{\"status\":\"success\", \"message\" : \"Students uploaded successfully.\", " +
									"\"records\" : []}";				
				}
				else {
					jsonData = "{\"status\":\"fail\", \"message\" : \"problem while uploading records.\", " +
							"\"records\" : []}";
				}
				logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Student insertion completed at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			}catch(Exception e){
				jsonData = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\", " +
						"\"records\" : []}";
				logger.error("Error while uploading file..", e);
			}
		}
		else {
			jsonData = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", " +
						"\"records\":[]}";
		}

		return jsonData;
	}

	@RequestMapping(value = "/uploadUpdateFile" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadUpdateFile(HttpServletRequest request, HttpSession session, @RequestParam("data") String data, @RequestParam("file") MultipartFile file) throws Exception{
		
		Map<String, String> columnsMap = new HashMap<String, String>();
		columnsMap.put(UploadConstants.STUDENT_ID_LABEL, "id");
		columnsMap.put(UploadConstants.STUDENT_ACTION_LABEL, "studentUploadAction");

		columnsMap.putAll(STUDENT_UPLOAD_COLUMNS_MAP);

		columnsMap.put(UploadConstants.FATHER_ID_LABEL, "fatherId");
		columnsMap.put(UploadConstants.FATHER_ACTION_LABEL, "fatherUploadAction");
		columnsMap.putAll(FATHER_UPLOAD_COLUMNS_MAP);

		columnsMap.put(UploadConstants.MOTHER_ID_LABEL, "motherId");
		columnsMap.put(UploadConstants.MOTHER_ACTION_LABEL, "motherUploadAction");
		columnsMap.putAll(MOTHER_UPLOAD_COLUMNS_MAP);

		columnsMap.put(UploadConstants.GUARDIAN_ID_LABEL, "guardianId");
		columnsMap.put(UploadConstants.GUARDIAN_ACTION_LABEL, "guardianUploadAction");
		columnsMap.putAll(GUARDIAN_UPLOAD_COLUMNS_MAP);

		logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Reading file started at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		loadDataFromContext(request);

		InputStream fileInputStream = file.getInputStream();
		List<Student> students = null;
		try {
			students = ExcelUtils.parseIgnoringErrors(columnsMap, fileInputStream, Student.class);
			logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Reading file completed at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}finally{
			if(fileInputStream != null){
				fileInputStream.close();
			}
		}

		String jsonData = null;
		if(students != null && students.size() > 0){
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.readTree(data);

			Integer branchId = ((node != null && node.get("branchId") != null) ? 
					node.get("branchId").asInt()  : null);
			checkUserBranch(branchId, session);
			Map<String, List<Student> > studentMap = new LinkedHashMap<>();
			studentMap.put("Add", new ArrayList<Student>());
			studentMap.put("Update", new ArrayList<Student>());
			studentMap.put("Delete", new ArrayList<Student>());
			List<ValidationError> validationList = validateAndPrepareStudents(students, branchId, studentMap);

			if(validationList.size() > 0){
				jsonData = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
						"\"records\":"+ow.writeValueAsString(validationList)+"}";
				return jsonData;
			}

			String token = (String) session.getAttribute("token");
			try {
				boolean flag = true;
				logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Validation completed at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				List<Student> oldStudentsData = studentDao.getStudentsByIds(studentMap.get("Update"));
				if(!studentDao.uploadStudent(studentMap.get("Add"), token)){
					flag = false;
				}
				if(!studentDao.uploadUpdateStudent(studentMap.get("Update"), token)){
					flag = false;
				}
				if(!studentDao.deleteStudents(studentMap.get("Delete"))){
					
					flag = false;
				}

				if(flag){
					if(studentMap.get("Delete").size() > 0)
						studentDao.auditStudentsDelete((Integer) session.getAttribute("userId"), studentMap.get("Delete"), getIpAddress(request));
					if(studentMap.get("Update").size() > 0)
						studentDao.auditStudentsUpdate((Integer) session.getAttribute("userId"), oldStudentsData, getIpAddress(request));
					jsonData = "{\"status\":\"success\", \"message\" : \"Students uploaded successfully.\", " +
							"\"records\" : []}";
				}
				else {
					jsonData = "{\"status\":\"fail\", \"message\" : \"problem while uploading records.\", " +
							"\"records\" : []}";
				}
				logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Student insertion completed at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			}catch(Exception e){
				jsonData = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\", " +
						"\"records\" : []}";
				logger.error("Error while uploading file..", e);
			}
		}
		else {
			jsonData = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", " +
					"\"records\":[]}";
		}
		
		return jsonData;
	}

	private List<StudentParent> formStudentParentList(Student student) throws ParseException {
		List<StudentParent> parents = new ArrayList<>();
		StudentParent parent = getFatherDetails(student);
		if(parent != null)  
			parents.add(parent);
		parent = getMotherDetails(student);
		if(parent != null) 
			parents.add(parent);
		parent = getGuardianDetails(student);
		if(parent != null) 
			parents.add(parent);
		return parents;
	}

	private StudentParent getFatherDetails(Student student) throws ParseException {
		StudentParent father =null;
		if(student.isFatherAvailable()) {
			father = new StudentParent();
			father.setId(student.getFatherId());
			father.setUploadAction(student.getFatherUploadAction());
			father.setFirstname(student.getFatherFirstname());
			father.setMiddlename(student.getFatherMiddleName());
			father.setLastname(student.getFatherSurname());
			father.setDob(student.getFatherBirthDate());
			father.setEmail(student.getFatherEmailAddress());
			father.setGender("M");
			father.setPrimaryContact(student.getFatherPrimaryMobileNumber());
			father.setSecondaryContact(student.getFatherSecondaryMobileNumber());
			father.setAddressLine(student.getFatherAddressLine1());
			father.setArea(student.getFatherArea());
			father.setCity(student.getFatherCity());
			father.setPincode(student.getPincode());
			father.setRelationId(1);
			father.setProfession(student.getFatherProfession());
			father.setIncomeRange(student.getFatherIncome());
			father.setCreateLogin(student.getCreateFatherLogin());
			father.setParentUserId(student.getFatherUserId());
			
		}
		return father;
	}

	private StudentParent getMotherDetails(Student student) throws ParseException {
		StudentParent mother =null;
		if(student.isMotherAvailable()) {
			mother = new StudentParent();
			mother.setId(student.getMotherId());
			mother.setUploadAction(student.getMotherUploadAction());
			mother.setFirstname(student.getMotherFirstname());
			mother.setMiddlename(student.getMotherMiddleName());
			mother.setLastname(student.getMotherSurname());
			mother.setDob(student.getMotherBirthDate());
			mother.setEmail(student.getMotherEmailAddress());
			mother.setGender("F");
			mother.setPrimaryContact(student.getMotherPrimaryMobileNumber());
			mother.setSecondaryContact(student.getMotherSecondaryMobileNumber());
			mother.setAddressLine(student.getMotherAddressLine1());
			mother.setArea(student.getMotherArea());
			mother.setCity(student.getMotherCity());
			mother.setPincode(student.getPincode());
			mother.setRelationId(2);
			mother.setProfession(student.getMotherProfession());
			mother.setIncomeRange(student.getMotherIncome());
			mother.setCreateLogin(student.getCreateMotherLogin());
			mother.setParentUserId(student.getMotherUserId());
		}
		return mother;
	}

	private StudentParent getGuardianDetails(Student student) throws ParseException {
		StudentParent parent =null;
		if(student.isParentAvailable()) {
			parent = new StudentParent();
			parent.setId(student.getGuardianId());
			parent.setUploadAction(student.getGuardianUploadAction());
			parent.setFirstname(student.getParentFirstname());
			parent.setMiddlename(student.getParentMiddleName());
			parent.setLastname(student.getParentSurname());
			parent.setDob(student.getParentBirthDate());
			parent.setEmail(student.getParentEmailAddress());
			parent.setGender(student.getParentGender());
			parent.setPrimaryContact(student.getParentPrimaryMobileNumber());
			parent.setSecondaryContact(student.getParentSecondaryMobileNumber());
			parent.setAddressLine(student.getParentAddressLine1());
			parent.setArea(student.getParentArea());
			parent.setCity(student.getParentCity());
			parent.setPincode(student.getPincode());
			parent.setRelationId(3);
			parent.setProfession(student.getParentProfession());
			parent.setIncomeRange(student.getParentIncome());
			parent.setCreateLogin(student.getCreateParentLogin());
			parent.setParentUserId(student.getParentUserId());
		}
		return parent;
	}

	private List<ValidationError> validateAndPrepareStudents(List<Student> studentList, Integer branchId, Map<String, List<Student>> studentMap) throws ParseException {
		int row = 1;
		String standard = "";
		String division = "";
		List<ValidationError> validationList = new ArrayList<>();
		studentValidator.setStudentList(studentList);
		studentValidator.initializeData(branchId, STATE_MAP, DISTRICT_MAP_BY_STATE, TALUKA_MAP_BY_DISTRICT, INCOME_RANGE_MAP, PROFESSION_MAP, studentMap);
		int index = 0;
		for(Student student : studentList){
			row ++;
			student.setBranchId(branchId);
			student.setState(!StringUtils.isEmpty(student.getState()) ? student.getState().replace("_", " ") : null);
			student.setDistrict(!StringUtils.isEmpty(student.getDistrict()) ? student.getDistrict().replace("_", " ") : null);
			student.setTaluka(!StringUtils.isEmpty(student.getTaluka()) ? student.getTaluka().replace("_", " ") : null);

			standard = (!StringUtils.isEmpty(student.getStandardName()) ? student.getStandardName().replace("__", "") : null);
			student.setStandardName(!StringUtils.isEmpty(standard) ? standard.replace("_", " ") : null);

			division = (!StringUtils.isEmpty(student.getDivisionName()) ? student.getDivisionName().replace("__", "") : null);
			student.setDivisionName(!StringUtils.isEmpty(division) ? division.replace("_", " ") : null);

			student.setStudentParentList(formStudentParentList(student));
			studentValidator.setIndex(index);

			if(studentMap != null){
				studentValidator.setUploadUpdate(true);	
			}
			else {
				studentValidator.setUploadInsert(true);
			}
			ValidationError validationError = ValidatorUtil.validateForAllErrors(student, studentValidator);

			if(validationError != null){
				validationError.setRow("ROW : "+row);
				String title = !StringUtils.isEmpty(student.getFirstname()) && !StringUtils.isEmpty(student.getSurname()) 
						? (!StringUtils.isEmpty(student.getFirstname()) ? student.getFirstname() +" " : "") +
						(!StringUtils.isEmpty(student.getSurname()) ? student.getSurname()  : ""): "Not Available";

				validationError.setTitle(title);
				validationList.add(validationError);	
			}
			index ++;
		}
		return validationList;
	}


	 /**
	 * This method is used to get list of students.
	 * @return List<Students>
	 * @throws UnauthorizedException 
	 */ 
	 @RequestMapping(value = "/getStudentsByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Student> getStudentsByBranch(HttpSession session,
			 @RequestParam(value = "branchId") Integer branchId)throws SQLException, UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return studentDao.getStudents(branchId);
	 }

	 /**
	  * This method is used to get list of students.
	  * @return List<Students>
	  * @throws UnauthorizedException 
	  */ 
	 @RequestMapping(value = "/downloadStudentsToUpdate", method = RequestMethod.GET)
	 @ResponseBody
	 public void downloadStudentsToUpdate(HttpSession session, HttpServletResponse response, @RequestParam(value = "branchId") Integer branchId) 
			 throws Exception{
		 checkUserBranch(branchId, session);
		 List<Student> students = studentDao.getAllStudentsWithParents(branchId);
		 System.out.println("********************"+students);
		 byte[] arr = studentsExcelTemplate.downloadStudents(students, branchId);
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		 response.setHeader("Content-Disposition", "attachment; filename=eduqfix-upload-student-template.xlsx");
		 response.getOutputStream().write(arr);
	 }

	 /**
	 * This method is used to get list of students.
	 * @return List<Students>
	 * @throws UnauthorizedException 
	 */ 
	 @RequestMapping(value = "/getStudentsByStandardDivision", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Student> getStudentsByStandardDivision(HttpSession session, 
			 @RequestParam(value = "branchId") Integer branchId,
			 @RequestParam(value = "standardId" , required = false) Integer standardId,
			 @RequestParam(value = "divisionId", required = false) Integer divisionId)throws SQLException, UnauthorizedException{

		 checkUserBranch(branchId, session);
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			 if(standardId != null){
				 validateStandard(branchId, standardId);
			 }

			 if(divisionId != null){
				 validateDivision(branchId, standardId, divisionId);
			 }
		 }
	 	 return studentDao.getStudentsByStandardDivision(branchId, standardId, divisionId);
	 }


	/**
	 * This method is called when we add new Student or edit Student. Based on the Student Id it will decide whether to add or edit a Student. 
	 * @param Student
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/save", method = RequestMethod.POST)
	 @ResponseBody
	 public String saveStudents(HttpServletRequest request, HttpSession session, @NotNull @NotEmpty @RequestParam("data") String data,  
			@RequestParam(value = "file",required = false) MultipartFile file) throws UnauthorizedException {

		String result = null;
		ObjectMapper mapper = new ObjectMapper();
		Student student = null;
		try {
			student = mapper.readValue(data, Student.class);
//			if(student.getFeesCodeId() !=null){
//			System.out.println(student.getFeesCodeId().size()+"aaaaa");
//			}
			checkUserBranch(student.getBranchId(), session);
			

			if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId")))
			{
				validateStandard((List <Integer>) session.getAttribute("branchIdList"), student.getStandardId());
				if(student.getDivisionId()!=null && !StringUtils.isEmpty(student.getDivisionId())){//pitabas
					validateDivision((List <Integer>) session.getAttribute("branchIdList"), student.getStandardId(), student.getDivisionId());
				}
			}
			if(student != null && student.getId() != null){
				if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
					validateStudent((List<Integer>) session.getAttribute("branchIdList"), student.getId());
				}
			}

			ValidationError error = ValidatorUtil.validateForAllErrors(student, studentValidator);
			if(error != null){
				String title = "Invalid student details.";

				error.setTitle(title);
				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
					"\"message\":\"Invalida Data provided\", " +
					"\"records\" : "+mapper.writeValueAsString(error)+"}";
				return result;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try{
			boolean flag = false;
			String token = (String) session.getAttribute("token");
			if(student != null && student.getId() != null){
				Student oldStudent = studentDao.getStudentById(student.getId());
				flag = studentDao.updateStudent(student, file, token, false);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Student updated successfully.\"}";
					studentDao.auditStudentUpdate((Integer)session.getAttribute("userId"), oldStudent, getIpAddress(request));
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating student.\"}";
				}
			}
			else if(student != null ){
				flag = studentDao.insertStudent(student, file, token);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Student inserted successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while inserting student.\"}";
				}
			}
		}catch(Exception e){
			result = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\", " +
					"\"records\" : []}";
			logger.error("Error updating student..", e);
		}
		return result;
	}

	 /**
	 * This method is used to get a single student.
	 * @param id
	 * @return student
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Student getStudent(HttpSession session, @PathVariable Integer id) throws IOException, UnauthorizedException{
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			 validateStudent((List<Integer>) session.getAttribute("branchIdList"), id);
		 }
		 return studentDao.getStudentById(id);
	 }


	 /**
	 * This method is used to delete a student.
	 * @param id
	 * @throws UnauthorizedException 
	 * @throws JsonProcessingException 
	 */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	 public String deleteStudents(HttpServletRequest request, HttpSession session, @RequestParam Integer id) throws UnauthorizedException, JsonProcessingException {
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			 validateStudent((List<Integer>) session.getAttribute("branchIdList"), id);
		 }
		 String result = "";
		 Student student = studentDao.getStudentById(id);
		 if(studentDao.deleteStudent(id)){
			 result = "{\"status\" : \"success\", \"message\" : \"Student deleted successfully\"}";
			 
			 studentDao.auditStudentDelete((Integer)session.getAttribute("userId"), student, getIpAddress(request));
		 }
		 else {
			 result = "{\"status\" : \"fail\", \"message\" : \"Problem while deleting student\"}";
		 }
		 return result;
	 }


	@RequestMapping(value = "/report", method = RequestMethod.POST)
	@ResponseBody
	public TableResponse getStudentReport(HttpSession session,  @RequestBody StudentExcelReportFilter filter) throws UnauthorizedException {
		
		logger.debug("GET Student request :: --"+filter);
		checkUserBranch(filter.getBranchId(), session);
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			if(filter.getStandardId() != null) {
				validateStandard(filter.getBranchId(), filter.getStandardId());
				if(filter.getDivisionId() != null ) {
					validateDivision(filter.getBranchId(), filter.getStandardId(), filter.getDivisionId());
				}
			}
		}
		else if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER) && filter.getStandardId() != null && filter.getDivisionId() != null){
			filter.setTeacherUserId((Integer) session.getAttribute("userId"));
			if(masterValidator.isStandardAndDivisionApplicableForTeacher((Integer) session.getAttribute("roleId"), 
					filter.getStandardId(), filter.getDivisionId())){
				throw new UnauthorizedException("This teacher does not belongs to this standard and division.");
			}
		}
		return studentDao.getStudentReport(filter);
	}


	@RequestMapping(value = "/downloadReport", method = RequestMethod.GET)
	public void downloadReport(HttpSession session, HttpServletResponse response,
			@RequestParam(value = "instituteId", required = true) Integer instituteId,
			@RequestParam(value = "branchId", required = true) Integer branchId, 
			@RequestParam(value = "standardId", required = false) Integer standardId,  
			@RequestParam(value = "divisionId", required = false) Integer divisionId,
			@RequestParam(value = "teacherUserId", required = false) Integer teacherUserId) throws IOException, UnauthorizedException {

		StudentExcelReportFilter filter = new StudentExcelReportFilter();
		filter.setInstituteId(instituteId);
		filter.setBranchId(branchId);
		filter.setStandardId(standardId);
		filter.setDivisionId(divisionId);

		logger.debug("GET Student request :: --"+filter);
		checkUserBranch(filter.getBranchId(), session);
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			if(filter.getStandardId() != null) {
				validateStandard(filter.getBranchId(), filter.getStandardId());
				if(filter.getDivisionId() != null ) {
					validateDivision(filter.getBranchId(), filter.getStandardId(), filter.getDivisionId());
				}
			}
		}
		else if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER) && filter.getStandardId() != null && filter.getDivisionId() != null){
			filter.setTeacherUserId((Integer) session.getAttribute("userId"));
			if(masterValidator.isStandardAndDivisionApplicableForTeacher((Integer) session.getAttribute("userId"), 
					filter.getStandardId(), filter.getDivisionId())){
				throw new UnauthorizedException("This teacher does not belongs to this standard and division.");
			}
		}

		List<Student> feesReports = studentDao.getStudentReport(filter).getStudents();
		TreeMap<Integer, Object[]> dataMap = generateStudentReportExcelData(feesReports);
		Workbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "StudentReport.xlsx");
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=Student-Report.xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
	}


	@RequestMapping(value = "/sendNotifications", method = RequestMethod.GET)
	public void sendNotifications(HttpSession session, HttpServletResponse response,
			@RequestParam(value = "branchId", required = true) Integer branchId, 
			@RequestParam(value = "standardId", required = false) Integer standardId,  
			@RequestParam(value = "divisionId", required = false) Integer divisionId) throws IOException, UnauthorizedException {

		checkUserBranch(branchId, session);
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			if(standardId != null){
				validateStandard(branchId, standardId);
				if(divisionId != null){
					validateDivision(branchId, standardId, divisionId);
				}
			}
		}
		else if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER) && standardId != null && divisionId != null){
			if(masterValidator.isStandardAndDivisionApplicableForTeacher((Integer) session.getAttribute("roleId"), 
					standardId, divisionId)){
				throw new UnauthorizedException("This teacher does not belongs to this standard and division.");
			}
		}
		studentDao.sendNotification(branchId, standardId, divisionId);
	}


	private TreeMap<Integer, Object[]> generateStudentReportExcelData(
				List<Student> feeReports) {
		
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		excelDataMap.put(2, new Object[]{"Student report for - " + Constants.USER_DATE_FORMAT.format(new Date())});

		int rowNum = 4;
		Object[] headerArr =  new Object[]{
			"First Name",
			"Surname",
			"Date Of Birth",
			"Gender",
			"Mobile Number",
			"Email",
			"Standard",
			"Division",
			"Roll Number",
			"Registration Code",
			"Address",
			"Area",
			"City",
			"Pin Code",
			"Religion",
			"Caste",
			"Status",
		};
		excelDataMap.put(rowNum, headerArr);

		for(Student student : feeReports){
			rowNum ++;
			Object[] dataArr = {
				student.getFirstname(), 
				student.getSurname(),
				student.getDateOfBirth(),
				(student.getGender().equalsIgnoreCase("M") ? "MALE" : "FEMALE"),
				student.getMobile(),
				student.getEmailAddress(),
				student.getStandardName(),
				student.getDivisionName(),
				student.getRollNumber(),
				student.getRegistrationCode(),
				student.getAddressLine1(),
				student.getArea(),
				student.getCity(),
				student.getPincode(),
				student.getReligion(),
				student.getCaste(),
				(student.getActive().equalsIgnoreCase("Y") ? "ACTIVE" : "INACTIVE")
			};
			excelDataMap.put(rowNum, dataArr);
		}
		return excelDataMap;
	}
	
	
	 @RequestMapping(value = "/exportStudentData",method = RequestMethod.GET)
	 @ResponseBody
	 public void exportStudentData(HttpSession session, HttpServletResponse response, @NotNull @NotEmpty @RequestParam("branchId") Integer branchId,
			 @RequestParam(value = "standardId", required = false) Integer standardId,
			 @RequestParam(value = "divisionId", required = false) Integer divisionId) throws Exception{

		 checkUserBranch(branchId, session);
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN) || session.getAttribute("roleId").equals(Constants.ROLE_ID_SCHOOL_ADMIN)){
			 if(standardId!=null){
				 validateStandard(branchId, standardId);
				 if(divisionId!=null){
					 validateDivision(branchId, standardId , divisionId);
				 }
			 }
			 
			 
		 }
		 List<Student> students = studentDao.exportStudentData(branchId, standardId, divisionId);
		 TreeMap<Integer, Object[]> dataMap = generateStudentsExcelData(students);
		 Workbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "StudentLoginDetails.xlsx");
		 response.setContentType("application/vnd.ms-excel");
		 response.setHeader("Content-Disposition", "attachment; filename=Student-Login-Details.xlsx");
		 ServletOutputStream outStream = response.getOutputStream();
		 workbook.write(outStream); // Write workbook to response.
		 outStream.close();
	 }

	 private TreeMap<Integer, Object[]> generateStudentsExcelData(
				List<Student> students) {
		
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		excelDataMap.put(2, new Object[]{"Student report for - " + Constants.USER_DATE_FORMAT.format(new Date())});
		if(students != null && students.size() > 0){
		excelDataMap.put(3, new Object[]{"Branch - " + students.get(0).getBranch()});
		}
		int rowNum = 5;
		Object[] headerArr =  new Object[]{
			"First Name",
			"Surname",
			"Mobile",
			"Email",
			"Standard",
			"Division",
			"Roll Number",
			"Registration Code",
			"Username",
			"Password"
		};
		excelDataMap.put(rowNum, headerArr);

		for(Student student : students){
			rowNum ++;
			Object[] dataArr = {
				student.getFirstname(), 
				student.getSurname(),
				student.getMobile(),
				student.getEmailAddress(),
				student.getStandardName(),
				student.getDivisionName(),
				student.getRollNumber(),
				student.getRegistrationCode(),
				student.getUsername(),
				student.getPassword()
			};
			excelDataMap.put(rowNum, dataArr);
		}
		return excelDataMap;
	}

@RequestMapping(value = "/exportParentData",method = RequestMethod.GET)
@ResponseBody
public void exportParentData(HttpSession session, HttpServletResponse response, @NotNull @NotEmpty @RequestParam("branchId") Integer branchId,
		 @RequestParam(value = "standardId", required = false) Integer standardId,
		 @RequestParam(value = "divisionId", required = false) Integer divisionId) throws Exception{	
	 checkUserBranch(branchId, session);
	 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN) || session.getAttribute("roleId").equals(Constants.ROLE_ID_SCHOOL_ADMIN)){
		 if(standardId!=null){
			 validateStandard(branchId, standardId);
			 if(divisionId!=null){
				 validateDivision(branchId, standardId , divisionId);
			 }
		 }
		/* validateStandard(branchId, standardId);
		 validateDivision(branchId, standardId , divisionId);
*/	 }
	 List<StudentParent> students = studentDao.exportParentData(branchId, standardId, divisionId);
	 TreeMap<Integer, Object[]> dataMap = generateParentsExcelData(students);
	 Workbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "ParentLoginDetails.xlsx");
	 response.setContentType("application/vnd.ms-excel");
	 response.setHeader("Content-Disposition", "attachment; filename=Parent-Login-Details.xlsx");
	 ServletOutputStream outStream = response.getOutputStream();
	 workbook.write(outStream); // Write workbook to response.
	 outStream.close();
}
	
	private TreeMap<Integer, Object[]> generateParentsExcelData(
				List<StudentParent> students) {
		
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		excelDataMap.put(2, new Object[]{"Parent report for - " + Constants.USER_DATE_FORMAT.format(new Date())});
		if(students != null && students.size() > 0){
		excelDataMap.put(3, new Object[]{"Branch - " + students.get(0).getStudent().getBranch()});
		}
		int rowNum = 5;
		Object[] headerArr =  new Object[]{
			"First Name",
			"Surname",
			"Mobile",
			"Email",
			"Username",
			"Password",
			"Student Name",
			"Standard",
			"Division",
			"Roll Number",
			"Registration Code",
		};
		excelDataMap.put(rowNum, headerArr);
	
		for(StudentParent parent : students){
			rowNum ++;
			Student student = parent.getStudent();
			Object[] dataArr = {
				parent.getFirstname(), 
				parent.getLastname(),
				parent.getPrimaryContact(),
				parent.getEmail(),
				parent.getUsername(),
				parent.getPassword(),
				(student.getFirstname() + " " + student.getSurname()),
				student.getStandardName(),
				student.getDivisionName(),
				student.getRollNumber(),
				student.getRegistrationCode()
			};
			excelDataMap.put(rowNum, dataArr);
		}
		return excelDataMap;
	}

	@RequestMapping(value="/audit/delete/{branchId}", method=RequestMethod.GET)
	public List<AuditLog> getDeletedStudents(@PathVariable("branchId") Integer branchId) throws Exception{
		List<AuditLog> auditLogs = studentDao.getDeletedStudentsData(branchId);
		log.info("Deleted students log " + auditLogs.size());
		return auditLogs;
	}
	
	@RequestMapping(value="/audit/update/{branchId}", method=RequestMethod.GET)
	public List<AuditLog> getUpdatedStudents(@PathVariable("branchId") Integer branchId) throws JsonParseException, JsonMappingException, IOException{
		List<AuditLog> auditLogs = studentDao.getUpdatedStudentsData(branchId);
		log.info("Updated students log " + auditLogs.size());
		return auditLogs;
	}
	
	 @RequestMapping(value = "/auditLog/delete/{branchId}", method = RequestMethod.GET)
	 @ResponseBody
	 public String downloadDeleteAuditLog(HttpServletResponse response, @PathVariable("branchId") Integer branchId) throws Exception{
		List<AuditLog> auditLogs = studentDao.getAuditLog("DELETE", branchId);
		TreeMap<Integer, Object[]> dataMap = studentDao.generateStudentExcelData(auditLogs, "DELETE");
		XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "AuditReport.xls");
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=Audit-Report.xls");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
		return "audit Log";
	 }
	 
	 @RequestMapping(value = "/auditLog/update/{branchId}", method = RequestMethod.GET)
	 @ResponseBody
	 public String downloadUpdateAuditLog(HttpServletResponse response, @PathVariable("branchId") Integer branchId) throws Exception{
		List<AuditLog> auditLogs = studentDao.getAuditLog("UPDATE", branchId);
		TreeMap<Integer, Object[]> dataMap = studentDao.generateStudentExcelData(auditLogs, "UPDATE");
		XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "AuditReport.xls");
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=Audit-Report.xls");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
		return "audit Log";
	 }
	 
	 private String getIpAddress(HttpServletRequest request){
		 Enumeration<String> e = request.getHeaderNames();
		 while(e.hasMoreElements()){
			 String key = e.nextElement();
			 String value = request.getHeader(key);
			 log.info(key + " " + value);
		 }
		 if(request.getHeader("x-forwarded-for") != null)
			 return request.getHeader("x-forwarded-for").split(",")[0];
		 else
			 return request.getRemoteAddr();
	 }
}
