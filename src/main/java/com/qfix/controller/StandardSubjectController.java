package com.qfix.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.qfix.dao.IStandardSubjectDao;
import com.qfix.dao.ISubjectDao;
import com.qfix.dao.StandardInterfaceDao;
import com.qfix.daoimpl.StandardDao;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.ExpressOnboard;
import com.qfix.model.ExpressOnboardStandardSubjectList;
import com.qfix.model.Standard;
import com.qfix.model.StandardSubject;
import com.qfix.model.ExpressOnboardStandardSubject;
import com.qfix.model.Subject;
import com.qfix.model.ValidationError;
import com.qfix.sep4j.ExcelUtils;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.StandardSubjectValidator;

@RestController
@RequestMapping(value = "/standardSubject")
public class StandardSubjectController extends BaseController{
	
	@Autowired
	private Gson gson;
	
	final static Logger logger = Logger.getLogger(StandardSubjectController.class);
	@Autowired
	IStandardSubjectDao standardSubjectDAO;

	@Autowired
	ISubjectDao subjectDao;
	
	@Autowired
	private StandardInterfaceDao standardInterfaceDao;

	@Autowired
	StandardSubjectValidator standardSubjectValidator;

	@RequestMapping(value = "/uploadStandardSubjectFile" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session, @RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws Exception{
		List<StandardSubject> standardSubjectList = null;
		Map<String, String> columnsMap = new HashMap<String, String>();
		
		columnsMap.put(UploadConstants.STANDARDSUBJECT_STANDARD_NAME_LABEL+UploadConstants.STAR_LABEL, "standard");
		columnsMap.put(UploadConstants.STANDARDSUBJECT_SUBJECT_NAME_LABEL+UploadConstants.STAR_LABEL , "subject");
		
		standardSubjectList = ExcelUtils.parseIgnoringErrors(columnsMap, file.getInputStream(), StandardSubject.class);
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		Integer branchId = ((node != null && node.get("branchId") != null) ? node.get("branchId").asInt()  : null);

		checkUserBranch(branchId, session);

		if(standardSubjectList != null && standardSubjectList.size() > 0){ 
			List<ValidationError> validationList = validateStandardSubjectList(standardSubjectList, branchId);
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

			if(validationList.size() > 0){
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
						"\"records\":"+ow.writeValueAsString(validationList)+"}";
				return json;
			}
			try {
				if(standardSubjectDAO.uploadStandardSubject(standardSubjectList,branchId)){
					standardSubjectList = standardSubjectDAO.getStandardSubjectsByBranchId(branchId);
					json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", " +
							"\"records\":"+ow.writeValueAsString(standardSubjectList)+"}";
				}
			} catch (Exception e) {
				json = "{\"status\":\"fail\", \"message\":\"Failed to upload.\"}";
				logger.error("", e);
			}
		}
		else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", " +
						"\"records\":[]}";
		}

		return json;
	}


	private List<ValidationError> validateStandardSubjectList(List<StandardSubject> standardSubjectList, Integer branchId) {

		int row = 1;
		List<ValidationError> validationList = new ArrayList<>();
		standardSubjectValidator.setUpload(true);
		standardSubjectValidator.setStandardSubjectList(standardSubjectList);
		int index = 0;
		for(StandardSubject standardSubject : standardSubjectList){
			row ++;
			standardSubject.setBranchId(branchId);
			standardSubjectValidator.setIndex(index);
			ValidationError validationError = ValidatorUtil.validate(standardSubject, standardSubjectValidator);
			if(validationError != null){
				validationError.setRow("ROW : "+row);
				validationError.setTitle(standardSubject.getStandard() + " " +standardSubject.getSubject());
				validationList.add(validationError);
			}
			index ++;
		}
		return validationList;
	}

	 /**
	  * 
	  * @param session
	  * @param instituteId
	  * @param branchId
	  * @return
	  */
	 @RequestMapping(value = "/getStandardSubjectByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<StandardSubject> getStudentsByInstituteAndBranch(HttpSession session,  
			 @RequestParam(value = "branchId") Integer branchId) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return standardSubjectDAO.getStandardSubjectsByBranchId(branchId);
	 }


	/**
	  * This method is called when we add new StandardSubject or edit StandardSubject. Based on the Fee Id it will decide whether to add or edit a Fee. 
	  * @param StandardSubject
	  */
	@SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.POST)
	 @ResponseBody 
	public String saveStandardSubject(@RequestBody StandardSubject standardSubject,HttpSession session) {
		boolean flag = false;
		String result = null;
		try {
			 checkUserBranch(standardSubject.getBranchId(), session);

			 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
				 validateStandard((List<Integer>) session.getAttribute("branchIdList"), standardSubject.getStandardId());
				 for(Integer subjectId : standardSubject.getSubjectIdList()){
					 validateSubject((List<Integer>) session.getAttribute("branchIdList"), subjectId);
				 }
			 }

			standardSubjectValidator.setUpload(false);
			ValidationError error = ValidatorUtil.validate(standardSubject, standardSubjectValidator);
			ObjectMapper mapper = new ObjectMapper();
			if(error != null){
				String title = "Invalid standardSubject details.";
				error.setTitle(title);
				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
					"\"message\":\"Invalida Data provided\", " +
					"\"records\" : "+mapper.writeValueAsString(error)+"}";
				return result;
			}

			if(standardSubject.getPreviousStandardId() != null){

				if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
					validateSubjectForStandardSubject((List<Integer>) session.getAttribute("branchIdList"), standardSubject.getPreviousStandardId());
				}

				flag = standardSubjectDAO.updateStandardSubject(standardSubject);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Standard Subject updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating standard Subject.\"}";
				}
			}
			else {
				flag = standardSubjectDAO.insertStandardSubject(standardSubject);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Standard Subject added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding standard Subject.\"}";
				}
			}
		} catch (Exception e) {
			logger.error("", e);
		}

		return result;
	}


	/**
	  * This method is used to get a single fee.
	  * @param id
	  * @return fee
	  */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public StandardSubject getFee(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateSubjectForStandardSubject((List<Integer>) session.getAttribute("branchIdList"), id);
		 }
		 return standardSubjectDAO.getStandardSubjectByStandardId(id);
	 }

	 /**
	  * This method is used to delete a fee.
	  * @param id
	  */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteStandardSubject(HttpSession session, @RequestParam Integer id) throws UnauthorizedException{
		 
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateSubjectForStandardSubject((List<Integer>) session.getAttribute("branchIdList"), id);
		 }

		 boolean flag = false;
		 String result = null;
		 flag =	standardSubjectDAO.deleteStandardSubject(id);
		 if(flag == true){
				result = "{\"status\":\"success\", \"message\" : \"Standard Subject deleted successfully.\"}";
			}
			else{
				result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting standard Subject.\"}";
			}
			return result;
	}

	/**
	 * This method is used to delete a standard.
	 * @param id
	 * @throws JsonProcessingException 
	 */
	@RequestMapping(value = "/express" ,method = RequestMethod.POST)
	@ResponseBody
	public String saveStandardSubject(HttpSession session, @RequestParam("branchId") Integer branchId, 
			@RequestParam("ayId") Integer ayId,
			@RequestBody ExpressOnboardStandardSubjectList subjectExpressOnboards,
			@RequestParam("type") String type) throws Exception{

		
		checkUserBranch(branchId, session);
		
		/* Add subject */
		List<ExpressOnboardStandardSubject> subjectsToAdd = subjectExpressOnboards.getSubjectsToAdd();
		for(ExpressOnboardStandardSubject subjectExpressOnboard : subjectsToAdd){
			String name = subjectExpressOnboard.getName();
			if(type.equalsIgnoreCase("SCHOOL")){
				subjectDao.insertSubjects(name, branchId, subjectExpressOnboard.getSubjects(), ayId);
			}else if(type.equals("COLLEGE")){
				subjectDao.insertYearCourseSubject(name, branchId, ayId, subjectExpressOnboard.getCourses(), subjectExpressOnboard.getSubjects());
			}
		}
		/* Add subject */
		
		
		/*Remove subjects*/
		List<ExpressOnboardStandardSubject> subjectsToremove = subjectExpressOnboards.getSubjectsToRemove();
		for(ExpressOnboardStandardSubject subjectExpressOnboard : subjectsToremove){
			String name = subjectExpressOnboard.getName();
			if(type.equalsIgnoreCase("SCHOOL")){
				subjectDao.removeSubjects(name, branchId, subjectExpressOnboard.getSubjects(), ayId);
			}else if(type.equals("COLLEGE")){
				subjectDao.removeYearCourseSubject(name, branchId, ayId, subjectExpressOnboard.getCourses(), subjectExpressOnboard.getSubjects());
			}
		}
		/*Remove subjects*/
		
		
		return "{\"status\" : \"success\", \"message\" : \"Standard subject added successfully.\"}";
	}
	
	@RequestMapping(value="/all",method = RequestMethod.GET)
	public List<Subject> listSubjects(@RequestParam("type")String type){
		return subjectDao.listSubjects(type);
	}
	
	private String getJson(HashMap<String,Object> response){
		return gson.toJson(response);
	}
}
