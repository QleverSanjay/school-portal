package com.qfix.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qfix.dao.DivisionInterfaceDao;
import com.qfix.exceptions.DivisionAllreadyExistException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Division;
import com.qfix.utilities.Constants;

@RestController
@RequestMapping(value = "/division")
public class DivisionController extends BaseController{

	final static Logger logger = Logger.getLogger(DivisionController.class);
	@Autowired
	DivisionInterfaceDao divisionDAO;
	/**
	 * This method is used to get list of divisions.
	 * @return List<Divisions>
	 */
	 @RequestMapping(value = "getDivisionByBranch" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Division> getAllDivisionsByBranch(HttpSession session, @RequestParam("branchId") Integer branchId) throws Exception{
		 checkUserBranch(branchId, session);
		 return divisionDAO.getAll(branchId);
	 }
	 
	 /**
	 * This method is used to get list of divisions.
	 * @return List<Divisions>
	 */
	 @RequestMapping(value = "getUnusedDivisionsByStandardTimetable" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Division> getUnusedDivisionsByStandardTimetable(@RequestParam("timetableId") Integer timetableId){
		 return divisionDAO.getUnusedDivisionsByStandardTimetable(timetableId);
	 }
	 
	 /**
	 * This method is used to get list of divisions.
	 * @return List<Divisions>
	 */
	 @RequestMapping(value = "getDivisionByStandardTimetable" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Division> getDivisionByStandardTimetable(@RequestParam("standardId") Integer standardId){
		 return divisionDAO.getAllByStandardTimetable(standardId);
	 }

	 /**
	  * This method is called when we add new division or edit division. Based on the division Id it will decide whether to add or edit a division. 
	  * @param division
	 * @throws JsonProcessingException 
	 * @throws IOException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	 @ResponseBody
	public String saveDivision(HttpServletRequest request , HttpSession session, @RequestBody Division division) throws JsonProcessingException, UnauthorizedException{
		 //User user = (User)session.getAttribute("currentUser");

		 String result = null;
		 
		 try {
			 
			 checkUserBranch(division.getBranchId(), session);
			 
			 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
				 validateAcademicYear(division.getBranchId(), division.getAcademicYearId());
				 if(division.getId() != null){
					 validateDivision((List<Integer>)session.getAttribute("branchIdList"), null, division.getId());
				 }
			 }

			if (division.getId() == null) {
				
				if(divisionDAO.insertDivision(division)){
					result = "{\"status\":\"success\", \"message\":\"Division inserted successfully\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while inserting division\"}";
					}
				}	
				else {
					if(divisionDAO.updateDivision(division)){
						result = "{\"status\":\"success\", \"message\":\"Division updated successfully\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while updating division\"}";
				}
			}
		} 
		catch (DivisionAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		} 

		 return result;
	}

	 /**
	  * This method is used to get a single division.
	  * @param id
	  * @return Divisions
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Division getDivision(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateDivision((List<Integer>)session.getAttribute("branchIdList"), null, id);
		 }
		 Division division = divisionDAO.getDivisionById(id);
		 return division;
	 }

	 /**
	  * This method is used to delete a division.
	  * @param id
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteDivision(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateDivision((List<Integer>)session.getAttribute("branchIdList"), null, id);
		 }
		String result = "";
		if(divisionDAO.deleteDivision(id)){
			result = "{\"status\" : \"success\", \"message\" : \"Division deleted successfully.\" }";
		}
		else {
			result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete division\" }";
		}
		return result;
	}	
}
