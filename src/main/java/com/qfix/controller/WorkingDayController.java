package com.qfix.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.dao.IWorkingDayDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.exceptions.WorkingDayAllreadyExistException;
import com.qfix.model.WorkingDay;
import com.qfix.utilities.Constants;

@Controller
@RequestMapping(value = "/workingday")
public class WorkingDayController extends BaseController{
	
	@Autowired
	IWorkingDayDao workingDayDao;
	
	final static Logger logger = Logger.getLogger(WorkingDayController.class);

	@RequestMapping(value = "/getWorkDayByBranch",method = RequestMethod.GET)
	@ResponseBody
	public List<WorkingDay> getWorkingDay(HttpSession session,@RequestParam("branchId") Integer branchId) throws Exception{
		checkUserBranch(branchId, session);
		return workingDayDao.getAll(branchId);
	}


	@RequestMapping(value = "/getCurrentWorkingDayForBranch",method = RequestMethod.GET)
	@ResponseBody
	public WorkingDay getCurrentWorkingDayForBranch(HttpSession session,@RequestParam("branchId") Integer branchId) throws Exception{
		checkUserBranch(branchId, session);
		return workingDayDao.getCurrentWorkingDayForBranch(branchId);
	}


	/**
	  * This method is called when we add new workingDay or edit workingDay. Based on the workingDay Id it will decide whether to add or edit a workingDay. 
	  * @param workingDay
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	 @ResponseBody 
	public String saveWorkingDays(@RequestBody WorkingDay workingDay,HttpSession session) {
		 boolean flag = false;
		String result = null;
		 try {
			
			 	checkUserBranch(workingDay.getBranchId(), session);
				
				if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
					validateAcademicYear(workingDay.getBranchId(), workingDay.getCurrentAcademicYearId());
				}
				
				if(workingDay.getId() != null){
					 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
						 validateWorkingDay((List<Integer>)session.getAttribute("branchIdList"), workingDay.getId());
					 }
				flag = workingDayDao.updateWorkingDay(workingDay);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Working days updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating working days.\"}";
				}
			}
			else {
				flag = workingDayDao.insertWorkingDay(workingDay);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Working days added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding working days.\"}";
				}
			}
		}
		 catch (WorkingDayAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		} 
		 catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
		return result;
	}
	
	 /**
	  * This method is used to get a single workingDay.
	  * @param id
	  * @return workingDay
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public WorkingDay getWorkingDay(@PathVariable Integer id, HttpSession session) throws UnauthorizedException{
		 
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateWorkingDay((List<Integer>)session.getAttribute("branchIdList"), id);
		 }
		 
		 return workingDayDao.getWorkingDayById(id);
	 }
	
	 /**
	  * This method is used to delete a workingDay.
	  * @param id
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteWorkingDay(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateWorkingDay((List<Integer>)session.getAttribute("branchIdList"), id);
		 }

		 boolean flag = false;
		 String result = null;
		 flag =	workingDayDao.deleteWorkingDay(id);
		 if(flag == true){
				result = "{\"status\":\"success\", \"message\" : \"Working day deleted successfully.\"}";
			}
			else{
				result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting working day.\"}";
			}
			return result;
	}
}
