package com.qfix.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.qfix.dao.FeesInterfaceDao;
import com.qfix.dao.HeadInterfaceDao;
import com.qfix.dao.IDisplayTemplateDao;
import com.qfix.excel.FeesExcelTemplate;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.ApplicationException;
import com.qfix.exceptions.FeeStructureAlreadyExistException;
import com.qfix.exceptions.HeadAllreadyExistException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.AuditLog;
import com.qfix.model.DisplayHead;
import com.qfix.model.DisplayTemplate;
import com.qfix.model.FeePayment;
import com.qfix.model.Fees;
import com.qfix.model.FeesCode;
import com.qfix.model.FeesDescription;
import com.qfix.model.FeesRecurringDetail;
import com.qfix.model.FeesReport;
import com.qfix.model.FeesReportFilter;
import com.qfix.model.Head;
import com.qfix.model.IncorrectTransactionIdReport;
import com.qfix.model.LateFeesDetail;
import com.qfix.model.LateFeesPayment;
import com.qfix.model.SchemeCode;
import com.qfix.model.StudentFees;
import com.qfix.model.TableResponse;
import com.qfix.model.TransactionReport;
import com.qfix.model.ValidationError;
import com.qfix.model.wrapper.FeesDetailWrapper;
import com.qfix.mq.FileDataInsertDao;
import com.qfix.sep4j.ExcelUtils;
import com.qfix.utilities.Constants;
import com.qfix.utilities.DateUtil;
import com.qfix.utilities.ReadWriteExcelUtil;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.DisplayTemplateValidator;
import com.qfix.validator.FeesValidator;

@Slf4j
@RestController
@RequestMapping(value = "/fees")
@Scope(value = "session")
public class FeesController extends BaseController {
	final static Logger logger = Logger.getLogger(FeesController.class);

	private static final String RESPONSE_STATUS = "status";

	private static final String RESPONSE_FAIL = "fail";

	@Autowired
	FeesInterfaceDao feesDAO;

	@Autowired
	FeesValidator feesValidator;

	@Autowired
	IDisplayTemplateDao displayTemplateDao;

	@Autowired
	FeesExcelTemplate feesExcelTemplate;

	@Autowired
	HeadInterfaceDao headDAO;

	@Autowired
	DisplayTemplateValidator displayTemplateValidator;

	@Autowired
	private Gson gson;
	
	@Autowired
	FileDataInsertDao fileDataInsertDao;

	private static Map<String, String> FEES_UPLOAD_COLUMNS_MAP = new HashMap<String, String>();

	static {
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_HEAD_LABEL
				+ UploadConstants.STAR_LABEL, "head");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_DESCRIPTION_LABEL,
				"description");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_CASTE_LABEL, "caste");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_DUE_DATE_LABEL
				+ UploadConstants.DATE_FORMAT_LABEL
				+ UploadConstants.STAR_LABEL, "dueDate");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_START_DATE_LABEL
				+ UploadConstants.DATE_FORMAT_LABEL
				+ UploadConstants.STAR_LABEL, "fromDate");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_END_DATE_LABEL
				+ UploadConstants.DATE_FORMAT_LABEL
				+ UploadConstants.STAR_LABEL, "toDate");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_CURRENCY_LABEL,
				"currency");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_AMOUNT_LABEL
				+ UploadConstants.STAR_LABEL, "amount");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_ALLOW_REPEAT_ENTRIES_LABEL,
				"allowRepeatEntries");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.RECURRING_LABEL,
				"recurring");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.RECURRING_FREQUENCY_LABEL,
				"recurringFrequency");
		FEES_UPLOAD_COLUMNS_MAP
				.put(UploadConstants.RECURRING_BY_DAY_ONLY_IF_FREQUENCY_IS_DAILY_LABEL,
						"byDays");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_TYPE_LABEL, "feeType");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_DISPLAY_TEMPLATE_LABEL,
				"displayTemplateName");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_LATE_PAYMENT_CHARGES_LABEL,
				"lateFeeDetailName");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_STANDARD_LABEL,
				"standard");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_DIVISION_LABEL,
				"division");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_STUDENT_NAME_LABEL,
				"studentName");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_ROLL_NO_LABEL,
				"rollNo");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_REGISTRATION_CODE_LABEL,
				"registrationCode");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_FEES_CODE_LABEL,
				"feesCodeName");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_IS_PARTIAL_PAYMENT_ALLOWED_LABEL,
				"isPartialPaymentAllowed");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_FEES_SCHEME_CODE_LABEL
				+ UploadConstants.STAR_LABEL, "schemeCode");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_IS_SPLIT_PAYMENT_LABEL, "isSplitPayment");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_FEES_SEED_SCHEME_CODE__LABEL
						+ UploadConstants.STAR_LABEL, "seedSchemeCode");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_FEES_SEED_SPLIT_AMOUNT_LABEL
						+ UploadConstants.STAR_LABEL, "seedSplitAmount");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.REMINDER_START_DATE,
				"reminderStartDate");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_EMAIL_REMINDER_LABEL,
				"isEmail");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_SMS_REMINDER_LABEL,
				"isSms");
		FEES_UPLOAD_COLUMNS_MAP.put(
				UploadConstants.FEES_MOBILE_NOTIFICATION_LABEL,
				"isNotification");
		FEES_UPLOAD_COLUMNS_MAP.put(UploadConstants.FEES_FREQUENCY_LABEL,
				"frequency");
	}

	@RequestMapping(value = "/uploadFeesFile", method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session,
			@RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws IOException,
			UnauthorizedException {
		List<Fees> feesList = null;

		Map<String, String> columnsMap = FEES_UPLOAD_COLUMNS_MAP;

		feesList = ExcelUtils.parseIgnoringErrors(columnsMap,
				file.getInputStream(), Fees.class);
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		Integer branchId = ((node != null && node.get("branchId") != null) ? node
				.get("branchId").asInt() : null);
		Integer academicYearId = ((node != null && node.get("academicYearId") != null) ? node
				.get("academicYearId").asInt() : null);

		checkUserBranch(branchId, session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateAcademicYear(branchId, academicYearId);
		}

		if (feesList != null && feesList.size() > 0) {
			List<ValidationError> validationList = validateFeesList(
					feesList,
					branchId,
					academicYearId,
					feesDAO.getSchemeCodes(branchId,
							(String) session.getAttribute("token")), null);

			ObjectWriter ow = new ObjectMapper().writer()
					.withDefaultPrettyPrinter();

			if (validationList.size() > 0) {
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":"
						+ "\"Some of the data you have provided is incorrect.\", "
						+ "\"records\":"
						+ ow.writeValueAsString(validationList) + "}";
				return json;
			}
			try {
				if (feesDAO.uploadFees(feesList,
						(Integer) session.getAttribute("userId"))) {
					feesList = feesDAO.getFeesByBranchId(branchId);
					json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", "
							+ "\"records\":"
							+ ow.writeValueAsString(feesList)
							+ "}";
				} else {
					json = "{\"status\":\"fail\", \"message\":\"Problem while uploading records\"}";
				}
			} catch (FeeStructureAlreadyExistException e) {
				logger.error("", e);
			}
		} else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", "
					+ "\"records\":[]}";
		}

		return json;
	}

	@RequestMapping(value = "/uploadUpdateFile", method = RequestMethod.POST)
	@ResponseBody
	public String uploadUpdateFile(HttpServletRequest request,
			HttpSession session, @RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws IOException,
			UnauthorizedException {
		List<Fees> feesList = null;

		Map<String, String> columnsMap = new HashMap<>();

		columnsMap.put(UploadConstants.FEES_ID, "id");
		columnsMap.put(UploadConstants.FEES_ACTION, "feesAction");
		columnsMap.putAll(FEES_UPLOAD_COLUMNS_MAP);

		feesList = ExcelUtils.parseIgnoringErrors(columnsMap,
				file.getInputStream(), Fees.class);
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		Integer branchId = ((node != null && node.get("branchId") != null) ? node
				.get("branchId").asInt() : null);
		Integer academicYearId = ((node != null && node.get("academicYearId") != null) ? node
				.get("academicYearId").asInt() : null);

		checkUserBranch(branchId, session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateAcademicYear(branchId, academicYearId);
		}

		if (feesList != null && feesList.size() > 0) {
			Map<String, List<Fees>> feesMap = new HashMap<>();
			feesMap.put("Add", new ArrayList<Fees>());
			feesMap.put("Update", new ArrayList<Fees>());
			feesMap.put("Delete", new ArrayList<Fees>());

			List<ValidationError> validationList = validateFeesList(
					feesList,
					branchId,
					academicYearId,
					feesDAO.getSchemeCodes(branchId,
							(String) session.getAttribute("token")), feesMap);

			ObjectWriter ow = new ObjectMapper().writer()
					.withDefaultPrettyPrinter();

			if (validationList.size() > 0) {
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":"
						+ "\"Some of the data you have provided is incorrect.\", "
						+ "\"records\":"
						+ ow.writeValueAsString(validationList) + "}";
				return json;
			}
			try {
				List<Fees> oldFeesData = feesDAO.getFeesByIds(feesMap
						.get("Update"));
				if (feesDAO.uploadUpdateFees(feesMap,
						(Integer) session.getAttribute("userId"))) {
					if (feesMap.get("Delete").size() > 0)
						feesDAO.auditFeesDelete(
								(Integer) session.getAttribute("userId"),
								feesMap.get("Delete"), getIpAddress(request));
					if (feesMap.get("Update").size() > 0)
						feesDAO.auditFeesUpdate(
								(Integer) session.getAttribute("userId"),
								oldFeesData, getIpAddress(request));

					feesList = feesDAO.getFeesByBranchId(branchId);
					json = "{\"status\":\"success\", \"message\":\"Upload update sucessfully done\", "
							+ "\"records\":"
							+ ow.writeValueAsString(feesList)
							+ "}";
				} else {
					json = "{\"status\":\"fail\", \"message\":\"Problem while uploading records\"}";
				}
			} catch (FeeStructureAlreadyExistException e) {
				logger.error("", e);
			}
		} else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", "
					+ "\"records\":[]}";
		}

		return json;
	}

	private List<ValidationError> validateFeesList(List<Fees> feesList,
			Integer branchId, Integer academicYearId,
			List<SchemeCode> schemeCodes, Map<String, List<Fees>> feesMap) {

		int row = 1;
		String standard = "";
		String lateFeeName = "";
		String division = "";
		List<ValidationError> validationList = new ArrayList<>();

		List<LateFeesDetail> lateFeesList = feesDAO.getAllLateFees(branchId);
		List<DisplayTemplate> templateList = displayTemplateDao
				.getDisplayTemplates(branchId);
		List<FeesCode> feesCodeList = feesDAO.getAllFeesCode(branchId);

		feesValidator.initialize(true, feesMap != null, feesList, schemeCodes,
				lateFeesList, templateList, feesCodeList, feesMap);

		int index = 0;
		for (Fees fees : feesList) {
			row++;
			fees.setBranchId(branchId);
			fees.setAcademicYearId(academicYearId);

			if ("Y".equals(fees.getRecurring())) {
				FeesRecurringDetail recurringDetail = new FeesRecurringDetail();
				recurringDetail.setFrequency(fees.getRecurringFrequency());
				recurringDetail.setWorkWeeks(fees.getByDays());
				fees.setRecurringDetail(recurringDetail);
			}

			if (fees.getLateFeeDetailName() != null) {
				lateFeeName = fees.getLateFeeDetailName().replace("__", "");
				fees.setLateFeeDetailName(lateFeeName.replace("_", " "));
			}

			if (fees.getStandard() != null) {
				standard = fees.getStandard().replace("__", "");
				fees.setStandard(standard.replace("_", " "));
			}

			if (fees.getDivision() != null) {
				division = fees.getDivision().replace("__", "");
				fees.setDivision(division.replace("_", " "));
			}

			if (fees.getFeesCodeName() != null) {
				String feesCodeName = fees.getFeesCodeName().replace("__", "");
				fees.setFeesCodeName(feesCodeName.replace("_", " "));
			}

			if (fees.getDisplayTemplateName() != null) {
				String dispalyTemplateName = fees.getDisplayTemplateName()
						.replace("__", "");
				fees.setDisplayTemplateName(dispalyTemplateName.replace("_",
						" "));
			}

			feesValidator.setIndex(index);

			ValidationError validationError = ValidatorUtil
					.validateForAllErrors(fees, feesValidator);
			if (validationError != null) {
				validationError.setRow("ROW : " + row);
				String title = "Fees for standard " + fees.getStandard()
						+ " And amount " + fees.getAmount();
				validationError.setTitle(title);
				validationList.add(validationError);
			}
			index++;
		}
		return validationList;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/report", method = RequestMethod.POST)
	@ResponseBody
	public TableResponse getFeesReport(HttpSession session,
			@RequestBody FeesReportFilter filter) throws Exception {

		checkUserBranch(filter.getBranchId(), session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			List<Integer> branchIds = (List<Integer>) session
					.getAttribute("branchIdList");
			if (filter.getFeesId() != null) {
				validateFee(branchIds, filter.getFeesId());
			}
			if (filter.getStandardId() != null) {
				validateStandard(branchIds, filter.getStandardId());
			}
			if (filter.getDivisionId() != null) {
				validateDivision(branchIds, filter.getStandardId(),
						filter.getDivisionId());
			}
			if (filter.getHeadId() != null) {
				validateFeeHead(filter.getBranchId(), filter.getHeadId());
			}
			if (filter.getRegistrationCode() != null) {// pitabas
				validateRegistrationCode(filter.getBranchId(),
						filter.getRegistrationCode());
			}
		}
		if (!StringUtils.isEmpty(filter.getFromDate())
				&& !StringUtils.isEmpty(filter.getToDate())) {
			DateUtil dateUtil = new DateUtil();
			Date fromDate = dateUtil.getDate(filter.getFromDate());
			Date toDate = dateUtil.getDate(filter.getToDate());
			if (fromDate == null || toDate == null) {
				throw new Exception("Please provide date in valid format..");
			}
		}

		Integer totalCount = (Integer) session
				.getAttribute("totalFeesReportCount");
		totalCount = totalCount == null ? 0 : totalCount;

		if (totalCount == 0 || "Y".equals(filter.getIsSearchClicked())) {
			totalCount = feesDAO.getFeesSummaryReportTotalCount(filter);
			session.setAttribute("totalFeesReportCount", totalCount);
		}

		TableResponse tableReponse = feesDAO.searchFeeReport(filter, false);
		tableReponse.setRecordsTotal(totalCount);
		tableReponse.setRecordsFiltered(totalCount);
		return tableReponse;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/offline/report", method = RequestMethod.POST)
	@ResponseBody
	public TableResponse getOfflineFeesReport(HttpSession session,
			@RequestBody FeesReportFilter filter) throws Exception {

		// checkUserBranch(filter.getBranchId(), session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			List<Integer> branchIds = (List<Integer>) session
					.getAttribute("branchIdList");
			if (filter.getFeesId() != null) {
				validateFee(branchIds, filter.getFeesId());
			}
			if (filter.getStandardId() != null) {
				validateStandard(branchIds, filter.getStandardId());
			}
			if (filter.getDivisionId() != null) {
				validateDivision(branchIds, filter.getStandardId(),
						filter.getDivisionId());
			}
			if (filter.getHeadId() != null) {
				validateFeeHead(filter.getBranchId(), filter.getHeadId());
			}
			if (filter.getRegistrationCode() != null) {// pitabas
				validateRegistrationCode(filter.getBranchId(),
						filter.getRegistrationCode());
			}
		}
		if (!StringUtils.isEmpty(filter.getFromDate())
				&& !StringUtils.isEmpty(filter.getToDate())) {
			DateUtil dateUtil = new DateUtil();
			Date fromDate = dateUtil.getDate(filter.getFromDate());
			Date toDate = dateUtil.getDate(filter.getToDate());
			if (fromDate == null || toDate == null) {
				throw new Exception("Please provide date in valid format..");
			}
		}

		/*
		 * Integer totalCount = (Integer)
		 * session.getAttribute("totalFeesReportCount"); totalCount = totalCount
		 * == null ? 0 : totalCount;
		 * 
		 * if(totalCount == 0 || "Y".equals(filter.getIsSearchClicked())){
		 * totalCount = feesDAO.getFeesSummaryReportTotalCount(filter);
		 * session.setAttribute("totalFeesReportCount", totalCount); }
		 */

		TableResponse tableReponse = feesDAO.getOfflinePayments(filter);
		/*
		 * tableReponse.setRecordsTotal(totalCount);
		 * tableReponse.setRecordsFiltered(totalCount);
		 */
		return tableReponse;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/downloadFeeDetailReport", method = RequestMethod.GET)
	public void downloadFeeDetailReport(
			HttpSession session,
			HttpServletResponse response,
			@RequestParam(value = "instituteId", required = true) Integer instituteId,
			@RequestParam(value = "branchId", required = true) Integer branchId,
			@RequestParam(value = "standardId", required = false) Integer standardId,
			@RequestParam(value = "divisionId", required = false) Integer divisionId,
			@RequestParam(value = "rollNumber", required = false) String rollNumber,
			@RequestParam(value = "registrationCode", required = false) String registrationCode,
			@RequestParam(value = "status", required = false) Integer status,
			@RequestParam(value = "headId", required = false) Integer headId,
			@RequestParam(value = "feesId", required = false) Integer feesId,
			@RequestParam(value = "casteId", required = false) Integer casteId,
			@RequestParam(value = "fromDate", required = false) String fromDateStr,
			@RequestParam(value = "toDate", required = false) String toDateStr)
			throws Exception {

		checkUserBranch(branchId, session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			List<Integer> branchIds = (List<Integer>) session
					.getAttribute("branchIdList");
			if (feesId != null) {
				validateFee(branchIds, feesId);
			}
			if (standardId != null) {
				validateStandard(branchIds, standardId);
			}
			if (divisionId != null) {
				validateDivision(branchIds, standardId, divisionId);
			}
			if (headId != null) {
				validateFeeHead(branchId, headId);
			}
			if (registrationCode != null) {
				validateRegistrationCode(branchId, registrationCode);

			}
		}
		if (!StringUtils.isEmpty(fromDateStr)
				&& !StringUtils.isEmpty(toDateStr)) {
			DateUtil dateUtil = new DateUtil();
			Date fromDate = dateUtil.getDate(fromDateStr);
			Date toDate = dateUtil.getDate(toDateStr);
			if (fromDate == null || toDate == null) {
				throw new Exception("Please provide date in valid format..");
			}
		}
		FeesReportFilter filter = new FeesReportFilter();
		filter.setInstituteId(instituteId);
		filter.setBranchId(branchId);
		filter.setStandardId(standardId);
		filter.setDivisionId(divisionId);
		filter.setStatus(status);
		filter.setRollNumber(rollNumber);
		filter.setRegistrationCode(registrationCode);
		filter.setHeadId(headId);
		filter.setFeesId(feesId);
		filter.setCasteId(casteId);
		filter.setFromDate(fromDateStr);
		filter.setToDate(toDateStr);

		logger.debug("Download template -- " + filter);
		List<FeesReport> feesReports = feesDAO.getFeesReport(filter);
		TreeMap<Integer, Object[]> dataMap = generateFeesDetailReportExcelData(feesReports);
		XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1,
				"FeesReport.xlsx");
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=Fees-Detail-Report.xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
	}

	@RequestMapping(value = "/downloadFeeSummaryReport", method = RequestMethod.GET)
	public void downloadFeeSummaryReport(
			HttpSession session,
			HttpServletResponse response,
			@RequestParam(value = "instituteId", required = true) Integer instituteId,
			@RequestParam(value = "branchId", required = true) Integer branchId,
			@RequestParam(value = "standardId", required = false) Integer standardId,
			@RequestParam(value = "divisionId", required = false) Integer divisionId,
			@RequestParam(value = "rollNumber", required = false) String rollNumber,
			@RequestParam(value = "registrationCode", required = false) String registrationCode,
			@RequestParam(value = "status", required = false) Integer status,
			@RequestParam(value = "headId", required = false) Integer headId,
			@RequestParam(value = "feesId", required = false) Integer feesId,
			@RequestParam(value = "casteId", required = false) Integer casteId,
			@RequestParam(value = "fromDate", required = false) String fromDateStr,
			@RequestParam(value = "toDate", required = false) String toDateStr)
			throws Exception {

		checkUserBranch(branchId, session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			List<Integer> branchIds = new ArrayList<>();
			branchIds.add(branchId);
			if (feesId != null) {
				validateFee(branchIds, feesId);
			}
			if (standardId != null) {
				validateStandard(branchIds, standardId);
			}
			if (divisionId != null) {
				validateDivision(branchIds, standardId, divisionId);
			}
			if (headId != null) {
				validateFeeHead(branchId, headId);
			}
			if (registrationCode != null) {
				validateRegistrationCode(branchId, registrationCode);// pitabas27/01/2017
			}
		}
		if (!StringUtils.isEmpty(fromDateStr)
				&& !StringUtils.isEmpty(toDateStr)) {
			DateUtil dateUtil = new DateUtil();
			Date fromDate = dateUtil.getDate(fromDateStr);
			Date toDate = dateUtil.getDate(toDateStr);
			if (fromDate == null || toDate == null) {
				throw new Exception("Please provide date in valid format..");
			}
		}
		FeesReportFilter filter = new FeesReportFilter();
		filter.setInstituteId(instituteId);
		filter.setBranchId(branchId);
		filter.setStandardId(standardId);
		filter.setDivisionId(divisionId);
		filter.setStatus(status);
		filter.setRollNumber(rollNumber);
		filter.setHeadId(headId);
		filter.setFeesId(feesId);
		filter.setCasteId(casteId);
		filter.setRegistrationCode(registrationCode);
		filter.setFromDate(fromDateStr);
		filter.setToDate(toDateStr);

		List<SchemeCode> schemeCodes = feesDAO.getSchemeCodes(branchId,
				(String) session.getAttribute("token"));
		List<DisplayHead> displayHeads = displayTemplateDao
				.getDisplayHeads(branchId);

		logger.debug("Download template -- " + filter);
		List<FeesReport> feesReports = feesDAO.getFeeSummaryReport(filter);
		TreeMap<Integer, Object[]> dataMap = generateFeesSummaryReportExcelData(feesReports, displayHeads, schemeCodes);
		XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1,
				"FeesReport.xlsx");
		String branchName = feesReports != null && feesReports.size() > 0 ? feesReports
				.get(0).getBranchName() : "NO DATA ";
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename="
				+ branchName + "-"
				+ ((status == null ? "All" : status == 1 ? "PAID" : "UNPAID"))
				+ ".xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
	}

	@RequestMapping(value = "/schemeCodes", method = RequestMethod.GET)
	@ResponseBody
	public List<SchemeCode> getSchemeCodes(HttpSession session,
			@RequestParam("branchId") Integer branchId)
			throws UnauthorizedException {
		checkUserBranch(branchId, session);
		return feesDAO.getSchemeCodes(branchId,
				(String) session.getAttribute("token"));
	}

	/**
	 * 
	 * @param session
	 * @param instituteId
	 * @param branchId
	 * @return
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getFeesPaymentDescription", method = RequestMethod.GET)
	@ResponseBody
	public List<FeesDescription> getFeesPaymentDescription(HttpSession session,
			@RequestParam(value = "feeScheduleId") Long feeScheduleId)
			throws UnauthorizedException {

		/*
		 * List<Integer> branchIds = (List<Integer>) session
		 * .getAttribute("branchIdList");
		 */
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			// validateFee(branchIds, feesId);
			// validateStudent(branchIds, studentId);
		}
		return feesDAO.getFeesPaymentDescription(feeScheduleId,
				(Integer) session.getAttribute("roleId"),
				(Integer) session.getAttribute("userId"));
	}

	/**
	 * 
	 * @param session
	 * @param instituteId
	 * @param branchId
	 * @return
	 * @throws UnauthorizedException
	 */
	@RequestMapping(value = "/getFeesByBranch", method = RequestMethod.GET)
	@ResponseBody
	public List<Fees> getFeesByBranch(HttpSession session,
			@RequestParam(value = "branchId") Integer branchId)
			throws UnauthorizedException {

		checkUserBranch(branchId, session);
		return feesDAO.getFeesByBranchId(branchId);
	}

	/**
	 * This method is called when we need to mark fees manually by school.
	 * 
	 * @param FeePayment
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/markFeePaid", method = RequestMethod.POST)
	@ResponseBody
	public void markFeePaid(HttpSession session,
			@RequestBody FeePayment feePayment) throws Exception {
		// TODO validate amount to be update for fees also..
		feePayment.setCreatedBy((Integer) session.getAttribute("userId"));
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			List<Integer> branchIds = (List<Integer>) session
					.getAttribute("branchIdList");
			validateFee(branchIds, feePayment.getFeeId());
			validateStudent(branchIds, feePayment.getStudentId());
		}
		logger.debug("Mark Paid Request :::>>>" + feePayment);
		feesDAO.markFeePaid(feePayment);
	}

	/**
	 * This method is called when we need to mark fees manually by school.
	 * 
	 * @param FeePayment
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/deletePaidFees", method = RequestMethod.POST)
	@ResponseBody
	public void deletePaidFees(HttpSession session,
			@RequestBody List<Long> paymentIdList)
			throws UnauthorizedException {
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateFeesScheduleIds((List<Integer>) session.getAttribute("branchIdList"), paymentIdList);
		}
		feesDAO.deletePaidFees(paymentIdList);
	}

	/**
	 * This method is called when we need to mark fees manually by school.
	 * 
	 * @param FeePayment
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/updatePaidFees", method = RequestMethod.POST)
	@ResponseBody
	public void updatePaidFees(HttpSession session,
			@RequestBody List<FeesDescription> feesDescriptionList)
			throws UnauthorizedException {
		// TODO validate amount to be update for fees also..
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			List<Integer> branchIds = (List<Integer>) session
					.getAttribute("branchIdList");
			for (FeesDescription feesDescription : feesDescriptionList) {
				validateFee(branchIds, feesDescription.getFeesId());
				validateStudent(branchIds, feesDescription.getStudentId());
			}
		}
		feesDAO.updatePaidFees(feesDescriptionList);
	}

	/**
	 * This method is called when we need to mark fees manually by school.
	 * 
	 * @param FeePayment
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/markLatePayment", method = RequestMethod.POST)
	@ResponseBody
	public void markLatePayment(HttpSession session,
			@RequestBody LateFeesPayment latePayment) throws Exception {
		// TODO validate amount to be update for fees also..
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			List<Integer> branchIds = (List<Integer>) session
					.getAttribute("branchIdList");
			for (StudentFees studentFees : latePayment.getStudentFeesList()) {
				validateFee(branchIds, studentFees.getFeesId());
				validateStudent(branchIds, studentFees.getStudentId());
			}
		}
		feesDAO.setLateFeesCharge(latePayment);
	}

	/**
	 * This method is called when we add new Fees or edit Fees. Based on the Fee
	 * Id it will decide whether to add or edit a Fee.
	 * 
	 * @param Fees
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String saveFees(@RequestBody Fees fees, HttpSession session,
			HttpServletRequest request) throws Exception {
		boolean flag = false;
		String result = null;
		if (fees.getIsEmail() == null && fees.getIsSms() == null
				&& fees.getIsNotification() == null) {

			fees.setFrequency(null);
			fees.setReminderStartDate(null);
		}
		try {

			checkUserBranch(fees.getBranchId(), session);

			if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
				validateAcademicYear(fees.getBranchId(),
						fees.getAcademicYearId());
				validateFeeHead(fees.getBranchId(), fees.getHeadId());
				if (fees.getFeesCodeId() != null) {
					validateFeesCode(
							(List<Integer>) session
									.getAttribute("branchIdList"),
							fees.getFeesCodeId());
				}
				if (fees.getStandardId() != null) {
					validateStandard(
							(List<Integer>) session
									.getAttribute("branchIdList"),
							fees.getStandardId());
				}
				if (fees.getStandardId() != null
						&& fees.getDivisionId() != null) {
					validateDivision(
							(List<Integer>) session
									.getAttribute("branchIdList"),
							fees.getStandardId(), fees.getDivisionId());
				}
			}

			feesValidator.initialize(false, false, null, null, null, null,
					null, null);
			ValidationError error = ValidatorUtil.validateForAllErrors(fees,
					feesValidator);
			ObjectMapper mapper = new ObjectMapper();

			if (error != null) {
				String title = "Invalid fees details.";
				error.setTitle(title);
				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", "
						+ "\"message\":\"Invalida Data provided\", "
						+ "\"records\" : "
						+ mapper.writeValueAsString(error)
						+ "}";
				return result;
			}

			if (fees.getId() != null) {
				if (!Constants.ROLE_ID_ADMIN.equals(session
						.getAttribute("roleId"))) {
					validateFee(
							(List<Integer>) session
									.getAttribute("branchIdList"),
							fees.getId());
				}

				if (!StringUtils.isEmpty(fees.getFeesCodeName())) {
					feesDAO.addFeeCode(fees);
				}
				Fees oldFeesData = feesDAO.getFeeById(fees.getId());
				flag = feesDAO.updateFees(fees,
						(Integer) session.getAttribute("userId"));
				if (flag) {
					feesDAO.auditFeesUpdate(
							(Integer) session.getAttribute("userId"),
							oldFeesData, getIpAddress(request));
					result = "{\"status\":\"success\", \"message\" : \"Fees updated successfully.\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating fees.\"}";
				}
			} else {
				if (!StringUtils.isEmpty(fees.getFeesCodeName())) {
					feesDAO.addFeeCode(fees);
				}
				flag = feesDAO.insertFees(fees,
						(Integer) session.getAttribute("userId"));
				if (flag) {
					result = "{\"status\":\"success\", \"message\" : \"Fees added successfully.\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding fees.\"}";
				}
			}
		} catch (Exception e) {
			logger.error(
					"Exception while saving fees..." + fees.getDescription(), e);
			if (e instanceof UnauthorizedException) {
				throw e;
			}
		}
		return result;
	}

	/**
	 * This method is used to get a single fee.
	 * 
	 * @param id
	 * @return fee
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Fees getFee(@PathVariable Integer id, HttpSession session)
			throws UnauthorizedException {
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateFee((List<Integer>) session.getAttribute("branchIdList"),
					id);
		}
		return feesDAO.getFeeById(id);
	}

	/**
	 * This method is used to delete a fee.
	 * 
	 * @param id
	 * @throws UnauthorizedException
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteFees(@RequestParam Integer id, HttpSession session,
			HttpServletRequest request) throws UnauthorizedException,
			JsonProcessingException {
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateFee((List<Integer>) session.getAttribute("branchIdList"),
					id);
		}
		boolean flag = false;
		String result = null;
		Fees fee = feesDAO.getFeeById(id);
		flag = feesDAO.deleteFee(id);
		if (flag == true) {
			result = "{\"status\":\"success\", \"message\" : \"Fees deleted successfully.\"}";
			feesDAO.auditFeesDelete((Integer) session.getAttribute("userId"),
					fee, getIpAddress(request));
		} else {
			result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting fees.\"}";
		}
		return result;
	}

	private TreeMap<Integer, Object[]> generateFeesSummaryReportExcelData(
			List<FeesReport> feeReports, List<DisplayHead> displayHeads,
			List<SchemeCode> schemeCodes) {

		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();

		excelDataMap.put(2, new Object[] { "Fees summary report for - "
				+ Constants.USER_DATE_FORMAT.format(new Date()) });

		int rowNum = 4;
		Object[] headerArr = new Object[] { "Institute", "Branch", "Standard",
				"Division", "Student Name", "Roll Number", "Registration Code",
				"Caste", "Email", "Mobile", "Fees Head", "Fees Due Date",
				"Currency", "Fees Amount", "Discount Amount",
				"Late Fees Charges", "Fees Paid", "Remaining Fees", "Status",
				"Bank Name", "Branch Name", "IFSC Code", "Cheque/DD No." };

		Map<Integer, Integer> displayHeadIndexes = new LinkedHashMap<>();
		Map<Integer, Integer> schemeCodeIndexes = new LinkedHashMap<>();
		List<Object> headerList = new ArrayList<Object>(
				Arrays.asList(headerArr));
		int index = 23;

		if (schemeCodes != null && schemeCodes.size() > 0) {
			for (SchemeCode scheme : schemeCodes) {
				headerList.add(scheme.getName());
				schemeCodeIndexes.put(scheme.getId(), index++);
			}
		}

		if (displayHeads != null && displayHeads.size() > 0) {
			for (DisplayHead head : displayHeads) {
				headerList.add(head.getName());
				displayHeadIndexes.put(head.getId(), index++);
			}
		}

		headerArr = headerList.toArray();
		excelDataMap.put(rowNum, headerArr);
		System.out
				.println("Display Head Index ::::: >>> " + displayHeadIndexes);
		for (FeesReport feeReport : feeReports) {
			if (feeReport != null) {
				rowNum++;
				Object[] dataArr = { feeReport.getInstituteName(),
						feeReport.getBranchName(), feeReport.getStandard(),
						feeReport.getDivision(), feeReport.getStudentName(),
						feeReport.getRollNumber(),
						feeReport.getRegistrationCode(),
						feeReport.getCasteName(), feeReport.getEmail(),
						feeReport.getMobile(), feeReport.getHead(),
						feeReport.getDueDate(), feeReport.getCurrencyCode(),
						feeReport.getFeeAmount(),
						feeReport.getDiscountAmount(),
						feeReport.getLatePaymentCharges(),
						feeReport.getPaidAmount(),
						feeReport.getRemainingAmount(), feeReport.getStatus(),
						feeReport.getBankName(), feeReport.getBankBranchName(),
						feeReport.getIfscCode(),
						feeReport.getChequeOrDDNumber() };

				int displayHeadSize = displayHeads != null ? displayHeads
						.size() : 0;
				int schemeCodeSize = schemeCodes != null ? schemeCodes.size()
						: 0;

				List<Object> dataList = new ArrayList<Object>(dataArr.length
						+ displayHeadSize + schemeCodeSize);
				dataList.addAll(Arrays.asList(dataArr));
				for (int i = 23; i < displayHeadSize + schemeCodeSize + 23; i++) {
					dataList.add("");
				}

				if (schemeCodes != null && schemeCodes.size() > 0) {
					Map<Integer, String> schemeCodeData = feeReport
							.getSplitSchemeCodes();
					System.out.println("schemeCodeData>>>>>" + schemeCodeData);
					if (schemeCodeData != null && schemeCodeData.size() > 0) {
						Set<Integer> schemeCodeIds = schemeCodeData.keySet();
						for (Integer schemeCodeId : schemeCodeIds) {
							String splitAmount = schemeCodeData
									.get(schemeCodeId);
							Integer schemeCodeIndex = schemeCodeIndexes
									.get(schemeCodeId);
							try {
								dataList.set(schemeCodeIndex,
										Double.parseDouble(splitAmount));
							} catch (Exception e) {
							}
						}
						dataArr = dataList.toArray();
					}
				}

				if (displayHeads != null && displayHeads.size() > 0) {
					Map<Integer, String> displayHeadData = feeReport
							.getDisplayHeads();
					System.out.println("displayHeadData>>>" + displayHeadData);
					if (displayHeadData != null && displayHeadData.size() > 0) {
						Set<Integer> displayHeadIds = displayHeadData.keySet();
						for (Integer headId : displayHeadIds) {
							String headAmount = displayHeadData.get(headId);
							Integer headIndex = displayHeadIndexes.get(headId);
							try {
								dataList.set(headIndex,
										Double.parseDouble(headAmount));
							} catch (Exception e) {
							}
						}
						dataArr = dataList.toArray();
					}
				}
				excelDataMap.put(rowNum, dataArr);
			}
		}
		return excelDataMap;
	}

	private TreeMap<Integer, Object[]> generateFeesDetailReportExcelData(
			List<FeesReport> feeReports) {
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();

		excelDataMap.put(2, new Object[] { "Fees detail report for - "
				+ Constants.USER_DATE_FORMAT.format(new Date()) });

		int rowNum = 4;
		Object[] headerArr = new Object[] { "Institute", "Branch", "Standard",
				"Division", "Student Name", "Roll Number", "Registration Code",
				"Caste", "Qfix Reference Number", "Transaction Id", "Currency",
				"Fee Amount", "Discount Amount", "Late Fees Charges",
				"Paid Amount", "Fees Head", "Fees Paid Date", "Fees Due Date",
				"Payment Option", "Payment Mode", "paymentReferenceDetails",
				"paymentDetails", "Bank Name", "Branch Name", "IFSC Code",
				"Cheque/DD No.", "Settlement Date", "Refund Amount", "Refund Date", "Refund Status" };
		excelDataMap.put(rowNum, headerArr);

		for (FeesReport feeReport : feeReports) {
			if (feeReport != null) {
				rowNum++;
				Object[] dataArr = { feeReport.getInstituteName(),
						feeReport.getBranchName(), feeReport.getStandard(),
						feeReport.getDivision(), feeReport.getStudentName(),
						feeReport.getRollNumber(),
						feeReport.getRegistrationCode(),
						feeReport.getCasteName(),
						feeReport.getQfixReferenceNumber(),
						feeReport.getPaymentGatewayTransactionId(),
						feeReport.getCurrencyCode(), feeReport.getFeeAmount(),
						feeReport.getDiscountAmount(),
						feeReport.getLatePaymentCharges(),
						feeReport.getPaidAmount(), feeReport.getHead(),
						feeReport.getPaidDate(), feeReport.getDueDate(),
						feeReport.getPaymentOption(),
						feeReport.getPaymentMode(),
						feeReport.getPaymentReferenceDetails(),
						feeReport.getPaymentDetails(), feeReport.getBankName(),
						feeReport.getBankBranchName(), feeReport.getIfscCode(),
						feeReport.getChequeOrDDNumber(),
						getSQLTimestamp(feeReport.getSettlementDate()), 
						feeReport.getRefundAmount(), feeReport.getRefundedAt(), feeReport.getRefundStatus() };
				excelDataMap.put(rowNum, dataArr);
			}
		}
		return excelDataMap;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveLateFeesDetail", method = RequestMethod.POST)
	public String saveLateFees(HttpSession session,
			@Valid @RequestBody LateFeesDetail lateFeesDetail)
			throws UnauthorizedException, SQLException {

		String res = null;
		checkUserBranch(lateFeesDetail.getBranchId(), session);

		// validate Data
		if ((Constants.LATE_FEE_TYPE.RECURRING.get().equals(lateFeesDetail
				.getType()))) {

			if (StringUtils.isEmpty(lateFeesDetail.getFrequency())) {
				return "{\"status\":\"fail\", \"message\" : \"Frequency is required when type is recurring or combination.\"}";
			}
		}
		try {
			prepareLateFeeFrequency(lateFeesDetail);
		} catch (ApplicationException e) {
			return "{\"status\":\"fail\", \"message\" : \"" + e.getMessage()
					+ "\"}";
		}

		if (lateFeesDetail.getId() != null) {

			if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
				validateLateFees(lateFeesDetail.getId(),
						(List<Integer>) session.getAttribute("branchIdList"));
			}
			feesDAO.updateLateFeesDetail(lateFeesDetail);
			res = "{\"status\":\"success\", \"message\" : \"Late Fees updated successfully.\"}";

		} else {
			feesDAO.insertLateFeesDetail(lateFeesDetail);
			res = "{\"status\":\"success\", \"message\" : \"Late Fees added successfully.\"}";
		}
		return res;
	}

	private void prepareLateFeeFrequency(LateFeesDetail lateFeesDetail)
			throws ApplicationException {
		if (Constants.FREQUENCY.DAILY.get().equals(
				lateFeesDetail.getFrequency())) {

			Map<String, Boolean> weekDays = lateFeesDetail.getWeekDays();
			if (weekDays != null) {
				int count = 0;
				String byDay = "";
				for (String day : weekDays.keySet()) {
					if (weekDays.get(day).equals(true)) {
						count++;
						byDay = byDay + (count != 1 ? "," : "") + day;
					}
				}
				lateFeesDetail.setByDays(byDay);
			} else {
				throw new ApplicationException(
						"Week days are required when Daily frequency.");
			}
		} else if (Constants.FREQUENCY.MONTHLY.get().equals(
				lateFeesDetail.getFrequency())) {

			if (lateFeesDetail.getByDate() == null
					|| lateFeesDetail.getByDate().equals(0)) {
				throw new ApplicationException(
						"Date is required when monthly frequency.");
			} else if (Constants.FREQUENCY.QUARTERLY.get().equals(
					lateFeesDetail.getFrequency())) {

				if (lateFeesDetail.getByDate() == null
						|| lateFeesDetail.getByDate().equals(0)) {
					throw new ApplicationException(
							"Date is required when quarterly frequency.");
				}
			}
		}
	}

	@RequestMapping(value = "/getAllLateFees", method = RequestMethod.GET)
	public List<LateFeesDetail> getLateFees(HttpSession session,
			@RequestParam(value = "branchId", required = true) Integer branchId)
			throws UnauthorizedException {

		checkUserBranch(branchId, session);
		return feesDAO.getAllLateFees(branchId);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editLateFees", method = RequestMethod.GET)
	public LateFeesDetail editLateFees(
			HttpSession session,
			@RequestParam(value = "lateFeesId", required = true) Integer lateFeesId)
			throws UnauthorizedException {

		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateLateFees(lateFeesId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}
		return feesDAO.getLateFees(lateFeesId);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/deleteLateFees", method = RequestMethod.DELETE)
	public String deleteLateFees(
			HttpSession session,
			@RequestParam(value = "lateFeesId", required = true) Integer lateFeesId)
			throws UnauthorizedException {
		String res = null;
		if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
			validateLateFees(lateFeesId,
					(List<Integer>) session.getAttribute("branchIdList"));
		}
		feesDAO.deleteLateFeesDetail(lateFeesId);
		res = "{\"status\":\"success\", \"message\" : \"Late Fees deleted successfully.\"}";
		return res;
	}

	/**
	 * This method is used to get list of students.
	 * 
	 * @return List<Students>
	 * @throws UnauthorizedException
	 */
	@RequestMapping(value = "/downloadFeesToUpdate", method = RequestMethod.GET)
	@ResponseBody
	public void downloadFeesToUpdate(HttpSession session,
			HttpServletResponse response,
			@RequestParam(value = "branchId") Integer branchId)
			throws Exception {
		checkUserBranch(branchId, session);
		List<Fees> feesList = feesDAO.downloadFeesToUploadUpdate(branchId,
				(String) session.getAttribute("token"));
		byte[] arr = feesExcelTemplate.getExcelTemplateFile(feesList, branchId,
				(String) session.getAttribute("token"));

		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition",
				"attachment; filename=eduqfix-upload-update-fees-template.xlsx");
		response.getOutputStream().write(arr);
	}

	@RequestMapping(value = "/fees-code", method = RequestMethod.POST)
	public String saveFeesCode(@RequestBody @Valid FeesCode feesCode) {
		String result = "";
		try {
			if (feesCode.getId() != null) {
				feesDAO.updateFeesCode(feesCode);
				result = "{\"status\":\"success\", \"message\":\"Fees Code Updated Successfully\"}";
			} else {
				feesDAO.insertFeesCode(feesCode);
				result = "{\"status\":\"success\", \"message\":\"Fees Code Added Successfully\"}";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "{\"status\":\"fail\", \"message\":\"" + e.getMessage()
					+ "\"}";
		}
		return result;
	}

	@RequestMapping(value = "/fees-code/multiple", method = RequestMethod.POST)
	public String saveMultipleFeesCode(
			@RequestBody @NotNull List<FeesCode> feesCodes) {
		String result = "";
		try {
			for (FeesCode feesCode : feesCodes) {
				feesDAO.insertFeesCode(feesCode);
			}
			result = "{\"status\":\"success\", \"message\":\"Fees Codes Added Successfully\"}";
		} catch (Exception e) {
			e.printStackTrace();
			result = "{\"status\":\"fail\", \"message\":\"" + e.getMessage()
					+ "\"}";
		}
		return result;
	}

	@RequestMapping(value = "/fees-code/{id}", method = RequestMethod.GET)
	public FeesCode getFeesCodeById(@PathVariable @NotNull Integer id) {
		return feesDAO.getFeesCodeById(id);
	}

	@RequestMapping(value = "/fees-code", method = RequestMethod.DELETE)
	public String deleteFeesCode(@RequestParam @NotNull Integer id) {
		feesDAO.deleteFeesCode(id);
		return "{\"status\":\"success\", \"message\":\"Fees Code Deleted Successfully\"}";
	}

	@RequestMapping(value = "/fees-code/list/{branchId}", method = RequestMethod.GET)
	public List<FeesCode> getFeesCodes(@PathVariable @NotNull Integer branchId) {
		return feesDAO.getAllFeesCode(branchId);
	}

	@RequestMapping(value = "/express-onboard/fees-details/{branchId}", method = RequestMethod.POST)
	public String saveExpressOnboardFeesDetails(
			@PathVariable("branchId") Integer branchID,
			@RequestBody FeesDetailWrapper feesDetailWrapper,
			HttpSession session) throws UnauthorizedException, SQLException {

		try {
			checkUserBranch(branchID, session);
		} catch (UnauthorizedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		HashMap<String, Object> response = null;

		List<Head> heads = feesDetailWrapper.getDeleteHeads();
		List<DisplayHead> displayHeads = feesDetailWrapper
				.getDeleteDisplayHeads();
		List<FeesCode> codes = feesDetailWrapper.getDeleteFeesCodes();
		List<LateFeesDetail> lateFeesDetails = feesDetailWrapper
				.getDeleteLateFeesDeatil();

		// Delete records which user wants to delete
		rollbackExpressOnboardFeesData(heads, displayHeads, codes,
				lateFeesDetails);

		// Get records to save
		heads = feesDetailWrapper.getHeads();
		displayHeads = feesDetailWrapper.getDisplayHeads();
		codes = feesDetailWrapper.getFeesCodes();
		lateFeesDetails = feesDetailWrapper.getLateFeesDetails();

		for (Head head : heads) {
			response = saveExpressOnboardFeesHead(head, session);
			if (response.get(RESPONSE_STATUS).equals(RESPONSE_FAIL)) {
				rollbackExpressOnboardFeesData(heads, displayHeads, codes,
						lateFeesDetails);
				return gson.toJson(response);
			}
		}

		for (DisplayHead displayHead : displayHeads) {
			response = saveExpressOnboardDisplayHead(displayHead, session);
			if (response.get(RESPONSE_STATUS).equals(RESPONSE_FAIL)) {
				rollbackExpressOnboardFeesData(heads, displayHeads, codes,
						lateFeesDetails);
				return gson.toJson(response);
			}
		}

		for (FeesCode code : codes) {
			response = saveExpressOnboardFeesCode(code, session);
			if (response.get(RESPONSE_STATUS).equals(RESPONSE_FAIL)) {
				rollbackExpressOnboardFeesData(heads, displayHeads, codes,
						lateFeesDetails);
				return gson.toJson(response);
			}
		}

		for (LateFeesDetail feesDetail : lateFeesDetails) {
			response = saveExpressOnboardLateFee(feesDetail, session);
			if (response.get(RESPONSE_STATUS).equals(RESPONSE_FAIL)) {
				rollbackExpressOnboardFeesData(heads, displayHeads, codes,
						lateFeesDetails);
				return gson.toJson(response);
			}
		}

		response = new HashMap<String, Object>();

		response.put("status", "success");
		response.put("message", "Fees details saved successfully");
		response.put("displayHeads", displayHeads);
		response.put("feeHeads", heads);
		response.put("feeCodes", codes);
		response.put("lateFees", lateFeesDetails);

		return gson.toJson(response);
	}

	private HashMap<String, Object> saveExpressOnboardFeesHead(Head head,
			HttpSession session) throws UnauthorizedException {
		String result = null;
		try {
			if (head.getId() == null) {
				if (headDAO.insertHead(head)) {
					result = "{\"status\":\"success\", \"message\":\"Fees Head inserted successfully\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while inserting fees head\"}";
				}
			} else {
				if (!session.getAttribute("roleId").equals(
						Constants.ROLE_ID_ADMIN)) {
					validateFeeHead(head.getBranchId(), head.getId());
				}
				if (headDAO.updateHead(head)) {
					result = "{\"status\":\"success\", \"message\":\"Fees Head updated successfully\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while updating fees head\"}";
				}
			}
		} catch (HeadAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\"" + e.getMessage()
					+ "\"}";
			logger.error("", e);
		}

		return getResponse(result);
	}

	private HashMap<String, Object> saveExpressOnboardDisplayHead(
			DisplayHead head, HttpSession session) throws UnauthorizedException {
		String response = "{\"status\":\"success\", \"message\":\"Display Head Saved.\"}";
		try {
			if (head.getId() != null) {
				if (!session.getAttribute("roleId").equals(
						Constants.ROLE_ID_ADMIN)) {
					validateDisplayHead(head.getId(),
							(List<Integer>) session
									.getAttribute("branchIdList"));
				}
				displayTemplateDao.updateDisplayHead(head);
			} else {
				displayTemplateDao.insertDisplayHead(head);
				response = "{\"status\":\"success\", \"message\":\"Display Head Saved.\", \"generated_id\" : "
						+ head.getId() + "}";
			}
		} catch (ApplicationException e) {
			logger.error("", e);
			response = "{\"status\":\"fail\", \"message\":\"" + e.getMessage()
					+ "\"}";
		}

		return getResponse(response);
	}

	private HashMap<String, Object> saveExpressOnboardFeesCode(
			FeesCode feesCode, HttpSession session) {
		String result = "";
		try {
			if (feesCode.getId() != null) {
				feesDAO.updateFeesCode(feesCode);
				result = "{\"status\":\"success\", \"message\":\"Fees Code Updated Successfully\"}";
			} else {
				feesDAO.insertFeesCode(feesCode);
				result = "{\"status\":\"success\", \"message\":\"Fees Code Added Successfully\"}";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "{\"status\":\"fail\", \"message\":\"" + e.getMessage()
					+ "\"}";
		}
		return getResponse(result);

	}

	private HashMap<String, Object> saveExpressOnboardLateFee(
			LateFeesDetail lateFeesDetail, HttpSession session)
			throws UnauthorizedException, SQLException {
		String res;
		if ((Constants.LATE_FEE_TYPE.RECURRING.get().equals(
				lateFeesDetail.getType()) || Constants.LATE_FEE_TYPE.COMBINATION
				.get().equals(lateFeesDetail.getType()))) {

			if (StringUtils.isEmpty(lateFeesDetail.getFrequency())) {
				return getResponse("{\"status\":\"fail\", \"message\" : \"Frequency is required when type is recurring or combination.\"}");
			}
		}
		try {
			prepareLateFeeFrequency(lateFeesDetail);
		} catch (ApplicationException e) {
			return getResponse("{\"status\":\"fail\", \"message\" : \""
					+ e.getMessage() + "\"}");
		}

		if (lateFeesDetail.getId() != null) {

			if (!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)) {
				validateLateFees(lateFeesDetail.getId(),
						(List<Integer>) session.getAttribute("branchIdList"));
			}
			feesDAO.updateLateFeesDetail(lateFeesDetail);
			res = "{\"status\":\"success\", \"message\" : \"Late Fees updated successfully.\"}";

		} else {
			feesDAO.insertLateFeesDetail(lateFeesDetail);
			res = "{\"status\":\"success\", \"message\" : \"Late Fees added successfully.\"}";
		}
		return getResponse(res);
	}

	private void rollbackExpressOnboardFeesData(List<Head> heads,
			List<DisplayHead> displayHeads, List<FeesCode> codeIds,
			List<LateFeesDetail> lateFees) {

		if (heads != null && heads.size() > 0) {
			deleteHeads(heads);
		}

		if (displayHeads != null && displayHeads.size() > 0) {
			deleteDisplayHeads(displayHeads);
		}

		if (codeIds != null && codeIds.size() > 0) {
			deleteCodes(codeIds);
		}

		if (lateFees != null && lateFees.size() > 0) {
			deleteLateFeeDetails(lateFees);
		}
	}

	private void deleteHeads(List<Head> heads) {
		for (int i = 0; i < heads.size(); i++) {
			Head head = heads.get(i);
			headDAO.deleteHead(head.getId());
		}
	}

	private void deleteDisplayHeads(List<DisplayHead> displayHeads) {
		for (int i = 0; i < displayHeads.size(); i++) {
			DisplayHead displayHead = displayHeads.get(i);
			displayTemplateDao.deleteDisplayHead(displayHead.getId());
			displayHeads.remove(i);
		}
	}

	private void deleteCodes(List<FeesCode> codes) {
		for (int i = 0; i < codes.size(); i++) {
			FeesCode feesCode = codes.get(i);
			feesDAO.deleteFeesCode(feesCode.getId());
			codes.remove(i);
		}
	}

	private void deleteLateFeeDetails(List<LateFeesDetail> lateFees) {
		for (int i = 0; i < lateFees.size(); i++) {
			LateFeesDetail lateFee = lateFees.get(i);

			feesDAO.deleteLateFeesDetail(lateFee.getId());
			lateFees.remove(i);

		}
	}

	private HashMap<String, Object> getResponse(String response) {
		return gson.fromJson(response, HashMap.class);
	}
	
	@RequestMapping(value="/audit/delete/{branchId}", method=RequestMethod.GET)
	public List<AuditLog> getDeletedFees(@PathVariable("branchId") Integer branchId) throws JsonParseException, JsonMappingException, IOException{
		List<AuditLog> auditLogs = feesDAO.getDeletedFeesData(branchId);
		log.info("Deleted fees log " + auditLogs.size());
		return auditLogs;
	}
	
	@RequestMapping(value="/audit/update/{branchId}", method=RequestMethod.GET)
	public List<AuditLog> getUpdatedFees(@PathVariable("branchId") Integer branchId) throws JsonParseException, JsonMappingException, IOException{
		List<AuditLog> auditLogs = feesDAO.getUpdatedFeesData(branchId);
		log.info("Updated fees log " + auditLogs.size());
		return auditLogs;
	}

	@RequestMapping(value = "/auditLog/delete/{branchId}", method = RequestMethod.GET)
	public String downloadDeleteAuditLog(HttpServletResponse response, @PathVariable("branchId") Integer branchId)
			throws Exception {
		List<AuditLog> auditLogs = feesDAO.getAuditLog("DELETE", branchId);
		TreeMap<Integer, Object[]> dataMap = feesDAO.generateFeesExcelData(auditLogs, "DELETE");
		XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "AuditReport.xlsx");
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=Audit-Report.xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
		return "audit Log";
	}

	@RequestMapping(value = "/auditLog/update/{branchId}", method = RequestMethod.GET)
	public String downloadUpdateAuditLog(HttpServletResponse response, @PathVariable("branchId") Integer branchId)
			throws Exception {
		List<AuditLog> auditLogs = feesDAO.getAuditLog("UPDATE", branchId);
		TreeMap<Integer, Object[]> dataMap = feesDAO.generateFeesExcelData(auditLogs, "UPDATE");
		XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "AuditReport.xlsx");
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=Audit-Report.xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
		return "audit Log";
	}

	@RequestMapping(value = "/report/failure-transactions", method = RequestMethod.GET)
	public void downloadFailureTransactionReport(
			@RequestParam("branchId") Integer branchId,
			HttpServletResponse response) throws IOException {
		List<TransactionReport> report = feesDAO.getFailedTransaction(branchId);

		System.out.println("Total failed transactions found " + report.size());
		
		TreeMap<Integer, Object[]> dataMap = feesDAO.generateFailedTransactionExcelData(report);
		XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "Failed Transaction Reprot.xlsx");
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=Failed Transaction Reprot.xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
	}

	private String getSQLTimestamp(java.sql.Date date) {

		if (date == null) {
			return "";
		}
		Date createdAt = new Date(date.getTime());
		DateFormat dateFormat = new SimpleDateFormat("dd MMM YYYY - hh:mm a");
		return dateFormat.format(createdAt);
	 }
	 
	 private String getIpAddress(HttpServletRequest request){
		 Enumeration<String> e = request.getHeaderNames();
		 while(e.hasMoreElements()){
			 String key = e.nextElement();
			 String value = request.getHeader(key);
			 log.info(key + " " + value);
		 }
		 if(request.getHeader("x-forwarded-for") != null)
			 return request.getHeader("x-forwarded-for").split(",")[0];
		 else
			 return request.getRemoteAddr();
	 }
	 
	 @RequestMapping(value="/incorrectTransactonId", method=RequestMethod.GET)
	 public void getIncorrecTractionIdReport(HttpServletResponse response) throws IOException{
		 List<IncorrectTransactionIdReport> report = feesDAO.getIncorrectTransactionIdReport();
		 log.info("Total incorrect transaction id records " + report.size());
		 TreeMap<Integer, Object[]> dataMap = feesDAO.generateIncorrectTransactionIdReport(report);
		 XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "Awaiting_Settlement_Date_Report.xlsx");
		 response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=Awaiting_Settlement_Date_Report.xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
		 /*response.setContentType("application/csv");   
		 response.setHeader("content-disposition","attachment;filename =Awaiting_Settlement_Date_Report.csv"); 
		 feesDAO.writeReport(response.getWriter(), report);*/
	 }
	 
	 @RequestMapping(value="/uploadAwaitingSettlementDate", method=RequestMethod.POST)
	 public String uploadAwaitingSettlementDate(@RequestParam("file") MultipartFile file) throws IOException{
		 Map<String, String> columnsMap = new HashMap<String, String>();
		 columnsMap.put("Transaction Id", "transactionId");
		 columnsMap.put("Settlement Date", "date");
		 columnsMap.put("Amount", "amount");
		 columnsMap.put("Merchant Id", "merchantId");
		 columnsMap.put("Bank Name", "bankName");
		 columnsMap.put("Qfix Reference Number", "qfixReferenceNumber");
		 columnsMap.put("Bank Transaction Id", "bankTransactionId");
		 columnsMap.put("Drawer name", "drawerName");
		 columnsMap.put("Pickup location", "pickupLocation");
		 columnsMap.put("Pickup Point", "pickupPoint");
		 columnsMap.put("CL Location", "clLocation");
		 columnsMap.put("Prod Code", "prodCode");
		 
		 log.info("into uploadAwaitingSettlementDate");
		 InputStream fileInputStream = file.getInputStream();
		 List<IncorrectTransactionIdReport> report = null;
		 try {
			 report = ExcelUtils.parseIgnoringErrors(columnsMap, fileInputStream, IncorrectTransactionIdReport.class);
		 	log.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Reading file completed at "+new java.util.Date()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		 }finally{
		 	if(fileInputStream != null){
		 		fileInputStream.close();
		 	}
		 }
		 
		 /*for(IncorrectTransactionIdReport i : report)
			 fileDataInsertDao.insertSettlementDate(i.getTransactionId(), i.getDate(), 0, i.getAmount());*/
		 
		 int i = fileDataInsertDao.insertMissingTransactionId(report);
		 String jsonData = null;
		 if(i > 0){
			fileDataInsertDao.deleteMissingTransactionId(report);
			jsonData = "{\"status\":\"success\", \"message\" : \"File uploaded and processed successfully\", "+ "\"records\" : []}";
			return jsonData;
		 }
		 else{
		 	jsonData = "{\"status\":\"fail\", \"message\" : \"Problem while file upload.\", "+ "\"records\" : []}";
		 	log.error("Error while inserting data during upload for missing transaction id");
		 	return jsonData;	
		 }
	 }
	 
	 @RequestMapping(value="/emptySettlementDate/{branchId}", method=RequestMethod.GET)
	 public void getSettlementDateNullReport(@PathVariable("branchId") Integer branchId, 
			 	HttpServletResponse response) throws IOException{
		 List<FeesReport> report = feesDAO.getSettlementDateNullReport(branchId);
		 log.info("Total settlement date null records " + report.size() + " for branch id : " + branchId);
		 TreeMap<Integer, Object[]> dataMap = feesDAO.generateSettlementDateNullReport(report);
		 XSSFWorkbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "Empty_Settlement_Date_Report.xlsx");
		 response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=Empty_Settlement_Date_Report.xlsx");
		ServletOutputStream outStream = response.getOutputStream();
		workbook.write(outStream); // Write workbook to response.
		outStream.close();
	 }
}