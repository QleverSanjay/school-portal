package com.qfix.controller;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.dao.IGroupDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Group;
import com.qfix.model.StandardDivision;
import com.qfix.model.User;
import com.qfix.utilities.Constants;
import com.qfix.validator.MasterValidator;


@Controller
@RequestMapping(value = "/groups")
public class GroupController extends BaseController{
	final static Logger logger = Logger.getLogger(GroupController.class);

	@Autowired
	IGroupDao groupDao;

	@Autowired
	private MasterValidator masterValidator;

	/**
	 * This method is used to get grouplist as per branch.
	 * @param session
	 * @param branchId
	 * @return
	 */
	 @RequestMapping(value = "/getGroupsByTeacher", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Group> getGroupsByTeacher(HttpSession session){
		 return groupDao.getGroupsByTeacherUserId((Integer)session.getAttribute("userId"));
	 }	


	/**
	 * This method is used to get grouplist as per branch.
	 * @param session
	 * @param branchId
	 * @return
	 * @throws UnauthorizedException 
	 */
	 @RequestMapping(value = "/getGroupsByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Group> getGroupsByBranch(HttpSession session,@RequestParam(value = "branchId") Integer branchId) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return groupDao.getGroupsByBranchId(branchId);
	 }

	 /**
	 * This method is used to get list of Group.
	 * @return List<Group>
	 */
	 @RequestMapping(method = RequestMethod.GET)
	 @ResponseBody
	 public List<Group> getGroups(HttpSession session)throws SQLException{
		 User user = (User) session.getAttribute("currentUser");
		 return groupDao.getAll(user.getInstituteId());
	 }

	/**
	* This method is called when we add new Group or edit Group. Based on the Group Id it will decide whether to add or edit a Group. 
	* @param Group
	*/
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody 
	public String saveGroups(HttpSession session,@RequestBody Group group) {
		boolean flag = false;
		String result = null;

		 try {
			checkUserBranch(group.getBranchId(), session);

			if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
				validateAcademicYear(group.getBranchId(), group.getAcademicYearId());
			}

			if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
				Set<Integer> individuals = (group.getIndividualIdList() != null && group.getIndividualIdList().size() > 0) 
						? new HashSet<Integer>(group.getIndividualIdList()) : null;

				if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
					Integer userId = (Integer)session.getAttribute("userId");
					if(group.getId() != null){
						validateGroup(group.getBranchId(), group.getId(), true , userId);
					}
					if(individuals != null && !masterValidator.isIndividualsApplicableForThisTeacher(userId, individuals)){
						throw new UnauthorizedException("Some individuals not found for this teacher.");
					}

					if(group.getStandardDivision() != null && group.getStandardDivision().size() > 0){
						for(StandardDivision standardDivision : group.getStandardDivision()){
							masterValidator.isStandardAndDivisionApplicableForTeacher(userId,  
								standardDivision.getStandard(), standardDivision.getDivision());
						}
					}
				}
				else {
					if(group.getId() != null){
						validateGroup(group.getBranchId(), group.getId(), true , null);
					}
					if(individuals != null && !masterValidator.isIndividualsPresentInBranch(group.getBranchId(), individuals)){
						throw new UnauthorizedException("Some individuals not found for this branch.");
					}
					if(group.getStandardDivision() != null && group.getStandardDivision().size() > 0){
						for(StandardDivision standardDivision : group.getStandardDivision()){
							validateStandard(group.getBranchId(), standardDivision.getStandard());
							if(standardDivision.getDivision()!=null){
								validateDivision(group.getBranchId(), standardDivision.getStandard(), standardDivision.getDivision());
							}
						}
					}
				}
			}

			if(group.getId() != null){
				flag = groupDao.updateGroup(group);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Group And Community updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating Group And Community .\"}";
				}
			}
			else {
				flag = groupDao.insertGroup(group);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Group And Community  added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding Group And Community .\"}";
				}
			}
		} catch (Exception e) {
			logger.error("", e);
			result = "{\"status\":\"fail\", \"message\" : \"" + e.getMessage() + "\"}";
		}
		return result;
	}


	 /**
	 * This method is used to get a single group.
	 * @param id
	 * @return Group
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Group getGroup(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
				 validateGroup((List<Integer>)session.getAttribute("branchIdList"), id, true , (Integer)session.getAttribute("userId"));
			 }
			 else {
				 validateGroup((List<Integer>)session.getAttribute("branchIdList"), id, true , null);
			 }
		 }
		 return groupDao.getGroupById(id);
	 }


	 /**
	 * This method is used to delete a group.
	 * @param id
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteGroup(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
				 validateGroup((List<Integer>)session.getAttribute("branchIdList"), id, true , (Integer)session.getAttribute("userId"));
			 }
			 else {
				 validateGroup((List<Integer>)session.getAttribute("branchIdList"), id, true , null);
			 }
		 }
		 boolean flag = false;
		 String result = null;
		 flag =	groupDao.deleteGroup(id);
		 if(flag == true){
			result = "{\"status\":\"success\", \"message\" : \"Group And Community  deleted successfully.\"}";
		}
		else{
			result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting Group And Community .\"}";
		}
		return result;
	}
}
