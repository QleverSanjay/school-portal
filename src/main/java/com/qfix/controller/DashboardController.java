package com.qfix.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.daoimpl.DashboardDao;
import com.qfix.model.Dashboard;
import com.qfix.model.DashboardFeeHeadData;
import com.qfix.model.DashboardFeeReply;
import com.qfix.model.DashboardFeeStatus;
import com.qfix.model.DashboardFeeTable;
import com.qfix.model.DashboardPayment;
import com.qfix.model.Head;


@RestController
@RequestMapping(value = "/dashboard")
public class DashboardController {
	

	@Autowired
	private DashboardDao dashboardDao;
	
	

	/**
	 * This method is used to get list of dashboard.
	 * @return List<Dashboard>
	 */
	 @RequestMapping(value = "getDashboardByBranch" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Dashboard> getAllDashboardByBranch(@RequestParam("branchId") Integer branchId){
		 return dashboardDao.getAll(branchId);
	 }
	 
	 @RequestMapping(value = "getHead" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<Head> getHead(@RequestParam("branchId") Integer branchId){
		 return dashboardDao.getHead(branchId);
	 }
	 
	 @RequestMapping(value="/payments/{branchId}", method=RequestMethod.GET)
	 public List<DashboardPayment> getDashboardPayment(@PathVariable("branchId") Integer branchId, 
			 @RequestParam("days") Integer days){
		 return dashboardDao.getDashboardPayment(branchId, days);
	 }
	 
	 @RequestMapping(value="/feeStatus/{branchId}", method=RequestMethod.GET)
	 public List<DashboardFeeReply> getDashboardFeeStatus(@PathVariable("branchId") Integer branchId){
		 return dashboardDao.getDashboardFeeStatus(branchId);
	 }
	 
	 @RequestMapping(value="/feeHeadTable/{branchId}", method=RequestMethod.GET)
	 public List<DashboardFeeTable> getDashboardFeeTableData(@PathVariable("branchId") Integer branchId){
		 return dashboardDao.getDashboardFeeTableData(branchId);
	 } 
}
