package com.qfix.controller;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ErrorHandler
 */
@WebServlet("/ErrorHandler")
public class ErrorHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ErrorHandler() {
		super();
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Analyze the servlet exception
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		String errorMessage = "";

		System.err.println("/*******************ERROR*******************/");
		

		if(statusCode == HttpServletResponse.SC_FORBIDDEN){
			errorMessage = "Request timed out !";
		}
		else if(statusCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR){
			errorMessage = "Internal server error !";
		}
		else if(statusCode == HttpServletResponse.SC_GATEWAY_TIMEOUT){
			errorMessage = "Request timed out !";
		}
		else if(statusCode == HttpServletResponse.SC_REQUEST_TIMEOUT){
			errorMessage = "Request timed out!";
		}
		else if(statusCode == HttpServletResponse.SC_NOT_FOUND){
			errorMessage = "404 Page Not Found.";
		}
		else if(statusCode == HttpServletResponse.SC_UNAUTHORIZED){
			errorMessage = "401 Unauthorised access.";
		}
		else {
			errorMessage = "Problem with server.";
		}

		System.err.println("ERROR :: "+errorMessage);
		PrintWriter out = response.getWriter();
		out.write("<html><body><center>");
		out.write("<br><br><h3>ERROR</h3>");
		out.write("<h4>"+errorMessage+"</h4>");
		out.write("</center><body></html>");
		response.setStatus(statusCode);
		/*request.setAttribute("errorMessage", errorMessage);
		request.getRequestDispatcher("error.jsp").forward(request, response);*/

	}

}
