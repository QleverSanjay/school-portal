package com.qfix.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.dao.INoticeDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Notice;
import com.qfix.utilities.Constants;
import com.qfix.validator.MasterValidator;

@Controller
@RequestMapping (value = "/notice")
public class NoticeController extends BaseController{

	@Autowired
	INoticeDao noticeDao;

	@Autowired
	MasterValidator masterValidator;

	final static Logger logger = Logger.getLogger(NoticeController.class);
	 /**
	 * This method is used to get list of notices.
	 * @return List<Notices>
	 * @throws UnauthorizedException 
	 */
	 @RequestMapping(value = "getNoticeByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Notice> getNoticeByBranch(HttpSession session, @RequestParam("branchId") Integer branchId) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return noticeDao.getAll(branchId,  Constants.ROLE_ID_TEACHER.equals((Integer) session.getAttribute("roleId")), (Integer) session.getAttribute("userId"));
	 }
	 @RequestMapping(value = "getAssignmentsByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Notice> getAssignmentsByBranch(HttpSession session, @RequestParam("branchId") Integer branchId) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return noticeDao.getAllAssignment(branchId,  Constants.ROLE_ID_TEACHER.equals((Integer) session.getAttribute("roleId")), (Integer) session.getAttribute("userId"));
	 }

	 /**
	 * This method is used to get list of notices based on teacher id and branch.
	 * @return List<Notices>
	 * @throws UnauthorizedException 
	 */
	 @RequestMapping(value = "getNoticeByBranchForTeacher", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Notice> getNoticeByBranchForTeacher(HttpSession session, @RequestParam("branchId") Integer branchId) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return noticeDao.getTeacherNotice((Integer) session.getAttribute("userId"),branchId);
	 }

	/**
	* This method is called when we add new notice or edit notice. Based on the notice Id it will decide whether to add or edit a notice. 
	* @param notices
	*/
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveNotices(HttpServletRequest request,HttpSession session, @RequestParam("data") String data,  
				@RequestParam(value = "file",required = false) MultipartFile file) {
		 ObjectMapper mapper = new ObjectMapper();
		 boolean flag = false;

		 String result = null;
		 try {
			Notice notice = mapper.readValue(data, Notice.class);
			checkUserBranch(notice.getBranchId(), session);
			notice.setSentBy((Integer) session.getAttribute("userId"));
			
			Set<Integer> groups = null;
			Set<Integer> individuals = null;
			if(notice.getGroupIdList() != null && notice.getGroupIdList().size() > 0){
				groups = new HashSet<Integer>(notice.getGroupIdList());
			}
			if(notice.getIndividualIdList() != null && notice.getIndividualIdList().size() > 0){
				individuals = new HashSet<>(notice.getIndividualIdList());
			}

			System.out.println("GROUPS --- "+groups);
			System.out.println("INDIVIDUALS --- "+individuals);

			if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
				if(groups != null && !masterValidator.isGroupsApplicableForThisTeacher(notice.getSentBy(), groups)){
					throw new UnauthorizedException("Some groups not found for this teacher.");
				}
				if(individuals != null && !masterValidator.isIndividualsApplicableForThisTeacher(notice.getSentBy(), individuals)){
					throw new UnauthorizedException("Some individuals not found for this teacher.");
				}
			}
			else if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
				if(groups != null && !masterValidator.isGroupsPresentInThisBranch(notice.getBranchId(), groups)){
					throw new UnauthorizedException("Some groups not found for this branch.");
				}
				if(individuals != null && !masterValidator.isIndividualsPresentInBranch(notice.getBranchId(), individuals)){
					throw new UnauthorizedException("Some individuals not found for this branch.");
				}
			}

			if(groups == null && individuals == null){
				throw new Exception("Individuals or groups are mandatory.");
			}
			flag = noticeDao.insertNotice(notice, file);
			String responseMessage = "";
			String badResponseMessage = "";
			
			
			if(notice.getCategory().equals("assignment")){
				responseMessage = "\"Assignment inserted successfully.\"";
				badResponseMessage = "\"Problem while inserting assignment.\"";
			}
			else{
				responseMessage = "\"Notice inserted successfully\"";
				badResponseMessage = "\"Problem while inserting notice.\"";
			}
			
			if(flag){
				result = "{\"status\":\"success\", \"message\" : " + responseMessage + "}";
			}
			else {
				result = "{\"status\":\"fail\", \"message\" : "+ badResponseMessage +"}";
			}
		} 
		catch (Exception e) {
			result = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\"}";
			logger.error("", e);
		} 

		return result;
	}

	 /**
	 * This method is used to get a single event.
	 * @param id
	 * @return Events
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/getById",method = RequestMethod.GET)
	 @ResponseBody
	 public Notice getNotice(HttpSession session, @RequestParam(value = "id") Integer id) throws UnauthorizedException{
		 Notice notice = null;
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateNotice((List<Integer>) session.getAttribute("branchIdList"), id);
		 }
		 if(Constants.ROLE_ID_TEACHER.equals(session.getAttribute("roleId"))){
			 try {
				 checkNoticeCreatedByThisUser(id, (Integer) session.getAttribute("userId"));
			 }catch(UnauthorizedException e){
				 logger.error("", e);
				 checkNoticeBelongsToThisUser(id, (Integer) session.getAttribute("userId"));
			 }
			 notice = noticeDao.getNoticeById(id, (Integer) session.getAttribute("userId"));
		 }
		 else {
			 notice = noticeDao.getNoticeById(id, null);
		 }
		 return notice;
	 }


	/**
	* This method is used to delete a notice.
	* @param id
	* @throws UnauthorizedException 
	*/
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteNotices(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 String response = null;
	

		 if(session.getAttribute("roleId").equals(Constants.ROLE_ID_SCHOOL_ADMIN)){
			 validateNotice((List<Integer>) session.getAttribute("branchIdList"), id);
		 }
		 else if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
			 checkNoticeCreatedByThisUser(id, (Integer) session.getAttribute("userId"));
		 }

		 if(noticeDao.deleteNotice(id)){
			 response = "{\"status\":\"success\", \"message\" : \"Notice deleted successfully.\"}";
		 }
		 else {
			 response = "{\"status\":\"fail\", \"message\" : \"Problem while deleting Notice.\"}";
		 }
		 return response;
	}
}