package com.qfix.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qfix.config.AppProperties;
import com.qfix.dao.VendorInterfaceDao;
import com.qfix.daoimpl.VendorDao;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Vendor;

@Controller
@RequestMapping(value = "/vendors")
public class VendorController {
	final static Logger logger = Logger.getLogger(VendorController.class);
	
	@Autowired
	VendorInterfaceDao vendorDAO;
	
	
	@Autowired
	private AppProperties appProperties;
	
	/**
	 * This method is used to upload file and show the results on the screen.
	 * @param file
	 * @return List<Vendor>
	 * @throws IOException
	 * @throws UserAllreadyExistException 
	 */
	/*@RequestMapping(value = "/uploadFile" ,method = RequestMethod.POST)
	 @ResponseBody
	public String uploadFile(HttpSession session,@RequestParam("file") MultipartFile file) throws IOException, UserAllreadyExistException{
		user = (User) session.getAttribute("currentUser");
		String[] columns = new String[] {
								"firstName",
								"middleName",
								"lastName",
								"dateOfBirthStr",
								"emailAddress",
								"gender",
								"primaryContact",
								"secondaryContact",
								"addressLine",
								"area",
								"city",
								"pinCode",
								"active",
								"photo"
								};
		
		List<Vendor> vendorList = null;
		vendorList = CSVFileReader.readFile(file.getInputStream(), columns, new Vendor(), Vendor.class);
		
		
		String json = null;
		VendorDao vendorDao = new VendorDao();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();	
		try {
			if(vendorList != null && vendorList.size() > 0){ 
				if(vendorDao.uploadVendor(vendorList)){
					vendorList = vendorDao.getAll();
					json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", " +
							"\"records\":"+ow.writeValueAsString(vendorList)+"}";
				}
				else {
					vendorList = vendorDao.getUploadingFailedRecords();
					json = "{\"status\":\"fail\", \"message\":\"Problem while uploading following records\", " +
							"\"records\":"+ow.writeValueAsString(vendorList)+"}";
				}
			}
		}
		catch (UserAllreadyExistException e) {
			vendorList = vendorDao.getAllreadyExistVendorList();
			json = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\", " +
					"\"records\" : "+ow.writeValueAsString(vendorList)+"}";
			logger.error("", e);
		}
		
		return json;
	}*/
	
	/**
	 * This method is used to get list of Vendor.
	 * @return List<Vendor>
	 */
	
	 @RequestMapping(method = RequestMethod.GET)
	 @ResponseBody
	 public List<Vendor> getVendors(HttpSession session)throws SQLException{
		 List<Vendor> vendorList = vendorDAO.getAll();
		 return vendorList;
	 }
	 
	 /**
	  * This method is called when we add new Vendor or edit Vendor. Based on the Vendor Id it will decide whether to add or edit a Vendor. 
	  * @param Vendor
	  */
	 @RequestMapping(value = "/save",method = RequestMethod.POST)
	 @ResponseBody
	 
		public String saveVendors(HttpServletRequest request,@RequestParam("data") String data,HttpSession session,@RequestParam(value = "file",required = false) MultipartFile file) {
			
		boolean flag = false;
		String result = null;
		 ObjectMapper mapper = new ObjectMapper();
		 try {
			 Vendor vendor = mapper.readValue(data, Vendor.class);
				if(file != null){
//					String photoPath = saveFile(request, file, vendor);
					//vendor.setPhoto(photoPath);	
				}
			
			if(vendor.getId() != null){
				flag = new VendorDao().updateVendor(vendor);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Vendor updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating vendor.\"}";
				}
			}
			else {
				flag = new VendorDao().insertVendor(vendor);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Vendor added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding vendor.\"}";
				}
			}
		}
		 catch (UserAllreadyExistException e) {
				result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
				logger.error("", e);
		}
		 catch (Exception e) {
			// TODO Auto-generated catch block
			 logger.error("", e);
		}
		 
		return result;
	}
	 
	 /**
	  * This method is used to get a single vendor.
	  * @param id
	  * @return vendor
	  */
	 
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Vendor getVendor(@PathVariable Integer id){
		 return new VendorDao().getVendorById(id);
	 }
	 
	 
	 /**
	  * This method is used to insert single vendor through school login.
	  * @param id
	  * @return vendor
	 * @throws UserAllreadyExistException 
	  */
	 
	 @RequestMapping(value = "/attachVendorByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public String attachVendorByBranch(@RequestParam(value = "userId",required = true) Integer userId,
			 @RequestParam(value = "branchId",required = true) Integer branchId){
		 boolean flag = false;
		 String result = null;
		 try {
			 flag = new VendorDao().attachVendorToBranch(userId,branchId);
			 if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Vendor attached successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while attaching vendor.\"}";
				}
		 }catch (UserAllreadyExistException e) {
				result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
				logger.error("", e);
		}
		 catch (Exception e) {
			// TODO Auto-generated catch block
			 logger.error("", e);
		}
		 return result;
	 }	 
	
	 /**
	  * This method is used to delete a vendor.
	  * @param id
	  */
	 
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteVendors(@RequestParam Integer id) {
		 
		 boolean flag = false;
		 String result = null;
		 flag =	new VendorDao().deleteVendor(id);
		 if(flag == true){
				result = "{\"status\":\"success\", \"message\" : \"Vendor deleted successfully.\"}";
			}
			else{
				result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting vendor.\"}";
			}
			return result;
	}
	 
	 
	 /**
	  * This method is used to delete a vendor.
	  * @param id
	  */
	 
	 @RequestMapping(value = "/removeVendor", method = RequestMethod.GET)
	 @ResponseBody
	public String removeVendors(@RequestParam(value = "userId",required = false) Integer userId,
			 @RequestParam(value = "branchId",required = false) Integer branchId) {
		 
		 boolean flag = false;
		 String result = null;
		 flag =	new VendorDao().deAttachVendorFromBranch(userId, branchId);
		 if(flag == true){
				result = "{\"status\":\"success\", \"message\" : \"Vendor removed successfully.\"}";
			}
			else{
				result = "{\"status\":\"fail\", \"message\" : \"Problem while removing vendor.\"}";
			}
			return result;
	}
	 
//	 private String saveFile(HttpServletRequest request, MultipartFile file, Vendor vendor){
//		 
//		 FileProps fileProps = appProperties.getFileProps();
//		 
//		 String baseImagepath; /**fileProps.getInternalDocumentsPath()
//				 
//				 ReadPropertiesUtil.properties.getProperty("BASE_SYSTEM_IMAGES_PATH")
//				 +ReadPropertiesUtil.properties.getProperty("VENDOR_IMAGES_PATH");**/
//		 
//		 String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
//		 
//		 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
//		 
//		 // check if directory structure is exists.
//		 File baseDirectory = new File(baseImagepath);
//		 if(!baseDirectory.exists()){
//			 // not exists create directory structure 
//			 baseDirectory.mkdirs();
//		 }
//		 
//		 String filePath = baseImagepath + simpleDateFormat.format(new Date()) + fileExtension;
//
//		 String relativePath = ReadPropertiesUtil.properties.getProperty("IMAGE_ROOT_PATH")
//				 + ReadPropertiesUtil.properties.getProperty("VENDOR_IMAGES_PATH")
//				 + simpleDateFormat.format(vendor.getBusinessStartDate()) + fileExtension;
//		 
//		 File fileToWrite = new File(filePath);
//		 
//		 try {
//			 if(!fileToWrite.exists()){
//				 fileToWrite.createNewFile();
//			 }
//			 FileOutputStream writer = new FileOutputStream(fileToWrite);
//			 writer.write(file.getBytes());
//	         writer.close();
//		} catch (FileNotFoundException e) {
//			logger.error("", e);
//		} catch (IOException e) {
//			logger.error("", e);
//		}
//		 return relativePath;
//	 }
	 
	 /**
	  * 
	  * @param session
	  * @param branchId
	  * @return
	  */
	 @RequestMapping(value = "/getVendorByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Vendor> getVendorByInstituteAndBranch(HttpSession session, @RequestParam(value = "branchId") Integer branchId){

		 return new VendorDao().getVendorByBranchId(branchId);
	 }
	 
}
