package com.qfix.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.daoimpl.VendorReportDao;
import com.qfix.model.Report;

@Controller
@RequestMapping(value = "/vendorreport")
public class VendorReportController {
	final static Logger logger = Logger.getLogger(VendorReportController.class);

	/**
	 * This method is used to get list of Vendor.
	 * @return List<Vendor>
	 */
	 @RequestMapping(value = "getFilteredReport", method = RequestMethod.GET)
	 @ResponseBody
	 public Report getVendors(@RequestParam(value = "branchId") Integer branchId, 
			 @RequestParam(value = "fromDate") Date  fromDate, 
			 @RequestParam(value = "toDate") Date toDate,
			 HttpSession session){

		 System.out.println("BranchId =="+branchId);
		 System.out.println("fromDate ==="+fromDate);
		 System.out.println("toDate ==="+toDate);
		 
		 Report vendorReports = new VendorReportDao().getReports(branchId, fromDate, toDate);
		 
		 return vendorReports;
	 }
}