package com.qfix.controller;

import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.daoimpl.ForumDao;
import com.qfix.exceptions.BranchForumNotExistException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Forum;
import com.qfix.utilities.Constants;
import com.qfix.validator.MasterValidator;

@Controller
@RequestMapping(value = "/forums")
public class ForumController extends BaseController{
	final static Logger logger = Logger.getLogger(ForumController.class);

	@Autowired
	MasterValidator masterValidator;

	/**
	  * 
	  * @param session
	  * @param instituteId
	  * @param branchId
	  * @return
	  */
	 @RequestMapping(value = "/getForumByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Forum> getForumByInstituteAndBranch(HttpSession session, @RequestParam(value = "userId") Integer userId,
			 @RequestParam(value = "roleId") Integer roleId,@RequestParam(value = "branchId") Integer branchId) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return new ForumDao().getForumByBranchId(userId, roleId,branchId);
	 }

	 /**
	  * This method is called when we add new Forum or edit Forum. Based on the Forum Id it will decide whether to add or edit a Forum. 
	  * @param Forum
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	 @ResponseBody
	public String saveForums(HttpSession session,@RequestBody Forum forum) {
		 boolean flag = false;
		 String result = null;
		 try {
		 	checkUserBranch(forum.getBranchId(), session);
		 	
		 	if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
		 		validateAcademicYear(forum.getBranchId(), forum.getAcademicYearId());
		 		if(Constants.ROLE_ID_TEACHER.equals(session.getAttribute("roleId"))){
		 			HashSet<Integer> groupSet = new HashSet<>();
		 			groupSet.add(forum.getGroupId());
		 			masterValidator.isGroupsApplicableForThisTeacher((Integer) session.getAttribute("userId"), groupSet);
		 		}
		 		else {
		 			validateGroup(forum.getBranchId(), forum.getGroupId(), true, null);
		 		}
		 	}

		 	forum.setActive("Y");
			if(forum.getId() != null){
				if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
				 	validateForumWithBranch((List<Integer>)session.getAttribute("branchIdList"), forum.getId());
			 	}

				flag = new ForumDao().updateForum(forum);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Forum updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating forum.\"}";
				}
			}
			else {
				flag = new ForumDao().insertForum(forum);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Forum added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding forum.\"}";
				}
			}
		} 
		catch (UserAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		} catch (BranchForumNotExistException e) {
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		}
		catch(UnauthorizedException e){
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		}
		catch (Exception e) {
			logger.error("", e);
		}
		
		return result;
	}
	 
	 
	 
	 /**
	  * This method is used to get a single forum.
	  * @param id
	  * @return forum
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Forum getForum(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{

		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
		 	 validateForumWithBranch((List<Integer>)session.getAttribute("branchIdList"), id);
		 	 if(Constants.ROLE_ID_TEACHER.equals(session.getAttribute("roleId"))){
		 		 checkForumCreatedByThisUser(id, (Integer) session.getAttribute("userId"), false);
		 	 }
	 	 }
		 return new ForumDao().getForumById(id);
	 }

	 /**
	  * This method is used to delete a forum.
	  * @param id
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	 public String deleteForums(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {

		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
		 	 validateForumWithBranch((List<Integer>)session.getAttribute("branchIdList"), id);
		 	 if(Constants.ROLE_ID_TEACHER.equals(session.getAttribute("roleId"))){
		 		 checkForumCreatedByThisUser(id, (Integer) session.getAttribute("userId"), false);
		 	 }
	 	 }

		 boolean flag = false;
		 String result = null;
		 flag =	new ForumDao().deleteForum(id);
		 if(flag == true){
		 		result = "{\"status\":\"success\", \"message\" : \"Forum deleted successfully.\"}";
		 }
		 else{
			result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting forum.\"}";
		 }
		 return result;
	 }
}
