package com.qfix.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.InstituteInterfaceDao;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.InstituteAllreadyExistException;
import com.qfix.exceptions.ProfileImageUploadException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.exceptions.UserAllreadyExistException;
import com.qfix.model.Branch;
import com.qfix.model.Institute;
import com.qfix.model.ValidationError;
import com.qfix.sep4j.ExcelUtils;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.InstituteValidator;

@RestController
@RequestMapping(value = "/institute")
public class InstituteController {

	final static Logger logger = Logger.getLogger(InstituteController.class);
	@Autowired
	InstituteInterfaceDao instituteDAO;
	
	@Autowired
	InstituteValidator instituteValidator;

	/**
	 * This method is used to upload file and show the results on the screen.
	 * @param file
	 * @return List<Institutes>
	 * @throws Exception 
	 */
	@RequestMapping(value = "/uploadFile" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session, HttpServletResponse response, 
			@RequestParam("file") MultipartFile file) throws Exception{

		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			throw new UnauthorizedException("Only super admin can do this..");
		}

		Map<String, String> columnsMap = new HashMap<String, String>();

		columnsMap.put(UploadConstants.INSTITUTE_NAME_LABEL + UploadConstants.STAR_LABEL , "instituteName");
		columnsMap.put(UploadConstants.INSTITUTE_CODE_LABEL , "code");
		columnsMap.put(UploadConstants.INSTITUTE_ADMIN_FIRST_NAME_LABEL + UploadConstants.STAR_LABEL, "firstName");
		columnsMap.put(UploadConstants.INSTITUTE_ADMIN_LAST_NAME_LABEL + UploadConstants.STAR_LABEL, "lastName");
		columnsMap.put(UploadConstants.INSTITUTE_ADMIN_EMAIL_LABEL + UploadConstants.STAR_LABEL , "adminEmail");	
		columnsMap.put(UploadConstants.INSTITUTE_GENDER_LABEL + UploadConstants.STAR_LABEL, "gender");
		columnsMap.put(UploadConstants.INSTITUTE_PRIMARY_CONTACT_LABEL + UploadConstants.STAR_LABEL , "primaryContact");
		columnsMap.put(UploadConstants.INSTITUTE_CITY_LABEL + UploadConstants.STAR_LABEL, "adminCity");
		columnsMap.put(UploadConstants.INSTITUTE_PINCODE_LABEL + UploadConstants.STAR_LABEL, "adminPinCode");
		columnsMap.put(UploadConstants.INSTITUTE_STATUS_LABEL + UploadConstants.STAR_LABEL, "status");
		
		List<Institute> instituteList = null;
	//	instituteList = CSVFileReader.readFile(file.getInputStream(), columns, new Institute(), Institute.class);
		
		instituteList = ExcelUtils.parseIgnoringErrors(columnsMap, file.getInputStream(), Institute.class);
	
		
		String json = null;
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
		
		if(instituteList != null && instituteList.size() > 0){
			List<ValidationError> validationList = validateInstitutes(instituteList);

			if(validationList.size() > 0){
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
						"\"records\":"+ow.writeValueAsString(validationList)+"}";
				return json;
			}

			try {
				if(instituteDAO.uploadInstitute(instituteList)){
					instituteList = instituteDAO.getAll();
					json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", " +
							"\"records\":"+ow.writeValueAsString(instituteList)+"}";
				}
				else {
					json = "{\"status\":\"fail\", \"message\":\"Problem while uploading following records\"}";
				}
			} catch (UserAllreadyExistException e) {
				instituteList = instituteDAO.getAllreadyExistInstituteAdminList();
				json = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\", " +
						"\"records\":"+ow.writeValueAsString(instituteList)+"}";
				logger.error("", e);
			} catch (InstituteAllreadyExistException e) {
				instituteList = instituteDAO.getAllreadyExistInstituteList();
				json = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\", " +
						"\"records\":"+ow.writeValueAsString(instituteList)+"}";
				logger.error("", e);
			}
			
		}
		else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading. File might be improper.\", " +
						"\"records\":[]}";
		}
		
		return json;
	}


	private List<ValidationError> validateInstitutes(List<Institute> instituteList) {

		int row = 1;
		List<ValidationError> validationList = new ArrayList<>();
		instituteValidator.setUpload(true);
		instituteValidator.setInstituteList(instituteList);
		int index = 0;
		for(Institute institute : instituteList){
			row ++;
			instituteValidator.setIndex(index);
			ValidationError validationError = ValidatorUtil.validate(institute, instituteValidator);
			if(validationError != null){
				validationError.setRow("ROW : "+row);
				validationError.setTitle(!StringUtils.isEmpty(institute.getInstituteName()) 
						? institute.getInstituteName() : "Not Available");

				validationList.add(validationError);	
			}
			index ++;
		}
		return validationList;
	}


	/**
	 * This method is used to get list of institutes.
	 * @return List<Institutes>
	 * @throws SQLException 
	 */
	 @RequestMapping(method = RequestMethod.GET)
	 @ResponseBody
	 public List<Institute> getInstitutes(HttpSession session, HttpServletResponse response) throws Exception{
		 
		if(Constants.ROLE_ID_BANK_PARTNER.equals(session.getAttribute("roleId")))
			return instituteDAO.getAll();
		
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			throw new UnauthorizedException("Only super admin can do this..");
		}
		return instituteDAO.getAll();
	 }

	 /**
	  * This method is called when we add new institute or edit institute. Based on the institute Id it will decide whether to add or edit a institute. 
	  * @param institute
	 * @throws Exception 
	  */
	 @RequestMapping(value = "/save" ,method = RequestMethod.POST)
	 @ResponseBody
	public String saveInstitute(HttpServletResponse response , HttpSession session, 
			@RequestParam("data") String data, 
			@RequestParam(value = "file",required = false) MultipartFile file) throws Exception {

	 	 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
	 		throw new UnauthorizedException("Only super admin can do this..");
		 }
		 //User user = (User)session.getAttribute("currentUser");
		 String result = null;
		 ObjectMapper mapper = new ObjectMapper();
		 
		 try {
			 Institute institute = mapper.readValue(data, Institute.class);

			 // Validate before insert or update.
			 instituteValidator.setUpload(false);
			 ValidationError error = ValidatorUtil.validate(institute, instituteValidator);
			 if(error != null){
				error.setTitle("Invalid institute details.");

				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
					"\"message\":\"Invalida Data provided\", " +
					"\"records\" : "+mapper.writeValueAsString(error)+"}";
				return result;
			 }
			 System.out.println("Institute Id " + institute.getId());

			 if (institute.getId() == null) {
				if(instituteDAO.insertInstitute(institute, file)){
					Integer lastInsertedId = null;
					try { 
						lastInsertedId = instituteDAO.getInstituteIdByName(institute.getInstituteName());
						instituteDAO.saveInstituteExpressSession(lastInsertedId, (Integer)session.getAttribute("userId"));
					}catch(Exception e){
						e.printStackTrace();
					}
					result = "{\"status\":\"success\", \"message\":\"Institute has been created. You can proceed to create branch\", \"lastInsertedId\" : "+lastInsertedId+"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while creating institute\"}";
					}
				}	
				else {
					if(instituteDAO.updateInstitute(institute, file)){
						result = "{\"status\":\"success\", \"message\":\"Institute updated successfully\", \"lastInsertedId\" : "+institute.getId()+"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\":\"Problem while updating institute\"}";
				}
			}
		}
	 	catch (ProfileImageUploadException e) {
	 		result = "{\"status\":\"fail\", \"message\" : \""+e.getMessage()+"\"}";
	 		logger.error("", e);
		} 
		catch (InstituteAllreadyExistException e) {
			result = "{\"status\":\"fail\", \"message\":\""+e.getMessage()+"\"}";
			logger.error("", e);
		}
		return result;
	}


	/**
	  * This method is used to get a single institute.
	  * @param id
	  * @return Institutes
	 * @throws Exception 
	  */
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Institute getInstitute(HttpSession session, HttpServletResponse response, 
			 @PathVariable Integer id) throws Exception{

		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 throw new UnauthorizedException("Only super admin can do this..");
		 }
		 Institute institute = instituteDAO.getInstituteById(id);
		 
		 return institute;
	 }

	 /**
	  * This method is used to delete a institute.
	  * @param id
	 * @throws Exception 
	  */
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteInstitute(HttpSession session, HttpServletResponse response, 
			@RequestParam Integer id) throws Exception {

		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			throw new UnauthorizedException("Only super admin can do this..");
		}
		String result = "";
		if(instituteDAO.deleteInstitute(id)){
			result = "{\"status\" : \"success\", \"message\" : \"Institute deleted successfully.\" }";
		}
		else {
			result = "{\"status\" : \"fail\", \"message\" : \"Failed to delete institute\" }";
		}
		return result;
	}
	 
	@RequestMapping(value="/expressSessionInstitutes", method=RequestMethod.GET)
	public List<Institute> getExpressSessionInstitutes(HttpSession session) throws UnauthorizedException{
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			throw new UnauthorizedException("Only super admin can do this..");
		}
		return instituteDAO.getSessionInstitutes((Integer) session.getAttribute("userId"));
	}
	
	@RequestMapping(value="/expressSessionInstituteDetail/{id}", method=RequestMethod.GET)
	public Institute getExpressSessionInstituteDetail(@PathVariable("id") Integer id, HttpSession session) throws UnauthorizedException{
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			throw new UnauthorizedException("Only super admin can do this..");
		}
		return instituteDAO.getInstituteById(id);
	}
}