package com.qfix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.dao.SchoolDao;
import com.qfix.model.SchoolBoard;

@RestController
@RequestMapping(value = "/school")
public class SchoolController {
	
	@Autowired
	private SchoolDao schoolDao;
	
	@RequestMapping(value = "/boards")
	public List<SchoolBoard> listBoards(){
		return schoolDao.listBoards(); 
	}

}
