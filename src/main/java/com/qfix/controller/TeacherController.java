package com.qfix.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.TeacherInterfaceDao;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.BranchForumNotExistException;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.StudentExcelReportFilter;
import com.qfix.model.Teacher;
import com.qfix.model.TeacherStandardSubject;
import com.qfix.model.ValidationError;
import com.qfix.sep4j.ExcelUtils;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ReadWriteExcelUtil;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.TeacherStandardSubjectValidator;
import com.qfix.validator.TeacherValidator;

@Controller
@RequestMapping(value = "/teachers")
public class TeacherController extends BaseController {

	final static Logger logger = Logger.getLogger(TeacherController.class);

	@Autowired
	TeacherInterfaceDao teacherDAO;

	@Autowired
	TeacherValidator teacherValidator;

	@Autowired
	TeacherStandardSubjectValidator teacherStandardSubjectValidator;

	/**
	 * This method is used to upload file and show the results on the screen.
	 * 
	 * @param file
	 * @return List<Teacher>
	 * @throws IOException
	 * @throws UnauthorizedException
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session,
			@RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws IOException,
			UnauthorizedException {

		Map<String, String> columnsMap = new HashMap<String, String>();

		columnsMap.put(UploadConstants.TEACHER_FIRST_NAME_LABEL + UploadConstants.STAR_LABEL, "firstName");
		columnsMap.put(UploadConstants.TEACHER_MIDDLE_NAME_LABEL, "middleName");
		columnsMap.put(UploadConstants.TEACHER_LAST_NAME_LABEL + UploadConstants.STAR_LABEL, "lastName");
		columnsMap.put(UploadConstants.TEACHER_BIRTH_DATE_LABEL + UploadConstants.DATE_FORMAT_LABEL , "dateOfBirth");
		columnsMap.put(UploadConstants.TEACHER_EMAIL_ADDRESS_LABEL + UploadConstants.STAR_LABEL, "emailAddress");
		columnsMap.put(UploadConstants.TEACHER_GENDER_LABEL + UploadConstants.STAR_LABEL, "gender");
		columnsMap.put(UploadConstants.TEACHER_PRIMARY_NUMBER_LABEL + UploadConstants.STAR_LABEL, "primaryContact");
		columnsMap.put(UploadConstants.TEACHER_SECONDARY_NUMBER_LABEL, "secondaryContact");
		columnsMap.put(UploadConstants.TEACHER_ADDRESS_LINE1_LABEL , "addressLine");
		columnsMap.put(UploadConstants.TEACHER_ADDRESS_AREA_LABEL, "area");
		columnsMap.put(UploadConstants.STATE_LABEL , "state");
		columnsMap.put(UploadConstants.DISTRICT_LABEL , "district");
		columnsMap.put(UploadConstants.TALUKA_CITY_LABEL , "taluka");
		columnsMap.put(UploadConstants.TEACHER_TOWN_VILLAGE_LABEL , "city");
		columnsMap.put(UploadConstants.TEACHER_PINCODE_LABEL , "pinCode");
		columnsMap.put(UploadConstants.TEACHER_ACTIVE_LABEL , "active");

		List<Teacher> teacherList = null;
		teacherList = ExcelUtils.parseIgnoringErrors(columnsMap,
				file.getInputStream(), Teacher.class);

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = null;
		if (teacherList != null && teacherList.size() > 0) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.readTree(data);
			Integer branchId = ((node != null && node.get("branchId") != null) ? node
					.get("branchId").asInt() : null);
			checkUserBranch(branchId, session);
			List<ValidationError> validationList = validateAndPrepareTeachers(
					teacherList, branchId);

			if (validationList.size() <= 0) {
				try {
					if (branchId != null && branchId != 0) {

						if (teacherDAO.uploadTeacher(teacherList, branchId)) {
							StudentExcelReportFilter filter = new StudentExcelReportFilter();
							filter.setBranchId(branchId);
							teacherList = teacherDAO.getTeacherReport(filter);
							json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", "
									+ "\"records\":"
									+ ow.writeValueAsString(teacherList) + "}";
						} else {
							teacherList = teacherDAO.getUploadingFailedRecords();
							json = "{\"status\":\"fail\", \"message\":\"Problem while uploading following records\", "
									+ "\"records\":" + ow.writeValueAsString(teacherList) + "}";
						}
					} else {
						json = "{\"status\":\"fail\", \"message\" : \"Please select branch to upload teachers.\", "
								+ "\"records\" : []}";
					}
				} catch (Exception e) {
					logger.error("", e);
				}
			} else {
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", "
						+ "\"records\":"
						+ ow.writeValueAsString(validationList) + "}";
			}
		} else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", "
					+ "\"records\":[]}";
		}

		return json;
	}

	private List<ValidationError> validateAndPrepareTeachers(List<Teacher> teacherList, Integer branchId) {
		int row = 1;
		List<ValidationError> validationList = new ArrayList<>();
		teacherValidator.setUpload(true);
		teacherValidator.setTeacherList(teacherList);
		int index = 0;
		for (Teacher teacher : teacherList) {
			row++;
			if (StringUtils.isEmpty(teacher.getFirstName())
					&& StringUtils.isEmpty(teacher.getLastName())
					&& StringUtils.isEmpty(teacher.getEmailAddress())) {
				ValidationError validationError = new ValidationError();
				validationError.setRow("ROW : " + row);
				String title = !StringUtils.isEmpty(teacher.getFirstName())
						&& !StringUtils.isEmpty(teacher.getLastName()) ? (!StringUtils
						.isEmpty(teacher.getFirstName()) ? teacher
						.getFirstName() + " " : "") + (!StringUtils.isEmpty(teacher.getLastName()) ? 
								teacher.getLastName() : "")
						: "Teacher data is not Available";

				validationError.setTitle(title);
				validationList.add(validationError);

			} else {
				if (teacher.getState() != null) {
					teacher.setState(teacher.getState().replace("_", " "));
				}
				if (teacher.getDistrict() != null) {
					teacher.setDistrict(teacher.getDistrict().replace("_", " "));
				}

				if (teacher.getTaluka() != null) {
					teacher.setTaluka(teacher.getTaluka().replace("_", " "));
				}

				teacher.setBranchId(branchId);
				teacherValidator.setIndex(index);

				ValidationError validationError = ValidatorUtil.validateForAllErrors(teacher, teacherValidator);

				if (validationError != null) {
					validationError.setRow("ROW : " + row);
					String title = !StringUtils.isEmpty(teacher.getFirstName())
							&& !StringUtils.isEmpty(teacher.getLastName()) ? (StringUtils
							.isEmpty(teacher.getFirstName()) ? teacher
							.getFirstName() : "")
							+ (!StringUtils.isEmpty(teacher.getLastName()) ? teacher
									.getLastName() : "")
							: "Not Available";

					validationError.setTitle(title);
					validationList.add(validationError);
				}
				index++;
			}
		}
		return validationList;
	}

	/**
	 * This method is used to upload file and show the results on the screen.
	 * 
	 * @param file
	 * @return List<Teacher>
	 * @throws IOException
	 * @throws UnauthorizedException
	 */
	@RequestMapping(value = "/uploadTeacherSubjectFile", method = RequestMethod.POST)
	@ResponseBody
	public String uploadTeacherSubjectFile(HttpSession session,
			@RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws IOException,
			UnauthorizedException {

		Map<String, String> columnsMap = new HashMap<String, String>();

		columnsMap.put(UploadConstants.TEACHER_NAME_LABEL
				+ UploadConstants.STAR_LABEL, "teacher");
		columnsMap.put(UploadConstants.STANDARD_LABEL
				+ UploadConstants.STAR_LABEL, "standard");
		columnsMap.put(UploadConstants.DIVISION_LABEL, "division");
		columnsMap.put(UploadConstants.SUBJECT_NAME_LABEL, "subject");

		List<TeacherStandardSubject> teacherStandardSubjectList = null;
		List<Teacher> teacherList = null;
		teacherStandardSubjectList = ExcelUtils.parseIgnoringErrors(columnsMap,
				file.getInputStream(), TeacherStandardSubject.class);
		System.out.println("Teacher standard subject list ::"
				+ teacherStandardSubjectList);

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = null;
		if (teacherStandardSubjectList != null
				&& teacherStandardSubjectList.size() > 0) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.readTree(data);
			Integer branchId = ((node != null && node.get("branchId") != null) ? node
					.get("branchId").asInt() : null);

			checkUserBranch(branchId, session);

			List<ValidationError> validationList = validateAndPrepareTeacherSubjects(
					teacherStandardSubjectList, branchId);

			if (validationList.size() <= 0) {
				try {
					if (branchId != null && branchId != 0) {

						if (teacherDAO.uploadTeacherStandardSubject(
								teacherStandardSubjectList, branchId)) {
							StudentExcelReportFilter filter = new StudentExcelReportFilter();
							filter.setBranchId(branchId);
							teacherList = teacherDAO.getTeacherReport(filter);
							json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", "
									+ "\"records\":"
									+ ow.writeValueAsString(teacherList) + "}";
						} else {
							teacherList = teacherDAO
									.getUploadingFailedRecords();
							json = "{\"status\":\"fail\", \"message\":\"Problem while uploading following records\", "
									+ "\"records\":"
									+ ow.writeValueAsString(teacherList) + "}";
						}
					} else {
						json = "{\"status\":\"fail\", \"message\" : \"Please select branch to upload teachers.\", "
								+ "\"records\" : []}";
					}
				} catch (Exception e) {
					logger.error("", e);
				}
			} else {
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", "
						+ "\"records\":"
						+ ow.writeValueAsString(validationList) + "}";
			}
		} else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", "
					+ "\"records\":[]}";
		}

		return json;
	}

	private List<ValidationError> validateAndPrepareTeacherSubjects(
			List<TeacherStandardSubject> teacherStandardSubjectList,
			Integer branchId) {
		int row = 1;
		String standard = "";
		String division = "";
		String subject = "";

		teacherStandardSubjectValidator
				.setTeacherStandardSubjectList(teacherStandardSubjectList);
		int index = 0;
		List<ValidationError> validationList = new ArrayList<>();
		for (TeacherStandardSubject teacherStandardSubject : teacherStandardSubjectList) {

			row++;
			if (StringUtils.isEmpty(teacherStandardSubject.getTeacher())
					|| StringUtils
							.isEmpty(teacherStandardSubject.getStandard())) {
				ValidationError validationError = new ValidationError();
				validationError.setRow("ROW : " + row);
				String title = !StringUtils.isEmpty(teacherStandardSubject
						.getTeacher()) ? teacherStandardSubject.getTeacher()
						: " Teacher data is not Available";

				validationError.setTitle(title);
				validationList.add(validationError);
				List<String> errors = new ArrayList<>();
				errors.add("Teacher and standard is mandatory.");
				validationError.setErrors(errors);
			} else {
				if (!StringUtils.isEmpty(teacherStandardSubject.getStandard())) {
					standard = (teacherStandardSubject.getStandard().replace(
							"__", ""));
					teacherStandardSubject.setStandard(standard.replace("_",
							" "));
				}
				if (!StringUtils.isEmpty(teacherStandardSubject.getDivision())) {
					division = teacherStandardSubject.getDivision().replace(
							"__", "");
					teacherStandardSubject.setDivision(division.replace("_",
							" "));
				}
				if (!StringUtils.isEmpty(teacherStandardSubject.getSubject())) {
					subject = teacherStandardSubject.getSubject().replace("__",
							"");
					teacherStandardSubject
							.setSubject(subject.replace("_", " "));
				}

				teacherStandardSubject.setBranchId(branchId);
				teacherStandardSubjectValidator.setIndex(index);

				ValidationError validationError = ValidatorUtil
						.validateForAllErrors(teacherStandardSubject,
								teacherStandardSubjectValidator);

				if (validationError != null) {
					validationError.setRow("ROW : " + row);
					String title = !StringUtils.isEmpty(teacherStandardSubject
							.getTeacher()) ? teacherStandardSubject
							.getTeacher() : "Not Available";

					validationError.setTitle(title);
					validationList.add(validationError);
				}
				index++;
			}
		}
		return validationList;
	}

	@RequestMapping(value = "/getTeacherByBranch", method = RequestMethod.POST)
	@ResponseBody
	public List<Teacher> getTeacherByBranch(HttpSession session,
			@RequestBody StudentExcelReportFilter filter)
			throws UnauthorizedException {

		logger.debug("getTeacherByBranch request params -- " + filter);
		checkUserBranch(filter.getBranchId(), session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			if (filter.getStandardId() != null) {
				validateStandard(filter.getBranchId(), filter.getStandardId());
				if (filter.getDivisionId() != null) {
					validateDivision(filter.getBranchId(),
							filter.getStandardId(), filter.getDivisionId());
				}
			}
		}
		return teacherDAO.getTeacherReport(filter);
	}

	/**
	 * This method is called when we add new Teacher or edit Teacher. Based on
	 * the Teacher Id it will decide whether to add or edit a Teacher.
	 * 
	 * @param Teacher
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveTeachers(HttpServletRequest request, HttpSession session,
			@RequestParam("data") String data,
			@RequestParam(value = "file", required = false) MultipartFile file) {

		boolean flag = false;
		String result = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			Teacher teacher = mapper.readValue(data, Teacher.class);

			checkUserBranch(teacher.getBranchId(), session);
			if (teacher.getId() != null) {
				if (!Constants.ROLE_ID_ADMIN.equals(session
						.getAttribute("roleId"))) {
					validateTeacher(teacher.getBranchId(), teacher.getId());
				}
			}

			teacherValidator.setUpload(false);
			ValidationError error = ValidatorUtil.validateForAllErrors(teacher,
					teacherValidator);
			if (error != null) {
				String title = !StringUtils.isEmpty(teacher.getFirstName())
						&& !StringUtils.isEmpty(teacher.getLastName()) ? (!StringUtils
						.isEmpty(teacher.getFirstName()) ? teacher
						.getFirstName() + " " : "")
						+ (!StringUtils.isEmpty(teacher.getLastName()) ? teacher
								.getLastName() : "")
						: "Not Available";

				error.setTitle(title);
				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", "
						+ "\"message\":\"Invalida Data provided\", "
						+ "\"records\" : "
						+ mapper.writeValueAsString(error)
						+ "}";
				return result;
			}
			if (teacher.getId() != null) {

				flag = teacherDAO.updateTeacher(teacher, file);
				if (flag) {
					result = "{\"status\":\"success\", \"message\" : \"Teacher updated successfully.\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating teacher.\"}";
				}
			} else {
				flag = teacherDAO.insertTeacher(teacher, file);
				if (flag) {
					result = "{\"status\":\"success\", \"message\" : \"Teacher added successfully.\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding teacher.\"}";
				}
			}
		} catch (BranchForumNotExistException e) {
			result = "{\"status\":\"fail\", \"message\" : \"" + e.getMessage()
					+ "\"}";
			logger.error(result, e);
		} catch (Exception e) {
			logger.error("Failed to insert..", e);
		}
		return result;
	}

	/**
	 * This method is called when we add new Teacher or edit Teacher. Based on
	 * the Teacher Id it will decide whether to add or edit a Teacher.
	 * 
	 * @param Teacher
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	@ResponseBody
	public String updateProfile(HttpServletRequest request,
			HttpSession session, @RequestParam("data") String data,
			@RequestParam(value = "file", required = false) MultipartFile file)
			throws Exception {
		// add this method updateProfile(HttpServletRequest request,HttpSession
		// session,@RequestBody Teacher teacher)

		boolean flag = false;
		String result = null;
		ObjectMapper mapper = new ObjectMapper();

		Teacher teacher = mapper.readValue(data, Teacher.class);
		// checkUserBranch(teacher.getBranchId(), session);
		if (!session.getAttribute("userId").equals(teacher.getUserId())) {
			logger.error("trying to update others profile..current userId = "
					+ session.getAttribute("userId")
					+ ", And updating profile for -" + teacher.getUserId());
			throw new UnauthorizedException(
					"You dont have permission to update this profile.");
		}

		if (teacher.getId() != null) {
			flag = teacherDAO.updateProfile(teacher, file);
			if (flag) {
				result = "{\"status\":\"success\", \"message\" : \"Teacher updated successfully.\"}";
			} else {
				result = "{\"status\":\"fail\", \"message\" : \"Problem while updating teacher.\"}";
			}
		}
		return result;
	}

	/**
	 * This method is used to get a single teacher.
	 * 
	 * @param id
	 * @return teacher
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Teacher getTeacher(HttpSession session, @PathVariable Integer id)
			throws UnauthorizedException {
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateTeacher(
					(List<Integer>) session.getAttribute("branchIdList"), id);
		}
		return teacherDAO.getTeacherById(id);
	}

	/**
	 * This method is used to delete a teacher.
	 * 
	 * @param id
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteTeachers(HttpSession session, @RequestParam Integer id)
			throws UnauthorizedException {
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateTeacher(
					(List<Integer>) session.getAttribute("branchIdList"), id);
		}
		boolean flag = false;
		String result = null;
		flag = teacherDAO.deleteTeacher(id);
		if (flag == true) {
			result = "{\"status\":\"success\", \"message\" : \"Teacher deleted successfully.\"}";
		} else {
			result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting teacher.\"}";
		}
		return result;
	}

	@RequestMapping(value = "/updateProfileFirstTime", method = RequestMethod.POST)
	@ResponseBody
	public String updateProfile(HttpServletRequest request,
			HttpSession session, @RequestBody Teacher teacher)
			throws Exception, Exception {

		String result = "";
		if (!session.getAttribute("userId").equals(teacher.getUserId())) {
			logger.error("trying to update others profile..current userId = "
					+ session.getAttribute("userId")
					+ ", And updating profile for -" + teacher.getUserId());
			throw new UnauthorizedException(
					"You dont have permission to update this profile.");
		}

		if (teacher.getUserId() != null && teacher.getRoleId() == 4) {
			Integer userId=teacher.getUserId();
			Integer roleId=teacher.getRoleId();
			Integer id = teacherDAO.getIdByUserId(teacher.getUserId());
			if (id != null) {
				teacher.setId(id);
				boolean flag = teacherDAO.updateProfileFirstTime(teacher);
				if (flag) {
					result = "{\"status\":\"success\", \"message\" : \"profile updated successfully.\"}";
				} else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating profile.\"}";
				}
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/exportTeacherData",method = RequestMethod.GET)
	 @ResponseBody
	 public void exportTeacherData(HttpSession session, HttpServletResponse response, @NotNull @NotEmpty @RequestParam("branchId") Integer branchId,
			 @RequestParam(value = "standardId", required = false) Integer standardId,
			 @RequestParam(value = "divisionId", required = false) Integer divisionId) throws Exception{

		 ObjectMapper mapper = new ObjectMapper();
		 checkUserBranch(branchId, session);
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)|| session.getAttribute("roleId").equals(Constants.ROLE_ID_SCHOOL_ADMIN)){
			 if(standardId!=null){
				 validateStandard(branchId, standardId);
				 if(divisionId!=null){
					 validateDivision(branchId, standardId , divisionId);
				 }
			 }
			 /*validateStandard(branchId, standardId);
			 validateDivision(branchId, standardId , divisionId);*/
		 }
		 List<Teacher> teachers = teacherDAO.exportTeacherData(branchId, standardId, divisionId);
		 TreeMap<Integer, Object[]> dataMap = generateTeachersExcelData(teachers);
		 Workbook workbook = ReadWriteExcelUtil.writeDataToExcel(dataMap, 1, "TeacherLoginDetails.xlsx");
		 response.setContentType("application/vnd.ms-excel");
		 response.setHeader("Content-Disposition", "attachment; filename=Teacher-Login-Details.xlsx");
		 ServletOutputStream outStream = response.getOutputStream();
		 workbook.write(outStream); // Write workbook to response.
		 outStream.close();
	}
	 

	 private TreeMap<Integer, Object[]> generateTeachersExcelData(
				List<Teacher> teachers) {
		
		TreeMap<Integer, Object[]> excelDataMap = new TreeMap<Integer, Object[]>();
		excelDataMap.put(2, new Object[]{"Teacher report for - " + Constants.USER_DATE_FORMAT.format(new Date())});
		if(teachers != null && teachers.size() > 0){
			excelDataMap.put(3, new Object[]{"Branch - " + teachers.get(0).getBranch()});
		}

		int rowNum = 5;
		Object[] headerArr =  new Object[]{
			"First Name",
			"Surname",
			"Mobile",
			"Email",
			"Username",
			"Password"
		};
		excelDataMap.put(rowNum, headerArr);

		for(Teacher teacher : teachers){
			rowNum ++;
			Object[] dataArr = {
				teacher.getFirstName(), 
				teacher.getLastName(),
				teacher.getPrimaryContact(),
				teacher.getEmailAddress(),
				teacher.getUsername(),
				teacher.getPassword()
			};
			excelDataMap.put(rowNum, dataArr);
		}
		return excelDataMap;
	}
}

