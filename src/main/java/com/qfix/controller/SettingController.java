package com.qfix.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qfix.dao.ISettingsDao;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.EmailSetting;
import com.qfix.model.SMSSetting;
import com.qfix.utilities.Constants;

@Controller
@RequestMapping(value = "/settings")
public class SettingController {
	
	@Autowired
	ISettingsDao settingsDao;

	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	@ResponseBody
	public void sendEmail(@RequestParam(value = "email") String email, @RequestParam(value = "email") String subject, 
			@RequestParam(value = "email") String messageBody) {
		settingsDao.sendEmail(email, subject, messageBody);
	}

/*	@RequestMapping(value = "/sendSms", method = RequestMethod.POST)
	@ResponseBody
	public void sendSms(@RequestParam(value = "contactNumber") String contactNumber, 
			@RequestParam(value = "message") String message) {
		SettingDao settingDao = new SettingDao();
		settingDao.sendSMS(contactNumber, message);
	}
*/

	@RequestMapping(value = "/updateSMSSetting", method = RequestMethod.POST)
	@ResponseBody
	public String changeSMSSetting(HttpSession session, @RequestBody SMSSetting smsSetting) throws UnauthorizedException {
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			throw new UnauthorizedException("Only super admin can do this..");
		}
		String result = "";
		if(settingsDao.updateSmsSetting(smsSetting)){
			result = "{\"status\" : \"success\", \"message\" : \"SMS Settings changed.\"}";
		}
		else {
			result = "{\"status\" : \"fail\", \"message\" : \"Problem while changing SMS settings.\"}";
		}
		return result;
	}

	@RequestMapping(value = "/updateEmailSetting", method = RequestMethod.POST)
	@ResponseBody
	public String changeEmailSetting(HttpSession session, @RequestBody EmailSetting emailSetting) throws UnauthorizedException {
		if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			throw new UnauthorizedException("Only super admin can do this..");
		}

		String result = "";
		if(settingsDao.updateEmailSetting(emailSetting)){
			result = "{\"status\" : \"success\", \"message\" : \"Email Settings changed.\"}";
		}
		else {
			result = "{\"status\" : \"fail\", \"message\" : \"Problem while changing Email settings.\"}";
		}
		return result;
	}
	
	 /**
	  * This method is used to get a single student.
	  * @param id
	  * @return student
	  */
	 @RequestMapping(value = "/getEmailSetting",method = RequestMethod.GET)
	 @ResponseBody
	 public EmailSetting getEmailSetting() throws IOException{
		 EmailSetting emailSetting = settingsDao.getEmailSetting();
		 return emailSetting;
	 }
	
	 /**
	  * This method is used to get a single student.
	  * @param id
	  * @return student
	  */
	 @RequestMapping(value = "/getSmsSetting",method = RequestMethod.GET)
	 @ResponseBody
	 public SMSSetting getSmsSetting() throws IOException{
		 SMSSetting smsSetting = settingsDao.getSmsSetting();
		 return smsSetting;
	 }
	
	/*public static void sendEmail(String subject, String messageBody,
			String email) {

		final String username = "diliphajare10@gmail.com";
		final String password = "";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(email));
			message.setSubject("From : [" + username + "] : " + subject);
			message.setText(messageBody);

			Transport.send(message);

			

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	@RequestMapping(value = "/sendSMS", method = RequestMethod.GET)
	@ResponseBody
	public String sendSMS(
						 * String user, String pwd, String contactNumber, String
						 * msg, String sid, String fl
						 ) {
		String user = "Prachi";
		String pwd = "P123";
		String contactNumber = "8097137625";
		String msg = "Hi Dilip Hajare...";
		String sid = "WEBSMS";
		String fl = "0";

		String rsp = "";
		try {
			// Construct The Post Data
			String data = URLEncoder.encode("user", "UTF-8") + "="
					+ URLEncoder.encode(user, "UTF-8");
			data += "&" + URLEncoder.encode("password", "UTF-8") + "="
					+ URLEncoder.encode(pwd, "UTF-8");
			data += "&" + URLEncoder.encode("msisdn", "UTF-8") + "="
					+ URLEncoder.encode(contactNumber, "UTF-8");
			data += "&" + URLEncoder.encode("msg", "UTF-8") + "="
					+ URLEncoder.encode(msg, "UTF-8");
			data += "&" + URLEncoder.encode("sid", "UTF-8") + "="
					+ URLEncoder.encode(sid, "UTF-8");
			data += "&" + URLEncoder.encode("fl", "UTF-8") + "="
					+ URLEncoder.encode(fl, "UTF-8");
			// Push the HTTP Request
			URL url = new URL(
					"http://cloud.smsindiahub.in/vendorsms/pushsms.aspx?");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			wr.write(data);
			
			wr.flush();
			// Read The Response
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				// Process line...
				rsp += line;
			}
			wr.close();
			rd.close();
			

		} catch (Exception e) {
			rsp = e.toString();
		}
		
		return rsp;
	}*/
}
