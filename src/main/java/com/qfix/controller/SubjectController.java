package com.qfix.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import com.qfix.sep4j.ExcelUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.ISubjectDao;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Subject;
import com.qfix.model.ValidationError;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.SubjectValidator;

@RestController
@RequestMapping(value = "/subject")
public class SubjectController extends BaseController{
	final static Logger logger = Logger.getLogger(SubjectController.class);
	@Autowired
	ISubjectDao subjectDAO;

	@Autowired
	SubjectValidator subjectValidator;

	@RequestMapping(value = "/uploadSubjectFile" ,method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(HttpSession session, @RequestParam("data") String data,
			@RequestParam("file") MultipartFile file) throws Exception{
		List<Subject> subjectList = null;

		Map<String, String> columnsMap = new HashMap<String, String>();

		columnsMap.put(UploadConstants.SUBJECT_TITLE_LABEL+UploadConstants.STAR_LABEL, "name");
		columnsMap.put(UploadConstants.SUBJECT_CODE_LABEL, "code");
		columnsMap.put(UploadConstants.SUBJECT_THEORY_MARKS_LABEL, "theoryMarks");
		columnsMap.put(UploadConstants.SUBJECT_PRACTICAL_MARKS_LABEL, "practicalMarks");	
		
		
		//subjectList = CSVFileReader.readFile(file.getInputStream(), columns, new Subject(), Subject.class);
		subjectList = ExcelUtils.parseIgnoringErrors(columnsMap, file.getInputStream(), Subject.class);
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		Integer branchId = ((node != null && node.get("branchId") != null) ? node.get("branchId").asInt()  : null);
		checkUserBranch(branchId, session);
		if(subjectList != null && subjectList.size() > 0){ 
			List<ValidationError> validationList = validateSubjectList(subjectList, branchId);
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

			if(validationList.size() > 0){
				json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
						"\"records\":"+ow.writeValueAsString(validationList)+"}";
				return json;
			}
			try {
				if(subjectDAO.uploadSubject(subjectList,branchId)){
					subjectList = subjectDAO.getSubjectsByBranchId(branchId);
					json = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", " +
							"\"records\":"+ow.writeValueAsString(subjectList)+"}";
				}
			} catch (Exception e) {
				json = "{\"status\":\"fail\", \"message\":\"Failed to upload.\"}";
				logger.error("", e);
			}
		}
		else {
			json = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", " +
						"\"records\":[]}";
		}

		return json;
	}


	private List<ValidationError> validateSubjectList(List<Subject> subjectList, Integer branchId) {

		int row = 1;
		List<ValidationError> validationList = new ArrayList<>();
		subjectValidator.setUpload(true);
		subjectValidator.setSubjectList(subjectList);
		int index = 0;
		for(Subject subject : subjectList){
			row ++;
			subject.setBranchId(branchId);
			// TODO Set categoryId default as 0
			subject.setCategoryId(1);
			subjectValidator.setIndex(index);
			ValidationError validationError = ValidatorUtil.validate(subject, subjectValidator);
			if(validationError != null){
				validationError.setRow("ROW : "+row);
				validationError.setTitle(subject.getName());
				validationList.add(validationError);
			}
			index ++;
		}
		return validationList;
	}

	 /**
	  * 
	  * @param session
	  * @param instituteId
	  * @param branchId
	  * @return
	  */
	 @RequestMapping(value = "/getSubjectByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Subject> getStudentsByInstituteAndBranch(HttpSession session,  
			 @RequestParam(value = "branchId") Integer branchId) throws Exception{
		 checkUserBranch(branchId, session);
		 return subjectDAO.getSubjectsByBranchId(branchId);
	 }


	/**
	  * This method is called when we add new Subject or edit Subject. Based on the Fee Id it will decide whether to add or edit a Fee. 
	  * @param Subject
	  */
	 @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	 @ResponseBody 
	public String saveSubject(@RequestBody Subject subject,HttpSession session) {
		 boolean flag = false;
		String result = null;
		//subject.setBranchId((user.getRoleId() == 6)? user.getBranchId()  : subject.getBranchId());
		 try {
			 
			 checkUserBranch(subject.getBranchId(), session);
			 
			subjectValidator.setUpload(false);
			ValidationError error = ValidatorUtil.validate(subject, subjectValidator);
			ObjectMapper mapper = new ObjectMapper();
			if(error != null){
				String title = "Invalid subject details.";
				error.setTitle(title);
				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
					"\"message\":\"Invalida Data provided\", " +
					"\"records\" : "+mapper.writeValueAsString(error)+"}";
				return result;
			}
			subject.setCategoryId(1);
			if(subject.getId() != null){
				
				if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
					 validateSubject((List<Integer>)session.getAttribute("branchIdList"), subject.getId());
				}
				
				flag = subjectDAO.updateSubject(subject);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Subject updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating subject.\"}";
				}
			}
			else {
				flag = subjectDAO.insertSubject(subject);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Subject added successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while adding subject.\"}";
				}
			}
		} catch (Exception e) {
			if(subject.getId() != null){
				result = "{\"status\":\"fail\", \"message\" : \"Problem while updating subject.\"}";
			}
			else {
				result = "{\"status\":\"fail\", \"message\" : \"Problem while adding subject.\"}";
			}
			logger.error("", e);
		}

		return result;
	}
	
	 /**
	  * This method is used to get a single fee.
	  * @param id
	  * @return fee
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Subject getFee(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateSubject((List<Integer>)session.getAttribute("branchIdList"), id);
		}
		 return subjectDAO.getSubjectById(id);
	 }
	
	 /**
	  * This method is used to delete a fee.
	  * @param id
	 * @throws UnauthorizedException 
	  */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	public String deleteSubject(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 
		 if(!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))){
			 validateSubject((List<Integer>)session.getAttribute("branchIdList"), id);
		}
		 
		 boolean flag = false;
		 String result = null;
		 flag =	subjectDAO.deleteSubject(id);
		 if(flag == true){
				result = "{\"status\":\"success\", \"message\" : \"Subject deleted successfully.\"}";
			}
			else{
				result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting subject.\"}";
			}
			return result;
	}
}
