package com.qfix.controller;

import java.util.Calendar;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.qfix.mq.FileDataInsertDao;
import com.qfix.model.FileUploadJob;
import com.qfix.mq.MessageProducer;
import com.qfix.service.file.FileUploadService;
import com.qfix.utilities.BranchUtils;
import com.qfix.utilities.Constants;

@Slf4j
@RestController
@RequestMapping(value = "/fileUpload")
@Scope(value = "session")
public class FileUploadController {
	
	@Autowired
	MessageProducer producer;

	@Autowired
	BranchUtils branchUtils;
	
	@Autowired
	FileDataInsertDao fileDataInsertDao;

	@Autowired
	FileUploadService fileUploadService;

	@SuppressWarnings("unused")
	@RequestMapping(value="/student")
	public String studentFileUpload(HttpServletRequest request, HttpSession session,
							@RequestParam("data") String data, @RequestParam("file") MultipartFile file) throws Exception{
		
		String jsonData = "";
		Integer branchId = fileUploadService.getBranchId(data);
		FileUploadJob job = fileUploadService.getFileUploadJobByBranchId(branchId);
		if(job != null){
			jsonData = "{\"status\":\"success\", \"message\" : \"File upload for this branch is already in Progress\", "+ "\"records\" : []}";
			return jsonData;
		}
		branchUtils.checkUserBranch(branchId, session);
		int userId = (int) session.getAttribute("userId");
		Integer id = fileUploadService.insertFileMetaData(file, branchId);
		fileDataInsertDao.insertUserId(userId, id);
		log.info("in File Upload Controller branch id : " + branchId + " file meta id : " + id);
		producer.publishMessage(id.toString());
		log.info("Message Published " + Calendar.getInstance().getTime().toString());
		
		
		if(id!=null){
			jsonData = "{\"status\":\"success\", \"message\" : \"File upload in Progress\", "+ "\"records\" : []}";
			return jsonData;
		}
		else{
			jsonData = "{\"status\":\"fail\", \"message\" : \"Problem while upload file.\", "+ "\"records\" : []}";
			log.error("Error while inserting data during upload");
			return jsonData;	
		}
	}
	
	
	
	@RequestMapping(value="/paymentReport")
	public String paymentReportUpload(HttpServletRequest request, HttpSession session, @RequestParam("file") MultipartFile file) throws Exception{
		log.info("in report");
		Integer userId = (Integer) session.getAttribute("userId");
		Integer id = fileUploadService.uploadFile(file, userId);
		String jsonData = "";
		if(id!=null){
			jsonData = "{\"status\":\"success\", \"message\" : \"File uploaded and processed successfully\", "+ "\"records\" : []}";
			return jsonData;
		}
		else{
			jsonData = "{\"status\":\"fail\", \"message\" : \"Problem while file upload.\", "+ "\"records\" : []}";
			log.error("Error while inserting data during upload for payment report");
			return jsonData;	
		}
	}


	@RequestMapping(value="/status", method = RequestMethod.GET)
	public List<FileUploadJob> getStatus(HttpServletRequest request, HttpSession session,
			@RequestParam("branchId") Integer branchId) throws Exception{

		Integer userId = null;
		System.out.println(session.getAttribute("roleId"));
		if(!Constants.ROLE_ID_ADMIN.equals((Integer)session.getAttribute("roleId"))){
			userId = (Integer)session.getAttribute("userId");
		}
		return fileUploadService.getFileUploadJobs(userId, branchId);
	}

	@RequestMapping(value="/error/log", method = RequestMethod.GET)
	public String getLog(HttpServletRequest request, HttpSession session,
			@RequestParam("id") Integer id) throws Exception{
		FileUploadJob job = fileUploadService.getFileUploadJobById(id);
		return job.getInvalidRecordData();
	}

	@RequestMapping(value="/error/download", method = RequestMethod.GET)
	public void downloadErrorFile(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam("id") Integer id) throws Exception{
		 byte[] arr = fileUploadService.getInvalidFile(id);
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		 response.setHeader("Content-Disposition", "attachment; filename=eduqfix-upload-student-template.xlsx");
		 response.getOutputStream().write(arr);

	}
}