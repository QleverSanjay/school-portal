package com.qfix.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.model.MenuGroupItem;
import com.qfix.model.MenuGroups;

@RestController
public class IndexController  extends JdbcDaoSupport {
	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	/*@Transactional
	@RequestMapping(value="/menu/group")
	public LinkedHashMap<String,LinkedList<MenuGroupItem>> getMenuGroups(){
		String query = "SELECT GROUP_CONCAT(menu_name order by group_item_position ASC) AS menuItems, "
				+ " GROUP_CONCAT(state_url order by group_item_position ASC) AS stateUrls, group_name AS groupName, "
				+ " group_tag AS groupTag FROM qfix.menu_items mi "
				+ " JOIN role_menu rm ON (mi.menu_id = rm.menu_id) WHERE role_id = 1 "
				+ " AND group_name IS NOT NULL AND group_tag IS NOT NULL "
				+ " GROUP BY group_tag ORDER BY menu_position ASC; ";
		
		List<MenuGroups> groups = getJdbcTemplate().query(query, new BeanPropertyRowMapper<MenuGroups>(MenuGroups.class));
		LinkedHashMap<String, LinkedList<MenuGroupItem>> groupItems = new LinkedHashMap<String, LinkedList<MenuGroupItem>>();
		
		for(MenuGroups menuGroup:groups){
			List<String> menuItems = Arrays.asList(menuGroup.getMenuItems().split(","));
			List<String> stateUrls = Arrays.asList(menuGroup.getStateUrls().split(","));
			LinkedList<MenuGroupItem> menuGroupItems = new LinkedList<MenuGroupItem>();
			
			for(int i=0;i<menuItems.size();i++){
				String item = menuItems.get(i);
				String stateUrl = stateUrls.get(i);
				
				MenuGroupItem groupItem = new MenuGroupItem();
				groupItem.setGroupTag(menuGroup.getGroupTag());
				groupItem.setGroupName(menuGroup.getGroupName());
				groupItem.setMenuItem(item);
				groupItem.setStateUrl(stateUrl);
				menuGroupItems.add(groupItem);
			}
			
			groupItems.put(menuGroup.getGroupName(),menuGroupItems);
		}
		
		return groupItems;
	}*/
}
