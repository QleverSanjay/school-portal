package com.qfix.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import com.qfix.sep4j.ExcelUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.qfix.dao.IEventDao;
import com.qfix.excel.UploadConstants;
import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.Event;
import com.qfix.model.ValidationError;
import com.qfix.utilities.Constants;
import com.qfix.utilities.ValidatorUtil;
import com.qfix.validator.EventValidator;

@RestController
@RequestMapping(value = "/events")
public class EventController extends BaseController{
	final static Logger logger = Logger.getLogger(EventController.class);
	@Autowired
	IEventDao eventDao;

	@Autowired
	EventValidator eventValidator;
	/**
	 * This method is used to upload file and show the results on the screen.
	 * @param file
	 * @return List<Events>
	 * @throws IOException
	 * @throws UnauthorizedException 
	 */

	@RequestMapping(value = "/uploadFile" ,method = RequestMethod.POST)
	 @ResponseBody
	public String uploadFile(HttpSession session, @RequestParam("data") String data, 
				@RequestParam("file") MultipartFile file) throws IOException, UnauthorizedException{

		List<Event> eventList = null;


		Map<String, String> columnsMap = new HashMap<String, String>();
		
		columnsMap.put(UploadConstants.EVENT_NAME_LABEL + UploadConstants.STAR_LABEL, "title");
		columnsMap.put(UploadConstants.EVENT_DESCRIPTION_LABEL , "description");
		columnsMap.put(UploadConstants.EVENT_START_DATE_LABEL + UploadConstants.DATE_FORMAT_LABEL + UploadConstants.STAR_LABEL  , "startDate");
		columnsMap.put(UploadConstants.EVENT_START_TIME_LABEL + UploadConstants.STAR_LABEL , "startTime");
		columnsMap.put(UploadConstants.EVENT_END_DATE_LABEL + UploadConstants.DATE_FORMAT_LABEL + UploadConstants.STAR_LABEL , "endDate");	
		columnsMap.put(UploadConstants.EVENT_END_TIME_LABEL + UploadConstants.STAR_LABEL  , "endTime");
		columnsMap.put(UploadConstants.RECURRING_LABEL , "reccuring");
		columnsMap.put(UploadConstants.EVENT_REMINDER_TIME_IN_MINUTES_LABEL , "reminderBefore");
		columnsMap.put(UploadConstants.EVENT_SUMMARY_LABEL, "summary");
		columnsMap.put(UploadConstants.EVENT_VENUE_LABEL + UploadConstants.STAR_LABEL , "venue");
		columnsMap.put(UploadConstants.STANDARD_LABEL + UploadConstants.STAR_LABEL , "standardName");
		columnsMap.put(UploadConstants.DIVISION_LABEL , "divisionName");
		columnsMap.put(UploadConstants.EVENT_EVENTS_FOR_STUDENTS_LABEL + UploadConstants.STAR_LABEL , "forStudent");
		columnsMap.put(UploadConstants.EVENT_EVENTS_FOR_PARENTS_LABEL + UploadConstants.STAR_LABEL , "forParent");
		columnsMap.put(UploadConstants.EVENT_EVENTS_FOR_TEACHERS_LABEL + UploadConstants.STAR_LABEL , "forTeacher");
		columnsMap.put(UploadConstants.EVENT_EVENTS_FOR_VENDORS_LABEL + UploadConstants.STAR_LABEL , "forVendor");
		columnsMap.put(UploadConstants.EVENT_FREQUENCY_LABEL , "frequency");
		columnsMap.put(UploadConstants.EVENT_BY_DAY_ONLY_IF_FREQUENCY_IS_WEEKLY_LABEL , "byDay");
		columnsMap.put(UploadConstants.EVENT_BY_DATE_ONLY_IF_FREQUENCY_IS_MONTHLY_LABEL, "byDate");
		
		//eventList = CSVFileReader.readFile(file.getInputStream(), columns, new Event(), Event.class);
		
		eventList = ExcelUtils.parseIgnoringErrors(columnsMap, file.getInputStream(), Event.class);
		String jsonData = null;
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(data);
		
		Integer branchId = ((node != null && node.get("branchId") != null) ? node.get("branchId").asInt()  : null);
		Integer academicYearId = ((node != null && node.get("academicYearId") != null) ? node.get("academicYearId").asInt()  : null);
		Integer userId = (Integer) session.getAttribute("userId");
		Integer roleId = (Integer) session.getAttribute("roleId");

		checkUserBranch(branchId, session);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
			validateAcademicYear(branchId, academicYearId);
		}

		if(eventList != null && eventList.size() > 0){

			List<ValidationError> validationList = validateAndPrepareEvents(eventList, branchId, academicYearId, userId, roleId);
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

			if(validationList.size() > 0){
				String json = "{\"status\":\"fail\", \"isValidationError\" : \"true\",\"message\":\"Some of the data you have provided is incorrect.\", " +
						"\"records\":"+ow.writeValueAsString(validationList)+"}";
				return json;
			}

			if(eventDao.uploadEvent(eventList)){
				if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
					eventList = eventDao.getSentEventByTeacher(branchId, true, userId);
				}
				else{
					eventList = eventDao.getEventsByBranchId(branchId);
				}
				jsonData = "{\"status\":\"success\", \"message\":\"Upload sucessfully done\", " +
						"\"records\":"+ow.writeValueAsString(eventList)+"}";
			}
		}
		else {
			jsonData = "{\"status\":\"fail\", \"message\":\"Problem while reading file might be improper..\", " +
						"\"records\":[]}";
		}

		return jsonData;
	}

	private List<ValidationError> validateAndPrepareEvents(List<Event> eventList, Integer branchId, Integer academicYearId,Integer userId, Integer roleId) {

		int row = 1;
		String standard;
		String division;
		List<ValidationError> validationList = new ArrayList<>();

		eventValidator.setUpload(true);
		for(Event event : eventList){
			row ++;
					
			if(StringUtils.isEmpty(event.getTitle()) || StringUtils.isEmpty(event.getStartDate()) || StringUtils.isEmpty(event.getStartTime())) {
				ValidationError validationError = new ValidationError();
				validationError.setRow("ROW : "+row);
				String title = !StringUtils.isEmpty(event.getTitle()) && !StringUtils.isEmpty(event.getStartDate()) 
						? (!StringUtils.isEmpty(event.getTitle()) ? event.getTitle() +" " : "") +
						(!StringUtils.isEmpty(event.getStartDate()) ? event.getStartDate()  : ""): "Event data is not Available";

				validationError.setTitle(title);
				validationList.add(validationError);
			
			} else {
				if(event.getStandardName() != null){
					standard = event.getStandardName().replace("__", "");
					event.setStandardName(standard.replace("_", " "));
				}
				if(event.getDivisionName() != null){
					division = event.getDivisionName().replace("__", "");
					event.setDivisionName(division.replace("_", " "));
				}
				event.setBranchId(branchId);
				event.setAcademicYearId(academicYearId);
				event.setSentBy(userId);
				event.setSentByRoleId(roleId);
				ValidationError validationError = ValidatorUtil.validateForAllErrors(event, eventValidator);
				if(validationError != null){
					validationError.setRow("ROW : "+row);
					validationError.setTitle(!StringUtils.isEmpty(event.getTitle()) ? event.getTitle() : "Not Available");
					validationList.add(validationError);
				}
			}
		}
		return validationList;
	}

	 /**
	  * 
	  * @param session
	  * @param instituteId
	  * @param branchId
	  * @return
	 * @throws UnauthorizedException 
	  */
	 @RequestMapping(value = "/getEventsByBranch", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Event> getEventsByBranch(HttpSession session,  
			 @RequestParam(value = "branchId") Integer branchId) throws UnauthorizedException{

		 checkUserBranch(branchId, session);
		 return eventDao.getEventsByBranchId(branchId);
	 }

	/**
	* This method is used to get list of events based on teacher id & branchId.
	* @return List<Events>
	* @throws UnauthorizedException 
	*/
	@RequestMapping(value = "/getEventsByBranchForTeacher", method = RequestMethod.GET)
	@ResponseBody
	public List<Event> getEventsByBranchForTeacher(HttpSession session) throws UnauthorizedException{
/*		 checkUserBranch(branchId, session);
		 if(!session.getAttribute("userId").equals(teacherId) ){
			 throw new UnauthorizedException("Invalid teacher userId specified.");
		 }*/
		 return eventDao.getTeacherEvents((Integer) session.getAttribute("userId"), (Integer) session.getAttribute("branchId"));
	 }


	/**
	* This method is called when we add new event or edit event. Based on the event Id it will decide whether to add or edit a event. 
	* @param events
	 * @throws UnauthorizedException 
	*/ 
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveEvents(HttpServletRequest request , HttpSession session, @RequestParam("data") String data,  
			@RequestParam(value = "file",required = false) MultipartFile file) throws UnauthorizedException {
		 //MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
		 ObjectMapper mapper = new ObjectMapper();
		 boolean flag = false;

		 String result = null;
		 try {
			Event event = mapper.readValue(data, Event.class);
			event.setCourseId(3);
			checkUserBranch(event.getBranchId(), session);
			event.setSentByRoleId((Integer) session.getAttribute("roleId"));

			if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))) {
				validateAcademicYear(event.getBranchId(), event.getAcademicYearId());
			}

			if(event.getId() != null){
				 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
					 validateEvent((List<Integer>)session.getAttribute("branchIdList"), event.getId());
				 }
			}

			eventValidator.setUpload(false);
			ValidationError error = ValidatorUtil.validateForAllErrors(event, eventValidator);

			if(error != null){
				error.setTitle("Invalid event details.");
				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
					"\"message\":\"Invalid Data provided\", " +
					"\"records\" : "+mapper.writeValueAsString(error)+"}";
				return result;
			}

			if(event.getId() != null){
				flag = eventDao.updateEvent(event, file);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Event updated successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while updating event.\"}";
				}
			}
			else {
				flag = eventDao.insertEvent(event, file);
				if(flag){
					result = "{\"status\":\"success\", \"message\" : \"Event inserted successfully.\"}";
				}
				else {
					result = "{\"status\":\"fail\", \"message\" : \"Problem while inserting event.\"}";
				}
			}
		} catch (Exception e) {
			logger.error("",e);
			if(e instanceof UnauthorizedException ){
				throw new UnauthorizedException(e.getMessage());
			}
		}
		return result;
	}


	 /**
	 * This method is used to get a single event.
	 * @param id
	 * @return Events
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	 @ResponseBody
	 public Event getEvent(HttpSession session, @PathVariable Integer id) throws UnauthorizedException{
		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 validateEvent((List<Integer>)session.getAttribute("branchIdList"), id);
		 }
		 if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
			 checkEventCreatedByThisUser(id, (Integer) session.getAttribute("userId"));
		 }
		 return eventDao.getEventById(id);
	 }


	 /**
	 * This method is used to get a single event.
	 * @param id
	 * @return Events
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(value = "/view",method = RequestMethod.GET)
	 @ResponseBody
	 public Event getEvent(HttpSession session, @RequestParam("id") Integer id, 
		 	@RequestParam(value = "teacherUserId", required = false) Integer teacherUserId) throws UnauthorizedException{

		 if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 validateEvent((List<Integer>)session.getAttribute("branchIdList"), id);
			 checkEventBelongsToThisUser(id, (Integer) session.getAttribute("userId"));
		 }

		 return eventDao.getEventByIdToView(id, teacherUserId);
	 }


	 /**
	 * This method is used to delete a event.
	 * @param id
	 * @throws UnauthorizedException 
	 */
	 @SuppressWarnings("unchecked")
	 @RequestMapping(method = RequestMethod.DELETE)
	 @ResponseBody
	 public String deleteEvents(HttpSession session, @RequestParam Integer id) throws UnauthorizedException {
		 if(session.getAttribute("roleId").equals(Constants.ROLE_ID_TEACHER)){
			 checkEventCreatedByThisUser(id, (Integer) session.getAttribute("userId"));
		 }
		 else if(!session.getAttribute("roleId").equals(Constants.ROLE_ID_ADMIN)){
			 validateEvent((List<Integer>)session.getAttribute("branchIdList"), id);
		 }

		 boolean flag = false;
		 String result = null;
		 flag = eventDao.deleteEvent(id);
		 if(flag == true){
			 result = "{\"status\":\"success\", \"message\" : \"Event deleted successfully.\"}";
		 }
		 else{
			 result = "{\"status\":\"fail\", \"message\" : \"Problem while deleting event.\"}";
		 }
		 return result;
	 }


	/**
	 * This method is used to get list of event sent by Teacher.
	 * @return List<Event>
	 * @throws UnauthorizedException 
	 */
	 @RequestMapping(value = "getSentTeacherEvent", method = RequestMethod.GET)
	 @ResponseBody
	 public List<Event> getSentTeacherEvent(HttpSession session, @RequestParam("branchId") Integer branchId,
			 @RequestParam("roleId") Integer roleId,@RequestParam("userId") Integer userId) throws UnauthorizedException{
		 checkUserBranch(branchId, session);
		 return eventDao.getSentEventByTeacher(branchId, true, (Integer) session.getAttribute("userId"));
	 }
}
