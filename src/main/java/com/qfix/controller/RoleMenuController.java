package com.qfix.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qfix.daoimpl.RoleMenuDao;
import com.qfix.model.RoleMenu;
import com.qfix.model.RoleMenuMasterList;
import com.qfix.model.RoleMenuPermissions;

@RestController
@RequestMapping(value = "/roles")
public class RoleMenuController {

	@RequestMapping(value = "/menu",method = RequestMethod.GET)
	@ResponseBody
	public RoleMenuMasterList getRole() {
		RoleMenuDao roleMenuDao = new RoleMenuDao();
		return roleMenuDao.getRole();
	}
	
	@RequestMapping(value = "/menu-list",method = RequestMethod.GET)
	@ResponseBody
	public List<RoleMenu> getRoleMenu(@RequestParam("roleId") Integer roleId) {
		RoleMenuDao roleMenuDao = new RoleMenuDao();
		return roleMenuDao.getRoleMenu(roleId);
	}
	
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	@ResponseBody
	public void save(@RequestBody List<RoleMenu> moduleList) {
		RoleMenuDao roleMenuDao = new RoleMenuDao();
		roleMenuDao.save(moduleList);
	}
	
	@RequestMapping(value = "/menu-permissions",method = RequestMethod.GET)
	@ResponseBody
	public List<RoleMenuPermissions> getMenuPermissions(@RequestParam("menuId") Integer menuId,@RequestParam("roleId") Integer roleId) {
		RoleMenuDao roleMenuDao = new RoleMenuDao();
		return roleMenuDao.getMenuPermissions(menuId,roleId);
	}
	
	@RequestMapping(value = "/save-permission",method = RequestMethod.POST)
	@ResponseBody
	public void savePermissions(@RequestBody List<RoleMenuPermissions> roleMenuPermissionList) {
		RoleMenuDao roleMenuDao = new RoleMenuDao();
		roleMenuDao.savePermissions(roleMenuPermissionList);
	}
}
