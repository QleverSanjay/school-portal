package com.qfix.controller;

import java.util.HashMap;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.qfix.dao.IBranchConfigurationDao;
import com.qfix.model.BranchConfig;
import com.qfix.model.Receipt;

@RestController
@RequestMapping(value = "/configuration")
public class BranchConfigurationController extends BaseController {
	final static Logger logger = Logger
			.getLogger(BranchConfigurationController.class);

	@Autowired
	IBranchConfigurationDao configurationDao;
	String flag;
	
	@Autowired
	private Gson gson;

	@RequestMapping(value = "receipt", method = RequestMethod.POST)
	public String saveReceipt(HttpSession session,
			@Valid @RequestBody Receipt receipt) throws Exception {

		checkUserBranch(receipt.getBranchId(), session);
	
		try {
			flag=configurationDao.saveReceiptConfiguration(receipt);
			if (flag=="insert") {
				return "{\"status\":\"success\", \"message\":\"Branch receipt configuration inserted\"}";
			} else {
				return "{\"status\":\"success\", \"message\":\"Branch receipt configuration updated\"}";
			}
		}
		catch(DataAccessException dae){
			logger.error("Problem while updating branch configuration", dae);
			return "{\"status\":\"failure\", \"message\":\"problem while configure Branch receipt\"}";
			
		}
		catch (Exception e) {
			logger.error("Problem while updating branch configuration", e);
			return "{\"status\":\"failure\", \"message\":\"problem while configure Branch receipt\"}";
		}
	}

	@RequestMapping(value = "receipt", method = RequestMethod.GET)
	public Receipt getBranchReceiptConfiguration(HttpSession session,
			@NotNull @NotEmpty @RequestParam("branchId") Integer branchId)
			throws Exception {

		checkUserBranch(branchId, session);
		Receipt receipt = configurationDao.getReceiptConfiguration(branchId);
		return receipt;
	}

	@RequestMapping(value = "student", method = RequestMethod.POST)
	public String saveStudentConfig(HttpSession session, @RequestBody BranchConfig branchConfig)
			throws Exception {
		
		checkUserBranch(branchConfig.getBranchId(), session);
		configurationDao.saveStudentBranchConfig(branchConfig);
		
		HashMap<String,Object> response = new HashMap<String, Object>();
		response.put("status","success");
		response.put("message", "Student configuration saved");
		response.put("configId", branchConfig.getId());
		
		return getJson(response);
	}	
	
	private String getJson(HashMap<String,Object> data ){
		return gson.toJson(data);
	}
}
