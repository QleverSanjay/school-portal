package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.Institute;

@Scope(value="prototype")
@Repository
public class InstituteValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private List<Institute> instituteList;
	private boolean isUpload;
	private int index;

	public void setIndex(int index){
		this.index = index;
	}

	public void setInstituteList(List<Institute> instituteList) {
		this.instituteList = instituteList;
	}

	public boolean supports(Class clazz) {
		return Institute.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		Institute institute = (Institute) target;
		if(institute.getInstituteName() != null){
			institute.setInstituteName(institute.getInstituteName().replaceAll("\'", ""));
		}
		if(isUpload ){
			validateWithUploadedList(institute, errors);
		}
		try {
			if(!StringUtils.isEmpty(institute.getCode())){
				if(isCodeUnique(institute)){
					errors.rejectValue("code", "error.code", "Code is already used.");
				}
			}

			if(!StringUtils.isEmpty(institute.getInstituteName())){
				if(isInstituteNameUnique(institute)){
					errors.rejectValue("instituteName", "error.instituteName", "Institute Name is already used.");
				}
			}

			if(!StringUtils.isEmpty(institute.getAdminEmail())){
				if(isAdminEmailUnique(institute)){
					errors.rejectValue("adminEmail", "error.adminEmail", "Admin Email is already used.");
				}
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	private void validateWithUploadedList(Institute currentInstitute, Errors errors) {
		for(int i = 0; i <instituteList.size(); i++){
			if(i > index){
				Institute nextInstitute = instituteList.get(i);
				if(!StringUtils.isEmpty(currentInstitute.getCode()) 
						&& currentInstitute.getCode().equals(nextInstitute.getCode())){
					errors.rejectValue("code", "erros.code", 
						"Code is same in row "+(i + 2)+", Code = "+currentInstitute.getCode());
				}

				if(!StringUtils.isEmpty(currentInstitute.getInstituteName()) 
						&& currentInstitute.getInstituteName().equals(nextInstitute.getInstituteName())){
					errors.rejectValue("instituteName", "erros.code", 
						"Institute name is same in row "+(i + 2) + ", name = "+currentInstitute.getInstituteName());
				}

				if(!StringUtils.isEmpty(currentInstitute.getAdminEmail()) 
						&& currentInstitute.getAdminEmail().equals(nextInstitute.getAdminEmail())){
					errors.rejectValue("adminEmail", "erros.adminEmail", 
						"Admin Email is same in row "+(i + 2) + ", admin email = "+currentInstitute.getAdminEmail());
				}
			}
		}
	}

	private boolean isAdminEmailUnique(Institute institute) throws SQLException {
		String sql = "select count(*) as count from school_admin where emailAddress = '"+institute.getAdminEmail()
			+"'"+(institute.getId() != null
				? " AND user_id != (select id from user where institute_id = "+institute.getId()+")" 
				: "");

		Integer count = getJdbcTemplate().query(sql , new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);

		return (count > 0);
	}

	private boolean isInstituteNameUnique(Institute institute) throws SQLException {
		String sql = "select count(*) as count from institute where name = '"+institute.getInstituteName()
				+"'"+(institute.getId() != null ? " AND isDelete = 'N' AND id != "+institute.getId() : "");
		Integer count = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		return (count > 0);
	}

	private boolean isCodeUnique(Institute institute) throws SQLException {
		String sql = "select count(*) as count from institute where code = '"+institute.getCode()
				+"'"+(institute.getId() != null ? " AND id != "+institute.getId() : "");
		Integer count = getJdbcTemplate().query(sql, new RowMapper<Integer>(){
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		return (count > 0);
	}

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}
}