package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.dao.IAcademicYearDao;
import com.qfix.model.AcademicYear;
import com.qfix.model.Lecture;
import com.qfix.model.Student;
import com.qfix.model.Timetable;
import com.qfix.utilities.Constants;
import com.qfix.validator.MasterValidator;

@Scope(value="prototype")
@Repository
public class TimetableValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Autowired
	private IAcademicYearDao academicYearDao;

	@Autowired
	MasterValidator masterValidator;

	private List<String> subjectList;
	private List<Lecture> lectureList;
	Date startDate ;
	Date endDate ;
	Date startTime;
	Date endTime;
	Timetable timetable;
	/*private Integer standardId, divisionId;
	private String marksRegex = "^[0-9]$";*/

	public void initData (Timetable timetable, List<Lecture> lectureList, Date startDate, Date endDate, Date startTime, Date endTime){
		/*this.standardId = standardId;
		this.divisionId = divisionId;*/
		this.lectureList = lectureList;
		//System.out.println("This is  the lectureList:::::::::::::::::::->"+lectureList);
		this.subjectList = getSubjectListBasedOnStandardAndDivision(timetable.getStandardId());
		this.startDate = startDate;
		this.endDate = endDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.timetable = timetable;
	}

	public boolean supports(Class clazz) {
		return Student.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		Lecture lecture = (Lecture) target;
	//	validateWithList(report, errors);
		validateWithStudentsFromDatabase(lecture, errors);
		try {

			System.out.println("Inside of the subject method try block");
			isSubjectExists(timetable,lectureList,errors);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			checkStartAndEndDateTime(startDate, endDate, lecture, errors);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}


	// UNUSED
	private void validateWithStudentsFromDatabase(Lecture lecture, Errors errors) {
		/*boolean isIdExist = false, isNameExist = false, isRollNumberExist = false;*/

	}
	
	public void checkStartAndEndDateTime(Date startDate, Date endDate, Lecture lecture, Errors errors) throws ParseException
	{
			// Code to check date and time is valid or not

			//This code is for validating Time

			Date fromDate = null;
			Date toDate = null;
		
			List<AcademicYear> academicYearList = new ArrayList<AcademicYear>();
			academicYearList = academicYearDao.getActiveYear(timetable.getBranchId());
			
			for(AcademicYear academicYear : academicYearList)		//here only one record is disply so overwrite of fromDate and toDate is ignorable
			{
				String temp;
				String temp1;

				temp =academicYear.getFromDate().toString();
				temp1 =academicYear.getToDate().toString();

				fromDate = Constants.DATABASE_DATE_FORMAT.parse(temp);
				toDate = Constants.DATABASE_DATE_FORMAT.parse(temp1);
			}
			System.out.println("This is  from date "+fromDate+"This is start Date"+startDate);
			System.out.println("This is  to date "+toDate+"This is end Date"+endDate);

			// code to check academic dates with database 

			if(startDate.before(fromDate) || startDate.after(toDate))
			{
				errors.rejectValue("", "error.notExist.startDate","Academic Start Date in excel template is wrong");
			}
			if(endDate.before(fromDate) || endDate.after(toDate))
			{
				errors.rejectValue("", "error.notExist.endDate","Academic End Date in excel template is wrong");
			}

			//code to check time in excel
			if(lecture.getEnd().before(lecture.getStart()) || lecture.getEnd().equals(lecture.getStart())){
				errors.rejectValue("start", "error.before.startTimeHour" ,"Lecture end time must be after start.");
			}
			if(lecture.getStart().getHours() < startTime.getHours())
			{
				errors.rejectValue("start", "error.notExist.startTimeHour" ,"Entered start time of lecture is wrong. ");
			}
			else if(lecture.getStart().getHours() == startTime.getHours() && lecture.getStart().getMinutes() < startTime.getMinutes()){
					errors.rejectValue("start", "error.notExist.startTimeMinutes" ,"Entered start time of lecture is wrong. ");
			}

			if(lecture.getEnd().getHours() > endTime.getHours())
			{
				errors.rejectValue("end", "error.notExist.endTime" ,"Entered end time of lecture is wrong ");
			}
			else if(lecture.getEnd().getHours() == endTime.getHours() && lecture.getEnd().getMinutes() > endTime.getMinutes()){
					errors.rejectValue("end", "error.notExist.endTimeMinutes" ,"Entered end time of lecture is wrong ");
			}
	}

	public List<String> getSubjectListBasedOnStandardAndDivision(Integer standardId) {
		List<String> subjectList = new ArrayList<>();
		String sql = "Select * from subject where is_delete = 'N' AND id in " +
					"(select subject_id from standard_subject where standard_id = "+standardId+" )";

		System.out.println(sql);
		subjectList = getJdbcTemplate().query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				return resultSet.getString("name");
			}
		});

		return subjectList;
	}

	private void isSubjectExists(Timetable timetable, List<Lecture> lectureList, Errors errors) throws SQLException {
		int subjectId = 0;

		for(Lecture lecture : lectureList){

			System.out.println("Inside of the subject check");
		if(!StringUtils.isEmpty(lecture.getTitle())){
			if(!lecture.getTitle().equals("BREAK")){
				System.out.println("Subject which fired to query :: "+lecture.getTitle());
				subjectId = masterValidator.getSubjectIdByValidatingNameWithStandard(lecture.getTitle(), timetable.getStandardId());
				if(subjectId == 0){
					errors.rejectValue("title", "title", "Subject "+lecture.getTitle()+" is not found.");
				}
			}
		}
		else {
			errors.rejectValue("title", "title", "Subject is required.");
		}
		}
	}
	
}