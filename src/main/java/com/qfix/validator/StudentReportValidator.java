package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.Student;
import com.qfix.model.StudentReport;
import com.qfix.model.Subject;
import com.qfix.utilities.Constants;

@Scope(value="prototype")
@Repository
public class StudentReportValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private Map<String, Subject> subjectMap;
	private List<StudentReport> studentReportList;
	private List<Student> studentList;
	private Integer resultTypeId;


	public void initData (Integer standardId, Integer divisionId, Integer resultTypeId, List<StudentReport> studentReportList){
		/*this.standardId = standardId;
		this.divisionId = divisionId;*/
		this.studentReportList = studentReportList;
		this.studentList = getStudentListBasedOnStandardAndDivision(standardId, divisionId);
		this.subjectMap = setSubjectsBasedOnStandard(standardId);
		this.resultTypeId = resultTypeId;
	}


	public boolean supports(Class clazz) {
		return Student.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		StudentReport report = (StudentReport) target;
		validateWithList(report, errors);
		validateWithStudentsFromDatabase(report, errors);
	}

	private void validateWithStudentsFromDatabase(StudentReport report, Errors errors) {
		boolean isIdExist = false, isNameExist = false, isRollNumberExist = false;

		if(report.getId() == null){
			errors.rejectValue("id", "error.id" ,"Student Id not available.");
		}

		if(StringUtils.isEmpty(report.getStudentName())){
			errors.rejectValue("studentName", "error.studentName" ,"Student Name not available.");
		}

		/*if(StringUtils.isEmpty(report.getRoleNumber())){
			errors.rejectValue("rollNumber", "error.rollNumber" ,"Roll number not available.");
		}*/

		for(Student student : studentList){

			if(student.getId().equals(report.getId())){
				isIdExist = true;
			}

			if(student.getName().equals(report.getStudentName())){
				isNameExist = true;
			}

			if(!StringUtils.isEmpty(report.getRoleNumber()) && report.getRoleNumber().equals(student.getRollNumber())){
				isRollNumberExist = true;
			}
		}

		if(!isIdExist){
			errors.rejectValue("id", "error.notExist.id" ,"Id same as record - (Student Id not found.");
		}

		if(!isNameExist){
			errors.rejectValue("studentName", "error.notExist.studentName" ,"Student Name not found.");
		}

		if(!isRollNumberExist && !StringUtils.isEmpty(report.getRoleNumber())){
			errors.rejectValue("roleNumber", "error.notExist.Roll Number" ,"Roll number not found.");
		}

		validateSubjectsAndMarks(report, errors);
	}


	private void validateSubjectsAndMarks(StudentReport report, Errors errors) {
		List<Subject> subjectList = report.getSubjectList();
		if(subjectList == null || subjectList.size() == 0 ){
			errors.rejectValue("subjectList", "error.required.subjectList" ,"Subjects not provided for record - (Student Id = "+report.getId() 
					+", Student Name = "+report.getStudentName() + ", Roll Number = "+report.getRoleNumber()+")");
		}
		else {
			if(resultTypeId.equals(Constants.RESULT_TYPE_MARKS_ID)){
				if(StringUtils.isEmpty(report.getGrossTotal())){
					errors.rejectValue("", "error.required.grossTotal" ,"Gross total is mandatory.");
				}
			}
			else {
				if(StringUtils.isEmpty(report.getFinalGrade())){
					errors.rejectValue("", "error.required.finalGrade" ,"Final grade is mandatory.");
				}
			}
			if(StringUtils.isEmpty(report.getResult())){
				errors.rejectValue("", "error.required.result" ,"Result is mandatory.");
			}
			validateMarks(report, errors, subjectList);
		}
	}


	private void validateMarks(StudentReport report, Errors errors, List<Subject> subjectList) {
		for(Subject subject : subjectList){
			String subjectName = subject.getName();

			if(StringUtils.isEmpty(subjectName)){
				errors.rejectValue("", "error.required."+subjectName+".subjectName" ,"Subject name is mandatory.");
			}
			else if(!subjectMap.containsKey(subjectName)){
				errors.rejectValue("", "error.invalid."+subjectName+".subjectName" ,"Subject not found for Subject = "+subjectName);
			}
			else {
				// TODO Check theory marks and practical marks less than actual subject marks.

				// Subject uploadedSubject = subjectMap.get(subjectName);
				try {
					if(resultTypeId.equals(Constants.RESULT_TYPE_MARKS_ID)){
						Integer theoryMarks = (int) (!StringUtils.isEmpty(subject.getTheory()) ? Double.parseDouble(subject.getTheory()) : 0);
						Integer practicalMarks = (int) (!StringUtils.isEmpty(subject.getPractical()) ? Double.parseDouble(subject.getPractical()) : 0);
						Integer obtainedMarks = theoryMarks + practicalMarks;
						subject.setObtainedMarks(obtainedMarks.toString());
						if(obtainedMarks <= 0){
							errors.rejectValue("", "error.required."+subjectName+".invalidMarks" ,"Invalid theory or practical marks specified for "+subjectName);
						}
					}

					subject.setId(subjectMap.get(subjectName).getId());

					if(StringUtils.isEmpty(subject.getObtainedMarks())){
						errors.rejectValue("", "error.required."+subjectName+".obtainedMarks" ,"Obtain marks or grade is mandatory for Subject = "+subjectName);
					}
					if(StringUtils.isEmpty(subject.getTheory()) && StringUtils.isEmpty(subject.getPractical())){
						errors.rejectValue("", "error.required."+subjectName+".theoryOrPractical" ,"Theory or practical marks is mandatory for Subject = "+subjectName);
					}	
				}catch(Exception e){
					errors.rejectValue("", "error.required."+subjectName+".invalidMarks" ,"Invalid theory or practical marks specified for "+subjectName);
					logger.error("Error while parsing theory or practical marks", e);
					
				}
			}
		}
	}


	private void validateWithList(StudentReport currentReport, Errors errors) {
		System.out.println("currentReport :::"+currentReport);
		for(StudentReport nextReport : studentReportList){
			if(!nextReport.equals(currentReport)){
				System.out.println("nextReport :::"+nextReport);
				if(currentReport.getId() == null){
					errors.rejectValue("id", "error.id" ,"Student Id not available.");
				}
				else if(nextReport.getId().equals(currentReport.getId())){
					errors.rejectValue("id", "error.id.same" ,"Id same as record - (Student Id = "+nextReport.getId() 
							+", Student Name = "+nextReport.getStudentName() + ", Roll Number = "+nextReport.getRoleNumber()+")");
				}

				if(StringUtils.isEmpty(currentReport.getStudentName())){
					errors.rejectValue("studentName", "error.studentName" ,"Student Name not available.");
				}
				else if(nextReport.getStudentName().equals(currentReport.getStudentName())){
					errors.rejectValue("studentName", "error.studentName.same" ,"Student Name same as record - (Student Id = "+nextReport.getId() 
							+", Student Name = "+nextReport.getStudentName() + ", Roll Number = "+nextReport.getRoleNumber()+")");
				}

				if(StringUtils.isEmpty(currentReport.getRoleNumber())){
					errors.rejectValue("roleNumber", "error.rollNumber" ,"Roll number not available.");
				}
				else if(nextReport.getRoleNumber().equals(currentReport.getRoleNumber())){
					errors.rejectValue("roleNumber", "error.roleNumber.same" ,"Roll number same as record - (Student Id = "+nextReport.getId() 
							+", Student Name = "+nextReport.getStudentName() + ", Roll Number = "+nextReport.getRoleNumber()+")");
				}
			}
		}
	}


	public Map<String, Subject> setSubjectsBasedOnStandard(Integer standardId) {
		String sql = "select s.* from subject as s " +
				" INNER JOIN standard_subject ss on ss.subject_id = s.id" +
				" INNER JOIN standard as st on st.id = ss.standard_id" +
				" WHERE s.is_delete = 'N' AND ss.is_delete = 'N' AND st.active = 'Y'" +
				" AND st.id = "+standardId;

		final Map<String, Subject> subjectMap = new HashMap<String, Subject>();
		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Subject subject = new Subject();
				subject.setTheoryMarks(resultSet.getDouble("theory"));
				subject.setPracticalMarks(resultSet.getDouble("practical"));
				subject.setId(resultSet.getInt("id"));
				subject.setName(resultSet.getString("name"));
				subjectMap.put(resultSet.getString("name"), subject);
				return null;
			}
		});
		
		return subjectMap;
	}


	public List<Student> getStudentListBasedOnStandardAndDivision(Integer standardId, Integer divisionId) {
		List<Student> studentList = new ArrayList<>();
		String sql = "SELECT id, first_name , father_name, last_name , " +
				"roll_number, branch_id, standard_id, division_id " +
				"FROM `student` where active = 'Y' AND isDelete = 'N' " +
				"AND standard_id = "+standardId+" AND division_id = "+divisionId;

		System.out.println(sql);
		studentList = getJdbcTemplate().query(sql, new RowMapper<Student>(){
			@Override
			public Student mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				Student student = new Student();
				student.setId(resultSet.getInt("id"));
				String firstName = resultSet.getString("first_name");
				String fatherName = resultSet.getString("father_name");
				String lastName = resultSet.getString("last_name");
				String name = (firstName != null ? firstName+" " : "" )+ (fatherName != null ? fatherName+" " : "")+(lastName != null ? lastName : "");
				student.setName(name);
				student.setRollNumber(resultSet.getString("roll_number"));
				student.setBranchId(resultSet.getInt("branch_id"));
				student.setStandardId(resultSet.getInt("standard_id"));
				student.setDivisionId(resultSet.getInt("division_id"));
				return student;
			}
		});

		return studentList;
	}
}