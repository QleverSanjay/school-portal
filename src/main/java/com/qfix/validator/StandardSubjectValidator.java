package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.StandardSubject;

@Repository
public class StandardSubjectValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private boolean isUpload;
	private List<StandardSubject> subjectList;
	@Autowired
	MasterValidator masterValidator;

	private int index;

	public void setIndex(int index){
		this.index = index;
	}

	public void setStandardSubjectList(List<StandardSubject> subjectList) {
		this.subjectList = subjectList;
	}

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}


	public boolean supports(Class clazz) {
		return StandardSubject.class.isAssignableFrom(clazz);
	}


	public void validate(Object target, Errors errors) 
	{
		StandardSubject subject = (StandardSubject) target;
		try {
			if(isUpload){
				validateWithUploadedList(subject, errors);
				checkStandardSubjectUniqueWhenUpload(subject, errors);
			}
			else {
				checkStandardSubjectUnique(subject, errors);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}


	private void validateWithUploadedList(StandardSubject currentStandardSubject, Errors errors) {
		for(int i = 0; i< subjectList.size(); i++){
			if(i > index){
				StandardSubject nextStandardSubject = subjectList.get(i);
				if(!StringUtils.isEmpty(currentStandardSubject.getStandard()) 
						&& currentStandardSubject.getStandard().equals(nextStandardSubject.getStandard()) 
						&& !StringUtils.isEmpty(currentStandardSubject.getSubject()) 
						&& currentStandardSubject.getSubject().equals(nextStandardSubject.getSubject())){

					errors.rejectValue("standard", "error.standardAndSubject", 
						"Subject is same in row "+(i + 2)+", subject = "+currentStandardSubject.getSubject()
						+", for Standard = "+currentStandardSubject.getStandard());
				}
			}
		}
	}

	private void checkStandardSubjectUnique(StandardSubject subject, final Errors errors) throws SQLException {
		if(subject.getStandardId() == null || subject.getStandardId() == 0){
			errors.rejectValue("standard", "error.standard", "Standard is mandatory.");
		}

		if(subject.getSubjectIdList() != null && subject.getSubjectIdList().size() > 0){

			final int i =0;
			for(Integer subjectId : subject.getSubjectIdList()){

				String sql = "select count(standard_id) as count, st.displayName as standard, su.id as subjectId, su.name as subject from standard_subject as ss" +
					" INNER JOIN standard as st on st.id = ss.standard_id" +
					" INNER JOIN subject as su on su.id = ss.subject_id" +
					" where ss.standard_id = '"+subject.getStandardId()+"'" +
					" AND ss.subject_id = "+subjectId+
					" AND ss.is_delete = 'N'" +
					" AND st.branch_id = "+subject.getBranchId() + 
					(subject.getPreviousStandardId() != null? " AND ss.standard_id != "+subject.getPreviousStandardId() : "");

				getJdbcTemplate().query(sql, new RowMapper<Integer>(){
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
						int count = resultSet.getInt("count");
						if(count > 0){
							errors.rejectValue("subject", "exist.standardAndSubject"+resultSet.getInt("subjectId")
								, "Linking allready exist for Standard = "+resultSet.getString("standard")
								+", Subject = "+resultSet.getString("subject"));
						}
						return resultSet.getInt("count");
					}
				});
			}	
		}
		else {
			errors.rejectValue("subject", "error.subject", "Subject is mandatory.");
		}
	}

	private void checkStandardSubjectUniqueWhenUpload(StandardSubject subject, Errors errors) throws SQLException {
		int standardId = 0;
		int subjectId = 0;

		if(!StringUtils.isEmpty(subject.getStandard())){
			standardId = masterValidator.getStandardIdByValidatingName(subject.getStandard(), subject.getBranchId(), null);
			if(standardId == 0){
				errors.rejectValue("standard", "standard", "Standard not found.");
			}
			else {
				subject.setStandardId(standardId);
			}
		}
		else {
			errors.rejectValue("standard", "standard", "Standard is required.");
		}


		if(!StringUtils.isEmpty(subject.getSubject())){
			subjectId = masterValidator.getSubjectIdByValidatingName(subject.getSubject(), subject.getBranchId(), null);
			if(subjectId == 0){
				errors.rejectValue("subject", "subject", "Subject not found.");
			}
			else {
				subject.setSubjectId(subjectId);
			}
		}
		else {
			errors.rejectValue("subject", "subject", "Subject is required.");
		}

		if(standardId != 0 && subjectId != 0){
			String sql = "select count(*) as count from standard_subject " +
					"where standard_id = '"+subject.getStandardId()+"'" +
					" AND subject_id = '"+subject.getSubjectId()+"'" +
					" AND is_delete = 'N'" +
					(subject.getPreviousStandardId() != null 
					? " AND standard_id != "+subject.getPreviousStandardId() : "");

			int count =  getJdbcTemplate().query(sql, new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					return resultSet.getInt("count");
				}
			}).get(0);

			if(count > 0){
				errors.rejectValue("subject", "exist.subject", 
					"Linking allready exist for Standard = "+subject.getStandard()+", Subject = "+subject.getSubject());	
			}
		}
	}
}