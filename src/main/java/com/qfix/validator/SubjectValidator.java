package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.Subject;

@Scope(value="prototype")
@Repository
public class SubjectValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private boolean isUpload;
	private List<Subject> subjectList;
	private int index;

	public void setIndex(int index){
		this.index = index;
	}
	public void setSubjectList(List<Subject> subjectList) {
		this.subjectList = subjectList;
	}

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}


	public boolean supports(Class clazz) {
		return Subject.class.isAssignableFrom(clazz);
	}


	public void validate(Object target, Errors errors) 
	{
		Subject subject = (Subject) target;
		try {
			if(isUpload){
				validateWithUploadedList(subject, errors);
			}
			checkSubjectUnique(subject, errors);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}


	private void validateWithUploadedList(Subject currentSubject, Errors errors) {
		for(int i = 0; i< subjectList.size(); i++){
			if(i > index){
				Subject nextSubject = subjectList.get(i);
				if(!StringUtils.isEmpty(currentSubject.getName()) 
						&& currentSubject.getName().equals(nextSubject.getName())){
					errors.rejectValue("name", "error.name", 
						"Subject name is same in row "+(i + 2)+", name = "+currentSubject.getName());
				}

				if(!StringUtils.isEmpty(currentSubject.getCode()) 
						&& currentSubject.getCode().equals(nextSubject.getCode())){
					errors.rejectValue("code", "error.code", 
						"Subject code is same in row "+(i + 2)+", code = "+currentSubject.getCode());
				}
			}
		}
	}


	private void checkSubjectUnique(Subject subject, Errors errors) throws SQLException {
		if(!StringUtils.isEmpty(subject.getName())){
			String sql = "select count(*) as count from subject where is_delete = 'N' AND name = '"
				+ subject.getName()+ "' AND branch_id = "+subject.getBranchId() + (subject.getId() != null ? 
				" AND id != (select id from subject where id = "+subject.getId()+")" : "");

			int count =  getJdbcTemplate().query(sql, new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					return resultSet.getInt("count");
				}
			}).get(0);

			if(count > 0){
				errors.rejectValue("name", "exist.name", "Subject name already exist.");	
			}
		}

		if(!StringUtils.isEmpty(subject.getCode())){
			String sql = "select count(*) as count from subject where is_delete = 'N' AND " +
				" code = '"+subject.getCode()+"'" +
				" AND branch_id = "+subject.getBranchId() + (subject.getId() != null ? 
				" AND id != (select id from subject where id = "+subject.getId()+")" : "");

			int count =  getJdbcTemplate().query(sql, new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					return resultSet.getInt("count");
				}
			}).get(0);

			if(count > 0){
				errors.rejectValue("code", "exist.code", "Subject code already exist.");	
			}
		}
	}
}