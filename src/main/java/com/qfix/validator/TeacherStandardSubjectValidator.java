package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.TeacherStandardSubject;

@Scope(value="prototype")
@Repository
public class TeacherStandardSubjectValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	MasterValidator masterValidator;

	List<TeacherStandardSubject> teacherStandardSubjectList;
	private int index;

	public void setTeacherStandardSubjectList(List<TeacherStandardSubject> teacherStandardSubjectList){
		this.teacherStandardSubjectList = teacherStandardSubjectList;
	}

	public void setIndex(int index){
		this.index = index;
	}

	public boolean supports(Class clazz) {
		return TeacherStandardSubject.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		TeacherStandardSubject teacherStandardSubject = (TeacherStandardSubject) target;
		try {
				isTeacherExists(teacherStandardSubject, errors);
				isStandardExists(teacherStandardSubject, errors);
				if(!StringUtils.isEmpty(teacherStandardSubject.getDivision())) {
					isDivisionExists(teacherStandardSubject, errors);
				}
				isSubjectExists(teacherStandardSubject, errors);
				validateAllFields(teacherStandardSubject, errors);
				validateDuplicateRecords(teacherStandardSubjectList, teacherStandardSubject, errors);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	private void isTeacherExists(TeacherStandardSubject teacherStandardSubject, Errors errors) throws SQLException {
		int teacherId = 0;
		if(!StringUtils.isEmpty(teacherStandardSubject.getTeacher())){
			String[] nameArr = teacherStandardSubject.getTeacher().split(" ");
			if(nameArr.length != 2) {
				errors.rejectValue("Teacher", "Teacher", "Teacher "+ teacherStandardSubject.getTeacher() +" is not exist.");
				return;
			} 
			teacherId = masterValidator.getTeacherIdByValidatingName(nameArr[0], nameArr[1], teacherStandardSubject.getBranchId());
			if(teacherId == 0){
				errors.rejectValue("Teacher", "Teacher", "Teacher "+ teacherStandardSubject.getTeacher() +" is not exist.");
			}
			else {
				teacherStandardSubject.setTeacherId(teacherId);
			}
		}
		else {
			errors.rejectValue("Teacher", "Teacher", "Teacher is required.");
		}
	}

	private void isStandardExists(TeacherStandardSubject teacherStandardSubject, Errors errors) throws SQLException {
		int standardId = 0;

		if(!StringUtils.isEmpty(teacherStandardSubject.getStandard())){
			standardId = masterValidator.getStandardIdByValidatingName(teacherStandardSubject.getStandard(), teacherStandardSubject.getBranchId(), null);
			if(standardId == 0){
				errors.rejectValue("standard", "standard", "Standard "+teacherStandardSubject.getStandard()+" is not found.");
			}
			else {
				teacherStandardSubject.setStandardId(standardId);
			}
		}
		else {
			errors.rejectValue("standard", "standard", "Standard is required.");
		}
	}

	private void isDivisionExists(TeacherStandardSubject teacherStandardSubject, Errors errors) throws SQLException {
		int divisionId = 0;

		if(!StringUtils.isEmpty(teacherStandardSubject.getStandard())){
			divisionId = masterValidator.getDivisionIdByValidatingName(teacherStandardSubject.getDivision(), teacherStandardSubject.getStandardId(), teacherStandardSubject.getBranchId(), null);
			if(divisionId == 0){
				errors.rejectValue("division", "division", "Division "+teacherStandardSubject.getStandard()+" is not found.");
			}
			else {
				teacherStandardSubject.setDivisionId(divisionId);
			}
		}
		else {
			errors.rejectValue("division", "division", "division is required.");
		}
	}

	private void validateDuplicateRecords(List<TeacherStandardSubject> teacherStandardSubjectList,TeacherStandardSubject currentTeacherStandardSubject, Errors errors) {
		// TODO Auto-generated method stub
		for(int i = 0; i< teacherStandardSubjectList.size(); i++){
			if(i > index){
				TeacherStandardSubject nextTeacher = teacherStandardSubjectList.get(i);
				
					nextTeacher.setStandard(!StringUtils.isEmpty(nextTeacher.getStandard()) ? nextTeacher.getStandard().replace("_", " ") : nextTeacher.getStandard());
					nextTeacher.setDivision(!StringUtils.isEmpty(nextTeacher.getDivision()) ? nextTeacher.getDivision().replace("_", " ") : nextTeacher.getDivision());
					nextTeacher.setSubject(!StringUtils.isEmpty(nextTeacher.getSubject()) ? nextTeacher.getSubject().replace("_", " ") : nextTeacher.getSubject());

					if(!StringUtils.isEmpty(currentTeacherStandardSubject.getTeacher()) 
						&& currentTeacherStandardSubject.getTeacher().equals(nextTeacher.getTeacher()) 
							&& currentTeacherStandardSubject.getStandard().equals(nextTeacher.getStandard()) 
								&& currentTeacherStandardSubject.getDivision().equals(nextTeacher.getDivision()) 
									&& currentTeacherStandardSubject.getSubject().equals(nextTeacher.getSubject())){
					errors.rejectValue("teacher", "error.teacher", 
						"Teacher record is same as in row "+(i + 2)+", Teacher = "+currentTeacherStandardSubject.getTeacher());
				}
			}
			
		}
		
	}

	private void isSubjectExists(TeacherStandardSubject teacherStandardSubject, Errors errors) throws SQLException {
		int subjectId = 0;

		if(!StringUtils.isEmpty(teacherStandardSubject.getSubject())){
			subjectId = masterValidator.getSubjectIdByValidatingName(teacherStandardSubject.getSubject(), teacherStandardSubject.getBranchId(), null);
			if(subjectId == 0){
				errors.rejectValue("subject", "subject", "Subject "+teacherStandardSubject.getSubject()+" is not found.");
			}
			else {
				teacherStandardSubject.setSubjectId(subjectId);
			}
		}
		/*else {
			errors.rejectValue("subject", "subject", "Subject is required.");
		}*/
	}

	private void validateAllFields(TeacherStandardSubject teacherStandardSubject, Errors errors) throws SQLException {
		if(teacherStandardSubject.getTeacherId() != 0 && teacherStandardSubject.getStandardId() != 0) {
			String sql = "select count(*) as count from teacher_standard " +
					"where teacher_id = '"+teacherStandardSubject.getTeacherId()+"'" +
					" AND standard_id = '"+teacherStandardSubject.getStandardId()+"'" +
					" AND division_id = '"+teacherStandardSubject.getDivisionId()+"'" +
					(
							(teacherStandardSubject.getSubjectId() != null && teacherStandardSubject.getSubjectId() != 0 ) 
							? " AND subject_id = '"+teacherStandardSubject.getSubjectId()+"'" 
							: ""
					)+
					" AND is_delete = 'N'" ;

			int count =  getJdbcTemplate().query(sql, new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
					return resultSet.getInt("count");
				}
			}).get(0);

			if(count > 0){
				errors.rejectValue("subject", "exist.subject", 
					"Linking allready exist for Teacher = "+teacherStandardSubject.getTeacher()+", Standard = "+teacherStandardSubject.getStandard()+", Subject = "+teacherStandardSubject.getSubject());	
			}
		}
	}
}