package com.qfix.validator;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.exceptions.ApplicationException;
import com.qfix.model.DisplayHead;
import com.qfix.model.DisplayTemplate;

@Scope(value="prototype")
@Repository
public class DisplayTemplateValidator  extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private boolean isUpload;

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}

	public boolean supports(Class clazz) {
		return DisplayTemplate.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		DisplayTemplate template = (DisplayTemplate) target;
		try {
			checkDisplayTemplateExistByName(template, errors);
			List<DisplayHead> heads = template.getDisplayHeadList();
			if(heads == null && heads.isEmpty()){
				errors.rejectValue("", "error.displayHeadList","Display Heads are required.");
			}
			else {
				validateDisplayHeads(heads, errors, template.getBranchId());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void checkDisplayTemplateExistByName(DisplayTemplate template, Errors errors) {
		String sql = "select count(*) from display_template where name = '"+template.getName()+"' AND branch_id = " + template.getBranchId();;
		if(template.getId() != null){
			sql += " AND id != " + template.getId();
		}
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count> 0){
			errors.rejectValue("", "error.templateExist", "Display Template already exist by this name.");
		}
	}

	private void validateDisplayHeads(List<DisplayHead> heads, Errors errors, Integer branchId) {
		List<Integer> headIdList = new ArrayList<>();
		for(DisplayHead head : heads){

			if(head.getId() != null){
				headIdList.add(head.getId());
			}
			else if(head.getId() == null && !StringUtils.isEmpty(head.getName())){
				errors.rejectValue("", "error.displayHeadNotPresent","Display Head is not present in system.");
			}
			else if(StringUtils.isEmpty(head.getName())){
				errors.rejectValue("", "error.displayHeadName","Display Head is required.");
			}
			if(head.getAmount() == null || head.getAmount() < 0){
				errors.rejectValue("", "error.displayHeadAmount","Display Head amount is required.");
			}
			if(head.getDisplayOrder() == null || head.getDisplayOrder() < 0){
				errors.rejectValue("", "error.displayHeadOrder","Display Head display order is required.");
			}
		}

		String headIdStr = org.apache.commons.lang.StringUtils.join(headIdList, ",");

		String sql = "select count(*) from display_head where id in ("+headIdStr+") and branch_id = " + branchId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count != headIdList.size()){
			errors.rejectValue("", "error.wrongDisplayHead","Some display does not belong your branch.");
		}
	}
}