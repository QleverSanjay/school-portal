package com.qfix.validator;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.dao.IAcademicYearDao;
import com.qfix.model.AcademicYear;
import com.qfix.model.Event;
import com.qfix.utilities.Constants;
import com.qfix.utilities.DateUtil;
import com.qfix.utilities.FrequencyConstants;

@Scope(value="prototype")
@Repository
public class EventValidator implements Validator {

	private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

	@Autowired
	MasterValidator masterValidator;

	@Autowired
	private IAcademicYearDao academicYearDao;
	
	private Date today;
	private boolean isUpload;

	
    public EventValidator() {
		super();
		dateutil = new DateUtil();
	}


	private DateUtil dateutil;
	
	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}
	
	public boolean supports(Class clazz) {
		return Event.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		Event event = (Event) target;

		Calendar calendar = Calendar.getInstance();
		
		today = new Date(calendar.getTimeInMillis());

		prepareEventByFrequency(event, errors, isUpload);
		validateDateTime(event, errors);
		validateRecurring(event, errors);
		validateEventForAndReminders(event, errors);

		if(isUpload){
			validateWithDatabase(event, errors);	
		}
		else {
			validateGroupsAndIndividuals(event, errors);
		}
	}

	private void validateGroupsAndIndividuals(Event event, Errors errors) {
		Set<Integer> groups = null;
		Set<Integer> individuals = null;
		if(event.getGroupIdList() != null){
			groups = new HashSet<Integer>(event.getGroupIdList());
		}
		if(event.getIndividualIdList() != null){
			individuals = new HashSet<>(event.getIndividualIdList());
		}

		if(groups == null && individuals == null ){
			errors.rejectValue("", "errors.groupsNotApplicable", "Either group or individual is mandatory.");
		}
		else {
			boolean hasGroupError = false;
			boolean hasIndividualError = false;
			if(groups != null && groups.size() > 0){
				if(!masterValidator.isGroupsPresentInThisBranch(event.getBranchId(), groups)){
					hasGroupError = true;
					errors.rejectValue("", "errors.groupsNotApplicable", "Some groups does not belongs to this branch.");
				}
			}

			if(individuals != null && individuals.size() > 0){
				if(!masterValidator.isIndividualsPresentInBranch(event.getBranchId(), individuals)){
					hasIndividualError = true;
					errors.rejectValue("", "errors.groupsNotApplicable", "Some individuals does not belongs to this branch.");
				}
			}

			if(event.getSentByRoleId().equals(Constants.ROLE_ID_TEACHER)){
				if(groups != null && groups.size() > 0 && !hasGroupError){
					if(!masterValidator.isGroupsApplicableForThisTeacher(event.getSentBy(), groups)){
						errors.rejectValue("", "errors.groupsNotApplicable", "Some groups are not applicable for this teacher.");
					}
				}

				if(individuals !=  null && individuals.size() > 0 && !hasIndividualError){
					if(!masterValidator.isIndividualsApplicableForThisTeacher(event.getSentBy(), individuals)){
						errors.rejectValue("", "errors.individualsNotApplicable", "Some individuals are not applicable for this teacher.");
					}
				}
			}
		}
	}

	private Event prepareEventByFrequency(Event event, Errors errors,
			boolean isUpload) {

		if (!StringUtils.isEmpty(event.getFrequency())) {
			if (!isUpload && event.getFrequency().equalsIgnoreCase("Weekly")) {
				String byDay = "";
				Map<String, Boolean> eventWeekDays = event.getEventWeekDays();
				int count = 0;
				for (String day : eventWeekDays.keySet()) {
					if (eventWeekDays.get(day).equals(true)) {
						count++;
						byDay = byDay + (count != 1 ? "," : "") + day;
					}
				}
				event.setByDay(byDay);
			} else if (!isUpload && event.getFrequency().equalsIgnoreCase("Yearly")) {
				String byDate = "";
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(event.getStartDateD());

				byDate = calendar.get(Calendar.DAY_OF_MONTH) + "";
				event.setByDate(byDate);
			} else if (event.getFrequency().equalsIgnoreCase("Monthly")) {
				if (StringUtils.isEmpty(event.getByDate())) {
					errors.rejectValue("byDate", "error.required.byDate",
							"Date is required when recurring is monthly.");
				}
			}
		}

		return event;
	}

	private void validateEventForAndReminders(Event event, Errors errors) {
		if(!StringUtils.isEmpty(event.getReminderSet()) && event.getReminderSet().equalsIgnoreCase("Y")){
			if(event.getReminderBefore() == null || event.getReminderBefore() == 0){
				errors.rejectValue("reminderBefore", "reminderBefore", 
					"Reminder before is required when Reminder is set to yes.");
			}
		}
		if((StringUtils.isEmpty(event.getForParent()) || event.getForParent().equalsIgnoreCase("N")) &&
			(StringUtils.isEmpty(event.getForTeacher()) || event.getForTeacher().equalsIgnoreCase("N")) && 
			(StringUtils.isEmpty(event.getForStudent()) || event.getForStudent().equalsIgnoreCase("N")) && 
			(StringUtils.isEmpty(event.getForVendor()) || event.getForVendor().equalsIgnoreCase("N"))){

			errors.rejectValue("forStudent", "", "Event must be for either student, parent, teacher or Vendor.");
		}
	}

	private void validateWithDatabase(Event event, Errors errors) {

		if(!StringUtils.isEmpty(event.getStandardName())){
			int standardId = masterValidator.getStandardIdByValidatingName(event.getStandardName(), event.getBranchId(), event.getAcademicYearId());
			if(standardId == 0){
				errors.rejectValue("standardName", "standard", "Standard not found.");
			}
			else {
				event.setStandardId(standardId);
			}
		}
		else {
			errors.rejectValue("standardName", "standard", "Standard is required.");
		}

		if(!StringUtils.isEmpty(event.getDivisionName())){
			int divisionId = masterValidator.getDivisionIdByValidatingName(event.getDivisionName(), event.getBranchId(), null);
			if(divisionId == 0){
				errors.rejectValue("divisionName", "division", "Division not found.");
			}
			else {
				event.setDivisionId(divisionId);
			}	
		}

		if(event.getSentByRoleId().equals(Constants.ROLE_ID_TEACHER) && event.getStandardId() != null && event.getDivisionId() != null){
			if(!masterValidator.isStandardAndDivisionApplicableForTeacher(event.getSentBy(), event.getStandardId(), event.getDivisionId())){
				errors.rejectValue("standardName", "standardOrDivision", "Standard or division does not belong to this teacher.");
			}
		}
	}

	private void validateRecurring(Event event, Errors errors) {
		if(!StringUtils.isEmpty(event.getReccuring()) && event.getReccuring().equalsIgnoreCase("Y")){
			String frequency = event.getFrequency();
			if(StringUtils.isEmpty(frequency)){
				errors.rejectValue("frequency", "error.frequency", "frequency is required when reccuring event.");
			}
			else {
				if(frequency.equalsIgnoreCase(FrequencyConstants.WEEKLY_FREQUENCY)){
					String byDay = event.getByDay();
					if(StringUtils.isEmpty(byDay)){
						errors.rejectValue("byDay", "by Day", "week days are required when frequency is weekly.");
					}
					else {
						String[] byDayArr = byDay.split(",");
						if(byDayArr != null){
							if(byDayArr.length > 7 || byDayArr.length <= 0){
								errors.rejectValue("frequency", "error.frequency", 
									"Event days must be at least one day and not more than 7 days");
							}
							Set<String> byDaySet = new HashSet<String>(Arrays.asList(byDayArr));
							if(byDaySet.size() != byDayArr.length){
								errors.rejectValue("byDay", "error.byDay", "Some days are repeated.");
							}
							for(String dayStr : byDayArr){
								try {
									Integer day = Integer.parseInt(dayStr);
									if(day > 7 || day < 1){
										errors.rejectValue("byDay", "error.byDay", 
											day + " - Invalid week day, cannot be less than 1 and greater than 7.");
									}	
								}
								catch(NumberFormatException e){
									e.printStackTrace();
									errors.rejectValue("byDay", "error.byDay", dayStr+" Invalid week Day.");
								}
							}
						}else {
							errors.rejectValue("byDay", "by Day", "week days are required when frequency is weekly.");
						}
					}
				}
			}
		}
	}


	private void validateDateTime(Event event, Errors errors) {

		Date startDate = (!StringUtils.isEmpty(event.getStartDate()) ? 
			dateutil.getDate(event.getStartDate(), errors, "startDate", "Start date") : null);

		Date endDate = (!StringUtils.isEmpty(event.getEndDate()) ? 
			dateutil.getDate(event.getEndDate(), errors, "endDate", "End Date") : null);
		
		if(startDate != null ){
			if(!DateUtils.isSameDay(startDate, today) && !startDate.after(today)){
				errors.rejectValue("startDate", "error.startDate", "Start Date must be after today.");
			}
			List<AcademicYear> academicYearList = academicYearDao.getActiveYear(event.getBranchId());
			for(AcademicYear academicYear : academicYearList){
				Date academicYearFromDate = dateutil.getDate(academicYear.getFromDate());
				Date academicYearToDate = dateutil.getDate(academicYear.getToDate());
				if(startDate.before(academicYearFromDate) || startDate.after(academicYearToDate)){
					errors.rejectValue("startDate", "error.startDate", "Please enter startdate in between academic year");
				}
				if(endDate.before(academicYearFromDate) || endDate.after(academicYearToDate)){
					errors.rejectValue("endDate", "error.endDate", "Please enter todate in between academic year");
				}
			}
		}
		if(endDate != null ){
			if(StringUtils.isEmpty(event.getEndDate()) && StringUtils.isEmpty(event.getReccuring()) 
					&& event.getReccuring().equalsIgnoreCase("N")){
				errors.rejectValue("endDate", "error.endDate", "Either end Date or reccurring is mandatory.");
			}
		}
		

		Time startTime = getTime(event.getStartTime(), errors, "startTime");
		Time endTime = getTime(event.getEndTime(), errors, "endTime");

		if(startDate != null && endDate != null){
			if(endDate.before(startDate)){
				errors.rejectValue("endDate", "error.endDate", "End date must be after start date.");	
			}
			else if( DateUtils.isSameDay(startDate, endDate) &&  startTime != null && endTime != null && (startTime.equals(endTime) 
					|| endTime.before(startTime))){
				errors.rejectValue("endTime", "error.endTime", 
						"End time must be after start time when start and end dates are same.");
			}
			event.setStartDate(Constants.DATABASE_DATE_FORMAT.format(startDate));
			event.setEndDate(Constants.DATABASE_DATE_FORMAT.format(endDate));
		}
	}


	private Time getTime(String timeStr, Errors errors, String fieldName) {
		Time time = null;
		try {
			time = new Time(timeFormat.parse(timeStr).getTime());
		}catch(ParseException e){
			errors.rejectValue(fieldName, "errors."+fieldName, "Time Format not supported must be in (HH:mm:ss).");
			e.printStackTrace();
		}

		return time;
	}

}


