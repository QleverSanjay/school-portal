package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.dao.IAcademicYearDao;
import com.qfix.model.AcademicYear;
import com.qfix.model.DisplayTemplate;
import com.qfix.model.Fees;
import com.qfix.model.FeesCode;
import com.qfix.model.FeesRecurringDetail;
import com.qfix.model.LateFeesDetail;
import com.qfix.model.SchemeCode;
import com.qfix.utilities.Constants;
import com.qfix.utilities.DateUtil;

@Scope(value="prototype")
@Repository
public class FeesValidator extends JdbcDaoSupport implements Validator {
	

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Autowired
	MasterValidator masterValidator;

	@Autowired
	private IAcademicYearDao academicYearDao;


	private DateUtil dateUtil;
	private boolean isUpload;
	private List<Fees> feesList;
	private List<SchemeCode> schemeCodes;
	private List<LateFeesDetail>  lateFeesList;
	private List<DisplayTemplate>  templateList;
	private List<FeesCode>  feesCodeList;
	private int index;
	boolean isUploadUpdate;
	Map<String, List<Fees>> feesMap;

	public FeesValidator(){
		dateUtil = new DateUtil();
	}

	public void setIndex(int index){
		this.index = index;
	}

	public void initialize(boolean isUpload, boolean isUploadUpdate, List<Fees> feesList, List<SchemeCode> schemeCodes,
			List<LateFeesDetail> lateFeesList, List<DisplayTemplate> templateList, List<FeesCode> feesCodeList, Map<String, List<Fees>> studentMap) {
		this.isUpload = isUpload;
		this.feesList = feesList;
		this.schemeCodes = schemeCodes;
		this.lateFeesList = lateFeesList;
		this.templateList = templateList;
		this.isUploadUpdate = isUploadUpdate;
		this.feesCodeList = feesCodeList;
		this.feesMap = studentMap;
	}

	public boolean supports(Class clazz) {
		return Fees.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		Fees fees = (Fees) target;
		/*make reminder as option in case uploading fees*/
		if(StringUtils.isEmpty(fees.getIsEmail())){
			fees.setIsEmail("N");
		}
		if(StringUtils.isEmpty(fees.getIsSms())){
			fees.setIsSms("N");
		}
		if(StringUtils.isEmpty(fees.getIsNotification())){
			fees.setIsNotification("N");
		}
		if(StringUtils.isEmpty(fees.getFeeType())){
			fees.setFeeType("SINGLE FEE");
		}
		if(StringUtils.isEmpty(fees.getIsPartialPaymentAllowed())){
			fees.setIsPartialPaymentAllowed("N");
		}
		if(StringUtils.isEmpty(fees.getIsSplitPayment())){
			fees.setIsSplitPayment("N");
		}

		if(StringUtils.isEmpty(fees.getAllowRepeatEntries())){
			fees.setAllowRepeatEntries("N");
		}

		if(StringUtils.isEmpty(fees.getCurrency())){
			fees.setCurrency("INR");
		}
		if(StringUtils.isEmpty(fees.getIsAllowUserEnterAmt())){
			
			fees.setIsAllowUserEnterAmt("N");
		}
		
		if("Y".equals(fees.getRecurring())){
			validateRecurring(fees, errors);
		}
		else {
			fees.setRecurring("N");
		}

/*		if("All".equalsIgnoreCase(fees.getStandard())){
			fees.setStandard(null);
		}

		if("All".equalsIgnoreCase(fees.getDivision())){
			fees.setDivision(null);
		}

		if("All".equalsIgnoreCase(fees.getCaste())){
			fees.setCaste(null);
		}

		if("All".equalsIgnoreCase(fees.getFeesCodeName())){
			fees.setFeesCodeName(null);
		}
*/
		/*make reminder as optional in case uploading fees*/
		validateNotoficationType(fees, errors);
		validateDates(fees, errors);
		try {
			if(isUpload || isUploadUpdate){
				if(isUploadUpdate){
					validateIdsAndActions(fees, errors);
				}
				validateWithDatabase(fees, errors);
				validateStudentNameRollNoRegistrationNumber(fees, errors);
				validateWithUploadedList(fees, errors);
			}
			else {
				validateFeesType(fees, errors);
				validateLateFeeCharges(fees, errors);
				if(fees.getFeesCodeId() != null && fees.getFeesCodeId() != 0){
					validateFeesCode(fees.getFeesCodeId(), fees.getBranchId(), errors);
				}
			}

			validateOthers(fees, errors, isUpload, schemeCodes);
			//(fees, errors);
			
			if(!StringUtils.isEmpty(fees.getAmount())){
				int amount = Integer.parseInt(fees.getAmount());
				if(amount == 0 ){
					validateFeesWithZeroAmount(fees, errors);
				}
			}
			
			if(fees.getStudentId()  != null && fees.getStudentId() != 0){
				validateStudentSpecific(fees, errors);
			}
		} catch (SQLException e) {
			errors.rejectValue("title", "error.all", "Problem while validating records.");
			e.printStackTrace();
		}
	}

	// Validate Fees when fees amount is specified as 0
	private void validateFeesWithZeroAmount(Fees fees, Errors errors) {
		if(fees.getLateFeeDetailId() != null && fees.getLateFeeDetailId() != 0 ){
			errors.rejectValue("", "errors.lateFeeNotAllowed", "Late fine cannot be applied on 0 amount fees.");
		}

		if("Y".equals(fees.getRecurring())){
			errors.rejectValue("", "errors.recurringNotAllowed", "Recurring not allowed with 0 amount fees.");
		}
		
		if(!StringUtils.isEmpty(fees.getReminderStartDate()) || 
				("Y".equals(fees.getIsEmail())  ||  "Y".equals(fees.getIsSms()) || "Y".equals(fees.getIsNotification()))){

			errors.rejectValue("", "errors.reminderNotAllowed", "Reminders are not allowed with 0 amount fees.");
		}
	}

	private void validateIdsAndActions(Fees fees, Errors errors) {
		String action = fees.getFeesAction();
		if(fees.getId() != null){
			String sql = "select count(*) as count from fees where id = " + fees.getId() + " AND branch_id = " + fees.getBranchId();
			Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
			if(count == 0){
				errors.rejectValue("feesAction", "errors.feesAction", "Fees Id is not present.");
			}
			else if("Update".equalsIgnoreCase(action)){
				feesMap.get("Update").add(fees);
			}
			else if("Delete".equalsIgnoreCase(action)){
				feesMap.get("Delete").add(fees);
			}
			else if("Add".equalsIgnoreCase(action)){
				errors.rejectValue("feesAction", "errors.feesAction", "This fees is already present cannot be added.");
			}
		}
		else if("Add".equalsIgnoreCase(action)){
			feesMap.get("Add").add(fees);
		}
		else if("Update".equalsIgnoreCase(action) || "Delete".equalsIgnoreCase(action)){
			errors.rejectValue("feesAction", "errors.feesAction", "Fees Id is required to update or delete.");
		}
	}

	private void prepareRecurring(Fees fees) {
		Map<String, Boolean> eventWeekDays = fees.getRecurringDetail().getByDays();
		if(eventWeekDays != null){
			int count = 0;
			String byDay = "";
			for (String day : eventWeekDays.keySet()) {
				if (eventWeekDays.get(day).equals(true)) {
					count++;
					byDay = byDay + (count != 1 ? "," : "") + day;
				}
			}
			fees.getRecurringDetail().setWorkWeeks(byDay);
		}
	}

	private void validateLateFeeCharges(Fees fees, Errors errors) {
		if(fees.getLateFeeDetailId() != null && !fees.getLateFeeDetailId().equals(0)){
			String sql = "select count(*) as count from late_fees_payment_detail " +
					" where id = "+fees.getLateFeeDetailId() + 
					" AND is_delete = 'N' AND branch_id = " + fees.getBranchId();
			Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
			if(count == 0){
				errors.rejectValue("", "error.invalidaLateFeeId", "Invalid late fee provided.");
			}
		}
	}

	private void validateFeesType(final Fees fees, final Errors errors) {
		if(Constants.FEE_TYPE.GROUP_FEE.get().equals(fees.getFeeType())){
			if(fees.getDisplayTemplateId() != null){

				String sql = "select count(*) as count, sum(dth.amount) as total_amount " +
						" from display_template as dt INNER JOIN display_template_head as dth on dth.display_template_id = dt.id " +
						" where dt.id = "+fees.getDisplayTemplateId()+" AND " +
						" dt.branch_id = " + fees.getBranchId() + " AND dt.is_delete = 'N'";

				getJdbcTemplate().query(sql, new RowMapper<Object>(){
					@Override
					public Object mapRow(ResultSet rs, int arg1)
							throws SQLException {

						if(rs.getInt("count") <= 0){
							errors.rejectValue("", "error.invalidDisplayTemplate", "Invalid display template provided.");
						}

						if(!StringUtils.isEmpty(fees.getAmount()) && 
								rs.getInt("total_amount") != Double.parseDouble(fees.getAmount())){
							errors.rejectValue("", "error.invalidaLateFeeId", 
									"Sum of breakdown amount must be equal to fees amount.");
						}
						return null;
					}
				});
			}else {
				errors.rejectValue("", "error.displayTemplateRequired", "Display Template is required when group fees.");
			}
		}
	}

	private void validateRecurring(Fees fees, Errors errors) {
		FeesRecurringDetail recurring = fees.getRecurringDetail();

		if(Constants.FREQUENCY.DAILY.get().equals(recurring.getFrequency())){
			prepareRecurring(fees);
			String byDays = recurring.getWorkWeeks();
			if(StringUtils.isEmpty(byDays)){
				errors.rejectValue("", "by Day", "Work week is required when frequency is Daily.");
			}
			else {
				String[] byDayArr = byDays.split(",");
				if(byDayArr != null){
					if(byDayArr.length > 7 || byDayArr.length <= 0){
						errors.rejectValue("", "error.byDaysDirty", 
							"Work week days must be at least one day and not more than 7 days");
					}
					Set<String> byDaySet = new HashSet<String>(Arrays.asList(byDayArr));
					if(byDaySet.size() != byDayArr.length){
						errors.rejectValue("byDay", "error.byDaysRepeated", "Some days are repeated.");
					}
					for(String dayStr : byDayArr){
						try {
							Integer day = Integer.parseInt(dayStr);
							if(day > 7 || day < 1){
								errors.rejectValue("", "error.byDaysInvalid", 
									day + " - Invalid week day, cannot be less than 1 and greater than 7.");
							}	
						}
						catch(NumberFormatException e){
							e.printStackTrace();
							errors.rejectValue("byDay", "error.byDay", dayStr+" Invalid week Day.");
						}
					}
				}else if(StringUtils.isEmpty(recurring.getFrequency())){
					errors.rejectValue("byDay", "by Day", "week days are required when frequency is weekly.");
				}
			}
		}
	}

	private void validateStudentSpecific(Fees fees, Errors errors) {
		/*if(fees.getFeesCodeId() != null && fees.getFeesCodeId() != 0){
			validateFeesCode(fees.getStudentId(), fees.getFeesCodeId(), errors);
		}*/
		if(fees.getStandardId() != null && fees.getStandardId() != 0){
			validateStandard(fees.getStudentId(), fees.getStandardId(), errors);
		}
		if(fees.getDivisionId() != null && fees.getDivisionId() != 0){
			validateDivision(fees.getStudentId(), fees.getDivisionId(), errors);
		}
		if(fees.getCasteId() != null && fees.getCasteId() != 0){
			validateCaste(fees.getStudentId(), fees.getCasteId(), errors);
		}
	}

	private void validateStandard(Integer studentId, Integer standardId, Errors errors) {
		String sql = "select count(*) as count from student where id = "+studentId + " AND standard_id = "+standardId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count == 0){
			errors.rejectValue("", "error.standardNotForStudent", "Standard selected is not related with this student.");
		}
	}

	private void validateDivision(Integer studentId, Integer divisionId, Errors errors) {
		String sql = "select count(*) as count from student where id = "+studentId + " AND division_id = "+divisionId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count == 0){
			errors.rejectValue("", "error.divisionNotForStudent", "Division selected is not related with this student.");
		}
	}

	private void validateCaste(Integer studentId, Integer casteId, Errors errors) {
		String sql = "select count(*) as count from student where id = "+studentId + " AND caste_id = "+casteId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count == 0){
			errors.rejectValue("", "error.casteNotForStudent", "Caste selected is not related with this student.");
		}
	}

	private void validateFeesCode(Integer feesCodeId, Integer branchId, Errors errors) {
		String sql = " select count(*) as count from fees_code_configuration where id = "+feesCodeId 
				+ " AND is_delete = 'N' AND branch_id = " + branchId;
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if(count == 0){
			errors.rejectValue("", "error.feesCodeNotForStudent", "Fees code selected is not related with this branch.");
		}
	}

	private void validateOthers(Fees fees, Errors errors, boolean isUpload, List<SchemeCode> schemeCodes) {
		try {
			if(isUpload){
				validateWithSchemeCodes(fees, errors, schemeCodes);
			}

			double amount = (StringUtils.isEmpty(fees.getAmount()) ? 0.0 : Double.parseDouble(fees.getAmount()));
			if(amount < 0.0 ){
				errors.rejectValue("amount", "errors.amountRequired", "Amount is mandatory.");
			}

			if(!StringUtils.isEmpty(fees.getIsSplitPayment()) && "Y".equals(fees.getIsSplitPayment())){
				double seedSplitAmount = (StringUtils.isEmpty(fees.getSeedSplitAmount()) ? 0.0 : Double.parseDouble(fees.getSeedSplitAmount()));

				if(seedSplitAmount <= 0.0 ){
					errors.rejectValue("seedSplitAmount", "errors.seedSplitAmountRequired", "Seed split amount is mandatory when split payment.");
				}

				if(seedSplitAmount >= amount){
					errors.rejectValue("seedSplitAmount", "errors.splitAndSeedSplitNotEqual", "Seed split amount must be less than total amount.");
				}
			}
			else {
				fees.setIsSplitPayment("N");
			}

			if(StringUtils.isEmpty(fees.getIsPartialPaymentAllowed())){
				fees.setIsPartialPaymentAllowed("N");
			}

			if("Y".equals(fees.getIsSplitPayment()) && fees.getIsSplitPayment().equals(fees.getIsPartialPaymentAllowed())){
				errors.rejectValue("isSplitPayment", "errors.splitNotAllowedWithPartial", "Split payment not allowed with partial payment.");
			}

			if(fees.getStandardId() == null && fees.getStudentId() == null && fees.getFeesCodeId() == null){
				errors.rejectValue("standard", "errors.rollNO", "Fees should be for standard or for fees code or for student.");
			}
		}catch(Exception e){
			logger.error("Problem while validating split payment.", e);
		}
	}

	private void validateWithSchemeCodes(Fees fees, Errors errors, List<SchemeCode> schemeCodes) {
		boolean isSchemeCodeAvailable = true, isSeedSchemeCodeAvailable = true, isSplitPayment = "Y".equals(fees.getIsSplitPayment());

		if(schemeCodes == null || schemeCodes.isEmpty()){
			errors.rejectValue("schemeCode", "errors.schemeCodesNotFound", "Scheme codes not available for this branch.");
			return;
		}

		if(StringUtils.isEmpty(fees.getSchemeCode())){
			isSchemeCodeAvailable = false;
			errors.rejectValue("schemeCode", "errors.schemeCodeRequired", "Scheme codes is mandatory.");
		}

		if(isSplitPayment && StringUtils.isEmpty(fees.getSeedSchemeCode())){
			isSeedSchemeCodeAvailable = false;
			errors.rejectValue("seedSchemeCode", "errors.seedSchemeCodeRequired", "Seed scheme codes is mandatory when split payment.");
		}

		if((isSplitPayment && isSeedSchemeCodeAvailable) || isSchemeCodeAvailable){
			Integer schemeGatewayId = null, seedSchemeGatewayId = null;

			for(SchemeCode schemeCode : schemeCodes){
				if(isSchemeCodeAvailable && schemeCode.getName().equalsIgnoreCase(fees.getSchemeCode())){
					fees.setSchemeCodeId(schemeCode.getId());
					schemeGatewayId = schemeCode.getGatewayId();
				}
				if(isSplitPayment && isSeedSchemeCodeAvailable && schemeCode.getName().equalsIgnoreCase(fees.getSeedSchemeCode())){
					fees.setSeedSchemeCodeId(schemeCode.getId());
					seedSchemeGatewayId = schemeCode.getGatewayId();
				}
			}

			if(isSchemeCodeAvailable && fees.getSchemeCodeId() == null){
				errors.rejectValue("schemeCode", "errors.invalidSchemeCode", "Scheme code not found.");
			}

			if(isSplitPayment && isSeedSchemeCodeAvailable && fees.getSeedSchemeCodeId() == null){
				errors.rejectValue("seedSchemeCode", "errors.invalidSeedSchemeCode", "Seed scheme codes not found.");
			}

			if(seedSchemeGatewayId != null && schemeGatewayId != null && !seedSchemeGatewayId.equals(schemeGatewayId)) {
				errors.rejectValue("seedSchemeCode", "errors.splitNotPossible", "Split Payment cannot be possible with these scheme codes.");
			}

			if(isSplitPayment && fees.getSchemeCodeId() != null && fees.getSeedSchemeCodeId() != null 
					&& fees.getSchemeCodeId().equals(fees.getSeedSchemeCodeId())){
				errors.rejectValue("seedSchemeCode", "errors.schemeCodeAndSeedSchemeCodeSame", "Scheme code and seed scheme code must be different.");	
			}
		}
	}


	private void validateWithUploadedList(Fees currentFees, Errors errors) {
		try{
			for(int i = 0; i < feesList.size(); i++){
				if(i > index){
					Fees nextFees = feesList.get(i);
					if((StringUtils.isEmpty(currentFees.getStandard()) || currentFees.getStandard().equals(nextFees.getStandard())) 
						&& (StringUtils.isEmpty(currentFees.getCaste()) || currentFees.getCaste().equals(nextFees.getCaste())) 
						&& currentFees.getHead().equals(nextFees.getHead())){

						if((StringUtils.isEmpty(currentFees.getFeesCodeName()) && StringUtils.isEmpty(nextFees.getFeesCodeName())) || 
								nextFees.getFeesCodeName().equals(currentFees.getFeesCodeName())){

							if(currentFees.getFromDateD() != null && currentFees.getToDateD() != null &&
									nextFees.getFromDateD() != null && currentFees.getToDateD() != null){

								if((DateUtils.isSameDay(currentFees.getFromDateD(), nextFees.getFromDateD()) || 
									DateUtils.isSameDay(currentFees.getToDateD(), nextFees.getFromDateD()) || 
									DateUtils.isSameDay(currentFees.getFromDateD(), nextFees.getToDateD()) ||
									DateUtils.isSameDay(currentFees.getToDateD(), nextFees.getToDateD())) || 
									(currentFees.getFromDateD().after(nextFees.getFromDateD()) 
									&& currentFees.getFromDateD().before(nextFees.getToDateD()))){

									errors.rejectValue("standard", "error.exist", "Fee strcture is same as row "+(i + 2)+"");
								}
							}
						}
					}
				}
			}
		}catch(Exception e){
			logger.error("Problem while validating fees..", e);
		}
	}

	private void validateNotoficationType(Fees fees, Errors errors) {
		
		if(!StringUtils.isEmpty(fees.getReminderStartDate())){
			if((StringUtils.isEmpty(fees.getIsEmail()) || fees.getIsEmail().equals("N")) && 
					(StringUtils.isEmpty(fees.getIsSms()) || fees.getIsSms().equals("N")) && 
					(StringUtils.isEmpty(fees.getIsNotification()) || fees.getIsNotification().equals("N"))){

					errors.rejectValue("isEmail", "type Email", "Either email or sms or notification is required.");
				}
			else if(StringUtils.isEmpty(fees.getFrequency())){
				errors.rejectValue("frequency", "type frequency", "Frequency is required.");
				
			}
		}
		else if(!StringUtils.isEmpty(fees.getFrequency())){
			if((StringUtils.isEmpty(fees.getIsEmail()) || fees.getIsEmail().equals("N")) && 
					(StringUtils.isEmpty(fees.getIsSms()) || fees.getIsSms().equals("N")) && 
					(StringUtils.isEmpty(fees.getIsNotification()) || fees.getIsNotification().equals("N"))){

					errors.rejectValue("isEmail", "type Email", "Either email or sms or notification is required.");
				}
			else if(StringUtils.isEmpty(fees.getReminderStartDate())){
				errors.rejectValue("reminderStartDate", "type reminderStartDate", "reminder start date is required.");
			}
			
		}
		else if(!fees.getIsEmail().equals("N") || !fees.getIsSms().equals("N") || !fees.getIsNotification().equals("N")){
			 if(StringUtils.isEmpty(fees.getReminderStartDate())){
				errors.rejectValue("reminderStartDate", "type reminderStartDate", "reminder start date is required.");
				
			}
			 else if(StringUtils.isEmpty(fees.getFrequency())){
					errors.rejectValue("frequency", "type frequency", "Frequency is required.");
					
				}
		}
		
	}

	private void validateWithDatabase(Fees fees, Errors errors) throws SQLException {
		if(!StringUtils.isEmpty(fees.getStandard())){
			int standardId = masterValidator.getStandardIdByValidatingName(fees.getStandard(), fees.getBranchId(), fees.getAcademicYearId());
			if(standardId == 0){
				errors.rejectValue("standard", "standard", "Standard not found.");
			}
			else {
				fees.setStandardId(standardId);
			}	
		}

		if(!StringUtils.isEmpty(fees.getDivision())){
			int divisionId = masterValidator.getDivisionIdByValidatingName(fees.getDivision(), fees.getBranchId(), fees.getAcademicYearId());
			if(divisionId == 0){
				errors.rejectValue("division", "division", "Division not found.");
			}
			else {
				fees.setDivisionId(divisionId);
			}	
		}

		if(!StringUtils.isEmpty(fees.getHead())){
			int headId = masterValidator.getHeadIdByValidatingName(fees.getHead(), fees.getBranchId());
			if(headId == 0){
				errors.rejectValue("head", "head", "Fees head not found.");
			}
			else {
				fees.setHeadId(headId);
			}	
		}
		else {
			errors.rejectValue("head", "head", "Fees head is required.");
		}

		if(!StringUtils.isEmpty(fees.getCaste())){
			int casteId = masterValidator.getCasteIdByValidatingName(fees.getCaste());
			if(casteId == 0){
				errors.rejectValue("caste", "caste", "Caste not found.");
			}
			else {
				fees.setCasteId(casteId);
			}
		}

		if(!StringUtils.isEmpty(fees.getFeesCodeName())){
			int feesCodeId =  0;
			for(FeesCode feesCode : feesCodeList){
				if(fees.getFeesCodeName().equals(feesCode.getName())){
					feesCodeId = feesCode.getId();
					break;
				}
			}
			if(feesCodeId == 0){
				errors.rejectValue("feesCodeName", "errors.feesCodeNotFound", "Fees code not found.");
			}
			else {
				fees.setFeesCodeId(feesCodeId);
			}
		}

		if(!StringUtils.isEmpty(fees.getLateFeeDetailName())){
			int lateFeesId =  0;
			for(LateFeesDetail lateFees : lateFeesList){
				if(fees.getLateFeeDetailName().equals(lateFees.getDescription())){
					lateFeesId = lateFees.getId();
					break;
				}
			}
			if(lateFeesId == 0){
				errors.rejectValue("lateFeeDetailId", "errors.lateFeesFound", "Late Fees not found.");
			}
			else {
				fees.setLateFeeDetailId(lateFeesId);
			}
		}
		if(!StringUtils.isEmpty(fees.getDisplayTemplateName())){
			int templateId =  0;
			for(DisplayTemplate template : templateList){
				if(fees.getDisplayTemplateName().equals(template.getName())){
					templateId = template.getId();
					break;
				}
			}
			if(templateId == 0){
				errors.rejectValue("feesCodeName", "errors.templateFound", "Display Template not found.");
			}
			else {
				fees.setDisplayTemplateId(templateId);
				validateFeesType(fees, errors);
			}
		}
	}

/*	private void feeStructureExist(Fees fees, Errors errors) throws SQLException{

		if(fees.getBranchId() != null && fees.getAcademicYearId() != null && fees.getCasteId() != null 
				&& fees.getStandardId() != null && fees.getFromDateD() != null && fees.getDueDateD() != null && fees.getToDateD() != null ){

			String sql = "select count(*) as count from fees where is_delete = 'N' AND branch_id = "+fees.getBranchId()
					+ " AND head_id = "+fees.getHeadId() 
					+ " AND academic_year_id = "+fees.getAcademicYearId()
					+ (fees.getStandardId() != null ? " AND standard_id = " + fees.getStandardId() : "")
					+ (fees.getDivisionId() != null ? " AND division_id = "+fees.getDivisionId()+" " : " AND division_id is null ")
					+ (fees.getCasteId() == null  ?  " AND caste_id is null " : " AND caste_id = "+fees.getCasteId() )
					+ (fees.getFeesCodeId() != null ? " AND fees_code_configuration_id = "+fees.getFeesCodeId() : "")
					+ (fees.getId() != null ? " AND id != "+fees.getId() : "")
					+ " AND (fromdate BETWEEN '"+ Constants.DATABASE_DATE_FORMAT.format(fees.getFromDateD())+"' AND " +
					"'"+ Constants.DATABASE_DATE_FORMAT.format(fees.getToDateD())+"')";

			logger.debug("fees structure exist sql --"+sql);

			int count = getJdbcTemplate().query(sql,  new RowMapper<Integer>(){
				@Override
				public Integer mapRow(ResultSet resultSet, int arg1)throws SQLException {
					
					return resultSet.getInt("count");
				}
			}).get(0);
			if(count > 0){
				errors.rejectValue("standard", "error.exist", "Fees structure already exist.");	
			}
		}
	}
*/
	private void validateDates(Fees fees, Errors errors) {
		Calendar calendar = Calendar.getInstance();
		Date today = new Date(calendar.getTimeInMillis());

		Date startDate = dateUtil.getDate(fees.getFromDate(), errors, "fromDate", "Start date");
		Date reminderStartDate = dateUtil.getDateAllowNull(fees.getReminderStartDate(), errors, "reminderStartDate", "Start date");
		Date endDate = dateUtil.getDate(fees.getToDate(), errors, "toDate", "End date");
		Date dueDate = dateUtil.getDate(fees.getDueDate(), errors, "dueDate", "Due date");

		if(dueDate != null && dueDate.before(today)){
			reminderStartDate = null;
			fees.setReminderStartDate(null);
			fees.setFrequency(null);
		}

		if(reminderStartDate != null){
			fees.setReminderStartDate(Constants.DATABASE_DATE_FORMAT.format(reminderStartDate));
			fees.setReminderStartDateD(reminderStartDate);
			if(fees.getId() == null && !DateUtils.isSameDay(reminderStartDate, today) && !reminderStartDate.after(today)){
				errors.rejectValue("reminderStartDate", "error.reminderStartDate", "Reminder Start Date must be after today.");
			}
		}

		if(startDate != null ){
			if(fees.getId() == null && !DateUtils.isSameDay(startDate, today) && !startDate.after(today) && "Y".equals(fees.getRecurring())){
				errors.rejectValue("fromDate", "error.fromDate", "Start Date must be after today.");
			}
		}

		if(startDate != null && endDate != null && dueDate != null){
			boolean isDateValid = true;
			if(endDate.before(startDate)){
				isDateValid = false;
				errors.rejectValue("fromDate", "error.fromDate", "End date must be after start date.");	
			}
			if(endDate.before(dueDate)){
				isDateValid = false;
				errors.rejectValue("toDate", "error.toDate", "End date must be after Due date.");
			}
			if(reminderStartDate!=null){
			if(dueDate.before(reminderStartDate) || DateUtils.isSameDay(reminderStartDate, dueDate)){
				isDateValid = false;
				if(reminderStartDate!=null){
				errors.rejectValue("reminderStartDate", "error.reminderStartDate", "Reminder start date must be before due date.");
				}
			}
			}
			
			/*due date calculation*/
			 
			if("Y".equals(fees.getRecurring()) && dueDate != null && startDate != null && endDate != null){
				Calendar c = Calendar.getInstance();
		        c.setTime(startDate);
		        System.out.println("FREQUENCY ::::::::::::::"+fees.getRecurringDetail().getFrequency());
				if(DateUtils.isSameDay(dueDate, startDate) || dueDate.before(startDate)){
					errors.rejectValue("", "dueDateError", "Due date must be on after start date when recurring.");
				}
				else if(dueDate.after(endDate)){
					errors.rejectValue("", "dueDateError", "Due date must be before end date when recurring.");
				}
				/*else if(Constants.FREQUENCY.DAILY.get().equals(fees.getRecurringDetail().getFrequency())){
					c.add(Calendar.DATE, 1);
					if(!DateUtils.isSameDay(c.getTime(), dueDate)){
						errors.rejectValue("", "dueDateError", "Due date must be next day when daily recurring.");
					}
				}
				else if(Constants.FREQUENCY.WEEKLY.get().equals(fees.getRecurringDetail().getFrequency())){
					c.add(Calendar.DATE, 7);
					if(!DateUtils.isSameDay(c.getTime(), dueDate) && !dueDate.before(c.getTime())){
						errors.rejectValue("", "dueDateError", "Due date must not be after 7 days from start date when weekly recurring.");
					}
				}
				else if(Constants.FREQUENCY.MONTHLY.get().equals(fees.getRecurringDetail().getFrequency())){
					c.add(Calendar.MONTH, 1);
					if(!DateUtils.isSameDay(c.getTime(), dueDate) && !dueDate.before(c.getTime())){
						errors.rejectValue("", "dueDateError", "Due date must not be afetr a month from start date when Monthly recurring.");
					}
				}
				else if(Constants.FREQUENCY.QUARTERLY.get().equals(fees.getRecurringDetail().getFrequency())){
					c.add(Calendar.MONTH, 3);
					System.out.println("DUEDATE:::::::::::::::"+dueDate);
					System.out.println(c.getTime());
					if(!DateUtils.isSameDay(c.getTime(), dueDate) && !dueDate.before(c.getTime())){
						errors.rejectValue("", "dueDateError", "Due date must not be after three month`s from start date when Quarterly recurring.");
					}
				}
				else if(Constants.FREQUENCY.HALF_YEARLY.get().equals(fees.getRecurringDetail().getFrequency())){
					c.add(Calendar.MONTH, 6);
					if(!DateUtils.isSameDay(c.getTime(), dueDate) && !dueDate.before(c.getTime())){
						errors.rejectValue("", "dueDateError", "Due date must not be after six month`s from start date when Half-Yearly recurring.");
					}
				}*/
				
				
			}
			
			
			/*due date calculation*/
			
			if(isDateValid){
				fees.setFromDateD(startDate);
				fees.setFromDate(Constants.DATABASE_DATE_FORMAT.format(startDate));
				fees.setToDateD(endDate);
				fees.setToDate(Constants.DATABASE_DATE_FORMAT.format(endDate));
				fees.setDueDateD(dueDate);
				fees.setDueDate(Constants.DATABASE_DATE_FORMAT.format(dueDate));
				AcademicYear year = academicYearDao.getYearById(fees.getAcademicYearId());
				Date academicYearFromDate;
				Date academicYearToDate;
				if(year != null){
					academicYearFromDate = dateUtil.getDate(year.getFromDate());
					academicYearToDate = dateUtil.getDate(year.getToDate());
					if(startDate.after(academicYearToDate) || startDate.before(academicYearFromDate)){
						errors.rejectValue("fromDate", "error.fromDate", "Please enter from date in between academic year.");
					}
					if(endDate.after(academicYearToDate) || endDate.before(academicYearFromDate)){
						errors.rejectValue("toDate", "error.toDate", "Please enter to date in between academic year.");
					}
					if(dueDate.after(academicYearToDate) || dueDate.before(academicYearFromDate)){
						errors.rejectValue("dueDate", "error.dueDate", "Please enter due date in between academic year.");
					}
				}
			}
		}
	}
	
	private void validateStudentNameRollNoRegistrationNumber(Fees fees, Errors errors){
		if(!StringUtils.isEmpty(fees.getStudentName()) || !StringUtils.isEmpty(fees.getRegistrationCode()) 
				|| !StringUtils.isEmpty(fees.getRollNo())){

			if(!StringUtils.isEmpty(fees.getRollNo()) || !StringUtils.isEmpty(fees.getRegistrationCode())){

				boolean validationPassed = true;
				if(StringUtils.isEmpty(fees.getStudentName())){
					errors.rejectValue("studentName", "errors.studentName", "Student Name is require");
					validationPassed = false;
				}
				if(!StringUtils.isEmpty(fees.getRollNo()) && StringUtils.isEmpty(fees.getDivision())){
					errors.rejectValue("division", "errors.division", "Division is require");
					validationPassed = false;
				}
				if(!StringUtils.isEmpty(fees.getRollNo()) && StringUtils.isEmpty(fees.getStandard())){
					errors.rejectValue("standard", "errors.standard", "Standard is require");
					validationPassed = false;
				}

				if(validationPassed){
					int studentId = 0;
					if(!StringUtils.isEmpty(fees.getRollNo())){
						studentId = masterValidator.getStudentIdByRollNumber(fees.getRollNo(), fees.getBranchId(),
							fees.getStandardId(), fees.getDivisionId());
						if(studentId == 0){
							errors.rejectValue("", "errors.rollNo", "Roll number not found.");
						}
					}
					else if(!StringUtils.isEmpty(fees.getRegistrationCode())){
						studentId = masterValidator.getStudentIdByRegNumber(fees.getRegistrationCode(), fees.getBranchId());
						if(studentId == 0){
							errors.rejectValue("", "errors.registrationNo", "Registration code not found.");
						}
					}
					fees.setStudentId(studentId);
				}
			}
			else{
				errors.rejectValue("", "errors.rollNO", "Student's Roll no or Registration code is require");
			}
		}
	}
}
