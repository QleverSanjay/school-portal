package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.Branch;
import com.qfix.model.BranchAdmin;


@Scope(value="prototype")
@Repository
public class BranchValidator extends JdbcDaoSupport implements Validator {
	

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	

	@Autowired
	MasterValidator masterValidator;

	private boolean isUpload;
	private List<Branch> branchList;
	private int index;

	public void setIndex(int index) {
		this.index = index;
	}

	public void setBranchList(List<Branch> branchList) {
		this.branchList = branchList;
	}

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}

	public boolean supports(Class clazz) {
		return Branch.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		Branch branch = (Branch) target;
		try {
			if (!StringUtils.isEmpty(branch.getName())) {
				if (isBranchNameUnique(branch)) {
					errors.rejectValue("name", "error.name",
							"Branch Name is already used.");
				}
			}
			if (isUpload) {
				validateWithUploadedList(branch, errors);
				validateForCsv(branch, errors);
			} else {
				validateBranchAdmins(branch, errors);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void validateBranchAdmins(Branch branch, Errors errors) {
		List<BranchAdmin> branchAdmins = branch.getBranchAdminList();
		if (branchAdmins != null && !branchAdmins.isEmpty()) {
			BranchAdmin previousAdmin = null;
			for (BranchAdmin admin : branchAdmins) {
				if (previousAdmin != null && previousAdmin != admin) {
					if (!StringUtils.isEmpty(previousAdmin.getEmail())
							&& previousAdmin.getEmail()
									.equals(admin.getEmail())) {
						errors.rejectValue("", "adminEmailRepeat",
								"Email is repeated for `" + admin.getFirstName() + "`");
					}
					if (!StringUtils.isEmpty(previousAdmin.getPrimaryContact())
							&& previousAdmin.getPrimaryContact().equals(
									admin.getPrimaryContact())) {
						errors.rejectValue("", "adminMobileRepeat", "Primary Contact is repeated for `"
										+ admin.getFirstName() + "`");
					}
				} else if (previousAdmin == null) {
					previousAdmin = admin;
				}
				checkAdminIsUniqueInDB(admin, errors);
			}
		}
	}

	private void checkAdminIsUniqueInDB(BranchAdmin admin, Errors errors) {
		String sql = "select count(*) as count from contact where email = '" + admin.getEmail()
				+ "' OR phone = '" + admin.getPrimaryContact() + "' "
				+ (admin.getId() != null ? " AND user_id != (select user_id from school_admin where id = "
						+ admin.getId() + ")" : "");

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			errors.rejectValue("", "adminEmailOrMobileRepeat",
					"Email or Primary Contact is already used for `" + admin.getFirstName() + "`");
		}
	}

	private void validateWithUploadedList(Branch currentBranch, Errors errors) {
		int i = 0;
		for (Branch nextBranch : branchList) {
			if (i > index) {
				if (!nextBranch.equals(currentBranch)) {
					System.out.println("Current Barcnh = "
							+ currentBranch.getName() + " :: Branch :: " + nextBranch.getName());
					if (!StringUtils.isEmpty(currentBranch.getName())
							&& currentBranch.getName().equals(nextBranch.getName())) {
						errors.rejectValue("name", "error.name",
								"Branch name is same in row " + (i + 2) 
								+ ", name = " + currentBranch.getName());
					}
				}
			}
			i++;
		}

		if (StringUtils.isEmpty(currentBranch.getArea())) {
			errors.rejectValue("area", "error.area", "Please enter area");
		}
		if (StringUtils.isEmpty(currentBranch.getAddressLine())) {
			errors.rejectValue("addressLine", "error.addressLine",
					"Please enter addressLine");
		}
	}

	private void validateForCsv(Branch branch, Errors errors)
			throws SQLException {

		int stateId = masterValidator.getStateIdByValidatingName(branch
				.getState());
		if (!StringUtils.isEmpty(branch.getState())) {
			if (stateId != 0) {
				branch.setStateId(stateId);
			} else {
				errors.rejectValue("state", "", "Please enter valid state.");
			}
		} else {
			errors.rejectValue("state", "", "Please enter state.");
		}
		int districtId = 0;
		if (!StringUtils.isEmpty(branch.getDistrict())) {
			districtId = masterValidator.getDistrictIdByValidatingName(
					branch.getDistrict(), stateId);
			if (districtId != 0) {
				branch.setDistrictId(districtId);
			} else {
				errors.rejectValue("district", "",
						"Please enter valid district.");
			}
		} else {
			errors.rejectValue("district", "", "Please enter district.");
		}

		if (!StringUtils.isEmpty(branch.getTaluka())) {
			int talukaId = masterValidator.getTalukaIdByValidatingName(
					branch.getTaluka(), districtId);
			if (talukaId != 0) {
				branch.setTalukaId(talukaId);
			} else {
				errors.rejectValue("taluka", "", "Please enter valid taluka.");
			}
		} else {
			errors.rejectValue("taluka", "", "Please enter taluka.");
		}
	}

	private boolean isBranchNameUnique(Branch branch) throws SQLException {
		String sql = "select count(*) as count from branch where name = '"
				+ branch.getName()
				+ "' AND institute_id = "
				+ branch.getInstituteId()
				+ (branch.getId() != null ? " AND id != " + branch.getId() : "");

		Integer count = getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1)
					throws SQLException {
				return resultSet.getInt("count");
			}
		}).get(0);
		return (count > 0);
	}
}