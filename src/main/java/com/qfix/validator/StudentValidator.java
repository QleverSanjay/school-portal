package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.Student;
import com.qfix.model.StudentParent;
import com.qfix.utilities.Constants;
import com.qfix.utilities.DateUtil;

@Scope(value="prototype")
@Repository
public class StudentValidator extends JdbcDaoSupport implements Validator {

	
	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	MasterValidator masterValidator;

	private boolean isUploadInsert;
	private boolean isUploadUpdate;

	private List<Student> studentList;
	private DateUtil dateUtil;
	private int index;

	private Map<String, Integer> STADARD_MAP, DIVISION_MAP, CASTE_MAP, FEES_CODE_MAP, INCOME_RANGE_MAP, PROFESSION_MAP;

	private Map<String, Integer> STATE_MAP;

	private Map<String, List<Student>> studentMap;

	private Map<Integer, Map<String, Integer>> DISTRICT_MAP_BY_STATE;

	private Map<Integer, Map<String, Integer>> TALUKA_MAP_BY_DISTRICT;

	public void setIndex(int index) {
		this.index = index;
	}

	public StudentValidator() {
		dateUtil = new DateUtil();
	}

	public boolean supports(Class<?> clazz) {
		return Student.class.isAssignableFrom(clazz);
	}

	public void setStudentList(List<Student> studentList) {
		this.studentList = studentList;
	}

	public void setUploadInsert(boolean isUploadInsert) {
		this.isUploadInsert = isUploadInsert;
	}

	public void setUploadUpdate(boolean isUploadUpdate) {
		this.isUploadUpdate = isUploadUpdate;
	}

	public void initializeData(Integer branchId, Map<String, Integer> STATE_MAP, Map<Integer, Map<String, Integer>> DISTRICT_MAP_BY_STATE, 
			Map<Integer, Map<String, Integer>> TALUKA_MAP_BY_DISTRICT, Map<String, Integer> INCOME_RANGE_MAP, 
			Map<String, Integer> PROFESSION_MAP, Map<String, List<Student>> studentMap){
		STADARD_MAP = masterValidator.getStandardsBasedOnBranchId(branchId);
		DIVISION_MAP = masterValidator.getDivisionsBasedOnBranchId(branchId);
		CASTE_MAP = masterValidator.getCastes();
		FEES_CODE_MAP = masterValidator.getFeesCodesBasedOnBranchId(branchId);
		this.INCOME_RANGE_MAP = INCOME_RANGE_MAP;
		this.PROFESSION_MAP = PROFESSION_MAP;
		this.STATE_MAP = STATE_MAP;
		this.DISTRICT_MAP_BY_STATE = DISTRICT_MAP_BY_STATE;
		this.TALUKA_MAP_BY_DISTRICT = TALUKA_MAP_BY_DISTRICT;
		this.studentMap = studentMap;
	}

	public void validate(Object target, Errors errors) {
		Student student = (Student) target;
		try {
			if(student.getId() == null){
				validateAndSetUserName(student, errors);
			}

			if (isUploadInsert || isUploadUpdate) {
				if(isUploadUpdate){
					validateIdsAndAction(student, errors);
				}
				validateAndFindDuplicateRecordsInUploadedList(student, errors);
				validateAndSetIdentifiers(student, errors);
				validateAddressFields(student, errors);
			}
			validateRequiredFields(student, errors);
			if(student.getStudentParentList() != null) {
				validateParentRecords(student.getStudentParentList(), errors);
			}
			checkStudentIsUnique(student, errors);
		} catch (SQLException e) {
			e.printStackTrace();
			errors.rejectValue("", "errors.exception", "Problem while validating student.");
		}
	}

	private void validateIdsAndAction(Student student, Errors errors) {
		String action = student.getStudentUploadAction();
		if(student.getId() != null){
			String sql = "select count(*) as count from student where id = " + student.getId();
			Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
			if(count == 0){
				errors.rejectValue("studentUploadAction", "errors.studentUploadAction", "Student Id is not present.");
			}
			else if("Update".equalsIgnoreCase(action)){
				studentMap.get("Update").add(student);
			}
			else if("Delete".equalsIgnoreCase(action)){
				studentMap.get("Delete").add(student);
			}
			else if("Add".equalsIgnoreCase(action)){
				errors.rejectValue("studentUploadAction", "errors.studentUploadAction", "This student is already present cannot be added.");
			}
		}
		else if("Add".equalsIgnoreCase(action)){
			studentMap.get("Add").add(student);
		}
		else if("Update".equalsIgnoreCase(action) || "Delete".equalsIgnoreCase(action)){
			errors.rejectValue("studentUploadAction", "errors.studentUploadAction", "Student Id is required to update or delete.");
		}
	}

	private String getRandomUserName(String firstName, String lastName) {
		String username = null;
		String n = RandomStringUtils.random(6, false, true);
		username = (firstName != null ? firstName : "")
				+ (lastName != null ? lastName : "") + n + "@eduqfix.com";

		username = username.replaceAll(" ", "");
		return username;
	}

	private void validateAndSetUserName(Student student, Errors errors) {
		System.out.println("inside validate UserId");
		String userId = StringUtils.isEmpty(student.getStudentUserId()) ? ""
				: student.getStudentUserId().toString();
		String emailAdddress = StringUtils.isEmpty(student.getEmailAddress()) ? ""
				: student.getEmailAddress().toString();

		/* Student Login */
		try {
			
				if (userId.isEmpty() && emailAdddress.isEmpty()) {
					userId = getRandomUserName(student.getFirstname(),
							student.getSurname());
				} else if (!emailAdddress.isEmpty() && userId.isEmpty()) {
					userId = emailAdddress;
				}

				String sql = "select count(*) from user where username='"
						+ userId + "'";
				Integer count = getJdbcTemplate().queryForObject(sql,
						Integer.class);
				if (count > 0) {
					userId = getRandomUserName(student.getFirstname(),
							student.getSurname());
				}
				student.setStudentUserId(userId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void validateParentRecords(List<StudentParent> parentList, Errors errors) {
		int count = 0;
		for (StudentParent parent : parentList) {
			String parentRelation = parent.getRelationId().equals(1) ? "Father"
					: parent.getRelationId().equals(2) ? "Mother"
							: "Guardian";
			count++;
			if (StringUtils.isEmpty(parent.getFirstname())) {
				errors.rejectValue("", "error.firstNameMandatory" + count,
						parentRelation + "`s First name is mandatory.");
			}
			if (StringUtils.isEmpty(parent.getLastname())) {
				errors.rejectValue("", "error.lastNameMandatory" + count,
						parentRelation + "`s Surname is mandatory.");
			}

			String action = parent.getUploadAction();
			if(parent.getId() != null && !StringUtils.isEmpty(action)){
				String sql = "select count(*) as count from parent_upload where id = " + parent.getId();
				Integer parentCount = getJdbcTemplate().queryForObject(sql, Integer.class);
				if(parentCount > 0){
					if("Add".equalsIgnoreCase(action)){
						errors.rejectValue("studentUploadAction", "errors.studentUploadAction", parentRelation + " is already present cannot be added.");
					}
				}
				else {
					errors.rejectValue("studentUploadAction", "errors.studentUploadAction", parentRelation + " Id is not present.");
				}
			}
			else if("Update".equalsIgnoreCase(action) || "Delete".equalsIgnoreCase(action)){
				errors.rejectValue("studentUploadAction", "errors.studentUploadAction", parentRelation + " Id is required to update or delete.");
			}
			validateParent(parent, errors);
		}
	}

	private void validateRequiredFields(Student student, Errors errors) {
		checkIfAtleastOneCreateLoginSpecified(student, errors);
		
		Date studentDateOfBirth = dateUtil.getDateAllowNull(
				student.getDateOfBirth(), errors, "dateOfBirth", "Date of Birth");
		if (studentDateOfBirth != null) {
			student.setDateOfBirth(Constants.DATABASE_DATE_FORMAT.format(studentDateOfBirth));
		}

		if (studentDateOfBirth != null && studentDateOfBirth.after(dateUtil.getTodayDate())) {
				errors.rejectValue("dateOfBirth", "error.dateOfBirth",
						"Date of birth cannot be in future, Your Entered Date of birth is "
								+ student.getDateOfBirth());
			
		}
	}

	private void checkIfAtleastOneCreateLoginSpecified(Student student,
			Errors errors) {
		boolean createLoginForEitherStudentOrParentFound = false;

		if ("Y".equals(student.getCreateStudentLogin())) {
			createLoginForEitherStudentOrParentFound = true;
		}
		else {
			student.setCreateStudentLogin("N");
		}

		List<StudentParent> parentList = student.getStudentParentList();
		if (parentList != null) {
			
			for (StudentParent parent : parentList) {
				
				if (!createLoginForEitherStudentOrParentFound && "Y".equals(parent.getCreateLogin())) {
					createLoginForEitherStudentOrParentFound = true;
				} else if (!"Y".equals(parent.getCreateLogin())) {
					parent.setCreateLogin("N");
				}
				
			}
		}

		if (!createLoginForEitherStudentOrParentFound) {
			errors.rejectValue("", "error.accountMandatoryFailed",
					"Atleast one account must be created.");
		}
	}

	private void validateParent(StudentParent parent, Errors errors) {
		int relation = parent.getRelationId();
		String name = (relation == 1) ? "Father" : (relation == 2) ? "Mother"
				: "Guardian";

		if (!StringUtils.isEmpty(parent.getProfession())) {
			if (PROFESSION_MAP.containsKey(parent.getProfession())) {
				parent.setProfessionId(PROFESSION_MAP.get(parent.getProfession()));
			} else {
				errors.rejectValue("", "profession", "Please select " + name
						+ " profession");
			}
		}

		if (!StringUtils.isEmpty(parent.getIncomeRange())) {
			if (INCOME_RANGE_MAP.containsKey(parent.getIncomeRange())) {
				parent.setIncomeRangeId(INCOME_RANGE_MAP.get(parent.getIncomeRange()));
			} else {
				errors.rejectValue("", "income", "Please select " + name
						+ " income range");
			}
		}

		Date parentBirthDate = dateUtil.getDateAllowNull(parent.getDob(),
				errors, "", "Date of Birth");
		if (!StringUtils.isEmpty(parentBirthDate)) {
			parent.setDob(Constants.DATABASE_DATE_FORMAT
					.format(parentBirthDate));
			if (parentBirthDate.after(dateUtil.getTodayDate())) {
				errors.rejectValue("", "dateOfBirthD",
						"Date of Birth is in future, your entered date of birth is "
								+ parent.getDateOfBirthD());
			}
		}
		if(!isUploadUpdate || "Add".equals(parent.getUploadAction())){
			checkAndAssignUserIdIFParentNotExist(parent, errors);
		}
	}


	private void checkAndAssignUserIdIFParentNotExist(final StudentParent parent,
			final Errors errors) {

		String creaParentLogin = parent.getCreateLogin();
		if ("Y".equals(creaParentLogin)) {
			String parentUserId = StringUtils.isEmpty(parent
					.getParentUserId()) ? "" : parent.getParentUserId().toString();

			String email = StringUtils.isEmpty(parent.getEmail()) ? ""
					: parent.getEmail().toString();
			boolean createRandom = false;
			if (parentUserId.isEmpty() && email.isEmpty()) {
				createRandom = true;
			} else if (!email.isEmpty() && parentUserId.isEmpty()) {
				parentUserId = email;
			}
			if(!StringUtils.isEmpty(parentUserId)){
				String sql = "select count(*) from user where username = '" + parentUserId + "'";
				Integer count = getJdbcTemplate().queryForObject(sql,
						Integer.class);
				if (count > 0) {
					createRandom = true;
				}
			}
			if (createRandom) {
				parentUserId = getRandomUserName(parent.getFirstname(),
						parent.getLastname());
				parent.setParentUserId(parentUserId);
			}
		}
	}

	private void validateAddressFields(Student student, Errors errors)
			throws SQLException {
		int stateId = 0;
		if (!StringUtils.isEmpty(student.getState())) {
			if (STATE_MAP.containsKey(student.getState())) {
				stateId = STATE_MAP.get(student.getState());
				student.setStateId(STATE_MAP.get(student.getState()));
			} else {
				errors.rejectValue("state", "", "Please enter valid state.");
			}
		} 
		int districtId = 0;
		if (!StringUtils.isEmpty(student.getDistrict()) && stateId != 0) {
			Map<String,Integer> districts = DISTRICT_MAP_BY_STATE.get(stateId);

			if (districts != null && districts.containsKey(student.getDistrict())) {
				districtId = districts.get(student.getDistrict());
				student.setDistrictId(districts.get(student.getDistrict()));
			} else {
				errors.rejectValue("district", "", "Please enter valid district.");
			}
		}

		if (!StringUtils.isEmpty(student.getTaluka()) && districtId != 0) {
			Map<String,Integer> talukas = TALUKA_MAP_BY_DISTRICT.get(districtId);
			if (talukas != null && talukas.containsKey(student.getTaluka())) {
				student.setTalukaId(talukas.get(student.getTaluka()));
			} else {
				errors.rejectValue("taluka", "", "Please enter valid taluka.");
			}
		}
	}

	private void validateAndFindDuplicateRecordsInUploadedList(Student currentStudent, Errors errors) {
		for (int i = index +1; i < studentList.size(); i++) {
			Student nextStudent = studentList.get(i);
			checkStudentsAreSame(currentStudent, nextStudent, errors, i);
		}
	}

	/**
	 * checks whether two students are same.
	 * 
	 * @param currentStudent
	 * @param nextStudent
	 * @param errors
	 * @return
	 */
	private void checkStudentsAreSame(Student currentStudent,
			Student nextStudent, Errors errors, int row) {

		try {
			if (currentStudent.hashCode() == nextStudent.hashCode()) {
				errors.rejectValue("userId", "error.all",
						"Student is same as row " + (row + 2));
			} else {
				if (!StringUtils.isEmpty(currentStudent.getRegistrationCode())
						&& currentStudent.getRegistrationCode().equals(
								nextStudent.getRegistrationCode())) {

					errors.rejectValue(
							"registrationCode",
							"error.registrationCode",
							"Registration code is same as row " + (row + 2)
									+ ", code = "
									+ currentStudent.getRegistrationCode());
				}
				if (!StringUtils.isEmpty(currentStudent.getStudentUserId())
						&& currentStudent.getStudentUserId().equals(
								nextStudent.getStudentUserId())) {
					errors.rejectValue(
							"studentUserId",
							"error.studentUserId",
							"Student userId is same as row " + (row + 2)
									+ ", studentUserId = "
									+ currentStudent.getStudentUserId());

				}

				if (!StringUtils.isEmpty(currentStudent.getRollNumber())
						&& currentStudent.getRollNumber().equals(
								nextStudent.getRollNumber())
						&& !StringUtils.isEmpty(currentStudent.getStandardName()) && currentStudent.getStandardName().equals(
								nextStudent.getStandardName())
						&& !StringUtils.isEmpty(currentStudent.getDivisionName()) && currentStudent.getDivisionName().equals(
								nextStudent.getDivisionName())) {

					errors.rejectValue(
							"rollNumber",
							"error.rollNumber",
							"Roll number is same as row " + (row + 2)
									+ ", code = "
									+ currentStudent.getRollNumber());
				}
			}
		} catch (Exception e) {
			logger.error("Exception while checking studenrts..", e);
			errors.rejectValue("", "error.problemValidating",
					"Problem while validating student data");
		}
	}

	private void validateAndSetIdentifiers(Student student, Errors errors) {

		if (!StringUtils.isEmpty(student.getStandardName())) {
			if (STADARD_MAP.containsKey(student.getStandardName())) {
				student.setStandardId(STADARD_MAP.get(student.getStandardName()));
			} else {
				errors.rejectValue("standardName", "standard", "Standard not found.");
			}
		}
		if (!StringUtils.isEmpty(student.getDivisionName())) {
//			System.out.println(DIVISION_MAP +" \n Division:::::?>?>>>>" + student.getDivisionName());
			String standardDivisionMapKey = student.getDivisionName() + "-" + student.getStandardName();
			if (DIVISION_MAP.containsKey( standardDivisionMapKey )) {
				student.setDivisionId(DIVISION_MAP.get( standardDivisionMapKey ));
			} else {
				errors.rejectValue("divisionName", "division", "Division not found.");
			}
		}
		if (!StringUtils.isEmpty(student.getCaste())) {
			if (CASTE_MAP.containsKey(student.getCaste())) {
				student.setCasteId(CASTE_MAP.get(student.getCaste()));
			} else {
				errors.rejectValue("caste", "caste", "Caste not found.");
			}
		}

		System.out.println(student.getFeesCodeName());
		if (!StringUtils.isEmpty(student.getFeesCodeName())) {
			if (FEES_CODE_MAP.containsKey(student.getFeesCodeName())) {
				List<Integer> feesCodes = new ArrayList<Integer>();
				feesCodes.add(FEES_CODE_MAP.get(student.getFeesCodeName()));
				student.setFeesCodeId(feesCodes);
			} else {
				errors.rejectValue("feesCodeName", "errors.feesCodeNotFound", "Fees code not found.");
			}
		}
	}

	private void checkStudentIsUnique(Student student, final Errors errors) {
		// Check name email and mobile is unique.
		String sql = null;
		/* String userName = student.getEmailAddress(); */

		sql = "select count(*) as count from student_upload where hash_code = "
				+ student.hashCode()
				+ (student.getId() != null ? " AND id != (select student_upload_id from student where id = "
						+ student.getId() + ")"
						: "") + " AND isDelete = 'N'";

		System.out.println("Check student with db SQL :::: \n" + sql);
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			errors.rejectValue("UserId", "error.userId",
					"Student with this username and contact details already exist.");
		}

		if (count.intValue() == 0) {
			if(!StringUtils.isEmpty(student.getFirstname())
				&& !StringUtils.isEmpty(student.getSurname())
				&& (!StringUtils.isEmpty(student.getStudentUserId()))){
				
				String userName = student.getStudentUserId() == null ? "" : student.getStudentUserId();

				if (StringUtils.isEmpty(userName)) {
					userName = student.getEmailAddress();
					if(StringUtils.isEmpty(userName)){
						userName = getRandomUserName(student.getFirstname(), student.getSurname());
					}
				}
				// Check userName is unique.
				sql = "select count(*) as count from user where username = '"
						+ userName
						+ "'"
						+ (student.getId() != null ? " AND id != (select user_id from student where id = "
								+ student.getId() + ")"
								: "");

				count = getJdbcTemplate().queryForObject(sql, Integer.class);
				if (count > 0) {
					userName = getRandomUserName(student.getFirstname(), student.getSurname());
				}
				student.setUsername(userName);
			}

			if (!StringUtils.isEmpty(student.getRollNumber())) {
				// Check rollNumber is unique.
				if (student.getStandardId() == null
						|| student.getDivisionId() == null) {
					errors.rejectValue("", "error.stdDivmandatoryWhenRollNum",
							"Standard and division is mandatory when roll number is specified.");
				} else {
					sql = "select count(*) as count from student where roll_number = '"
							+ student.getRollNumber()
							+ "' AND branch_id = "
							+ student.getBranchId()
							+ " AND standard_id = "
							+ student.getStandardId()
							+ " AND division_id = "
							+ student.getDivisionId()
							+ " AND active = 'Y' AND isDelete = 'N' "
							+ (student.getId() != null ? " AND id != "
									+ student.getId() + "" : "");

					getJdbcTemplate().query(sql, new RowMapper<Integer>() {
						@Override
						public Integer mapRow(ResultSet resultSet, int arg1)
								throws SQLException {
							if (resultSet.getInt("count") > 0) {
								errors.rejectValue("rollNumber",
										"error.rollNumber",
										"Student with this roll number already exist.");
							}
							return null;
						}
					});
				}
			}

			if (!StringUtils.isEmpty(student.getRegistrationCode())) {
				// Check registration code is unique.
				sql = "select count(*) as count from student where registration_code = '"
						+ student.getRegistrationCode()
						+ "' AND branch_id = "
						+ student.getBranchId()
						+ " AND active = 'Y' AND isDelete = 'N' "
						+ (student.getId() != null ? " AND id != "
								+ student.getId() + "" : "");

				getJdbcTemplate().query(sql, new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						if (resultSet.getInt("count") > 0) {
							errors.rejectValue("studentRegistrationId",
									"error.mobile",
									"Student with this registration Code already exist.");
						}
						return null;
					}
				});
			}
		}

	}
}