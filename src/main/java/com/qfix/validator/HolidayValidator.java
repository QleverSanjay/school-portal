package com.qfix.validator;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.dao.IAcademicYearDao;
import com.qfix.model.AcademicYear;
import com.qfix.model.Holiday;
import com.qfix.utilities.Constants;
import com.qfix.utilities.DateUtil;

@Scope(value="prototype")
@Repository
public class HolidayValidator extends JdbcDaoSupport implements Validator {


	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	private IAcademicYearDao academicYearDao;
	
	private DateUtil dateUtil;
	private boolean isUpload;
	private List<Holiday> holidayList;
	private Date fromDate;
	private Date toDate;
	private int index;

	public void setIndex(int index){
		this.index = index;
	}

	public HolidayValidator(){
		dateUtil = new DateUtil();
	}

	public void setHolidayList(List<Holiday> feesList) {
		this.holidayList = feesList;
	}

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}

	public boolean supports(Class clazz) {
		return Holiday.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		Holiday holiday = (Holiday) target;
		List<AcademicYear> academicYearList = academicYearDao.getActiveYear(holiday.getBranchId());
		validateDates(holiday, errors, academicYearList);

		try {
			if(isUpload){
				validateWithUploadedList(holiday, errors);
			}
			holidayExist(holiday, errors);
		} catch (SQLException e) {
			errors.rejectValue("title", "error.all", "Problem while validating records.");
			e.printStackTrace();
		}
	}

	private void validateWithUploadedList(Holiday currentHoliday, Errors errors) {
		try{

			Date currentFromDate = (StringUtils.isEmpty(currentHoliday.getFromDate()) ? null : dateUtil.getDate(currentHoliday.getFromDate()));
			Date currentToDate = (StringUtils.isEmpty(currentHoliday.getToDate()) ? null : dateUtil.getDate(currentHoliday.getToDate()));

			if(currentFromDate != null && currentToDate != null){
				for(int i = 0; i < holidayList.size(); i++){
					if(i > index){
						Holiday nextHoliday = holidayList.get(i);
						Date nextFromDate = (StringUtils.isEmpty(nextHoliday.getFromDate()) ? null : dateUtil.getDate(nextHoliday.getFromDate()));
						Date nextToDate = (StringUtils.isEmpty(nextHoliday.getToDate()) ? null : dateUtil.getDate(nextHoliday.getToDate()));
						
						if(nextFromDate != null && nextToDate != null){

							if((currentFromDate.after(nextFromDate) && currentFromDate.before(nextToDate) || DateUtils.isSameDay(currentFromDate, nextFromDate) || DateUtils.isSameDay(currentFromDate, nextToDate))){
								errors.rejectValue("fromDate", "error.same.fromDate", "Holiday for this duration is same as row "+(i + 2)+"");
							}
							else if((currentToDate.after(nextFromDate) && currentToDate.before(nextToDate) || DateUtils.isSameDay(currentToDate, nextFromDate) || DateUtils.isSameDay(currentToDate, nextToDate))){
								errors.rejectValue("toDate", "error.same.toDate", "Holiday for this duration is same as row "+(i + 2)+"");
							}
						}
						if(!StringUtils.isEmpty(currentHoliday.getTitle())){
							if(nextHoliday.getTitle() .equals(currentHoliday.getTitle())){
								errors.rejectValue("title", "error.same.title", "Title is same as row "+(i + 2)+"");
							}
						}
						else{
							errors.rejectValue("title", "error.same.title", "Title is require for this row "+(i + 2)+"");
						}
					}
				}
			}

			if(StringUtils.isEmpty(currentHoliday.getFromDate())){
				errors.rejectValue("fromDate", "error.same.title", "From date is required");
			}
			if(StringUtils.isEmpty(currentHoliday.getToDate())){
				errors.rejectValue("toDate", "error.same.title", "To date is required");
			}
			
			/*if(currentFromDate != null && currentToDate != null){
				for(AcademicYear academicYear : academicYearList){
					
					Date academicFromDate = Constants.DATABASE_DATE_FORMAT.parse(academicYear.getFromDate());
					Date academicToDate = Constants.DATABASE_DATE_FORMAT.parse(academicYear.getToDate());
					
					if(currentFromDate.before(academicFromDate) || currentFromDate.after(academicToDate)){
						errors.rejectValue("fromDate", "error.dateBrforeAcademicDate","Please enter from date in between Academic Year.");
					}
					if(currentToDate.before(academicFromDate) || currentToDate.after(academicToDate)){
						errors.rejectValue("toDate", "error.dateAfterAcademicYearDate","Please enter to date in between Academic Year.");
					}
				}
			}*/
		}catch(Exception e){
			logger.error("Problem while validating Holiday..", e);
		}
	}


	private void holidayExist(Holiday holiday, Errors errors) throws SQLException{
		
		
		if(fromDate != null && toDate != null){
			String sql = "select count(*) as count from holidays where " +
				     "((from_date <= '" + new java.sql.Date(fromDate.getTime()) +
				     "' AND to_date >= '"+ new java.sql.Date(fromDate.getTime()) +"' ) OR (from_date <= '"+ new java.sql.Date(toDate.getTime())+
			     	 "' AND to_date >= '"+ new java.sql.Date(toDate.getTime()) +"'))" +
				     " AND current_academic_year = "+holiday.getCurrentAcademicYearId()+ " AND " +
				     "branch_id = "+holiday.getBranchId()+
				     (holiday.getId() != null ? " AND id != "+holiday.getId() : "");

			Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
			if(count > 0){
				errors.rejectValue("fromDate", "error.dates", "Holiday for this duration is already exist.");	
			}
			if(!StringUtils.isEmpty(holiday.getTitle())){
				String holidayTitle = holiday.getTitle().replace("'", "''");
				sql = "select count(*) as count from holidays where title = '"+holidayTitle+"'" +
					     " AND current_academic_year = "+holiday.getCurrentAcademicYearId()+ " AND " +
					     "branch_id = "+holiday.getBranchId()+
					     (holiday.getId() != null ? " AND id != "+holiday.getId() : "");
				
				count = getJdbcTemplate().queryForObject(sql, Integer.class);
			}
			if(count > 0){
				errors.rejectValue("title", "error.title", "Holiday by this title is already exist.");	
			}	
		}
	}


	private void validateDates(Holiday holiday, Errors errors, List<AcademicYear> academicYearList) {

		fromDate = (!StringUtils.isEmpty(holiday.getFromDate()) ? 
			dateUtil.getDate(holiday.getFromDate(), errors, "fromDate", "Start Date") : null);

		if(fromDate != null ){
			holiday.setFromDate(Constants.DATABASE_DATE_FORMAT.format(fromDate));
		}

		toDate = (!StringUtils.isEmpty(holiday.getToDate()) ? dateUtil.getDate(holiday.getToDate(), errors, "toDate", "To Date") : null);

		if(toDate != null ){
			holiday.setToDate(Constants.DATABASE_DATE_FORMAT.format(toDate));
			if(fromDate != null){
				System.out.println("Going inside----Is Start Date Before ----"+toDate.before(fromDate));
				if(toDate.before(fromDate)){
					errors.rejectValue("toDate", "error.toDate", "To date must be after start date.");	
				}
			}
		}

		if(academicYearList != null && academicYearList.size() > 0){
			for(AcademicYear academicYear : academicYearList){
				if("Y".equals(academicYear.getIsCurrentActiveYear())){
					try {
						Date academicYearFromDate = Constants.DATABASE_DATE_FORMAT.parse(academicYear.getFromDate());
						Date academicYearToDate = Constants.DATABASE_DATE_FORMAT.parse(academicYear.getToDate());
						if(fromDate != null && fromDate.before(academicYearFromDate)){
							errors.rejectValue("fromDate", "error.fromDate", "From Date must be between academic year start and end.");
							System.out.println("195::::::::::::::::::::::::::::");
						}
						else if(fromDate != null && fromDate.after(academicYearToDate)){
							errors.rejectValue("fromDate", "error.fromDate", "From Date must be between academic year start and end.");
							System.out.println("199::::::::::::::::::::::::::::");
						}
						else if(toDate != null && toDate.after(academicYearToDate)){
							errors.rejectValue("fromDate", "error.fromDate", "To Date must be between academic year start and end.");
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	
}


