package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

//@EnableCaching
//@CacheConfig(cacheManager = "cacheManager")
@Repository
public class MasterValidator extends JdbcDaoSupport {

	@Autowired
	private DriverManagerDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	public int getStandardIdByValidatingCode(String standardCode,
			Integer branchId, Integer academicYearId) {
		int standardId = 0;
		String sql = "select id from standard where code = '"
				+ standardCode
				+ "' AND active = 'Y' AND branch_id = "
				+ branchId
				+ (academicYearId != null ? " AND academic_year_id = "
						+ academicYearId : "");

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			standardId = ids.get(0);
		}

		return standardId;
	}

	public int getStandardIdByValidatingName(String standardName,
			Integer branchId, Integer academicYearId) {
		int standardId = 0;
		String sql = "select id from standard where displayName = '"
				+ standardName
				+ "' AND active = 'Y' AND branch_id = "
				+ branchId
				+ (academicYearId != null ? " AND academic_year_id = "
						+ academicYearId : "");

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			standardId = ids.get(0);
		}

		return standardId;
	}

	public int getStudentIdByRollNumber(String rollNumber, Integer branchId,
			Integer standardId, Integer divisionId) {
		int studentId = 0;

		String sql = "select id from student where branch_id = " + branchId
				+ " AND standard_id = " + standardId + " AND division_id = "
				+ divisionId + " AND roll_number = '" + rollNumber
				+ "' AND isDelete = 'N' AND active = 'Y'";

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			studentId = ids.get(0);
		}
		return studentId;
	}

	public int getStudentIdByRegNumber(String registrationNumber,
			Integer branchId) {
		int studentId = 0;
		String sql = "select id from student where branch_id = " + branchId
				+ " AND registration_code = '" + registrationNumber
				+ "' AND isDelete = 'N' AND active = 'Y'";

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			studentId = ids.get(0);
		}
		return studentId;
	}

	public int getTeacherIdByValidatingName(String firstName, String lastName,
			Integer branchId) {
		int teacherId = 0;
		String sql = "select id from teacher where firstName = '" + firstName
				+ "' AND " + "lastName = '" + lastName + "' AND "
				+ "isDelete = 'N' AND " + "branch_id = " + branchId;

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			teacherId = ids.get(0);
		}

		return teacherId;
	}

	public int getProfessionIdByValidatingName(String professionName) {
		int professionId = 0;
		String sql = null;

		sql = "select id from profession where name = '" + professionName + "'";
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			professionId = ids.get(0);
		}

		return professionId;
	}

	public int getIncomeIdByValidatingName(String incomeName) {
		int incomeId = 0;
		String sql = null;

		sql = "select id from income_range where name = '" + incomeName + "'";
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			incomeId = ids.get(0);
		}

		return incomeId;
	}

	public int getHeadIdByValidatingName(String headName, Integer branchId) {
		int headId = 0;
		String sql = null;

		sql = "select id from head where name = '" + headName
				+ "' AND active = 'Y' AND branch_id = " + branchId
				+ " AND is_delete = 'N'";

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			headId = ids.get(0);
		}
		return headId;
	}

	public int getCourseIdByValidatingName(String courseName, Integer branchId) {
		int courseId = 0;
		String sql = null;

		sql = "select id from course where name = '" + courseName
				+ "' AND active = 'Y' "
				+ (branchId != null ? " AND branch_id = " + branchId : "");
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			courseId = ids.get(0);
		}
		return courseId;
	}

	public int getDivisionIdByValidatingCode(String divisionName,
			Integer branchId, Integer academicYearId) {
		int divisionId = 0;
		String sql = null;

		sql = "select id from division where code = '"
				+ divisionName
				+ "' AND active = 'Y' AND branch_id = "
				+ branchId
				+ (academicYearId != null ? " AND academic_year_id = "
						+ academicYearId : "");
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			divisionId = ids.get(0);
		}
		return divisionId;
	}

	public int getDivisionIdByValidatingName(String divisionName,
			Integer branchId, Integer academicYearId) {
		int divisionId = 0;
		String sql = null;

		sql = "select id from division where displayName = '"
				+ divisionName
				+ "' AND active = 'Y' AND branch_id = "
				+ branchId
				+ (academicYearId != null ? " AND academic_year_id = "
						+ academicYearId : "");
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			divisionId = ids.get(0);
		}
		return divisionId;
	}

	public int getDivisionIdByValidatingName(String divisionName,
			Integer standardId, Integer branchId, Integer academicYearId) {
		int divisionId = 0;
		String sql = null;

		sql = "select d.id from division d, standard_division sd where d.displayName = '"
				+ divisionName
				+ "'  AND d.active = 'Y' AND d.branch_id = "
				+ branchId
				+ (academicYearId != null ? " AND d.academic_year_id = "
						+ academicYearId : "")
				+ " AND sd.division_id = d.id AND sd.standard_id = "
				+ standardId;
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			divisionId = ids.get(0);
		}
		return divisionId;
	}

	public int getCasteIdByValidatingName(String casteName) {
		int casteId = 0;
		String sql = null;

		sql = "select id from caste where name = '" + casteName
				+ "' AND active = 'Y'";

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			casteId = ids.get(0);
		}
		return casteId;
	}

	public int getStateIdByValidatingName(String stateName) {
		int stateId = 0;
		String sql = null;

		sql = "select id from state where name = '" + stateName
				+ "' AND active = 'Y'";
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			stateId = ids.get(0);
		}
		return stateId;
	}

	public int getDistrictIdByValidatingName(String districtName,
			Integer stateId) {
		int districtId = 0;
		String sql = null;

		sql = "select id from district where name = '" + districtName
				+ "' AND active = 'Y' AND state_id = " + stateId;
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			districtId = ids.get(0);
		}
		return districtId;
	}

	public int getTalukaIdByValidatingName(String talukaName, Integer districtId) {
		int talukaId = 0;
		String sql = null;

		sql = "select id from taluka where name = '" + talukaName
				+ "' AND active = 'Y' AND district_id = " + districtId;

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			talukaId = ids.get(0);
		}
		return talukaId;
	}

	public int getSubjectIdByValidatingName(String subjectName,
			Integer branchId, Integer academicYear) {
		int subjectId = 0;
		String sql = null;

		sql = "select id from subject where name = '" + subjectName
				+ "' AND is_delete = 'N' AND branch_id = " + branchId;

		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			subjectId = ids.get(0);
		}
		return subjectId;
	}

	public int getSubjectIdByValidatingNameWithStandard(String subjectName,
			Integer standardId) {
		int talukaId = 0;
		String sql = null;

		sql = "select s.id " + "from subject s "
				+ "inner join standard_subject ss on ss.subject_id = s.id "
				+ "inner join standard std on std.id = ss.standard_id "
				+ "where std.id = '" + standardId + "' and s.name = '"
				+ subjectName + "' ";
		List<Integer> ids = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});
		if (ids.size() > 0) {
			talukaId = ids.get(0);
		}
		return talukaId;
	}

	public int getFeesCodeIdByName(String name, Integer branchId) {
		int feesCode = 0;
		String sql = "select id from fees_code_configuration where name = '"
				+ name + "' AND branch_id = " + branchId;
		List<Integer> feesCodeList = getJdbcTemplate().query(sql,
				new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						return resultSet.getInt("id");
					}
				});

		feesCode = (feesCodeList != null && !feesCodeList.isEmpty()) ? feesCodeList
				.get(0) : 0;

		return feesCode;
	}

	public boolean isStandardAndDivisionApplicableForTeacher(
			Integer teacherUserId, Integer standardId, Integer divisionId) {
		String sql = "select count(*) as count "
				+ " from teacher_standard as ts "
				+ " INNER JOIN teacher as t on ts.teacher_id = t.id "
				+ " WHERE t.user_id = " + teacherUserId
				+ " AND ts.standard_id = " + standardId
				+ " AND (division_id is null OR division_id = " + divisionId
				+ ")";

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isGroupsPresentInThisBranch(Integer branchId,
			Set<Integer> groupIds) {

		String groups = StringUtils.join(groupIds, ", ");
		String sql = "select count(*) as count from `group`" + " where id in ("
				+ groups + ") AND branch_id = " + branchId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count >= groupIds.size()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isIndividualsPresentInBranch(Integer branchId,
			Set<Integer> individualIds) {
		String individualsStr = StringUtils.join(individualIds, ", ");

		String sql = "select user_id from student where branch_id = "
				+ branchId + " AND user_id in(" + individualsStr + ")";
		Set<Integer> individualSet = new HashSet<>();
		List<Integer> individuals = getJdbcTemplate().queryForList(sql,
				Integer.class);
		individualSet.addAll(individuals);

		sql = "select user_id from teacher where branch_id = " + branchId
				+ " AND user_id in(" + individualsStr + ")";
		individuals = getJdbcTemplate().queryForList(sql, Integer.class);
		individualSet.addAll(individuals);

		if (individualSet.size() >= individualIds.size()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isIndividualsApplicableForThisTeacher(Integer teacherUserId,
			Set<Integer> individualIds) {
		String individualsStr = StringUtils.join(individualIds, ", ");
		Set<Integer> individualSet = new HashSet<>();

		String sql = "select user_id from teacher where id in "
				+ " (select teacher_id from teacher_standard where standard_id in "
				+ " (select standard_id from teacher_standard where teacher_id = "
				+ " (select id from teacher where user_id = " + teacherUserId
				+ "))) AND user_id in (" + individualsStr + ")";

		List<Integer> individuals = getJdbcTemplate().queryForList(sql,
				Integer.class);
		individualSet.addAll(individuals);

		sql = "select user_id from student where standard_id in"
				+ " (select standard_id from teacher_standard where teacher_id = "
				+ " (select id from teacher where user_id = " + teacherUserId
				+ ")) AND user_id in (" + individualsStr + ")";

		individuals = getJdbcTemplate().queryForList(sql, Integer.class);
		individualSet.addAll(individuals);

		if (individualSet.size() >= individualIds.size()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isGroupsApplicableForThisTeacher(Integer teacherUserId,
			Set<Integer> groupIds) {

		String groups = StringUtils.join(groupIds, ", ");
		String sql = "select count(*) as count from group_user "
				+ " where group_id in (" + groups + ") AND user_id = "
				+ teacherUserId;

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count >= groupIds.size()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isStandardIdExistInThisBranch(Integer standardId,
			Integer branchId) {
		String sql = "select count(*) as count from standard "
				+ " where id = "
				+ standardId
				+ " "
				+ " AND branch_id = "
				+ branchId
				+ " "
				+ " AND active = 'Y' AND academic_year_id = "
				+ " (select id from academic_year where is_current_active_year = 'Y' AND branch_id = "
				+ branchId + " )";
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count <= 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isDivisionIdExistInThisBranchAndStandard(Integer divisionId,
			Integer standardId, Integer branchId) {
		String sql = "select count(*) as count from standard_division as sd"
				+ " INNER JOIN division as d on d.id = sd.division_id "
				+ " where sd.standard_id = "
				+ standardId
				+ " "
				+ " AND sd.branch_id = "
				+ branchId
				+ " "
				+ " AND sd.division_id = "
				+ divisionId
				+ " AND d.active = 'Y' AND d.academic_year_id = "
				+ " (select id from academic_year where is_current_active_year = 'Y' AND branch_id = "
				+ branchId + " )";
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count <= 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isSubjectIdExistInThisBranchAndForStandard(
			List<Integer> subjectIds, Integer standardId, Integer branchId) {
		String sql = "select count(*) as count from standard_subject as ss"
				+ " INNER JOIN subject as s on s.id = ss.subject_id "
				+ " INNER JOIN standard as std on std.branch_id = s.branch_id "
				+ " where ss.standard_id = " + standardId + " "
				+ " AND s.branch_id = " + branchId + " "
				+ " AND ss.subject_id in ("
				+ StringUtils.join(subjectIds, ", ") + ")"
				+ " AND s.is_delete = 'N'";

		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count >= subjectIds.size()) {
			return true;
		} else {
			return false;
		}
	}

	public Map<String, Integer> getStandardsBasedOnBranchId(Integer branchId) {
		final Map<String, Integer> data = new HashMap<String, Integer>();
		String sql = "select displayName, id from standard where active = 'Y' AND branch_id = "
				+ branchId;
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				data.put(rs.getString("displayName"), rs.getInt("id"));
				return null;
			}
		});
		return data;
	}

	public Map<String, Integer> getDivisionsBasedOnBranchId(Integer branchId) {
		final Map<String, Integer> data = new HashMap<String, Integer>();
		String sql = "select d.displayName as divisionName, d.id as divisionId, st.displayName as standardName from division as d " +
				" INNER JOIN standard_division as sd on sd.division_id = d.id " +
				" INNER JOIN standard as st on sd.standard_id = st.id " +
				" where d.active = 'Y' AND d.branch_id = " + branchId;

		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				data.put(rs.getString("divisionName") + "-" + rs.getString("standardName"), rs.getInt("divisionId"));
				return null;
			}
		});
		return data;
	}

	public Map<String, Integer> getCastes() {
		final Map<String, Integer> data = new HashMap<String, Integer>();
		String sql = "select name, id from caste where active = 'Y'";
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				data.put(rs.getString("name"), rs.getInt("id"));
				return null;
			}
		});
		return data;
	}

	public Map<String, Integer> getFeesCodesBasedOnBranchId(Integer branchId) {
		final Map<String, Integer> data = new HashMap<String, Integer>();
		String sql = "select name, id from fees_code_configuration where branch_id = "
				+ branchId;
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				data.put(rs.getString("name"), rs.getInt("id"));
				return null;
			}
		});
		return data;
	}

//	@Cacheable(value={"professionMap"})
	public Map<String, Integer> getProfessions() {
		System.err.println("*************Getting Professions***********");
		final Map<String, Integer> data = new HashMap<String, Integer>();
		String sql = "select name, id from profession";
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				data.put(rs.getString("name"), rs.getInt("id"));
				return null;
			}
		});
		System.err.println("Returning data.....");
		return data;
	}

//	@Cacheable(value="incomeRangeMap")
	public Map<String, Integer> getAllIncomeRanges() {
		System.err.println("*************Getting IncomeRanges***********");
		final Map<String, Integer> data = new HashMap<String, Integer>();
		String sql = "select name, id from income_range";
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				data.put(rs.getString("name"), rs.getInt("id"));
				return null;
			}
		});
		return data;
	}
	
	public Map<String, Integer> getAllStates(){
		final Map<String, Integer>  states = new HashMap<>();
		String sql = "select name, id from state ";
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				states.put(rs.getString("name"), rs.getInt("id"));
				return null;
			}
		});
		return states;
	}
	
	public Map<Integer, Map<String, Integer>> getAllDistrictsUgainstState(){
		final Map<Integer, Map<String, Integer>>  districts = new HashMap<>();
		String sql = "select name, id, state_id from district ";
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				Map<String, Integer> districtsList = null;
				if(districts.containsKey(rs.getInt("state_id"))){
					districtsList = districts.get(rs.getInt("state_id"));
				}
				else {
					districtsList = new HashMap<>();
				}
				districtsList.put(rs.getString("name"), rs.getInt("id"));
				districts.put(rs.getInt("state_id"), districtsList);
				return null;
			}
		});
		return districts;
	}


	public  Map<Integer, Map<String, Integer>> getAllTalukasUgainstDistrict(){
		final  Map<Integer, Map<String, Integer>>  talukas = new HashMap<>();
		String sql = "select name, id, district_id from taluka ";
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				Map<String, Integer> talukaMap = null;
				if(talukas.containsKey(rs.getInt("district_id"))){
					talukaMap = talukas.get(rs.getInt("district_id"));
				}
				else {
					talukaMap = new HashMap<>();
				}
				talukaMap.put(rs.getString("name"), rs.getInt("id"));
				talukas.put(rs.getInt("district_id"), talukaMap);
				return null;
			}
		});
		return talukas;
	}
	
}