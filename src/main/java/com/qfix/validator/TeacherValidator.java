package com.qfix.validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.qfix.model.Master;
import com.qfix.model.Teacher;
import com.qfix.model.TeacherStandard;
import com.qfix.utilities.Constants;
import com.qfix.utilities.DateUtil;

@Scope(value="prototype")
@Repository
public class TeacherValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private DriverManagerDataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Autowired
	MasterValidator masterValidator;
	
	private boolean isUpload;
	private List<Teacher> teacherList;
	DateUtil dateUtil;
	private int index;

	public void setIndex(int index){
		this.index = index;
	}

	public TeacherValidator(){
		dateUtil = new DateUtil();
	}
	
	public void setTeacherList(List<Teacher> teacherList) {
		this.teacherList = teacherList;
	}

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}

	public boolean supports(Class clazz) {
		return Teacher.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) 
	{
		Teacher teacher = (Teacher) target;
		try {
			if(StringUtils.isEmpty(teacher.getActive())){
				teacher.setActive("N");
			}
			Date teacherDob = dateUtil.getDate(teacher.getDateOfBirth(), errors, "dateOfBirth", "Birth Date");
			if(teacherDob != null){
				teacher.setDateOfBirth(Constants.DATABASE_DATE_FORMAT.format(teacherDob));
				checkDateOfBirth(teacherDob, errors);
			}

			if(isUpload){
				validateWithUploadedList(teacher, errors);
				validateAddressFields(teacher, errors);
			}
			else {
				checkStandardDivisions(teacher, errors);
			}

			if(!StringUtils.isEmpty(teacher.getEmailAddress())){
				checkTeacherUnique(teacher, errors);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	private void validateAddressFields(Teacher teacher, Errors errors) throws SQLException{
		int stateId = masterValidator.getStateIdByValidatingName(teacher.getState());
		if(!StringUtils.isEmpty(teacher.getState())){
			
			if(stateId != 0){
				teacher.setStateId(stateId);
			}		
			else {
				errors.rejectValue("state", "", "Please enter valid state.");
			}
		}
		/*else{
			errors.rejectValue("state", "", "State is require.");
		}*/
		int districtId = 0;
		if(!StringUtils.isEmpty(teacher.getDistrict())){
			districtId = masterValidator.getDistrictIdByValidatingName(teacher.getDistrict(), stateId);
			if(districtId  != 0){
				teacher.setDistrictId(districtId);
			}
			else {
				errors.rejectValue("district", "", "Please enter valid district.");
			}	
		}
		/*else{
			errors.rejectValue("district", "", "District is require.");
		}*/

		int talukaId = 0;
		if(!StringUtils.isEmpty(teacher.getTaluka())){
			talukaId = masterValidator.getTalukaIdByValidatingName(teacher.getTaluka(), districtId);
			if(talukaId != 0){
				teacher.setTalukaId(talukaId);
			}
			else {
				errors.rejectValue("taluka", "", "Please enter valid taluka.");
			}	
		}
		/*else{
			errors.rejectValue("taluka", "", "Taluka is require.");
		}*/
	}

	private void checkStandardDivisions(Teacher teacher, Errors errors) {
		if(teacher.getTeacherStandardList() != null){
			for(TeacherStandard teacherStandard : teacher.getTeacherStandardList()){
				isStandardExists(teacherStandard, errors, teacher.getBranchId());
				isDivisionExists(teacherStandard, errors, teacher.getBranchId());
				isSubjectsExists(teacherStandard, errors, teacher.getBranchId());
			}
		}
	}


	private void isStandardExists(TeacherStandard teacherStandard, Errors errors, Integer branchId){
		if(teacherStandard.getStandardId() != null){
			if(!masterValidator.isStandardIdExistInThisBranch(teacherStandard.getStandardId(), branchId)){
				errors.rejectValue("", "errors.standardNotFound"+teacherStandard.getStandardId(), "Standard not found in this branch Standard is - "+teacherStandard.getStandard());
			}
		}
	}


	private void isDivisionExists(TeacherStandard teacherStandard, Errors errors, Integer branchId){
		if(teacherStandard.getDivisionId() != null){
			if(!masterValidator.isDivisionIdExistInThisBranchAndStandard(teacherStandard.getDivisionId(), teacherStandard.getStandardId(), branchId)){
				errors.rejectValue("", "errors.divisionNotFound"+teacherStandard.getStandardId(), 
					"Division not found in this branch or standard, Division is - "+teacherStandard.getDivision());
			}
		}
	}


	private void isSubjectsExists(TeacherStandard teacherStandard, Errors errors, Integer branchId){
		if(teacherStandard.getSubjectList() != null){
			List<Integer> subjectIds = new ArrayList<>();
			List<String> subjectNames = new ArrayList<>();
			for(Master master : teacherStandard.getSubjectList()){
				subjectIds.add(master.getId());
				subjectNames.add(master.getName());
			}
			if(subjectIds.size() > 0 && !masterValidator.isSubjectIdExistInThisBranchAndForStandard(subjectIds, teacherStandard.getStandardId(), branchId)){
				errors.rejectValue("", "errors.subjectNotFound"+teacherStandard.getStandardId(), 
						"Subject not found in this branch or standard, Subjects are - "+org.apache.commons.lang3.StringUtils.join(subjectNames, ", "));
			}
		}
	}

	private void validateWithUploadedList(Teacher currentTeacher, Errors errors) {
		for(int i = 0; i< teacherList.size(); i++){
			if(i > index){
				Teacher nextTeacher = teacherList.get(i);
				if(!StringUtils.isEmpty(currentTeacher.getEmailAddress()) 
						&& currentTeacher.getEmailAddress().equals(nextTeacher.getEmailAddress())){
					errors.rejectValue("emailAddress", "error.email", 
						"Email address is same in row "+(i + 2)+", email = "+currentTeacher.getEmailAddress());
				}
			}
			
		}
	}

	private void checkDateOfBirth(Date teacherDob, Errors errors){
		if(teacherDob.after(dateUtil.getTodayDate())){
			errors.rejectValue("","","Teacher date of birth can not be in future");
		}
	}

	private void checkTeacherUnique(final Teacher teacher, final Errors errors) throws SQLException {

		String sql = "select r.name as role, count(*) as count, c.* from contact as c INNER JOIN user_role as ur ON ur.user_id = c.user_id INNER JOIN role as r ON r.id = ur.role_id " +
				" WHERE (c.email = '"+teacher.getEmailAddress()+"' OR phone = '"+teacher.getPrimaryContact()+"')" +
				(teacher.getId() != null ? " AND c.user_id != (select user_id from teacher where id = "+teacher.getId()+")" : "");

		getJdbcTemplate().query(sql, new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {

				if(rs.getInt("count") > 1){
					errors.rejectValue("emailAddress", "email Address", "Email or mobile is already used.");
				}
				else if(rs.getInt("count") == 1){
					if(!rs.getString("role").equals(Constants.ROLE_PARENT)){
						errors.rejectValue("emailAddress", "emailOrMobile", "Email or mobile is already used.");	
					} 
					else if(rs.getString("firstname").equals(teacher.getFirstName()) && rs.getString("lastname").equals(teacher.getLastName())){
						teacher.setTeacherExistsAsParent(true);
					}
					else {
						errors.rejectValue("emailAddress", "email Address", "Email or mobile is already used.");
					}
				}
				return null;
			}
		});
	}

}
