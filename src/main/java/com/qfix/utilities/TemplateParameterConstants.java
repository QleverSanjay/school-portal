package com.qfix.utilities;

public interface TemplateParameterConstants {

	public String FIRST_NAME = "first_name";
	public String SYSTEM_NAME = "system_name";
	public String INSTITUTE_NAME = "institute_name";
	public String BRANCH_NAME = "branch_name";
	public String OTP = "otp";
	public String ATTACHMENTS = "attachments";
	public String NOTICE_TYPE = "notice_type";
	public String PUSH_TITLE = "push-title";
	public String PUSH_MESSAGE = "push-message";
	public String PUSH_DETAIL_JSON = "push-detailJson";
	
	public String USERNAME = "username";
	public String PASSWORD = "password";
}
