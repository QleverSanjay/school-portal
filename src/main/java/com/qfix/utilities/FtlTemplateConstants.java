package com.qfix.utilities;

public interface FtlTemplateConstants {

	public static final String CREATE_EVENT_CODE = "NEW_EVENT";
	public static final String UPDATE_EVENT_CODE = "UPDATE_EVENT";
	public static final String CREATE_USER_CODE = "USER_CREATE";
	public static final String PROFILE_UPDATE_CODE = "PROFILE_UPDATE";
	public static final String CREATE_NOTICE_CODE = "CREATE_NOTICE";
	public static final String CREATE_EDIARY_CODE = "CREATE_EDIARY";
	public static final String CREATE_STUDENT_FOR_PARENT_CODE = "STUDENT_CREATE";
	public static final String ADMISSION_SUCCESS_CODE = "ADMISSION_SUCCESS_EMAIL";
	public static final String CREATE_ASSIGNMENTS_CODE="CREATE_ASSIGNMENTS";
	
}
