package com.qfix.utilities;

public interface ReportHeaderConstants {

	public String STUDENT_ID_TEXT = "Student Id";
	public String STUDENT_NAME_TEXT = "Student Name";
	public String ROLE_NUMBER_TEXT = "Roll Number";
	public String THEROY_MARKS_TEXT = "Theory";
	public String PRACTICAL_MARKS_TEXT = "Practical";
	public String OBTAINED_MARKS_TEXT = "Obtained Marks/Grade";
	public String TOTAL_MARKS_TEXT = "Total Marks/Grade";
	public String CATEGORY_TEXT = "Category";
	public String REMARKS_TEXT = "Remarks";
	public String RESULT_TEXT = "Result";
	public String FINAL_GRADE_TEXT = "Final Grade/Marks";
	public String POSITION_TEXT = "Position";
	public String GENERAL_REMARK_TEXT = "General Remark";
	public String GROSS_TOTAL_TEXT = "Gross Total";
}