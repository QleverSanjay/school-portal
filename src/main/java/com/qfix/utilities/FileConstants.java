package com.qfix.utilities;

import java.io.File;

public class FileConstants {

	public static String NOTICE_DOCUMENTS_PATH = "notices" + File.separator;
	public static String PROFILE_DOCUMENTS_PATH = "profiles" + File.separator;
	public static String EVENT_DOCUMENTS_PATH = "events" + File.separator;
	public static String UPLOAD_DOCUMENTS_PATH = "upload" + File.separator;

	public static String DEFAULT_IMAGE_NAME = "no-images.png";
	public static String DEFAULT_PROFILE_IMAGE_NAME = "no-profile.png";

	public static String TEACHER_PROFILE_PATH = "teachers/";
	public static String STUDENT_PROFILE_PATH = "students/";
	public static String SCHOOL_ADMIN_PROFILE_PATH = "school_admins/";
	public static String PARENT_PROFILE_PATH = "parents/";
}
