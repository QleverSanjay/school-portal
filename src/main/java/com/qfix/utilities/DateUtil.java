package com.qfix.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import com.qfix.model.Fees;

public final class DateUtil {
	String dateFormatRegex1 = "^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])$";

	public Date getDate(String dateStr,Errors errors, String field, String title) {

	/*	if (StringUtils.isEmpty(dateStr)) {
			errors.rejectValue(field, "error." + field, title
					+ " is mandatory.");
			return null;
		}
		*/
		SimpleDateFormat simpleDateFormat=null;
		if (!StringUtils.isEmpty(dateStr)){
			simpleDateFormat = dateStr.matches(dateFormatRegex1) ? Constants.DATABASE_DATE_FORMAT : Constants.USER_DATE_FORMAT;
			simpleDateFormat.setLenient(false);	
		}

		
		Date date = null;
		try {
			if (!StringUtils.isEmpty(dateStr)){
			date = simpleDateFormat.parse(dateStr);
			}
		} catch (ParseException e) {
			errors.rejectValue(field, "errors." + field,
					"Please check date and date format must be in "
							+ Constants.EXCEL_DATE_FORMAT.toUpperCase() + ", Your entered date is " + dateStr);
			e.printStackTrace();
		}
		return date;
	}


	public Date getDateAllowNull(String dateStr, Errors errors, String field, String title) {

		if (StringUtils.isEmpty(dateStr)) {
			return null;
		}

		SimpleDateFormat simpleDateFormat = dateStr.matches(dateFormatRegex1) ? Constants.DATABASE_DATE_FORMAT : Constants.USER_DATE_FORMAT;
		simpleDateFormat.setLenient(false);
		Date date = null;
		try {
			date = simpleDateFormat.parse(dateStr);
		} catch (ParseException e) {
			errors.rejectValue(field, "errors." + field,
					"Please check date and date format must be in "
							+ Constants.EXCEL_DATE_FORMAT.toUpperCase() + ", Your entered date is " + dateStr);
			e.printStackTrace();
		}
		
		System.out.println("Returning date to studentValidator");
		return date;
	}

	public Date getDate(String dateStr){

		if (StringUtils.isEmpty(dateStr)) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = dateStr.matches(dateFormatRegex1) ? Constants.DATABASE_DATE_FORMAT : Constants.USER_DATE_FORMAT;
		simpleDateFormat.setLenient(false);
		Date date = null;
		try {
			date = simpleDateFormat.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public Date getTodayDate(){
		Date todayDate = new Date();
		return todayDate;
	}
	
	
	/*public static Date getDueDate(Fees fees){
		Date dueDate=fees.getDueDateD();
		if()
		return null;
		
	}*/
}
