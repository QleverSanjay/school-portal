package com.qfix.utilities;

public interface AppConstants {

	public static final String TYPE_EMAIL = "EMAIL";
	public static final String TYPE_SMS = "SMS";
	public static final String SYSTEM_NAME = "EDU-QFIX";
	public static final String LOGIN_URL_STUDENT = "www.eduqfix.com/ParentPortal/#/";
	public static final String LOGIN_URL_SCHOOL = "www.eduqfix.com/SchoolPortal/#/";
	public static final String LOGIN_URL_PARENT = "www.eduqfix.com/ParentPortal/#/";
}
