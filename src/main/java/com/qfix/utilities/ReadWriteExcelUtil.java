package com.qfix.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.qfix.model.ExcelContentModel;

public class ReadWriteExcelUtil {
	final static Logger logger = Logger.getLogger(ReadWriteExcelUtil.class);
	
	public Sheet getSheetByReadingExcelFile(InputStream inputStream, boolean isXLSFile){
		Sheet sheet = null;

		Workbook workbook = null;

		try {
			if(isXLSFile){
				workbook = new XSSFWorkbook(inputStream);
			}
			else {
				workbook = new XSSFWorkbook(inputStream);
			}
			sheet = (workbook != null ) ? workbook.getSheetAt(0) : null;
		} catch (FileNotFoundException e) {
			logger.error("", e);
		} catch (IOException e) {
			logger.error("", e);
		}
		finally{
			if(inputStream != null){
				try {
					inputStream.close();
				} catch (IOException e) {
					logger.error("", e);
				}
			}
		}
		return sheet;
	}
	
	
	public static XSSFWorkbook writeDataToExcel(TreeMap<Integer, Object[]> data, int startrownum, String fileName) {
 		XSSFWorkbook workbook = new XSSFWorkbook();
 		
        Sheet sheet =  workbook.createSheet("Sample sheet");  

        CellStyle centerVerticalStyle = workbook.createCellStyle();
        centerVerticalStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

        CellStyle rightVerticalStyle = workbook.createCellStyle();
        rightVerticalStyle.setVerticalAlignment(XSSFCellStyle.ALIGN_RIGHT);

        SortedSet<Integer> keyset = (SortedSet<Integer>) data.keySet();
        try {
	        for (Integer key : keyset)   
	        {  
	            Row row = sheet.createRow(key);  
	            Object [] objArr = data.get(key);  
	            int cellnum = 0;  

	            for (Object obj : objArr)
	            {  
	                Cell cell = row.createCell(cellnum++);  
	                if(obj instanceof Date)  {
	                	 /*CellStyle cellStyle = workbook.createCellStyle();
	                	    cellStyle.setDataFormat(
	                	        createHelper.createDataFormat().getFormat("d/m/yy"));*/
	                	
	                	String date = Constants.USER_DATE_FORMAT.format((java.util.Date)obj);
	                	cell.setCellValue(date); 
	                	
	                }
	                else if(obj instanceof Boolean){
	                	cell.setCellValue((Boolean)obj);
	                }	  
	                else if(obj instanceof String) {
	                	cell.setCellValue((String)obj);
	                }
	                else if(obj instanceof Double) {
	                	cell.setCellValue((Double)obj);
	                	cell.setCellStyle(rightVerticalStyle);
	                }  
	                else if(obj instanceof Integer){
	                	cell.setCellValue((Integer)obj);
	                	cell.setCellStyle(centerVerticalStyle);
	                }
	                else if(obj instanceof Float){
	                	cell.setCellValue((Float)obj);
	                	cell.setCellStyle(rightVerticalStyle);
	                }
	            }  
	        }  
        
//			FileOutputStream out = new FileOutputStream(new File(fileName));
//			workbook.write(out);
//		    out.close();
		} catch (Exception e) {
			logger.error("", e);
		}
        return workbook;
	}


	public static Workbook writeToExcel(TreeMap<Integer, List<ExcelContentModel>> data, 
			List<CellRangeAddress> mergedCellRangeList,
			int startrownum, String fileName) {

 		Workbook workbook = new XSSFWorkbook();
 		
 		CellStyle centerVerticalStyle = workbook.createCellStyle();
        centerVerticalStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

        CellStyle rightVerticalStyle = workbook.createCellStyle();
        rightVerticalStyle.setVerticalAlignment(XSSFCellStyle.ALIGN_RIGHT);
        
 		//CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet =  workbook.createSheet("Sample sheet");  
        SortedSet<Integer> keyset = (SortedSet<Integer>) data.keySet();
        try {
        	/* cell style for locking */
        	CellStyle lockedCellStyle = workbook.createCellStyle();
        	lockedCellStyle.setLocked(true);
        	/* cell style for editable cells */
        	CellStyle unlockedCellStyle = workbook.createCellStyle();
        	unlockedCellStyle.setLocked(false);

	        for (Integer key : keyset)   
	        {  
	            Row row = sheet.createRow(key);  
	            List<ExcelContentModel> objArr = data.get(key);  
	            if(objArr != null){
	            	for (ExcelContentModel excelContentModel : objArr)
		            {  
		            	Object obj = excelContentModel.getCellData();
		                Cell cell = row.createCell(excelContentModel.getColumnIndex());
		                if(excelContentModel.isReadOnly()){
		                	cell.setCellStyle(lockedCellStyle);
		                }
		                else {
		                	cell.setCellStyle(unlockedCellStyle);
		                }
		                if(obj instanceof Date)  {

		                	String date = Constants.USER_DATE_FORMAT.format((java.util.Date)obj);
		                	cell.setCellValue(date);
		                }
		                else if(obj instanceof Boolean){
		                	cell.setCellValue((Boolean)obj);
		                }	  
		                else if(obj instanceof String) {
		                	cell.setCellValue((String)obj);
		                }
		                else if(obj instanceof Double) {
		                	cell.setCellValue((Double)obj);
		                	cell.setCellStyle(rightVerticalStyle);
		                }  
		                else if(obj instanceof Integer){
		                	cell.setCellValue((Integer)obj);
		                	cell.setCellStyle(centerVerticalStyle);
		                }
		                else if(obj instanceof Float){
		                	cell.setCellValue((Float)obj);
		                	cell.setCellStyle(rightVerticalStyle);
		                }
		            }
	            }
	        }
	        if(mergedCellRangeList != null){
	        	for(CellRangeAddress cellAddress : mergedCellRangeList){
	        		sheet.addMergedRegion(cellAddress);
	        	}
	        }
			FileOutputStream out = new FileOutputStream(new File(fileName));
			workbook.write(out);
		    out.close();
		} catch (FileNotFoundException e) {
			logger.error("", e);
		} catch (IOException e) {
			logger.error("", e);
		}
        return workbook;
	}
	
	/**
	 * Gets the cell data .
	 *
	 * @param nextCell the next cell
	 * @return the cell data
	 */
	public static String getCellData(Cell nextCell) {
		String cellData = null;
		switch(nextCell.getCellType()){
		case Cell.CELL_TYPE_STRING:
            cellData = nextCell.getStringCellValue();
            break;
        case Cell.CELL_TYPE_NUMERIC:
            cellData = nextCell.getNumericCellValue()+"";
            break;
        case Cell.CELL_TYPE_BLANK :
        	cellData = null;
        	break;
		}
		return cellData;
	}
}