package com.qfix.utilities;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.qfix.exceptions.SendNotificationException;

public class PushNotificationUtil {

//	private static final String GOOGLE_SERVER_KEY = "AIzaSyD9VB7htSK-zbrvdOTSr0E7GMuNy7cXrBw";
	private static final String GOOGLE_SERVER_KEY = "AIzaSyC5rupCe31uDmIe1-Zf6EExJjS7-pYIFk0";
	static final String MESSAGE_KEY = "message";
	final static Logger logger = Logger.getLogger(PushNotificationUtil.class);

	public static MulticastResult sendAndroidNotication(String title, String messageData, String additionalData, List<String> regIdList) 
			throws SendNotificationException{
		MulticastResult result = null;
		try {
			logger.debug("PUSH MESSAGE IDS :::"+regIdList);
			Sender sender = new Sender(GOOGLE_SERVER_KEY.trim());

			logger.debug("PUSH MESSAGE TITLE :::"+title+"-"+messageData);
			Message message = new Message.Builder().timeToLive(30)
					.collapseKey("1")
                    .delayWhileIdle(false)
                    .addData("title", title)
                    .addData( "content-available", "1")
					.addData(MESSAGE_KEY,messageData)
					.addData("messageData",additionalData)
					.build();

			System.out.println(message);
			result = sender.send(message, regIdList, 5);
			logger.debug("PUSH MESSAGE RESULT :::"+result);
		}
		catch (Exception e) {
			logger.error("Problem while sending Notification.", e);
			e.printStackTrace();
			if(e instanceof InvalidRequestException){
				InvalidRequestException e1 = (InvalidRequestException) e;
				System.out.println(e1.getDescription());
			}
		}
		return null;
	}


	// For live - /home/portal/tomcat/conf/qfix_production_push_notification.p12
	// Password for live = Innovator@123

	public static void sendIOSNotification(String title, String messageData, List<String> idList) throws SendNotificationException{
		if(idList != null && idList.size() > 0){
			try {
				ApnsService service = APNS.newService()
					    .withCert("C:/Users/lenovo/Downloads/iplit_adhoc_push.p12","")
//						.withCert("/home/portal/tomcat/conf/qfix_production_push_notification.p12", "Innovator@123")
				   .withProductionDestination()
//					    .withSandboxDestination()
					    .build();

				System.out.println("////////////////////////////////////////////");
				String alertBodyTxt = "{\"title\" : \""+title+"\", \"body\" : \"This is demo message body.\"}";
				String payload = APNS.newPayload()
		                .alertBody(alertBodyTxt)
		                .sound("default")
		                .customField("content-available", 1)
		                .customField("messageData", messageData).build();
				System.out.println("PAYLOAD:::::\n"+payload);
				service.push(idList, payload);
			}catch (Exception e) {
				logger.error("Problem while sending Notification.", e);
				e.printStackTrace();
				if(e instanceof InvalidRequestException){
					InvalidRequestException e1 = (InvalidRequestException) e;
					logger.error(e1.getDescription());
				}
			}
		}
	}

	static List<String> getList (){
		List<String> list = new ArrayList<>();
		
		return list;
	}
	static List<String> getIOSList (){
		List<String> list = new ArrayList<>();
		
		return list;
	}

	public static void main(String a[]){
		for(int i = 0; i >= 0; i--){

			List<String> idList = new ArrayList<>();
			idList.add("a2374996b0c0d17f263dd04d913ddd8333970f85e7a84cb1f693d0f5726fd9e7");
//			idList.add("c5g3OQEteMU:APA91bFiw-N8Bc14rFr6ZckE9TPV_6YyEngvlKUFckvNeMW9MeVR7K4yRtz-edQ5mMhKevPoBkkKx7-mTVcveXwMPQ1eLH620N_4RRBhrQGZUIOxuu-xaO64GlE2lo3lDflhbgfu-8di");
			String messageData = "Please download and install latest eduqfix application from Play store. Uninstall any old version before you re-install latest version.";

			try {
				System.out.println(idList.get(0).length());
//				sendAndroidNotication("IMPORTNAT: ", messageData, "{\"id\" : 112}", idList, i);
				sendIOSNotification("IMPORTNAT: ", messageData, idList);
//				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static void sendIOSNotification(String title, String messageData,
			List<String> idList, int i) {

		if(idList != null && idList.size() > 0){
			try {
				ApnsService service = APNS.newService()
					    .withCert("D:/project/PROJECT-DATA/Qfix/CERTIFICATES/IOS-PUSH-CERT/qfix-certificates/qfix_production_push_notification.p12","")
						//.withCert("/home/portal/tomcat/conf/qfix_production_push_notification.p12", "Innovator@123")
					    .withProductionDestination()
//					    .withSandboxDestination()
					    .build();

				String payload = null;
				if(i == 0 ){
					payload = APNS.newPayload()
			                //.alertBody(title)
			                .sound("default")
			                .customField("content-available", 0)
			                .customField("messageData", messageData).build();
				}
				else {
					String alertBodyTxt = "IMPORTNAT: Please download and install latest eduqfix application from Play store. Uninstall any old version before you re-install latest version.";
					payload = APNS.newPayload()
			                .alertBody(alertBodyTxt)
			                .sound("default")
//			                .customField("EVENT", "TEST..")
			                .customField("content-available", 1)
			                .customField("messageData", messageData).build();
				}
				System.out.println("PAYLOAD:::::\n"+payload);
				service.push(idList, payload);
				
			}catch (Exception e) {
				logger.error("Problem while sending Notification.", e);
				e.printStackTrace();
				if(e instanceof InvalidRequestException){
					InvalidRequestException e1 = (InvalidRequestException) e;
					logger.error(e1.getDescription());
				}
			}
		}
	}


	public static MulticastResult sendAndroidNotication(String title, String messageData, String additionalData, List<String> regIdList, int i) 
			throws SendNotificationException{
		MulticastResult result = null;
		try {
			logger.debug("PUSH MESSAGE IDS :::"+regIdList);
			Sender sender = new Sender(GOOGLE_SERVER_KEY.trim());

			logger.debug("PUSH MESSAGE TITLE :::"+title+"-"+messageData);
			logger.debug("PUSH MESSAGE DATA :::"+additionalData);
			Message message = null;
			System.out.println("i ==="+i);

			if(i == 0 ){
				message = new Message.Builder().timeToLive(30)
						.collapseKey("1")
	                    .delayWhileIdle(false)
	                    .addData( "content-available", "1")
						//.addData(MESSAGE_KEY,messageData)
						.addData("messageData",additionalData)
						.build();
			}
			else {
				message = new Message.Builder().timeToLive(30)
				.collapseKey("1")
                .delayWhileIdle(false)
                .addData("title", title)
                .addData( "content-available", "0")
				.addData(MESSAGE_KEY,messageData)
				.addData("messageData",additionalData)
				.build();
			}
			
			System.out.println("MESSAGE :::: \n"+message);
			result = sender.send(message, regIdList, 5);
			System.out.println("RESULT ::: "+result);
			logger.debug("PUSH MESSAGE RESULT :::"+result);
		}
		catch (Exception e) {
//			logger.error("Problem while sending Notification.", e);
			e.printStackTrace();
//			if(e instanceof InvalidRequestException){
//				InvalidRequestException e1 = (InvalidRequestException) e;
//				System.out.println(e1.getDescription());
//			}
			// throw new SendNotificationException("Problem while sending Notification.");
		}
		return null;
	}

}










