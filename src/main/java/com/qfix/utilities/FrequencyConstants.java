package com.qfix.utilities;

public interface FrequencyConstants {
	public String DAILY_FREQUENCY = "DAILY";
	public String WEEKLY_FREQUENCY = "WEEKLY";
	public String MONTHLY_FREQUENCY = "MONTHLY";
	public String YEARLY_FREQUENCY = "YEARLY";

}
