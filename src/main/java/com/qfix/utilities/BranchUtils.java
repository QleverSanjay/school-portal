package com.qfix.utilities;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

import com.qfix.exceptions.UnauthorizedException;
import com.qfix.utilities.Constants;

@Slf4j
@Component
public class BranchUtils {
	public void checkUserBranch(Integer branchId, HttpSession session)
			throws UnauthorizedException {

		log.info("User accessing branch ** ::::::::::" + branchId);
		@SuppressWarnings("unchecked")
		List<Integer> branchIdList = (List<Integer>) session
				.getAttribute("branchIdList");
		System.out.println(branchIdList);
		if (!Constants.ROLE_ID_ADMIN.equals(session.getAttribute("roleId"))
				&& (branchIdList == null || branchIdList.size() == 0 || !branchIdList
						.contains(branchId))) {
			throw new UnauthorizedException(
					"Error occured as user do not belong to this branch.");
		}
	}
}
