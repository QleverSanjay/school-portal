package com.qfix.utilities;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qfix.config.AppProperties;
import com.qfix.config.AppProperties.FileProps;

@Component
public class CSVTemplateUtil {

	@Autowired
	private AppProperties appProperties;

	final static Logger logger = Logger.getLogger(CSVTemplateUtil.class);

	public byte[] getCsvFile(String csvFileName) throws IOException {

		FileProps fileProps = appProperties.getFileProps();

		String filePath = fileProps.getBaseSystemPath()
				+ fileProps.getUploadCsvTemplatesPath() + "/" + csvFileName;

		filePath = FilenameUtils.separatorsToSystem(filePath);
		logger.debug("FilePath to read = " + filePath);
		File file = new File(filePath);
		BufferedInputStream stream = new BufferedInputStream(
				new FileInputStream(file));
		byte[] dataArray = new byte[1024];
		stream.read(dataArray);
		stream.close();
		return dataArray;
	}
}
