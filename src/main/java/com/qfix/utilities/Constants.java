package com.qfix.utilities;

import java.text.SimpleDateFormat;

public interface Constants {
	public Integer ROLE_ID_ADMIN = 1;
	public Integer ROLE_ID_STUDENT = 2;
	public Integer ROLE_ID_PARENT = 3;
	public Integer ROLE_ID_TEACHER = 4;
	public Integer ROLE_ID_VENDOR = 5;
	public Integer ROLE_ID_SCHOOL_ADMIN = 6;
	public Integer ROLE_ID_STAFF = 7;
	public Integer ROLE_ID_BANK_PARTNER = 8;

	public String ROLE_ADMIN = "ADMIN";
	public String ROLE_STUDENT = "STUDENT";
	public String ROLE_PARENT = "PARENT";
	public String ROLE_TEACHER = "TEACHER";
	public String ROLE_VENDOR = "VENDOR";
	public String ROLE_SCHOOL_ADMIN = "SCHOOL ADMIN";
	public String ROLE_STAFF = "STAFF";
	public String ROLE_BANK_PARTNER = "BANK_PARTNER";

	public String RECORD_DELETED = "D";
	public String RECORD_ADDED = "N";
	public String RECORD_MODIFIED = "M";

	// public String EXCEL_DATE_FORMAT = "dd-MM-yyyy";
	public String EXCEL_DATE_FORMAT = "dd/MM/yyyy";
	// public String DATE_REGEX =
	// "^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])$";
	public String APPLICATION_URL = "http://localhost:8090";

	public Integer RESULT_TYPE_MARKS_ID = 1;
	public Integer RESULT_TYPE_GRADE_ID = 2;
	public String DOMAIN_NAME = "eduqfix.com";

	public static final SimpleDateFormat DATABASE_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");

	public static final SimpleDateFormat DATABASE_TIME_FORMAT = new SimpleDateFormat(
			"HH:mm:ss");

	public static final SimpleDateFormat USER_DATE_FORMAT = new SimpleDateFormat(
			"dd/MM/yyyy");

	public static enum FEE_TYPE {

		GROUP_FEE("GROUP FEE"), SINGLE_FEE("SINGLE FEE");

		private final String feeType;

		private FEE_TYPE(String feeType) {
			this.feeType = feeType;
		}

		public String get() {
			return this.feeType;
		}
	}

	public static enum LATE_FEE_TYPE {

		RECURRING("RECURRING"), ONE_TIME("ONE TIME"), COMBINATION("COMBINATION");

		private final String feeType;

		private LATE_FEE_TYPE(String feeType) {
			this.feeType = feeType;
		}

		public String get() {
			return this.feeType;
		}
	}

	public static enum FREQUENCY {

		DAILY("DAILY"), MONTHLY("MONTHLY"), WEEKLY("WEEKLY"), QUARTERLY(
				"QUARTERLY"), HALF_YEARLY("HALF_YEARLY"), YEARLY("YEARLY");

		private final String frequency;

		private FREQUENCY(String frequency) {
			this.frequency = frequency;
		}

		public String get() {
			return this.frequency;
		}
	}

	public static enum PAYMENT_STATUS {

		PAID("PAID"), UNPAID("UNPAID"), PARTIAL_PAID("PARTIAL_PAID"), OVER_PAID("OVER PAID"), PARTIAL_REFUND ("PARTIAL REFUND"), REFUND ("REFUND");

		private final String frequency;

		private PAYMENT_STATUS(String frequency) {
			this.frequency = frequency;
		}

		public String get() {
			return this.frequency;
		}
	}
}