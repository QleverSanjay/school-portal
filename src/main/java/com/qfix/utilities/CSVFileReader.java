package com.qfix.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

public class CSVFileReader {
	final static Logger logger = Logger.getLogger(CSVFileReader.class);

	public static <T> List<T> readFile(InputStream insStream, String[] columnList, T classType, Class<T> clazz) {
		CsvToBean<T> csv = new CsvToBean<T>();
		List<T> dataList = new ArrayList<T>();
		CSVReader reader = null;
		InputStreamReader inputStreamReader = null;
		try {
			inputStreamReader = new InputStreamReader(insStream);
			
			ColumnPositionMappingStrategy<T> strat = new ColumnPositionMappingStrategy<T>();
			strat.setType(clazz);
			String[] columns = columnList;
			strat.setColumnMapping(columns);

			CsvEmptyRowFilter filter = new CsvEmptyRowFilter(strat);

			reader = new CSVReader(inputStreamReader, CSVParser.DEFAULT_SEPARATOR, CSVParser.DEFAULT_QUOTE_CHARACTER, CSVParser.DEFAULT_ESCAPE_CHARACTER, 1, false, true);
			// dataList = csv.parse(strat, reader, filter, false);
		}
		finally {
			try {
				if(insStream != null){
					insStream.close();	
				}
				if(inputStreamReader != null){
					inputStreamReader.close();	
				}
				if(reader != null){
					reader.close();	
				}
			}
			catch(IOException e){
				logger.error("", e);
			}
		}

		logger.debug("Uploading records -- for --"+clazz.getName()+", uploaded records --"+dataList);
		return dataList;

	}
}
