package com.qfix.utilities;

import org.springframework.util.StringUtils;

import com.opencsv.bean.CsvToBeanFilter;
import com.opencsv.bean.MappingStrategy;

public class CsvEmptyRowFilter implements CsvToBeanFilter {

 	private final MappingStrategy strategy;

 	public CsvEmptyRowFilter(MappingStrategy strategy) {
 		this.strategy = strategy;
 	}

 	public boolean allowLine(String[] line) {
 		boolean allowLine = false;
 		for(String lineValue: line) {
 			if(!StringUtils.isEmpty(lineValue) && !StringUtils.isEmpty(lineValue.trim())) {
 				allowLine = true;
 				break;
 			}
 		}
 		
 		return allowLine;
 	}

 }