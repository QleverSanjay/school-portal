package com.qfix.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.qfix.model.ValidationError;


public class ValidatorUtil {
	// final static Logger logger = Logger.getLogger(ValidatorUtil.class);

	public static <S extends org.springframework.validation.Validator, T> ValidationError validate(T objectToValidate, S customValidator){
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		Validator validator = validatorFactory.getValidator();
		
		List<String> errorList = new ArrayList<>();
		Set<ConstraintViolation<T>> violations = validator.validate(objectToValidate);

		ValidationError validationError = new ValidationError();

		for (ConstraintViolation<T> violation : violations) {
		    String propertyPath = violation.getPropertyPath().toString();
		    String message = violation.getMessage();
		    errorList.add(propertyPath+" : "+message);    
		}

		checkCustomValidation(objectToValidate, customValidator, errorList);

		if(errorList.size() > 0){
			validationError.setErrors(errorList);
		}
		else {
			validationError = null;
		}

		return validationError;
	}

	private static <S extends org.springframework.validation.Validator, T> void checkCustomValidation(T objectToValidate, 
			S customValidator, List<String> errorList) {

		Errors errors = new BindException(objectToValidate, objectToValidate.getClass().getName());
		customValidator.validate(objectToValidate, errors);

		if(errors.hasErrors()){
			 List<FieldError> fError = errors.getFieldErrors();
			 for(FieldError error : fError){
				 errorList.add(error.getField() + " : " + error.getDefaultMessage());
			 }
		}
	}
	
	
	
	
	public static <S extends org.springframework.validation.Validator, T> ValidationError validateForAllErrors(T objectToValidate, S customValidator){
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		Validator validator = validatorFactory.getValidator();
		
		List<String> errorList = new ArrayList<>();
		Set<ConstraintViolation<T>> violations = validator.validate(objectToValidate);

		ValidationError validationError = new ValidationError();

		for (ConstraintViolation<T> violation : violations) {
		    String propertyPath = violation.getPropertyPath().toString();
		    String message = violation.getMessage();
		    errorList.add(propertyPath+" : "+message);
		}

		checkCustomValidationForAllErrors(objectToValidate, customValidator, errorList, validationError);

		if(errorList.size() > 0){
			validationError.setErrors(errorList);
		}
		else {
			validationError = null;
		}

		return validationError;
	}


	private static <S extends org.springframework.validation.Validator, T> void checkCustomValidationForAllErrors(T objectToValidate, 
			S customValidator, List<String> errorList, ValidationError validationError) {

		Errors errors = new BindException(objectToValidate, objectToValidate.getClass().getName());
		customValidator.validate(objectToValidate, errors);
		
		if(errors.hasErrors()){
			 List<ObjectError> fError = errors.getAllErrors();
			 for(ObjectError error : fError){
				 errorList.add(error.getDefaultMessage());
			 }
		}
	}
}