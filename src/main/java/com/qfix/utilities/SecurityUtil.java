package com.qfix.utilities;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.util.StringUtils;

import com.qfix.exceptions.ApplicationException;

// References : https://www.javacodegeeks.com/2012/05/secure-password-storage-donts-dos-and.html
public class SecurityUtil {

	private static final int PASSWORD_HASH_ITERATION_COUNT = 10;

	public static byte[] hashPassword(final String password, final byte[] salt) throws ApplicationException {

		if (StringUtils.isEmpty(password) || salt == null) {
			throw new ApplicationException("Required parameters are missing.");
		}

		try {
			final int keyLength = 160;
			// TODO move to PBKDF2WithHmacSHA512 with JDK 1.8 as SHA1 is less
			// secure.
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, PASSWORD_HASH_ITERATION_COUNT, keyLength);
			SecretKey key = skf.generateSecret(spec);
			byte[] res = key.getEncoded();
			return res;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] generateSalt() throws NoSuchAlgorithmException {
		// VERY important to use SecureRandom instead of just Random
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		// Generate a 8 byte (64 bit) salt as recommended by RSA PKCS5
		byte[] salt = new byte[8];
		random.nextBytes(salt);
		return salt;
	}

	public static boolean authenticate(String attemptedPassword, byte[] encryptedPassword, byte[] salt)
					throws NoSuchAlgorithmException, InvalidKeySpecException, ApplicationException {
		// Encrypt the clear-text password using the same salt that was used to
		// encrypt the original password
		byte[] encryptedAttemptedPassword = hashPassword(attemptedPassword, salt);

		// Authentication succeeds if encrypted password that the user entered
		// is equal to the stored hash
		System.out.println("encryptedAttemptedPassword length" + encryptedAttemptedPassword.length);
		System.out.println("encryptedPassword length" + encryptedPassword.length);

		return Arrays.equals(encryptedPassword, encryptedAttemptedPassword);
	}

	private static Connection getConnection() throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/payment";
        String user = "root";
        String password = "root";
        Connection connection = DriverManager.getConnection(url, 
            user, password);
        return connection;
	}

	public static void updateUser (Connection connection) throws Exception{
		 byte[] randomSalt = SecurityUtil.generateSalt();
 		byte[] passwordHash = SecurityUtil.hashPassword("gThZAbHvCtyla7jl7nhk", randomSalt);
 		System.out.println("updating..");
 		String userSql = "update `user` set `password` = ?, `salt` = ?" +
				" WHERE username = 'user-service'";

 		PreparedStatement preparedStatementUser = connection.prepareStatement(userSql);
		preparedStatementUser.setBytes(1, passwordHash);
		preparedStatementUser.setBytes(2, randomSalt);
//		preparedStatementUser.setString(3, "123456");
		preparedStatementUser.executeUpdate();
		System.out.println("updated..");
	}

	public static void main(String[] args) throws SQLException
    {
		Connection connection = null;
		try {
			connection = getConnection();
			 updateUser(connection);
//			insertUser(connection);
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(connection != null){
				connection.close();
			}
		}
    }

	private static void insertUser(Connection connection) throws Exception{
        byte[] randomSalt = SecurityUtil.generateSalt();
		byte[] passwordHash = SecurityUtil.hashPassword("QF!X-Payment#!spatcher@dm!n@8090", randomSalt);

		String userSql = "INSERT INTO `user` (`username`, `password`,`password_salt`,`temp_password`,`active`, `isDelete`) " +
						 "values (?, ?, ?, ?, ?, ?)";

		PreparedStatement preparedStatementUser = connection.prepareStatement(userSql);
		preparedStatementUser.setString(1, "QF!X-Payment#1spatcher@dm!n" );
		preparedStatementUser.setBytes(2, passwordHash);
		preparedStatementUser.setBytes(3, randomSalt);
		preparedStatementUser.setString(4, "QF!X-Payment#!spatcher@dm!n@8090");
		preparedStatementUser.setString(5, "Y");
		preparedStatementUser.setString(6, "N");
		preparedStatementUser.executeUpdate();
		System.out.println("inserted..");
	}
}
