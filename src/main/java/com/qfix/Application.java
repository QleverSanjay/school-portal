package com.qfix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import com.google.gson.Gson;

@SpringBootApplication(exclude={})
@ComponentScan({"com.qfix"})
@ImportResource({ "classpath:notification-sender.xml" })
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public Gson gson(){
		return new Gson();
	}
}
