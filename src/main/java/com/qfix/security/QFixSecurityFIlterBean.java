package com.qfix.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.qfix.exceptions.UnauthorizedException;
import com.qfix.model.User;


public class QFixSecurityFIlterBean implements HandlerInterceptor{

	private static List<String> ALLOWED_URLS = new ArrayList<>();

	static {
		ALLOWED_URLS.add("/");
		ALLOWED_URLS.add("/school/boards");
		ALLOWED_URLS.add("/error");
		ALLOWED_URLS.add("/users/user-role");
	}

	private static String ALLOWED_FILES_REGEX = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp|js|css|html|pdf|xls|xlsx))$)";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		HttpSession session = request.getSession();
		String url = request.getRequestURI().toString();

		boolean canAccessUrl = ALLOWED_URLS.contains(url) || url.matches(ALLOWED_FILES_REGEX );

		if(session != null){
			User user = (User) session.getAttribute("currentUser");
			if( (!canAccessUrl && user == null )){
				response.setStatus(HttpStatus.SC_UNAUTHORIZED);
				throw new UnauthorizedException("User must be logged in..");
			}
		}
		else if(!canAccessUrl){
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			throw new UnauthorizedException("User must be logged in..");
		}

		return true;
	}

	
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}