package com.qfix.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//@EnableWebMvc
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    QFixSecurityFIlterBean localInterceptor() {
    	System.out.println("Creating filter Object.. FILTER ........*******");
    	return new QFixSecurityFIlterBean();
    }

    @Override
    public void addInterceptors(InterceptorRegistry  registry) {
    	System.out.println("Registring FILTER ........*******");
        registry.addInterceptor(localInterceptor());
    }

}