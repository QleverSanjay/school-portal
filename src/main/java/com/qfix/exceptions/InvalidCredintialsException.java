package com.qfix.exceptions;

public class InvalidCredintialsException extends Exception {
	
	private static final long serialVersionUID = 2639495546763759037L;
	private String message;
	
	public InvalidCredintialsException(){
		
	}
	
	public InvalidCredintialsException(String message){
		this.message = message;
	}
	
	public InvalidCredintialsException(String message,Throwable e){
		super(message,e);
		this.message = message;
		
	}
	
	public String getMessage(){
		return message;
	}

	@Override
	public String toString() {
		return "InvalidCredintialsException [message=" + message + "]";
	}
}
