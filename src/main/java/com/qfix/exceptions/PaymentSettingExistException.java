/**
 * 
 */
package com.qfix.exceptions;

/**
 * @author Dilip
 *
 */
public class PaymentSettingExistException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2639495546763759037L;
	private String message;
	
	public PaymentSettingExistException(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
	@Override
	public String toString() {
		return "PaymentSettingExistException [message=" + message + "]";
	}
	
	
}
