package com.qfix.exceptions;

public class ShoppingAccountNotCreatedException extends Exception {
	
	private static final long serialVersionUID = 2639495546763759037L;
	private String message;
	
	public ShoppingAccountNotCreatedException(){
		
	}
	
	public ShoppingAccountNotCreatedException(String message){
		this.message = message;
	}
	
	public ShoppingAccountNotCreatedException(String message,Throwable e){
		super(message,e);
		this.message = message;
		
	}
	
	public String getMessage(){
		return message;
	}

	@Override
	public String toString() {
		return "ShoppingAccountNotCreatedException [message=" + message + "]";
	}
}
