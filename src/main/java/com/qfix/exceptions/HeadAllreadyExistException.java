package com.qfix.exceptions;

public class HeadAllreadyExistException extends Exception {
	private static final long serialVersionUID = 2639495546763759037L;
	private String message;

	public HeadAllreadyExistException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "HeadAllreadyExistException [message=" + message + "]";
	}
}
