/**
 * 
 */
package com.qfix.exceptions;

/**
 * @author Dilip
 *
 */
public class StudentNotExistException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2639495546763759037L;
	private String message;
	
	public StudentNotExistException(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
	@Override
	public String toString() {
		return "StudentNotExistException [message=" + message + "]";
	}
	
	
}
