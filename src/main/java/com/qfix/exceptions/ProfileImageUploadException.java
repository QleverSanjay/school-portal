/**
 * 
 */
package com.qfix.exceptions;

import java.io.IOException;

/**
 * @author Dilip
 *
 */
public class ProfileImageUploadException extends IOException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2639495546763759037L;
	private String message;
	
	public ProfileImageUploadException(){
		
	}
	
	public ProfileImageUploadException(String message){
		this.message = message;
	}
	
	public ProfileImageUploadException(String message,Throwable e){
		super(message,e);
		this.message = message;
		
	}
	
	public String getMessage(){
		return message;
	}

	@Override
	public String toString() {
		return "FileNotCreatedException [message=" + message + "]";
	}
	
	
}
