package com.qfix.exceptions;

public class FeeStructureAlreadyExistException extends Exception {
	private static final long serialVersionUID = 2639495546763759037L;
	private String message;

	public FeeStructureAlreadyExistException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "FeeStructureAlreadyExistException [message=" + message + "]";
	}
}
