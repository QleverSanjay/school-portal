# School-Portal
--------------------
School related functionality is implemented in this module.


How to build and install
------------------------------

You will need to have JDK 1.7 + and latest maven installation on your system.


Application at present uses spring boot, so just follow below steps

To build: 
> Go to command prompt, navigate to root folder of the project i.e. SchoolPortal

> On command prompt type mvn clean install command as shown below
c:/workspace/SchoolPortal> mvn clean install

To run
> Deploy SchoolPortal.war file to tomcat server.



# Required services are listed below
---------------------------------------
chat-service
char-service.war file is available at resources/ folder.
Copy same file and deploy it in tomcat.
Please modify chat service URLs in src/com/qfix/service/user/client/chat/UrlConfiguration.java

# Change Logs

[Change logs](https://github.com/Qfix-Info/documentation) contains a detailed change log accross the organization. Detailed documentation can be found with admin.
